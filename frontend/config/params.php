<?php

return [
'necta_url'=>'http://localhost/necta_api/pub/results.php/necta','allocation_maximum_data_to_process'=>40,'appeal_base_directory'=>'attachments/appeal',
    'adminEmail' => 'admin@example.com',
	'emailReturnUrl' =>'http://localhost/ilms_repayment/index.php?r=repayment/employer/',
	'employeeExcelTemplate' =>'frontend/web/dwload',
	'refundAttachments' =>'refund_attachment/',
	'employerUploadExcelTemplate' =>'frontend/web/uploads/',
	'daysSinceLoanSummaryCreatedCheck' =>28,
	'deadlineDayForEmployerPenaltyEachMonth' =>15,
	'cancelBillReason'=>'Unconfirmed Bill',
	'beneficiaryDocument' =>'beneficiary_document/',
	'employerUpdateBeneficiariesSalaries' =>'frontend/web/uploads/',
];

//return [
//    'adminEmail' => 'admin@example.com',
//    'supportEmail' => 'support@example.com',
//    //'necta_url'=>'http://localhost/necta_api/pub/results.php/necta',
//    'necta_url'=>'http://localhost/necta_api/pub/results.php/necta',
//    'user.passwordResetTokenExpire' => 3600,
//    'languages'=>[
//                 'en'=>"English",
//                 'sw'=>"Kiswahili",
//    ]
//];