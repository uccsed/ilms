<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
use yii\helpers\Html;
 
?>
<div class="site-login">

    <div class="row">	
		<div class="row" style="margin:0px;margin-top: 30px;">
		<div class="column">
			<p style="margin:0px;font-size:16px;">
				If you are not registered yet;
			</p>
			<p>
				<strong>
					<a href="index.php?r=repayment/employer/create">
						<button type="button" class="btn btn-primary btn-sm" style="width:80%;font-size:16px;">
							NEW EMPLOYER REGISTRATION
						</button>
					</a>
				</strong>
			</p>
		</div>
		<div class="column">
			<p style="margin:0px;font-size:16px;">
				If you are registered already and want to login;
			</p>
			<p>
				<strong>
					<a href="index.php?r=application/default/home-page&amp;activeTab=login_tab_id">
						<button type="button" class="btn btn-primary btn-sm" style="width:80%;font-size:16px;background-color: #00C0EF;">
							LOGIN
						</button>
					</a>
				</strong>
			</p>
		</div>
	</div>
   
</div>
</div>