<?php
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use kartik\widgets\FileInput;
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL,'options' => ['enctype' => 'multipart/form-data'],
                'enableClientValidation' => TRUE,]);
/*
echo $form->field($model, 'employeesFile')->widget(FileInput::classname(), [
    'options' => ['accept' => 'verification/pdf'],
        'pluginOptions' => [
        'showCaption' => false,
        'showRemove' => TRUE,
        'showUpload' => false,
       // 'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="fa fa fa-file-pdf-o"></i> ',
        'browseLabel' =>  'Support Document (required format .xlsx,xls only)',
         'initialPreview'=>[
            "$model->employeesFile",
           
        ],
        'initialCaption'=>$model->employeesFile,
        'initialPreviewAsData'=>true,
    ]
]);
 * 
 */
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>1,
    'attributes'=>[
        'employeesFile'=>['type' => Form::INPUT_FILE,
            ],
        ],
]);
?>
  <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Upload' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  
<?php
echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
?>
 <?= Html::a('Cancel', ['index-view-beneficiary'], ['class' => 'btn btn-warning']) ?>
      <?php
ActiveForm::end();
?>
    </div>