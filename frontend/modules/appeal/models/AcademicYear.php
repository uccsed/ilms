<?php

namespace frontend\modules\appeal\models;

use Yii;

class AcademicYear extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'academic_year';
    }

}