
<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\bootstrap\ActiveForm;
//use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\depdrop\DepDrop;
use backend\modules\allocation\models\LearningInstitution;
use backend\modules\allocation\models\Programme;

/* @var $this yii\web\View */
/* @var $model backend\modules\appeal\models\Appeal */

$this->title = 'Create Appeal';
$this->params['breadcrumbs'][] = ['label' => 'Appeals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js = "$(document).ready(function(){
    $('.appeal_question').click(function(){
        
        var appealQnId = $(this).data('appealqnid');

        var response = $(this).val();

        $('.new-upload').click(function(e){
            var appealQnId = $(this).data('qn');
            
            attachmentField = '<br/><br/>".Html::label("Uploading New Supporting Document", null, ['class' => 'form-label'])."';
            attachmentField += '<input type=\"file\" name=\"answ['+appealQnId+']\" id=\"attachment-'+appealQnId+'\" class=\"form-control\" />'

            $('#attachment-container-'+appealQnId).html(attachmentField);

            return false;
        });

        $.get('".Url::to(['check-response'])."', {response:response, appealQnId:appealQnId}, function(data){
            var response = JSON.parse(data);

            if(response.success){

                attachmentField = '<br/><br/>".Html::label("Uploading Support Document", null, ['class' => 'form-label'])."';
                attachmentField += '<input type=\"file\" name=\"answ['+appealQnId+']\" id=\"attachment-'+appealQnId+'\" class=\"form-control\" />'

                $('#attachment-container-'+appealQnId).html(attachmentField);

                console.log($('#attachment-container'+appealQnId));
            }else{
                $('#attachment-container-'+appealQnId).html('');
            }
        });

    });
})";

$this->registerJs($js);
?>

<div class="appeal-create">
 
 <?php $form = ActiveForm::begin([
             'action' => ['appeal/update', 'id'=>$appeal->appeal_id],
             'method' => 'POST',
             'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>
        <?= Html::csrfMetaTags() ?>

        <div class="row">
            <div class="col-sm-6">
              
            </div>
            <div class="col-sm-6">

            </div>
        </div>

        <div class="padding-v-md">
            <div class="line line-dashed"></div>
        </div>
      
        <div class="panel panel-info">
            <div class="panel-heading">
                Edit Appeal
               
                <div class="clearfix"></div>
            </div>
            <div class="panel-body container-items"><!-- widgetContainer -->

                <?= Html::label('Appeal Type', null, ['class' => 'form-label']) ?>
                <?php 
                    $model->appeal_category_id = $appeal->appeal_category_id;        
                ?>
                
                <?php 
                //echo ;
                echo Form::widget([ // fields with labels
                    'model'=>$appeal,
                    'form'=>$form,
                    'columns'=>1,
                    'attributes'=>[
                        'appeal_category_id' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            'label' => 'What are your reasons for Appeal ?/Nini sababu kuu za kuwasilisha rufaa yako?',
                            'options' => [
                                'data' => ArrayHelper::map($appealCategories, 'appeal_category_id', 'name'),
                                'options' => [
                                    'prompt' => 'Select Appeal Reason',
                                    //'onchange' => 'check_reset()',
                                    // 'id'=>'region_Id'
                                ],
                            ],
                        ],
                        'institution_id' => ['type' => Form::INPUT_WIDGET,
                                          'widgetClass' => \kartik\select2\Select2::className(),
                                           'label' => 'Institution Name',
                                           'options' => [
                                               'data' => ArrayHelper::map(LearningInstitution::find()->where(["institution_type"=>"UNIVERSITY"])->all(),'learning_institution_id','institution_name'),
                                              'options' => [
                                                        'prompt' => 'Select Institution Name',
                                                     
                                                ],
                                       ],
                                    ],
                        
                    ]
                ]);
                echo $form->field($appeal, 'programme_code')->widget(DepDrop::classname(), [
                    'type'=>DepDrop::TYPE_SELECT2,
                    'data'=>ArrayHelper::map(Programme::find()->where(['learning_institution_id'=>$appeal->institution_id])->all(),'programme_id','programme_name'),
                    'options'=>['id'=>'subcat1-id', 'placeholder'=>'Select ...'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['appeal-institution_id'],
                        'url'=>Url::to(['/appeal/appeal/programme-name']),
                        'params'=>['input-type-1', 'input-type-2']
                    ]
                ]);
                ?>
                

                <br/>

                <?php foreach($appealQuestions as $aq) {?>
                    
                    <div class="form-group">
                        <?= Html::label($aq->getQuestionString()." ?", null, ['class' => 'form-label']) ?>
                        <br/>

                        <?php 
                            if(in_array($aq->appeal_question_id, array_keys($appealAttachments))) 
                            {
                                $viewAttached = true; 
                            }else{
                                $viewAttached = false; 
                            }
                        ?>

                        <?php foreach($aq->possibleAnswers() as $pa) { ?>

                            <?php if($aq->expected_response_id == $pa['qresponse_list_id'] && $viewAttached) { ?>
                               <input 
                                    type="radio" 
                                    name="qn[<?=$aq->appeal_question_id?>]"  
                                    checked='checked' 
                                    value="<?=$pa['qresponse_list_id']?>" 
                                    data-appealqnid="$aq->appeal_question_id", 
                                    class='form-radio appeal_question' 
                                />
                                <strong><?=$pa['response']?></strong>
                            <?php } else {?>
                                <?= Html::radio('qn['.$aq->appeal_question_id.']', null, ['label'=>$pa['response'], 'checked'=>'checked', 'value'=>$pa['qresponse_list_id'], 'data-appealqnid'=>$aq->appeal_question_id, 'class'=>'form-radio appeal_question'] ) ?>
                            <?php } ?>

                        <?php 
                            } 
                        ?>

                        <?php if($viewAttached) { ?>
                            <?= Html::a('ViewAttached File', ['appeal/download', 'id'=>$appealAttachments[$aq->appeal_question_id]],['style'=>'margin-left:30px']); ?>
                            <a href='#' style='margin-left:30px' class='new-upload appeal_question' data-qn='<?= $aq->appeal_question_id ?>'>Upload New attachment</a>
                        <?php 
                            } 
                        ?>
                        <br/>
                        
                        <span class="form-group" id='attachment-container-<?= $aq->appeal_question_id ?>'>
                        </span>
                    </div>

                    <br/><br/>
            
                <?php }?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Update' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                </div>
            </div>
        </div>
             <?php
ActiveForm::end();
?>


</div>
