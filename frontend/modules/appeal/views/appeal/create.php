<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\depdrop\DepDrop;
use backend\modules\allocation\models\LearningInstitution;

/* @var $this yii\web\View */
/* @var $model backend\modules\appeal\models\Appeal */

$this->title = 'Create Appeal';
$this->params['breadcrumbs'][] = ['label' => 'Appeals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js = "$(document).ready(function(){
    $('.appeal_question').click(function(){
        
        var appealQnId = $(this).data('appealqnid');

        var response = $(this).val();

        $.get('".Url::to(['check-response'])."', {response:response, appealQnId:appealQnId}, function(data){
            var response = JSON.parse(data);
                
            if(response.success){

                attachmentField = '<br/><br/>".Html::label("Uploading Support Document", null, ['class' => 'form-label'])."';
                attachmentField += '<input type=\"file\" name=\"answ['+appealQnId+']\" id=\"attachment-'+appealQnId+'\" class=\"form-control\" />'

                $('#attachment-container-'+appealQnId).html(attachmentField);

                console.log($('#attachment-container'+appealQnId));
            }else{
                $('#attachment-container-'+appealQnId).html('');
            }
        });

    });
})";

$this->registerJs($js);
?>

<div class="appeal-create">

    <div class="customer-form">
      <?php 
      // $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]);
      //$form = ActiveForm::begin(['appeal/store', 'id'=>$appeal->appeal_id],['enctype' => 'multipart/form-data']);
     
      ?>
      <?php $form = ActiveForm::begin([
             'action' => ['appeal/store', 'id'=>$appeal->appeal_id],
             'method' => 'POST',
             'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>
        <?= Html::csrfMetaTags() ?>

        <div class="row">
            <div class="col-sm-6">
              
            </div>
            <div class="col-sm-6">

            </div>
        </div>

        <div class="padding-v-md">
            <div class="line line-dashed"></div>
        </div>
      
        <div class="panel panel-info">
            <div class="panel-heading">
                Create Appeal
               
                <div class="clearfix"></div>
            </div>
            <div class="panel-body container-items"><!-- widgetContainer -->
 
                <?php 
                //echo Yii::getAlias('@webroot');
                echo Form::widget([ // fields with labels
                    'model'=>$appeal,
                    'form'=>$form,
                    'columns'=>1,
                    'attributes'=>[
                        'appeal_category_id' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            'label' => 'What are your reasons for Appeal ?/Nini sababu kuu za kuwasilisha rufaa yako?',
                            'options' => [
                                'data' => ArrayHelper::map($appealCategories, 'appeal_category_id', 'name'),
                                'options' => [
                                    'prompt' => 'Select Appeal Reason',
                                    //'onchange' => 'check_reset()',
                                    // 'id'=>'region_Id'
                                ],
                            ],
                        ],
                        'institution_id' => ['type' => Form::INPUT_WIDGET,
                                          'widgetClass' => \kartik\select2\Select2::className(),
                                           'label' => 'Institution Name (College Name)',
                                           'options' => [
                                               'data' => ArrayHelper::map(LearningInstitution::find()->where(["institution_type"=>"UNIVERSITY"])->all(),'learning_institution_id','institution_name'),
                                              'options' => [
                                                        'prompt' => 'Select Institution Name/College Name',
                                                     
                                                ],
                                       ],
                                    ],
                        
                    ]
                ]);
               echo $form->field($appeal, 'programme_code')->widget(DepDrop::classname(), [
                    'type'=>DepDrop::TYPE_SELECT2,
                    //'data'=>[2 => 'Tablets'],
                    'options'=>['id'=>'subcat1-id', 'placeholder'=>'Select ...'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['appeal-institution_id'],
                        'url'=>Url::to(['/appeal/appeal/programme-name']),
                        'params'=>['input-type-1', 'input-type-2']
                    ]
                ]);
                ?>
                <br/>
                <?php foreach($appealQuestions as $aq) {?>
                    
                    <div class="form-group">
                        <?= Html::label($aq->getQuestionString()." ", null, ['class' => 'form-label']) ?>
                        <br/>
                        <?php foreach($aq->possibleAnswers() as $pa) { ?>
                            <?= Html::radio('qn['.$aq->appeal_question_id.']', null, ['label'=>$pa['response'], 'value'=>$pa['qresponse_list_id'], 'data-appealqnid'=>$aq->appeal_question_id, 'class'=>'form-radio appeal_question'] ) ?>
                        <?php } ?>
                        <span class="form-group" id='attachment-container-<?= $aq->appeal_question_id ?>'>
                        </span>
                    </div>

                    <br/><br/>
            
                <?php }?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                </div>
            </div>
        </div>
          <?php
ActiveForm::end();
?>

</div>
