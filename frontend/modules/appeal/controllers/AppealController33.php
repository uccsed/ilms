<?php

namespace frontend\modules\appeal\controllers;

use Yii;
use frontend\modules\appeal\models\Appeal;
use frontend\modules\appeal\models\Applicant;
use frontend\modules\appeal\models\Application;
use frontend\modules\appeal\models\AppealCategory;
use frontend\modules\appeal\models\AppealQuestion;
use frontend\modules\appeal\models\AppealAttachment;
use frontend\modules\appeal\models\AppealDate;
use frontend\modules\appeal\models\AppealPrice;
use frontend\modules\appeal\models\ApplicantCagetory;
use frontend\modules\appeal\models\AcademicYear;
use frontend\modules\appeal\models\AppealPlan;

use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\allocation\models\Programme;

class AppealController extends \yii\web\Controller
{
    public $layout="main_public_beneficiary";

    public function actionIndex()
    {
        $applicant = $this->applicant();
       
        if($applicant != null){

            $appeal = Appeal::find()->where(['application_id'=>$applicant->application->application_id])->one();
            //  print_r($appeal); || $appeal->need_payments == 0
            // exit();
            if($appeal != null){
                
                if($appeal->control_number == null && $appeal->need_payments == 1){
                    return $this->render("controll_number", ['appeal'=>$appeal]);
                }else if($appeal->submitted == 0 || $appeal->submitted == 1){
                    return $this->redirect(['appeal/view', 'id'=>$appeal->appeal_id]);
                }
                else if(($appeal->receipt_number != null && $appeal->submitted == -1)||$appeal->receipt_number==0){
                    return $this->redirect(['appeal/create', 'id'=>$appeal->appeal_id]);
                    
                    //exit();
                }
 
                return $this->render("payment_instruction", ['appeal'=>$appeal]);
            }
            
        }
        
        $appealPlan = $this->appealPlan();

        if($appealPlan == null || $appealPlan->status == 0){
            return $this->render('appeal_closed');
        }

        $appealPrice = AppealPrice::find()->where(['status'=>1])->one();

        if($appealPrice != null){
            $price = $appealPrice->price;
        }else{
            //$price =  0;
            $appeal1 = new Appeal();
            $appeal1->application_id =$applicant->application->application_id;
            $appeal1->current_study_year = $applicant->application->current_study_year;
            $appeal1->submitted = -1;
            $appeal1->need_payments =0;
            $appeal1->save(false);
            return $this->redirect(['appeal/create', 'id'=>$appeal1->appeal_id]);
        }

        return $this->render('appeal_instruction', ['price'=>$price]);
    }

    public function appealPlan(){

        $applicant = $this->applicant();

        $academicYear = AcademicYear::find()->where(['is_current'=>1])->one();

        $appealPlan = null;

        if($academicYear != null){
            $appealPlan = AppealPlan::find()
                ->where(['applicant_category_id'=>$applicant->application->applicantCategory->applicant_category_id])
                ->where(['academic_year_id'=>$academicYear->academic_year_id,'status'=>1])->one();
        }
     return $appealPlan;
    }

    public function actionCheckResponse(){

        $data = Yii::$app->request->get();

        $appealQuestionId = $data['appealQnId'];
        $expectedResponseId = $data['response'];

        $appealQuestion = AppealQuestion::find()->where(['appeal_question_id'=>$appealQuestionId])->one();
    
        if($appealQuestion != null){
            if($appealQuestion->expected_response_id == trim($expectedResponseId)){
                return json_encode(['success'=>true]);
            }
        }

        return json_encode(['success'=>false]);
    }
  public function actionCreate($id)
    {
        $applicant = $this->applicant();
        $appeal = Appeal::find()->where(['application_id'=>$applicant->application->application_id])->one();
        $appealPlan = $this->appealPlan();

        if($appealPlan != null){
            $appealQuestions = AppealQuestion::find()->where(['appeal_plan_id'=>$appealPlan->appeal_plan_id])->all();
        }else{
            $appealQuestions = [];            
        }
       // print_r($appealQuestions);
       /// exit();
        $appelCategory = AppealCategory::find()->all();
        $model = new AppealCategory();
        $models = [new AppealCategory()];
     return $this->render('create', ['model'=>$model, 'models'=>$models, 'appeal'=>$appeal, 'appealQuestions'=>$appealQuestions, 'appealCategories'=>$appelCategory]);
    }
    public function actionControllNumber($id)
    {
        $applicant = $this->applicant();

        $appeal = Appeal::find()->where(['application_id'=>$applicant->application->application_id])->one();

        $appeal->control_number = uniqid();
        
        $appeal->save(false);

        //return $this->redirect(['create', 'id'=>$id]);
    }


    public function actionPay($id)
    {
        $applicant = $this->applicant();

        $appeal = Appeal::find()->where(['application_id'=>$applicant->application->application_id])->one();

        $appeal->receipt_number = uniqid();
        $appeal->amount_paid = 45000;
        $appeal->pay_phone_number = "+255713241438";
        
        $appeal->save(false);

        //return $this->redirect(['create', 'id'=>$id]);
    }

    public function actionRequestControllNumber()
    {
        $applicant = $this->applicant();

        $application = Application::find()->where(['applicant_id'=>$applicant->applicant_id])->one();
        
        $appeal = Appeal::find()->where(['application_id'=>$applicant->application->application_id])->one();

        $appealPrice = AppealPrice::find()->where(['status'=>'1'])->one();

        $needPayments = 0;

        if($appealPrice != null && $appealPrice->price > 0){
            $needPayments = 1;
        }

        if($appeal == null){

            $appeal = new Appeal();
            $appeal->application_id = $application->application_id;
            $appeal->current_study_year = $application->current_study_year;
            $appeal->submitted = -1;
            $appeal->need_payments = $needPayments;
            $appeal->save(false);
            
            if($needPayments == 1){
                $this->storeBillNumberToDetails($appeal);
            }
            
        }

        return $this->redirect(['appeal/index']);
    }

    public function applicant(){
        
        $userId = Yii::$app->user->identity->user_id;
    
        $applicant = Applicant::find()->where(['user_id'=>$userId])->one();

        if ($applicant !== null) {
            return $applicant;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
    }

    /**
     * Finds the Appeal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Appeal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        
        if (($model = Appeal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionStore($id)
    {
    
        $data = Yii::$app->request->post();

        $appelCategory = $data['Appeal']['appeal_category_id'];
       
         
      //  $appelCategory = $data['Appeal']['appeal_category_id'];
        //$appelCategory = $data['Appeal']['appeal_category_id'];
        if(empty($appelCategory)){
            Yii::$app->session->setFlash('error', "Appeal type not selected.");
            return $this->redirect(["create", 'id'=>$id]);
        }
        $appeal = $this->findModel($id);
        $appeal->load(Yii::$app->request->post());
       // print_r($appeal);
        //print_r($appeal);
      //  exit();
        $appealQns = $data['qn'];
        
        $appealAttachments = [];

        $count = 0;

        foreach($appealQns as $k=>$qn){
           
            $appealQn = AppealQuestion::find()->where(['appeal_question_id'=>$k])->one();

            if($appealQn != null && $appealQn->expected_response_id == trim($qn)){

                $appAtt = new AppealAttachment();
                $att = UploadedFile::getInstanceByName('answ['.$k.']');
                
                $fName = Yii::$app->security->generateRandomString().'.'.$att->extension;

                $basePath = trim(Yii::$app->params['appeal_base_directory']);
               
                $basePath = rtrim($basePath, '/');

                $path = $att->saveAs(Yii::$app->params['appeal_base_directory'].'/'.$fName);

                $appAtt->appeal_id = $id;
                $appAtt->appeal_question_id = $k;
                $appAtt->attachment_path = $fName;
                
                $appAtt->verification_status = -1;

                $appAtt->save(false);
                $count++;
            }

        }

        if($count == 0){
            Yii::$app->session->setFlash('error', "At least on question should be yes");
            return $this->redirect(["create", 'id'=>$id]);
        }

        $appeal->submitted = 0;
        //$appeal->appeal_category_id = $appelCategory;
        $appeal->verification_status = -1;
        $appeal->save(false);

        Yii::$app->session->setFlash('success', "Appeal draft created.");

        //exit;
        return $this->redirect(['view', 'id' => $id]);
    }


    public function actionDownload($id){
        
        $att = AppealAttachment::findOne(['appeal_attachment_id'=>$id]);

        $basePath = trim(Yii::$app->params['appeal_base_directory']);
        $basePath = rtrim($basePath, '/');
        
        $file = $basePath.'/'.$att->attachment_path;

        if (file_exists($file)) {
            return Yii::$app->response->sendFile($file);
        }else{
            echo "File Doesn't Exist";
        }

    }

    /**
     * Displays a single Appeal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $appeal = $this->findModel($id);

        $appealAttachments = new ActiveDataProvider([
            'query' => AppealAttachment::find()->where(['appeal_id'=> $id]),
        ]);

        return $this->render('view', [
            'appeal' => $appeal, 'appealAttachments'=>$appealAttachments
        ]);
    }


    public function actionDeleteAttachment($id)
    {

        $attachment = $this->findAttachmentModel($id);

        $appeal = $this->findModel($attachment->appeal_id);

        $attachment->delete();

        return $this->redirect(['appeal/view', "id"=>$appeal->appeal_id]);
    }

    protected function findAttachmentModel($id)
    {
        if (($model = AppealAttachment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Updates an existing Appeal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        
        if($request->isPost){
            $appeal = $this->findModel($id);
            
           // print_r($appeal);
          //  exit();
            if($appeal->submitted == 1){
                return $this->redirect(["appeal/view", 'id'=>$id]);    
            }

            $this->updateEntry($id, $request);

            Yii::$app->session->setFlash('success', "Appeal updated successfully.");
            
            return $this->redirect(['appeal/view', 'id'=>$id]);
        }else{
            
            $appeal = $this->findModel($id);

            if($appeal->submitted == 1){
                return $this->redirect(["appeal/view", 'id'=>$id]);    
            }

            $appelCategory = AppealCategory::find()->all();
            $model = new AppealCategory();
            $models = [ new AppealCategory()];
            $appealPlan = $this->appealPlan();
            
            if($appealPlan != null){
                $appealQuestions = AppealQuestion::find()->where(['appeal_plan_id'=>$appealPlan->appeal_plan_id])->all();
            }else{
                $appealQuestions = [];
            }
           // $appealQuestions = AppealQuestion::find()->all();

            $appealAtts = AppealAttachment::find()->where(['appeal_id'=> $id])->all();

            $appealAttachments = [];

            foreach($appealAtts as $ap){
                $appealAttachments[$ap->appeal_question_id] = $ap->appeal_attachment_id;
            }

            return $this->render('update', ['model'=>$model, 'models'=>$models, 'appealAttachments'=>$appealAttachments, 'appeal'=>$appeal, 'appealQuestions'=>$appealQuestions, 'appealCategories'=>$appelCategory]);
        }
    }

    public function actionSubmitAppeal($id){
        
        $appeal = $this->findModel($id);
        
        //$applicant = $this->applicant();

        //sif($appeal->applicant_id == 122)
        {
            //check if one or more appeal question attempted
            
            $appeal->submitted = 1;
            $appeal->save(false);

            $appeal = $this->findModel($id);

            Yii::$app->session->setFlash('success', "Appeal submitted successfully.");

            return $this->redirect(["appeal/view", "id"=>$id]);
        }

       
    }

    private function updateEntry($id,$request){

        $data = $request->post();

        ///$appelCategory = $data['AppealCategory']['appeal_category_id'];

        $appeal = Appeal::findOne(['appeal_id'=> $id]);
        $appeal->load($request->post());
        //$appeal->appeal_category_id = $appelCategory;

        $appealQns = $data['qn'];
        $appealAttachments = [];

        foreach($appealQns as $k=>$qn){
           
            $att = UploadedFile::getInstanceByName('answ['.$k.']');

            if($qn == "0" && $att == null){
                continue;
            } else if ($qn == "0" && $att != null){

                $appAtt = AppealAttachment::find()->where(['appeal_id'=>$id, 'appeal_question_id'=>$k])->one();
                
                if($appAtt == null){
                    $appAtt = new AppealAttachment();
                }

                $fName = Yii::$app->security->generateRandomString().'.'.$att->extension;
                
                $basePath = trim(Yii::$app->params['appeal_base_directory']);
                $basePath = trim($basePath, '/');
                
                $path = $att->saveAs($basePath.'/'.$fName);
                if($path){
                $appAtt->appeal_id = $id;
                $appAtt->appeal_question_id = $k;
                $appAtt->attachment_path = $fName;
                $appAtt->verification_status = -1;

                $appAtt->save(false);
                }
                else{
                   // print_r($path);
                    //echo "Mickidadi Khalifa";
                }
               // exit();
            }

        }
        
        $appeal->save();


    }

    private function storeBillNumberToDetails($appeal){

        $apepalId = $appeal->appeal_id;
        
        $amount = $appeal->amount_paid;

        $applicant = $this->applicant();

        $bill_number = "APPEAL-".$appeal->appeal_id;
        
        $name = $applicant->user->firstname." ".$applicant->user->middlename." ".$applicant->user->surname;

        $phone_number = $applicant->user->phone_number;
        
        $email = $applicant->user->email_address;

        $applicantId = $applicant->applicant_id;

        $indexNumber = $applicatDetails->f4indexno;

        $appeal->bill_number = $bill_number;

        $appeal->save();

        $dataToQueue = ['billNumber' => $bill_number, 
                        "amount_paid" => $amount, 
                        "name" => $name, 
                        "phone_number" => $phone_number, 
                        "email" => $email, 
                        "applicantID" => $applicantId, 
                        "indexNumber" => $indexNumber];


        //Producer::queue("GePGBillSubmitionQueue", $dataToQueue);
    }
   public function actionProgrammeName() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                //$region_id =$parents[0];
                $institution_id =$parents[0];
                $out = Appeal::getProgrammeName($institution_id);
                echo \yii\helpers\Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        //$out =  \common\models\District::getWard(1);
        $out = Appeal::getProgrammeName(2);
        echo \yii\helpers\Json::encode(['output' => $out, 'selected' => '']);
        return;
    }
}
