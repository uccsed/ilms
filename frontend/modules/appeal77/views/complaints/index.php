<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Complaints';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="complaint-index">
    <div class="panel panel-info">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            
            <div class="panel-body">
            
                <br>
                <?php if($token != null) { ?>
                    <p>
                        <?= Html::a('Create Complaint', ['create', 'token'=>$token], ['class' => 'btn btn-success pull-right']) ?>
                    </p>
                <?php }?>

            <br/><br/>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute'=>'complaint',
                        'contentOptions'=>['style'=>'width: 45%;']
                    ],
                    [
                        'attribute'=>'complaintCategory.complaint_category_name',
                        'label'=>'Category'
                    ],
                    [
                        'attribute'=>'creatorName',
                        'label'=>'Creator'
                    ],
                    [
                        'attribute'=>'statusValue',
                        'label'=>'Status'
                    ],
                    
                    // 'created_by',
                    // 'updated_by',
                    // 'created_at',
                    // 'updated_at',

                    [
                            'class' => 'yii\grid\ActionColumn', 
                            'template' => '{view}{edit}{delete}',
                            'buttons' => ['view' => function($url, $model) {
                                    return Html::a('<b class="fa fa-eye"></b>', ['complaints/view', 'id'=>$model->complaint_id]);
                                },
                                'edit'=>function($url, $model){
                                    if($model->level != 1)
                                    {
                                        return Html::a('<b class="fa fa-pencil"></b>', ['complaints/update', 'id'=>$model->complaint_id]);
                                    }
                                },
                                'delete'=>function($url, $model){
                                    {
                                        if($model->level != 1)
                                        {
                                            return Html::a('<b class="fa fa-trash"></b>', ['complaints/delete', 'id'=>$model->complaint_id], ['data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                                            'data-method' => 'post', 'data-pjax' => '0']);
                                        }
                                    }
                                }
                            ]
                            ,
                        
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>