<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\appeal\models\Appeal */

$this->title = 'Create Appeal';
$this->params['breadcrumbs'][] = ['label' => 'Appeals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js = "$(document).ready(function(){
    $('.appeal_question').click(function(){
        
        var appealQnId = $(this).data('appealqnid');

        var response = $(this).val();

        $.get('".Url::to(['check-response'])."', {response:response, appealQnId:appealQnId}, function(data){
            var response = JSON.parse(data);

            if(response.success){

                attachmentField = '<br/><br/>".Html::label("Uploading Support Document", null, ['class' => 'form-label'])."';
                attachmentField += '<input type=\"file\" name=\"answ['+appealQnId+']\" id=\"attachment-'+appealQnId+'\" class=\"form-control\" />'

                $('#attachment-container-'+appealQnId).html(attachmentField);

                console.log($('#attachment-container'+appealQnId));
            }else{
                $('#attachment-container-'+appealQnId).html('');
            }
        });

    });
})";

$this->registerJs($js);
?>

<div class="appeal-create">

    <div class="customer-form">

        <?= Html::beginForm(['appeal/store', 'id'=>$appeal->appeal_id], 'post', ['enctype' => 'multipart/form-data']) ?>
        
        <?= Html::csrfMetaTags() ?>

        <div class="row">
            <div class="col-sm-6">
              
            </div>
            <div class="col-sm-6">

            </div>
        </div>

        <div class="padding-v-md">
            <div class="line line-dashed"></div>
        </div>
      
        <div class="panel panel-info">
            <div class="panel-heading">
                Create Appeal
               
                <div class="clearfix"></div>
            </div>
            <div class="panel-body container-items"><!-- widgetContainer -->

                <?= Html::label('Appeal Type', null, ['class' => 'form-label']) ?>
                <?= Html::activeDropDownList($model, 'appeal_category_id', ArrayHelper::map($appealCategories, 'appeal_category_id', 'name'),  ['prompt'=>'Select Appeal Type','class'=>'form-control'] ) ?>

                <br/>
                <?php foreach($appealQuestions as $aq) {?>
                    
                    <div class="form-group">
                        <?= Html::label($aq->getQuestionString()." ?", null, ['class' => 'form-label']) ?>
                        <br/>

                        <?php foreach($aq->possibleAnswers() as $pa) { ?>
                            <?= Html::radio('qn['.$aq->appeal_question_id.']', null, ['label'=>$pa['response'], 'value'=>$pa['qresponse_list_id'], 'data-appealqnid'=>$aq->appeal_question_id, 'class'=>'form-radio appeal_question'] ) ?>
                        <?php } ?>
                        <span class="form-group" id='attachment-container-<?= $aq->appeal_question_id ?>'>
                        </span>
                    </div>

                    <br/><br/>
            
                <?php }?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                </div>
            </div>
        </div>
       
        <?= Html::endForm() ?>

</div>
