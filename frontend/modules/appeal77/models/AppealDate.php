<?php

namespace frontend\modules\appeal\models;

use Yii;

/**
 * This is the model class for table "appeal_dates".
 */
class AppealDate extends \yii\db\ActiveRecord
{


        /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appeal_dates';
    }
    
}
