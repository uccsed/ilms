<?php

namespace frontend\modules\appeal\models;

use Yii;

class AppealPlan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appeal_plan';
    }

}