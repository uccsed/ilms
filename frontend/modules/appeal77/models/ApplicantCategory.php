<?php

namespace frontend\modules\appeal\models;

use Yii;

class ApplicantCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'applicant_category';
    }

}