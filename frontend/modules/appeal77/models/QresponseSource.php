<?php

namespace frontend\modules\appeal\models;

use Yii;

class QresponseSource extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qresponse_source';
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponseList()
    {
        return $this->hasMany(QResponseList::className(), ['qresponse_source_id' => 'qresponse_source_id']);
    }

}