<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\husernameelpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model frontend\modules\repayment\models\Employer */
/* @var $form yii\widgets\ActiveForm */
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]);
?>



<body class="full-width page-condensed">

    <div class="login-wrapper">
 <div class="form-box " id="login-box">
   <div class="well col-lg-6 col-lg-12" >
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
     <p>Please enter new login credentials.</p>

                <div class="form-group has-feedback">
                     <?= $form->field($model, 'password')->passwordInput() ?>

                </div>

                <div class="form-group has-feedback">
               <?= $form->field($model, 'confirm_password')->passwordInput() ?>

                </div>
              <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'captchaAction'=>'/site/captcha',
                ]) ?>
                <div class="row form-actions">
                    <div class="col-lg-6 ">
                    <div class="col-xs-6">
                        <button type="submit" class="btn btn-warning pull-right"><i class="fa   fa-forward"> </i> Submit </button>
                    </div>

                </div>


   </div>


<?php
ActiveForm::end();
?>
    </div>

