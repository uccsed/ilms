<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.5.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker',
    ),
  ),
  'kartik-v/yii2-date-range' => 
  array (
    'name' => 'kartik-v/yii2-date-range',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/daterange' => $vendorDir . '/kartik-v/yii2-date-range',
    ),
  ),
  '2amigos/yii2-date-picker-widget' => 
  array (
    'name' => '2amigos/yii2-date-picker-widget',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@dosamigos/datepicker' => $vendorDir . '/2amigos/yii2-date-picker-widget/src',
    ),
  ),
  'kartik-v/yii2-widget-depdrop' => 
  array (
    'name' => 'kartik-v/yii2-widget-depdrop',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/depdrop' => $vendorDir . '/kartik-v/yii2-widget-depdrop',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.12.2.0',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'yiister/yii2-adminlte' => 
  array (
    'name' => 'yiister/yii2-adminlte',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@yiister/adminlte' => $vendorDir . '/yiister/yii2-adminlte',
    ),
  ),
  'drsdre/yii2-wizardwidget' => 
  array (
    'name' => 'drsdre/yii2-wizardwidget',
    'version' => '1.2.1.0',
    'alias' => 
    array (
      '@drsdre/wizardwidget' => $vendorDir . '/drsdre/yii2-wizardwidget',
    ),
  ),
  'beastbytes/yii2-wizard' => 
  array (
    'name' => 'beastbytes/yii2-wizard',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@beastbytes/wizard' => $vendorDir . '/beastbytes/yii2-wizard',
    ),
  ),
  'kartik-v/yii2-tabs-x' => 
  array (
    'name' => 'kartik-v/yii2-tabs-x',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/tabs' => $vendorDir . '/kartik-v/yii2-tabs-x',
    ),
  ),
  'moonlandsoft/yii2-phpexcel' => 
  array (
    'name' => 'moonlandsoft/yii2-phpexcel',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@moonland/phpexcel' => $vendorDir . '/moonlandsoft/yii2-phpexcel',
    ),
  ),
  '2amigos/yii2-gallery-widget' => 
  array (
    'name' => '2amigos/yii2-gallery-widget',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@dosamigos/gallery' => $vendorDir . '/2amigos/yii2-gallery-widget/src',
    ),
  ),
  '2amigos/yii2-file-upload-widget' => 
  array (
    'name' => '2amigos/yii2-file-upload-widget',
    'version' => '1.0.6.0',
    'alias' => 
    array (
      '@dosamigos/fileupload' => $vendorDir . '/2amigos/yii2-file-upload-widget/src',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform',
    ),
  ),
  'barcode/yii2-barcode' => 
  array (
    'name' => 'barcode/yii2-barcode',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@barcode/barcode' => $vendorDir . '/barcode/yii2-barcode',
    ),
  ),
  'kartik-v/yii2-checkbox-x' => 
  array (
    'name' => 'kartik-v/yii2-checkbox-x',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/checkbox' => $vendorDir . '/kartik-v/yii2-checkbox-x',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.1.3.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid',
    ),
  ),
  'kartik-v/yii2-export' => 
  array (
    'name' => 'kartik-v/yii2-export',
    'version' => '1.2.5.0',
    'alias' => 
    array (
      '@kartik/export' => $vendorDir . '/kartik-v/yii2-export',
    ),
  ),
  '2amigos/yii2-ckeditor-widget' => 
  array (
    'name' => '2amigos/yii2-ckeditor-widget',
    'version' => '2.0.1.0',
    'alias' => 
    array (
      '@dosamigos/ckeditor' => $vendorDir . '/2amigos/yii2-ckeditor-widget/src',
    ),
  ),
  'kartik-v/yii2-field-range' => 
  array (
    'name' => 'kartik-v/yii2-field-range',
    'version' => '1.3.1.0',
    'alias' => 
    array (
      '@kartik/field' => $vendorDir . '/kartik-v/yii2-field-range',
    ),
  ),
  'kartik-v/yii2-helpers' => 
  array (
    'name' => 'kartik-v/yii2-helpers',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/helpers' => $vendorDir . '/kartik-v/yii2-helpers',
    ),
  ),
  'kartik-v/yii2-widget-datetimepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datetimepicker',
    'version' => '1.4.2.0',
    'alias' => 
    array (
      '@kartik/datetime' => $vendorDir . '/kartik-v/yii2-widget-datetimepicker',
    ),
  ),
  'kartik-v/yii2-widget-timepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-timepicker',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/time' => $vendorDir . '/kartik-v/yii2-widget-timepicker',
    ),
  ),
  'mdmsoft/yii2-admin' => 
  array (
    'name' => 'mdmsoft/yii2-admin',
    'version' => '2.7.0.0',
    'alias' => 
    array (
      '@mdm/admin' => $vendorDir . '/mdmsoft/yii2-admin',
    ),
  ),
  'miloschuman/yii2-highcharts-widget' => 
  array (
    'name' => 'miloschuman/yii2-highcharts-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@miloschuman/highcharts' => $vendorDir . '/miloschuman/yii2-highcharts-widget/src',
    ),
  ),
  'kartik-v/yii2-widget-typeahead' => 
  array (
    'name' => 'kartik-v/yii2-widget-typeahead',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/typeahead' => $vendorDir . '/kartik-v/yii2-widget-typeahead',
    ),
  ),
  'uran1980/yii2-pace-widget' => 
  array (
    'name' => 'uran1980/yii2-pace-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@uran1980/yii/widgets/pace' => $vendorDir . '/uran1980/yii2-pace-widget',
    ),
  ),
  'kartik-v/yii2-detail-view' => 
  array (
    'name' => 'kartik-v/yii2-detail-view',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/detail' => $vendorDir . '/kartik-v/yii2-detail-view',
    ),
  ),
  'kartik-v/yii2-builder' => 
  array (
    'name' => 'kartik-v/yii2-builder',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/builder' => $vendorDir . '/kartik-v/yii2-builder',
    ),
  ),
  'mootensai/yii2-relation-trait' => 
  array (
    'name' => 'mootensai/yii2-relation-trait',
    'version' => '1.1.8.0',
    'alias' => 
    array (
      '@mootensai/relation' => $vendorDir . '/mootensai/yii2-relation-trait',
    ),
  ),
  'kartik-v/yii2-widget-touchspin' => 
  array (
    'name' => 'kartik-v/yii2-widget-touchspin',
    'version' => '1.2.1.0',
    'alias' => 
    array (
      '@kartik/touchspin' => $vendorDir . '/kartik-v/yii2-widget-touchspin',
    ),
  ),
  'kartik-v/yii2-widget-switchinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-switchinput',
    'version' => '1.3.1.0',
    'alias' => 
    array (
      '@kartik/switchinput' => $vendorDir . '/kartik-v/yii2-widget-switchinput',
    ),
  ),
  'kartik-v/yii2-widget-spinner' => 
  array (
    'name' => 'kartik-v/yii2-widget-spinner',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/spinner' => $vendorDir . '/kartik-v/yii2-widget-spinner',
    ),
  ),
  'kartik-v/yii2-widget-sidenav' => 
  array (
    'name' => 'kartik-v/yii2-widget-sidenav',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/sidenav' => $vendorDir . '/kartik-v/yii2-widget-sidenav',
    ),
  ),
  'kartik-v/yii2-widget-rating' => 
  array (
    'name' => 'kartik-v/yii2-widget-rating',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/rating' => $vendorDir . '/kartik-v/yii2-widget-rating',
    ),
  ),
  'kartik-v/yii2-widget-rangeinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-rangeinput',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/range' => $vendorDir . '/kartik-v/yii2-widget-rangeinput',
    ),
  ),
  'kartik-v/yii2-widget-growl' => 
  array (
    'name' => 'kartik-v/yii2-widget-growl',
    'version' => '1.1.1.0',
    'alias' => 
    array (
      '@kartik/growl' => $vendorDir . '/kartik-v/yii2-widget-growl',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '1.0.6.0',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput',
    ),
  ),
  'kartik-v/yii2-widget-colorinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-colorinput',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/color' => $vendorDir . '/kartik-v/yii2-widget-colorinput',
    ),
  ),
  'kartik-v/yii2-widget-alert' => 
  array (
    'name' => 'kartik-v/yii2-widget-alert',
    'version' => '1.1.1.0',
    'alias' => 
    array (
      '@kartik/alert' => $vendorDir . '/kartik-v/yii2-widget-alert',
    ),
  ),
  'kartik-v/yii2-widget-affix' => 
  array (
    'name' => 'kartik-v/yii2-widget-affix',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/affix' => $vendorDir . '/kartik-v/yii2-widget-affix',
    ),
  ),
  'kartik-v/yii2-widgets' => 
  array (
    'name' => 'kartik-v/yii2-widgets',
    'version' => '3.4.0.0',
    'alias' => 
    array (
      '@kartik/widgets' => $vendorDir . '/kartik-v/yii2-widgets',
    ),
  ),
  'mootensai/yii2-optimistic-lock-validator' => 
  array (
    'name' => 'mootensai/yii2-optimistic-lock-validator',
    'version' => '0.0.1.0',
    'alias' => 
    array (
      '@mootensai/components' => $vendorDir . '/mootensai/yii2-optimistic-lock-validator',
    ),
  ),
  'mootensai/yii2-jsblock' => 
  array (
    'name' => 'mootensai/yii2-jsblock',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@mootensai/components' => $vendorDir . '/mootensai/yii2-jsblock',
    ),
  ),
  'mootensai/yii2-enhanced-gii' => 
  array (
    'name' => 'mootensai/yii2-enhanced-gii',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@mootensai/enhancedgii' => $vendorDir . '/mootensai/yii2-enhanced-gii',
    ),
    'bootstrap' => 'mootensai\\enhancedgii\\Bootstrap',
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  '2amigos/qrcode-library' => 
  array (
    'name' => '2amigos/qrcode-library',
    'version' => '1.1.2.0',
    'alias' => 
    array (
      '@Da/QrCode' => $vendorDir . '/2amigos/qrcode-library/src',
    ),
  ),
);
