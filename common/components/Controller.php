<?php //

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*ct Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Controller
 *
 * @author Mickidadi Kosiyanga
 * @email mickidadimsoka@gmail.com
 */

namespace common\components;
use Yii;
use yii\web\ForbiddenHttpException;

class Controller extends \yii\web\Controller {

    //put your code here
    public function beforeAction($action) {
        //Actions that dont require login
        
        $dontRequireLogin = array();
        array_push($dontRequireLogin, '/site/index');
        array_push($dontRequireLogin, '/site/login');
        array_push($dontRequireLogin, '/application/default/home-page');
        array_push($dontRequireLogin, '/application/default/loan-apply');
        array_push($dontRequireLogin, '/application/default/necta');
        array_push($dontRequireLogin, '/application/default/nectain');
        
        array_push($dontRequireLogin, '/application/default/guideline-swahili');
        array_push($dontRequireLogin, '/application/default/guideline-english');
        array_push($dontRequireLogin, '/application/default/payment-status');
             $mustLogin = array();
        array_push($mustLogin, '/site/change-password');
        array_push($mustLogin, '/app-backend/users/profile');
        array_push($mustLogin, '/app-backend/users/updatepic');
        array_push($mustLogin, '/app-backend/users/updateidentification');
        array_push($mustLogin, '/app-backend/users/changepassword');
     
        $action_id = \Yii::$app->controller->action->id;
        $controller_id = \Yii::$app->controller->id;
        $module_id = Yii::$app->controller->module->id;
        $uaction = "/{$module_id}/{$controller_id}/{$action_id}";
    
        $uaction_btrimmed1 =str_replace('/app-backend', '', $uaction);
        $uaction_btrimmed =str_replace('/app-frontend', '', $uaction_btrimmed1);
    
        //Actions accessible by any user, but must login first 
          if (\Yii::$app->user->isGuest&& !in_array($uaction, $dontRequireLogin)) {
                          $this->redirect(['/site/index']);  
           //exit();
           }else{
   
        //A user must first login
        
        if (\Yii::$app->user->isGuest && !in_array($uaction, $dontRequireLogin)) {
         $this->redirect(['/site/index']);   
        }
     //checking if user is allowed to access this action
        if (!Yii::$app->user->can($uaction_btrimmed) && !in_array($uaction, $dontRequireLogin)) {
        //  echo $uaction_btrimmed;
            if (\Yii::$app->user->isGuest) {
               $this->redirect(['/site/index']);  
         //  exit();
           }
           else{
         if(!$action instanceof \yii\web\ErrorAction) {
           throw new ForbiddenHttpException("Sorry, you are not allowed to perform this action!");
              }
           }
        }
          //checking if user is logged in
       if (in_array($uaction, $mustLogin)) {
            if (\Yii::$app->user->isGuest) {
              $this->redirect(['/site/index']);
            } 
        }
   return parent::beforeAction($action);
           }
    }

}
