<?php

namespace common\models;

use Yii;
use frontend\modules\application\models\Applicant;
use frontend\modules\application\models\Application;
use frontend\modules\repayment\models\Employer;
//use frontend\modules\application\models\User;
use \common\models\User;
/**
 * This is the model class for table "loan_beneficiary".
 *
 * @property integer $loan_beneficiary_id
 * @property string $firstname
 * @property string $middlename
 * @property string $surname
 * @property string $f4indexno
 * @property string $NID
 * @property string $date_of_birth
 * @property integer $place_of_birth
 * @property integer $learning_institution_id
 * @property string $postal_address
 * @property string $physical_address
 * @property string $phone_number
 * @property string $email_address
 * @property string $password
 *
 * @property Ward $placeOfBirth
 * @property LearningInstitution $learningInstitution
 */
class LoanBeneficiary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loan_beneficiary';
    }

    /**
     * @inheritdoc
     */
    public $confirm_password;
    public $region;
	public $district;
	public $ward_id;
    public function rules()
    {
        return [
            [['firstname', 'middlename', 'surname', 'date_of_birth', 'place_of_birth', 'learning_institution_id', 'physical_address', 'phone_number', 'email_address', 'password','district','confirm_password','sex','region'], 'required', 'on' => 'loanee_registration'],
            ['password', 'string', 'length' => [8, 24]],
            [['date_of_birth','created_at','updated_at','updated_by','sex','region','applicant_id','NID','ward_id'], 'safe'],
            [['place_of_birth', 'learning_institution_id', 'phone_number','applicant_id'], 'integer'],
            [['firstname', 'middlename', 'surname', 'f4indexno'], 'string', 'max' => 45],
            [['NID', 'postal_address'], 'string', 'max' => 30],
            [['physical_address', 'email_address'], 'string', 'max' => 100],
            [['phone_number'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 255],
            //[['email_address'], 'unique','message'=>'Email Address Exist'],
            ['email_address', 'email'],
            ['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords must be retyped exactly", 'on' => 'loanee_registration' ],
            [['place_of_birth'], 'exist', 'skipOnError' => true, 'targetClass' => \backend\modules\application\models\Ward::className(), 'targetAttribute' => ['place_of_birth' => 'ward_id']],
            [['learning_institution_id'], 'exist', 'skipOnError' => true, 'targetClass' => \backend\modules\application\models\LearningInstitution::className(), 'targetAttribute' => ['learning_institution_id' => 'learning_institution_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\User::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
			[['applicant_id'], 'exist', 'skipOnError' => true, 'targetClass' => \frontend\modules\application\models\Applicant::className(), 'targetAttribute' => ['applicant_id' => 'applicant_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'loan_beneficiary_id' => 'Loan Beneficiary ID',
            'firstname' => 'First Name',
            'middlename' => 'Middle Name',
            'surname' => 'Last Name',
            'f4indexno' => 'Form IV Index No.',
            'NID' => 'National Identification No.',
            'date_of_birth' => 'Date Of Birth',
            'place_of_birth' => 'Place Of Birth(Ward)',
            'learning_institution_id' => 'Learning Institution',
            'postal_address' => 'Postal Address',
            'physical_address' => 'Physical Address',
            'phone_number' => 'Phone Number',
            'email_address' => 'Email Address',
            'password' => 'Password',
            'confirm_password'=>'Confirm Password',
            'district'=>'Place of Birth(District)', 
            'created_at'=>'Created at',
            'updated_at'=>'Updated at',
            'updated_by'=>'Updated by',
			//'place_of_birth'=>'Ward',
			'sex'=>'Sex',
			'region'=>'Region',
			'applicant_id'=>'applicant_id',
			'ward_id'=>'Ward',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	 public function getApplicant() {
        return $this->hasOne(\frontend\modules\application\models\Applicant::className(), ['applicant_id' => 'applicant_id']);
    }
	 /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaceOfBirth()
    {
        return $this->hasOne(\backend\modules\application\models\Ward::className(), ['ward_id' => 'place_of_birth']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLearningInstitution()
    {
        return $this->hasOne(\backend\modules\application\models\LearningInstitution::className(), ['learning_institution_id' => 'learning_institution_id']);
    }
    public function getUpdatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['user_id' => 'updated_by']);
    }
    public function getUser($applicantID){
        $details_applicant = Applicant::findBySql("SELECT a.user_id AS user_id,b.username AS username FROM applicant a INNER JOIN user b ON a.user_id=b.user_id  WHERE  a.applicant_id='$applicantID' ORDER BY a.applicant_id DESC")->one();
        $user_id=$details_applicant->user_id;        
        $value = (count($user_id) == 0) ? '0' : $details_applicant;
        return $value;
        }
    public function updateUserBasicInfo($username,$password,$auth_key,$user_id){
        User::updateAll(['username' =>$username,'password_hash' =>$password,'auth_key' =>$auth_key,'status'=>'0' ], 'user_id ="'.$user_id.'"');
 } 
    public function getApplicantDetails($applcantF4IndexNo, $NIN) {
        if ($applcantF4IndexNo != '' && $NIN != '') {

            $details1 = \frontend\modules\application\models\Applicant::findBySql("SELECT * FROM applicant  WHERE  f4indexno='$applcantF4IndexNo' AND NID='$NIN' ORDER BY applicant_id DESC")->one();
            $applicantResult = $details1->applicant_id;

            $value1 = (count($applicantResult) == 0) ? '0' : '1';
            if ($value1 == 0) {
                $details2 = \frontend\modules\application\models\Applicant::findBySql("SELECT * FROM applicant  WHERE f4indexno='$applcantF4IndexNo' ORDER BY applicant_id DESC")->one();
                $applicantResult2 = $details2->applicant_id;
                $value2 = (count($applicantResult2) == 0) ? '0' : '1';
                if ($value2 == 0) {
                    $details3 = \frontend\modules\application\models\Applicant::findBySql("SELECT * FROM applicant  WHERE NID='$NIN' ORDER BY applicant_id DESC")->one();
                    $applicantResult3 = $details3->applicant_id;
                    $value3 = (count($applicantResult3) == 0) ? '0' : '1';
                    if ($value3 == 1) {
                        $resultsFound = $applicantResult3;
                    } else {
                        $resultsFound = $applicantResult3;
                    }
                } else {
                    $resultsFound = $details2;
                }
            } else {
                $resultsFound = $details1;
            }
        } else if ($applcantF4IndexNo == '' && $NIN != '') {
            $details_4 = \frontend\modules\application\models\Applicant::find()
                            ->where(['NID' => $NIN])
                            ->orderBy('applicant_id DESC')
                            ->limit(1)->one();
            $resultsFound = $details_4;
        } else if ($applcantF4IndexNo != '' && $NIN == '') {
            $details_5 = \frontend\modules\application\models\Applicant::find()
                            ->where(['f4indexno' => $applcantF4IndexNo])
                            ->orderBy('applicant_id DESC')
                            ->limit(1)->one();
            $resultsFound = $details_5;
        } else if ($applcantF4IndexNo == '' && $NIN == '') {
            $resultsFound = "";
        }
        return $resultsFound;
    } 
	public function getApplicantDetailsUsingNonUniqueIdentifiers($f4indexno, $dateofbirth, $placeofbirth, $learningInstitution) {
        $details_applicant = Applicant::findBySql("SELECT * FROM applicant INNER JOIN user ON user.user_id=applicant.user_id INNER JOIN application ON application.applicant_id=applicant.applicant_id INNER JOIN programme ON programme.programme_id=application.programme_id INNER JOIN ward ON ward.ward_id=applicant.place_of_birth "
                        . "WHERE  applicant.date_of_birth='$dateofbirth' AND applicant.f4indexno='$f4indexno' AND ward.ward_id='$placeofbirth' AND programme.learning_institution_id ='$learningInstitution' ORDER BY applicant.applicant_id DESC")->one();
		if(count($details_applicant)>0){
		$applicant_idR = $details_applicant->applicant_id;
		}else{
		$applicant_idR=0;
		}
        return $applicant_idR;
    }
	public function getApplicantDetailsUsingApplicantID($id) {
        $details_applicant = Applicant::findBySql("SELECT * FROM applicant INNER JOIN user ON user.user_id=applicant.user_id INNER JOIN application ON application.applicant_id=applicant.applicant_id INNER JOIN programme ON programme.programme_id=application.programme_id INNER JOIN ward ON ward.ward_id=applicant.place_of_birth "
                        . "WHERE  applicant.applicant_id='$id' ORDER BY applicant.applicant_id DESC")->one();
		if(count($details_applicant)>0){
		$applicant_idR = $details_applicant;
		}else{
		$applicant_idR=0;
		}
        return $applicant_idR;
    }
	public function getApplicantLearningInstitution($applicantID) {
        $details_application = Application::findBySql("SELECT * FROM application INNER JOIN  programme ON programme.programme_id=application.programme_id  "
                        . "WHERE application.applicant_id='$applicantID' ORDER BY application.applicant_id DESC")->one();
		if(count($details_application)>0){
		$institutionDetail = $details_application;
		}else{
		$institutionDetail=0;
		}
        return $institutionDetail;
    }
	public function getUserIDFromEmployer($employerID){
      $results=Employer::find()->where(['employer_id'=>$employerID])->one();
	  if(count($results) > 0){
	  $userID=$results;
	  }else{
	  $userID=0;
	  }
	return $userID;	
 }
	public function updateUserVerifyEmail($user_id){
        User::updateAll(['email_verified' =>'1'], 'user_id ="'.$user_id.'"');
 }
 public function updateUserActivateAccount($user_id){
        User::updateAll(['status' =>10,'activation_email_sent' =>1], 'user_id ="'.$user_id.'"');
 }
 public function getUserDetailsFromUserID($userID){
      $results_user=User::find()->where(['user_id'=>$userID])->one();
	  if(count($results_user) > 0){
	  $userID=$results_user;
	  }else{
	  $userID=0;
	  }
	return $userID;	
 }
 
 public function getUserDetailsGeneral($username){
      $userDetails = User::findBySql("SELECT * FROM user "
                        . "WHERE user.email_address='$username' OR user.username='$username'")->one();
		if(count($userDetails)>0){
		$results = $userDetails;
		}else{
		$results=0;
		}		
		if($results==0){
		
		$userDetailsApplicant = Applicant::findBySql("SELECT * FROM applicant "
                        . "WHERE f4indexno='$username'")->one();
		if(count($userDetailsApplicant)>0){
		$results_userID = $userDetailsApplicant->user_id;
		$userDetailsApplicant2 = User::findBySql("SELECT * FROM user "
                        . "WHERE user_id='$results_userID'")->one();
						if(count($userDetailsApplicant2)>0){
						$results_final=$userDetailsApplicant2;
						}else{
						$results_final=0;
						}
		}else{
		$results_final=0;
		}
		return $results_final;
		}else{
		return $results;
		}
		
		
 }
}
