<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "form_storage".
 *
 * @property integer $id
 * @property string $folder_number
 * @property integer $folder_limit
 * @property string $date_created
 * @property integer $created_by
 * @property string $last_retrieval
 */
class FormStorage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_storage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['folder_number', 'folder_limit'], 'required'],
            [['folder_limit', 'created_by'], 'integer'],
            [['date_created', 'last_retrieval'], 'safe'],
            [['folder_number'], 'string', 'max' => 100],
            [['folder_number'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'folder_number' => 'Folder Number',
            'folder_limit' => 'Folder Limit',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'last_retrieval' => 'Last Retrieval',
        ];
    }
}
