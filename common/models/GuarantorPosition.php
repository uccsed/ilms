<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "guarantor_position".
 *
 * @property integer $guarantor_position_id
 * @property string $guarantor_position
 * @property integer $status
 */
class GuarantorPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guarantor_position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['guarantor_position', 'status'], 'required'],
            [['status'], 'integer'],
            [['guarantor_position'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'guarantor_position_id' => 'Guarantor Position ID',
            'guarantor_position' => 'Guarantor Position',
            'status' => 'Status',
        ];
    }
}
