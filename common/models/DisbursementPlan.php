<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "disbursement_default_plan".
 *
 * @property integer $plan_id
 * @property integer $academic_year
 * @property integer $year_of_study
 * @property integer $programme_id
 * @property integer $instalment_id
 * @property integer $loan_item_id
 * @property integer $semester_number
 * @property double $disbursement_percent
 * @property string $sync_id
 * @property string $date_created
 * @property integer $created_by
 */
class DisbursementDefaultPlan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disbursement_default_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['academic_year', 'year_of_study', 'programme_id', 'instalment_id', 'loan_item_id', 'semester_number', 'disbursement_percent', 'sync_id'], 'required'],
            [['academic_year', 'year_of_study', 'programme_id', 'instalment_id', 'loan_item_id', 'semester_number', 'created_by'], 'integer'],
            [['disbursement_percent'], 'number'],
            [['date_created'], 'safe'],
            [['sync_id'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => 'Plan ID',
            'academic_year' => 'Academic Year',
            'year_of_study' => 'Year Of Study',
            'programme_id' => 'Programme ID',
            'instalment_id' => 'Instalment ID',
            'loan_item_id' => 'Loan Item ID',
            'semester_number' => 'Semester Number',
            'disbursement_percent' => 'Disbursement Percent',
            'sync_id' => 'Sync ID',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        ];
    }
}
