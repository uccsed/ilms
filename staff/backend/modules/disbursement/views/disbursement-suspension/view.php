<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementSuspension */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Suspensions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-suspension-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'application_id',
            'loan_item_id',
            'suspension_percent',
            'remaining_percent',
            'status',
            'status_reason',
            'remarks:ntext',
            'status_date',
            'status_by',
            'supporting_document:ntext',
            'suspension_history:ntext',
            'sync_id',
        ],
    ]) ?>

</div>
