<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementSuspension */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disbursement-suspension-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'application_id')->textInput() ?>

    <?= $form->field($model, 'loan_item_id')->textInput() ?>

    <?= $form->field($model, 'suspension_percent')->textInput() ?>

    <?= $form->field($model, 'remaining_percent')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'status_reason')->textInput() ?>

    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status_date')->textInput() ?>

    <?= $form->field($model, 'status_by')->textInput() ?>

    <?= $form->field($model, 'supporting_document')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'suspension_history')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sync_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
