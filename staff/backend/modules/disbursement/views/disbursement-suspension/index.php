<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\DisbursementSuspensionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disbursement Suspensions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-suspension-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Disbursement Suspension', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'application_id',
            'loan_item_id',
            'suspension_percent',
            'remaining_percent',
            // 'status',
            // 'status_reason',
            // 'remarks:ntext',
            // 'status_date',
            // 'status_by',
            // 'supporting_document:ntext',
            // 'suspension_history:ntext',
            // 'sync_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
