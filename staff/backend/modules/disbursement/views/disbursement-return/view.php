<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementReturn */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Returns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-return-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'index_number',
            'application_id',
            'header_id',
            'disbursement_batch_id',
            'sex',
            'year_of_study',
            'pay_sheet_serial_number',
            'loan_item',
            'instalment_number',
            'amount',
            'remarks:ntext',
            'academic_year',
            'status',
            'reasons:ntext',
            'created_by',
            'created_on',
        ],
    ]) ?>

</div>
