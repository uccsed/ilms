<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementDepositStaging */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disbursement-deposit-staging-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'index_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'student_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sex')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'year_of_study')->textInput() ?>

    <?= $form->field($model, 'pay_sheet_serial_number')->textInput() ?>

    <?= $form->field($model, 'loan_item')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'instalment_number')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'academic_year')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'matching_remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_on')->textInput() ?>

    <?= $form->field($model, 'header_id')->textInput() ?>

    <?= $form->field($model, 'sync_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'disbursement_id')->textInput() ?>

    <?= $form->field($model, 'disbursement_cheque_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deposit_header_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
