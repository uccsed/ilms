<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementDepositStaging */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Deposit Stagings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-deposit-staging-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'index_number',
            'student_name',
            'sex',
            'year_of_study',
            'pay_sheet_serial_number',
            'loan_item',
            'instalment_number',
            'amount',
            'academic_year',
            'status',
            'matching_remarks:ntext',
            'created_by',
            'created_on',
            'header_id',
            'sync_id',
            'disbursement_id',
            'disbursement_cheque_number',
            'deposit_header_id',
        ],
    ]) ?>

</div>
