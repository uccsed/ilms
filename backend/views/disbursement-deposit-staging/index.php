<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\DisbursementDepositStagingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disbursement Deposit Stagings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-deposit-staging-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Disbursement Deposit Staging', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'index_number',
            'student_name',
            'sex',
            'year_of_study',
            // 'pay_sheet_serial_number',
            // 'loan_item',
            // 'instalment_number',
            // 'amount',
            // 'academic_year',
            // 'status',
            // 'matching_remarks:ntext',
            // 'created_by',
            // 'created_on',
            // 'header_id',
            // 'sync_id',
            // 'disbursement_id',
            // 'disbursement_cheque_number',
            // 'deposit_header_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
