<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\SuspensionStaging */

$this->title = 'Create Suspension Staging';
$this->params['breadcrumbs'][] = ['label' => 'Suspension Stagings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suspension-staging-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
