<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\SuspensionBatch */

$this->title = 'Create Suspension Batch';
$this->params['breadcrumbs'][] = ['label' => 'Suspension Batches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suspension-batch-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
