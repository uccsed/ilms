<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\SuspensionBatch */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Suspension Batches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suspension-batch-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description:ntext',
            'files_uploaded:ntext',
            'status',
            'status_date',
            'created_by',
            'created_at',
            'updated_by',
            'updated_at',
            'type',
        ],
    ]) ?>

</div>
