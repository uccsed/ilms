<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\SuspensionBatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Suspension Batches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suspension-batch-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Suspension Batch', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description:ntext',
            'files_uploaded:ntext',
            'status',
            // 'status_date',
            // 'created_by',
            // 'created_at',
            // 'updated_by',
            // 'updated_at',
            // 'type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
