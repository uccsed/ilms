<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/21/18
 * Time: 3:33 PM
 */

ini_set('memory_limit', '-1');
set_time_limit(0);
/*use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use backend\modules\disbursement\Module;*/

//echo $this->render('report_header',['reportName'=>$reportName]);
?>

<table width="100%">
    <thead>
    <tr>
        <th style="text-align: left; ">S/N</th>
        <th style="text-align: left; ">CATEGORY</th>
        <th style="text-align: right; ">NUMBER OF BENEFICIARIES</th>
        <th style="text-align: right; ">AMOUNT</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $count=0;
    $rowNo=0;
    $GRAND1 = 0;
    $GRAND2 = 0;

    foreach($reportData as $row){
        $count++;
        $GRAND1 += $row['Beneficiaries'];
        $GRAND2 += $row['Amount'];
        ?>

        <tr>
            <td style="text-align: left; "><?php echo $count; ?></td>
            <td style="text-align: left; " ><?php echo $row['Category']; ?></td>
            <td style="text-align: right; " ><?php echo number_format($row['Beneficiaries'],0); ?></td>
            <td style="text-align: right; "><?php echo number_format($row['Amount'],2); ?></td>

        </tr>
        <?php

    }


    ?>
    </tbody>
    <tfoot>
    <tr>
        <td style="text-align: left; "></td>
        <th style="text-align: left; " >GRAND TOTAL</th>
        <th style="text-align: right; " ><?php echo number_format($GRAND1,0); ?></th>
        <th style="text-align: right; "><?php echo number_format($GRAND2,2); ?></th>

    </tr>
    </tfoot>
</table>

