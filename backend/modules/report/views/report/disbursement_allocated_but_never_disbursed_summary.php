<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/21/18
 * Time: 3:33 PM
 */

/*ini_set('memory_limit', '-1');
set_time_limit(0);*/
ini_set('max_execution_time','-1');

ini_set('memory_limit', '-1');
$o='background-color: #f7f7f7; border: 1px solid #aaa; padding-right: 10px;';
$i = 'border: 1px solid #aaa; padding-right: 10px;';
?>

<table width="100%" border="0" style="display: table; border:1px solid #aaa; " cellspacing="0" cellpadding="0">
    <thead>
    <tr>
        <th style="text-align: left; <?php echo $o; ?>">S/N</th>
        <th style="text-align: left; <?php echo $i; ?>">INSTITUTION</th>
        <th style="text-align: right; <?php echo $o; ?>">ALLOCATED STUDENTS</th>
        <th style="text-align: right; <?php echo $i; ?>">ALLOCATED AMOUNT</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $count=0;
    $rowNo=0;
    $GRAND1 = 0;
    $GRAND2 = 0;


    foreach($reportData as $row){
        $count++;
        $GRAND1 += $row['AllocatedStudents'];
        $GRAND2 += $row['AllocatedAmount'];
        ?>

        <tr>
            <td style="text-align: left; <?php echo $o; ?>"><?php echo $count; ?></td>
            <td style="text-align: left; <?php echo $i; ?>" ><?php echo $row['Institution']; ?></td>
            <td style="text-align: right; <?php echo $o; ?>" ><?php echo number_format($row['AllocatedStudents'],0); ?></td>
            <td style="text-align: right; <?php echo $i; ?>"><?php echo number_format($row['AllocatedAmount'],2); ?></td>

        </tr>
        <?php

    }


    ?>
    </tbody>
    <tfoot>
    <tr>
        <td style="text-align: left; <?php echo $o; ?>"></td>
        <th style="text-align: left; <?php echo $i; ?>" >GRAND TOTAL</th>
        <th style="text-align: right; <?php echo $o; ?>" ><?php echo number_format($GRAND1,0); ?></th>
        <th style="text-align: right; <?php echo $i; ?>"><?php echo number_format($GRAND2,2); ?></th>

    </tr>
    </tfoot>
</table>

