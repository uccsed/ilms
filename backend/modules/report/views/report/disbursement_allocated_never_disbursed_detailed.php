
<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/21/18
 * Time: 8:44 PM
 */

ini_set('memory_limit', '-1');
set_time_limit(0);
$o='background-color: #f7f7f7; border: 1px solid #aaa; padding-right: 10px;';
$i = 'border: 1px solid #aaa; padding-right: 10px;';
?>

<table width="100%" border="0" style="display: table; border:1px solid #aaa; " cellspacing="0" cellpadding="0">
    <thead>
    <tr style="font-weight:bold; border:1px solid #aaa; text-align: center;">
        <th style="text-align: left; <?php echo $o; ?>">S/N</th>
        <th style="text-align: left; <?php echo $i; ?>">INSTITUTION</th>
        <th style="text-align: left; <?php echo $o; ?>">INDEX</th>
        <th style="text-align: left; <?php echo $i; ?>">NAME</th>
        <th style="text-align: center; <?php echo $o; ?>">SEX</th>
        <th style="text-align: left; <?php echo $i; ?>">ITEM</th>
        <th style="text-align: right; <?php echo $o; ?>">ALLOCATED AMOUNT</th>
        <th style="text-align: right; <?php echo $i; ?>">DISBURSED AMOUNT</th>
        <th style="text-align: right; <?php echo $o; ?>">UNDISBURSED AMOUNT</th>

    </tr>
    </thead>
    <tbody>
    <?php
    $count=0;
    $rowNo=0;
    $GRAND1 = 0;
    $GRAND2 = 0;
    $GRAND3 = 0;


    foreach($reportData as $row){
        $count++;
        $GRAND1 += $row['AllocatedAmount'];
        $GRAND2 += $row['DisbursedAmount'];
        $GRAND3 += $row['UnDisbursedAmount'];
        ?>

        <tr style="font-weight:bold; border:1px solid #aaa;">
            <td style="text-align: left; <?php echo $o; ?>"><?php echo $count; ?></td>
            <td style="text-align: left; <?php echo $i; ?>" ><?php echo $row['Institution']; ?></td>
            <td style="text-align: left; <?php echo $o; ?>" ><?php echo $row['Index']; ?></td>
            <td style="text-align: left; <?php echo $i; ?>" ><?php echo $row['Name']; ?></td>
            <td style="text-align: left; <?php echo $o; ?>" ><?php echo $row['Sex']; ?></td>
            <td style="text-align: center; <?php echo $i; ?>" ><?php echo $row['Item']; ?></td>
            <td style="text-align: right; <?php echo $o; ?>"><?php echo number_format($row['AllocatedAmount'],2); ?></td>
            <td style="text-align: right; <?php echo $i; ?>"><?php echo number_format($row['DisbursedAmount'],2); ?></td>
            <td style="text-align: right; <?php echo $o; ?>"><?php echo number_format($row['UnDisbursedAmount'],2); ?></td>
        </tr>
        <?php

    }


    ?>
    </tbody>
    <tfoot>
    <tr style="border: 1px solid #aaa;">
        <td style="text-align: left; <?php echo $o; ?>"></td>
        <th colspan="5" style="text-align: left; <?php echo $i; ?>" >GRAND TOTAL</th>
        <th style="text-align: right; <?php echo $o; ?>" ><?php echo number_format($GRAND1,2); ?></th>
        <th style="text-align: right; <?php echo $i; ?>" ><?php echo number_format($GRAND2,2); ?></th>
        <th style="text-align: right; <?php echo $o; ?>" ><?php echo number_format($GRAND3,2); ?></th>

    </tr>
    </tfoot>
</table>





