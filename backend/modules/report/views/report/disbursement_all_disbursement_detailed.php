<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/21/18
 * Time: 8:44 PM
 */

ini_set('memory_limit', '-1');
set_time_limit(0);
$o='background-color: #f7f7f7; border: 1px solid #aaa; padding-right: 10px;';
$i = 'border: 1px solid #aaa; padding-right: 10px;';
?>

<table width="100%" border="0" style="display: table; border:1px solid #aaa; " cellspacing="0" cellpadding="0">
    <thead>
    <tr style="font-weight:bold; border:1px solid #aaa; text-align: center;">
        <th style="text-align: left; <?php echo $o; ?>">S/N</th>
        <th style="text-align: left; <?php echo $i; ?>">INSTITUTION</th>
        <th style="text-align: left; <?php echo $o; ?>">HID</th>
        <th style="text-align: left; <?php echo $i; ?>">INDEX</th>
        <th style="text-align: left; <?php echo $o; ?>">NAME</th>
        <th style="text-align: center; <?php echo $i; ?>">SEX</th>
        <th style="text-align: right; <?php echo $o; ?>">YOS</th>
        <th style="text-align: left; <?php echo $i; ?>">ITEM</th>
        <th style="text-align: right; <?php echo $o; ?>">INSTALMENT</th>
        <th style="text-align: right; <?php echo $i; ?>">AMOUNT</th>
        <th style="text-align: left; <?php echo $o; ?>">ACADEMIC YEAR</th>
        <th style="text-align: left; <?php echo $i; ?>">FINANCIAL YEAR</th>
        <th style="text-align: left; <?php echo $o; ?>">DATE</th>
        <th style="text-align: left; <?php echo $i; ?>">CHEQUE</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $count=0;
    $rowNo=0;
    $GRAND = 0;


    foreach($reportData as $row){
        $count++;
        $GRAND += $row['Amount'];
        ?>

        <tr style="font-weight:bold; border:1px solid #aaa;">
            <td style="text-align: left; <?php echo $o; ?>"><?php echo $count; ?></td>
            <td style="text-align: left; <?php echo $i; ?>" ><?php echo $row['Institution']; ?></td>
            <td style="text-align: left; <?php echo $o; ?>" ><?php echo $row['HID']; ?></td>
            <td style="text-align: left; <?php echo $i; ?>" ><?php echo $row['Index']; ?></td>
            <td style="text-align: left; <?php echo $o; ?>" ><?php echo $row['Name']; ?></td>
            <td style="text-align: center; <?php echo $i; ?>" ><?php echo $row['Sex']; ?></td>
            <td style="text-align: left; <?php echo $o; ?>" ><?php echo $row['YOS']; ?></td>
            <td style="text-align: left; <?php echo $i; ?>" ><?php echo $row['Item']; ?></td>
            <td style="text-align: right; <?php echo $o; ?>" ><?php echo $row['Instalment']; ?></td>
            <td style="text-align: right; <?php echo $i; ?>"><?php echo number_format($row['Amount'],2); ?></td>
            <td style="text-align: left; <?php echo $o; ?>" ><?php echo $row['AcademicYear']; ?></td>
            <td style="text-align: left; <?php echo $i; ?>" ><?php echo $row['FinancialYear']; ?></td>
            <td style="text-align: left; <?php echo $o; ?>" ><?php echo $row['Date']; ?></td>
            <td style="text-align: left; <?php echo $i; ?>" ><?php echo $row['Cheque']; ?></td>

        </tr>
        <?php

    }


    ?>
    </tbody>
    <tfoot>
    <tr style="border: 1px solid #aaa;">
        <td style="text-align: left; <?php echo $o; ?>"></td>
        <th colspan="8" style="text-align: left; <?php echo $o; ?>" >GRAND TOTAL</th>
        <th style="text-align: right; <?php echo $i; ?>" ><?php echo number_format($GRAND,2); ?></th>
        <th colspan="4" style="text-align: right; <?php echo $o; ?>"></th>
    </tr>
    </tfoot>
</table>


