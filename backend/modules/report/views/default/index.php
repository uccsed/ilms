<?php
use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use backend\modules\application\models\Application;
use backend\modules\application\models\Applicant;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\application\models\BankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/*
$user_id = Yii::$app->user->identity->id;
$modelUser = common\models\User::findone($user_id);
$username=strtoupper($modelUser->firstname).' '.strtoupper($modelUser->middlename).' '.strtoupper($modelUser->surname);

$applicants_who_paid_fee = Application::find()->where(['<>','payment_status',0])->count();
$applicants_not_paid_fee = Application::find()->where(['payment_status' => 0])->count();
$total_fee_collected = ($applicants_who_paid_fee -1) * 30000 + 1500;

$female_applicants = Applicant::find()->where(['sex' => 'F'])->count();
$male_applicants = Applicant::find()->andWhere(['or',
                                        ['sex'=>'M'],    
                                        ['sex'=>NULL]])
                                            ->count();

$submitted_applications = Application::find()->where(['loan_application_form_status' => [3]])->count();
//$submitted_applications = Application::find()->where(['loan_application_form_status' => 2])->count();
//$complete_applications = Application::find()->where(['loan_application_form_status' => 3])->count();
//$incomplete_applications = Application::find()->where(['verification_status' => 2])->count();
$incomplete_applications = Application::find()
                                        ->andWhere(['or',
                                        ['loan_application_form_status'=>0],
                                        ['loan_application_form_status'=>1],
                                        ['loan_application_form_status'=>2],    
                                        ['loan_application_form_status'=>NULL],
                                                    ])
                                        ->count();

$diploma_applications = Application::find()->where(['applicant_category_id' => 3])->count();
$undergraduate_applications = Application::find()->where(['applicant_category_id' => 1])->count();
$postgraduate_applications = Application::find()->where(['applicant_category_id' => [2,4,5]])->count();
$all_applications = Application::find()->count();
*/
$this->title ="Reports Dashboard";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="application-view">
   <div class="panel panel-info">
       <div class="panel-heading"><?= $this->title; ?></div>
       <div class="panel-body"></div>
    </div>
</div>