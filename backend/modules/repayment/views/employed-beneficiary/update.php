<?php

use yii\helpers\Html;
?>
<div class="employed-beneficiary-update">
<div class="panel panel-info">
        <div class="panel-heading">
<?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
    </div>
</div>
