<?php
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;

$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]);
 ?>
 <?php
echo Form::widget([ // fields with labels
    'model'=>$model,
    'form'=>$form,
    'columns'=>1,
    'attributes'=>[
         'employer_type'=>['label'=>'Employer Type', 'options'=>['placeholder'=>'Employer Type']], 
		 'is_active'=>[
            'type'=>Form::INPUT_RADIO_LIST, 
            'items'=>[1=>'Active', 0=>'Inactive'],
            'default'=>[1=>'Active'],			
            'label'=>Html::label('Status'), 
            'options'=>['inline'=>true]
        ],
    ]
]);
 
 ?>
  <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  
<?php
echo Html::resetButton('Reset', ['class'=>'btn btn-default']);

ActiveForm::end();
?>
    </div>
