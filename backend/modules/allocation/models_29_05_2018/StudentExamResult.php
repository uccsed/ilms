<?php

namespace backend\modules\allocation\models;

use Yii;
use \backend\modules\allocation\models\base\StudentExamResult as BaseStudentExamResult;

/**
 * This is the model class for table "student_exam_result".
 */
class StudentExamResult extends BaseStudentExamResult
{
    /**
     * @inheritdoc
     */
      public $file;
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            //[['registration_number','file','academic_year_id', 'study_year', 'exam_status_id', 'semester','is_last_semester'], 'required'],
            [['registration_number','academic_year_id', 'study_year', 'exam_status_id', 'semester','f4indexno','programme_id','is_last_semester'], 'required','on' => 'add_students_examination_results'],    
            [['academic_year_id', 'programme_id', 'exam_status_id', 'semester', 'confirmed', 'created_by', 'updated_by'], 'integer'],
            [['students_exam_results_file'], 'file', 'extensions' => 'xlsx, xls', 'skipOnEmpty' => true, 'on' => 'students_exam_results_upload'],
            [['students_exam_results_file','academic_year_id','semester','is_last_semester'], 'required', 'on' => 'students_exam_results_upload'],
            [['academic_year_id','semester','is_last_semester'], 'required', 'on' => 'students_exam_results_upload2'],    
            [['created_at','f4indexno', 'updated_at'], 'safe'],
            [['registration_number'], 'string', 'max' => 20],
            [['f4indexno'], 'string', 'max' => 45]
        ]);
    }
	
}
