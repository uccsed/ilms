<?php

namespace backend\modules\allocation\models;

use Yii;
use \backend\modules\allocation\models\base\AllocationBatch as BaseAllocationBatch;

/**
 * This is the model class for table "allocation_batch".
 */
class AllocationBatch extends BaseAllocationBatch
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['batch_number', 'academic_year_id', 'available_budget','contained_student'], 'required'],
            [['batch_number', 'academic_year_id', 'is_approved', 'created_by', 'updated_by', 'is_canceled', 'disburse_status'], 'integer'],
            [['available_budget'], 'number'],
            [['approval_comment', 'cancel_comment'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['batch_desc'], 'string', 'max' => 45],
            [['disburse_comment'], 'string', 'max' => 300]
        ]);
    }
	
}
