<?php

namespace backend\modules\allocation\models;

use Yii;
use \backend\modules\allocation\models\base\LoanItemPriority as BaseLoanItemPriority;

/**
 * This is the model class for table "loan_item_priority".
 */
class LoanItemPriority extends BaseLoanItemPriority {

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_replace_recursive(parent::rules(), [
            [['academic_year_id', 'loan_item_id', 'priority_order','loan_item_category','study_level'], 'required'],
//            [['loan_award_percentage'], 'required'],
            [['academic_year_id', 'loan_item_id', 'priority_order', 'created_by', 'updated_by','loan_award_percentage'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ]);
    }

}
