<?php

namespace backend\modules\allocation\models;

use Yii;

/**
 * This is the model class for table "allocation_plan_student".
 *
 * @property integer $allocation_plan_id
 * @property integer $application_id
 * @property double $needness_amount
 * @property double $allocated_amount
 * @property integer $study_year
 *
 * @property Application $application
 * @property AllocationPlan $allocationPlan
 * @property AllocationPlanStudentLoanItem[] $allocationPlanStudentLoanItems
 * @property AllocationPlanLoanItem[] $loanItems
 */
class AllocationPlanStudent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'allocation_plan_student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['allocation_plan_id', 'application_id', 'needness_amount', 'study_year'], 'required'],
            [['allocation_plan_id', 'application_id', 'study_year'], 'integer'],
            [['needness_amount', 'allocated_amount'], 'number'],
            [['application_id'], 'exist', 'skipOnError' => true, 'targetClass' => Application::className(), 'targetAttribute' => ['application_id' => 'application_id']],
            [['allocation_plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => AllocationPlan::className(), 'targetAttribute' => ['allocation_plan_id' => 'allocation_plan_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'allocation_plan_id' => 'Allocation Plan ID',
            'application_id' => 'Application ID',
            'needness_amount' => 'Needness Amount',
            'allocated_amount' => 'Allocated Amount',
            'study_year' => 'Study Year',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplication()
    {
        return $this->hasOne(Application::className(), ['application_id' => 'application_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationPlan()
    {
        return $this->hasOne(AllocationPlan::className(), ['allocation_plan_id' => 'allocation_plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationPlanStudentLoanItems()
    {
        return $this->hasMany(AllocationPlanStudentLoanItem::className(), ['allocation_plan_id' => 'allocation_plan_id', 'application_id' => 'application_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanItems()
    {
        return $this->hasMany(AllocationPlanLoanItem::className(), ['loan_item_id' => 'loan_item_id'])->viaTable('allocation_plan_student_loan_item', ['allocation_plan_id' => 'allocation_plan_id', 'application_id' => 'application_id']);
    }
}
