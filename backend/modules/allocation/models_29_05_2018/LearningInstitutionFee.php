<?php

namespace backend\modules\allocation\models;

use Yii;
use \backend\modules\allocation\models\base\LearningInstitutionFee as BaseLearningInstitutionFee;

/**
 * This is the model class for table "learning_institution_fee".
 */
class LearningInstitutionFee extends BaseLearningInstitutionFee
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['learning_institution_id', 'academic_year_id','study_level', 'fee_amount'], 'required'],
            [['learning_institution_id', 'academic_year_id', 'created_by', 'updated_by'], 'integer'],
            [['study_level', 'fee_amount'], 'number'],
            [['created_at', 'updated_at'], 'safe']
        ]);
    }
	
}
