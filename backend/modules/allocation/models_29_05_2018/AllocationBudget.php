<?php

namespace backend\modules\allocation\models;

use Yii;

/**
 * This is the model class for table "allocation_budget".
 *
 * @property integer $allocation_budget_id
 * @property double $budget_amount
 * @property integer $academic_year_id
 * @property string $applicant_category
 * @property integer $study_level
 * @property string $place_of_study
 * @property string $budget_scope
 * @property integer $is_active
 *
 * @property AcademicYear $academicYear
 * @property ApplicantCategory $studyLevel
 */
class AllocationBudget extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'allocation_budget';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['budget_amount', 'academic_year_id', 'applicant_category', 'study_level', 'place_of_study'], 'required'],
            [['budget_amount'], 'number'],
            [['academic_year_id', 'study_level', 'is_active'], 'integer'],
            [['applicant_category', 'place_of_study', 'budget_scope'], 'string'],
            [['academic_year_id'], 'exist', 'skipOnError' => true, 'targetClass' => AcademicYear::className(), 'targetAttribute' => ['academic_year_id' => 'academic_year_id']],
            [['study_level'], 'exist', 'skipOnError' => true, 'targetClass' => ApplicantCategory::className(), 'targetAttribute' => ['study_level' => 'applicant_category_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'allocation_budget_id' => 'Allocation Budget ID',
            'budget_amount' => 'Budget Amount',
            'academic_year_id' => 'Academic Year ID',
            'applicant_category' => 'Applicant Category',
            'study_level' => 'Study Level',
            'place_of_study' => 'Place Of Study',
            'budget_scope' => 'Budget Scope',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(AcademicYear::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudyLevel()
    {
        return $this->hasOne(ApplicantCategory::className(), ['applicant_category_id' => 'study_level']);
    }
}
