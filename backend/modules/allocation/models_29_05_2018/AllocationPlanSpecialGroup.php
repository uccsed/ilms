<?php

namespace backend\modules\allocation\models;

use Yii;

/**
 * This is the model class for table "allocation_framework_special_group".
 *
 * @property integer $special_group_id
 * @property integer $allocation_framework_id
 * @property string $group_name
 * @property string $applicant_source_table
 * @property string $applicant_souce_column
 * @property string $applicant_source_value
 * @property string $operator
 *
 * @property AllocationFramework $allocationFramework
 */
class AllocationPlanSpecialGroup extends \yii\db\ActiveRecord {

    const OPERATOR_EQUAL_TO = '=';
    const OPERATOR_NOT_EQUAL_TO = '!=';
    const OPERATOR_GREATER_THAN = '>';
    const OPERATOR_GREATER_THAN_OR_EQUAL = '>=';
    const OPERATOR_LESS_THAN = '<';
    const OPERATOR_LESS_THAN_OR_EQUAL = '=<';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'allocation_plan_special_group';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['allocation_plan_id', 'group_name', 'applicant_source_table', 'applicant_souce_column', 'applicant_source_value'], 'required'],
            [['group_name'], 'validateGroup'],
            [['allocation_plan_id'], 'integer'],
            [['group_name'], 'string', 'max' => 100],
            [['applicant_source_table', 'applicant_souce_column', 'applicant_source_value'], 'string', 'max' => 50],
            [['operator'], 'string', 'max' => 2],
            [['group_name'], 'unique'],
            [['allocation_framework_id'], 'exist', 'skipOnError' => true, 'targetClass' => AllocationFramework::className(), 'targetAttribute' => ['allocation_framework_id' => 'allocation_plan_framework_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'special_group_id' => 'Special Group ID',
            'allocation_framework_id' => 'Allocation Framework ID',
            'group_name' => 'Group Name',
            'applicant_source_table' => 'Applicant Source Table',
            'applicant_souce_column' => 'Applicant Souce Column',
            'applicant_source_value' => 'Applicant Source Value',
            'operator' => 'Operator',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationFramework() {
        return $this->hasOne(AllocationPLan::className(), ['allocation_plan_id' => 'allocation_plan_id']);
    }

    static function getAllocationSpecialGroupsByFrameworkId($allocation_plan_id) {
        return new \yii\data\ActiveDataProvider([
            'query' => self::find()->where(['allocation_plan_id' => $allocation_plan_id]),
        ]);
    }

    /*
     * alidates that the scenarios should not exist in the particular allocation framework
     * returns TRUE if not existing, FLASE if exists
     */

    function validateGroup($attribute) {
        if ($this->applicant_source_table && $this->applicant_souce_column && $this->applicant_source_value && $this->operator && $this->allocation_framework_id) {
            $exists = self::find()
                            ->where(
                                    [ 'allocation_framework_id' => $this->allocation_framework_id,
                                        'applicant_source_table' => $this->applicant_source_table,
                                        'applicant_souce_column' => $this->applicant_souce_column,
                                        'applicant_source_value' => $this->applicant_source_value,
                                        'operator' => $this->operator
                                    ]
                              )->exists();
            if ($exists) {
                $this->addError($attribute, 'Special Group Setting Already Exist in this Framework');
                return FALSE;
            }
        }
        return TRUE;
    }

    static function getOperators() {
        return [self::OPERATOR_EQUAL_TO => 'Equal To (=)',
            self::OPERATOR_NOT_EQUAL_TO => 'Not Equal To (!=)',
            self::OPERATOR_GREATER_THAN => 'Greater Than (<)',
            self::OPERATOR_GREATER_THAN_OR_EQUAL => 'Greater Than or Equal (>=)',
            self::OPERATOR_LESS_THAN => 'Less than (<)',
            self::OPERATOR_LESS_THAN_OR_EQUAL => 'Less than or Equal (<=)'
        ];
    }

}
