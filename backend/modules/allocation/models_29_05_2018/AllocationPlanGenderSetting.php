<?php

namespace backend\modules\allocation\models;

use Yii;

/**
 * This is the model class for table "allocation_plan_gender_setting".
 *
 * @property integer $allocation_plan_gender_setting_id
 * @property integer $allocation_plan_id
 * @property double $female_percentage
 * @property double $male_percentage
 *
 * @property AllocationPlan $allocationPlan
 */
class AllocationPlanGenderSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'allocation_plan_gender_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['allocation_plan_id', 'female_percentage', 'male_percentage'], 'required'],
            [['allocation_plan_id'], 'integer'],
            [['female_percentage', 'male_percentage'], 'number'],
            [['allocation_plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => AllocationPlan::className(), 'targetAttribute' => ['allocation_plan_id' => 'allocation_plan_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'allocation_plan_gender_setting_id' => 'Allocation Plan Gender Setting ID',
            'allocation_plan_id' => 'Allocation Plan ID',
            'female_percentage' => 'Female Percentage',
            'male_percentage' => 'Male Percentage',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationPlan()
    {
        return $this->hasOne(AllocationPlan::className(), ['allocation_plan_id' => 'allocation_plan_id']);
    }
}
