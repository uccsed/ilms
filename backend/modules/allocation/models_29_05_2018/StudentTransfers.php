<?php

namespace backend\modules\allocation\models;

use Yii;

/**
 * This is the model class for table "student_transfers".
 *
 * @property integer $student_transfer_id
 * @property integer $application_id
 * @property integer $programme_from
 * @property integer $programme_to
 * @property string $date_initiated
 * @property string $date_completed
 * @property integer $effetive_study_year
 * @property integer $admitted_student_id
 *
 * @property Application $application
 * @property Programme $programmeTo
 */
class StudentTransfers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_transfers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['application_id', 'programme_from', 'programme_to', 'date_initiated', 'admitted_student_id'], 'required'],
            [['application_id', 'programme_from', 'programme_to', 'effetive_study_year', 'admitted_student_id'], 'integer'],
            [['date_initiated', 'date_completed'], 'safe'],
            [['application_id'], 'exist', 'skipOnError' => true, 'targetClass' => Application::className(), 'targetAttribute' => ['application_id' => 'application_id']],
            [['programme_to'], 'exist', 'skipOnError' => true, 'targetClass' => Programme::className(), 'targetAttribute' => ['programme_to' => 'programme_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'student_transfer_id' => 'Student Transfer ID',
            'application_id' => 'Application ID',
            'programme_from' => 'Programme From',
            'programme_to' => 'Programme To',
            'date_initiated' => 'Date Initiated',
            'date_completed' => 'Date Completed',
            'effetive_study_year' => 'Effetive Study Year',
            'admitted_student_id' => 'Admitted Student ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplication()
    {
        return $this->hasOne(Application::className(), ['application_id' => 'application_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgrammeTo()
    {
        return $this->hasOne(Programme::className(), ['programme_id' => 'programme_to']);
    }
}
