<?php

use yii\helpers\Html;
?>
<div class="panel-heading"><h4>Allocation Special Groups </h4></div>
<?php if ($model->allocation_plan_stage==\backend\modules\allocation\models\AllocationPlan::STATUS_CLOSED): ?>
    <button type="button" class="btn btn-success" data-toggle="popover-x" data-placement="top" data-target="#w13">Add/Set Special Group</button>
<?php endif; ?>
<?=
kartik\grid\GridView::widget([
    'dataProvider' => $model_special_group,
//    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'group_name',
        'applicant_source_table',
        'applicant_souce_column',
        'operator',
        'applicant_source_value',
        [
            'class' => 'yii\grid\ActionColumn',
            'visible' => ($model->allocation_plan_stage != \backend\modules\allocation\models\AllocationPlan::STATUS_CLOSED) ? TRUE : FALSE,
            'header' => 'Action',
            'headerOptions' => ['style' => 'color:#337ab7'],
            'template' => '{delete}',
            'buttons' => [
                'delete' => function($model) {
                    return Html::a('<span class="glyphicon glyphicon-remove"></span>', Yii::$app->urlManager->createUrl('/allocation/allocation-plan-special-group/delete', ['id' => $model->allocation_id]), [
                                'title' => Yii::t('app', 'Delete'),]);
                },
                    ],
                ],
            ],
        ]);
        ?>

        <!--POPUP FORM-->
        <?php
        //echo $this->render('_form_add_special_group', ['allocation_framework_model' => $allocation_framework_model]);
        ?>