
<div class="scholarship-learning-institution-index">
    <!--<h4>Learning Institutions</h4>-->
    <p>
        <?= \yii\bootstrap\Html::a('Add Cluster Setting', ['/allocation/allocation-plan/add-cluster-setting', 'id' => $model->allocation_plan_id], ['class' => 'btn btn-success']) ?>
        <?=
        \yii\bootstrap\Html::a('Copy Existing Cluster setting', ['/allocation/allocation-plan/clone-cluster-setting', 'id' => $model->allocation_plan_id], ['class' => 'btn btn-warning',
            'data' => [
                'confirm' => 'Are you sure you want to Copy Cluster Setting from One Academic Year Into another?',
                'method' => 'post',
            ],]
        )
        ?>
    </p>
    <?=
    \kartik\grid\GridView::widget([
        'dataProvider' => $model_clusters,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'clusterDefinition.cluster_name',
            'cluster_priority',
            'student_percentage_distribution',
            'budget_percentage_distribution',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
