<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\popover\PopoverX;

PopoverX::begin([
    'placement' => PopoverX::ALIGN_TOP,
    'size' => PopoverX::SIZE_LARGE,
    'toggleButton' => $allocation_framework_model->is_active != backend\modules\allocation\models\AllocationFramework::STATUS_CLOSED ? ['label' => 'Add/Set Special Group', 'class' => 'btn btn-success'] : NULL,
    'header' => ' Form - Add/Set Allocation Special Group',
    'footer' => Html::button('Add/Save', [
        'class' => 'btn btn-sm btn-primary',
        'onclick' => '$("#allocation-special-group-form").trigger("submit")'
    ])
]);

$model = new backend\modules\allocation\models\AllocationFrameworkSpecialGroup();
$model->allocation_framework_id = $allocation_framework_model->allocation_plan_framework_id;
// form with an id used for action buttons in footer
$form = ActiveForm::begin(
                ['fieldConfig' => ['showLabels' => false],
                    'options' => ['id' => 'allocation-special-group-form',],
                    'action' => ['allocation-framework/add-special-group', 'id' => $model->allocation_framework_id],
        ]);

echo Form::widget([// fields with labels
    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [
        'allocation_framework_id' => ['type' => Form::INPUT_HIDDEN,
            'options' => [
                'data' => Yii::$app->params['priority_order_list'],
                'options' => [
                    'prompt' => '-- Select Priority Order --',
                ],
            ],
        ],
        'group_name' => ['type' => Form::INPUT_TEXT,
            'options' => [
                'prompt' => '-- Enter Special Group Name--',
            ],
        ],
        'applicant_source_table' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Source Table',
            'options' => [
                //'data' => ArrayHelper::map(\backend\modules\allocation\models\SourceTable::find()->all(), 'source_table_id', 'source_table_name'),
                'data' => \backend\modules\allocation\models\AllocationFramework::getSchemaTablesList(),
                'options' => [
                    'prompt' => 'Select Source Table',
                    'id' => 'source-table_Id'
                ],
            ],
        ],
        'applicant_souce_column' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\depdrop\DepDrop::className(),
            'label' => 'Source Table Columns',
            'options' => [
//                'data' => Yii::$app->db->schema->getTableSchema('applicant')->columnNames,
                'options' => [
                    'prompt' => 'Select Source column ',
                    'id' => 'source-table_field_Id'
                ],
                'pluginOptions' => [
                    'depends' => ['source-table_Id'],
                    'loading' => true,
                    'placeholder' => '-- Select --',
                    'initialize' => true,
                    'url' => \yii\helpers\Url::to(['/allocation/allocation-framework/table-columns']),
                    'loadingText' => 'Loading ...'
                ]
            ],
        ],
        'applicant_source_value' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\depdrop\DepDrop::className(),
            'label' => 'Source Table Columns',
            'options' => [
//                'data' => Yii::$app->db->schema->getTableSchema('applicant')->columnNames,
                'options' => [
                    'prompt' => 'Select Source column ',
                    'id' => 'source-table_field_value_Id'
                ],
                'pluginOptions' => [
                    'depends' => ['source-table_Id','source-table_field_Id'],
                    'loading' => true,
                    'placeholder' => '-- Select --',
                    'initialize' => true,
                    'url' => \yii\helpers\Url::to(['/allocation/allocation-framework/column-values']),
                    'loadingText' => 'Loading ...'
                ]
            ],
        ],
        'operator' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'options' => [
                'data' => \backend\modules\allocation\models\AllocationFrameworkSpecialGroup::getOperators(),
                'options' => [
                    'prompt' => '-- Select --',
                ],
            ],
        ],
]]);
?>

<?php

ActiveForm::end();

PopoverX::end();
?>

