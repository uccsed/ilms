
<div class="scholarship-learning-institution-index">
    <!--<h4>Learning Institutions</h4>-->
    <p>
        <?= \yii\bootstrap\Html::a('Add Institution Type setting', ['/allocation/allocation-plan/add-institution-type', 'id' => $model->allocation_plan_id], ['class' => 'btn btn-success']) ?>
        <?=
        \yii\bootstrap\Html::a('Copy Existing Into New Academic Year', ['/allocation/allocation-plan/clone-institution-type', 'id' => $model->allocation_plan_id], ['class' => 'btn btn-warning',
            'data' => [
                'confirm' => 'Are you sure you want to Copy Institution Type Setting from One Academic Year Into another?',
                'method' => 'post',
            ],]
        )
        ?>

    </p>
    <?=
    \kartik\grid\GridView::widget([
        'dataProvider' => $model_institutions_type,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'institution_type',
            'student_distribution_percentage',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
