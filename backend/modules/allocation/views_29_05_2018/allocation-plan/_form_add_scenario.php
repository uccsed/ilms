<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\popover\PopoverX;

PopoverX::begin([
    'placement' => PopoverX::ALIGN_TOP,
    'size' => PopoverX::SIZE_LARGE,
    'toggleButton' => $model_scenarios->is_active != backend\modules\allocation\models\AllocationPLan::STATUS_CLOSED ? ['label' => 'Add Criteria', 'class' => 'btn btn-success'] : NULL,
    'header' => ' Form - Add/Set Allocation Criteria or Scenario',
    'footer' => Html::button('Add/Save', [
        'class' => 'btn btn-sm btn-primary',
        'onclick' => '$("#allocation-scenario-form").trigger("submit")'
    ])
]);
$model = new backend\modules\allocation\models\AllocationPlanScenario();
$model->allocation_plan_id = $model_scenarios->allocation_plan_id;
// form with an id used for action buttons in footer
$form = ActiveForm::begin(
                ['fieldConfig' => ['showLabels' => false],
                    'options' => ['id' => 'allocation-scenario-form',],
                    'action' => ['allocation-plan/addscenario', 'id' => $model->allocation_plan_id],
        ]);

echo Form::widget([// fields with labels
    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [
        'allocation_plan_id' => ['type' => Form::INPUT_HIDDEN,
        ],
        'allocation_scenario' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'options' => [
                'data' => \backend\modules\allocation\models\AllocationPlanScenario::getAllocationPlanScenarios(),
                'options' => [
                    'prompt' => '-- Select Criteria--',
                ],
            ],
        ],
        'priority_order' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Priority Order',
            'options' => [
                'data' => Yii::$app->params['priority_order_list'],
                'options' => [
                    'prompt' => '-- Select Priority Order --',
                ],
            ],
        ],
]]);
?>

<?php

ActiveForm::end();

PopoverX::end();
?>


