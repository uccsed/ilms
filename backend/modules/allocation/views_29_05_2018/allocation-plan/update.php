<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationPlan */

$this->title = 'Update Allocation Plan: ' . $model->allocation_plan_id;
$this->params['breadcrumbs'][] = ['label' => 'Allocation Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->allocation_plan_id, 'url' => ['view', 'id' => $model->allocation_plan_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="allocation-plan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
