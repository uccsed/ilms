<?php

use yii\helpers\Html;
?>
<div class="panel-heading"><h4>Allocation Scenarios or Criterias </h4></div>

<?=
kartik\grid\GridView::widget([
    'dataProvider' => $model_scenarios,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'allocation_scenario',
            'value' => function ($model) {
                return $model->getName();
            },
        ],
        'priority_order',
        [
            'class' => 'yii\grid\ActionColumn',
        //    'visible' => ($model_scenarios->is_active != \backend\modules\allocation\models\AllocationPlan::STATUS_CLOSED) ? TRUE : FALSE,
            'header' => 'Action',
            'headerOptions' => ['style' => 'color:#337ab7'],
            'template' => '{delete}',
            'buttons' => [
                'delete' => function($model) {
                    return Html::a('<span class="glyphicon glyphicon-remove"></span>', Yii::$app->urlManager->createUrl('/allocation/allocation-plan/delete-scenario',['id' =>$model->allocation_plan_scenario_id]), [
                                'title' => Yii::t('app', 'Delete'),]);
                },
                    ],
//                    'urlCreator' => function ($action, $model, $key) {
//                if ($action === 'delete') {
//                    return yii\web\UrlManager::createUrl('allocation/allocation-framewrork/delete-scenario', ['id' => $key]);
//                }
//            },
                ],
            ],
        ]);
        ?>

        <!--POPUP FORM-->
        <?php
       //echo $this->render('_form_add_scenario', ['model' => $model]);
        ?>
