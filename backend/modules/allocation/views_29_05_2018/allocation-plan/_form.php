<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;

$form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);

echo Form::widget([// fields with labels
    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [
//''=>['label'=>'Item Name', 'options'=>['placeholder'=>'Item Name...']],
//        'allocation_framework_id' => [
//            'type' => Form::INPUT_WIDGET,
//            'widgetClass' => \kartik\select2\Select2::className(),
//            'label' => 'Allocation Framework',
//            'options' => [
//                'data' => ArrayHelper::map(\backend\modules\allocation\models\AllocationFramework::find()->where(['IN', 'is_active', [\backend\modules\allocation\models\AllocationFramework::STATUS_INACTIVE, \backend\modules\allocation\models\AllocationFramework::STATUS_ACTIVE]])->orderBy('allocation_framework_name')->asArray()->all(), 'allocation_plan_framework_id', 'allocation_framework_name'),
//                'options' => [
//                    'prompt' => '-- select --',
//                ],
//            ],
//        ],
        'allocation_plan_title' => ['type' => Form::INPUT_TEXT,
            'options' => ['prompt' => 'Select Academic  Year',
            ],
        ],
        'allocation_plan_desc' => ['type' => Form::INPUT_TEXT,
            'options' => ['prompt' => 'Select Academic  Year',
            ],
        ],
        'allocation_plan_stage' => [
            'type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'options' => [
                'data' => backend\modules\allocation\models\AllocationPlan::getStatusList(),
            ],
        ],
    ],
]);
?>
<div class="text-right">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php
    echo Html::resetButton('Reset', ['class' => 'btn btn-default']);
    ?>
    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-warning']) ?>
    <?php
    ActiveForm::end();
    ?>
</div>
