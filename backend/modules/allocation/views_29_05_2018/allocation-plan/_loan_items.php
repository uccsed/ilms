<div class="scholarship-student-index">
    <!--    <h4>Grant / Scholarship Students</h4>-->
    <p>
        <?= \yii\bootstrap\Html::a('Add Loan Item Setting', ['/allocation/allocation-plan/add-loan-item-setting', 'id' => $model->allocation_plan_id], ['class' => 'btn btn-success']) ?>
        <?=
        \yii\bootstrap\Html::a('Copy Existing Setting Into New Academic Year', ['/allocation/allocation-plan/clone-loan-item-setting', 'id' => $model->allocation_plan_id], ['class' => 'btn btn-warning',
            'data' => [
                'confirm' => 'Are you sure you want to Copy Loan item Setting from One Academic Year Into another?',
                'method' => 'post',
            ],]
        )
        ?>
    </p>
    <?=
    \kartik\grid\GridView::widget([
        'dataProvider' => $model_loan_item,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'loanItem.item_name',
            'priority_order',
            ['attribute' => 'unit_amount',
                'value' => 'unit_amount',
//                'format'=>  Yii::$app->formatter
            ],
            ['attribute' => 'rate_type',
                'value' => function($model) {
                    return backend\modules\allocation\models\LoanItem::getItemRateByValue($model->rate_type);
                },
            ],
            'duration',
            'loan_award_percentage',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
