<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<div class="allocation-setting-index">
    <div class="panel panel-info">

        <div class="panel-body">
            <p>
                <?= Html::a('Update', ['update', 'id' => $model->allocation_plan_id], ['class' => 'btn btn-primary']) ?>
                <?=
                Html::a('Delete', ['delete', 'id' => $model->allocation_plan_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ])
                ?>

                <?=
                \yii\bootstrap\Html::a('Copy Existing Plan Into New Academic Year', ['/allocation/allocation-plan/clone-loan-item-setting', 'id' => $model->allocation_plan_id], ['class' => 'btn btn-warning',
                    'data' => [
                        'confirm' => 'Are you sure you want to Copy Existing Setting from One Academic Year Into another?',
                        'method' => 'post',
                    ],]
                )
                ?>
            </p>
            <table class="table table-striped table-bordered detail-view" id="w0">
                <tbody>
                    <tr>
                        <th>Plan Number</th>
                        <td><?php echo strtoupper($model->allocation_plan_number); ?></td>
                        <td></td>
                        <th>Academic Year</th>
                        <td><?php echo $model->academicYear->academic_year ?></td>
                    </tr>

                    <tr>
                        <th>Allocation Plan Title</th>
                        <td colspan="5"> <?php echo strtoupper($model->allocation_plan_title); ?></td>
                    </tr>

                    <tr>
                        <th>Plan Description</th>
                        <td colspan="5"><?php echo $model->allocation_plan_desc; ?></td>
                    </tr>

                    <tr>
                        <th>Status</th>
                        <td> <?php echo $model->getStatusNameByValue(); ?></td>
                        <td></td>

                        <th>Created At</th>
                        <td><?php echo $model->created_at; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
