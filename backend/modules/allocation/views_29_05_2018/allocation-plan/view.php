<?php

use yii\helpers\Html;
use kartik\tabs\TabsX;

$this->title = 'Allocation Plan Details';
$this->params['breadcrumbs'][] = ['label' => 'Allocation Plan Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fixedassets-view">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?php
            echo $this->render('_details', ['model' => $model]);
            ?>
            <?php
            echo TabsX::widget([
                'items' => [
                    [
                        'label' => 'Scenarios Settings ',
                        'content' => $this->render('_allocation_scenario', ['model' => $model, 'model_scenarios' => $model_scenarios]),
                        'id' => 'tab1',
                        'active' => ($active == 'tab1') ? true : false,
                    ],
                    [
                        'label' => 'Special Groups ',
                        'content' => $this->render('_allocation_special_group', ['model' => $model, 'model_special_group' => $model_special_group]),
                        'id' => 'tab2',
                        'active' => ($active == 'tab2') ? true : false,
                    ],[
                        'label' => 'Clusters Settings ',
                        'content' => $this->render('_clusters', ['model' => $model, 'model_clusters' => $model_clusters]),
                        'id' => 'tab3',
                        'active' => ($active == 'ta3') ? true : false,
                    ],
                    [
                        'label' => 'Institutions Student % Distribution ',
                        'content' => $this->render('_institutions_type', ['model' => $model, 'model_institutions_type' => $model_institutions_type]),
                        'id' => 'tab4',
                        'active' => ($active == 'tab4') ? true : false,
                    ],
                    [
                        'label' => 'Loan Items Setting ',
                        'content' => $this->render('_loan_items', ['model' => $model, 'model_loan_item' => $model_loan_item]),
                        'id' => 'tab5',
                        'active' => ($active == 'tab5') ? true : false,
                    ],
                ],
                'position' => TabsX::POS_ABOVE,
                'bordered' => true,
                'encodeLabels' => false
            ]);
            ?>
        </div>

    </div> 
</div>