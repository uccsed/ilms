<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\StudentTransfers */

$this->title = 'Update Student Transfers: ' . $model->student_transfer_id;
$this->params['breadcrumbs'][] = ['label' => 'Student Transfers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->student_transfer_id, 'url' => ['view', 'id' => $model->student_transfer_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-transfers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
