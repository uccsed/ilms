<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\allocation\models\StudentTransfersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Student Transfers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-transfers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Student Transfers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'student_transfer_id',
            'application_id',
            'programme_from',
            'programme_to',
            'date_initiated',
            // 'date_completed',
            // 'effetive_study_year',
            // 'admitted_student_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
