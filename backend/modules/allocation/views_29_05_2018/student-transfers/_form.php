<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\StudentTransfers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-transfers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'application_id')->textInput() ?>

    <?= $form->field($model, 'programme_from')->textInput() ?>

    <?= $form->field($model, 'programme_to')->textInput() ?>

    <?= $form->field($model, 'date_initiated')->textInput() ?>

    <?= $form->field($model, 'date_completed')->textInput() ?>

    <?= $form->field($model, 'effetive_study_year')->textInput() ?>

    <?= $form->field($model, 'admitted_student_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
