<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
USE backend\modules\allocation\models\AllocationPlan;
?>
<div class="allocation-batch-create">
    <div class="panel panel-info">
        <div class="panel-warning">
            <?= Html::encode('Allocate Freshers Loan') ?>
        </div>
        <div class="panel-body">
            <?php if (Yii::$app->session->hasFlash('failure')) { ?>
                <p class="warning">
                    <?php echo Yii::$app->session->getFlash('failure'); ?>
                </p>
            <?php }
            ?>
            <?php
//contained_student
            $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
            echo Form::widget([ // fields with labels
                'model' => $model,
                'form' => $form,
                'columns' => 1,
                'attributes' => [
                    'academic_year' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        'options' => [
                            'data' => ArrayHelper::map(\common\models\AcademicYear::find()->asArray()->all(), 'academic_year_id', 'academic_year'),
                            'options' => [
                                'id' => 'academic-year',
                                'readonly' => TRUE, 'disabled' => TRUE
                            ],
                        ],
                    ],
                    'study_level' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        'options' => [
                            'data' => ArrayHelper::map(\backend\modules\allocation\models\ApplicantCategory::find()->asArray()->all(), 'applicant_category_id', 'applicant_category'),
                            'options' => [
                                'id' => 'programme-id',
                                'prompt' => '--select--'
                            ],
                            'pluginOptions' => [
                                'depends' => ['academic-year'],
                                'url' => \yii\helpers\Url::to(['/allocation/allocate-loan/allocation-plan'])
                            ],
                        ],
                    ],
                     'student_type' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        'options' => [
                            'data' => backend\modules\allocation\models\LoanItem::getStudentsType(),
                            'options' => [
                                'id' => 'student-type-id',
                                'prompt' => '--select--'
                            ],
                          
                        ],
                    ],
                    'allocation_framework' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\depdrop\DepDrop::classname(),
                        'options' => [
                            'data' => ArrayHelper::map(AllocationPlan::find()->where(['academic_year_id' => $model->academic_year, 'allocation_plan_stage' => AllocationPlan::STATUS_ACTIVE])->asArray()->all(), 'allocation_plan_id', 'allocation_plan_title'),
                            'options' => [
                                'id' => 'programme-id',
                                'prompt' => '--select--'
                            ],
                            'pluginOptions' => [
                                'depends' => ['academic-year'],
                                'url' => \yii\helpers\Url::to(['/allocation/allocate-loan/allocation-plan'])
                            ],
                        ],
                    ],
                ]
            ]);
            ?>
            <div class="text-right">
                <?= Html::submitButton('Process Loan Allocation', ['class' => 'btn btn-primary']) ?>
                <?php
                ActiveForm::end();
                ?>
            </div>

        </div>

    </div>