<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
?>
<div class="panel panel-info">
    <div class="panel-heading">
        Institution  Detail
    </div>
    <div class="panel-body">
        <?php
        echo Form::widget([ // fields with labels
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'institution_type' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    'label' => 'Institution Type',
                    'options' => [
                        'data' => backend\modules\allocation\models\LearningInstitution::getInstitutionTypes(),
                        'options' => [
                            'prompt' => 'Institution Type',
                        ],
                    ],
                ],
                'country' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    'label' => 'Country',
                    'options' => [
                        'data' => ArrayHelper::map(\frontend\modules\application\models\Country::find()->asArray()->all(), 'country_code', 'country_name'),
                        'options' => [
                            'prompt' => ' Select Parent',
                        ],
                    ],
                ],
                'ownership' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    'label' => 'Ownership',
                    'options' => [
                        'data' => \backend\modules\allocation\models\LearningInstitution::getOwneshipsList(),
                        'options' => [
                            'prompt' => '-- Select --',
                        ],
                    ],
                ],
                'institution_name' => ['label' => 'Institution Name', 'labelSpan' => 4, 'options' => ['placeholder' => 'Institution Name', 'labelSpan' => 4]],
                'institution_code' => ['label' => 'Institution Code', 'options' => ['placeholder' => 'Institution Code...']],
                'parent_id' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    'label' => 'Parent Name',
                    'options' => [
                        'data' => ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::getHigherLearningInstitution(), 'learning_institution_id', 'institution_name'),
                        'options' => [
                            'prompt' => ' Select Parent Institution',
                        ],
                    ],
                ],
                'phone_number' => ['label' => 'Institution Phone', 'options' => ['placeholder' => 'Institution Phone']],
                'physical_address' => ['label' => 'Institution Address', 'options' => ['placeholder' => 'Institution Address']],
                'ward_id' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    'label' => 'Ward',
                    'options' => [
                        'data' => ArrayHelper::map(\backend\modules\application\models\Ward::find()->asArray()->all(), 'ward_id', 'ward_name'),
                        'options' => [
                            'prompt' => ' Select Ward',
                        ],
                    ],
                ],
                'bank_account_number' => ['label' => 'Bank Account Number', 'options' => ['placeholder' => 'Bank Account Number']],
                'bank_account_name' => ['label' => 'Bank Account Name', 'options' => ['placeholder' => 'Bank Account Name']],
                'is_active' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
//                    'label' => 'is_active',
                    'options' => [
                        'data' => \backend\modules\allocation\models\LearningInstitution::getStatusList(),
                    ],
                ],
            ]
        ]);
        ?>
    </div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">
        Contact Personal Detail
    </div>
    <div class="panel-body">
        <?php
        echo Form::widget([ // fields with labels
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'cp_firstname' => ['label' => 'Contact First Name', 'options' => ['placeholder' => 'Contact First Name']],
            // 'entered_by_applicant'=>['label'=>'Institution Address', 'options'=>['placeholder'=>'Institution Address']],
            ]
        ]);
        echo Form::widget([ // fields with labels
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'cp_middlename' => ['label' => 'Contact Middle Name', 'options' => ['placeholder' => 'Contact Middle Name']],
            // 'entered_by_applicant'=>['label'=>'Institution Address', 'options'=>['placeholder'=>'Institution Address']],
            ]
        ]);
        echo Form::widget([ // fields with labels
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'cp_surname' => ['label' => 'Contact Surname', 'options' => ['placeholder' => 'Contact Surname']],
            //    'entered_by_applicant'=>['label'=>'Institution Address', 'options'=>['placeholder'=>'Institution Address']],
            ]
        ]);

        echo Form::widget([ // fields with labels
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'cp_email_address' => ['label' => 'Contact Email Address', 'options' => ['placeholder' => 'Contact Email Address']],
            //    'entered_by_applicant'=>['label'=>'Institution Address', 'options'=>['placeholder'=>'Institution Address']],
            ]
        ]);
        echo Form::widget([ // fields with labels
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'cp_phone_number' => ['label' => 'Contact Phone Number', 'options' => ['placeholder' => 'Contact Phone Number'], 'columnOptions' => ['colspan' => 2]],
            //  'entered_by_applicant'=>['label'=>'Institution Address', 'options'=>['placeholder'=>'Institution Address']],
            ]
        ]);
        ?>
    </div>
</div>
<div class="text-right">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php
    echo Html::resetButton('Reset', ['class' => 'btn btn-default']);
    ?>
    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-warning']) ?>
    <?php
    ActiveForm::end();
    ?>
</div>
