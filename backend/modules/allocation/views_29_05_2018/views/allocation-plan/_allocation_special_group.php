<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>
<!--<div class="panel-heading"><h4>Allocation Special Groups </h4></div>-->
<?php if ($model->allocation_plan_stage != \backend\modules\allocation\models\AllocationPlan::STATUS_CLOSED): ?>
    <p><?php if ($model->allocation_plan_stage != backend\modules\allocation\models\AllocationPlan::STATUS_CLOSED && !$model->hasStudents()) { ?>

            <?= Html::a('Add/Set Special Group', ['/allocation/allocation-plan/add-special-group', 'id' => $model->allocation_plan_id], ['class' => 'btn btn-success']) ?>

        <?php } ?>
    </p>

<?php endif; ?>
<?=
kartik\grid\GridView::widget([
    'dataProvider' => $model_special_group,
//    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'group_name',
        'applicant_source_table',
        'applicant_souce_column',
        'operator',
        'applicant_source_value',
        [
            'class' => 'yii\grid\ActionColumn',
            'visible' => ($model->allocation_plan_stage != \backend\modules\allocation\models\AllocationPlan::STATUS_CLOSED) ? TRUE : FALSE,
            'header' => 'Action',
            'headerOptions' => ['style' => 'color:#337ab7'],
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $model) {
        $url = Url::to(['allocation-plan/delete-plan-specialgroup', 'id' => $model->special_group_id]);
        return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, [
            'title'        => 'delete',
            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method'  => 'post',
        ]);
    },
                    ],
                ],
            ],
        ]);
        ?>
