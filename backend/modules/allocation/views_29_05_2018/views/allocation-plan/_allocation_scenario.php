<?php

use yii\helpers\Html;
?>
<!--<div class="panel-heading"><h4>Allocation Scenarios or Criterias </h4></div>-->
<p>
    <?php if ($model->allocation_plan_stage != backend\modules\allocation\models\AllocationPlan::STATUS_CLOSED && !$model->hasStudents()) { ?>

        <?= Html::a('Add Scenario', ['/allocation/allocation-plan/add-scenario', 'id' => $model->allocation_plan_id], ['class' => 'btn btn-success']) ?>
    <?php } ?>
</p>
<?=
kartik\grid\GridView::widget([
    'dataProvider' => $model_scenarios,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'allocation_scenario',
            'value' => function ($model) {
                return $model->getName();
            },
        ],
        'priority_order',
        [
            'class' => 'yii\grid\ActionColumn',
            //    'visible' => ($model_scenarios->is_active != \backend\modules\allocation\models\AllocationPlan::STATUS_CLOSED) ? TRUE : FALSE,
            'header' => 'Action',
            'headerOptions' => ['style' => 'color:#337ab7'],
            'template' => '{delete}',
            'buttons' => [
                'delete' => function($model) {
                    return Html::a('<span class="glyphicon glyphicon-remove"></span>', Yii::$app->urlManager->createUrl('/allocation/allocation-plan/delete-scenario', ['id' => $model->allocation_plan_scenario_id]), [
                                'title' => Yii::t('app', 'Delete'),]);
                },
                    ],
                ],
            ],
        ]);
        ?>
