<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;

$model_special_group->allocation_plan_id = $model->allocation_plan_id;
// form with an id used for action buttons in footer
// form with an id used for action buttons in footer
$form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL], [
            'enableAjaxValidation' => true,
            'method' => 'POST',
            'action' => ['allocation-framework/add-special-group', 'id' => $model->allocation_plan_id],
        ]);

echo Form::widget([// fields with labels
    'model' => $model_special_group,
    'form' => $form,
    'columns' => 1,
    'attributes' => [
        'allocation_plan_id' => ['type' => Form::INPUT_HIDDEN,
            'options' => [
                'data' => Yii::$app->params['priority_order_list'],
                'options' => [
                    'prompt' => '-- Select Priority Order --',
                ],
            ],
        ],
        'group_name' => ['type' => Form::INPUT_TEXT,
            'options' => [
                'prompt' => '-- Enter Special Group Name--',
            ],
        ],
        'applicant_source_table' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Source Table',
            'options' => [
                //'data' => ArrayHelper::map(\backend\modules\allocation\models\SourceTable::find()->all(), 'source_table_id', 'source_table_name'),
                'data' => \backend\modules\allocation\models\AllocationPlan::getSchemaTablesList(),
                'options' => [
                    'prompt' => 'Select Source Table',
                    'id' => 'source-table_Id'
                ],
            ],
        ],
        'applicant_souce_column' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\depdrop\DepDrop::className(),
            'label' => 'Source Table Columns',
            'options' => [
//                'data' => Yii::$app->db->schema->getTableSchema('applicant')->columnNames,
                'options' => [
                    'prompt' => 'Select Source column ',
                    'id' => 'source-table_field_Id'
                ],
                'pluginOptions' => [
                    'depends' => ['source-table_Id'],
                    'loading' => true,
                    'placeholder' => '-- Select --',
                    'initialize' => true,
                    'url' => \yii\helpers\Url::to(['/allocation/allocation-plan/table-columns']),
                    'loadingText' => 'Loading ...'
                ]
            ],
        ],
        'applicant_source_value' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\depdrop\DepDrop::className(),
            'label' => 'Columns Value',
            'options' => [
//                'data' => Yii::$app->db->schema->getTableSchema('applicant')->columnNames,
                'options' => [
                    'prompt' => 'Select Source column ',
                    'id' => 'source-table_field_value_Id'
                ],
                'pluginOptions' => [
                    'depends' => ['source-table_Id', 'source-table_field_Id'],
                    'loading' => true,
                    'placeholder' => '-- Select --',
                    'initialize' => true,
                    'url' => \yii\helpers\Url::to(['/allocation/allocation-plan/column-values']),
//                    'loadingText' => 'Loading ...'
                ]
            ],
        ],
        'operator' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'options' => [
                'data' => \backend\modules\allocation\models\AllocationPlanSpecialGroup::getOperators(),
                'options' => [
                    'prompt' => '-- Select --',
                ],
            ],
        ],
]]);
?>

<div class="text-right">
    <?= Html::submitButton($cluster->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php
    echo Html::resetButton('Reset', ['class' => 'btn btn-default']);
    ?>
    <?= Html::a('Cancel', ['/allocation/allocation-plan/view', 'id' => $model->allocation_plan_id], ['class' => 'btn btn-warning        ']) ?>
    <?php
    ActiveForm::end();
    ?>
</div>

