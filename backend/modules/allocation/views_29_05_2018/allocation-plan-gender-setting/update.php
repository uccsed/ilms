<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationPlanGenderSetting */

$this->title = 'Update Allocation Plan Gender Setting: ' . $model->allocation_plan_gender_setting_id;
$this->params['breadcrumbs'][] = ['label' => 'Allocation Plan Gender Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->allocation_plan_gender_setting_id, 'url' => ['view', 'id' => $model->allocation_plan_gender_setting_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="allocation-plan-gender-setting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
