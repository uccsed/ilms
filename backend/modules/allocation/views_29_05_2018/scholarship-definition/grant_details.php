 <p>
        <?= \yii\bootstrap\Html::a('Edit/Update', ['update', 'id' => $model->scholarship_id], ['class' => 'btn btn-primary']) ?>
        <?=
        \yii\bootstrap\Html::a('Delete', ['delete', 'id' => $model->scholarship_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    kartik\detail\DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'is_full_scholarship',
                'label' => 'Grant / Scholarship Type',
                'value' => $model->getScholarshipTypeName()
            ],
            'scholarship_name',
            'scholarship_desc',
            'sponsor',
            [
                'attribute' => 'country_of_study',
                'value' => $model->country->country_name
            ],
            'start_year',
            'end_year',
            [
                'attribute' => 'is_active',
                'label' => 'Status',
                'value' => $model->getScholarshipStatusName()
            ],
            'closed_date',
        ],
    ])
    ?>





