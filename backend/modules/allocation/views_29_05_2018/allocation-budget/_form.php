<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationBudget */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="allocation-budget-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'budget_amount')->textInput() ?>

    <?= $form->field($model, 'academic_year_id')->textInput() ?>

    <?= $form->field($model, 'applicant_category')->dropDownList([ 'normal' => 'Normal', 'scholarship' => 'Scholarship', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'study_level')->textInput() ?>

    <?= $form->field($model, 'place_of_study')->dropDownList([ 'TZ' => 'TZ', 'FCOUNTRY' => 'FCOUNTRY', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'budget_scope')->dropDownList([ 1 => '1', 2 => '2', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'is_active')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
