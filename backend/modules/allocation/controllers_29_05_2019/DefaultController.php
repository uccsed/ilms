<?php

namespace backend\modules\allocation\controllers;

use Yii;
//use yii\web\Controller;
use backend\modules\application\models\Application;
use frontend\modules\repayment\models\LoanRepaymentBillDetail;
use backend\modules\allocation\models\Criteria;
use backend\modules\application\models\ApplicationSearch;
use common\components\Controller;

/**
 * Default controller for the `allocation` module
 */
class DefaultController extends Controller {

    /**
     * Renders the index view for the module
     * @return string
     */
    public $layout = "main_private";

    public function actionIndex() {

        return $this->render('index');
    }

    public function actionIndexCompliance() {

        $searchModel = new ApplicationSearch();
        $dataProvider = $searchModel->searchcompliance(Yii::$app->request->queryParams);

        return $this->render('application-compliance', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCheckCompliance() {
        //select all application with status=0
        $modelapplication = Application::find()->joinWith('applicant')->where(['allocation_status' => NULL])->limit(100)->all();
//        print_r($modelapplication);
//        exit();
        //check eligibility  eligibility
        $rate_loan = $require_topay = 0;
        if (count($modelapplication) > 0) {
            $finalstatus = 0;
            foreach ($modelapplication as $rows) {
                $applicationId = $rows["application_id"];
                $programme_id = $rows["programme_id"];

                $transfer_status = $rows["transfer_status"];
                $f4indexno = $rows['applicant']["f4indexno"];
                //  exit();
                /**
                 * check admission status.
                 */
                if ($programme_id > 0) {
                    /**
                     * end check admission status
                     *
                     * Check loan History.
                     */
                    //getApplicantDetails($applcantF4IndexNo,$NIN);
                    $loan_rate =  \backend\models\SystemSetting::findOne(["is_active" => 1,'setting_code'=>'PLRFNL']);

                    $total_loan = \backend\modules\disbursement\models\Disbursement::getLoan($f4indexno);

                    if (count($loan_rate) > 0) {
                        $rate_loan = ($loan_rate->setting_value/100);
                    }
                    //   exit();
                    if ($total_loan > 0) {
                        $require_topay = $total_loan * $rate_loan;
                    }
                    $loan_paid = \backend\modules\disbursement\models\Disbursement::getLoanPayment($f4indexno);
                    //check transfer information
                    //If exit waiting to complite
                    if ($loan_paid >= $require_topay) {
                        //get pull of eligibility criteria question and answer or criteria field with answer
                        //criteria question with answer
                        /**
                         * Processing  Criteria question answer .
                         * @
                         */
                        $this->CheckCriteriaQuestion($applicationId);
                        /**
                         * End Processing  Criteria question answer .
                         *
                         *
                         * Start Processing  Criteria Field question answer .
                         * @
                         */
                        $this->CheckCriteriaField($applicationId);
                        //print_r($question_criteria_field);
                        //    exit();
                    } else {
                        /*
                         * User or applicant have loan and not pay the rate or percent required
                         */
                        //update admission status
                        $comment = "Not pay the Previous Loan";
                        $status = 2;
                        $this->UpdateAdmission($applicationId, $comment, $status);
                        $finalstatus+=1;
                        //upate end
                    }
                } else {
                    //update admission status
                    $comment = "Missing Admission";
                    $status = 2;
                    $this->UpdateAdmission($applicationId, $comment, $status);
                    $finalstatus+=1;
                    //upate end
                }
                //update final  eligibility status
                if ($finalstatus == 0) {
                    $comment = "Eligible";
                    $status = 1;
                    $this->UpdateAdmission($applicationId, $comment, $status);
                }
                //end
            }
        }
        //end
        return $this->redirect(['index-compliance']);
        /// return $this->render('');
    }

    public function getApplicationQuestion($applicationId, $question_id, $answered) {

        return Yii::$app->db->createCommand("SELECT * FROM `applicant_question` aq
                                                  join `applicant_qn_response` ar
                                                  on ar.`applicant_question_id`=aq.`applicant_question_id`
                                                  join `application` a
                                                  on a.`application_id`=aq.`application_id`
                                                  join qresponse_source qs
                                                  on qs.`qresponse_source_id`=ar.`qresponse_source_id`
                                                  WHERE a.application_id='{$applicationId}' AND question_id='{$question_id}' AND $answered")->queryAll();
    }

    public function CheckCriteriaQuestion($applicationId) {
        $question_criteria = Criteria::getCriteriaQuestion(1);
        $commentcount = 0;
        if (count($question_criteria) > 0) {
            foreach ($question_criteria as $system_row) {
                $final_test = 0;
                //application question answer
                $qresponse_source_id = $system_row["qresponse_source_id"];
                $parentId = $system_row["parent_id"];
                $response_id = $system_row["response_id"];
                $question_id = $system_row["question_id"];
                $question_answer = $system_row["value"];
                if ($commentcount == 0) {
                    if ($qresponse_source_id != "" && $response_id != "") {
                        $answered = "qs.qresponse_source_id='{$qresponse_source_id}' AND ar.response_id='{$response_id}'";
                    } else {
                        $answered = "question_answer='{$question_answer}'";
                    }

                    //get system criteria answer
                    $answeredvalue = Criteria::getCriteriaPosibleQuestionAnswer($system_row['source_table'], $system_row['source_table_value_field'], $system_row['response_id'], $system_row['source_table_text_field']);

                    $application_question = $this->getApplicationQuestion($applicationId, $question_id, $answered);
                    
                    //print_r($application_question);
                    if (count($application_question) > 0) {
                        foreach ($application_question as $rowsq)
                            ;

                        $final_test = Criteria::getapplicantCriteriaQuestionAnswer($rowsq['source_table'], $rowsq['source_table_value_field'], $rowsq['response_id'], $rowsq['source_table_text_field'], $system_row["operator"], $answeredvalue);
                        //echo $answereddata;
                    }
                    //check child data
                    $criteria_field_child = Criteria::getCriteriaQuestionChild($parentId, 1);
                    if (count($criteria_field_child) > 0) {
                        foreach ($criteria_field_child as $rows_child) {
                            $join_operator_child = $rows_child["join_operator"];
                            //  $parentId=$rows_child["criteria_field_id"];
                            //$response_value_child = Criteria::TestApplicantCriteriaFieldAnswer($rows_child["source_table"], $rows_child["source_table_field"], $rows_child["value"], $rows_child["operator"]);
                            //get system criteria answer
                            $answeredvaluechild = Criteria::getCriteriaPosibleQuestionAnswer($rows_child['source_table'], $rows_child['source_table_value_field'], $rows_child['response_id'], $rows_child['source_table_text_field']);

                            $application_question_child = $this->getApplicationQuestion($applicationId, $question_id, $answered);
                            $final_testchild = 0;
                            //print_r($application_question);
                            if (count($application_question_child) > 0) {
                                foreach ($application_question_child as $rowsq_child)
                                    ;

                                $final_testchild = Criteria::getapplicantCriteriaQuestionAnswer($rowsq_child['source_table'], $rowsq_child['source_table_value_field'], $rowsq_child['response_id'], $rowsq_child['source_table_text_field'], $rowsq_child["operator"], $answeredvaluechild);
                                //echo $answereddata;
                            }
                            if ($join_operator_child != "") {
                                if ($join_operator_child == "AND") {
                                    $final_test = $final_test * $final_testchild;
                                } else if ($join_operator_child == "OR") {
                                    $final_test = $final_test + $final_testchild;
                                }
                            }
                        }
                    }
                    //update status
                    if ($final_test == 0) {
                        //update application status
                        // $commentcount+=1;
                        $comment = " Your not eligibaly";
                        $status = 2;
                        $this->UpdateAdmission($applicationId, $comment, $status);
                        continue;
                        ///end
                    } else if ($final_test > 0) {
                        //update application status
                        // $commentcount+=1;
                        $comment = " Your Eligibaly";
                        $status = 1;
                        $this->UpdateAdmission($applicationId, $comment, $status);
                        continue;
                        ///end
                    }
                }
            }
        }
    }

    public function CheckCriteriaField($applicationId) {
        $final_test = 0;
        $question_criteria_field = Criteria::getCriteriaFieldQuestion(1);
        if (count($question_criteria_field) > 0) {
            foreach ($question_criteria_field as $rows_qs) {
                //$join_operator=$rows_qs["join_operator"];
                $parentId = $rows_qs["criteria_field_id"];
                //$final_test=-1;
                $response_value = Criteria::TestApplicantCriteriaFieldAnswer($rows_qs["source_table"], $rows_qs["source_table_field"], $rows_qs["value"], $rows_qs["operator"]);
                $final_test = $response_value;

                $criteria_field_child = Criteria::getCriteriaFieldChild($parentId, 1);
                if (count($criteria_field_child) > 0) {
                    foreach ($criteria_field_child as $rows_child) {
                        $join_operator_child = $rows_child["join_operator"];
                        //  $parentId=$rows_child["criteria_field_id"];
                        $response_value_child = Criteria::TestApplicantCriteriaFieldAnswer($rows_child["source_table"], $rows_child["source_table_field"], $rows_child["value"], $rows_child["operator"]);
                        if ($join_operator_child != "") {
                            if ($join_operator_child == "AND") {
                                $final_test = $final_test * $response_value_child;
                            } else if ($join_operator_child == "OR") {
                                $final_test = $final_test + $response_value_child;
                            }
                        }
                    }
                }
                //update status
                if ($final_test == 0) {
                    //update application status
                    // $commentcount+=1;
                    $comment = " Your not eligibaly";
                    $status = 2;
                    $this->UpdateAdmission($applicationId, $comment, $status);
                    continue;
                    ///end
                } else if ($final_test > 0) {
                    //update application status
                    // $commentcount+=1;
                    $comment = " Your Eligibaly";
                    $status = 1;
                    $this->UpdateAdmission($applicationId, $comment, $status);
                    continue;
                    ///end
                }
                //end
            }
        }
    }

    /**
     * Update Application  table.
     * @param integer $applicationId
     * @return mixed
     */
    public function UpdateAdmission($applicationId, $comment, $status) {
        $model_update = Application::findone($applicationId);
        $model_update->allocation_status = $status;
        $model_update->allocation_comment = $comment . " ; " . $model_update->allocation_comment;
        $model_update->save();
        return $model_update;
    }

    public function actionIndexMeanTest() {

        $searchModel = new ApplicationSearch();
        $dataProvider = $searchModel->searchMeanTest(Yii::$app->request->queryParams);

        return $this->render('application-mean-test', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionComputeNeedness() {
        //select all application with status=0
        $modelapplication = Application::find()->where(['submitted' => 0])->limit(20)->asArray()->all();
        //print_r($modelapplication);
        //check eligibility  eligibility
        if (count($modelapplication)) {
            foreach ($modelapplication as $rows) {
                $applicationId = $rows["applicant_id"];
                //get pull of eligibility criteria question and answer or criteria field with answer
                //criteria question with answer

                $sql_system_question_criteria = Yii::$app->db->createCommand("SELECT * from `criteria_question` cq,`criteria_question_answer` ca WHERE
                                                                                     ca.`criteria_question_id`=cq.`criteria_question_id` AND `type`=2")->queryAll();
                //end
                foreach ($sql_system_question_criteria as $system_row) {
                    //application question answer
                    $qresponse_source_id = $system_row["qresponse_source_id"];
                    $response_id = $system_row["response_id"];
                    $question_id = $system_row["question_id"];
                    $question_answer = $system_row["value"];
                    if ($qresponse_source_id != "" && $response_id != "") {
                        $answered = "qresponse_source_id='{$qresponse_source_id}' AND response_id='{$response_id}'";
                    } else {
                        $answered = "question_answer='{$question_answer}'";
                    }
                    $sql_application = Yii::$app->db->createCommand("SELECT * FROM `applicant_question` aq join `applicant_qn_response` ar
                                                                                          on ar.`applicant_question_id`=aq.`applicant_question_id`
                                                                                          join `application` a on a.`application_id`=aq.`application_id` WHERE a.application_id='{$applicationId}' AND question_id='{$question_id}' AND $answered ")->queryAll();
                }
            }
        }
        //end
        // return $this->render('index');
    }

    public function AdmissionStatus() {
        
    }

    public function LoanHistory() {
        
    }

    public function RepaymentStatus() {
        
    }

}
