<?php

namespace backend\modules\allocation\controllers;

use Yii;
use backend\modules\allocation\models\ClusterProgramme;
use backend\modules\allocation\models\ClusterProgrammeSearch;
//use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Controller;

/**
 * ClusterProgrammeController implements the CRUD actions for ClusterProgramme model.
 */
class ClusterProgrammeController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        $this->layout = "default_main";
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ClusterProgramme models.
     * @return mixed
     */
    public function actionIndex($id) {
        $searchModel = new ClusterProgrammeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'id' => $id,
                    'cmodel' => \backend\modules\allocation\models\ClusterDefinition::find()->select('is_active')->where(['cluster_definition_id' => $id])->one()
        ]);
    }

    /**
     * Displays a single ClusterProgramme model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ClusterProgramme model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id) {
        $model = new ClusterProgramme();
        $models = [ new ClusterProgramme()];

        if ($model->load(Yii::$app->request->post())) {
            $error = NULL;
            //fixed by me tele
            $model->programme_group_id=1;
            $model->cluster_definition_id=1;
            $model->programme_id=1;
            $model->created_at =date("Y-m-d H:i:s");
            $model->created_by=1;
            //echo $model->programme_group_id;
            //exit;
            //end fixed by me tele
            //getting all the programmes that are in that group
            /*
            $group_programmes = \backend\modules\allocation\models\Programme::getProgrammesByProgrammeGroupId($model->programme_group_id);
            if ($group_programmes) {
                $count_success = $count_failure = 0;
                foreach ($group_programmes as $programme) {
                    $cluster_programme_model = new ClusterProgramme();
                    $cluster_programme_model->cluster_definition_id = $model->cluster_definition_id;
                    $cluster_programme_model->academic_year_id = $model->academic_year_id;
                    $cluster_programme_model->programme_category_id = $model->programme_category_id;
                    $cluster_programme_model->programme_group_id = $model->programme_group_id;
                    $cluster_programme_model->programme_id = $programme->programme_id;
                    $cluster_programme_model->programme_priority = $model->programme_priority;
                    if ($cluster_programme_model->save()) {
                        $count_success++;
                    } else {
                        $count_failure++;
                    }
                }
                if ($count_success > 0 && $count_success == $count_failure) {
                    $sms = 'Operation succesful, All Programmes saved';
                    Yii::$app->getSession()->setFlash(
                            'success', $sms
                    );
                } elseif ($count_success == 0) {
                    $sms = 'Operation failed, No Programme has been saved';
                    Yii::$app->getSession()->setFlash(
                            'failure', $sms
                    );
                } elseif ($count_success > 0 && $count_failure > 0) {
                    $sms = 'Operation Successful, Some Programmes were not saved';
                    Yii::$app->getSession()->setFlash(
                            'success', $sms
                    );
                }
            }
            */
            if($model->save()){
            return $this->redirect(['index', 'id' => $model->cluster_definition_id]);
            }
        } else {
            return $this->render('create', [
            'model' => $model, 'models' => $models,
            'cluster_id' => $id
            ]);
        }
    }

    /**
     * Updates an existing ClusterProgramme model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash(
                    'success', 'Data Successfully Updated!'
            );
            return $this->redirect(['index', 'id' => $model->programme_category_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ClusterProgramme model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $model1 = $this->findModel($id);
        $model1->delete();
        Yii::$app->getSession()->setFlash(
                'success', 'Data Successfully Deleted!'
        );
        return $this->redirect(['index', 'id' => $model->cluster_definition_id]);
    }

    /**
     * Finds the ClusterProgramme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClusterProgramme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = ClusterProgramme::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
