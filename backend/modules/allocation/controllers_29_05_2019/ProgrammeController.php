<?php

namespace backend\modules\allocation\controllers;

use Yii;
use backend\modules\allocation\models\Programme;
use backend\modules\allocation\models\ProgrammeSearch;
//use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use common\components\Controller;
use yii\web\Controller;
use backend\modules\allocation\models\LoanItem;
use \backend\modules\allocation\models\ProgrammeCost;
use backend\modules\allocation\models\base\LearningInstitution;
use yii\web\UploadedFile;

/**
 * ProgrammeController implements the CRUD actions for Programme model.
 */
class ProgrammeController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        $this->layout = "main_private";
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Programme models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ProgrammeSearch();
        $dataProviderActive = $searchModel->search(Yii::$app->request->queryParams);
        $dataProviderInActive = $searchModel->search(Yii::$app->request->queryParams);
        $dataProviderClosed = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProviderActive' => $dataProviderActive,
                    'dataProviderInActive' => $dataProviderInActive,
                    'dataProviderClosed' => $dataProviderClosed,
        ]);
    }

    public function actionProgrammeList() {
        $searchModel = new ProgrammeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('programmeslist', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Programme model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        $programme_costs = new \backend\modules\allocation\models\ProgrammeCostSearch();
        $programme_costs->programme_id = $model->programme_id;
        $dataProvider = $programme_costs->search(Yii::$app->request->queryParams);

        return $this->render('view', [
                    'model' => $model, 'programme_costs' => $programme_costs,
        ]);
    }

    /**
     * Creates a new Programme model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Programme();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash(
                    'success', 'Data Successfully Created!'
            );
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Programme model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash(
                    'success', 'Data Successfully Updated!'
            );
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Programme model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Programme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Programme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Programme::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetprogrammename() {

        $out = [];
        //$programme_category_id=2;
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $academic_year = $parents[0];
                $programme_category_id = implode(',', $parents[1]);
                $out = Programme::getprogrammeName($programme_category_id, $academic_year);

                echo \yii\helpers\Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
    }

    /**
     * Creates a new ProgrammeFee model. based on the pprogramme selected
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCost($id) {
        $model = $this->findModel($id); //getting the programme model
        $cost = new ProgrammeCost;
        $cost->programme_id = $model->programme_id;
        $model_cost = [ new ProgrammeCost];
        //getting programme Country of study to determine the LoanItems to retrieve based on the country 
        $programme_country_of_study = $model->learningInstitution->country;
        $programme_group = $model->programmeGroup;
        ////Setting Item Category based on the country of study of the programme
        $cost->item_category = ($programme_country_of_study == 'TZ' OR $programme_country_of_study == 'TZA') ? LoanItem::ITEM_CATEGORY_NORMAL : LoanItem::ITEM_CATEGORY_SCHOLARSHIP;

        if (Yii::$app->request->post('ProgrammeCost')) {
            $post_data = Yii::$app->request->post('ProgrammeCost');
            $count = count($post_data);
            //validating the data received
            if ($count) {
//                $valid_entry = 0;
                $valid_models = array();
                $academic_year = $post_data['academic_year_id'];
                $year_of_study = $post_data['year_of_study'];
                $loan_items = $post_data['loan_item_id'];
                $rate_type = $post_data['rate_type'];
                $unit_amount = $post_data['unit_amount'];
                $duration = $post_data['duration'];
                foreach ($loan_items as $key => $data) {
                    $validation_model = new ProgrammeCost;
                    $validation_model->programme_id = $model->programme_id;
                    $validation_model->academic_year_id = $academic_year;
                    $validation_model->year_of_study = $year_of_study;
                    $validation_model->loan_item_id = $data;
                    $validation_model->rate_type = $rate_type[$key];
                    $validation_model->unit_amount = $unit_amount[$key];
                    $validation_model->duration = $duration[$key];
                    if ($validation_model->validate()) {
                        array_push($valid_models, $validation_model);
                    } else {
                        var_dump($validation_model->errors);
                    }
                }
                var_dump($valid_models);
                exit;
                ///end validation
                if (count($valid_models)) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        $failure = 0;
                        //loopng  through thr valid mode to save each
                        foreach ($valid_models as $key => $valid_model) {
                            if (!$valid_model->save()) {
                                $failure++;
                            }
                        }
                        if ($failure) {
                            $transaction->rollBack();
                        } else {
                            $transaction->commit();
                            return $this->redirect(['view', 'id' => $model->programme_id]);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
            }
        }
        return $this->render('add_cost', [
                    'model' => $model,
                    'cost' => $cost,
                    'model_cost' => (empty($model_cost)) ? [new \backend\modules\allocation\models\ProgrammeCost] : $model_cost
        ]);
    }
/*
    public function actionBulkUpload() {
        $model = new Programme;

        if (Yii::$app->request->post()) {
            
        }
        return $this->render('bulk_upload', [
                    'model' => $model,
        ]);
    }
 * 
 */
    /*
     public function actionBulkUpload() {
        $model = new Programme;

        if (Yii::$app->request->post()) {
            
        }
        return $this->render('bulk_upload', [
                    'model' => $model,
        ]);
    }
     * 
     */
    
    public function actionBulkUpload() {	
        //$searchModelGepgBill = new ProgrammeSearch();
		//$modelGepgBill = new Programme();
                
                $searchModelProgramme = new ProgrammeSearch();
		$modelProgramme = new Programme();
                
        $loggedin = Yii::$app->user->identity->user_id;		
        $dataProvider = $searchModelProgramme->search(Yii::$app->request->queryParams);
                $modelProgramme->scenario = 'programme_bulk_upload';
            if ($modelProgramme->load(Yii::$app->request->post())) {
                $date_time = date("Y_m_d_H_i_s");
                $inputFiles1 = UploadedFile::getInstance($modelProgramme, 'programe_file');
                $modelProgramme->programe_file = UploadedFile::getInstance($modelProgramme, 'programe_file');                
                $modelProgramme->upload($date_time);
                $inputFiles = 'upload/' . $date_time . $inputFiles1;

                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFiles);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFiles);
                } catch (Exception $ex) {
                    die('Error');
                }
                $learningInstitutionID= $modelProgramme->learning_institution_id;
   
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();                
                if (strcmp($highestColumn, "E") == 0 && $highestRow >= 3) {
                    //VALIDATING IF A FILE HAS NO RECORD TO BE DISCARDED...
                    
                    
                    $row = 2;
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);                   
                    //checking the template format
                    $s_sno=$rowData[0][0];
                    $PROGRAMME_NAME=$rowData[0][1];
                    $PROGRAMME_CODE=$rowData[0][2];
                    $PROGRAMME_GROUP_CODE=$rowData[0][3];
                    $YEARS_OF_STUDY=$rowData[0][4];
                                       
                    if(strcmp($s_sno,'S/No')==0 && strcmp($PROGRAMME_NAME,'PROGRAMME_NAME')==0 && strcmp($PROGRAMME_CODE,'PROGRAMME_CODE')==0 && strcmp($PROGRAMME_GROUP_CODE,'PROGRAMME_GROUP_CODE')==0 && strcmp($YEARS_OF_STUDY,'YEARS_OF_STUDY')==0){
                    //end check template format
                        $sn=$s_sno;
                    if ($sn == '') {
                        unlink('upload/' . $date_time . $inputFiles1);
                        $sms = '<p>Operation failed, file with no records is not allowed</p>';
                        Yii::$app->getSession()->setFlash('error', $sms);
                        return $this->redirect(['bulk-upload']);
                    } else {
                        $objPHPExcelOutput = new \PHPExcel();
                        $objPHPExcelOutput->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $objPHPExcelOutput->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);
                        $objPHPExcelOutput->setActiveSheetIndex(0);
                        $objPHPExcelOutput->getActiveSheet()->SetCellValue('A1', 'PROGRAMMES UPLOAD REPORT');
                        $objPHPExcelOutput->setActiveSheetIndex(0)->mergeCells('A1:G1', 'PROGRAMMES UPLOAD REPORT');

                        $rowCount = 3;
                        $s_no = 0;
                        $customTitle = ['SNo', 'PROGRAMME_NAME','PROGRAMME_CODE', 'PROGRAMME_GROUP_CODE', 'YEARS_OF_STUDY', 'UPLOAD STATUS', 'FAILED REASON'];
                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('A' . $rowCount, $customTitle[0]);
                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('B' . $rowCount, $customTitle[1]);
                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('C' . $rowCount, $customTitle[2]);
                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('D' . $rowCount, $customTitle[3]);
                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('E' . $rowCount, $customTitle[4]);
					$objPHPExcelOutput->getActiveSheet()->SetCellValue('F' . $rowCount, $customTitle[5]);
					$objPHPExcelOutput->getActiveSheet()->SetCellValue('G' . $rowCount, $customTitle[6]);

                    for ($row = 3; $row <= $highestRow; ++$row) {
                            $s_no++;
                            $rowCount++;
                            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                            $modelProgramme = new Programme();
                            $modelProgramme->scenario = 'programme_bulk_upload3';
                            $modelProgramme->programme_name = Programme::formatRowData($rowData[0][1]);
                            $modelProgramme->programme_code = Programme::formatRowData($rowData[0][2]);
			    $modelProgramme->programmeGcode = Programme::formatRowData($rowData[0][3]);
			    $modelProgramme->years_of_study = Programme::formatRowData($rowData[0][4]);
                            $modelProgramme->learning_institution_id=$learningInstitutionID;
                            $modelProgramme->is_active  = Programme::STATUS_PENDING;
                            $programmeGroupCode = \backend\modules\allocation\models\ProgrammeGroup::getProgrammGroupID($programmeGroupCode);
                            //$modelProgramme->learning_institution_id=1;
                            $modelProgramme->programme_group_id=$programmeGroupCode->programme_group_id;
							//$modelGepgBill->date_created='';
                            $modelProgramme->validate();
                             $reason = '';
                             if($modelProgramme->hasErrors()){
                                $errors = $modelProgramme->errors;
                                foreach ($errors as $key => $value) {
                                    $reason = $reason.$value[0].'  ';									
                                }
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('A' . $rowCount, $s_no);
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('B' . $rowCount, $modelProgramme->programme_name);
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('C' . $rowCount, $modelProgramme->programme_code);
			        $objPHPExcelOutput->getActiveSheet()->SetCellValue('D' . $rowCount, $modelProgramme->programmeGcode);
				$objPHPExcelOutput->getActiveSheet()->SetCellValue('E' . $rowCount, $modelProgramme->years_of_study);
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('F' . $rowCount, 'UPLOADED FAILED');
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('G' . $rowCount, $reason);
                            unlink('upload/' . $date_time . $inputFiles1);
                                
                            } else {
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('A' . $rowCount, $s_no);
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('B' . $rowCount, $modelProgramme->programme_name);
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('C' . $rowCount, $modelProgramme->programme_code);
				$objPHPExcelOutput->getActiveSheet()->SetCellValue('D' . $rowCount, $modelProgramme->programmeGcode);
				$objPHPExcelOutput->getActiveSheet()->SetCellValue('E' . $rowCount, $modelProgramme->years_of_study);
                                $modelProgramme->save();
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('F' . $rowCount, 'UPLOADED SUCCESSFUL');
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('G' . $rowCount, 'N/A');                              
										
                            }                            
                        }                                                        
		        unlink('upload/' . $date_time . $inputFiles1);                      
                        $objPHPExcelOutput->getActiveSheet()->getStyle('A1:G' . $highestRow)->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN)->getColor()->setRGB('DDDDDD');
                        $writer = \PHPExcel_IOFactory::createWriter($objPHPExcelOutput, 'Excel5');
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="Programmes upload report.xls"');
                        header('Cache-Control: max-age=0');
                        $writer->save('php://output');
                        //$writer->save('upload'."/programmes_".$date_time.".xls");
                        $currentFileName="programmes_".$date_time.".xls";                        
                        
                        $sms ="Programmes file submitted, kindly check the upload results in excel file!";                      
                        
                        Yii::$app->getSession()->setFlash('success', $sms);
                       // return $this->redirect(['bulk-upload']);
                        
                    }
                }else{
                    unlink('upload/' . $date_time . $inputFiles1);
                    $sms = '<p>Operation failed,excel template used has invalid format, please download sample from system and populate data!</p>';
                    Yii::$app->getSession()->setFlash('error', $sms);
                    return $this->redirect(['bulk-upload']);
                }
                    
                }
                else{
                        unlink('upload/' . $date_time . $inputFiles1);
                        $sms = '<p>Operation failed, file with no records is not allowed</p>';
                        Yii::$app->getSession()->setFlash('error', $sms);
                        return $this->redirect(['bulk-upload']);
                }
            }
        return $this->render('bulk_upload', [
                    'model'=>$modelProgramme,
        ]);
    }
    
    
    
    
    public function actionUploadError()
    {
        $model = new Programme();
        return $this->render('upload_error', [
            'model' =>$model,
        ]);
    }
    public function actionDownload($currentFileName)
{
    $path=Yii::getAlias('@webroot'). '/upload';
    $file=$path. '/'.$currentFileName;
    if (file_exists($file)) {
        return Yii::$app->response->sendFile($file);
    } else {
        throw new \yii\web\NotFoundHttpException("{$file} is not found!");
    }
}
public function actionDownloadTemplate()
{
    $path=Yii::getAlias('@webroot'). '/upload';
    $file=$path. '/institution_programmes_template.xlsx';
    if (file_exists($file)) {
        return Yii::$app->response->sendFile($file);
    } else {
        throw new \yii\web\NotFoundHttpException("{$file} is not found!");
    }
}

}
