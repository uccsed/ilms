<?php

namespace backend\modules\allocation\controllers;

use Yii;
use backend\modules\allocation\models\AllocationPlan;
use backend\modules\allocation\models\AllocationPlanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AllocationPlanController implements the CRUD actions for AllocationPlan model.
 */
class AllocationPlanController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        $this->layout = "main_private";
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AllocationPlan models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AllocationPlanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AllocationPlan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        $active = '';
        if (\Yii::$app->session['active_tab']) {
            $active = \Yii::$app->session['active_tab'];
        }
        if ($model) {

            $model_scenarios = \backend\modules\allocation\models\AllocationPlanScenario::getAllocationScenarionsByFrameworkId($model->allocation_plan_id);
            //getting allocation special groups
            $model_special_group = \backend\modules\allocation\models\AllocationPlanSpecialGroup::getAllocationSpecialGroupsByFrameworkId($model->allocation_plan_id);

//getting plan cluster
            $model_clusters = \backend\modules\allocation\models\AllocationPlanClusterSetting::getAllocationPlanClustersById($id);
//getting programmes under the given scholarship
            $model_institutions_type = \backend\modules\allocation\models\AllocationPlanInstitutionTypeSetting::getInstitutionTypeSettingsById($id);
//getting grant loan items
            $model_loan_item = \backend\modules\allocation\models\AllocationPlanLoanItem::getLoanItemsById($id);

            return $this->render('view', [
                        'model' => $model, 'active' => $active,
                        'model_special_group' => $model_special_group,
                        'model_scenarios' => $model_scenarios,
                        'model_clusters' => $model_clusters,
                        'model_institutions_type' => $model_institutions_type,
                        'model_loan_item' => $model_loan_item,
            ]);
        }
    }

    /**
     * Creates a new AllocationPlan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AllocationPlan();

        if ($model->load(Yii::$app->request->post())) {
            $model->allocation_plan_number = AllocationPlan::generateRandomString(99999999);
            if ($model->allocation_framework_id) {
                $model->academic_year_id = \backend\modules\allocation\models\AllocationFramework::getFrameworkAcademicYearByFrameworkId($model->allocation_framework_id);
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->allocation_plan_id]);
            }
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing AllocationPlan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->allocation_plan_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AllocationPlan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AllocationPlan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AllocationPlan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AllocationPlan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAddClusterSetting($id) {
        $model = $this->findModel($id);
        $cluster = New \backend\modules\allocation\models\AllocationPlanClusterSetting();
        $cluster->allocation_plan_id = $model->allocation_plan_id;
        if (Yii::$app->request->post('AllocationPlanClusterSetting')) {
            $cluster->attributes = Yii::$app->request->post('AllocationPlanClusterSetting');
            if ($cluster->allocation_plan_id == $id) {
                try {
                    if ($cluster->save()) {
                        \Yii::$app->session['active_tab'] = 'tab2';
                        return $this->redirect(['view', 'id' => $id]);
                    }
                } catch (yii\db\Exception $exception) {
                    if ($exception !== null) {
                        return $this->render('error', ['exception' => $exception]);
                    }
                }
            }
        }

        return $this->render('addClusters', [
                    'model' => $model, 'cluster' => $cluster
        ]);
    }

    public function actionAddInstitutionType($id) {
        $model = $this->findModel($id);
        $institution = New \backend\modules\allocation\models\AllocationPlanInstitutionTypeSetting();
        $institution->allocation_plan_id = $model->allocation_plan_id;
        if (Yii::$app->request->post('AllocationPlanInstitutionTypeSetting')) {
            $institution->attributes = Yii::$app->request->post('AllocationPlanInstitutionTypeSetting');
            if ($institution->allocation_plan_id == $id) {
                try {
                    if ($institution->save()) {
                        \Yii::$app->session['active_tab'] = 'tab3';
                        return $this->redirect(['view', 'id' => $id]);
                    }
                } catch (yii\db\Exception $exception) {
                    if ($exception !== null) {
                        return $this->render('error', ['exception' => $exception]);
                    }
                }
            }
        }

        return $this->render('addInstitution', [
                    'model' => $model, 'institution' => $institution
        ]);
    }

    public function actionAddLoanItemSetting($id) {
        $model = $this->findModel($id);
        $loan_item = New \backend\modules\allocation\models\AllocationPlanLoanItem();
        $loan_item->allocation_plan_id = $model->allocation_plan_id;
        if (Yii::$app->request->post('AllocationPlanLoanItem')) {
            $loan_item->attributes = Yii::$app->request->post('AllocationPlanLoanItem');

            if ($loan_item->allocation_plan_id == $id) {

                try {
                    if ($loan_item->save()) {
                        \Yii::$app->session['active_tab'] = 'tab4';
                        return $this->redirect(['view', 'id' => $id]);
                    }
                } catch (yii\db\Exception $exception) {
                    if ($exception !== null) {
                        return $this->render('error', ['exception' => $exception]);
                    }
                }
            }
        }
        return $this->render('addLoanItem', [
                    'model' => $model, 'loan_item' => $loan_item
        ]);
    }
    
    
    /**
     * Creates a new AllocationFramework model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAddscenario($id) {
        $model_scenario = new AllocationFrameworkScenario();
        $model = $this->findModel($id);
        if ($model->is_active == AllocationFramework::STATUS_INACTIVE OR $model->is_active == AllocationFramework::STATUS_ACTIVE) {
            if (Yii::$app->request->post('AllocationFrameworkScenario')) {
                $model_scenario->attributes = Yii::$app->request->post('AllocationFrameworkScenario');
                if ($id == $model_scenario->allocation_framework_id) {
                    $model_scenario->save();
                }
            }
        }
        \Yii::$app->session['tab'] = 'tab1';
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Creates a new AllocationFramework model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAddSpecialGroup($id) {
        $model_special_group = new AllocationFrameworkSpecialGroup();
        $model = $this->findModel($id);
        if ($model->is_active == AllocationFramework::STATUS_INACTIVE OR $model->is_active == AllocationFramework::STATUS_ACTIVE) {
            if (Yii::$app->request->post('AllocationFrameworkSpecialGroup')) {
                $model_special_group->attributes = Yii::$app->request->post('AllocationFrameworkSpecialGroup');
                if ($id == $model_special_group->allocation_framework_id) {
                    $model_special_group->save();
                }
            }
        }
        \Yii::$app->session['tab'] = 'tab2';
        return $this->redirect(['view', 'id' => $id]);
    }

    public function ActionCronConfiguration($id) {
        $model = $this->findModel($id);
        if ($model->is_active != AllocationFramework::STATUS_INACTIVE && ($model->is_active == AllocationFramework::STATUS_CLOSED OR $model->is_active == AllocationFramework::STATUS_ACTIVE)) {
            
        }
        echo 'Page Uner Construction';
    }

}
