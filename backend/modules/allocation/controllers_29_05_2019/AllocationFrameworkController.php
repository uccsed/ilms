<?php

namespace backend\modules\allocation\controllers;

use Yii;
use backend\modules\allocation\models\AllocationFramework;
use backend\modules\allocation\models\AllocationFrameworkSearch;
use backend\modules\allocation\models\AllocationFrameworkScenario;
use backend\modules\allocation\models\AllocationFrameworkSpecialGroup;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

//use common\components\Controller;
/**
 * AllocationFrameworkController implements the CRUD actions for AllocationFramework model.
 */
class AllocationFrameworkController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        $this->layout = "main_private";
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AllocationFramework models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AllocationFrameworkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AllocationFramework model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        $dataProviderScenarios = $dataProviderSpecialGroups = NULL;
        $tab = \Yii::$app->session['tab'];
        if ($model) {
            ///getting allocation framework scenarios
            $dataProviderScenarios = AllocationPlanScenario::getAllocationScenarionsByFrameworkId($model->allocation_plan_id);
            //getting allocation special groups
            $dataProviderSpecialGroups = AllocationFrameworkSpecialGroup::getAllocationSpecialGroupsByFrameworkId($model->allocation_plan_id);
        }
        return $this->render('view', [
                    'model' => $model,
                    'dataProviderScenarios' => $dataProviderScenarios,
                    'dataProviderSpecialGroups' => $dataProviderSpecialGroups,
                    'active' => $tab
        ]);
    }

    /**
     * Creates a new AllocationFramework model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AllocationFramework();
        $model->is_active = AllocationFramework::STATUS_INACTIVE;
        $model->release_number = AllocationFramework::generateRandomString(9999);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->allocation_plan_framework_id]);
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Creates a new AllocationFramework model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAddscenario($id) {
        $model_scenario = new AllocationFrameworkScenario();
        $model = $this->findModel($id);
        if ($model->is_active == AllocationFramework::STATUS_INACTIVE OR $model->is_active == AllocationFramework::STATUS_ACTIVE) {
            if (Yii::$app->request->post('AllocationFrameworkScenario')) {
                $model_scenario->attributes = Yii::$app->request->post('AllocationFrameworkScenario');
                if ($id == $model_scenario->allocation_framework_id) {
                    $model_scenario->save();
                }
            }
        }
        \Yii::$app->session['tab'] = 'tab1';
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Creates a new AllocationFramework model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAddSpecialGroup($id) {
        $model_special_group = new AllocationFrameworkSpecialGroup();
        $model = $this->findModel($id);
        if ($model->is_active == AllocationFramework::STATUS_INACTIVE OR $model->is_active == AllocationFramework::STATUS_ACTIVE) {
            if (Yii::$app->request->post('AllocationFrameworkSpecialGroup')) {
                $model_special_group->attributes = Yii::$app->request->post('AllocationFrameworkSpecialGroup');
                if ($id == $model_special_group->allocation_framework_id) {
                    $model_special_group->save();
                }
            }
        }
        \Yii::$app->session['tab'] = 'tab2';
        return $this->redirect(['view', 'id' => $id]);
    }

    public function ActionCronConfiguration($id) {
        $model = $this->findModel($id);
        if ($model->is_active != AllocationFramework::STATUS_INACTIVE && ($model->is_active == AllocationFramework::STATUS_CLOSED OR $model->is_active == AllocationFramework::STATUS_ACTIVE)) {
            
        }
        echo 'Page Uner Construction';
    }

    /**
     * Updates an existing AllocationFramework model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->is_active == AllocationFramework::STATUS_INACTIVE OR $model->is_active == AllocationFramework::STATUS_ACTIVE) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->allocation_plan_framework_id]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing AllocationFramework model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        if ($model->is_active == AllocationFramework::STATUS_INACTIVE OR $model->is_active == AllocationFramework::STATUS_ACTIVE) {
            $this->findModel($id)->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the AllocationFramework model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AllocationFramework the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AllocationFramework::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*
     * gets the list of columns for a prticular database table
     */

    public function actionTableColumns() {
        $out = [];
        //$programme_category_id=2;
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null && isset($parents[0])) {
                $table_name = trim($parents[0]);
                $data = Yii::$app->db->schema->getTableSchema($table_name)->columnNames;
                foreach ($data as $key => $value) {
                    array_push($out, ['id' => $value, 'name' => $value]);
                }
                echo \yii\helpers\Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo \yii\helpers\Json::encode(['output' => '', 'selected' => '']);
    }

    /*
     * returns the possible values for a particular table column
     */

    public function actionColumnValues() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null && isset($parents[0]) && $parents[0] && isset($parents[1]) && $parents[1]) {
                $table_name = trim($parents[0]);
                $column_name = trim($parents[1]);
                $sql = "SELECT DISTINCT(" . $column_name . ") AS " . $column_name . " FROM " . $table_name;
                $data = Yii::$app->db->createCommand($sql)->queryAll();
                foreach ($data as $value) {
                    if ($value[$column_name]) {
                        array_push($out, ['id' => $value[$column_name], 'name' => $value[$column_name]]);
                    }
                }
                echo \yii\helpers\Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo \yii\helpers\Json::encode(['output' => '', 'selected' => '']);
    }

}
