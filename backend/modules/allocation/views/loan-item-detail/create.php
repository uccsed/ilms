<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\LoanItemPriority */
$this->title = 'Add/Create Loan Item Setting';
$this->params['breadcrumbs'][] = ['label' => 'Loan Item Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-item-priority-create">
<div class="panel panel-info">
        <div class="panel-heading">
       <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>

        </div>
    </div>
</div>