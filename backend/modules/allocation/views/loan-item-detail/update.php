<?php
use yii\helpers\Html;

$this->title = 'Update Loan Item Settinf: ';
$this->params['breadcrumbs'][] = ['label' => 'Loan Item Details', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->loan_item_priority_id, 'url' => ['view', 'id' => $model->loan_item_priority_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="loan-item-priority-update">
<div class="panel panel-info">
        <div class="panel-heading">
       <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>

        </div>
    </div>
</div>