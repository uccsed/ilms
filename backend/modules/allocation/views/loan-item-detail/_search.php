<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\LoanItemDetailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-item-detail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'loan_item_detail_id') ?>

    <?= $form->field($model, 'loan_item_id') ?>

    <?= $form->field($model, 'loan_item_category') ?>

    <?= $form->field($model, 'study_level') ?>

    <?= $form->field($model, 'is_active') ?>

    <?php // echo $form->field($model, 'list_order') ?>

    <?php // echo $form->field($model, 'place_of_study') ?>

    <?php // echo $form->field($model, 'academic_year_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
