<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = "Loan Item Details:" . $model->loan_item_detail_id;
$this->params['breadcrumbs'][] = ['label' => 'Loan Item Setting Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="learning-institution-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->loan_item_detail_id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->loan_item_detail_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //  'loan_item_detail_id',
                    [ 'attribute' => 'academic_year_id',
                        'value' => $model->academicYear->academic_year,
                    ],
                    [ 'attribute' => 'loan_item_detail_id',
                        'value' => $model->loanItem->item_name,
                    ],
                    [ 'attribute' => 'loan_item_category',
                        'value' => $model->loan_item_category,
                    ],
                    [ 'attribute' => 'loan_item_category',
                        'header'=>'Study Level',
                        'value' => $model->studyLevel->applicant_category,
                    ],
                    'list_order',
                    'place_of_study',
                ],
            ])
            ?>

        </div>
    </div>
</div>