<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationMovementSchedule */

$this->title = 'Save As New Allocation Movement Schedule: '. ' ' . $model->allocation_movement_schedule_id;
$this->params['breadcrumbs'][] = ['label' => 'Allocation Movement Schedule', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->allocation_movement_schedule_id, 'url' => ['view', 'id' => $model->allocation_movement_schedule_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="allocation-movement-schedule-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
