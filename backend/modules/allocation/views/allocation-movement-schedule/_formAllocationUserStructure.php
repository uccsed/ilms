<div class="form-group" id="add-allocation-user-structure">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\models\User;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'AllocationUserStructure',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'allocation_user_structure_id' => ['type' => TabularForm::INPUT_HIDDEN,'label'=>''],
        'allocation_structure_id' => [
            'label' => 'Allocation structure',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\AllocationStructure::find()->orderBy('allocation_structure_id')->asArray()->all(), 'allocation_structure_id', 'structure_name'),
                'options' => ['placeholder' => 'Choose Allocation structure'],
            ],
            //'columnOptions' => ['width' => '200px']
        ],
        'allocation_task_id' => [
            'label' => 'Allocation Task',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\AllocationTask::find()->orderBy('allocation_task_id')->asArray()->all(), 'allocation_task_id', 'task_name'),
                'options' => ['placeholder' => 'Choose Allocation task '],
            ],
           // 'columnOptions' => ['width' => '200px']
        ],
       'order_level' => [
            'label' => 'Order Level',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => [1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10],
                'options' => ['placeholder' => 'Choose Order Level'],
            ],
            // 'columnOptions' => ['width' => '200px']
        ],
        'user_id' => [
            'label' => 'Staff',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(User::find()->where(['login_type'=>5])->orderBy('username')->asArray()->all(), 'user_id', 'username'),
                'options' => ['placeholder' => 'Choose Staff'],
            ],
           // 'columnOptions' => ['width' => '200px']
        ],
       //'status' => ['type' => TabularForm::INPUT_TEXT],
        'status' => [
            'label' => 'Status',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' =>[1=>'Active',2=>'Inactive'],
                'options' => ['placeholder' => 'Choose Status'],
            ],
            // 'columnOptions' => ['width' => '200px']
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowAllocationUserStructure(' . $key . '); return false;', 'id' => 'allocation-user-structure-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Allocation User Structure', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowAllocationUserStructure()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

