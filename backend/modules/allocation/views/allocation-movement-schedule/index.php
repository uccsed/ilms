<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\allocation\models\AllocationMovementScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Allocation Movement Schedule';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="allocation-movement-schedule-index">
<div class="panel panel-info">
        <div class="panel-heading">
       <?= Html::encode($this->title); ?>
        </div>
        <div class="panel-body">
    <p>
        <?= Html::a('Register Allocation Movement Schedule', ['create'], ['class' => 'btn btn-success']) ?>
     
    </p>
    
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ///'allocation_movement_schedule_id',
        [
                'attribute' => 'allocation_movement_id',
                'label' => 'Allocation Movement',
                'value' => function($model){                   
                return $model->allocationMovement->movement_name;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\AllocationMovement::find()->asArray()->all(), 'allocation_movement_id', 'movement_name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Allocation movement', 'id' => 'grid-allocation-movement-schedule-search-allocation_movement_id']
            ],
        [
                'attribute' => 'academic_year_id',
                'label' => 'Academic Year',
                'value' => function($model){                   
                    return $model->academicYear->academic_year;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\AcademicYear::find()->asArray()->all(), 'academic_year_id', 'academic_year'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Academic year', 'id' => 'grid-allocation-movement-schedule-search-academic_year_id']
            ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-allocation-movement-schedule']],
     
    ]); ?>

</div>
</div>
</div>
