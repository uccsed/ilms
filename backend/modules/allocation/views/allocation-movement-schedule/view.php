<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationMovementSchedule */

$this->title ='Allocation Movement Schedule';
$this->params['breadcrumbs'][] = ['label' => 'Allocation Movement Schedule', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allocation-movement-schedule-view">
<div class="panel panel-info">
        <div class="panel-heading">
       <?= Html::encode($this->title); ?>
        </div>
        <div class="panel-body">
        <p>
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->allocation_movement_schedule_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->allocation_movement_schedule_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->allocation_movement_schedule_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
    </p>
<?php 
    $gridColumn = [
        //'allocation_movement_schedule_id',
        [
            'attribute' => 'allocationMovement.movement_name',
            'label' => 'Allocation Movement',
        ],
        [
            'attribute' => 'academicYear.academic_year',
            'label' => 'Academic Year',
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
  
        <h4>Allocation Movement</h4>
     
    <?php 
    $gridColumnAllocationMovement = [
        'movement_name',
        'description',
       // 'status',
    ];
    echo DetailView::widget([
        'model' => $model->allocationMovement,
        'attributes' => $gridColumnAllocationMovement    ]);
    ?>
  
<?php
if($providerAllocationUserStructure->totalCount){
    $gridColumnAllocationUserStructure = [
        ['class' => 'yii\grid\SerialColumn'],
            //'allocation_user_structure_id',
            [
                'attribute' => 'allocationStructure.structure_name',
                'label' => 'Allocation Structure'
            ],
                        [
                'attribute' => 'allocationTask.task_name',
                'label' => 'Allocation Task'
            ],
            [
                'attribute' => 'user.username',
                'label' => 'User'
            ],
            //'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAllocationUserStructure,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-allocation-user-structure']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Allocation User Structure'),
        ],
        'export' => false,
        'columns' => $gridColumnAllocationUserStructure
    ]);
}
?>

    </div>
</div>
</div>
