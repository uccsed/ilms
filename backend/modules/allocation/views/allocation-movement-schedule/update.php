<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationMovementSchedule */

$this->title = 'Update Allocation Movement Schedule: ' ;
$this->params['breadcrumbs'][] = ['label' => 'Allocation Movement Schedule', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' =>'Details', 'url' => ['view', 'id' => $model->allocation_movement_schedule_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="allocation-movement-schedule-update">
<div class="panel panel-info">
        <div class="panel-heading">
       <?= Html::encode($this->title); ?>
        </div>
        <div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>