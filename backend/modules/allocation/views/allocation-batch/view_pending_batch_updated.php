<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use backend\modules\allocation\models\AllocationTask;
use common\models\AcademicYear;

/* @var $this yii\web\View */
/* @var $model frontend\modules\allocation\models\AllocationBatch */

//$this->title = $model->allocation_batch_id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Allocation Batch'), 'url' => ['/disbursement/default/allocation-batch']];
//$this->params['breadcrumbs'][] = $this->title;
$academic_year_id=AcademicYear::getLatestAcademicYear();
?>
<div class="allocation-batch-view">
 
    <div class="panel panel-info">
                        <div class="panel-heading">
                     
                        </div>
                        <div class="panel-body">
  
    <p>
        <?php 
        if($model->status==3&&$model->movement_type==3){
              ?>
  
    <?= Html::a('LARC Approve', ['allocation-batch-approve', 'id' => $model->allocation_batch_id], [
            'class' => 'btn btn-primary',
            'data' => [
                'confirm' => 'Are you sure you want to maked as review this allocation batch?',
                'method' => 'post',
            ],
        ]);
 
           }
    
         
        //end
       ?>
     <?= Html::a('Download Allocation Batch', ['allocation/allocation-batch-download','allocation_batch_id'=>$model->allocation_batch_id], ['class' => 'btn btn-warning text-right','target'=>'_blank']) ?>
 
    </p>
    
    <?= DetailView::widget([
        'model' => $model,
         'condensed' => true,
        'hover' => true,
        'attributes' => [
            //'allocation_batch_id',
            //'batch_number',
            'batch_desc',
           // 'academic_year_id',
           // 'academicYear.academic_year', 
                       [
                        'attribute'=>'academic_year_id', 
                        'label'=>'Academic Year',
                        'format'=>'raw',
                        'value'=>call_user_func(function ($data){

                                           return $data->academicYear->academic_year;
                        },$model),
                       // 'valueColOptions'=>['style'=>'width:30%']
                       ],
           // 'available_budget',
           // 'is_approved',
                [
                        'attribute'=>'is_approved', 
                        'label'=>'Approve Status',
                        'format'=>'raw',
                        'value'=>call_user_func(function ($data){
                                        if($data->status==10){
                                            $status='<label class="label label-success">Approved</label>';
                                        }
                                        else if($data->status==2){
                                            $status='<label class="label label-danger">Disapproved</label>';
                                        }
                                        else{
                                            $status='<label class="label label-info">In movement</label>';
                                        }
                                           return $status;
                        },$model),
                       // 'valueColOptions'=>['style'=>'width:30%']
                       ],
            'approval_comment:ntext',
            //'created_at',
            //'created_by',
           // 'is_canceled',
            //'cancel_comment:ntext',
        ],
    ]) ?>
  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <td colspan="2"><b>ALLOCATION BATCH SUMMARY</b></td>
                      </tr>
                      <tr>
                          <td><b>LOAN ITEM</b></td>
                        <td align='right'><b>AMOUNT</b></td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                       // print_r($dataProvider);
                              
                        $total=0;
                 $sql=  backend\modules\allocation\models\Allocation::find()->select("*,sum(allocated_amount) as amount,item_name")->joinWith("loanItem")->GroupBy("item_name")->where("allocation_batch_id=$model->allocation_batch_id")->asArray()->all();
                  if(count($sql)>0){
                 foreach ($sql as $rows){
                 echo "<tr>
                        <td>".$rows["item_name"]."</td>
                        <td align='right'>".number_format($rows["amount"])."</td>
                     
                      </tr>";
                 $total+=$rows["amount"];
                           }
               echo "<tr>
                        <td> </td>
                        <td align='right'><b>".number_format($total)."</b></td>
                     
                      </tr>";
                  }
                  else{
               echo "<tr><td colspan='2'><font color='red'>Sorry No results found</font></td></tr>";       
                  }
                  
                  function getstatus($order_level,$allocation_task_id){
                      $sqlall ="SELECT * FROM `allocation_task_assignment` dt join allocation_task_definition dd on dd.allocation_task_id=dt.`allocation_task_id` join allocation_user_structure aus on dt.allocation_user_structure_id=aus.allocation_user_structure_id join allocation_structure als on als.allocation_structure_id=aus.allocation_structure_id AND order_level<='{$order_level}' AND dt.allocation_task_id<>'{$allocation_task_id}'";
                      $model_task= Yii::$app->db->createCommand($sqlall)->queryOne();
                      
                      
                  }
                       ?>
                  
                    </tbody>
                    
                  </table>
</div>
    </div>
</div>