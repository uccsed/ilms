<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
?>
<div class="allocation-batch-view">
    <p>       
      <?=
        Html::a('Delete Batch', ['delete-additional-batch', 'id' => $model->allocation_batch_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'method' => 'post',
            ],
        ]);
        ?>
    </p>
    <div class="panel panel-info">

        <div class="panel-body">
            <p>            </p>
            <?=
            DetailView::widget([
                'model' => $model,
                'condensed' => true,
                'hover' => true,
                'attributes' => [
                    'batch_number',
                    'batch_desc',
                    [
                        'attribute' => 'academic_year_id',
                        'label' => 'Academic Year',
                        'format' => 'raw',
                        'value' => call_user_func(function ($data) {

                                    return $data->academicYear->academic_year;
                                }, $model),
                    ],
                    [
                        'attribute' => 'is_approved',
                        'label' => 'Approve Status',
                        'format' => 'raw',
                        'value' => call_user_func(function ($data) {
                                    if ($data->is_approved == 0) {
                                        $status = '<label class="label label-info">Pending</label>';
                                    } else if ($data->is_approved == 1) {
                                        $status = '<label class="label label-success">Approved</label>';
                                    } else if ($data->is_approved == 2) {
                                        $status = '<label class="label label-danger">Disapproved</label>';
                                    }
                                    return $status;
                                }, $model),
                    // 'valueColOptions'=>['style'=>'width:30%']
                    ],
                    'approval_comment:ntext',
                //'created_at',
                //'created_by',
                // 'is_canceled',
                //'cancel_comment:ntext',
                ],
            ])
            ?>
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <td colspan="2"><b>ALLOCATION BATCH SUMMARY</b></td>
                    </tr>
                    <tr>
                        <td><b>LOAN ITEM</b></td>
                        <td align='right'><b>AMOUNT</b></td>
                    </tr>
                </thead>
                <tbody>

                </tbody>

            </table>
        </div>
    </div>
</div>