<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use backend\modules\allocation\models\LearningInstitution;

//contained_student
$form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
echo Form::widget([ // fields with labels
    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [
        'contained_student' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Contained Student',
            'options' => [
                'data' => [1 => "New Awardee Student", 2 => "Continuing Awardee Student"],
                'options' => [
                    'prompt' => '',
                ],
            ],
        ],
    ]
]);

echo Form::widget([ // fields with labels
    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [
        'study_country' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Country Of Study',
            'options' => [
                'options' => ['multiple' => FALSE, 'placeholder' => 'Defaulty- All countries'],
                'data' => [\backend\modules\allocation\models\AllocationHistory::PLACE_TZ => 'Tanzania', \backend\modules\allocation\models\AllocationHistory::PLACE_FCOUNTRY => 'Others/Foreign Country'],
            ],
        ],
        'study_level' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Level of Study',
            'options' => [
                'options' => ['multiple' => YES, 'placeholder' => 'Default - All Levels'],
                'data' => yii\helpers\ArrayHelper::map(backend\modules\allocation\models\ApplicantCategory::find()->all(), 'applicant_category_id', 'applicant_category'),
            ],
        ],
        'institution' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Institution',
            'options' => [
                'options' => ['multiple' => true, 'placeholder' => 'Select Institution(s)'],
                'data' => yii\helpers\ArrayHelper::map(LearningInstitution::find()->where(["is_active" => LearningInstitution::STATUS_ACTIVE, 'institution_type' => LearningInstitution::INSTITUTION_TYPE_UNIVERSITY])->all(), 'learning_institution_id', 'institution_name'),
                'pluginOptions' => [
                    'placeholder' => 'Select ',
                ],
            ],
        ],
        'programme' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Programme',
            'options' => [
                'options' => ['multiple' => true, 'placeholder' => 'Select Programe(s)'],
                'data' => yii\helpers\ArrayHelper::map(backend\modules\allocation\models\Programme::find()->where(["is_active" => backend\modules\allocation\models\Programme::STATUS_ACTIVE])->all(), 'programme_id', 'programme_name'),
                'pluginOptions' => [
                    'placeholder' => 'Select ',
                ],
            ],
        ],
        'study_year' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Year of Study',
            'options' => [
                'options' => ['multiple' => true, 'placeholder' => 'Select Institution'],
                'data' => Yii::$app->params['programme_years_of_study'],
                'pluginOptions' => [
                    'placeholder' => 'Select ',
                ],
            ],
        ],
    ]
]);
echo Form::widget([ // fields with labels
    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [
        'batch_desc' => ['type' => Form::INPUT_TEXTAREA, 'label' => 'Reason(s) for Batch Creation', 'options' => ['placeholder' => 'Please Enter Reason(s) or Details for creating a special batch']],
    ]
]);
?>
<?= $form->field($model, 'academic_year_id')->label(FALSE)->hiddenInput(["value" => $model_year->academic_year_id]) ?>
<div class="text-right">
    <?= Html::submitButton($model->isNewRecord ? 'create Batch' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php
    echo Html::resetButton('Reset', ['class' => 'btn btn-default']);
    ?>
    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-warning']) ?>
    <?php ActiveForm::end(); ?>
</div>
