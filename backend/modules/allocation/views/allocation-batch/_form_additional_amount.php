<?php

//use yii\helpers\Html;
//use yii\widgets\ActiveForm;
//use wbraganca\dynamicform\DynamicFormWidget;

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="customer-form">
    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL, 'id' => 'dynamic-form']);
    ?>
    <?= $form->field($model, 'allocation_batch_id')->label(FALSE)->hiddenInput(["value" => $model->allocation_batch_id]) ?>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body" style="margin: 0;">
                <?php
                DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
                    //'limit' => 1, // the maximum times, an element can be cloned (default 999)
                    'min' => 1, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $model,
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'loan_item_id',
                        'unit_amount',
                        'duration'
                    ],
                ]);
                ?>


                <div class="container-items"><!-- widgetContainer -->
                    <?php //foreach ($ClusterProgramme as $i => $ClusterProgramme2): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">Programmes</h3>
                            <div class="pull-right">
                                <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-3">
                                <?php
                                echo Form::widget([ // fields with labels
                                    'model' => $allocation,
                                    'form' => $form,
                                    'columns' => 1,
                                    'attributes' => [
                                        "loan_item_id[]" => [
                                            'class' => "loan_item_id",
                                            'type' => Form::INPUT_DROPDOWN_LIST,
                                            'items' => ArrayHelper::map($loan_items, 'loan_item_id', 'loanItem.item_name'), 'options' => ['prompt' => '-- Select --'],
                                        ],
                                    ]
                                ]);
                                ?>
                            </div>                        
                            <div class="col-sm-3">                        
                                <?= $form->field($allocation, "unit_amount[]")->textInput(['prompt' => 'Enter Unit Amount Value'])->label('Unit Amount') ?>    
                            </div>
                            <div class="col-sm-3">                        
                                <?= $form->field($allocation, "duration[]")->textInput(['prompt' => 'Enter Unit Amount Value'])->label('Duration (Days/Year)') ?>    
                            </div>
                            <?php //endforeach; ?>
                        </div>
                        <?php DynamicFormWidget::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="text-right">        
        <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
        <?php
        echo Html::resetButton('Reset', ['class' => 'btn btn-default']);
        ?>
        <?= Html::a('Cancel', ['index', 'id' => $cluster_id], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
