<?php
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use kartik\file\FileInput;

//contained_student
$form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_VERTICAL,
    'options' => ['enctype' => 'multipart/form-data'],
    'enableClientValidation' => TRUE,
]);
 
echo $form->field($model, 'larc_approve_attachment')->widget(FileInput::classname(), [
    
    'options' => ['accept' => 'image/*'],
    'pluginOptions' => [
        'initialPreviewShowDelete' => true,
        'showCaption' => false,
        //'showRemove' => TRUE,
        'showUpload' => false,
        //'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="fa fa fa-file-pdf-o"></i> ',
        'browseLabel' =>  'Attach Approve Document (required format .pdf,.jpg and .png only)',
        'initialPreview'=>[
            "$model->larc_approve_attachment",
            
        ],
        'initialPreviewConfig' => [
            ['type'=> explode(".",$model->larc_approve_attachment)[1]=="pdf"?"pdf":"image"],
        ],
        'initialCaption'=>$model->larc_approve_attachment,
        'initialPreviewAsData'=>true,
        
    ]
]);
?>
 <div class="text-right">
<?= Html::submitButton($model->isNewRecord ? 'Approve Allocation Batch' : 'Approve Allocation Batch', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

<?php
echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
?>
      
 <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-warning']) ?>
      
 <?php ActiveForm::end(); ?>
    </div>
