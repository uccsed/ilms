 
<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use backend\modules\allocation\models\AllocationTask;
use common\models\AcademicYear;

/* @var $this yii\web\View */
/* @var $model frontend\modules\allocation\models\AllocationBatch */

//$this->title = $model->allocation_batch_id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Allocation Batch'), 'url' => ['/disbursement/default/allocation-batch']];
//$this->params['breadcrumbs'][] = $this->title;
$academic_year_id=AcademicYear::getLatestAcademicYear();
?>
<div class="allocation-batch-view">
 
    <div class="panel panel-info">
                        <div class="panel-heading">
                     
                        </div>
                        <div class="panel-body">
  
    <p>
        <?php 
        if($model->status==2&&$model->movement_type==1){
              ?>
     <?= Html::a('ED Partial Approve', ['allocation-batch-partial-approve', 'id' => $model->allocation_batch_id], [
            'class' => 'btn btn-primary',
            'data' => [
                'confirm' => 'Are you sure you want to maked as review this allocation batch?',
                'method' => 'post',
            ],
        ]);
    ?>
    <?= Html::a('LARC Approve', ['allocation-batch-larc-approve', 'id' => $model->allocation_batch_id], [
            'class' => 'btn btn-primary',
            'data' => [
                'confirm' => 'Are you sure you want to maked as review this allocation batch?',
                'method' => 'post',
            ],
        ]);
    ?>
    <?=Html::a('Submit Allocation Batch For Update from LARC', ['submit-allocation-batch', 'id' => $model->allocation_batch_id,'mt'=>2], [
             'class' => 'btn btn-success',
             'data' => [
             'confirm' => 'Are you sure you want to this allocation batch for changes?',
             'method' => 'post',
             ],
             ]);
           }
          else  if($model->is_reviewed==1&&$model->is_approved==0){
     
              
            }
            //find the structure of use /staff login  AND user_id='{$userId}'
            $userId=Yii::$app->user->identity->user_id;
            //$model_task=AllocationTask::find()->where(['status'=>1])->all();
            $sqlall = " SELECT * FROM allocation_user_structure aus 
                                join allocation_task_definition dd on dd.`allocation_task_id`=aus.`allocation_task_id` 
                                join allocation_structure als on als.allocation_structure_id=aus.allocation_structure_id  
                                join      allocation_movement_schedule ams on   
                                ams.`allocation_movement_schedule_id`=aus.`allocation_movement_schedule_id` AND user_id='{$userId}' AND academic_year_id='{$academic_year_id}' order by order_levels DESC";
            $model_task= Yii::$app->db->createCommand($sqlall)->queryAll();
          // print_r($model_task);
           // $level=0;
            if(count($model_task)>0){
                foreach ($model_task as $rows) {
                      //  $innercheck = \backend\modules\disbursement\models\PayoutlistMovement::findOne(['movement_status' => 0, 'to_officer' => $userId, 'disbursements_batch_id'=>$model->disbursement_batch_id]);
                    //inner check  
                      //  $level+=1;
                        $task_id=$rows["allocation_task_id"];
                        //   echo "SELECT * FROM `disbursement_payoutlist_movement`,`disbursement_user_structure`  WHERE `to_officer`=`disbursement_user_structure_id` AND movement_status=0 AND user_id='{$userId}' AND disbursements_batch_id='{$model->disbursement_batch_id}'";
                        $innercheck=Yii::$app->db->createCommand("SELECT * FROM `allocation_payoutlist_movement` WHERE  movement_status=1  AND allocation_batch_id='{$model->allocation_batch_id}' AND to_officer='{$userId}' AND allocation_next_task_id='{$task_id}'")->queryone();
                        if ($innercheck) {
                            $level=$rows["order_level"];
                           
                          //echo   $allocation_next_task_id=$innercheck['allocation_next_task_id'];
                            ?>
                          <?= Html::a($rows["accept_code"], ['review-decision', 'id' => $model->allocation_batch_id, 'level' =>$level,'task_id'=>$task_id,'mt'=>1], [
                                'class' => 'btn btn-success',
                                'data' => [
                                    'confirm' => 'Are you sure you want to Accept this Item?',
                                    'method' => 'post',
                                ],
                            ])
                            ?>
                           <?= Html::a($rows["reject_code"], ['reject-decision', 'id' => $model->allocation_batch_id, 'level' => $level,'task_id'=>$task_id,'mt'=>1], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to  Reject this Item?',
                                    'method' => 'post',
                                ],
                            ])
                            ?>
                          <?php
                             } else {
                                 if($model->status==0){
                            ?>
                            <?= Html::button($rows["accept_code"] . '(Disabled)', ['value' => "", 'title' => '', 'disabled' => 'disabled', 'class' => 'btn btn-success']); ?>
                            <?= Html::button($rows["reject_code"] . '(Disabled)', ['value' => "", 'title' => '', 'disabled' => 'disabled', 'class' => 'btn btn-danger']); ?>
                           
                        <?php 
                            }
                           }
                        ?>
                        <?php
                   
                }
              }  else {
              //echo "Mickidadi empty";    
             }
         
        //end
       ?>
     <?= Html::a('Download Allocation Batch', ['allocation/allocation-batch-download','allocation_batch_id'=>$model->allocation_batch_id], ['class' => 'btn btn-warning text-right','target'=>'_blank']) ?>
 
    </p>
    
    <?= DetailView::widget([
        'model' => $model,
         'condensed' => true,
        'hover' => true,
        'attributes' => [
            //'allocation_batch_id',
            //'batch_number',
            'batch_desc',
           // 'academic_year_id',
           // 'academicYear.academic_year', 
                       [
                        'attribute'=>'academic_year_id', 
                        'label'=>'Academic Year',
                        'format'=>'raw',
                        'value'=>call_user_func(function ($data){

                                           return $data->academicYear->academic_year;
                        },$model),
                       // 'valueColOptions'=>['style'=>'width:30%']
                       ],
           // 'available_budget',
           // 'is_approved',
                [
                        'attribute'=>'status', 
                        'label'=>'Approve Status',
                        'format'=>'raw',
                        'value'=>call_user_func(function ($data){
                        if($data->status==10){
                            $status='<label class="label label-success">Approved</label>';
                        }
                        else if($data->status==2){
                            $status='<label class="label label-danger">Disapproved</label>';
                        }
                        else{
                            $status='<label class="label label-info">In movement</label>';
                        }
                         return $status;
                        },$model),
                       // 'valueColOptions'=>['style'=>'width:30%']
                       ],
            'approval_comment:ntext',
            //'created_at',
            //'created_by',
           // 'is_canceled',
            //'cancel_comment:ntext',
        ],
    ]) ?>
  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <td colspan="2"><b>ALLOCATION BATCH SUMMARY</b></td>
                      </tr>
                      <tr>
                          <td><b>LOAN ITEM</b></td>
                        <td align='right'><b>AMOUNT</b></td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                       // print_r($dataProvider);
                              
                        $total=0;
                 $sql=  backend\modules\allocation\models\Allocation::find()->select("*,sum(allocated_amount) as amount,item_name")->joinWith("loanItem")->GroupBy("item_name")->where("allocation_batch_id=$model->allocation_batch_id")->asArray()->all();
                  if(count($sql)>0){
                 foreach ($sql as $rows){
                 echo "<tr>
                        <td>".$rows["item_name"]."</td>
                        <td align='right'>".number_format($rows["amount"])."</td>
                     
                      </tr>";
                 $total+=$rows["amount"];
                           }
               echo "<tr>
                        <td> </td>
                        <td align='right'><b>".number_format($total)."</b></td>
                     
                      </tr>";
                  }
                  else{
               echo "<tr><td colspan='2'><font color='red'>Sorry No results found</font></td></tr>";       
                  }
                  
                  function getstatus($order_level,$allocation_task_id){
                      $sqlall ="SELECT * FROM `allocation_task_assignment` dt join allocation_task_definition dd on dd.allocation_task_id=dt.`allocation_task_id` join allocation_user_structure aus on dt.allocation_user_structure_id=aus.allocation_user_structure_id join allocation_structure als on als.allocation_structure_id=aus.allocation_structure_id AND order_level<='{$order_level}' AND dt.allocation_task_id<>'{$allocation_task_id}'";
                      $model_task= Yii::$app->db->createCommand($sqlall)->queryOne();
                      
                      
                  }
                       ?>
                  
                    </tbody>
                    
                  </table>
</div>
    </div>
</div>