<?php

use yii\helpers\Html;
use kartik\tabs\TabsX;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Fixedassets */

$this->title = "Additional Allocation Batch";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Allocation Batch'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fixedassets-view">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?php
            echo TabsX::widget([
                'items' => [
                    [
                        'label' => 'Batch Details',
                        'content' => $this->render('_additionalBatch', ['model' => $model,]),
                        'id' => '1',
                    ],
                    [
                        'label' => 'List of Allocated Student(s) Details',
                        'content' => $this->render('_students', ['model' => $model, 'student_model' => $student_model,'dataProvider'=>$dataProvider]),
                        'id' => '2',
                    ],
                ],
                'position' => TabsX::POS_ABOVE,
                'bordered' => true,
                'encodeLabels' => false
            ]);
            ?>
        </div>

    </div>  
</div>