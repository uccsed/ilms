<?php

use yii\helpers\Html;

$model_year = common\models\AcademicYear::findone(["is_current" => 1]);
//print_r($model_year);
$this->title = Yii::t('app', 'Add  Additionals');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Allocation Batch'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allocation-batch-create">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) . " [ Additional Batch #" . $model->batch_number . " , Year : " . $model_year->academic_year . "]"; ?>
        </div>
        <?php
        if (Yii::$app->session->hasFlash('failure')) {
            echo '<p class="failure">' . Yii::$app->session->getFlash('failure') . '</p>';
        }
        ?>
        <div class="panel-body">
            <?=
            $this->render('_form_additional_amount', [
                'model' => $model, 'loan_items' => $loan_items, 'allocation' => $allocation
            ])
            ?>

        </div>
    </div>
</div>
