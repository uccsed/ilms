<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use backend\modules\allocation\models\AllocationPlanStudentLoanItemSearch;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\allocation\models\AllocationPlanStudentLoanItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Allocation Plan Student Loan Items';
//$this->params['breadcrumbs'][] = $this->title;
$searchModel = new AllocationPlanStudentLoanItemSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams,$model->allocation_plan_student_id);
?>
 
<div class="allocation-plan-student-loan-item-index">

 
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

           // 'allocation_plan_student_loan_item_id',
           // 'allocation_plan_student_id',
             'loanItem.item_name',
            // 'priority_order',
            // 'rate_type',
             //'unit_amount',
            [
                'attribute' => 'unit_amount',
                'hAlign'=>'right',
                'format'=>['decimal', 2],
                //'label'=>"Status",
                // 'width' => '200px',
            ],
             'duration',
             'loan_award_percentage',
            // 'total_amount_awarded',
            [
                'attribute' => 'total_amount_awarded',
                'hAlign'=>'right',
                'format'=>['decimal', 2],
                //'label'=>"Status",
                // 'width' => '200px',
            ],
            // 'is_additional',

           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
 
