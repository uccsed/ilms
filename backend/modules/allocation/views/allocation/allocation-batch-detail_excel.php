<?php
use backend\modules\allocation\models\Criteria;

ob_end_clean();
$objPHPExcel = new \PHPExcel();
$sheet=0;

$objPHPExcel->setActiveSheetIndex($sheet);

$objPHPExcel->setActiveSheetIndex(0)
->mergeCells('A1:G1')
->mergeCells('A2:G2')
->mergeCells('A3:G3')
->mergeCells('A4:H4')
->setCellValue('A2', '')
->setCellValue('A3', "")
->setCellValue('A4', "");

$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize('8');
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A5',"#")
->setCellValue('B5',"F4IndexNo")
->setCellValue('C5',"Name")
->setCellValue('D5',"Gender")
->setCellValue('E5',"RegNo")
->setCellValue('F5',"YoS")
->setCellValue('G5',"Institution")
->setCellValue('H5',"Inst Type")
->setCellValue('I5',"Programme Code")
->setCellValue('J5',"Cluster");

//find data
//create social economic factors
$model_neediness=Yii::$app->db->createCommand("SELECT criteria.`criteria_id`, `criteria_description` FROM `criteria`  join criteria_field on criteria_field.criteria_id=criteria.criteria_id WHERE `type`=2 group by criteria.`criteria_id`")->queryAll();

$col=74;
$index=0;
$criteria_Array=array();
if(count($model_neediness)>0){
  //  $cluster_Array=array();
    foreach ($model_neediness as $models_d) {
        $col++;
        //echo $models_d['criteria_description'];
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col)."5","{$models_d['criteria_description']}");
        $criteria_Array[$models_d['criteria_id']]=$models_d['criteria_id'];
        //$index++;
    }
}
//print_r($criteria_Array);
//exit();
//end 
$col++;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col)."5","Fee");
$col++;
$objPHPExcel->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col)."5","Fee Factor");
$col++;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col)."5","My Factor");
$col++;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col)."5","Programme Cost");
$col++;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col)."5","Ability");
$col++;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col)."5","Neediness".chr($col));
//$col++;
//print_r($criteria_Array);
//exit();
//create social economic factors
//$col=68;
//create social economic factors
/*$model_loan_item=Yii::$app->db->createCommand("SELECT * FROM loan_item join loan_item_detail on loan_item_detail.loan_item_id=loan_item.loan_item_id where  study_level=1 AND loan_item_category='normal' group by loan_item.loan_item_id")->queryAll();
$index=0;
if(count($model_loan_item)>0){
 $loan_item_Array=array();
 foreach ($model_loan_item as $model_loan_items) {
 $col++;
 $objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col)."5","{$model_loan_items['item_code']}");
 $loan_item_Array[$model_loan_items['loan_item_id']]=$model_loan_items['loan_item_id'];
 //$index++;
 }
 }*/
 $col++;
//end 
$objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col)."5","Total Amount");
$objPHPExcel->getActiveSheet()->getStyle('A5:'.chr($col)."5")->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('FF999999');
$objPHPExcel->getActiveSheet()->getStyle('A6:'.chr($col)."6")->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('FF999999');
$numbers=0;
$row=6;
//print_r($modelall);
 //exit();
//$sqlstudent=Yii::$app->db->createCommand("SELECT distinct sc.regNo as regNo ,s.Id as Id from student s, studentcourse sc,studentprogram st where  st.regNo=sc.regNo AND sc.regNo=s.regNo AND  st.regNo=s.regNo AND programCode='{$level}' AND st.year='{$year}' AND sc.schoolId='{$schoolId}' AND semester=$terms")->queryAll();
  if(count($modelall)>0){
    foreach ($modelall as $models) {
        $name_fully=$models["firstname"]." ".$models["middlename"]." ".$models["surname"];
        $f4indexno=$models["f4indexno"];
        $gender=$models["gender"];
        $regNo=$models["registration_number"];
        $yos=$models["registration_number"];
        $institution=$models["institution_name"];
        $type=$models["ownership"]==0?'Government':'Private';
        $programme_code=$models["programme_code"];
        $cluster= $models['cluster_name'];
        $row++;
        $numbers++;
        
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A".$row,"{$numbers}")
        ->setCellValue("B".$row,"{$f4indexno}")
        ->setCellValue("C".$row,"{$name_fully}")
        ->setCellValue('D'.$row,"{$gender}")
        ->setCellValue('E'.$row,"{$regNo}")
        ->setCellValue('F'.$row,"{$yos}")
        ->setCellValue('G'.$row,"{$institution}")
        ->setCellValue('H'.$row,"{$type}")
        ->setCellValue('I'.$row,"{$programme_code}")
        ->setCellValue('J'.$row,"{$cluster}");
        // }
        $col=74;
       //social economic factors
       // $model_loan_item=Yii::$app->db->createCommand("")->queryAll();
        $index=0;
        $application_id=$models["application_id"];
        if(count($criteria_Array)>0){
            //$loan_item_Array=array();
            foreach ($criteria_Array as $criterias=>$value) {
                $col++;
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col).$row,getCriteria($application_id,$value));
              //  $loan_item_Array[$model_loan_items['loan_item_id']]=$model_loan_items['loan_item_id'];
                //$index++;
            }
        }
        $col++;
        
       //end 
        $student_fee=$models['student_fee'];
        $programme_cost=$models['programme_cost'];
        $myfactor=$models['myfactor'];
        $fee_factor=$models['fee_factor'];
        $ability=$models['ability'];
        $neediness=$models['needness'];
        $col++;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col).$row,$student_fee);
        $col++;
        $objPHPExcel->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col).$row,$fee_factor);
        $col++;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col).$row,$myfactor);
        $col++;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col).$row,$programme_cost);
        $col++;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col).$row,$ability);
        $col++;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col).$row,$neediness);
        //$col++;
        //create social economic factors
        //$col=68;
        //create social economic factors
       /* $model_loan_item=Yii::$app->db->createCommand("SELECT * FROM loan_item join loan_item_detail on loan_item_detail.loan_item_id=loan_item.loan_item_id where  study_level=1 AND loan_item_category='normal' group by loan_item.loan_item_id")->queryAll();
        $index=0;
        $application_id=$models["application_id"];
        if(count($model_loan_item)>0){
            $loan_item_Array=array();
            foreach ($model_loan_item as $model_loan_items) {
                $col++;
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col).$row,getLoanItemAmount($application_id,$model_loan_items["loan_item_id"],$models["allocation_batch_id"]));
                $loan_item_Array[$model_loan_items['loan_item_id']]=$model_loan_items['loan_item_id'];
                //$index++;
            }
        }
        $col++;*/
        //end
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr($col).$row,number_format($models["amount_total"]));
        $objPHPExcel->getActiveSheet()->getStyle('A5:'.chr($col)."5")->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FF999999');
        $objPHPExcel->getActiveSheet()->getStyle('A6:'.chr($col)."6")->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FF999999');
    }
}
header('Content-Type: application/vnd.ms-excel');
$filename = "consolidatesheet_".date("d-m-Y-His").".xls";
header('Content-Disposition: attachment;filename='.$filename .' ');
header('Cache-Control: max-age=0');
$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit();
################################################
#  Find class position of the student
#
 
##############################################
#### Find Marks Grade
####
 
ob_end_clean() ;
function getLoanItemAmount($application_id,$loan_item_id,$allocation_batch_id){
    $amount=0;
    $model=  backend\modules\allocation\models\Allocation::find()->where(["application_id"=>$application_id,'loan_item_id'=>$loan_item_id,'allocation_batch_id'=>$allocation_batch_id])->sum("allocated_amount");
    
    if($model>0){
        $amount=$model;
    }
    return $amount;
}
function getProgrammeCode($programme_code){
    $model= Yii::$app->db->createCommand("SELECT `group_code` FROM `programme` join programme_group on programme.programme_group_id=programme_group.programme_group_id  WHERE programme_code='{$programme_code}' limit 1")->queryOne();
    if(count($model)>0){
        return $model->group_code;
    }
 
}

function getCriteria($application_id,$criteria_id){
    $value=0;
    $model=Yii::$app->db->createCommand("SELECT `criteria_id`,`weight_points`,cf.`criteria_field_id` FROM `criteria_field` cf join `applicant_criteria_score` app on cf.`criteria_field_id`=app.`criteria_field_id` WHERE application_id='{$application_id}' AND cf.`criteria_id`='{$criteria_id}'  GROUP by criteria_id ORDER by criteria_id")->queryOne();
    if($model>0){
        $value=$model["weight_points"];
    }
    return $value;
}
?>
