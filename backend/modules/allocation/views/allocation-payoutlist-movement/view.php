<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationPayoutlistMovement */

$this->title = $model->movement_id;
$this->params['breadcrumbs'][] = ['label' => 'Allocation Payoutlist Movement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allocation-payoutlist-movement-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Allocation Payoutlist Movement'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->movement_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->movement_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->movement_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'movement_id',
        [
            'attribute' => 'allocationBatch.allocation_batch_id',
            'label' => 'Allocation Batch',
        ],
        [
            'attribute' => 'fromOfficer.username',
            'label' => 'From Officer',
        ],
        [
            'attribute' => 'toOfficer.username',
            'label' => 'To Officer',
        ],
        'movement_status',
        [
            'attribute' => 'allocationTask.allocation_task_id',
            'label' => 'Allocation Task',
        ],
        'signature',
        'date_out',
        'comment:ntext',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>AllocationBatch<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnAllocationBatch = [
        'batch_number',
        'batch_desc',
        'academic_year_id',
        'available_budget',
        'contained_student',
        'is_approved',
        'approved_at',
        'approved_by',
        'is_reviewed',
        'review_at',
        'review_by',
        'approval_comment',
        'is_canceled',
        'cancel_comment',
        'disburse_status',
        'disburse_comment',
    ];
    echo DetailView::widget([
        'model' => $model->allocationBatch,
        'attributes' => $gridColumnAllocationBatch    ]);
    ?>
    <div class="row">
        <h4>AllocationTaskDefinition<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnAllocationTaskDefinition = [
        'task_name',
        'accept_code',
        'reject_code',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->allocationTask,
        'attributes' => $gridColumnAllocationTaskDefinition    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnUser = [
        'user_id',
        'firstname',
        'middlename',
        'surname',
        'sex',
        'username',
        'password_hash',
        'password_hash1',
        'security_question_id',
        'security_answer',
        'email_address',
        'passport_photo',
        'phone_number',
        'is_default_password',
        'status',
        'status_comment',
        'login_counts',
        'last_login_date',
        'date_password_changed',
        'auth_key',
        'password_reset_token',
        'activation_email_sent',
        'email_verified',
        'login_type',
    ];
    echo DetailView::widget([
        'model' => $model->fromOfficer,
        'attributes' => $gridColumnUser    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnUser = [
        'user_id',
        'firstname',
        'middlename',
        'surname',
        'sex',
        'username',
        'password_hash',
        'password_hash1',
        'security_question_id',
        'security_answer',
        'email_address',
        'passport_photo',
        'phone_number',
        'is_default_password',
        'status',
        'status_comment',
        'login_counts',
        'last_login_date',
        'date_password_changed',
        'auth_key',
        'password_reset_token',
        'activation_email_sent',
        'email_verified',
        'login_type',
    ];
    echo DetailView::widget([
        'model' => $model->toOfficer,
        'attributes' => $gridColumnUser    ]);
    ?>
</div>
