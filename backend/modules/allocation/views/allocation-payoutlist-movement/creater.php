<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationPayoutlistMovement */

$this->title = ' Allocation Movement';
$this->params['breadcrumbs'][] = ['label' => 'Allocation Movement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allocation-payoutlist-movement-create">
<div class="panel panel-info">
        <div class="panel-heading">
       <?= Html::encode($this->title); ?>
        </div>
        <div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
        'level'=>$level,
        'task_id'=>$task_id,
        'allocation_batch_id'=>$allocation_batch_id
    ]) ?>

</div>
</div>
</div>