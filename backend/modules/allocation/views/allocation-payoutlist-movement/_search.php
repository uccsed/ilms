<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationPayoutlistMovementSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-allocation-payoutlist-movement-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'movement_id')->textInput(['placeholder' => 'Movement']) ?>

    <?= $form->field($model, 'allocation_batch_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\AllocationBatch::find()->orderBy('allocation_batch_id')->asArray()->all(), 'allocation_batch_id', 'allocation_batch_id'),
        'options' => ['placeholder' => 'Choose Allocation batch'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'from_officer')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\User::find()->orderBy('user_id')->asArray()->all(), 'user_id', 'username'),
        'options' => ['placeholder' => 'Choose User'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'to_officer')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\User::find()->orderBy('user_id')->asArray()->all(), 'user_id', 'username'),
        'options' => ['placeholder' => 'Choose User'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'movement_status')->textInput(['placeholder' => 'Movement Status']) ?>

    <?php /* echo $form->field($model, 'allocation_task_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\AllocationTaskDefinition::find()->orderBy('allocation_task_id')->asArray()->all(), 'allocation_task_id', 'allocation_task_id'),
        'options' => ['placeholder' => 'Choose Allocation task definition'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'signature')->textInput(['maxlength' => true, 'placeholder' => 'Signature']) */ ?>

    <?php /* echo $form->field($model, 'date_out')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Date Out',
                'autoclose' => true
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'comment')->textarea(['rows' => 6]) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
