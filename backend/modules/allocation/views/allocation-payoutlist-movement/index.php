<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\allocation\models\AllocationPayoutlistMovementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use common\models\User;

$this->title = 'Allocation Payoutlist Movement';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="allocation-payoutlist-movement-index">
   <div class="programme-index">
        <div class="panel panel-info">

            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
      //  'movement_id',
        [
                'attribute' => 'allocation_batch_id',
                'label' => 'Allocation Batch',
                'value' => function($model){                   
                    return $model->allocationBatch->allocation_batch_id;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\AllocationBatch::find()->asArray()->all(), 'allocation_batch_id', 'allocation_batch_id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Allocation batch', 'id' => 'grid-allocation-payoutlist-movement-search-allocation_batch_id']
            ],
        [
                'attribute' => 'from_officer',
                'label' => 'From Officer',
                'value' => function($model){                   
                    return $model->fromOfficer->username;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' =>  \yii\helpers\ArrayHelper::map(User::find()->where(['login_type'=>5])->orderBy('user_id')->asArray()->all(), 'user_id', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Staff ', 'id' => 'grid-allocation-payoutlist-movement-search-from_officer']
            ],
        [
                'attribute' => 'to_officer',
                'label' => 'To Officer',
                'value' => function($model){                   
                    return $model->toOfficer->username;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' =>  \yii\helpers\ArrayHelper::map(User::find()->where(['login_type'=>5])->orderBy('user_id')->asArray()->all(), 'user_id', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Staff', 'id' => 'grid-allocation-payoutlist-movement-search-to_officer']
            ],
       
        [
                'attribute' => 'allocation_task_id',
                'label' => 'Allocation Task',
                'value' => function($model){
                    if ($model->allocationTask)
                    {return $model->allocationTask->task_name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\AllocationTask::find()->where(['status'=>1])->asArray()->all(), 'allocation_task_id', 'task_name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Allocation Task', 'id' => 'grid-allocation-payoutlist-movement-search-allocation_task_id']
            ],
       // 'signature',
        'date_out',
        'comment:ntext',
       // 'movement_status',
        [
            'attribute' => 'movement_status',
            'label' => 'Movement Status',
            'value' => function($model){
            if ($model->movement_status)
            {
                if($model->movement_status==1){
                    $status='Pending' ; 
                }
                else if($model->movement_status==2){
                  
                    $status='In movement' ; 
                }
                else if($model->movement_status==3){
                    $status='Complete' ; 
                }
          return $status;
            }
            else
            {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' =>[2=>'In movement',1=>'Pending',3=>'Complete'],
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Movement Status', 'id' => 'grid-allocation-payoutlist-movement-search-allocation_movement_status_id']
            ],
       /* [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],*/
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-allocation-payoutlist-movement']],
    
    ]); ?>

</div>
</div>
</div>
