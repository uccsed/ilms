<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationPayoutlistMovement */
/* @var $form yii\widgets\ActiveForm */
//$model->allocation_batch_id=$allocation_batch_id;
//$model->movement_status=43;
?>

<div class="allocation-payoutlist-movement-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'allocation_batch_id')->label(false)->hiddenInput(['maxlength' => true, 'placeholder' => ' ','value'=>$allocation_batch_id]) ?>
    <?= $form->field($model, 'from_officer')->label(FALSE)->hiddenInput(['maxlength' => true, 'placeholder' => ' ','value'=>\Yii::$app->user->identity->user_id]) ?>
    <?= $form->field($model, 'to_officer')->label(FALSE)->hiddenInput(['maxlength' => true, 'placeholder' => ' ']) ?>
    <?= $form->field($model, 'order_level')->label(FALSE)->hiddenInput(['maxlength' => true, 'placeholder' => ' ','value'=>$level]) ?>
    <?= $form->field($model, 'allocation_task_id')->label(false)->hiddenInput(['maxlength' => true, 'placeholder' => ' ','value'=>$task_id]) ?>
    <?= $form->field($model, 'allocation_movement_id')->label(false)->hiddenInput(['maxlength' => true, 'placeholder' => ' ','value'=>$allocation_movement_id]) ?>
    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
    <div class="form-group">
    
        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Submit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
   
     
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
