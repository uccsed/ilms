<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationPayoutlistMovement */

$this->title = 'Save As New Allocation Payoutlist Movement: '. ' ' . $model->movement_id;
$this->params['breadcrumbs'][] = ['label' => 'Allocation Payoutlist Movement', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->movement_id, 'url' => ['view', 'id' => $model->movement_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="allocation-payoutlist-movement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
