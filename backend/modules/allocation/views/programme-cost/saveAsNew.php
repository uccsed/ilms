<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\ProgrammeCost */

$this->title = 'Save As New Programme Cost: '. ' ' . $model->programme_cost_id;
$this->params['breadcrumbs'][] = ['label' => 'Programme Cost', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->programme_cost_id, 'url' => ['view', 'id' => $model->programme_cost_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="programme-cost-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
