<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\ProgrammeCostSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-programme-cost-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'programme_cost_id')->textInput(['placeholder' => 'Programme Cost']) ?>

    <?= $form->field($model, 'academic_year_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\AcademicYear::find()->orderBy('academic_year_id')->asArray()->all(), 'academic_year_id', 'academic_year'),
        'options' => ['placeholder' => 'Choose Academic year'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'programme_id')->textInput(['placeholder' => 'Programme']) ?>

    <?= $form->field($model, 'programme_type')->textInput(['placeholder' => 'Programme Type']) ?>

    <?= $form->field($model, 'loan_item_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->orderBy('loan_item_id')->asArray()->all(), 'loan_item_id', 'loan_item_id'),
        'options' => ['placeholder' => 'Choose Loan item'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?php /* echo $form->field($model, 'rate_type')->checkbox() */ ?>

    <?php /* echo $form->field($model, 'unit_amount')->textInput(['placeholder' => 'Unit Amount']) */ ?>

    <?php /* echo $form->field($model, 'duration')->textInput(['placeholder' => 'Duration']) */ ?>

    <?php /* echo $form->field($model, 'year_of_study')->textInput(['placeholder' => 'Year Of Study']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
