<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\ProgrammeCost */

$this->title = 'Update Programme Cost: ';
$this->params['breadcrumbs'][] = ['label' => 'Programme Cost', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->programme_cost_id, 'url' => ['view', 'id' => $model->programme_cost_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="programme-cost-update">
 <div class="panel panel-info">
        <div class="panel-heading">
<?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>