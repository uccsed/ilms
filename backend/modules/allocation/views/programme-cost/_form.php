<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\ProgrammeCost */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="programme-cost-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>
 
    <?= $form->field($model, 'academic_year_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\AcademicYear::find()->orderBy('academic_year_id')->asArray()->all(), 'academic_year_id', 'academic_year'),
        'options' => ['placeholder' => 'Choose Academic year'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
  
    <?= $form->field($model, 'loan_item_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->orderBy('loan_item_id')->asArray()->all(), 'loan_item_id', 'item_name'),
        'options' => ['placeholder' => 'Choose Loan item'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'year_of_study')->widget(\kartik\widgets\Select2::classname(), [
        'data' =>[1=>1,2=>2,3=>3,4=>4,5=>5,6=>6],
        'options' => ['placeholder' => 'Choose Study Year'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
                   <?= $form->field($model, "rate_type")->dropDownList(\backend\modules\allocation\models\LoanItem::getItemRates(), ['prompt' => '--select--']); ?>
    <?= $form->field($model, 'unit_amount')->textInput(['placeholder' => 'Unit Amount']) ?>

    <?= $form->field($model, 'duration')->textInput(['placeholder' => 'Duration']) ?>

   
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
