<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\allocation\models\ProgrammeCostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Programme Cost';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="programme-cost-index">
 <div class="panel panel-info">
        <div class="panel-heading">
<?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
      <?= Html::a('Add Programme Cost', ['/allocation/programme/cost','id'=>$programme_id], ['class' => 'btn btn-success']) ?>
    </p>
     
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        //'programme_cost_id',
        [
                'attribute' => 'academic_year_id',
                'label' => 'Academic Year',
                'value' => function($model){                   
                    return $model->academicYear->academic_year;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\AcademicYear::find()->asArray()->all(), 'academic_year_id', 'academic_year'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Academic year', 'id' => 'grid-programme-cost-search-academic_year_id']
            ],
        //'programme_id',
       // 'programme_type',
        [
                'attribute' => 'loan_item_id',
                'label' => 'Loan Item',
                'value' => function($model){                   
                    return $model->loanItem->item_name;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->asArray()->all(), 'loan_item_id', 'item_name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Loan item', 'id' => 'grid-programme-cost-search-loan_item_id']
            ],
       // 'rate_type',
       // 'unit_amount',
         [
            'attribute' => 'unit_amount',
            'hAlign'=>'right',
            'format'=>['decimal', 2],
            //'label'=>"Status",
            // 'width' => '200px',
        ],
        'duration',
        'year_of_study',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-programme-cost']],
       
        'export' => false,
        // your toolbar can include the additional full export menu
        
    ]); ?>

</div>
</div>
</div>
