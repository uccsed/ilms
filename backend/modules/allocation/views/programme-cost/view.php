<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\ProgrammeCost */

$this->title = $model->programme_cost_id;
$this->params['breadcrumbs'][] = ['label' => 'Programme Cost', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="programme-cost-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Programme Cost'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->programme_cost_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->programme_cost_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->programme_cost_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'programme_cost_id',
        [
            'attribute' => 'academicYear.academic_year',
            'label' => 'Academic Year',
        ],
        'programme_id',
        'programme_type',
        [
            'attribute' => 'loanItem.loan_item_id',
            'label' => 'Loan Item',
        ],
        'rate_type',
        'unit_amount',
        'duration',
        'year_of_study',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>AcademicYear<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnAcademicYear = [
        'academic_year',
        'is_current',
        'closed_date',
        'closed_by',
    ];
    echo DetailView::widget([
        'model' => $model->academicYear,
        'attributes' => $gridColumnAcademicYear    ]);
    ?>
    <div class="row">
        <h4>LoanItem<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnLoanItem = [
        'item_name',
        'item_code',
        'is_active',
    ];
    echo DetailView::widget([
        'model' => $model->loanItem,
        'attributes' => $gridColumnLoanItem    ]);
    ?>
</div>
