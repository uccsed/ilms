<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationUserStructure */

$this->title ='Allocation Use Details';
$this->params['breadcrumbs'][] = ['label' => 'Allocation User Structure', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allocation-user-structure-view">
<div class="panel panel-info">
        <div class="panel-heading">
       <?= Html::encode($this->title); ?>
        </div>
        <div class="panel-body">
           <p>
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->allocation_user_structure_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->allocation_user_structure_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->allocation_user_structure_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
          </p>
<?php 
    $gridColumn = [
      //  'allocation_user_structure_id',
        [
            'attribute' => 'user.username',
            'label' => 'Full Name',
        ],
        [
            'attribute' => 'allocationStructure.structure_name',
            'label' => 'Allocation Structure',
        ],
       
       // 'user_id',
      //  'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
  
<?php
if($providerAllocationTaskAssignment->totalCount){
    $gridColumnAllocationTaskAssignment = [
        ['class' => 'yii\grid\SerialColumn'],
            //'allocation_task_assignment_id',
                        [
                'attribute' => 'allocationTask.task_name',
                'label' => 'Allocation Task'
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAllocationTaskAssignment,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-allocation-task-assignment']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Allocation Task Assignment'),
        ],
        'export' => false,
        'columns' => $gridColumnAllocationTaskAssignment
    ]);
}
?>

   
