<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\AllocationUserStructure */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'AllocationTaskAssignment', 
        'relID' => 'allocation-task-assignment', 
        'value' => \yii\helpers\Json::encode($model->allocationTaskAssignments),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="allocation-user-structure-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>
   
     <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(User::find()->where(['login_type'=>5])->orderBy('user_id')->asArray()->all(), 'user_id', 'username'),
        'options' => ['placeholder' => 'Choose Allocation Staff'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'allocation_structure_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\modules\allocation\models\AllocationStructure::find()->orderBy('allocation_structure_id')->asArray()->all(), 'allocation_structure_id', 'structure_name'),
        'options' => ['placeholder' => 'Choose Allocation structure'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
  <?= $form->field($model, 'status')->label(false)->dropDownList(
                                [0=>"In Active",1=>'Active'], 
                                [
                                'prompt'=>'[--Select Status--]',
                                ]
                    ) ?> 
    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('AllocationTaskAssignment'),
            'content' => $this->render('_formAllocationTaskAssignment', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->allocationTaskAssignments),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
