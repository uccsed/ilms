<?php

namespace backend\modules\allocation\controllers;

use Yii;
use backend\modules\allocation\models\AllocationBatch;
use backend\modules\allocation\models\AllocationBatchSearch;
use backend\modules\allocation\models\Allocation;
use backend\modules\allocation\models\AllocationBatchHistory;
//use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Controller;
use backend\modules\allocation\models\AllocationPayoutlistMovement;
use common\models\AcademicYear;
use yii\web\UploadedFile;

/**
 * AllocationBatchController implements the CRUD actions for AllocationBatch model.
 */
class AllocationBatchController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        $this->layout = "main_private";
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AllocationBatch models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new AllocationBatchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AllocationBatch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

        return $this->render('profile', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AllocationBatch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $max_data_processing = 1000;
        $start_record = 0;
        $model = new AllocationBatch();
        $modelh = new AllocationBatchHistory();
        if ($model->load(Yii::$app->request->post()) && $modelh->load(Yii::$app->request->post())) {
            ##############allocation batch generation ###################

            $model->save();
            //create batch number 
            $modelbatch = AllocationBatch::find()->orderBy('batch_number DESC')->where("academic_year_id=$model->academic_year_id AND allocation_batch_id<=$model->allocation_batch_id")->one();
            if (count($modelbatch) > 0) {

                $modelbatch_number = 1 + $modelbatch->batch_number;
            } else {
                $modelbatch_number = 1;
            }
            $modelupdate = AllocationBatch::findone($model->allocation_batch_id);
            $modelupdate->batch_number = $modelbatch_number;
            $modelupdate->batch_desc = $modelupdate->academicYear->academic_year . '-BATCH-' . $modelbatch_number;
            $modelupdate->save();
            //end create batch number
            if (count($modelh) > 0) {
                foreach ($modelh->allocation_history_id as $modelhs => $value) {
                    $modelh1 = new AllocationBatchHistory();
                    $modelh1->allocation_batch_id = $model->allocation_batch_id;
                    $modelh1->allocation_history_id = $value;
                    $modelh1->save();
                }
            }
            #####################put student in a allocation batch ###############
            $allocation_history_id = implode(',', $modelh->allocation_history_id);
            ##################count applicant data ######################################
            $model_student_batch_count = AllocationBatch::countAllocationStudentHistory($allocation_history_id, $model->academic_year_id);
            ####################end count ##########################
            if ($model_student_batch_count > $max_data_processing) {
                $index_max_limit = ceil(($model_student_batch_count / $max_data_processing));
                $start_record = 0;
                $end_record = $max_data_processing;
            } else {
                $index_max_limit = 1;
                $start_record = 0;
                $end_record = $model_count;
            }
            // for ($i = 1; $i <= $index_max_limit; $i++) {
            $model_student_batch = AllocationBatch::putAllocationStudentHistoryBatch($allocation_history_id, $model->academic_year_id);
            if (count($model_student_batch) > 0) {
                foreach ($model_student_batch as $model_student_batchs) {
                    $model_allocation = new Allocation();
                    $model_allocation->allocation_batch_id = $model->allocation_batch_id;
                    $model_allocation->application_id = $model_student_batchs["application_id"];
                    $model_allocation->loan_item_id = $model_student_batchs["loan_item_id"];
                    $model_allocation->allocated_amount = $model_student_batchs["total_amount_awarded"];
                    $model_allocation->save();
                    ////HERE  ADD THIS LINE
                    if ($model_allocation->save()) {
                        //UPDATING THE ALLOCATION BATH STUDENT TABLE
                        \backend\modules\allocation\models\AllocationBatchStudent::CreateAllocationBatchStudent($model_allocation, $model_student_batchs);
                    }
                    ///END HERE
                }

                $start_record = $end_record + 1;
                $end_record = $end_record + $max_data_processing;
            }
            ######################## End process @@@@############################
            // }
            ##################### end ###################################
            return $this->redirect(['view', 'id' => $model->allocation_batch_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'modelh' => $modelh,
            ]);
        }
    }

//    public function actionCreate() {
//en
//        $model = new AllocationBatch();
//
//        if ($model->load(Yii::$app->request->post())) {
//            /* get list of student with status 0f 6 and neednes is
//             *  equal of greater than min loan amount
//             */
//            $loan_min = \backend\models\SystemSetting::findOne(["is_active" => 1, 'setting_code' => 'MLA']);
//            /*
//             * check if new application 
//             */
//            if ($model->contained_student == 1) {
//                if (count($loan_min) > 0) {
//                    $totalbudget = 0;
//                    $totalbudget = $model->available_budget;
//                    //find the allocation priority
////                $academic_year = 0;
////                $modelacademicyear = \common\models\AcademicYear::findOne(['is_current' => 1]);
////                if (count($modelacademicyear) > 0) {
////                    $academic_year = $modelacademicyear->academic_year;
////                }
////                $model_allocationpriority = \backend\modules\allocation\models\base\AllocationPriority::find()->joinWith("sourceTable")->where(['academic_year_id' =>$model->academic_year_id])->orderBy(['priority_order' => SORT_ASC])->all();
////              //  print_r($model_allocationpriority);
//                    $model_allocationpriority = Yii::$app->db->createCommand("SELECT * FROM `allocation_priority` ap join `source_table` st on `source_table`=`source_table_id` WHERE academic_year_id ='{$model->academic_year_id}'")->queryAll();
//                    foreach ($model_allocationpriority as $rows) {
//                        /*
//                         * source of data criteria question
//                         */
//                        $sql_sub = " " . $rows["source_table_id_field"] . "=" . $rows["field_value"];
//                        // echo "SELECT * FROM `application` a join `applicant_criteria_score` ac on a.`application_id`=ac.`application_id` join criteria_question cq on ac.`criteria_question_id`=cq.`criteria_question_id` join cluster_programme cp on cp.`programme_id`=a.`programme_id` WHERE  $sql_sub order by ability";
//                        $sqlall = Yii::$app->db->createCommand("SELECT * FROM `application` a join `applicant_criteria_score` ac on a.`application_id`=ac.`application_id` join criteria_question cq on ac.`criteria_question_id`=cq.`criteria_question_id` join cluster_programme cp on cp.`programme_id`=a.`programme_id` WHERE  $sql_sub order by ability")->queryAll();
//                        //end
//                        /*
//                         * end
//                         * source of data criteria field
//                         */
//                        echo "SELECT * FROM `application` a join `applicant_criteria_score` ac on a.`application_id`=ac.`application_id` join criteria_field cf on ac.`criteria_field_id`=cf.`criteria_field_id` join cluster_programme cp on cp.`programme_id`=a.`programme_id` WHERE $sql_sub order by ability";
//                        $sqlall2 = Yii::$app->db->createCommand("SELECT * FROM `application` a join `applicant_criteria_score` ac on a.`application_id`=ac.`application_id` join criteria_field cf on ac.`criteria_field_id`=cf.`criteria_field_id` join cluster_programme cp on cp.`programme_id`=a.`programme_id` WHERE $sql_sub order by ability")->queryAll();
//                        /*
//                         * end
//                         */
//                    }
//                    //exit();
//                    if (count($model_allocationpriority) > 0) {
//                        //get 
//                        $sql = " SELECT * FROM `application` a join  cluster_programme cp    on a.`programme_id`=cp.`programme_id` WHERE allocation_status=5 AND needness>='{$loan_min->setting_value}' order by ability ";
//                        $sqldata = Yii::$app->db->createCommand($sql)->queryAll();
//
//                        $modelall = \backend\modules\application\models\Application::find()->where("allocation_status=5 AND needness>='{$loan_min->setting_value}'")->orderBy(['ability' => SORT_DESC])->asArray()->all();
//                        // print_r($modelall);
//                    } else {
//                        /* allocation process normally
//                         * user neednesd to allocate loan
//                         */
//
//                        /*
//                         * end allocation process
//                         */
//                    }
//                    $loan_mins = $loan_min->setting_value;
//                    if (count($modelall) > 0) {
//                        foreach ($modelall as $rows) {
//                            $needness = $rows["needness"];
//                            if ($needness >= $loan_mins && $totalbudget >= $needness) {
//                                $status = 3;
//                            } else {
//                                $status = 4;
//                            }
//                        }
//                    }
//                }
//            } else {
//                ///award loan for Loanee student or continou student 
//                //
//
//                //end              
//            }
//            // return $this->redirect(['view', 'id' => $model->allocation_batch_id]);
//        } else {
//            return $this->render('create', [
//                        'model' => $model,
//            ]);
//        }
//    }

    /**
     * Updates an existing AllocationBatch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->allocation_batch_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AllocationBatch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        if ($model) {
            $batch_student = \backend\modules\allocation\models\AllocationBatchStudent::deleteAll(['allocation_batch_id' => $model->allocation_batch_id]);
            $student_allocation = \backend\modules\allocation\models\Allocation::deleteAll(['allocation_batch_id' => $model->allocation_batch_id]);
            if ($batch_student && $student_allocation) {
                $this->findModel($id)->delete();
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the AllocationBatch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AllocationBatch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AllocationBatch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAwardLoan() {

        return $this->render('profileaward', [
                    'model' => \common\models\AcademicYear::findOne(['is_current' => 1]),
        ]);
    }

    public function actionAwardLoanfresher($status) {

        $model = new AllocationBatch();

        // if ($model->load(Yii::$app->request->post())) {
        /* get list of student with status 0f 6 and neednes is
         *  equal of greater than min loan amount
         */
        $loan_min = \backend\models\SystemSetting::findOne(["is_active" => 1, 'setting_code' => 'MLA']);
        /*
         * check if new application 
         */
        $totalbudget = 0;
        if ($status == 1) {
            if (count($loan_min) > 0) {

                $totalbudget = 60000000;
                //find the allocation priority
                $academic_year = 0;
                $modelacademicyear = \common\models\AcademicYear::findOne(['is_current' => 1]);
                if (count($modelacademicyear) > 0) {
                    $academic_year = $modelacademicyear->academic_year;
                }

                /*
                 * check allocation priority
                 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                 */
                $model_allocationpriority = Yii::$app->db->createCommand("SELECT * FROM `allocation_priority` ap "
                                . " join `source_table` st "
                                . " on `source_table`=`source_table_id` "
                                . " WHERE academic_year_id ='{$academic_year}'")->queryAll();
                if (count($model_allocationpriority) > 0) {
                    foreach ($model_allocationpriority as $rows) {
                        /*
                         * source of data criteria question
                         * Find application order by ability 
                         *  $arrayapplication=array();
                         * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                         */
                        $baseline = $rows['baseline'];
                        $sql_sub = " " . $rows["source_table_id_field"] . "=" . $rows["field_value"];

                        $sqlall = Yii::$app->db->createCommand("SELECT * FROM `application` a "
                                        . " join `applicant_criteria_score` ac "
                                        . " on a.`application_id`=ac.`application_id` "
                                        . " join criteria_question cq "
                                        . " on ac.`criteria_question_id`=cq.`criteria_question_id` "
                                        . " join cluster_programme cp "
                                        . "  on cp.`programme_id`=a.`programme_id` "
                                        . " WHERE  $sql_sub AND allocation_status=6 order by ability")->queryAll();
                        if (count($sqlall) > 0) {
                            foreach ($sqlall as $rowsdata) {
                                $applicationId = $rowsdata["application_id"];
                                $needness_amount = $rowsdata["needness"];
                                /* item needness
                                 * @@@@@@@@@@@ 
                                 * application_id  loan_item_id  allocated_amount
                                 * Find loan Item  associate with application programme and year of study
                                 * check loan item ordering
                                 */

                                if ($totalbudget >= $needness_amount) {
                                    $academic_year = 1;
                                    $yos = 2;
                                    $programme = 4;
                                    $totalbudget = $this->allocateLoan($applicationId, $needness_amount, $totalbudget, $academic_year, $yos, $programme);
                                } else {
                                    echo "Budget imeisha my friend";
                                }
                            }
                        }

                        //end
                        /*
                         * end
                         * source of data criteria field
                         */
                        // echo "SELECT * FROM `application` a join `applicant_criteria_score` ac on a.`application_id`=ac.`application_id` join criteria_field cf on ac.`criteria_field_id`=cf.`criteria_field_id` join cluster_programme cp on cp.`programme_id`=a.`programme_id` WHERE $sql_sub order by ability";

                        $sqlall2 = Yii::$app->db->createCommand("SELECT * FROM `application` a "
                                        . " join `applicant_criteria_score` ac "
                                        . " on a.`application_id`=ac.`application_id` "
                                        . " join criteria_field cf "
                                        . " on ac.`criteria_field_id`=cf.`criteria_field_id` "
                                        . " join cluster_programme cp "
                                        . " on cp.`programme_id`=a.`programme_id` "
                                        . " WHERE $sql_sub AND allocation_status=6 order by ability")->queryAll();
                        /*
                         * end
                         */
                        if (count($sqlall2) > 0) {
                            foreach ($sqlall2 as $rowsdata2) {
                                $applicationId2 = $rowsdata2["application_id"];
                                $needness_amount2 = $rowsdata2["needness"];
                                /* item needness
                                 * @@@@@@@@@@@ mickidadimsoka@gmail.com
                                 * application_id  loan_item_id  allocated_amount
                                 * Find loan Item  associate with application programme and year of study
                                 * check loan item ordering
                                 */

                                if ($totalbudget >= $needness_amount2) {
                                    $academic_year = 1;
                                    $yos = 2;
                                    $programme = 4;
                                    $totalbudget = $this->allocateLoan($applicationId2, $needness_amount2, $totalbudget, $academic_year, $yos, $programme);
                                } else {
                                    echo "Budget imeisha my friend";
                                }
                            }
                        }
                    }
                } else {
                    /*
                     * allocate normal base on ability of applicant
                     * mickidadimsoka@gmail.com
                     * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                     */
                }
            }
        }
        return $this->redirect(['award-loan']);
    }

    function allocateLoan($applicationId, $needness_amount, $totalbudget, $academic_year, $yos, $programme) {
        $sql_sub = Yii::$app->db->createCommand("SELECT * FROM `programme_fee` pf join loan_item li "
                        . "  on pf.`loan_item_id`=li.`loan_item_id` "
                        . "  join loan_item_priority lp on lp.`loan_item_id`=li.`loan_item_id` "
                        . "  WHERE pf.`academic_year_id`='{$academic_year}' "
                        . "  AND  `programme_id`='{$programme}' "
                        . "  AND `year_of_study`='{$yos}' order by priority_order")->queryAll();
        //  $arrayapplication[$rowsdata["application_id"]]=array();
        if (count($sql_sub) > 0) {
            /*
             * Award loan Item to application 
             * @@@@@@@@@@@@@@@@@@@@@@@@@@@@
             */
//            echo "SELECT * FROM `programme_fee` pf join loan_item li "
//                                                    . "  on pf.`loan_item_id`=li.`loan_item_id` "
//                                                    . "  join loan_item_priority lp on lp.`loan_item_id`=li.`loan_item_id` "
//                                                    . "  WHERE pf.`academic_year_id`='{$academic_year}' "
//                                                    . "  AND  `programme_id`='{$programme}' "
//                                                    . "  AND `year_of_study`='{$yos}' order by priority_order";

            $subtotal = 0;
            echo $needness_amount;
            echo "<br/>";
            foreach ($sql_sub as $subrow) {

                $fee_days = $subrow["days"] == NULL ? 1 : $subrow["days"];
                echo $amount = $subrow["amount"] * $fee_days;
                /*
                 * check loan item amount Vs needness amount
                 */
                if ($needness_amount > 0) {
                    //$balance = $needness_amount - $amount;
                    //echo  $amounttotal = $balance >= 0 ? $amount : $needness_amount;
                    if ($needness_amount >= $amount) {
                        $amounttotal = $amount;
                    } else if ($needness_amount < $amount && $needness_amount > 0) {
                        $amounttotal = $needness_amount;
                    }
                    $model_allocation = new \backend\modules\allocation\models\Allocation();
                    $model_allocation->application_id = $applicationId;
                    $model_allocation->loan_item_id = $subrow["loan_item_id"];
                    $model_allocation->allocated_amount = $amounttotal;
                    $model_allocation->save(FALSE);
                    $subtotal += $amounttotal;
                    echo $needness_amount -= $amounttotal;
                    echo "<br/>";
                } else {
                    echo "needleness imaisha";
                }
            }

            $totalbudget -= $subtotal;
            // exit();
            /*
             * End Award loan Item to application 
             * #############################
             */
        }
        return $totalbudget;
    }

    public function actionAllocationHistory() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $item_id = trim(isset($parents[0]) ? $parents[0] : NULL);

                $items = \backend\modules\allocation\models\AllocationBatchHistory::getAllocationHist($item_id);
                foreach ($items as $item) {
                    $data = ['id' => $item['loan_allocation_history_id'], 'name' => $item['allocation_name']];
                    array_push($out, $data);
                }
                echo \yii\helpers\Json::encode(['output' => $out]);
                return;
            }
        }
        echo \yii\helpers\Json::encode(['output' => '']);
    }

    public function actionReviewAllocationBatch($id) {
        $model = AllocationBatch::findOne($id);
        $model->is_reviewed = 1;
        $model->review_at = date("Y-m-d");
        $model->review_by = \Yii::$app->user->identity->user_id;
        $model->save();
        if ($model) {
            Yii::$app->getSession()->setFlash('success', "Allocation Batch Review Successfully");
        } else {
            Yii::$app->getSession()->setFlash('error', "Allocation Batch Review Failed");
        }
        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionApproveAllocationBatch($id) {
        $model = AllocationBatch::findOne($id);
        if ($model->load(Yii::$app->request->post())) {

            $model->is_approved = 1;
            $model->approved_at = date("Y-m-d");
            $model->approved_by = \Yii::$app->user->identity->user_id;
            $model->save();
            Yii::$app->getSession()->setFlash('success', "Allocation Batch Approved Successfully");
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('approvecreate', [
                        'model' => $model,
            ]);
        }
    }

    public function actionDisapproveAllocationBatch($id) {
        $model = AllocationBatch::findOne($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->is_approved = 2;
            $model->approved_at = date("Y-m-d");
            $model->approved_by = \Yii::$app->user->identity->user_id;
            $model->save();
            Yii::$app->getSession()->setFlash('success', "Allocation Batch Disapproved Successfully");

            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('disapprovecreate', [
                        'model' => $model,
            ]);
        }
    }

    public function actionReviewDecision($id, $level, $task_id, $mt) {
        $model = new AllocationPayoutlistMovement();

        if ($model->load(Yii::$app->request->post())) {
            //find data
            $academic_year_id = AcademicYear::getLatestAcademicYear();
            //find the title of user
            //SELECT order_level,user_id,dd.allocation_task_id,order_levels FROM allocation_user_structure aus join allocation_task_definition dd on dd.`allocation_task_id`=aus.`allocation_task_id` join allocation_structure als on als.allocation_structure_id=aus.allocation_structure_id order by order_levels DESC,order_level DESC limit 1 
            // $sqlall ="SELECT * FROM `allocation_task_assignment` dt join allocation_task_definition dd on dd.allocation_task_id=dt.`allocation_task_id` join allocation_user_structure aus on dt.allocation_user_structure_id=aus.allocation_user_structure_id join allocation_structure als on als.allocation_structure_id=aus.allocation_structure_id AND order_level<='{$model->order_level}' AND dt.allocation_task_id<>'{$model->allocation_task_id}' order by order_levels DESC,order_level DESC limit 1"
            $sqlall = "SELECT order_level,user_id,dd.allocation_task_id,order_levels FROM allocation_user_structure aus 
                                 join allocation_task_definition dd on dd.`allocation_task_id`=aus.`allocation_task_id` 
                                 join allocation_structure als on als.allocation_structure_id=aus.allocation_structure_id
                                 join allocation_movement_schedule ams 
                                 on ams.`allocation_movement_schedule_id`=aus.`allocation_movement_schedule_id` 
                                 AND academic_year_id='{$academic_year_id}' AND allocation_movement_id='{$mt}'
                                 AND order_level<'{$model->order_level}' order by order_levels DESC,order_level DESC limit 1";
            $model_task = Yii::$app->db->createCommand($sqlall)->queryOne();
            if ($model_task) {
                $model->allocation_next_task_id = $model_task['allocation_task_id'];
                $model->allocation_movement_id = $mt;
                $model->movement_status = 1;
                //update movement status
                Yii::$app->db->createCommand("Update `allocation_payoutlist_movement` set movement_status=2 WHERE   allocation_batch_id='{$model->allocation_batch_id}' AND allocation_movement_id='{$model->allocation_movement_id}'")->execute();
                //end update movement
            } else {
                $model->movement_status = 3;
                //update movement status
                $model->allocation_movement_id = $mt;
                Yii::$app->db->createCommand("Update `allocation_payoutlist_movement` set movement_status=3 WHERE   allocation_batch_id='{$model->allocation_batch_id}' AND allocation_movement_id='{$model->allocation_movement_id}'")->execute();
                //end update movement
                //update allocation  batch

                /* $modelupdate=AllocationBatch::findOne($model->allocation_batch_id);
                  $modelupdate->status=$status;
                  $modelupdate->movement_type=$mt;
                  $modelupdate->save(); */
                if ($mt == 3) {
                    $status = 3;
                } else {
                    $status = 2;
                }
                $this->updateAllocationStatus($model->allocation_batch_id, $status, $mt);
                //end 
            }
            $model->date_out = date("Y-m-d");
            $model->to_officer = $model_task['user_id'] > 0 ? $model_task['user_id'] : $model->from_officer;
            $model->save();

            // print_r($model);
            //  exit();
            /* $models=  DisbursementBatch::findone($model->disbursements_batch_id);
              //$models
              //$models->is_approved=1;
              $models->save(); */
            //end
            $id = $model->allocation_batch_id;
            if ($mt == 1) {
                return $this->redirect(['view', 'id' => $id]);
            } else if ($mt == 2) {
                //view-larc-allocation-batch-update
                return $this->redirect(['view-larc-allocation-batch-update', 'id' => $id]);
            } else if ($mt == 3) {
                return $this->redirect(['view-larc-allocation-batch-update', 'id' => $id]);
            }
        } else {
            return $this->render('../allocation-payoutlist-movement/create', [
                        'model' => $model,
                        'allocation_batch_id' => $id,
                        'task_id' => $task_id,
                        'level' => $level,
                        'allocation_movement_id' => $mt
            ]);
        }
    }

    #########################################################################################################################################
    ####################################  Reject or Return back to previous activity  #######################################################
    #########################################################################################################################################

    public function actionRejectDecision($id, $level, $task_id, $mt) {
        $model = new AllocationPayoutlistMovement();

        if ($model->load(Yii::$app->request->post())) {
            //find data
            $academic_year_id = AcademicYear::getLatestAcademicYear();
            //find the title of user
            //SELECT order_level,user_id,dd.allocation_task_id,order_levels FROM allocation_user_structure aus join allocation_task_definition dd on dd.`allocation_task_id`=aus.`allocation_task_id` join allocation_structure als on als.allocation_structure_id=aus.allocation_structure_id order by order_levels DESC,order_level DESC limit 1
            // $sqlall ="SELECT * FROM `allocation_task_assignment` dt join allocation_task_definition dd on dd.allocation_task_id=dt.`allocation_task_id` join allocation_user_structure aus on dt.allocation_user_structure_id=aus.allocation_user_structure_id join allocation_structure als on als.allocation_structure_id=aus.allocation_structure_id AND order_level<='{$model->order_level}' AND dt.allocation_task_id<>'{$model->allocation_task_id}' order by order_levels DESC,order_level DESC limit 1"
            $sqlall = "SELECT * from allocation_payoutlist_movement 
                                  allocation_movement_id='{$mt}'
                                  AND movement_status=1 order by movement_id DESC limit 1";
            $model_task = Yii::$app->db->createCommand($sqlall)->queryOne();
            if ($model_task) {
                //$model_task->allocation_batch_id=$model_task["allocation_batch_id"];
                $model->allocation_next_task_id = $model_task['allocation_task_id'];
                $model->allocation_movement_id = $mt;
                $model->movement_status = 1;
                //update movement status
                Yii::$app->db->createCommand("Update `allocation_payoutlist_movement` set movement_status=1 WHERE   allocation_batch_id='{$model->allocation_batch_id}' AND allocation_movement_id='{$model->allocation_movement_id}'")->execute();
                //end update movement
                $model->date_out = date("Y-m-d");
                $model->to_officer = $model_task['to_officer'];
                $model->save();
            } else {
                $model->movement_status = 2;
                //update movement status
                $model->allocation_movement_id = $mt;
                Yii::$app->db->createCommand("Update `allocation_payoutlist_movement` set movement_status=2 WHERE   allocation_batch_id='{$model->allocation_batch_id}' AND allocation_movement_id='{$model->allocation_movement_id}'")->execute();
                //end update movement
                //update allocation  batch

                /* $modelupdate=AllocationBatch::findOne($model->allocation_batch_id);
                  $modelupdate->status=$status;
                  $modelupdate->movement_type=$mt;
                  $modelupdate->save(); */

                if ($mt == 1) {
                    $this->updateAllocationStatus($model->allocation_batch_id, 0, 0);
                } else if ($mt == 3) {
                    $this->updateAllocationStatus($model->allocation_batch_id, 2, 2);
                }
                //end
            }



            $id = $model->allocation_batch_id;
            if ($mt == 1) {
                return $this->redirect(['view', 'id' => $id]);
            } else if ($mt == 2) {
                //view-larc-allocation-batch-update
                return $this->redirect(['view-larc-allocation-batch-update', 'id' => $id]);
            } else if ($mt == 3) {
                return $this->redirect(['view-larc-allocation-batch-update', 'id' => $id]);
            }
        } else {

            return $this->render('../allocation-payoutlist-movement/creater', [
                        'model' => $model,
                        'allocation_batch_id' => $id,
                        'task_id' => $task_id,
                        'level' => $level,
                        'allocation_movement_id' => $mt
            ]);
        }
    }

    public function actionSubmitAllocationBatch($id, $mt) {
        //get latest academic year
        //ms means movement type
        $academic_year_id = AcademicYear::getLatestAcademicYear();


        //end 
        //find movement user 
        $sqlall = "SELECT order_level,user_id,dd.allocation_task_id,order_levels FROM allocation_user_structure aus 
                                 join allocation_task_definition dd on dd.`allocation_task_id`=aus.`allocation_task_id` 
                                 join allocation_structure als on als.allocation_structure_id=aus.allocation_structure_id
                                 join allocation_movement_schedule ams 
                                 on ams.`allocation_movement_schedule_id`=aus.`allocation_movement_schedule_id` 
                                 AND academic_year_id='{$academic_year_id}' AND allocation_movement_id='{$mt}' order by order_levels DESC,order_level DESC limit 1 ";
        $model_task = Yii::$app->db->createCommand($sqlall)->queryOne();
        //end 
        //  exit();
        if ($model_task) {
            //$this->updateAllocationStatus($id,2,$mt);
            $model = new AllocationPayoutlistMovement();
            $model->movement_status = 1;
            $model->allocation_batch_id = $id;
            $model->allocation_task_id = 4;
            $model->allocation_movement_id = $mt;
            $model->allocation_next_task_id = $model_task['allocation_task_id'];
            $model->from_officer = Yii::$app->user->identity->user_id;
            $model->date_out = date("Y-m-d");
            $model->to_officer = $model_task['user_id'] > 0 ? $model_task['user_id'] : Yii::$app->user->identity->user_id;
            $model->save();
            // print_r($model);
            //exit();
            $this->updateAllocationStatus($id, 1, $mt);
            Yii::$app->getSession()->setFlash('success', "Allocation Batch Submitted Successfully");
        } else {
            Yii::$app->getSession()->setFlash('danger', "Allocation Batch Failed to Submit due missing Allocation use structure Configuration");
        }
        if ($mt == 1) {
            return $this->redirect(['view', 'id' => $id]);
        } else if ($mt == 2) {
            return $this->redirect(['view-larc-allocation-batch', 'id' => $id]);
        } else if ($mt == 3) {
            return $this->redirect(['view-larc-allocation-batch-update', 'id' => $id]);
        }
    }

    function UpdateAllocationStatus($id, $status, $mt) {
        $model = AllocationBatch::findOne($id);
        $model->movement_type = $mt;
        $model->status = $status;
        $model->save();
    }

    public function actionViewLarc($id) {

        return $this->render('profile_larc', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionViewUpdate($id) {

        return $this->render('profile_update', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionViewApproved($id) {

        return $this->render('profile_approved', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionAllocationBatchLarc() {

        $searchModel = new AllocationBatchSearch();
        $dataProvider = $searchModel->searchAllocationBatchLarc(Yii::$app->request->queryParams);

        return $this->render('allocation_batch_larc', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAllocationBatchUpdate() {

        $searchModel = new AllocationBatchSearch();
        $dataProvider = $searchModel->searchUpdateAllocationBatch(Yii::$app->request->queryParams);

        return $this->render('allocation_batch_update', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApprovedAllocationBatch() {

        $searchModel = new AllocationBatchSearch();
        $dataProvider = $searchModel->searchApprovedAllocationBatch(Yii::$app->request->queryParams);
        return $this->render('approved_allocation_batch', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAllocationBatchPartialApprove($id) {

        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->ed_approve_attachment = UploadedFile::getInstance($model, 'ed_approve_attachment');
            if ($model->ed_approve_attachment != "") {
                $model->ed_approve_attachment->saveAs('attachment/ED_' . $model->allocation_batch_id . '_' . date("Y_h_i") . '.' . $model->ed_approve_attachment->extension);
                $model->ed_approve_attachment = 'attachment/ED_' . $model->allocation_batch_id . '_' . date("Y_h_i") . '.' . $model->ed_approve_attachment->extension;
                $model->partial_approve_status = 1;
                $model->save();
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('createED', [
                        'model' => $model,
            ]);
        }
    }

    public function actionAllocationBatchLarcApprove($id) {

        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->larc_approve_attachment = UploadedFile::getInstance($model, 'larc_approve_attachment');

            if ($model->larc_approve_attachment != "") {
                $model->larc_approve_attachment->saveAs('attachment/LARC_' . $model->allocation_batch_id . '_' . date("Y_h_i") . '.' . $model->larc_approve_attachment->extension);
                $model->larc_approve_attachment = 'attachment/LARC_' . $model->allocation_batch_id . '_' . date("Y_h_i") . '.' . $model->larc_approve_attachment->extension;
                $model->larc_approve_status = 1;
                $model->save();
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('createLarc', [
                        'model' => $model,
            ]);
        }
    }

    //view-approved-allocation-batch
    public function actionViewApprovedAllocationBatch($id) {

        return $this->render('profile_approve', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionViewLarcAllocationBatch($id) {

        return $this->render('profile_larc', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionAllocationBatchLarcSubmit($id) {
        //submit allocation batch to LARC
        $model = $this->findModel($id);
        //$model->
        $model->larc_approve_status = 2;
        $model->save();

        return $this->redirect(['index']);
    }

    public function actionAllocationBatchLarcUpdate($id) {
        /*
         * Release batch for update  
         */
        $model = $this->findModel($id);
        $model->larc_approve_status = 3;
        $model->status = 3;
        $model->save();
        // print_r
        return $this->redirect(['allocation-batch-larc']);
    }

    public function actionViewLarcAllocationBatchUpdate($id) {
        return $this->render('profile_larc_update', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionViewPendingBatchUpdate($id) {

        return $this->render('profile_pending_batch_update.php', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionPendingBatchUpdate() {

        $searchModel = new AllocationBatchSearch();
        $dataProvider = $searchModel->searchPendingBatchUpdate(Yii::$app->request->queryParams);

        return $this->render('index_pending_allocation_batch_update', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    //allocation-batch-updated

    public function actionAllocationBatchUpdated() {

        $searchModel = new AllocationBatchSearch();
        $dataProvider = $searchModel->searchAllocationBatchUpdated(Yii::$app->request->queryParams);

        return $this->render('index_allocation_batch_updated', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewAllocationBatchUpdated($id) {

        return $this->render('view_pending_batch_update.php', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionPendingAllocationBatchUpdated() {

        $searchModel = new AllocationBatchSearch();
        $dataProvider = $searchModel->searchPendingBatchUpdated(Yii::$app->request->queryParams);

        return $this->render('index_pending_batch_updated', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewPendingBatchUpdated($id) {

        return $this->render('profile_pending_batch_updated', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionAllocationBatchApprove($id) {

        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->larc_approve_attachment = UploadedFile::getInstance($model, 'larc_approve_attachment');

            if ($model->larc_approve_attachment != "") {
                $model->larc_approve_attachment->saveAs('attachment/LARC_' . $model->allocation_batch_id . '_' . date("Y_h_i") . '.' . $model->larc_approve_attachment->extension);
                $model->larc_approve_attachment = 'attachment/LARC_' . $model->allocation_batch_id . '_' . date("Y_h_i") . '.' . $model->larc_approve_attachment->extension;
                $model->larc_approve_status = 1;
                $model->status = 10;
                $model->save();
                return $this->redirect(['pending-allocation-batch-updated']);
            }
        } else {
            return $this->render('create_update_approve', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCreateSpecialBatch() {
        $max_data_processing = 1000;
        $start_record = 0;
        $model = new AllocationBatch();
        //$model->scenario='special_batch';
        if ($model->load(Yii::$app->request->post())) {
            ##############allocation batch generation ###################
            $model->batch_type = AllocationBatch::BATCH_SPECIAL;
            $contained_student = $model->contained_student;
            $study_country = $model->study_country;
            $institution = $model->institution;
            $programme = $model->programme;
            $study_year = $model->study_year;
            $academic_year = $model->academic_year_id;
            $batch_students = \backend\modules\allocation\models\AllocationPlanStudent::getAwardeeStudentsByAcademicYearTypeCountryIntitutionProgrammeStudyYear($model->academic_year_id, $model->contained_student, $model->study_country, $model->institution, $model->programme, $model->study_year);
            ///check if stundent Exist
            if ($batch_students) {
                if ($model->save()) { ///
                    //create batch number
                    $modelbatch = AllocationBatch::find()->orderBy('batch_number DESC')->where("academic_year_id=$model->academic_year_id AND allocation_batch_id<=$model->allocation_batch_id")->one();
                    if (count($modelbatch) > 0) {
                        $modelbatch_number = 1 + $modelbatch->batch_number;
                    } else {
                        $modelbatch_number = 1;
                    }
                    //updating the batchNo
                    $model->batch_number = $modelbatch_number;
                    $model->save();
                    //end create batch number
                    //creating llocation history
                    /* $modelh1 = new AllocationBatchHistory();
                      $modelh1->allocation_batch_history_id = $model->alloca;
                      $modelh1->allocation_batch_id = $model->allocation_batch_id;
                      $modelh1->save(); */
                    $failure = $success = 0;
                    foreach ($batch_students as $batch_student) {
                        $student_allocated_loan_items = $batch_student->getStudentAllocationsByAllocationPlan();
                        //                        var_dump($student_allocated_loan_items);
                        //                        exit;
                        foreach ($student_allocated_loan_items as $student_allocated_loan_item) {
                            $model_allocation = new Allocation();
                            $model_allocation->allocation_batch_id = $model->allocation_batch_id;
                            $model_allocation->application_id = $batch_student->application_id;
                            $model_allocation->loan_item_id = $student_allocated_loan_item->loan_item_id;
                            $model_allocation->allocated_amount = $student_allocated_loan_item->total_amount_awarded;
                            if (($student_allocated_loan_item->duration * $student_allocated_loan_item->rate_type * $student_allocated_loan_item->unit_amount) == $model_allocation->allocated_amount) {
                                if ($model_allocation->save()) {
                                    ////HERE  ADD THIS LINE
                                    //UPDATING THE ALLOCATION BATH STUDENT TABLE
                                    $batch_student_details = [
                                        'allocation_history_id' => $batch_student->allocation_history_id,
                                        'programme_id' => $batch_student->programme_id,
                                        'study_year' => $batch_student->study_year
                                    ];
                                    \backend\modules\allocation\models\AllocationBatchStudent::CreateAllocationBatchStudent($model_allocation, $batch_student_details);

                                    ///END HERE
                                    $success++;
                                }
                            } else {
                                $failure++;
                            }
                        }
                    }
                    if ($success) {
                        if ($failure) {
                            $sms = 'Operation Successful, ' . $failure . ' Batch Operations failed, Please check';
                            Yii::$app->session->setFlash('failure', $sms);
                        }
                        return $this->redirect(['view', 'id' => $model->allocation_batch_id]);
                    } else {
                        ///delete the batch and show SMS to the person
                        $sms = 'Operation Failed, Batch Creation failed Please check';
                        Yii::$app->session->setFlash('failure', $sms);
                    }
                    ######################## End process @@@@############################
                } else {
                    $sms = 'Operation Failed, Batch Creation failed Please check';
                    Yii::$app->session->setFlash('failure', $sms);
                }
            } else {
                $sms = 'No Students Found to Match Your Selection Criteria, Please check your Criteria of Selection';
                Yii::$app->session->setFlash('failure', $sms);
            }
        }
        return $this->render('create_special_batch', [
                    'model' => $model,
                    'modelh' => $modelh,
        ]);
    }

    public function actionCreateFileBatch() {
        $model = new AllocationBatch(['scenario' => create_file_batch]);
        //        $model->scenario='create_file_batch';
        if ($model->load(Yii::$app->request->post()) && ( $model->academic_year_id == \common\models\AcademicYear::getCurrentYearID())) {
            $model->academic_year_id = \common\models\AcademicYear::getCurrentYearID();
            $model->batch_type = AllocationBatch::BATCH_SPECIAL;

            $model->data_file = \yii\web\UploadedFile::getInstance($model, 'data_file');
            if ($model->data_file != "") {
                $model->data_file->saveAs('../uploadimage/upload/' . $model->data_file->name);
                $model->data_file = '../uploadimage/upload/' . $model->data_file->name;

                $data = \moonland\phpexcel\Excel::widget([
                            'mode' => 'import',
                            'fileName' => $model->data_file,
                            'setFirstRecordAsKeys' => true, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel.
                            'setIndexSheetByName' => true, // set this if your excel data with multiple worksheet, the index of array will be set with the sheet name. If this not set, the index will use numeric.
                            'getOnlySheet' => 'Sheet1', // you can set this property if you want to get the specified sheet from the excel data with multiple worksheet.
                ]);
                //                    print_r($data);
                //                    exit();
                if (count($data) > 0) {
                    $check = 0;
                    $objPHPExcelOutput = new \PHPExcel();
                    $objPHPExcelOutput->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcelOutput->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);
                    $objPHPExcelOutput->setActiveSheetIndex(0);

                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('A1', 'Sn');
                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('B1', 'f4indexno');
                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('C1', 'firstName');
                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('D1', 'secondName');
                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('E1', 'surname');
                    // $objPHPExcelOutput->getActiveSheet()->SetCellValue('F1', 'programme_code');
                    //$objPHPExcelOutput->getActiveSheet()->SetCellValue('G1', 'programme');
                    //$objPHPExcelOutput->getActiveSheet()->SetCellValue('H1', 'institution_code');

                    $rowCount = 2;
                    $s_no = 1;
                    foreach ($data as $datas12) {
                        //check if exit
                        foreach ($datas12 as $label => $value) {
                            //print_r($label);
                            if ($label == "f4indexno") {
                                $check+=0;
                            } else if ($label == "firstName") {
                                $check+=0;
                            } else if ($label == "secondName") {
                                $check+=0;
                            } else if ($label == "surname") {
                                $check+=0;
                            }
                            /* else if ($label == "programme_code") {
                              $check+=0;
                              } else if ($label == "programme") {
                              $check+=0;
                              } else if ($label == "institution_code") {
                              $check+=0;
                              } */ else if ($label == "Sn") {
                                $check+=0;
                            } else {
                                if ($label != "") {
                                    $check+=1;
                                }
                            }
                        }
                        // echo $datas12;

                        if ($check == 0) {
                            $f4index = trim($datas12["f4indexno"]);
                            $firstname = trim($datas12["firstName"]);
                            $middlename = trim($datas12["secondName"]);
                            $lastname = trim($datas12["surname"]);
                            //$programme_code = trim($datas12["programme_code"]);
                            //$programme_name = trim($datas12["programme"]);
                            //$institution_code = trim($datas12["institution_code"]);
                            $empty_field = 0;
                            $comment_empt = "";
                            if ($f4index == "") {
                                $empty_field++;
                                $comment_empt.=" Empty Form 4 index Number , ";
                            }
                            if ($firstname == "") {
                                $empty_field++;
                                $comment_empt.=" Empty first Name ,";
                            }
                            if ($lastname == "") {
                                $empty_field++;
                                $comment_empt.=" Empty Last Name , ";
                            }
                            /* if ($programme_code == "") {
                              $empty_field++;
                              $comment_empt.=" Empty Programme Code , ";
                              }
                              if ($institution_code == "") {
                              $empty_field++;
                              $comment_empt.=" Empty Institution Code ";
                              } */
                            if ($empty_field == 0) {
                                ##############applicant exit in this academic year ###############
                                $sql_exist = "SELECT  FROM `applicant`
                                            INNER JOIN application ON application.applicant_id=applicant.applicant_id
                                            INNER JOIN user.user_id=applicant.user_id
                                            INNER JOIN allocation_plan_student.application_id=application.application_id
                                            WHERE applicant.f4indexno=:f4indexNo  AND (user.firstname LIKE '%:firstname%'
                                            OR user.middlename LIKE '%:middlename%' OR user.surname LIKE '%:surname)
                                            ";
                                $batch_students = \backend\modules\allocation\models\AllocationPlanStudent::findBySql($sql_exist, [':f4indexNo' => $f4index, ':firstname' => $firstname, ':middlename' => $middlename, ':surname' => $surname])->all();
                                #################check end ######################################
                                if ($batch_students) {
                                    ///checking if student exists
                                    ///create batch
                                    if ($model->save()) {
                                        $model->save();
                                        //create batch number
                                        $modelbatch = AllocationBatch::find()->orderBy('batch_number DESC')->where("academic_year_id=$model->academic_year_id AND allocation_batch_id<=$model->allocation_batch_id")->one();
                                        if (count($modelbatch) > 0) {

                                            $modelbatch_number = 1 + $modelbatch->batch_number;
                                        } else {
                                            $modelbatch_number = 1;
                                        }
                                        $model->batch_number = $modelbatch_number;
                                        $model->save();
                                        $failure = $success = 0;
                                        foreach ($batch_students as $batch_student) {
                                            $student_allocated_loan_items = $batch_student->getStudentAllocationsByAllocationPlan();
                                            foreach ($student_allocated_loan_items as $student_allocated_loan_item) {
                                                $model_allocation = new Allocation();
                                                $model_allocation->allocation_batch_id = $model->allocation_batch_id;
                                                $model_allocation->application_id = $batch_student->application_id;
                                                $model_allocation->loan_item_id = $student_allocated_loan_item->loan_item_id;
                                                $model_allocation->allocated_amount = $student_allocated_loan_item->total_amount_awarded;
                                                if (($student_allocated_loan_item->duration * $student_allocated_loan_item->rate_type * $student_allocated_loan_item->unit_amount) == $model_allocation->allocated_amount) {
                                                    if ($model_allocation->save()) {
                                                        $success++;
                                                         ////
                                                        ////HERE  ADD THIS LINE
                                                        //UPDATING THE ALLOCATION BATH STUDENT TABLE
                                                        $batch_student_details = [
                                                            'allocation_history_id' => $batch_student->allocation_history_id,
                                                            'programme_id' => $batch_student->programme_id,
                                                            'study_year' => $batch_student->study_year
                                                        ];
                                                        \backend\modules\allocation\models\AllocationBatchStudent::CreateAllocationBatchStudent($model_allocation, $batch_student_details);
                                                        ///END HERE
                                                        /////
                                                    }
                                                } else {
                                                    $failure++;
                                                }
                                            }
                                        }
                                        if ($success) {
                                            if ($failure) {
                                                $sms = 'Operation Successful, ' . $failure . ' Batch Operations failed, Please check';
                                                Yii::$app->session->setFlash('failure', $sms);
                                            }
                                            return $this->redirect(['view', 'id' => $model->allocation_batch_id]);
                                        } else {
                                            ///delete the batch and show SMS to the person
                                            $sms = 'Operation Failed, Batch Creation failed Please check';
                                            Yii::$app->session->setFlash('failure', $sms);
                                        }
                                    }
                                } else {
                                    //Applicant already exit
                                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('A' . $rowCount, $s_no);
                                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('B' . $rowCount, $f4index);
                                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('C' . $rowCount, $firstname);
                                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('D' . $rowCount, $middlename);
                                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('E' . $rowCount, $lastname);
                                    //                                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('F' . $rowCount, $programme_code);
                                    //                                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('G' . $rowCount, $programme_name);
                                    //                                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('H' . $rowCount, $institution_code);
                                    $objPHPExcelOutput->getActiveSheet()->SetCellValue('F' . $rowCount, 'Already Exist');
                                    $rowCount++;
                                    $s_no++;
                                }
                                //end
                                // Yii::$app->session->setFlash('success', 'Information Upload Successfully');
                            } else {
                                /*
                                 * Empty field
                                 */

                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('A' . $rowCount, $s_no);
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('B' . $rowCount, $f4index);
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('C' . $rowCount, $firstname);
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('D' . $rowCount, $middlename);
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('E' . $rowCount, $lastname);
                                //                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('F' . $rowCount, $programme_code);
                                //                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('G' . $rowCount, $programme_name);
                                //                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('H' . $rowCount, $institution_code);
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('F' . $rowCount, $comment_empt);
                                $rowCount++;
                                $s_no++;
                            }
                        } else {
                            Yii::$app->session->setFlash('error', 'Sorry ! The Excel Label are case sensitive download new formate or change Label Name');
                        }
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'The Excel File Selected Is empty');
                }
            }
            $objPHPExcelOutput->getActiveSheet()->getStyle('A1:H1')->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN)->getColor()->setRGB('DDDDDD');
            $writer = \PHPExcel_IOFactory::createWriter($objPHPExcelOutput, 'Excel5');
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Allocation_BatchUpload_Report.xls"');
            header('Cache-Control: max-age=0');
            $writer->save('php://output');
        }
        return $this->render('create_file_batch', [
                    'model' => $model,
        ]);
    }

    public function actionCreateAdditionalBatch() {
        $model = new AllocationBatch;
        $model->cancel_comment = 'NILL';
        $model->scenario = 'additional_batch';
        if ($model->load(Yii::$app->request->post())) {
            $model->is_approved = 0;
            $model->created_by = $model->updated_by = Yii::$app->user->id;
            $model->is_canceled = 0;
            $model->disburse_status = 0;
            $model->batch_type = AllocationBatch::BATCH_ADDITIONAL;
            switch ($model->student) {
                case 1: //file upload ith students
                    $model->scenario = 'student_list_file';
                    $model->data_file = \yii\web\UploadedFile::getInstance($model, 'data_file');
                    break;
                default;
                    switch ($model->contained_student) {
                        case 1; ///new freshers student
                            $model->students_list = $model->getNewAwardeeStudents();
                            break;
                        case 2:///contunuing student
                            $model->students_list = $model->getContinuingAwardeeStudents();
                            break;
                    }
                    break;
            }
            if ($model->validate()) {
                ///processing the data  file
                if ($model->save()) {
                    $modelbatch = AllocationBatch::find()->orderBy('batch_number DESC')->where("academic_year_id=$model->academic_year_id AND allocation_batch_id<=$model->allocation_batch_id")->one();
                    if (count($modelbatch) > 0) {

                        $modelbatch_number = 1 + $modelbatch->batch_number;
                    } else {
                        $modelbatch_number = 1;
                    }
                    $model->batch_number = $modelbatch_number;
                    $model->save();
                    $count_student = 0;
                    if ($model->scenario == 'student_list_file') {
                        ////process the uploaded file
                        if ($model->data_file != "") {
                            $model->data_file->saveAs(Yii::$app->params['default_upload_folder'] . $model->data_file->name);
                            $model->data_file = Yii::$app->params['default_upload_folder'] . $model->data_file->name;
                            $data = \moonland\phpexcel\Excel::widget([
                                        'mode' => 'import',
                                        'fileName' => $model->data_file,
                                        'setFirstRecordAsKeys' => true, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel.
                                        'setIndexSheetByName' => true, // set this if your excel data with multiple worksheet, the index of array will be set with the sheet name. If this not set, the index will use numeric.
                                        'getOnlySheet' => 'Sheet1', // you can set this property if you want to get the specified sheet from the excel data with multiple worksheet.
                            ]);
                            if (count($data)) {
                                $check = 0;
                                $objPHPExcelOutput = new \PHPExcel();
                                $objPHPExcelOutput->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                $objPHPExcelOutput->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);
                                $objPHPExcelOutput->setActiveSheetIndex(0);
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('A1', 'Sn');
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('B1', 'AcademicYear');
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('C1', 'F4indexno');
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('D1', 'ProgrammeCode');
                                $objPHPExcelOutput->getActiveSheet()->SetCellValue('E1', 'YOS');
                                $rowCount = 2;
                                $s_no = 1;
                                foreach ($data as $datas12) {
                                    foreach ($datas12 as $label => $value) {
                                        if ($label == "AcademicYear") {
                                            $check+=0;
                                        } else if ($label == "F4indexno") {
                                            $check+=0;
                                        } else if ($label == "ProgrammeCode") {
                                            $check+=0;
                                        } else if ($label == "YOS") {
                                            $check+=0;
                                        } else if ($label == "Sn") {
                                            $check+=0;
                                        } else {
                                            if ($label != "") {
                                                $check+=1;
                                            }
                                        }
                                    }
                                    $comment = NULL;
                                    $error = $invalid_data = 0;
                                    if ($check == 0) {
                                        $AcademicYear = trim($datas12["AcademicYear"]);
                                        $F4indexno = trim($datas12["F4indexno"]);
                                        $ProgrammeCode = trim($datas12["ProgrammeCode"]);
                                        $YOS = trim($datas12["YOS"]);
                                        if (empty($AcademicYear)) {
                                            $comment .= ', Empty Acacemic Year';
                                            $error++;
                                        }
                                        if (empty($F4indexno)) {
                                            $comment .= ', Empty F4indexno';
                                            $error++;
                                        }
                                        if (empty($ProgrammeCode)) {
                                            $comment .= ', Empty Programme Code';
                                            $error++;
                                        }
                                        if (empty($YOS)) {
                                            $comment .= ', Empty Year of Study';
                                            $error++;
                                        }
                                        if (!$error) {
                                            //validate student
                                            if ($AcademicYear != \common\models\AcademicYear::getCurrentYearName()) {
                                                $comment .=', Wrong Academic Year Details';
                                                $invalid_data++;
                                            }
                                            $Programme = \backend\modules\allocation\models\Programme::getProgrammeByProgrammeCode($ProgrammeCode);
                                            if (!$programme) {
                                                $comment .=', Wrong Programme Code';
                                                $invalid_data++;
                                            }
                                            if (in_array($YOS, Yii::$app->params['programme_years_of_study'])) {
                                                $comment .=', Wrong Year of Study Year Details';
                                                $invalid_data++;
                                            }
                                            if ($F4indexno && $programme && $YOS && $AcademicYear) {
                                                $sql = "SELECT *
                                                        FROM allocation
                                                        INNER JOIN allocation_batch ON allocation_batch.allocation_batch_id=allocation.allocation_batch_id
                                                        INNER JOIN allocation_plan_student ON allocation_plan_student.application_id=allocation.application_id
                                                        WHERE allocation_plan_student.academic_year=" . \common\models\AcademicYear::getCurrentYearID() . "
                                                        AND allocation_plan_student.programme_id=" . $Programme->programme_id . "
                                                        AND allocation_plan_student.application_id IN (
                                                          SELECT application.application_id FROM application
                                                          INNER JOIN applicant ON application.applicant_id=applicant.applicant_id
                                                          WHERE applicant.f4indexno='" . $F4indexno . "'
                                                          )
                                                         AND allocation_batch.contained_student=" . $model->contained_student . "
                                                        AND allocation_plan_student.study_year=" . $YOS;

                                                $exist = Allocation::find($sql)->one();
                                                if ($exist) {
                                                    ////creatinf student data in allocation batch student table
                                                    $allocation_barch_student = new \backend\modules\allocation\models\AllocationBatchStudent;
                                                    $allocation_barch_student->allocation_batch_id = $model->allocation_batch_id;
                                                    $allocation_barch_student->allocation_history_id = AllocationBatchHistory::getHistoryIDByBatchId($exist->allocation_batch_id);
                                                    $allocation_barch_student->application_id = $exist->application_id;
                                                    $allocation_barch_student->sex = \backend\modules\allocation\models\AllocationBatchStudent::getApplicantGenderByApplicationId($exist->application_id);
                                                    $allocation_barch_student->programme_id = $Programme->programme_id;
                                                    $allocation_barch_student->cluster_definition_id = \backend\modules\allocation\models\AllocationBatchStudent::getProgrammeClusterByProgrammeId($Programme->programme_id, $model->academic_year_id);
                                                    $allocation_barch_student->study_year = $YOS;
                                                    if ($allocation_barch_student->save()) {
                                                        $count_student++;
                                                    }
                                                } else {
                                                    $comment .=', No Student Exists';
                                                    $invalid_data++;
                                                }
                                            } else {
                                                $comment .=', Wrong Student Details';
                                                $invalid_data++;
                                            }

                                            ///creating an array ofte students name
                                        }
                                    }
                                }
                            } else {
                                $model->students_list = NULL;
                            }
                        }

                        ///END FILE PROCESSING
                    } else {
                        foreach ($model->students_list as $student) {
                            $allocation_barch_student = new \backend\modules\allocation\models\AllocationBatchStudent;
                            $allocation_barch_student->allocation_batch_id = $model->allocation_batch_id;
                            $allocation_barch_student->application_id = $student->application_id;
                            $allocation_barch_student->allocation_history_id = $student->allocation_history_id;
                            $allocation_barch_student->sex = \backend\modules\allocation\models\AllocationBatchStudent::getApplicantGenderByApplicationId($student->application_id);
                            $allocation_barch_student->programme_id = $student->programme_id;
                            $allocation_barch_student->cluster_definition_id = \backend\modules\allocation\models\AllocationBatchStudent::getProgrammeClusterByProgrammeId($student->programme_id, $model->academic_year_id);
                            $allocation_barch_student->study_year = $student->study_year;
                            if ($allocation_barch_student->save()) {
                                $count_student++;
                            }
                        }
                    }
                    if ($count_student) {
                        return $this->redirect(['view-additional-batch', 'id' => $model->allocation_batch_id]);
                    } else {
                        ///removes the created batch
                        $model->delete();
                        $sms = 'Opereation Failed,No Sudent Data found for Batch Creation';
                        Yii::$app->session->setFlash('failure', $sms);
                    }
                }
            }
        }
        return $this->render('create_additional_batch', [
                    'model' => $model,
        ]);
    }

    function actionViewAdditionalBatch($id) {
        $model = $this->findModel($id);
        $student_model = new \backend\modules\allocation\models\AllocationBatchStudent();
        $dataProvider = NULL;
        if ($model) {
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => \backend\modules\allocation\models\AllocationBatchStudent::find()->where(['allocation_batch_id' => $model->allocation_batch_id]),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
        }
        return $this->render('view_additional_batch', [
                    'model' => $model, 'dataProvider' => $dataProvider, 'student_model' => $student_model
        ]);
    }

    function actionAddAdditionalAmount($id) {
        $model = $this->findModel($id);
        $allocation = new Allocation();
        $allocation->scenario = 'additional_batch';
        $loan_items = NULL;
        if ($model) {
            $loan_items = \backend\modules\allocation\models\AllocationLoanItemSetting::getLoanItemsByStudyLevel();
        }
        //var_dump(Yii::$app->request->post());
        if (Yii::$app->request->post()) {
            if (count($allocation)) {
                $allocation_items = Yii::$app->request->post('Allocation');
                ///getting ellibible students
                $students = \backend\modules\allocation\models\AllocationBatchStudent::find()
                                ->where(['allocation_batch_id' => $model->allocation_batch_id])->all();
                if ($students && $allocation_items['loan_item_id']) {
                    $items_success = $items_failure = 0;
                    foreach ($students as $student) {
                        foreach ($allocation_items["loan_item_id"] as $key => $allocation_loan_item) {
                            $allocation_item = New Allocation;
                            $allocation_item->allocation_batch_id = $model->allocation_batch_id;
                            $allocation_item->application_id = $student->application_id;
                            $allocation_item->loan_item_id = (int) $allocation_loan_item;
                            $loan_item_details = \backend\modules\allocation\models\AllocationLoanItemSetting::getItemDetailsById($allocation_loan_item);
                            //ensure the annualpaid amounts are always annualy
                            $duration = $allocation_items["duration"][$key];
                            $unit_amount = $allocation_items["unit_amount"][$key];
                            if ($loan_item_details->rate_type == \backend\modules\allocation\models\AllocationLoanItemSetting::RATE_YEARLY) {
                                $duration = 1;
                            }
                            $allocation_item->allocated_amount = $unit_amount * $duration;
                            /// var_dump($allocation_item->attributes);
                            if ($allocation_item->save()) {
                                // echo '=== SUCCESS===';
                                ///adding the record in the allocation plan_student
                                $allocation_plan_student_loan_item = new \backend\modules\allocation\models\AllocationPlanStudentLoanItem();
                                $allocation_plan_student_loan_item->is_additional = 1; ///setting it as additional batch
                                $allocation_plan_student_loan_item->total_amount_awarded = $allocation_item->allocated_amount;
                                $allocation_plan_student_loan_item->duration = $duration;
                                $allocation_plan_student_loan_item->unit_amount = $unit_amount;
                                $allocation_plan_student_loan_item->loan_item_id = (int) $allocation_loan_item;
                                $item_current_allocation = \backend\modules\allocation\models\AllocationPlanStudentLoanItem::getStudentLoanItemCurrentAllocation($allocation_plan_student_loan_item->loan_item_id, $student->application_id, $student->allocation_history_id);
                                if ($item_current_allocation) {
                                    $allocation_plan_student_loan_item->rate_type = $item_current_allocation->rate_type;
                                    $allocation_plan_student_loan_item->loan_award_percentage = $item_current_allocation->loan_award_percentage;
                                    $allocation_plan_student_loan_item->allocation_plan_student_id = $item_current_allocation->allocation_plan_student_id;
                                    $allocation_plan_student_loan_item->priority_order = $item_current_allocation->priority_order;
                                }
                                if ($allocation_plan_student_loan_item->save()) {
                                    /// echo '=== SUCCESS ALLOCATIONSTUDEN LN ITEM===';
                                }
                                $items_success++;
                            } else {
                                // echo '=== FAILURE===';
                                $items_failure++;
                            }
                        }
                    }

                    if ($items_success) {
                        $sms = 'Operation Successful, ' . $items_success . ' out of ' . ($items_success + $items_failure) . ' Loan Items Operation completed,Please check amount reflected in the Batch';
                        Yii::$app->session->setFlash('success', $sms);
                        return $this->redirect(['view-additional-batch', 'id' => $model->allocation_batch_id]);
                    }
                } else {
                    $sms = 'No Students Available for the selected Allocation Batch';
                    Yii::$app->session->setFlash('failure', $sms);
                }
            }
        }
        return $this->render('_add_additional_amount', [
                    'model' => $model, 'loan_items' => $loan_items, 'allocation' => $allocation
        ]);
    }

}
