<?php
namespace backend\modules\allocation\controllers;



use Yii;

use backend\modules\allocation\models\ProgrammeCost;

use backend\modules\allocation\models\ProgrammeCostSearch;

use yii\web\Controller;

use yii\web\NotFoundHttpException;

use yii\filters\VerbFilter;



/**

 * ProgrammeCostController implements the CRUD actions for ProgrammeCost model.

 */

class ProgrammeCostController extends Controller
{

    public function behaviors()

    {
        //default_main.php
        $this->layout = "default_main";

        return [

            'verbs' => [

                'class' => VerbFilter::className(),

                'actions' => [

                    'delete' => ['post'],

                ],

            ],

            'access' => [

                'class' => \yii\filters\AccessControl::className(),

                'rules' => [

                    [

                        'allow' => true,

                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'save-as-new'],

                        'roles' => ['@']

                    ],

                    [

                        'allow' => false

                    ]

                ]

            ]

        ];

    }



    /**

     * Lists all ProgrammeCost models.

     * @return mixed

     */

    public function actionIndex($id=null)

    {
        //echo $id;
//         /exit();

        $searchModel = new ProgrammeCostSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);



        return $this->render('index', [

            'searchModel' => $searchModel,

            'dataProvider' => $dataProvider,
             'programme_id'=>$id

        ]);

    }



    /**

     * Displays a single ProgrammeCost model.

     * @param integer $id
     * @return mixed

     */

    public function actionView($id)

    {

        $model = $this->findModel($id);

        return $this->render('view', [

            'model' => $this->findModel($id),

        ]);

    }



    /**

     * Creates a new ProgrammeCost model.

     * If creation is successful, the browser will be redirected to the 'view' page.

     * @return mixed

     */

    public function actionCreate()

    {

        $model = new ProgrammeCost();



        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['index', 'id' => $model->programme_id]);

        } else {

            return $this->render('create', [

                'model' => $model,

            ]);

        }

    }



    /**

     * Updates an existing ProgrammeCost model.

     * If update is successful, the browser will be redirected to the 'view' page.

     * @param integer $id
     * @return mixed

     */

    public function actionUpdate($id)

    {

        if (Yii::$app->request->post('_asnew') == '1') {

            $model = new ProgrammeCost();

        }else{

            $model = $this->findModel($id);

        }



        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            

            return $this->redirect(['index', 'id' => $model->programme_id]);

        } else {

            return $this->render('update', [

                'model' => $model,

            ]);

        }

    }



    /**

     * Deletes an existing ProgrammeCost model.

     * If deletion is successful, the browser will be redirected to the 'index' page.

     * @param integer $id
     * @return mixed

     */

    public function actionDelete($id)

    {

        $this->findModel($id)->deleteWithRelated();



        return $this->redirect(['index']);

    }



    /**

    * Creates a new ProgrammeCost model by another data,

    * so user don't need to input all field from scratch.

    * If creation is successful, the browser will be redirected to the 'view' page.

    *

    * @param mixed $id

    * @return mixed

    */

    public function actionSaveAsNew($id) {

        $model = new ProgrammeCost();



        if (Yii::$app->request->post('_asnew') != '1') {

            $model = $this->findModel($id);

        }

    

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['view', 'id' => $model->programme_cost_id]);

        } else {

            return $this->render('saveAsNew', [

                'model' => $model,

            ]);

        }

    }

    

    /**

     * Finds the ProgrammeCost model based on its primary key value.

     * If the model is not found, a 404 HTTP exception will be thrown.

     * @param integer $id
     * @return ProgrammeCost the loaded model

     * @throws NotFoundHttpException if the model cannot be found

     */

    protected function findModel($id)

    {

        if (($model = ProgrammeCost::findOne($id)) !== null) {

            return $model;

        } else {

            throw new NotFoundHttpException('The requested page does not exist.');

        }

    }

}


