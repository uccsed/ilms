<?php

namespace backend\modules\allocation\models;

use Yii;

/**
 * This is the model class for table "loan_item_detail".
 *
 * @property integer $loan_item_detail_id
 * @property integer $loan_item_id
 * @property string $loan_item_category
 * @property integer $study_level
 * @property integer $is_active
 * @property integer $list_order
 * @property string $place_of_study
 * @property integer $academic_year_id
 *
 * @property ApplicantCategory $studyLevel
 * @property LoanItem $loanItem
 */
class LoanItemDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loan_item_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loan_item_id','loan_item_category','study_level','list_order', 'place_of_study', 'academic_year_id'], 'required'],
            [['loan_item_id', 'study_level', 'is_active', 'list_order', 'academic_year_id'], 'integer'],
            [['place_of_study'], 'string'],
            [['loan_item_category'], 'string', 'max' => 200],
            [['study_level'], 'exist', 'skipOnError' => true, 'targetClass' => ApplicantCategory::className(), 'targetAttribute' => ['study_level' => 'applicant_category_id']],
            [['loan_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoanItem::className(), 'targetAttribute' => ['loan_item_id' => 'loan_item_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'loan_item_detail_id' => 'Loan Item Detail ID',
            'loan_item_id' => 'Loan Item ID',
            'loan_item_category' => 'Item Category',
            'study_level' => 'Study Level',
            'is_active' => 'Is Active',
            'list_order' => 'List Order',
            'place_of_study' => 'Place Of Study',
            'academic_year_id' => 'Academic Year',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudyLevel()
    {
        return $this->hasOne(ApplicantCategory::className(), ['applicant_category_id' => 'study_level']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanItem()
    {
        return $this->hasOne(LoanItem::className(), ['loan_item_id' => 'loan_item_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear() {
        return $this->hasOne(\backend\modules\allocation\models\AcademicYear::className(), ['academic_year_id' => 'academic_year_id']);
    }

   
}
