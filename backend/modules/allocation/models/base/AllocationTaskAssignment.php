<?php

namespace backend\modules\allocation\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "allocation_task_assignment".
 *
 * @property integer $allocation_task_assignment_id
 * @property integer $allocation_user_structure_id
 * @property integer $allocation_task_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property string $deleted_at
 *
 * @property \backend\modules\allocation\models\AllocationUserStructure $allocationUserStructure
 * @property \backend\modules\allocation\models\AllocationTaskDefinition $allocationTask
 */
class AllocationTaskAssignment extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'allocationUserStructure',
            'allocationTask'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['allocation_user_structure_id', 'allocation_task_id'], 'required'],
            [['allocation_user_structure_id', 'allocation_task_id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'allocation_task_assignment';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'allocation_task_assignment_id' => 'Allocation Task Assignment ID',
            'allocation_user_structure_id' => 'Allocation User Structure',
            'allocation_task_id' => 'Allocation Task',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationUserStructure()
    {
        return $this->hasOne(\backend\modules\allocation\models\AllocationUserStructure::className(), ['allocation_user_structure_id' => 'allocation_user_structure_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationTask()
    {
        return $this->hasOne(\backend\modules\allocation\models\AllocationTask::className(), ['allocation_task_id' => 'allocation_task_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => \Yii::$app->user->identity->user_id,
            ],
        ];
    }
}
