<?php

namespace backend\modules\allocation\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\User;

/**
 * This is the base model class for table "allocation_payoutlist_movement".
 *
 * @property integer $movement_id
 * @property integer $allocation_batch_id
 * @property integer $from_officer
 * @property integer $to_officer
 * @property integer $movement_status
 * @property integer $allocation_task_id
 * @property string $signature
 * @property string $date_out
 * @property string $comment
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property \backend\modules\allocation\models\AllocationBatch $allocationBatch
 * @property \backend\modules\allocation\models\AllocationTaskDefinition $allocationTask
 * @property \backend\modules\allocation\models\User $fromOfficer
 * @property \backend\modules\allocation\models\User $toOfficer
 */
class AllocationPayoutlistMovement extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public $order_level;
    public function relationNames()
    {
        return [
            'allocationBatch',
            'allocationTask',
            'fromOfficer',
            'toOfficer'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['allocation_batch_id', 'from_officer'], 'required'],
            [['allocation_batch_id', 'from_officer','allocation_next_task_id','to_officer','order_level','movement_status', 'allocation_task_id', 'created_by', 'updated_by'], 'integer'],
            [['date_out', 'created_at', 'updated_at','allocation_movement_id'], 'safe'],
            [['comment'], 'string'],
            [['signature'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'allocation_payoutlist_movement';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'movement_id' => 'Movement ID',
            'allocation_batch_id' => 'Allocation Batch ID',
            'from_officer' => 'From Officer',
            'to_officer' => 'To Officer',
            'movement_status' => 'Movement Status',
            'allocation_task_id' => 'Allocation Task ID',
            'signature' => 'Signature',
            'date_out' => 'Date Out',
            'comment' => 'Comment',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationBatch()
    {
        return $this->hasOne(\backend\modules\allocation\models\AllocationBatch::className(), ['allocation_batch_id' => 'allocation_batch_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationTask()
    {
        return $this->hasOne(\backend\modules\allocation\models\AllocationTask::className(), ['allocation_task_id' => 'allocation_task_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromOfficer()
    {
        return $this->hasOne(User::className(), ['user_id' => 'from_officer']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToOfficer()
    {
        return $this->hasOne(User::className(), ['user_id' => 'to_officer']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => \Yii::$app->user->identity->user_id,
            ],
        ];
    }
}
