<?php

namespace backend\modules\allocation\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "allocation_movement_schedule".
 *
 * @property integer $allocation_movement_schedule_id
 * @property integer $allocation_movement_id
 * @property integer $academic_year_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \backend\modules\allocation\models\AllocationMovement $allocationMovement
 * @property \backend\modules\allocation\models\AcademicYear $academicYear
 * @property \backend\modules\allocation\models\AllocationUserStructure[] $allocationUserStructures
 */
class AllocationMovementSchedule extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'allocationMovement',
            'academicYear',
            'allocationUserStructures'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['allocation_movement_id', 'academic_year_id'], 'required'],
            [['allocation_movement_id', 'academic_year_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'allocation_movement_schedule';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'allocation_movement_schedule_id' => 'Allocation Movement Schedule ID',
            'allocation_movement_id' => 'Allocation Movement',
            'academic_year_id' => 'Academic Year',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationMovement()
    {
        return $this->hasOne(\backend\modules\allocation\models\AllocationMovement::className(), ['allocation_movement_id' => 'allocation_movement_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(\backend\modules\allocation\models\AcademicYear::className(), ['academic_year_id' => 'academic_year_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationUserStructures()
    {
        return $this->hasMany(\backend\modules\allocation\models\AllocationUserStructure::className(), ['allocation_movement_schedule_id' => 'allocation_movement_schedule_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => \Yii::$app->user->identity->user_id,
            ],
        ];
    }
}
