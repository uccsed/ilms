<?php

namespace backend\modules\allocation\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\User;

/**
 * This is the base model class for table "allocation_user_structure".
 *
 * @property integer $allocation_user_structure_id
 * @property integer $allocation_structure_id
 * @property integer $allocation_movement_schedule_id
 * @property integer $allocation_task_id
 * @property integer $user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $status
 *
 * @property \backend\modules\allocation\models\AllocationTaskAssignment[] $allocationTaskAssignments
 * @property \backend\modules\allocation\models\AllocationStructure $allocationStructure
 * @property \backend\modules\allocation\models\User $user
 * @property \backend\modules\allocation\models\AllocationMovementSchedule $allocationMovementSchedule
 * @property \backend\modules\allocation\models\AllocationTaskDefinition $allocationTask
 */
class AllocationUserStructure extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'allocationTaskAssignments',
            'allocationStructure',
            'user',
            'allocationMovementSchedule',
            'allocationTask'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['allocation_structure_id', 'user_id','allocation_task_id','status','order_level'], 'required'],
            [['allocation_structure_id', 'allocation_movement_schedule_id', 'allocation_task_id', 'user_id', 'created_by', 'updated_by', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'allocation_user_structure';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'allocation_user_structure_id' => 'Allocation User Structure ID',
            'allocation_structure_id' => 'Allocation Structure ID',
            'allocation_movement_schedule_id' => 'Allocation Movement Schedule ID',
            'allocation_task_id' => 'Allocation Task ID',
            'user_id' => 'User ID',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationTaskAssignments()
    {
        return $this->hasMany(\backend\modules\allocation\models\AllocationTaskAssignment::className(), ['allocation_user_structure_id' => 'allocation_user_structure_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationStructure()
    {
        return $this->hasOne(\backend\modules\allocation\models\AllocationStructure::className(), ['allocation_structure_id' => 'allocation_structure_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationMovementSchedule()
    {
        return $this->hasOne(\backend\modules\allocation\models\AllocationMovementSchedule::className(), ['allocation_movement_schedule_id' => 'allocation_movement_schedule_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationTask()
    {
        return $this->hasOne(\backend\modules\allocation\models\AllocationTask::className(), ['allocation_task_id' => 'allocation_task_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => \Yii::$app->user->identity->user_id,
            ],
        ];
    }
}
