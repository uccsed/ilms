<?php

namespace backend\modules\allocation\models;

use Yii;

/**
 * This is the model class for table "allocation_movement".
 *
 * @property integer $allocation_movement_id
 * @property string $movement_name
 * @property string $description
 * @property integer $status
 */
class AllocationMovement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'allocation_movement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['movement_name', 'status'], 'required'],
            [['description'], 'string'],
            [['status'], 'integer'],
            [['movement_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'allocation_movement_id' => 'Allocation Movement ID',
            'movement_name' => 'Movement Name',
            'description' => 'Description',
            'status' => 'Status',
        ];
    }
}
