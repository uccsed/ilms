<?php

namespace backend\modules\allocation\models;

use Yii;
use \backend\modules\allocation\models\base\AllocationMovementSchedule as BaseAllocationMovementSchedule;

/**
 * This is the model class for table "allocation_movement_schedule".
 */
class AllocationMovementSchedule extends BaseAllocationMovementSchedule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['allocation_movement_id', 'academic_year_id'], 'required'],
            [['allocation_movement_id', 'academic_year_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ]);
    }
	
}
