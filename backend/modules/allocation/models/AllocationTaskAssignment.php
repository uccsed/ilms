<?php

namespace backend\modules\allocation\models;

use Yii;
use \backend\modules\allocation\models\base\AllocationTaskAssignment as BaseAllocationTaskAssignment;

/**
 * This is the model class for table "allocation_task_assignment".
 */
class AllocationTaskAssignment extends BaseAllocationTaskAssignment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['allocation_user_structure_id', 'allocation_task_id'], 'required'],
            [['allocation_user_structure_id', 'allocation_task_id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe']
        ]);
    }
	
}
