<?php

namespace backend\modules\allocation\models;

use Yii;

/**
 * This is the model class for table "allocation_batch_student".
 *
 * @property integer $allocation_batch_student_id
 * @property integer $allocation_batch_id
 * @property integer $application_id
 * @property string $sex
 * @property integer $programme_id
 * @property integer $cluster_definition_id
 * @property integer $allocation_history_id
 * @property integer $study_year
 */
class AllocationBatchStudent extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'allocation_batch_student';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['allocation_batch_id', 'application_id', 'programme_id', 'cluster_definition_id', 'allocation_history_id', 'study_year'], 'integer'],
            [['application_id', 'programme_id', 'allocation_history_id', 'study_year'], 'required'],
            [['sex'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'allocation_batch_student_id' => 'Allocation Batch Student ID',
            'allocation_batch_id' => 'Allocation Batch ID',
            'application_id' => 'Application ID',
            'sex' => 'Sex',
            'programme_id' => 'Programme ID',
            'cluster_definition_id' => 'Cluster Definition ID',
            'allocation_history_id' => 'Allocation History ID',
            'study_year' => 'Study Year',
        ];
    }

    public function getAllocationBatch() {
        return $this->hasOne(AllocationBatch::className(), ['allocation_batch_id' => 'allocation_batch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplication() {
        return $this->hasOne(\backend\modules\application\models\Application::className(), ['application_id' => 'application_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationHistory() {
        return $this->hasOne(AllocationHistory::className(), ['loan_allocation_history_id' => 'allocation_history_id']);
    }

    static function CreateAllocationBatchStudent($allocation, $allocation_history) {
        $exist = self::find()->where(['allocation_batch_id' => $allocation->allocation_batch_id,
                    'application_id' => $allocation->application_id,
                    'allocation_history_id' => $allocation_history['allocation_history_id']])->exists();
        if (!$exist) {
            $allocation_student_details = new AllocationBatchStudent;
            $allocation_student_details->allocation_batch_id = $allocation->allocation_batch_id;
            $allocation_student_details->application_id = $allocation->application_id;
            $allocation_student_details->allocation_history_id = $allocation_history['allocation_history_id'];
            $allocation_student_details->sex = self::getApplicantGenderByApplicationId($allocation->application_id);
            $allocation_student_details->programme_id = $allocation_history['programme_id'];
            $allocation_student_details->cluster_definition_id = self::getProgrammeClusterByProgrammeId($allocation_history['programme_id'], $allocation->allocationBatch->academic_year_id);
            $allocation_student_details->study_year = $allocation_history['study_year'];
            return $allocation_student_details->save();
        }
    }

    static function getApplicantGenderByApplicationId($application_id) {
        $data = \backend\modules\application\models\Applicant::findBySql("SELECT sex FROM applicant INNER JOIN application ON applicant.applicant_id=application.applicant_id WHERE application.application_id=:application_id", [':application_id' => $application_id])->one();
        if ($data) {
            return $data->sex;
        }
        return NULL;
    }

    static function getProgrammeClusterByProgrammeId($programme_id, $academic_year) {
        $data = ClusterProgramme::findBySql("SELECT cluster_definition_id FROM cluster_programme 
                                      WHERE programme_id=:programme_id AND academic_year_id=:academic_year", [':programme_id' => $programme_id, ':academic_year' => $academic_year])->one();
        if ($data) {
            return $data->cluster_definition_id;
        }
        return NULL;
    }

}
