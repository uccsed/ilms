<?php

namespace backend\modules\allocation\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\allocation\models\LoanItemDetail;

/**
 * LoanItemDetailSearch represents the model behind the search form about `backend\modules\allocation\models\LoanItemDetail`.
 */
class LoanItemDetailSearch extends LoanItemDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loan_item_detail_id', 'loan_item_id', 'study_level', 'is_active', 'list_order', 'academic_year_id'], 'integer'],
            [['loan_item_category', 'place_of_study'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoanItemDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'loan_item_detail_id' => $this->loan_item_detail_id,
            'loan_item_id' => $this->loan_item_id,
            'study_level' => $this->study_level,
            'is_active' => $this->is_active,
            'list_order' => $this->list_order,
            'academic_year_id' => $this->academic_year_id,
        ]);

        $query->andFilterWhere(['like', 'loan_item_category', $this->loan_item_category])
            ->andFilterWhere(['like', 'place_of_study', $this->place_of_study]);

        return $dataProvider;
    }
}
