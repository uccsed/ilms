<?php

namespace backend\modules\allocation\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\allocation\models\AllocationMovementSchedule;

/**
 * backend\modules\allocation\models\AllocationMovementScheduleSearch represents the model behind the search form about `backend\modules\allocation\models\AllocationMovementSchedule`.
 */
 class AllocationMovementScheduleSearch extends AllocationMovementSchedule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['allocation_movement_schedule_id', 'allocation_movement_id', 'academic_year_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AllocationMovementSchedule::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'allocation_movement_schedule_id' => $this->allocation_movement_schedule_id,
            'allocation_movement_id' => $this->allocation_movement_id,
            'academic_year_id' => $this->academic_year_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
