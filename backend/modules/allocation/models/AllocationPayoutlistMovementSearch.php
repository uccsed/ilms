<?php

namespace backend\modules\allocation\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\allocation\models\AllocationPayoutlistMovement;

/**
 * backend\modules\allocation\models\AllocationPayoutlistMovementSearch represents the model behind the search form about `backend\modules\allocation\models\AllocationPayoutlistMovement`.
 */
 class AllocationPayoutlistMovementSearch extends AllocationPayoutlistMovement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['movement_id', 'allocation_batch_id', 'from_officer', 'to_officer', 'movement_status', 'allocation_task_id', 'created_by', 'updated_by'], 'integer'],
            [['signature', 'date_out', 'comment', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = AllocationPayoutlistMovement::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'movement_id' => $this->movement_id,
            'allocation_batch_id' => $this->allocation_batch_id,
            'from_officer' => $this->from_officer,
            'to_officer' => $this->to_officer,
            'movement_status' => $this->movement_status,
            'allocation_task_id' => $this->allocation_task_id,
            'date_out' => $this->date_out,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'signature', $this->signature])
              ->andFilterWhere(['like', 'allocation_batch_id', $id])
              ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
