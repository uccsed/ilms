<?php

namespace backend\modules\allocation\models;

use Yii;
use \backend\modules\allocation\models\base\AllocationPayoutlistMovement as BaseAllocationPayoutlistMovement;

/**
 * This is the model class for table "allocation_payoutlist_movement".
 */
class AllocationPayoutlistMovement extends BaseAllocationPayoutlistMovement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['allocation_batch_id', 'from_officer'], 'required'],
            [['allocation_batch_id','allocation_next_task_id','from_officer', 'to_officer', 'movement_status','order_level','allocation_task_id', 'created_by', 'updated_by','allocation_movement_id'], 'integer'],
            [['date_out', 'created_at', 'updated_at'], 'safe'],
            [['comment'], 'string'],
            [['signature'], 'string', 'max' => 200]
        ]);
    }
	
}
