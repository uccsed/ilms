<?php

namespace backend\modules\allocation\models;

use Yii;
use \backend\modules\allocation\models\base\AllocationBatch as BaseAllocationBatch;

/**
 * This is the model class for table "allocation_batch".
 */
class AllocationBatch extends BaseAllocationBatch {

    /**
     * @inheritdoc
     */
    public $institution;
    public $programme;
    public $study_year;
    public $study_country;
    public $data_file;
    public $students_list;
    public $student;
    public $study_level;

    public function rules() {
        return array_replace_recursive(parent::rules(), [
            [['contained_student', 'batch_desc'], 'required'],
             ///additional validation for additional for excel batch,special batch, additional batch by charles, was not included in merging
            [['students_list',], 'required', 'on' => 'additional_batch', 'message' => 'no Students available meeting you selection Criteria'],
            [['study_country', 'study_level'], 'required', 'on' => 'additional_batch'],
            [['data_file'], 'file', 'skipOnEmpty' => TRUE, 'maxSize' => 10240000,], //'extensions' => 'xlsx, xls',
            /// end additional for excel batch,special batch, additional batch
            [['batch_number', 'academic_year_id', 'status', 'is_reviewed', 'review_by', 'is_approved', 'created_by', 'updated_by', 'is_canceled', 'disburse_status'], 'integer'],
            [['available_budget'], 'number'],
            [['approval_comment', 'cancel_comment'], 'string'],
            [['created_at', 'updated_at', 'batch_number', 'academic_year_id', 'review_at', 'ed_approve_attachment', 'larc_approve_attachment', 'available_budget'], 'safe'],
            [['batch_desc'], 'string', 'max' => 45],
            [['disburse_comment'], 'string', 'max' => 300]
        ]);
    }

    /* OLD MIKIDAD 
      public function putAllocationStudentHistoryBatch($allocation_history_id, $academic_year_id) {
      $sql = "SELECT `loan_item_id`,  `total_amount_awarded`,`application_id` FROM `allocation_plan_student` aps join allocation_plan_student_loan_item asl  on aps.`allocation_plan_student_id`=asl.`allocation_plan_student_id` WHERE `allocation_history_id` IN($allocation_history_id) AND `academic_year_id`='{$academic_year_id}'";
      return Yii::$app->db->createCommand($sql)->queryAll();
      } */

    /*
     * New Charles: added only colums to retrieve
     * added column: ,study_year,allocation_history_id,allocation_plan_id,programme_id
     */
    public function putAllocationStudentHistoryBatch($allocation_history_id, $academic_year_id) {
        $sql = "SELECT `loan_item_id`,  `total_amount_awarded`,`application_id`,study_year,allocation_history_id,allocation_plan_id,programme_id FROM `allocation_plan_student` aps join allocation_plan_student_loan_item asl  on aps.`allocation_plan_student_id`=asl.`allocation_plan_student_id` WHERE `allocation_history_id` IN($allocation_history_id) AND `academic_year_id`='{$academic_year_id}'";
        return Yii::$app->db->createCommand($sql)->queryAll();
    }

    public function countAllocationStudentHistory($allocation_history_id, $academic_year_id) {
        $sql = "SELECT count(*) FROM `allocation_plan_student` aps join allocation_plan_student_loan_item asl  on aps.`allocation_plan_student_id`=asl.`allocation_plan_student_id` WHERE `allocation_history_id` IN($allocation_history_id) AND `academic_year_id`='{$academic_year_id}'";
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    function getNewAwardeeStudents() {
        $params = [':academic_year' => $this->academic_year_id,
            ':contained_student' => $this->contained_student,
            ':country' => AllocationHistory::PLACE_TZ,
            ':allocation_plan_stage' => 1,
        ];
        /////filtering by programme of study
        if ($this->programme) {
            $additional = 'AND  programme_id IN (:programme)';
            $params[':programme'] = implode(',', $this->programme);
        } else {
            ////filtering by institution
            if ($this->institution) {
                $additional = 'AND programme_id IN (
                  SELECT programme_id
                  FROM programme
                  WHERE learning_institution_id IN(:institution)
                  )';
                $params[':institution'] = implode(',', $this->institution);
            } else {
                ////filtering student by country of study
                $additional = 'AND  programme_id IN (
                  SELECT programme_id
                  FROM programme
                  WHERE learning_institution_id IN(SELECT learning_institution_id
                  FROM learning_institution
                  WHERE  country IN (:country) )
                 )';
                if ($this->study_country == AllocationHistory::PLACE_FCOUNTRY) {
                    $additional = 'AND  programme_id IN (
                  SELECT programme_id
                  FROM programme
                  WHERE learning_institution_id IN(SELECT learning_institution_id
                  FROM learning_institution
                  WHERE country NOT IN (:country) )
                     
                 )';
                }
            }
        }
        ////filtering student by study year
        if ($this->study_year) {
            $additional .= 'AND allocation_plan_student.study_year IN(:study_year)';
            $params[':study_year'] = implode(',', $this->study_year);
        }
        ///filter by containing student allocation type
        if ($this->contained_student) {
            $additional .= ' AND allocation_plan_student.allocation_type=:allocation_type';
            $params[':allocation_type'] = AllocationPlanStudent::ALLOCATION_TYPE_FIRST_TIME;
        }
        //        echo '<pre/>';
        //        var_dump($params);


        $sql = 'SELECT MAX(allocation_plan_student.application_id) AS application_id,allocation_plan_student.academic_year_id,
                        allocation_plan_student.study_year,allocation_plan_student.student_fee,
                        allocation_plan_student.programme_id,allocation_plan_student.programme_cost
                FROM allocation_plan_student
                INNER JOIN allocation_plan ON allocation_plan_student.allocation_plan_id=allocation_plan.allocation_plan_id
                WHERE  allocation_plan_student.academic_year_id=:academic_year
                AND allocation_plan.allocation_plan_stage=:allocation_plan_stage
                AND allocation_plan_student.application_id IN(
                SELECT DISTINCT allocation.application_id AS application_id
                FROM allocation
                INNER JOIN allocation_batch ON allocation_batch.allocation_batch_id=allocation.allocation_batch_id
                WHERE
                allocation_batch.academic_year_id=:academic_year
                AND  allocation_batch.contained_student=:contained_student
                AND allocation_batch.allocation_batch_id IN(
                    SELECT allocation_batch_id
                   FROM allocation_batch WHERE allocation_batch.academic_year_id=:academic_year AND allocation_batch.contained_student=:contained_student
                   )
                )
               ' . $additional . ' GROUP BY allocation_plan_student.application_id';

        //        echo $sql;
        //        exit;
        return AllocationPlanStudent::findBySql($sql, $params)->all();
    }

    function getContinuingAwardeeStudents() {
        $params = [':academic_year' => $this->academic_year_id,
            ':contained_student' => $this->contained_student,
            ':country' => AllocationHistory::PLACE_TZ,
            ':allocation_plan_stage' => 1,
        ];
        /////filtering by programme of study
        if ($this->programme) {
            $additional = 'AND  programme_id IN (:programme)';
            $params[':programme'] = implode(',', $this->programme);
        } else {
            ////filtering by institution
            if ($this->institution) {
                $additional = 'AND programme_id IN (
                  SELECT programme_id
                  FROM programme
                  WHERE learning_institution_id IN(:institution)
                  )';
                $params[':institution'] = implode(',', $this->institution);
            } else {
                ////filtering student by country of study
                $additional = 'AND  programme_id IN (
                  SELECT programme_id
                  FROM programme
                  WHERE learning_institution_id IN(SELECT learning_institution_id
                  FROM learning_institution
                  WHERE  country IN (:country) )
                 )';
                if ($this->study_country == AllocationHistory::PLACE_FCOUNTRY) {
                    $additional = 'AND  programme_id IN (
                  SELECT programme_id
                  FROM programme
                  WHERE learning_institution_id IN(SELECT learning_institution_id
                  FROM learning_institution
                  WHERE country NOT IN (:country) )
                     
                 )';
                }
            }
        }
        ////filtering student by study year
        if ($this->study_year) {
            $additional .= 'AND allocation_plan_student.study_year IN(:study_year)';
            $params[':study_year'] = implode(',', $this->study_year);
        }
        ///filter by containing student allocation type
        if ($this->contained_student) {
            $additional .= ' AND allocation_plan_student.allocation_type=:allocation_type';
            $params[':allocation_type'] = AllocationPlanStudent::ALLOCATION_TYPE_FIRST_TIME;
        }
        //        echo '<pre/>';
        //        var_dump($params);


        $sql = 'SELECT MAX(allocation_plan_student.application_id) AS application_id,allocation_plan_student.academic_year_id,
                        allocation_plan_student.study_year,allocation_plan_student.student_fee,
                        allocation_plan_student.programme_id,allocation_plan_student.programme_cost
                FROM allocation_plan_student
                INNER JOIN allocation_plan ON allocation_plan_student.allocation_plan_id=allocation_plan.allocation_plan_id
                WHERE  allocation_plan_student.academic_year_id=:academic_year
                AND allocation_plan.allocation_plan_stage=:allocation_plan_stage
                AND allocation_plan_student.application_id IN(
                SELECT DISTINCT allocation.application_id AS application_id
                FROM allocation
                INNER JOIN allocation_batch ON allocation_batch.allocation_batch_id=allocation.allocation_batch_id
                WHERE
                allocation_batch.academic_year_id=:academic_year
                AND  allocation_batch.contained_student=:contained_student
                AND allocation_batch.allocation_batch_id IN(
                    SELECT allocation_batch_id
                   FROM allocation_batch WHERE allocation_batch.academic_year_id=:academic_year AND allocation_batch.contained_student=:contained_student
                   )
                ) AND allocation_batch.study_year > 1
               ' . $additional . ' GROUP BY allocation_plan_student.application_id';

        //        echo $sql;
        //        exit;
        return AllocationPlanStudent::findBySql($sql, $params)->all();
    }

}
