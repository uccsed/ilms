<?php

namespace backend\modules\allocation\models;

use Yii;

/**
 * This is the model class for table "allocation_plan_special_group_criteria".
 *
 * @property integer $allocation_special_group_criteria_id
 * @property integer $allocation_plan_id
 * @property integer $special_group_id
 * @property integer $allocation_group_criteria_id
 *
 * @property Criteria $allocationGroupCriteria
 * @property AllocationPlan $allocationPlan
 * @property AllocationPlanSpecialGroup $specialGroup
 */
class AllocationPlanSpecialGroupCriteria extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'allocation_plan_special_group_criteria';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['allocation_plan_id', 'special_group_id', 'allocation_group_criteria_id'], 'required'],
            [['allocation_plan_id', 'special_group_id', 'allocation_group_criteria_id'], 'integer'],
            [['allocation_group_criteria_id'], 'exist', 'skipOnError' => true, 'targetClass' => Criteria::className(), 'targetAttribute' => ['allocation_group_criteria_id' => 'criteria_id']],
            [['allocation_plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => AllocationPlan::className(), 'targetAttribute' => ['allocation_plan_id' => 'allocation_plan_id']],
            [['special_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => AllocationPlanSpecialGroup::className(), 'targetAttribute' => ['special_group_id' => 'special_group_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'allocation_special_group_criteria_id' => 'Allocation Special Group Criteria ID',
            'allocation_plan_id' => 'Allocation Plan ID',
            'special_group_id' => 'Special Group ID',
            'allocation_group_criteria_id' => 'Allocation Group Criteria ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationGroupCriteria() {
        return $this->hasOne(Criteria::className(), ['criteria_id' => 'allocation_group_criteria_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationPlan() {
        return $this->hasOne(AllocationPlan::className(), ['allocation_plan_id' => 'allocation_plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialGroup() {
        return $this->hasOne(AllocationPlanSpecialGroup::className(), ['special_group_id' => 'special_group_id']);
    }

    static function getCriteriaNamesByGroupID($special_group_id) {
        $sql = "SELECT criteria_description,criteria_id 
                  FROM criteria 
                  INNER JOIN allocation_plan_special_group_criteria
                  ON  allocation_plan_special_group_criteria.allocation_group_criteria_id=criteria.criteria_id
                  WHERE special_group_id=:special_group_id";
        return self::findBySql($sql, [':special_group_id' => $special_group_id])->asArray()->all();
    }

    static function getCriteriaNameByGroupID($special_group_id) {
        $data = self::getCriteriaNamesByGroupID($special_group_id);
        $data_list = '';
        if ($data) {
            foreach ($data as $key => $data) {
                $data_list .=', ' . ' ' . $data['criteria_description'];
            }
            $data_list = substr($data_list, 1);
        }
        return $data_list;
    }

}
