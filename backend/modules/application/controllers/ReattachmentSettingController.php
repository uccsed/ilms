<?php

namespace backend\modules\application\controllers;

use Yii;
use backend\modules\application\models\ReattachmentSetting;
use backend\modules\application\models\ReattachmentSettingSearch;
//use yii\web\Controller;
use common\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReattachmentSettingController implements the CRUD actions for ReattachmentSetting model.
 */
class ReattachmentSettingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
		$this->layout = "main_private";
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReattachmentSetting models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReattachmentSettingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ReattachmentSetting model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ReattachmentSetting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ReattachmentSetting();
        $created_by=Yii::$app->user->identity->user_id;
        $created_at=date("Y-m-d H:i:s");
        $updated_by=Yii::$app->user->identity->user_id;
        $updated_at=date("Y-m-d H:i:s");
        if ($model->load(Yii::$app->request->post())) {
            $verification_status=$model->verification_status;
            $status_flag=$model->status_flag;
            $array=$model->comment_id;
            if($status_flag==0){
$resultsCount1=ReattachmentSetting::find()->where(['verification_status'=>$verification_status,'status_flag'=>0])->count();
if($resultsCount1==0){
$resultsCount=ReattachmentSetting::find()->where(['verification_status'=>$verification_status,'is_active'=>1])->count(); 
if($resultsCount==0){
$model->created_by = $created_by;
$model->created_at = $created_at;
$model->updated_by = $created_by;
$model->updated_at = $created_at;
$model->save();
// here for logs
                        $old_data=\yii\helpers\Json::encode($model->attributes);						
			$new_data=\yii\helpers\Json::encode($model->attributes);
			$model_logs=\common\models\base\Logs::CreateLogall($model->reattachment_setting_id,$old_data,$new_data,"reattachment_setting","CREATE",1);
}				//end for logs
}
            }else if($status_flag==1){
foreach ($array as $value) {
$ReattachmentSettingModel = new ReattachmentSetting();
$ReattachmentSettingModel->verification_status =$verification_status;
$ReattachmentSettingModel->comment_id = $value;
$ReattachmentSettingModel->created_by = $created_by;
$ReattachmentSettingModel->created_at = $created_at;
$ReattachmentSettingModel->updated_by = $created_by;
$ReattachmentSettingModel->updated_at = $created_at;
$ReattachmentSettingModel->status_flag = $status_flag;
$resultsCount1=ReattachmentSetting::find()->where(['verification_status'=>$verification_status,'status_flag'=>0,'is_active'=>1])->count();
if($resultsCount1==0){
$resultsCount=ReattachmentSetting::find()->where(['verification_status'=>$verification_status,'comment_id'=>$value])->count();
if($resultsCount==0){
                  	
$ReattachmentSettingModel->save(false);
// here for logs
                        $old_data=\yii\helpers\Json::encode($ReattachmentSettingModel->attributes);						
			$new_data=\yii\helpers\Json::encode($ReattachmentSettingModel->attributes);
			$model_logs=\common\models\base\Logs::CreateLogall($ReattachmentSettingModel->reattachment_setting_id,$old_data,$new_data,"reattachment_setting","CREATE",1);
				//end for logs
}
}
} 
}         
            $sms="<p>Operation successful</p>";
            Yii::$app->getSession()->setFlash('success', $sms);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ReattachmentSetting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        // here for logs
                        $old_data=\yii\helpers\Json::encode($model->oldAttributes);
        //end for logs
                        $old_is_ative_flag=$model->is_active;
        if ($model->load(Yii::$app->request->post())) {
            $verification_status=$model->verification_status;
            $value=$model->comment_id;
            $isActive=$model->is_active;
            $status_flag=$model->status_flag;
            if($isActive==1 && $status_flag==0 && $old_is_ative_flag==0){
$resultsCount=ReattachmentSetting::find()->where(['verification_status'=>$verification_status,'is_active'=>1])->count();
if($resultsCount==0){
    $model->save();
}
}else if($isActive==1 && $status_flag==1 && $old_is_ative_flag==0){
$resultsCount2=ReattachmentSetting::find()->where(['verification_status'=>$verification_status,'status_flag'=>0,'is_active'=>1])->count();
if($resultsCount2==0){
    $model->save();
}
}else if($isActive==0){
    $model->save();
}
            // here for logs						
			$new_data=\yii\helpers\Json::encode($model->attributes);
			$model_logs=\common\models\base\Logs::CreateLogall($model->reattachment_setting_id,$old_data,$new_data,"reattachment_setting","CREATE",1);
				//end for logs
            $sms="<p>Operation successful</p>";
            Yii::$app->getSession()->setFlash('success', $sms);            
            return $this->redirect(['index']);

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ReattachmentSetting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ReattachmentSetting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ReattachmentSetting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReattachmentSetting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
