<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use common\models\ApplicantQuestion;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\Application */

$this->title ="Application Verification";
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    img {
    border: 1px solid #ddd;
    border-radius: 4px;
    padding: 5px;
    width: 150px;
}

img:hover {
    box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}
</style>
<div class="application-view">
   <div class="panel panel-info">
        <div class="panel-heading">
       <?= Html::encode($this->title) ?>
        </div>


   <div class="panel-body">

    <div class="row" style="margin: 1%;">
        <div class="col-xs-4">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">APPLICANT PHOTO</h3>
                </div>     
              <img class="img" src="uploadimage/profile/<?=$model->passport_photo?>" alt="">
          </div>
        </div>

        <div class="col-xs-8">
            <div class="box box-primary">
              <div class="box-header">
                    <h3 class="box-title">APPLICANT DETAILS</h3>
              </div>     
               <p>
                 &nbsp;Full Name:-<b><?= $model->applicant->user->firstname.' '.$model->applicant->user->middlename.' '.$model->applicant->user->surname;?></b><br/>
                 &nbsp;Form IV Index No:- <b><?= $model->applicant->f4indexno;?></b><br/>
                 <br/><br/><br/>
              </p>
          </div>
        </div>
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">VERIFY APPLICANT PHOTO</h3>
                </div>
                  <div class="bank-form" style="margin: 1%;overflow: hidden">
                    <?php 
                       $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]); 
                        echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' => 2,
                         'attributes' => [                 
                            'passport_photo_verified' => [
                               'type' => Form::INPUT_DROPDOWN_LIST,
                               'items' => ApplicantQuestion::getVerificationStatus(), 
                               'options' => ['prompt' => '-- Select --'],
                               'label' => 'Photo Verification Status',
                               ],
                            'passport_photo_comment' => [
                               'type' => Form::INPUT_TEXT,
                               'options' => ['placeholder' => 'Enter Comment'],
                             ],
                           ]
                      ]);
                     echo Html::submitButton('Submit', ['class' => 'btn btn-success', 'style' => 'float: right;margin:5px;']);
                     ActiveForm::end();
                  ?>
                </div> 
            </div>
        </div>
    </div>
           
  
  <div class="row" style="margin: 1%;">  
      <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">VERIFY APPLICANT ATTACHMENTS</h3>
                </div>     
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '',
        'columns' => [
             ['class' => 'yii\grid\SerialColumn'],            
              [
                'class' => 'kartik\grid\ExpandRowColumn',
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'allowBatchToggle' => true,
                'detail' => function ($model) {
                  return $this->render('verification',['model'=>$model]);  
                },
                'detailOptions' => [
                    'class' => 'kv-state-enable',
                ],
            ],
            /* [
               'attribute' => 'question',
                'label' => 'Question'
             ],*/
             [
               'attribute' => 'attachment_desc',
                'label' => 'Verification Items'
             ],          
             [
                'attribute' => 'verification_status',
                    'value' => function ($data) {
                       if($data["verification_status"] == 0){
                            return Html::label("UNVERIFIED", NULL, ['class'=>'label label-default']);
                            } else if($data["verification_status"] == 1) {
                            return Html::label("VALID", NULL, ['class'=>'label label-success']);
                            }
                              else if($data["verification_status"] == 2) {
                            return Html::label("INVALID", NULL, ['class'=>'label label-danger']);
                            }
                               else if($data["verification_status"] == 3) {
                                        return Html::label("WAITING", NULL, ['class'=>'label label-warning']);
                                    }
                        },
                        'filterInputOptions' => ['placeholder' => 'Search'],
                        'format' => 'raw'
                    ], 

             [
               'attribute' => 'comment',
                'label' => 'Verification Status Reason'
             ],
           ],
      ]); ?>
</div>
</div>          
</div>
   </div>
</div>