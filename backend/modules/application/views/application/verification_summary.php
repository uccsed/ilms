<?php
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;

$this->title = 'Verification Summary'
?>
<div class="application-index">
<div class="panel panel-info">
        <div class="panel-heading">
         
<?= Html::encode($this->title) ?>
          
        </div>
        <div class="panel-body">
            <?php  echo $this->render('_search_application_verification_report_summary', ['model' => $searchModel,'action'=>'verification-summary']); ?>
            <?php  $searchReports=$this->render('_search_verification_report_print', ['searchModel' => $searchModel]); 
                   $searchReportsC=$this->render('_search_verification_report_criteria', ['searchModel' => $searchModel]);
                   $searchReportsCexcel=$this->render('_search_verification_report_criteria_excel', ['searchModel' => $searchModel]);
              ?>
            <br/><br/>
            <div class="text-right">
                <?php 
                $date_verified=$searchModel->date_verified;
                $date_verified2=$searchModel->date_verified2;
				$totalApplication_new = 0;
				$totalunverified=0;
				$totalpending=0;
				$totalincomplete=0;
				$totalwaiting=0;
				$totalinvalid=0;
				$totalcomplete=0;
				$totalattempted=0;
    if (!empty($dataProvider->getModels())) {
        foreach ($dataProvider->getModels() as $key => $val) {
			$applica22=\backend\modules\application\models\VerificationAssignment::getApplicationAttemptedSummary($val->assignee);
            $totalApplication_new += $applica22->totalApplication;
			$totalunverified += $applica22->not_attempted;
			
			$applica33=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($val->assignee,$date_verified,$date_verified2);
			$totalpending +=$applica33->pending;
			$totalincomplete +=$applica33->incomplete;
			$totalwaiting +=$applica33->waiting;
			$totalinvalid +=$applica33->invalid;
			$totalcomplete +=$applica33->complete;
			$totalattempted +=$applica33->attempted;
        }
    }
                /*
                $verification_status2 =8;
echo Html::a('Export PDF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp', Url::toRoute(['verification-print-reportpdf', 'searches' =>$searchReports,'verification_status'=>$verification_status2,'criteriaSearchV'=>$searchReportsC]),
                ['target' => '_blank']);
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; echo Html::a('Export Excel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp', Url::toRoute(['verification-print-reportexcel', 'searches' =>$searchReports,'verification_status'=>$verification_status2,'criteriaSearchV'=>$searchReportsCexcel]));
                 * 
                 */
                ?>
                </div>
<?php
$gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],
   
                    [
                     'attribute' => 'officer',
                     'label'=>'Officer',
                        'value' => function ($model) {
                                   
                                     return $model->assignee0->firstname." ".$model->assignee0->middlename." ".$model->assignee0->surname;
                                    
                        }, 
                    'footer'=>'Total',						
                    ],
                    [
                     'attribute' => 'totalApplication',
                     'label'=>'Total Applications',					
                        'value' => function ($model) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getApplicationAttemptedSummary($model->assignee);
                                     return number_format($results->totalApplication);
                                    
                        },
                        'format' => 'raw',
						'footer'=>number_format($totalApplication_new),	
                    ],            
                    [
                     'attribute' => 'unverified',
                     'label'=>'Unverified',
                        'value' => function ($model) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getApplicationAttemptedSummary($model->assignee);
                                     return number_format($results->not_attempted);
                                    
                        },
                        'format' => 'raw',
						'footer'=>number_format($totalunverified),
                    ], 
                                [
                     'attribute' => 'pending',
                     'label'=>'Pending',
                        'value' => function ($model) use($date_verified,$date_verified2) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($model->assignee,$date_verified,$date_verified2);
                                     return number_format($results->pending);
                                    
                        },
                        'format' => 'raw',
						'footer'=>number_format($totalpending),
                    ],
                                [
                     'attribute' => 'incomplete',
                     'label'=>'Incomplete',
                        'value' => function ($model) use($date_verified,$date_verified2) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($model->assignee,$date_verified,$date_verified2);
                                     return number_format($results->incomplete);
                                    
                        },
                        'format' => 'raw',
						'footer'=>number_format($totalincomplete),
                    ],
                                [
                     'attribute' => 'waiting',
                     'label'=>'Waiting',
                        'value' => function ($model) use($date_verified,$date_verified2) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($model->assignee,$date_verified,$date_verified2);
                                     return number_format($results->waiting);
                                    
                        },
                        'format' => 'raw',
						'footer'=>number_format($totalwaiting),
                    ],
                                [
                     'attribute' => 'invalid',
                     'label'=>'Invalid',
                        'value' => function ($model) use($date_verified,$date_verified2) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($model->assignee,$date_verified,$date_verified2);
                                     return number_format($results->invalid);
                                    
                        },
                        'format' => 'raw',
						'footer'=>number_format($totalinvalid),
                    ],
                                [
                     'attribute' => 'complete',
                     'label'=>'Complete',
                        'value' => function ($model) use($date_verified,$date_verified2) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($model->assignee,$date_verified,$date_verified2);
                                     return number_format($results->complete);
                                    
                        },
                        'format' => 'raw',
						'footer'=>number_format($totalcomplete),
                    ],
                                [
                     'attribute' => 'attempted',
                     'label'=>'Attempted',
                        'value' => function ($model) use($date_verified,$date_verified2) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($model->assignee,$date_verified,$date_verified2);
                                     return number_format($results->attempted);
                                    
                        },
                        'format' => 'raw',
						'footer'=>number_format($totalattempted),
                    ],                                       
];
echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'pjax' => true,
    'bordered' => true,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
    'showPageSummary' => false,
	'showFooter'=>TRUE,
    'footerRowOptions'=>['style'=>'font-weight:bold;'],
//'columns' =>$grid_columns,
    /*
    'panel' => [
        'type' => GridView::TYPE_PRIMARY
    ],
     * 
     */
]);
?>
</div>
</div>
</div>
