<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use common\models\ApplicantQuestion;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\application\models\ApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="application-index">
<div class="panel panel-info">
        <div class="panel-heading">         
            <?php //echo Html::encode($this->title) ?>         
        </div>
        <div class="panel-body">

     <?php
       if($model['question_answer']){?>
        <p>
           <iframe src="../applicant_attachment/<?=$model['question_answer']?>" style="width:900px; height:300px;" frameborder="0"></iframe>
        </p>
     <?php
        }else{
            echo "<p><front color='red'>NO ATTACHEMENT ATTACHED</front></p>";
          }     
      ?>

     <div class="bank-form">

       <?php 
         $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]); 
         $applicat_question = ApplicantQuestion::findOne($model['applicant_question_id']);
       ?>
  <div class="col-lg-12">
    <?php
       echo Form::widget([
        'model' => $applicat_question,
        'form' => $form,
        'columns' => 2,
        'attributes' => [                 
            'verification_status' => [
                'type' => Form::INPUT_DROPDOWN_LIST,
                'items' => ApplicantQuestion::getVerificationStatus(), 
                'options' => ['prompt' => '-- Select --'],
              ],
            'comment' => [
                'type' => Form::INPUT_TEXT,
                'options' => ['placeholder' => 'Enter Comment'],
              ],
            'applicant_question_id'=>[
                    'type'=>Form::INPUT_HIDDEN, 
                ],
          ]
    ]);
       ?>
  </div>
    <div class="pull-right">
      <?php
    echo Html::submitButton('Submit', ['class' => 'btn btn-success']);
    ActiveForm::end();
   ?>
    </div>
     </div>
</div>
</div>