<?php
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;

$this->title = 'Verification Summary'
?>
<div class="application-index">
<div class="panel panel-info">
        <div class="panel-heading">
         
<?= Html::encode($this->title) ?>
          
        </div>
        <div class="panel-body">
            <?php  echo $this->render('_search_application_verification_report_summary', ['model' => $searchModel,'action'=>'verification-summary']); ?>
            <?php  $searchReports=$this->render('_search_verification_report_print', ['searchModel' => $searchModel]); 
                   $searchReportsC=$this->render('_search_verification_report_criteria', ['searchModel' => $searchModel]);
                   $searchReportsCexcel=$this->render('_search_verification_report_criteria_excel', ['searchModel' => $searchModel]);
              ?>
            <br/><br/>
            <div class="text-right">
                <?php 
                $date_verified=$searchModel->date_verified;
                $date_verified2=$searchModel->date_verified2;
                /*
                $verification_status2 =8;
echo Html::a('Export PDF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp', Url::toRoute(['verification-print-reportpdf', 'searches' =>$searchReports,'verification_status'=>$verification_status2,'criteriaSearchV'=>$searchReportsC]),
                ['target' => '_blank']);
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; echo Html::a('Export Excel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp', Url::toRoute(['verification-print-reportexcel', 'searches' =>$searchReports,'verification_status'=>$verification_status2,'criteriaSearchV'=>$searchReportsCexcel]));
                 * 
                 */
                ?>
                </div>
<?php
$gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],
   
                    [
                     'attribute' => 'officer',
                     'label'=>'Officer',
                        'value' => function ($model) {
                                   
                                     return $model->assignee0->firstname." ".$model->assignee0->middlename." ".$model->assignee0->surname;
                                    
                        },                               
                    ],
                    [
                     'attribute' => 'totalApplication',
                     'label'=>'Total Applications',
                        'value' => function ($model) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getApplicationAttemptedSummary($model->assignee);
                                     return number_format($results->totalApplication);
                                    
                        },
                        'format' => 'raw'
                    ],            
                    [
                     'attribute' => 'unverified',
                     'label'=>'Unverified',
                        'value' => function ($model) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getApplicationAttemptedSummary($model->assignee);
                                     return number_format($results->not_attempted);
                                    
                        },
                        'format' => 'raw'
                    ], 
                                [
                     'attribute' => 'pending',
                     'label'=>'Pending',
                        'value' => function ($model) use($date_verified,$date_verified2) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($model->assignee,$date_verified,$date_verified2);
                                     return number_format($results->pending);
                                    
                        },
                        'format' => 'raw'
                    ],
                                [
                     'attribute' => 'incomplete',
                     'label'=>'Incomplete',
                        'value' => function ($model) use($date_verified,$date_verified2) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($model->assignee,$date_verified,$date_verified2);
                                     return number_format($results->incomplete);
                                    
                        },
                        'format' => 'raw'
                    ],
                                [
                     'attribute' => 'waiting',
                     'label'=>'Waiting',
                        'value' => function ($model) use($date_verified,$date_verified2) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($model->assignee,$date_verified,$date_verified2);
                                     return number_format($results->waiting);
                                    
                        },
                        'format' => 'raw'
                    ],
                                [
                     'attribute' => 'invalid',
                     'label'=>'Invalid',
                        'value' => function ($model) use($date_verified,$date_verified2) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($model->assignee,$date_verified,$date_verified2);
                                     return number_format($results->invalid);
                                    
                        },
                        'format' => 'raw'
                    ],
                                [
                     'attribute' => 'complete',
                     'label'=>'Complete',
                        'value' => function ($model) use($date_verified,$date_verified2) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($model->assignee,$date_verified,$date_verified2);
                                     return number_format($results->complete);
                                    
                        },
                        'format' => 'raw'
                    ],
                                [
                     'attribute' => 'attempted',
                     'label'=>'Attempted',
                        'value' => function ($model) use($date_verified,$date_verified2) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getVerificationSummary($model->assignee,$date_verified,$date_verified2);
                                     return number_format($results->attempted);
                                    
                        },
                        'format' => 'raw'
                    ],
                                               
];
echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'pjax' => true,
    'bordered' => true,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
    'showPageSummary' => false,
    /*
    'panel' => [
        'type' => GridView::TYPE_PRIMARY
    ],
     * 
     */
]);
?>
</div>
</div>
</div>
