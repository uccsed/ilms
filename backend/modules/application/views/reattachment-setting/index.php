<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\application\models\ReattachmentSettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Re-Attachment Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reattachment-setting-index">
<div class="panel panel-info">
    <div class="panel-heading">  

    <?= Html::encode($this->title) ?>
    </div>
        <div class="panel-body">

    <p>
        <?= Html::a('Create Re-Attachment Setting', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'verification_status',
            [
                     'attribute' => 'verification_status',
                        'width' => '140px',
                        'value' => function ($model) {
                                   if($model->verification_status ==2){
                                     return "Invalid";
                                    } else if($model->verification_status==3) {
                                        return "Waiting";
                                    }
                                   else if($model->verification_status==4) {
                                        return "Incomplete";
                                    }
                        },
                        'format' => 'raw',
                'filter' => [2 => 'Invalid', 3 => 'Waiting', 4 => 'Incomplete']
                    ],
            //'status_flag',
                                [
                     'attribute' => 'status_flag',
                        'width' => '140px',
                        'value' => function ($model) {
                                   if($model->status_flag ==0){
                                     return "All";
                                    } else if($model->status_flag==1) {
                                        return "Specific Comments";
                                    }
                        },
                        'format' => 'raw',
                'filter' => [0 => 'All', 1 => 'Specific Comments']
                    ],
                                [
            'attribute'=>'comment_id',
            'header'=>'Comment',
            //'filter' => ['Y'=>'Active', 'N'=>'Deactive'],
            'format'=>'raw',    
            'value' => function($model)
            {
                            if($model->comment_id==''){
                                return "All";
                            }else{
                              return $model->verificationCommentGroup->comment_group;  
                            } 
                    
                
            },
        ],
                                
            //'is_active',
                                
             [
                     'attribute' => 'is_active',
                        'width' => '140px',
                        'value' => function ($model) {
                                   if($model->is_active ==1){
                                     return "Active";
                                    } else if($model->is_active==0) {
                                        return "Inactive";
                                    }
                        },
                        'format' => 'raw',
                'filter' => [1 => 'Active', 0 => 'Inactive']
                    ],                                              
            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                ],
        ],
    ]); ?>
</div>
  </div>
</div>
