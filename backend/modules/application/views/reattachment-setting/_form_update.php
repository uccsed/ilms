<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use common\models\ApplicantQuestion;
use backend\modules\application\models\ReattachmentSetting;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_VERTICAL,
    ]); ?>

    <?php  
     echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' =>1,
            'attributes' => [
                'verification_status' => [
                'type' => Form::INPUT_DROPDOWN_LIST,
                'items' => ApplicantQuestion::getVerificationStatusReattach(),
                'options' => ['prompt' => '-- Select --',
                    'disabled' => true,
                    ],
              ],     
                'status_flag' => [
                'type' => Form::INPUT_DROPDOWN_LIST,
                'items' => ReattachmentSetting::getStatusFlag(), 
                'options' => [
                    'prompt' => '-- Select --',
                    'id' => 'status_flag_id',
		    'onchange'=>'ShowHideDiv()',
                    'disabled' => true,
                    ],
              ], 
            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],                 
           ]
           ]);
     ?>
<?php
echo Form::widget([// fields with labels
    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [        
'comment_id' => ['type' => Form::INPUT_WIDGET,
                  'widgetClass' => \kartik\select2\Select2::className(),
                  'label' => 'Comment',
                  'options' => [
                      //'data' => ArrayHelper::map(\common\models\User::find()->where(['login_type' => 5])->all(), 'user_id', 'firstname'),
                      
                      'data' =>ArrayHelper::map(\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),
                      
                       'options' => [
                        'prompt' => 'Select', 
                      'disabled' => true,  
                    ],
                ],
            ],
'is_active'=>['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'Is Active',              
                'options' => [
                    'data' => ['1'=>'Active', '0'=>'Inactive'],
                    'options' => [
                        'prompt' => '---Select--- '                  
                    ],
                ],
             ],        
]]);
?>
    <div class="text-right">
       <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  
<?php
echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

ActiveForm::end();
?>
    </div>
