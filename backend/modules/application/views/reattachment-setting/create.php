<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\ReattachmentSetting */

$this->title = 'Create Re-attachment Setting';
$this->params['breadcrumbs'][] = ['label' => 'Re-attachment Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reattachment-setting-create">
<div class="panel panel-info">
    <div class="panel-heading">  

    <?= Html::encode($this->title) ?>
    </div>
        <div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
  </div>
</div>
