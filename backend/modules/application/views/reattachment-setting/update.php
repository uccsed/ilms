<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\ReattachmentSetting */

$this->title = 'Update Reattachment Setting';
$this->params['breadcrumbs'][] = ['label' => 'Reattachment Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->reattachment_setting_id, 'url' => ['view', 'id' => $model->reattachment_setting_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reattachment-setting-update">
<div class="panel panel-info">
    <div class="panel-heading">  

    <?= Html::encode($this->title) ?>
    </div>
        <div class="panel-body">

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>
</div>
  </div>
</div>
