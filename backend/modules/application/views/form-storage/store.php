<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 8/11/18
 * Time: 8:14 AM
 */

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\application\models\ApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = strtoupper('Application Forms Storage');
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="../js/jquery-1.11.1.min.js"></script>
<script src="../js/JsBarcode.all.min.js"></script>
<div class="panel panel-flat">
    <div class="panel-heading bg-info">
        <?= Html::encode($this->title) ?>

    </div>
    <div class="panel-body">
        <?php echo Yii::$app->controller->renderPartial('_storeForm'); ?>

    </div>

</div>