<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 8/11/18
 * Time: 7:36 AM
 */

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\application\models\ApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = strtoupper('Application Forms Storage');
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="../js/jquery-1.11.1.min.js"></script>
<script src="../js/JsBarcode.all.min.js"></script>
<!--<script src="../ExtraPlugins/datatables/jquery.dataTables.min.js"></script>
<script src="../ExtraPlugins/datatables/dataTables.bootstrap.min.js"></script>-->
    <div class="panel panel-flat">
        <div class="panel-heading bg-info">
            <?= Html::encode($this->title) ?>

        </div>
        <div class="panel-body">
            <div class="row">
                <?php echo Yii::$app->controller->renderPartial('_folderSearch'); ?>
            </div>

            <div class="row">
                <?php echo Yii::$app->controller->renderPartial('_folderGrid'); ?>
            </div>


        </div>

    </div>


