<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\application\models\AttachmentDefinitionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attachment List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attachment-definition-index">
  <div class="panel panel-info">
    <div class="panel-heading">  

    <?= Html::encode($this->title) ?>
    </div>
        <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'attachment_definition_id',
            'attachment_desc',
             [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}'
            ],         
        ],
            'responsive' => true,
            'hover' => true,
            'condensed' => true,
    ]); 
?>
    </div>
  </div>
</div>

