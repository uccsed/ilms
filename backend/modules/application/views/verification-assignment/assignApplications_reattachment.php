<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\application\models\VerificationAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Verifications Assignments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="verification-assignment-index">
<div class="panel panel-info">
    <div class="panel-heading">  
        <?= Html::encode($this->title) ?>               
        <?php
        echo $this->render('_details_reattached');
        ?>
        <br/>
    </div>
        <div class="panel-body">

    <p>
        <?= Html::a('Assign-Reattachment', ['assign-reattached'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php

	            $totalApplication_new = 0;
				$totalnot_attempted=0;
				$totalattempted=0;
    if (!empty($dataProvider->getModels())) {
        foreach ($dataProvider->getModels() as $key => $val) {
			$applica22=\backend\modules\application\models\VerificationAssignment::getApplicationAttemptedSummaryReattached($val->assignee);
            $totalApplication_new += $applica22->totalApplication;
			$totalnot_attempted += $applica22->not_attempted;
			$totalattempted +=$applica22->attempted;
        }
    }
	
	?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'showFooter'=>TRUE,
		'footerRowOptions'=>['style'=>'font-weight:bold;'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'verification_assignment_id',
            [
                     'attribute' => 'assignee',
                     'label'=>'Assignee',
                        'value' => function ($model) {
                                   
                                     return $model->assignee0->firstname." ".$model->assignee0->middlename." ".$model->assignee0->surname;
                                    
                        },
                                'filterType' => GridView::FILTER_SELECT2,
                        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                        'filter' =>ArrayHelper::map(\common\models\User::findBySql('SELECT user.user_id,CONCAT(user.firstname," ",user.surname) AS "Name" FROM `user` INNER JOIN verification_assignment ON verification_assignment.assignee=user.user_id  WHERE user.login_type=5')->asArray()->all(), 'user_id', 'Name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw',
						'footer'=>'Total',
                    ],
            [
                     'attribute' => 'totalApplication',
                     'label'=>'Total Applications',
                        'value' => function ($model) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getApplicationAttemptedSummaryReattached($model->assignee);
                                     return number_format($results->totalApplication);
                                    
                        },
                        'format' => 'raw',
						'footer'=>number_format($totalApplication_new),
                    ],
            [
                     'attribute' => 'attempted',
                     'label'=>'Attempted',
                        'value' => function ($model) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getApplicationAttemptedSummaryReattached($model->assignee);
                                     return number_format($results->attempted);
                                    
                        },
                        'format' => 'raw',
						'footer'=>number_format($totalattempted),
                    ],
            [
                     'attribute' => 'not_attempted',
                     'label'=>'Not Attempted',
                        'value' => function ($model) {
                                   $results=\backend\modules\application\models\VerificationAssignment::getApplicationAttemptedSummaryReattached($model->assignee);
                                     return number_format($results->not_attempted);
                                    
                        },
                        'format' => 'raw',
						'footer'=>number_format($totalnot_attempted),
                    ],                  

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
  </div>
</div>
