<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationCommentGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="verification-comment-group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'comment_group')->textInput(['maxlength' => true]) ?>
    <div class="text-right">
       <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  
<?php
echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

ActiveForm::end();
?>
    </div>

</div>
