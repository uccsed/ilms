<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\application\models\VerificationCommentGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Verification Comment Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="verification-comment-group-index">
<div class="panel panel-info">
    <div class="panel-heading">
       <?= Html::encode($this->title) ?>
    </div>
        <div class="panel-body">
    <p>
        <?= Html::a('Create Verification Comment Group', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'comment_group',
            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                ],
        ],
    ]); ?>
</div>
  </div>
</div>
