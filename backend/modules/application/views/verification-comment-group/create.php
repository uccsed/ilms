<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationCommentGroup */

$this->title = 'Create Verification Comment Group';
$this->params['breadcrumbs'][] = ['label' => 'Verification Comment Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="verification-comment-group-create">
<div class="panel panel-info">
    <div class="panel-heading">
        <?= Html::encode($this->title) ?>
    </div>
        <div class="panel-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
  </div>
</div>
