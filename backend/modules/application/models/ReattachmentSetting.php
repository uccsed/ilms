<?php

namespace backend\modules\application\models;

use Yii;

/**
 * This is the model class for table "reattachment_setting".
 *
 * @property integer $reattachment_setting_id
 * @property integer $verification_status
 * @property integer $status_flag
 * @property integer $comment_id
 * @property integer $is_active
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 */
class ReattachmentSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_FLAG_ALL = 0;
    const STATUS_FLAG_SELECTED = 1;
    
    public static function tableName()
    {
        return 'reattachment_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['verification_status', 'status_flag'], 'required'],
        ['comment_id', 'required', 'when' => function ($model) {
    return $model->status_flag > 0;
}, 'whenClient' => "function (attribute, value) {
    return $('#status_flag_id').val() > 0;
}"],
            [['verification_status', 'status_flag', 'comment_id', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reattachment_setting_id' => 'Reattachment Setting ID',
            'verification_status' => 'Verification Status',
            'status_flag' => 'Status Category',
            'comment_id' => 'Comment',
            'is_active' => 'Is Active',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
    
    static function getStatusFlag() {
        return array(
            self::STATUS_FLAG_ALL => 'All',
            self::STATUS_FLAG_SELECTED => 'Specific Comments',
        );
    }
    public function getVerificationCommentGroup()
    {
        return $this->hasOne(VerificationCommentGroup::className(), ['verification_comment_group_id' => 'comment_id']);
    }
}
