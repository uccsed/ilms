<?php

namespace backend\modules\application\models;

use Yii;
use \backend\modules\application\models\GepgTransactions;

/**
 * This is the model class for table "gepg_transactions".
 *
 * @property integer $gepg_transactions_id
 * @property string $f4indexno
 * @property string $payer_name
 * @property string $control_number
 * @property string $trans_time
 * @property string $trans_id
 * @property string $source
 * @property string $amount
 * @property string $bank_account
 * @property string $account_number
 * @property string $trans_type
 * @property integer $payment_trasaction_id
 * @property string $created_at
 */
class GepgTransactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gepg_transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trans_time', 'created_at'], 'safe'],
            [['amount'], 'number'],
            [['account_number', 'trans_type'], 'required'],
            [['payment_trasaction_id'], 'integer'],
            [['f4indexno'], 'string', 'max' => 45],
            [['payer_name'], 'string', 'max' => 200],
            [['control_number', 'trans_id', 'source', 'account_number'], 'string', 'max' => 20],
            [['bank_account'], 'string', 'max' => 60],
            [['trans_type'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gepg_transactions_id' => 'Gepg Transactions ID',
            'f4indexno' => 'F4indexno',
            'payer_name' => 'Payer Name',
            'control_number' => 'Control Number',
            'trans_time' => 'Trans Time',
            'trans_id' => 'Trans ID',
            'source' => 'Source',
            'amount' => 'Amount',
            'bank_account' => 'Bank Account',
            'account_number' => 'Account Number',
            'trans_type' => 'Trans Type',
            'payment_trasaction_id' => 'Payment Trasaction ID',
            'created_at' => 'Created At',
        ];
    }
    
    public static function getAllApplicationsPaid(){
       $QUERY_BATCH_SIZE = 1000;
       $offset = 0;
       $done = false;
       $startTime = time();
       while(!$done){
           $limit=$QUERY_BATCH_SIZE;
		   
			$resultdApplicationID1=GepgTransactions::find()->where(['trans_type'=>'Application'])->orderBy(['gepg_transactions_id' => SORT_DESC])->one();			
			if(count($resultdApplicationID1) > 0){
		    $resultdApplicationID=$resultdApplicationID1->payment_trasaction_id;		
			if($resultdApplicationID > 0){
			$applicationIDCond="gepg_receipt.id > $resultdApplicationID";
			}
			}else{
				$resultdApplicationID =0;
			$applicationIDCond="gepg_receipt.id > $resultdApplicationID";
			}
                   //exit;
    $applicationDetails = \backend\modules\application\models\GepgReceipt::find()
        ->select(['gepg_receipt.id','gepg_receipt.transact_date_gepg','gepg_receipt.trans_id','gepg_receipt.paymentChanelUsed','gepg_receipt.paid_amount','gepg_receipt.paymServProvName','gepg_receipt.account_number','user.firstname','user.middlename','user.surname','applicant.f4indexno','application.control_number','application.receipt_number','application.amount_paid','application.application_id','applicant.applicant_id'])
        ->joinWith('application', ['application.application_id'=>'gepg_receipt.application_id'])    
        ->joinWith('application.applicant', ['application.applicant_id'=>'applicant.applicant_id'])
        ->joinWith('application.applicant.user', ['applicant.user_id'=>'user.user_id'])   
        ->where(['application.payment_status'=>1])
        //->andWhere($applicationIDCond)		
        ->andWhere(['<>','application.receipt_number',''])
	    ->andWhere(['IS NOT','application.receipt_number',NULL]) 
        ->limit($limit)
        ->offset($offset)		
        ->orderBy(['gepg_receipt.id' => SORT_ASC])
        //->asArray()        		
        ->all();
		

        if(count($applicationDetails) > 0){
        foreach ($applicationDetails as $Result1) {
		     $payer_name = $Result1->application->applicant->user->firstname.", ".$Result1->application->applicant->user->middlename." ".$Result1->application->applicant->user->surname;
                    
            $control_number=$Result1->application->control_number;			
			$payment_trasaction_id=$Result1->id;
			$receipt_number=$Result1->application->receipt_number;
                        $applicationID=$Result1->application->application_id;
			$educationResults=\common\models\Education::find()
			->where(['level'=>'OLEVEL','application_id'=>$applicationID])			
			->orderBy(['education_id'=>SORT_ASC])
			->one();
			$f4indexno = trim($educationResults->registration_number).".".trim($educationResults->completion_year);
                        /*
			$gepgReceipt=\backend\modules\application\models\GepgReceipt::find()
			->where(['application_id'=>$payment_trasaction_id,'control_number'=>$control_number,'receipt_number'=>$receipt_number])
			->one();
                         * 
                         */
			$trans_time=$Result1->transact_date_gepg;
			$trans_id=$Result1->trans_id;
			if($trans_id=='' || empty($trans_id)){
			$trans_id=$receipt_number;	
			}
			$source=$Result1->paymentChanelUsed;
			$amount=$Result1->paid_amount;
			$bank_account=$Result1->paymServProvName;
			$account_number=$Result1->account_number;
			if($account_number=='' || empty($account_number)){
				if($bank_account='National Microfinance Bank'){
				$account_number='00-acc number NMB';	
				}else{
				$account_number='00-acc number CRDB';
				}
			}
			$trans_type='Application';
			
			
			
			$f4indexno = $educationResults->registration_number.".".$educationResults->completion_year;
			$created_at=date("Y-m-d H:i:s");
                        //check if exists
                        $resultsExists=GepgTransactions::find()->where(['control_number'=>$control_number,'trans_id'=>$trans_id])->count();
                        //end check if exists
                        if($resultsExists ==0){
			Yii::$app->db->createCommand()
        ->insert('gepg_transactions', [
        'f4indexno' =>$f4indexno,
		'payer_name' =>$payer_name,
		'control_number' =>$control_number,
		'trans_time' =>$trans_time,
		'trans_id' =>$trans_id,
		'source' =>$source,
		'amount' =>$amount,
		'bank_account' =>$bank_account,
		'account_number' =>$account_number,
		'trans_type' =>$trans_type,
		'payment_trasaction_id' =>$payment_trasaction_id,
		'created_at' =>$created_at,  
        ])->execute();
                        }
        unset($applicationDetails[$Result1]);
        }
		$offset += $QUERY_BATCH_SIZE;        
       }else {
        $done = true;
        }
	   }
        $endTime = time();
        echo "Total time to cache results: ".($endTime - $startTime)." seconds.
        ";
        return "Done";		
        }
}
