<?php

namespace backend\modules\application\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\application\models\ReattachmentSetting;

/**
 * ReattachmentSettingSearch represents the model behind the search form about `backend\modules\application\models\ReattachmentSetting`.
 */
class ReattachmentSettingSearch extends ReattachmentSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reattachment_setting_id', 'verification_status', 'status_flag', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at','comment_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReattachmentSetting::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->joinWith("verificationCommentGroup");
        $query->andFilterWhere([
            'reattachment_setting_id' => $this->reattachment_setting_id,
            'verification_status' => $this->verification_status,
            'status_flag' => $this->status_flag,
            //'comment_id' => $this->comment_id,
            'is_active' => $this->is_active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);
        $query->andFilterWhere(['like', 'verification_comment_group.comment_group', $this->comment_id]);

        return $dataProvider;
    }
}
