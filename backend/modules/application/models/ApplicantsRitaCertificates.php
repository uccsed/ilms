<?php

namespace backend\modules\application\models;

use Yii;

/**
 * This is the model class for table "applicants_rita_certificates".
 *
 * @property integer $id
 * @property string $certificate_number
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $applicant_id
 * @property string $application_id
 * @property string $applicant_firstname
 * @property string $applicant_middlename
 * @property string $applicant_surname
 * @property integer $checked
 */
class ApplicantsRitaCertificates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'applicants_rita_certificates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['checked','applicant_id', 'application_id',], 'integer'],
            [['certificate_number', 'firstname', 'middlename', 'lastname', 'applicant_firstname', 'applicant_middlename', 'applicant_surname'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'certificate_number' => 'Certificate Number',
            'firstname' => 'Firstname',
            'middlename' => 'Middlename',
            'lastname' => 'Lastname',
            'applicant_id' => 'Applicant ID',
            'application_id' => 'Application ID',
            'applicant_firstname' => 'Applicant Firstname',
            'applicant_middlename' => 'Applicant Middlename',
            'applicant_surname' => 'Applicant Surname',
            'checked' => 'Checked',
        ];
    }
}
