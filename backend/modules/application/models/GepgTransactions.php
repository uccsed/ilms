<?php

namespace backend\modules\application\models;

use Yii;
use \backend\modules\application\models\GepgTransactions;

/**
 * This is the model class for table "gepg_transactions".
 *
 * @property integer $gepg_transactions_id
 * @property string $f4indexno
 * @property string $payer_name
 * @property string $control_number
 * @property string $trans_time
 * @property string $trans_id
 * @property string $source
 * @property string $amount
 * @property string $bank_account
 * @property string $account_number
 * @property string $trans_type
 * @property integer $payment_trasaction_id
 * @property string $created_at
 */
class GepgTransactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gepg_transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trans_time', 'created_at'], 'safe'],
            [['amount'], 'number'],
            [['account_number', 'trans_type'], 'required'],
            [['payment_trasaction_id'], 'integer'],
            [['f4indexno'], 'string', 'max' => 45],
            [['payer_name'], 'string', 'max' => 200],
            [['control_number', 'trans_id', 'source', 'account_number'], 'string', 'max' => 20],
            [['bank_account'], 'string', 'max' => 60],
            [['trans_type'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gepg_transactions_id' => 'Gepg Transactions ID',
            'f4indexno' => 'F4indexno',
            'payer_name' => 'Payer Name',
            'control_number' => 'Control Number',
            'trans_time' => 'Trans Time',
            'trans_id' => 'Trans ID',
            'source' => 'Source',
            'amount' => 'Amount',
            'bank_account' => 'Bank Account',
            'account_number' => 'Account Number',
            'trans_type' => 'Trans Type',
            'payment_trasaction_id' => 'Payment Trasaction ID',
            'created_at' => 'Created At',
        ];
    }
    
    public static function getAllApplicationsPaid(){
       $QUERY_BATCH_SIZE = 1000;
       $offset = 0;
       $done = false;
       $startTime = time();
       while(!$done){
           $limit=$QUERY_BATCH_SIZE;
		   
			$resultdApplicationID1=GepgTransactions::find()->where(['trans_type'=>'Application'])->orderBy(['gepg_transactions_id' => SORT_DESC])->one();			
			if(count($resultdApplicationID1) > 0){
		    $resultdApplicationID=$resultdApplicationID1->payment_trasaction_id;		
			if($resultdApplicationID > 0){
			$applicationIDCond="gepg_receipt.id > $resultdApplicationID";
			}
			}else{
				$resultdApplicationID =0;
			$applicationIDCond="gepg_receipt.id > $resultdApplicationID";
			}
                   //exit;
    $applicationDetails = \backend\modules\application\models\GepgReceipt::find()
        ->select(['gepg_receipt.id','gepg_receipt.transact_date_gepg','gepg_receipt.trans_id','gepg_receipt.paymentChanelUsed','gepg_receipt.paid_amount','gepg_receipt.paymServProvName','gepg_receipt.account_number','user.firstname','user.middlename','user.surname','applicant.f4indexno','application.control_number','application.receipt_number','application.amount_paid','application.application_id','applicant.applicant_id'])
        ->joinWith('application', ['application.application_id'=>'gepg_receipt.application_id'])    
        ->joinWith('application.applicant', ['application.applicant_id'=>'applicant.applicant_id'])
        ->joinWith('application.applicant.user', ['applicant.user_id'=>'user.user_id'])   
        ->where(['application.payment_status'=>1])
        ->andWhere($applicationIDCond)		
        ->andWhere(['<>','application.receipt_number',''])
	    ->andWhere(['IS NOT','application.receipt_number',NULL]) 
       //->limit($limit)
        //->offset($offset)		
        ->orderBy(['gepg_receipt.id' => SORT_ASC])
        //->asArray()        		
        ->all();
		

        if(count($applicationDetails) > 0){
        foreach ($applicationDetails as $Result1) {
		     $payer_name = $Result1->application->applicant->user->firstname.", ".$Result1->application->applicant->user->middlename." ".$Result1->application->applicant->user->surname;
			 $f4indexno=$Result1->application->applicant->f4indexno;
                    
            $control_number=$Result1->application->control_number;			
			$payment_trasaction_id=$Result1->id;
			$receipt_number=$Result1->application->receipt_number;
                        $applicationID=$Result1->application->application_id;
			/*			
			$educationResults=\common\models\Education::find()
			->where(['level'=>'OLEVEL','application_id'=>$applicationID])			
			->orderBy(['education_id'=>SORT_ASC])
			->one();
			$f4indexno = trim($educationResults->registration_number).".".trim($educationResults->completion_year);
                        
			$gepgReceipt=\backend\modules\application\models\GepgReceipt::find()
			->where(['application_id'=>$payment_trasaction_id,'control_number'=>$control_number,'receipt_number'=>$receipt_number])
			->one();
                         * 
                         */
			$trans_time=$Result1->transact_date_gepg;
			$trans_id=$Result1->trans_id;
			if($trans_id=='' || empty($trans_id)){
			$trans_id=$receipt_number;	
			}
			$source=$Result1->paymentChanelUsed;
			$amount=$Result1->paid_amount;
			$bank_account=$Result1->paymServProvName;
			$account_number=$Result1->account_number;
			if($account_number=='' || empty($account_number)){
				if($bank_account='National Microfinance Bank'){
				$account_number='00-acc number NMB';	
				}else{
				$account_number='00-acc number CRDB';
				}
			}
			$trans_type='Application';
			
			$created_at=date("Y-m-d H:i:s");
                        //check if exists
                        $resultsExists=GepgTransactions::find()->where(['control_number'=>$control_number,'trans_id'=>$trans_id])->count();
                        //end check if exists
                        if($resultsExists ==0){
			Yii::$app->db->createCommand()
        ->insert('gepg_transactions', [
        'f4indexno' =>$f4indexno,
		'payer_name' =>$payer_name,
		'control_number' =>$control_number,
		'trans_time' =>$trans_time,
		'trans_id' =>$trans_id,
		'source' =>$source,
		'amount' =>$amount,
		'bank_account' =>$bank_account,
		'account_number' =>$account_number,
		'trans_type' =>$trans_type,
		'payment_trasaction_id' =>$payment_trasaction_id,
		'created_at' =>$created_at,  
        ])->execute();
                        }
        unset($control_number);unset($f4indexno);unset($payer_name);unset($trans_time);unset($trans_id);unset($source);unset($amount);unset($bank_account);unset($account_number);unset($trans_type);unset($payment_trasaction_id);unset($created_at);
		
        }
		$offset += $QUERY_BATCH_SIZE;        
       }else {
        $done = true;
        }
	   }
        $endTime = time();
        /*
        echo "Total time to cache results: ".($endTime - $startTime)." seconds.
        ";
        return "Done";
         */		
        }

/*
public static function checkMatchRITAcertified(){
       $QUERY_BATCH_SIZE = 1000;
       $offset = 0;
       $done = false;
       $startTime = time();
       while(!$done){
           $limit=$QUERY_BATCH_SIZE;

        $applicationDetails = \backend\modules\application\models\ApplicantsRitaCertificates::find()
        ->select(['id','certificate_number'])   
        //->where(['checked'=>0,'certificate_number'=>'1004035412']) 
		->where(['checked'=>0])
        ->limit($limit)
        ->offset($offset)		
        ->orderBy(['id' => SORT_ASC])       		
        ->all();
		

        if(count($applicationDetails) > 0){
        foreach ($applicationDetails as $Result1) {
			 $certificate_number=trim($Result1->certificate_number);
			 $id=$Result1->id;
		             
			$Results_applicant_id=\backend\modules\application\models\Applicant::find()
			->select(['applicant.user_id','user.firstname','user.middlename','user.surname','applicant_id'])
			->joinWith('user', ['user.user_id'=>'applicant.user_id'])
			->where(['birth_certificate_number'=>$certificate_number])
			->one();

			$applicant_id=$Results_applicant_id->applicant_id;
			$userID=$Results_applicant_id->user_id;
			$applicant_firstname=$Results_applicant_id->user->firstname;
			$applicant_middlename=$Results_applicant_id->user->middlename;
			$applicant_surname=$Results_applicant_id->user->surname;
			
			
			
			if($applicant_id > 0){
			$Results_application_id=\backend\modules\application\models\Application::find()
			->select(['application_id'])
			->where(['applicant_id'=>$applicant_id])
			->one();
			$resultsApplicationID=$Results_application_id->application_id;

			$foundCert=\backend\modules\application\models\ApplicantsRitaCertificates::findOne($id);
            $foundCert->checked=1;
            $foundCert->applicant_id=$applicant_id;	
            $foundCert->application_id=$resultsApplicationID;
			$foundCert->applicant_firstname=$applicant_firstname;
			$foundCert->applicant_middlename=$applicant_middlename;
			$foundCert->applicant_surname=$applicant_surname;
            $foundCert->save();
            			
			}else{
			$foundCert2=\backend\modules\application\models\ApplicantsRitaCertificates::findOne($id);
            $foundCert2->checked=1;
            $foundCert2->save();            
			}
			
			
        unset($applicant_id);unset($id);unset($certificate_number);
		
        }
		$offset += $QUERY_BATCH_SIZE;        
       }else {
        $done = true;
        }
	   }
        $endTime = time();
	
        }
*/


public static function checkMatchRITAcertified(){
       $QUERY_BATCH_SIZE = 3;
       $offset = 0;
       $done = false;
       $startTime = time();
       while(!$done){
           $limit=$QUERY_BATCH_SIZE;

        $applicationDetails = \backend\modules\application\models\ApplicantsRitaCertificates::find()
        ->select(['id','certificate_number'])   
        //->where(['checked'=>0,'certificate_number'=>'1004035412']) 
		->where(['checked'=>0])
        ->limit($limit)
       // ->offset($offset)		
        ->orderBy(['id' => SORT_ASC])       		
        ->all();
		

        if(count($applicationDetails) > 0){
        foreach ($applicationDetails as $Result1) {
               
			 $certificate_number=trim($Result1->certificate_number);
			 $id=$Result1->id;

        
			$Results_applicant_id=\backend\modules\application\models\Applicant::find()
			->select(['applicant.user_id','user.firstname','user.middlename','user.surname','applicant_id'])
			->joinWith('user', ['user.user_id'=>'applicant.user_id'])
			->where(['birth_certificate_number'=>$certificate_number])
			->one();
             if(count($Results_applicant_id > 0)){ 
			$applicant_id=$Results_applicant_id['applicant_id'];
			$userID=$Results_applicant_id['user_id'];
/*
			$applicant_firstname=$Results_applicant_id->user->firstname;
			$applicant_middlename=$Results_applicant_id->user->middlename;
			$applicant_surname=$Results_applicant_id->user->surname;

*/
$applicant_firstname=$Results_applicant_id['firstname'];
			$applicant_middlename=$Results_applicant_id['middlename'];
			$applicant_surname=$Results_applicant_id['surname'];			
echo $applicant_id;
exit;
 }
			
			if(count($Results_applicant_id > 0)){
			if($applicant_id > 0){
			$Results_application_id=\backend\modules\application\models\Application::find()
			->select(['application_id'])
			->where(['applicant_id'=>$applicant_id])
			->one();
			$resultsApplicationID=$Results_application_id->application_id;

			$foundCert=\backend\modules\application\models\ApplicantsRitaCertificates::findOne($id);
            $foundCert->checked=1;
            $foundCert->applicant_id=$applicant_id;	
            $foundCert->application_id=$resultsApplicationID;
			$foundCert->applicant_firstname=$applicant_firstname;
			$foundCert->applicant_middlename=$applicant_middlename;
			$foundCert->applicant_surname=$applicant_surname;
            $foundCert->save();
            			
			}else{
			$foundCert2=\backend\modules\application\models\ApplicantsRitaCertificates::findOne($id);
            $foundCert2->checked=1;
            $foundCert2->save();            
			}
		}else{
			$foundCert2=\backend\modules\application\models\ApplicantsRitaCertificates::findOne($id);
            $foundCert2->checked=1;
            $foundCert2->save();
		}
			
			
        unset($applicant_id);unset($id);unset($certificate_number);
		
        }
		$offset += $QUERY_BATCH_SIZE;        
       }else {
        $done = true;
        }
	   }
        $endTime = time();
		//echo $certificate_number."TELE".$applicant_id."tele".$resultsApplicationID."--".$applicant_surname."ooo".$userID."--applicantID--".$applicant_id;
		/*
        echo "Total time to cache results: ".($endTime - $startTime)." seconds.";
        return "Done";	
        */		
        }

		
		public static function checkAttachmentMatch(){
       $QUERY_BATCH_SIZE = 1000;
       $offset = 0;
       $done = false;
       $startTime = time();
       while(!$done){
           $limit=$QUERY_BATCH_SIZE;

        $applicationDetails2 = \backend\modules\application\models\ApplicantsRitaCertificates::find()
        ->select(['id','application_id'])   
       //->where(['checked'=>1,'application_id'=>'44959']) 
	->where(['checked'=>1])
		->andWhere(['>','application_id','0'])
        ->limit($limit)
        ->offset($offset)		
        ->orderBy(['id' => SORT_ASC])       		
        ->all();
		

        if(count($applicationDetails2) > 0){
        foreach ($applicationDetails2 as $Result12) {
			 $applicationID=trim($Result12->application_id);
			 $id=$Result12->id;
			 
			 $attachmentStatus=\frontend\modules\application\models\ApplicantAttachment::find()
			 ->select(['applicant_attachment_id'])
			 ->where(['application_id'=>$applicationID,'attachment_definition_id'=>'7'])
			 ->andWhere(['>','verification_status','1'])->one();
			 $applivantAttachmentID=$attachmentStatus->applicant_attachment_id;
			 if($applivantAttachmentID > 0){
				 
				$getFrameworkID=\backend\modules\application\models\Application::find()
			 ->select(['verification_framework_id'])
			 ->where(['application_id'=>$applicationID])->one();
			 
			 $framework=$getFrameworkID->verification_framework_id;
			 

			 
				$attachmentDetails=\frontend\modules\application\models\ApplicantAttachment::findOne($applivantAttachmentID); 
				$attachmentDetails->rita_birthCertificate_status=1;
				$attachmentDetails->framework_id=$framework;
				$attachmentDetails->save();
			 }
		             
			
			$foundCert2=\backend\modules\application\models\ApplicantsRitaCertificates::findOne($id);
            $foundCert2->checked=2;
            $foundCert2->save();            
			
			
			
        unset($applicationID);unset($id);unset($attachmentStatus);unset($applivantAttachmentID);unset($attachmentDetails);unset($foundCert2);unset($framework);
		
        }
		$offset += $QUERY_BATCH_SIZE;        
       }else {
        $done = true;
        }
	   }
        $endTime = time();
		
		//echo $certificate_number."TELE".$applicant_id."tele".$resultsApplicationID."--".$applicant_surname."ooo".$userID."--applicantID--".$applicant_id;
		/*
        echo "Total time to cache results: ".($endTime - $startTime)." seconds.";
        return "Done";	
        */		
        }



}
