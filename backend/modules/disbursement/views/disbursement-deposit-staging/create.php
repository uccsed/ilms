<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementDepositStaging */

$this->title = 'Create Disbursement Deposit Staging';
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Deposit Stagings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-deposit-staging-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
