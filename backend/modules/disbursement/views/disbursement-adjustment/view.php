<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementAdjustment */
$amount=0;
if ($model->type=='CR'){
    $amount =$model->credit_amount;
}else{
    $amount =$model->debit_amount;
}
$this->title = $model->Type.' Adjustment of '.number_format($amount,2).' for '.$model->LoanItem.' for '.$model->Applicant;
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Adjustments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-adjustment-view">

    <div class="panel panel-info">
        <div class="panel-heading">
           <h3><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="panel-body">

            <p>
                <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-warning']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
<div class="row">
    <div class="col-md-5">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'id',
                'adjustment_number',
                'Applicant',
                'LoanItem',
                //'disbursement_id',
                'Type',
                //'original_amount',
                [
                    'name'=>'original_amount',
                    'value'=>$model->original_amount,
                    'format'=>['decimal',2],
                    'label'=>'Original Amount'
                ],
                [
                    'name'=>'debit_amount',
                    'value'=>$model->debit_amount,
                    'format'=>['decimal',2],
                    'label'=>'Debit Amount'
                ],
                [
                    'name'=>'credit_amount',
                    'value'=>$model->credit_amount,
                    'format'=>['decimal',2],
                    'label'=>'Credit Amount'
                ],
                //'debit_amount',
                // 'credit_amount',
                'Reason',
                'remarks:ntext',
                //'adjustment_date',
                [
                    'name'=>'adjustment_date',
                    'value'=>date('D d/m/Y',strtotime($model->adjustment_date)),
                    'format'=>'raw',
                    'label'=>'adjustment Date'
                ],
                // 'adjusted_by',
                [
                    'name'=>'adjusted_by',
                    'value'=>Module::UserInfo($model->adjusted_by,'fullName'),
                    'format'=>'raw',
                    'label'=>'Adjusted By'
                ],
            ],
        ]) ?>
    </div>

    <div class="col-md-7">
        <table class="table table-bordered table-condensed" style="font-size: large;">
            <thead>
            <tr>
                <th class="text-center bg-green">DR <span class="pull-right" id="dr_check"></span></th>
                <th class="text-center bg-red">CR <span class="pull-right" id="cr_check"></span></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th class="amounts text-blue" style="text-align: right; font-size: 18px; font-weight: bolder;" id="original"><span class="pull-left text-muted">Balance BF</span><?php echo number_format($model->original_amount,2); ?></th>
                <th class="amounts text-red" style="text-align: right; font-size: 18px; font-weight: bolder;" id="CR_amount"><span class="pull-left text-muted">CR ADJ.</span><?php if($model->credit_amount!=0){ echo number_format($model->credit_amount,2);}else{ echo '-';} ?></th>
            </tr>

            <tr>
                <th class="amounts text-green" style="text-align: right; font-size: 18px; font-weight: bolder;" id="DR_amount"><span class="pull-left text-muted">DR ADJ.</span><?php if($model->debit_amount!=0){ echo number_format($model->debit_amount,2);}else{ echo '-';} ?></th>
                <th class="amounts text-green" style="text-align: right; font-size: 18px; font-weight: bolder;" id="balance_CF"><span class="pull-left text-muted">Balance CF</span><?php echo number_format($balanceCF=(($model->debit_amount + $model->original_amount)-$model->credit_amount),2); ?></th>
            </tr>
            </tbody>
            <tfoot class="text-blue">
            <tr class="bg-blue-gradient"><th></th><th></th></tr>
            <tr>
                <th style="text-align: right; font-size: 18px; font-weight: bolder;" id="DR_balance"><?php echo number_format(($model->debit_amount + $model->original_amount),2); ?></th>
                <th style="text-align: right; font-size: 18px; font-weight: bolder;" id="CR_balance"><?php echo number_format(($model->credit_amount + $balanceCF),2); ?></th>
            </tr>
            <tr class="bg-blue-gradient"><th></th><th></th></tr>
            </tfoot>
        </table>
    </div>
</div>

        </div>
    </div>
</div>
