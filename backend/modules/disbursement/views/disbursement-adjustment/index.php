<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use backend\modules\disbursement\Module;


$SQL = "
        SELECT 
        application.application_id AS 'application_id',
        applicant.f4indexno AS 'index_number',
        user.phone_number AS 'phone_number',
        user.email_address AS 'email_address',
        application.registration_number AS 'registration_number',
        CONCAT(applicant.f4indexno,' | ',user.firstname,' ',user.surname) AS 'full_name',
        CASE WHEN user.sex = 'M' THEN 'MALE' ELSE 'FEMALE' END AS 'Sex'
        FROM disbursement_adjustment
        LEFT  JOIN application ON disbursement_adjustment.application_id = application.application_id
        LEFT  JOIN applicant ON application.applicant_id = applicant.applicant_id
        LEFT  JOIN user ON applicant.user_id = user.user_id
        
        GROUP BY application.application_id
        ORDER BY user.firstname ASC,user.surname ASC;
        ";

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\DisbursementAdjustmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disbursement Adjustments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-adjustment-index">

    <?php /* GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'adjustment_number',
            'application_id',
            'loan_item_id',
            'disbursement_id',
            // 'type',
            // 'original_amount',
            // 'debit_amount',
            // 'credit_amount',
            // 'reasons',
            // 'remarks:ntext',
            // 'adjustment_date',
            // 'adjusted_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */ ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Create Disbursement Adjustment', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider'=>$dataProvider,
                'filterModel'=>$searchModel,
                'showPageSummary'=>true,
                'pjax'=>true,
                'striped'=>true,
                'hover'=>true,

                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    ['class'=>'kartik\grid\SerialColumn'],

                    //'adjustment_number',
                    [
                            'attribute' => 'adjustment_number',
                        'vAlign' => 'middle',
                        'label'=>'Adj. #',
                        'width' => '150px',
                        'value' => function ($model) {
                            return $model->adjustment_number;
                        },
                    ],
                    [
                        'attribute' => 'adjustment_date',
                        //'value' => 'adjustment_date',
                        'width' => '200px',
                        'value' => function ($model) {
                            return date('d/m/Y',strtotime($model->adjustment_date));
                        },
                        'format' => 'raw',
                        'label' => "Date",
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'name' => 'DisbursementAdjustment[adjustment_date]',
                            //'value' => date("Y-m-d H:i:s"),
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                // 'autoclose' => true,
                            ]
                        ])

                    ],

                    [
                        'attribute' => 'application_id',
                        'vAlign' => 'middle',
                        'label'=>'Lonee',
                        'noWrap' => true,
                        'value' => function ($model) {
                            return $model->Applicant;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(\backend\modules\disbursement\models\DisbursementAdjustment::findBySql($SQL)->asArray()->all(), 'application_id', 'full_name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search'],
                        'format' => 'raw'
                    ],

                    [
                        'attribute' => 'loan_item_id',
                        'vAlign' => 'middle',

                        //'width' => '200px',
                        'value' => function ($model) {
                            return $model->LoanItem;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->asArray()->all(), 'loan_item_id', 'item_name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search'],
                        'format' => 'raw'
                    ],

                    //'allocation_batch_id',
                    [
                        'attribute' => 'reasons',
                        'vAlign' => 'middle',
                        //'width' => '200px',
                        'value' => function ($model) {
                            return $model->Reason;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(\backend\modules\disbursement\models\AdjustmentReason::find()->where("is_active=1")->asArray()->all(), 'id', 'name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search'],
                        'format' => 'raw'
                    ],
                    [
                        'attribute' => 'type',
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                        'width' => '150px',
                        'value' => function ($model) {
                            return $model->type;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => Module::AdjustmentType(),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search'],
                        'format' => 'raw',
                        'pageSummary'=>'GRAND TOTAL',
                        'pageSummaryOptions'=>['class'=>'text-right text-green'],
                    ],


                    [
                        'attribute' => 'original_amount',
                        'hAlign' => 'right',
                        'width' => '200px',
                        'format'=>['decimal', 2],
                        'pageSummary'=>true,

                    ],
                    [
                        'attribute' => 'debit_amount',
                        'label'=>'DR',
                        'hAlign' => 'right',
                        'width' => '200px',
                        'format'=>['decimal', 2],
                        'pageSummary'=>true,

                    ],
                    [
                        'attribute' => 'credit_amount',
                        'label'=>'CR',
                        'hAlign' => 'right',
                        'width' => '200px',
                        'format'=>['decimal', 2],
                        'pageSummary'=>true,

                    ],


                    ['class' => 'kartik\grid\ActionColumn',
                        'template' => '{view}',
                        'buttons' => [
                            'update' => function ($url,$model) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-pencil" title="Edit"></span>',
                                    $url);
                            },
                            'view' => function ($url,$model,$key) {
                                return Html::a('<span class="green"> <i class="glyphicon glyphicon-eye-open" title="View"></i></span>', $url);
                            },

                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>
