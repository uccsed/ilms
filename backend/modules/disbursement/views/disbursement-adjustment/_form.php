<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementAdjustment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disbursement-adjustment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'adjustment_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'application_id')->textInput() ?>

    <?= $form->field($model, 'loan_item_id')->textInput() ?>

    <?= $form->field($model, 'disbursement_id')->textInput() ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'original_amount')->textInput() ?>

    <?= $form->field($model, 'debit_amount')->textInput() ?>

    <?= $form->field($model, 'credit_amount')->textInput() ?>

    <?= $form->field($model, 'reasons')->textInput() ?>

    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'adjustment_date')->textInput() ?>

    <?= $form->field($model, 'adjusted_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
