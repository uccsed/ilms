<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/21/18
 * Time: 1:46 PM
 */
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use backend\modules\disbursement\Module;
use kartik\select2\Select2;


echo Module::FetchBootstrap('js');
?>
<?php $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>
<div class="row">
    <div class="col-md-5">
        <div class="panel">
            <div class="panel-heading"></div>
            <div class="panel-body">

                <table class="table">
                    <tbody>
                    <tr>
                        <td>
                            <div class="row">

                                <div class="col-md-8">
                                    <div class="input-group">
                                        <div class="input-group-addon"><span class="fa fa-search"></span></div>
                                        <input id="search" name="search" class="form-control" style="font-size: 24px; height: 50px;" placeholder="Search index #" />
                                        <div class="input-group-addon" id="search_feedback"><span class="fa fa-spinner fa-2x"></span></div>
                                        <div class="input-group-addon"><button id="searchButton" type="button" class="btn btn-primary">Search</button></div>
                                    </div>

                                </div>
                                <div class="col-md-4"><?= $form->field($model, 'adjustment_number')->textInput(['value'=>Module::AdjustmentNumber(),'style'=>'font-size:18px; font-weight:bold;','readOnly'=>'readOnly','class'=>'form-control text-blue'])->label(false) ?></div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div class="row">

                                <div class="col-md-6">
                                    <?php echo
                                    Select2::widget(
                                        [
                                            'name' => 'semester',
                                            'value' => '',
                                            'data' => ArrayHelper::map(\common\models\Semester::findBySql("SELECT * FROM semester WHERE is_active = '1' ORDER BY semester_number ASC")->asArray()->all(),'semester_id','semester_number'),
                                            'options' => ['label' => 'Semester', 'placeholder' => 'Select Semester ...', 'id' => 'semester']
                                        ]
                                    );
                                    // $locationDropdown = $this->form->field($this->model, 'location_id')->label(false)->widget(DepDrop::classname(), ['type' => DepDrop::TYPE_SELECT2, 'options' => ['id' => 'location-selection'], 'select2Options' => ['pluginOptions' => ['allowClear' => TRUE]], 'pluginOptions' => ['depends' => ['country-selection'], 'placeholder' => 'Select Location', 'url' => Url::to(['/location/admin/load'])]]);
                                    ?>
                                </div>

                                <div class="col-md-6">
                                    <?php echo
                                    Select2::widget(
                                        [
                                            'name' => 'instalment',
                                            'value' => '',
                                            'data' => ArrayHelper::map(\common\models\Semester::findBySql("SELECT * FROM instalment_definition  WHERE is_active = '1' ORDER BY instalment ASC")->asArray()->all(),'instalment_definition_id','instalment'),
                                            'options' => ['label' => 'Semester', 'placeholder' => 'Select Instalment ...', 'id' => 'instalment']
                                        ]
                                    );
                                    // $locationDropdown = $this->form->field($this->model, 'location_id')->label(false)->widget(DepDrop::classname(), ['type' => DepDrop::TYPE_SELECT2, 'options' => ['id' => 'location-selection'], 'select2Options' => ['pluginOptions' => ['allowClear' => TRUE]], 'pluginOptions' => ['depends' => ['country-selection'], 'placeholder' => 'Select Location', 'url' => Url::to(['/location/admin/load'])]]);
                                    ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="row">


                </div>






                <div class="form-group">

                    <div class="row">
                        <div class="col-md-6">

                            <?php echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [
                                    'type' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        'label' => 'Adjustment Type',
                                        'options' => [
                                            'data' =>Module::AdjustmentType(),
                                            'options' => [
                                                'prompt' => 'Select Type',
                                                //'multiple'=>TRUE,

                                            ],
                                        ],
                                    ],

                                ]
                            ]);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php /*
                            echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [
                                    'loan_item_id' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        'label' => 'Loan Item',
                                        'options' => [
                                            'data' =>ArrayHelper::map(\backend\modules\allocation\models\LoanItem::findBySql("SELECT loan_item_id, CONCAT(item_name,' (',item_code,')') AS 'name' FROM loan_item WHERE is_active = '1' ")->asArray()->all(),'loan_item_id','name') ,
                                            'options' => [
                                                'prompt' => 'Select Item',
                                                //'multiple'=>TRUE,

                                            ],
                                        ],
                                    ],

                                ]
                            ]);*/

                            echo $form->field($model, 'loan_item_id')->widget(DepDrop::classname(), [
                                'type'=>DepDrop::TYPE_SELECT2,
                                'options'=>['id'=>'loan_item_id'],
                                'pluginOptions'=>[
                                    'depends'=>['disbursementadjustment-application_id','semester','instalment'],
                                    'placeholder'=>'Select...',
                                    'url'=>Url::to(['/disbursement/disbursement-adjustment/applicant-items'])
                                ]
                            ]);
                            ?>


                            <span class="pull-right" id="items_feedback"></span>
                        </div>

                    </div>

                </div>

                <div class="form-group">
                    <?php
                    echo $form->field($model, 'disbursement_id')->widget(DepDrop::classname(), [
                        'type'=>DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'disbursementadjustment-disbursement_id'],
                        'pluginOptions'=>[
                            'depends'=>['disbursementadjustment-application_id','semester','instalment','loan_item_id'],
                            'placeholder'=>'Select Disbursement ...',
                            'url'=>Url::to(['/disbursement/disbursement-adjustment/applicant-disbursements'])
                        ]
                    ]);
                    ?>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'original_amount')->hiddenInput(['readOnly'=>'readOnly','class'=>'form-control'])->label(false) ?>

                            <div class="form-group">
                                <?php
                                echo Form::widget([
                                    'model' => $model,
                                    'form' => $form,
                                    'columns' =>1,
                                    'attributes' => [
                                        'reasons' => ['type' => Form::INPUT_WIDGET,
                                            'widgetClass' => \kartik\select2\Select2::className(),
                                            'label' => 'Adjustment Reason',
                                            'options' => [
                                                'data' =>ArrayHelper::map(\backend\modules\disbursement\models\AdjustmentReason::findBySql("SELECT * FROM adjustment_reason WHERE is_active = '1' ")->asArray()->all(),'id','name') ,
                                                'options' => [
                                                    'prompt' => 'Select Reason',
                                                    //'multiple'=>TRUE,

                                                ],
                                            ],
                                        ],

                                    ]
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id="cr_div"><?= $form->field($model, 'credit_amount')->textInput(['class'=>'form-control']) ?></div>
                            <div id="dr_div"><?= $form->field($model, 'debit_amount')->textInput(['class'=>'form-control']) ?></div>



                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <?= $form->field($model, 'remarks')->textarea(['rows'=>'3','class'=>'form-control']) ?>


                    <?= $form->field($model, 'application_id')->hiddenInput(['class'=>'form-control'])->label(false); ?>
                    <?= $form->field($model, 'adjustment_date')->hiddenInput(['value'=>date('Y-m-d H:i:s'),'class'=>'form-control'])->label(false); ?>
                    <?= $form->field($model, 'adjusted_by')->hiddenInput(['value'=>Yii::$app->user->id,'class'=>'form-control'])->label(false); ?>
                </div>

<div class="row">

</div>





                <!--* @property string $adjustment_number
                * @property integer $application_id
                * @property integer $loan_item_id
                * @property integer $disbursement_id
                * @property string $type
                * @property double $original_amount
                * @property double $debit_amount
                * @property double $credit_amount
                * @property integer $reasons
                * @property string $remarks
                * @property string $adjustment_date
                * @property integer $adjusted_by-->


            </div>
        </div>
        <div class="text-right">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?php
            echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
            echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

            ?>
        </div>

    </div>
    <div class="col-md-7">
        <div class="panel">
            <div class="panel-heading"></div>
            <div class="panel-body">
                <table class="table table-bordered table-condensed" style="font-size: large;">
                    <thead>
                    <tr>
                        <th class="text-center bg-green">DR <span class="pull-right" id="dr_check"></span></th>
                        <th class="text-center bg-red">CR <span class="pull-right" id="cr_check"></span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th class="amounts text-blue" style="text-align: right; font-size: 18px; font-weight: bolder;" id="original"></th>
                        <th class="amounts text-red" style="text-align: right; font-size: 18px; font-weight: bolder;" id="CR_amount"></th>
                    </tr>

                    <tr>
                        <th class="amounts text-green" style="text-align: right; font-size: 18px; font-weight: bolder;" id="DR_amount"></th>
                        <th class="amounts text-green" style="text-align: right; font-size: 18px; font-weight: bolder;" id="balance_CF"></th>
                    </tr>
                    </tbody>
                    <tfoot class="text-blue">
                            <tr class="bg-blue-gradient"><th></th><th></th></tr>
                            <tr>
                                <th style="text-align: right; font-size: 18px; font-weight: bolder;" id="DR_balance"></th>
                                <th style="text-align: right; font-size: 18px; font-weight: bolder;" id="CR_balance"></th>
                            </tr>
                            <tr class="bg-blue-gradient"><th></th><th></th></tr>
                    </tfoot>
                </table>
            </div>
        </div>


        <div class="panel">
            <div class="panel-heading"><h4 class="text-bold text-uppercase">Loanee's Profile</h4></div>
            <div class="panel-body">
                <table class="table table-condensed table-bordered">
                    <tbody>
                    <tr>
                        <th class="text-bold text-uppercase">index#</th><td id="indexNo"></td>
                        <th class="text-bold text-uppercase">Registration#</th><td id="regNo"></td>
                    </tr>
                    <tr>
                        <th class="text-bold text-uppercase">Full Name</th><td id="applicantName" colspan="3"></td>
                    </tr>
                    <tr>
                        <th class="text-bold text-uppercase">Sex</th><td id="sex"></td>
                        <th class="text-bold text-uppercase">DOB</th><td id="birthDate"></td>
                    </tr>
                    <tr>
                        <th class="text-bold text-uppercase">Phone#</th><td id="phone_number"></td>
                        <th class="text-bold text-uppercase">E-mail</th><td id="email_address"></td>
                    </tr>

                    <tr>
                        <th class="text-bold text-uppercase">Programme</th><td id="programme"></td>
                        <th class="text-bold text-uppercase">Year of Study</th><td id="studyYear"></td>
                    </tr>
                    <tr>
                        <th class="text-bold text-uppercase">Institution</th><td colspan="3" id="institution"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="panel">
            <div class="panel-heading"><h4 class="text-bold text-uppercase">Disbursement History</h4></div>
            <div class="panel-body">
                <table class="table table-condensed table-striped table-bordered">
                    <thead class="bg-blue-gradient">
                    <tr class="text-uppercase">
                        <th>Loan Item</th>
                        <th>Academic Year</th>
                        <th class="text-right">Allocated Amount</th>
                        <th class="text-right">Disbursed Percentage</th>
                        <th class="text-right">Disbursed Amount</th>
                        <th class="text-right">Remaining Amount</th>
                    </tr>
                    </thead>
                    <tbody id="history_table"></tbody>
                    <tfoot class="bg-blue-gradient">
                    <tr class="text-uppercase">
                        <th>TOTAL</th>
                        <th></th>
                        <th id="total_allocated" class="text-right text-bold">0.00</th>
                        <th class="text-right text-bold"></th>
                        <th id="total_disbursed" class="text-right text-bold">0.00</th>
                        <th id="total_remainig" class="text-right text-bold">0.00</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
</div>

<script>
    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }
    function SearchLoanee(){
        var spinner = '<span class="fa fa-spinner fa-2x fa-spin">';
        $("#search_feedback").html(spinner);
       // $("#history_table").html('<tr><td colspan="4" style="font-size: large;">'+spinner+' Please Wait ... Loading</td></tr>');
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-adjustment/search-lonee'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                search:$('#search').val()
            },
            success:function (data) {
               // console.log(data);
                $("#history_table").html('');
                var dataArray = data.output;
                var totalAllocation = 0;
                var totalDisbursement = 0;
                if (dataArray.length!=0){
                    $("#search_feedback").html('<span class="fa fa-check fa-2x text-green"></span>');

                    $.each(dataArray,function (index,value) {
                        $('#applicantName').html(value.full_name);
                        $('#regNo').html(value.registration_number);
                        $('#indexNo').html(value.index_number);
                        $('#sex').html(value.Sex);
                        $('#birthDate').html(value.DOB);
                        $('#programme').html(value.programme_name+' ( <b>'+value.programme_code+'</b>)'+' [ <b>'+value.years_of_study+'</b> years]');
                        $('#institution').html(value.Institution);
                        $('#studyYear').html(value.current_study_year);
                        $('#phone_number').html(value.phone_number);
                        $('#email_address').html(value.email_address);
                        $("#disbursementadjustment-application_id").val(value.application_id);

                        $("#history_table").append(
                            '<tr>' +
                            '<td>'+value.item_name+'</td>' +
                            '<td>'+value.academic_year+'</td>' +
                            '<td class="text-right text-bold">'+commaSeparateNumber((value.allocated_amount*1).toFixed(2))+'</td>' +
                            '<td class="text-right text-bold">'+commaSeparateNumber((value.disbursed_percentage*1).toFixed(2))+'</td>' +
                            '<td class="text-right text-bold">'+commaSeparateNumber((value.disbursed_amount*1).toFixed(2))+'</td>' +
                            '<td class="text-right text-bold">'+commaSeparateNumber(((value.allocated_amount*1)-(value.disbursed_amount*1)).toFixed(2))+'</td>' +


                            '</tr>'
                        );
                        ;
                        totalAllocation+=(value.allocated_amount*1);
                        totalDisbursement+=(value.disbursed_amount*1);
                        $("#total_allocated").html(commaSeparateNumber((totalAllocation*1).toFixed(2)));
                        $("#total_disbursed").html(commaSeparateNumber((totalDisbursement*1).toFixed(2)));
                        $("#total_remainig").html(commaSeparateNumber((totalAllocation-totalDisbursement).toFixed(2)));


                    });

                }else{
                    $("#search_feedback").html('<span class="fa fa-ban fa-2x text-red"></span>');
                    $('#applicantName').html('');
                    $('#regNo').html('');
                    $('#indexNo').html('');
                    $('#sex').html('');
                    $('#birthDate').html('');
                    $('#programme').html('');
                    $('#institution').html('');
                    $("#disbursementadjustment-application_id").val('');
                    $("#history_table").html('<tr><td colspan="4" style="font-size: large;"><span class="text-red fa fa-ban fa-3x"></span> Record Not Found</td></tr>');

                }

            }
        });
    }

    function SearchLoaneeDisbursement(){
        var spinner = '<span class="fa fa-spinner fa-2x fa-spin">';
        $("#items_feedback").html(spinner);
        // $("#history_table").html('<tr><td colspan="4" style="font-size: large;">'+spinner+' Please Wait ... Loading</td></tr>');
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-adjustment/search-lonee-disbursement'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                application:$("#disbursementadjustment-application_id").val(),
                semester:$("#semester").val(),
                instalment:$("#instalment").val(),
            },
            success:function (data) {
                console.log(data);

                var dataArray = data.output;

                if (dataArray.length!=0){
                    $("#items_feedback").html('<span class="fa fa-check fa-2x text-green"></span>');

                   /* $.each(dataArray,function (index,value) {


                    });*/

                }else{
                    $("#items_feedback").html('<span class="fa fa-ban fa-2x text-red"></span>');

                }

            }
        });
    }



    function AdjustmentEffect(){
        var type = $("#disbursementadjustment-type").val();
        var original = $("#disbursementadjustment-original_amount").val()*1;

        var amount = 0;
        var balance = 0;
        var balance_CF = 0;
        $(".amounts").html("");

        $("#original").html('<span class="pull-left text-muted">Balance BF </span>'+commaSeparateNumber(original));

        if(type==='CR'){
            amount = $("#disbursementadjustment-credit_amount").val()*1;
            balance_CF = (original-amount);
            balance =balance_CF+amount;

            $("#CR_amount").html('<span class="pull-left text-muted">CR ADJ.</span>'+commaSeparateNumber(amount));
            $("#CR_balance").html(commaSeparateNumber(balance));
            $("#DR_balance").html(commaSeparateNumber(original));

        }else{
            amount = $("#disbursementadjustment-debit_amount").val()*1;
            $("#DR_amount").html('<span class="pull-left text-muted">DR ADJ.</span>'+commaSeparateNumber(amount));
            balance = original + amount;
            balance_CF = balance;
            $("#DR_balance").html(commaSeparateNumber(balance));

            var cr_balance = balance_CF;

            $("#CR_balance").html(commaSeparateNumber(cr_balance));

        }

        $("#balance_CF").html('<span class="pull-left text-muted">Balance CF </span>'+commaSeparateNumber(balance_CF));



    }
    
    
    function CheckSelected() {
        var type =$("#disbursementadjustment-type").val();
        if (type==='CR'){
            $("#cr_div").show();
            $("#dr_div").hide();
            $("#disbursementadjustment-debit_amount").val(0.00);
        }else {
            $("#cr_div").hide();
            $("#dr_div").show();
            $("#disbursementadjustment-credit_amount").val(0.00);
        }
        AdjustmentEffect();
    }

    function SearchDisbursement() {
        var application =$("#disbursementadjustment-application_id").val();
        var semester = $("#semester").val();
        var instalment = $("#instalment").val();
        if(application !=='' && semester !=='' && instalment !==''){
            SearchLoaneeDisbursement();
        }
    }


    $(document).ready(function () {
        $("#cr_div").hide();
        $("#dr_div").hide();
        CheckSelected();
        AdjustmentEffect();
        $("#disbursementadjustment-disbursement_id").on('change',function () {

            var originalAmount =  $("#disbursementadjustment-disbursement_id option:selected").text();
             $("#disbursementadjustment-original_amount").val(originalAmount);


        });

        $("#disbursementadjustment-type").on('change',function () {

           CheckSelected();
            AdjustmentEffect();
            SearchDisbursement();
        });

            $("#disbursementadjustment-credit_amount").on('change keyup',function () {
                AdjustmentEffect();
                SearchDisbursement();
            });

            $("#disbursementadjustment-debit_amount").on('change keyup',function () {
                AdjustmentEffect();
                SearchDisbursement();
            });

        $("#searchButton").on('click',function () {
            SearchLoanee();
            SearchDisbursement();
        });

        $("#semester").on('change',function () {

            SearchDisbursement();
        });

        $("#instalment").on('change',function () {

            SearchDisbursement();
        });

        $("#disbursementadjustment-disbursement_id").on('change',function () {

            AdjustmentEffect();
        });






    });
</script>
 <?php ActiveForm::end(); ?>
