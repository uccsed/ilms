<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementAdjustment */

$this->title = 'Create Disbursement Adjustment';
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Adjustments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="disbursement-adjustment-create">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">
                <?= $this->render('_specialForm', [
                    'model' => $model,
                ]) ?>


            </div>
        </div>
    </div>
