<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementAdjustmentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disbursement-adjustment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'adjustment_number') ?>

    <?= $form->field($model, 'application_id') ?>

    <?= $form->field($model, 'loan_item_id') ?>

    <?= $form->field($model, 'disbursement_id') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'original_amount') ?>

    <?php // echo $form->field($model, 'debit_amount') ?>

    <?php // echo $form->field($model, 'credit_amount') ?>

    <?php // echo $form->field($model, 'reasons') ?>

    <?php // echo $form->field($model, 'remarks') ?>

    <?php // echo $form->field($model, 'adjustment_date') ?>

    <?php // echo $form->field($model, 'adjusted_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
