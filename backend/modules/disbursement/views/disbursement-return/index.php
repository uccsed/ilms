<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\DisbursementReturnSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disbursement Returns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-return-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Disbursement Return', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'return_batch_id',
            'index_number',
            'application_id',
            'header_id',
            // 'disbursement_batch_id',
            // 'sex',
            // 'year_of_study',
            // 'pay_sheet_serial_number',
            // 'loan_item',
            // 'instalment_number',
            // 'amount',
            // 'remarks:ntext',
            // 'academic_year',
            // 'status',
            // 'reasons:ntext',
            // 'created_by',
            // 'created_on',
            // 'sync_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
