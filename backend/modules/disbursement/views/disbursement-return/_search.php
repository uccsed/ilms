<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementReturnSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disbursement-return-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'return_batch_id') ?>

    <?= $form->field($model, 'index_number') ?>

    <?= $form->field($model, 'application_id') ?>

    <?= $form->field($model, 'header_id') ?>

    <?php // echo $form->field($model, 'disbursement_batch_id') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'year_of_study') ?>

    <?php // echo $form->field($model, 'pay_sheet_serial_number') ?>

    <?php // echo $form->field($model, 'loan_item') ?>

    <?php // echo $form->field($model, 'instalment_number') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'remarks') ?>

    <?php // echo $form->field($model, 'academic_year') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'reasons') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'sync_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
