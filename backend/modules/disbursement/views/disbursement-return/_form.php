<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementReturn */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disbursement-return-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'return_batch_id')->textInput() ?>

    <?= $form->field($model, 'index_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'application_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'header_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'disbursement_batch_id')->textInput() ?>

    <?= $form->field($model, 'sex')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'year_of_study')->textInput() ?>

    <?= $form->field($model, 'pay_sheet_serial_number')->textInput() ?>

    <?= $form->field($model, 'loan_item')->textInput() ?>

    <?= $form->field($model, 'instalment_number')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'academic_year')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'reasons')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_on')->textInput() ?>

    <?= $form->field($model, 'sync_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
