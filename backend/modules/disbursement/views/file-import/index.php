<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/29/18
 * Time: 11:11 AM
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'File Import Zone';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-fund-request-index">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4><?= Html::encode($this->title) ?></h4>
        </div>
        <div class="panel-body">
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading bg-primary"><h4 class="text-uppercase text-bold text-center">Semester Paid Beneficiaries</h4></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">

                            <a href="<?php echo Url::to(['/disbursement/file-import/enrollment']); ?>"><span class="fa fa-money fa-5x text-green"></span></a>
                        </div>
                        <div class="col-md-8">
                            <p style="text-align: justify;">
                                Upload an Excel with a list of <b>Paid beneficiaries</b> as per specified template.
                                <span class="pul-right text-right">
                                   Click <a href="../attachments/disbursement/PAYMENT_SAMPLE_FILE.xls" class="text-green text-bold text-xl-center" target="_blank">Here</a> to download Template
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading bg-primary"><h4 class="text-uppercase text-bold text-center">Inter-semester Dropouts</h4></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="<?php echo Url::to(['/disbursement/file-import/drop-outs']); ?>"><span class="fa fa-user-times fa-5x text-red"></span></a>
                        </div>

                        <div class="col-md-8">
                            <p style="text-align: justify;">
                                Upload an Excel with a list of <b>inter-semester dropouts beneficiaries</b> as per specified template
                                <span class="pul-right text-right">
                                   Click <a href="../attachments/disbursement/DOPOUTS_SAMPLE_FILE.xls" class="text-red text-bold text-xl-center" target="_blank">Here</a> to download Template
                                </span>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
