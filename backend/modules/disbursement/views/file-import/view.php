<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 3/12/19
 * Time: 10:42 PM
 */
?>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\tabs\TabsX;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementReturnBatch */

$this->title = "Loanees Payments Deposits for ".$model->learningInstitution->institution_name.' ('.$model->learningInstitution->institution_code.') '.$model->academicYear->academic_year;
$this->params['breadcrumbs'][] = ['label' => 'Loanees Payments Deposit Batches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
$detailView = $updated = $confirmed = '';
$detailView.='<p>';
$detailView.= Html::a('Update', ['/disbursement/disbursement-deposit-batch/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
if ($model->status=="NEW"){
    $detailView.= Html::a('Delete', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]);
}
//NEW | MATCH | MISMATCH | CONFIRMED
$detailView.='</p>';
$detailView.=  DetailView::widget([
    'model' => $model,
    'attributes' => [

        //'institution_id',
        [
            'name'=>'institution_id',
            'value'=>$model->learningInstitution->institution_name,
            'label'=>'Learning Institution',
            'format'=>'raw',

        ],
        //'academic_year',
        [
            'name'=>'academic_year',
            'value'=>$model->academicYear->academic_year,
            'label'=>'Academic Year',
            'format'=>'raw',

        ],
          [
          'name'=>'cheque_amount',
            'value'=>number_format($model->cheque_amount,2),
            'label'=>'Total Amount',
            'format'=>'raw',

        ],
        'deposit_cheque_number',

        [
            'name'=>'deposit_date',
            'value'=>date('M jS, Y',strtotime($model->deposit_date)),
            'label'=>'Deposit Date',
            'format'=>'raw',

        ],
        //'financial_year',
        'description:ntext',
        'status',
       /* [
            'name'=>'PendingAmount',
            'value'=>number_format($model->PendingAmount,2),
            'label'=>'Pending Amount',
            'format'=>'raw',

        ],
        [
            'name'=>'ConfirmedAmount',
            'value'=>number_format($model->ConfirmedAmount,2),
            'label'=>'Confirmed Amount',
            'format'=>'raw',

        ],

        [
            'name'=>'UpdatedAmount',
            'value'=>number_format($model->UpdatedAmount,2),
            'label'=>'Updated Amount',
            'format'=>'raw',

        ],*/
        [
            'name'=>'created_on',
            'value'=>date('D, d-M-Y',strtotime($model->created_on)),
            'label'=>'Created On',
            'format'=>'raw',

        ],
        //'created_on',
        //'created_by',
        [
            'name'=>'created_by',
            'value'=>Module::UserInfo($model->created_by,'fullName'),
            'label'=>'Created By',
            'format'=>'raw',

        ],
    ],
]);

?>


<div class="disbursement-return-batch-view">
    <div class="panel panel-info">
        <div class="panel-heading"><h4><?= Html::encode($this->title) ?></h4></div>
        <div class="panel-body">

            <?php

            echo TabsX::widget([
                'items' => [
                    [
                        'label' => 'Deposit Summary',
                        'content' =>$detailView,
                        'id' => '1',
                    ],
                   [
                        'label' => 'Pending Deposits',
                        'content' => '<iframe src="' . yii\helpers\Url::to(['/disbursement/disbursement-deposit-batch/staging', 'batchID' =>$model->id, 'status' =>'NEW']) . '" width="100%" height="600px" style="border: 0"></iframe>',
                        'id' => '2',
                    ],

                     [
                        'label' => 'Matched',
                        'content' => '<iframe src="' . yii\helpers\Url::to(['/disbursement/disbursement-deposit-batch/staging', 'batchID' =>$model->id, 'status' =>'MATCH']) . '" width="100%" height="600px" style="border: 0"></iframe>',
                        'id' => '3',
                    ],

                    [
                        'label' => 'MisMatched',
                        'content' => '<iframe src="' . yii\helpers\Url::to(['/disbursement/disbursement-deposit-batch/staging', 'batchID' =>$model->id, 'status' =>'MISMATCH']) . '" width="100%" height="600px" style="border: 0"></iframe>',
                        'id' => '4',
                    ],
                    [
                        'label' => 'Confirmed',
                        //'content' =>$confirmed,
                        'content' => '<iframe src="' . yii\helpers\Url::to(['/disbursement/disbursement-deposit-batch/staging', 'batchID' =>$model->id, 'status' =>'CONFIRMED']) . '" width="100%" height="600px" style="border: 0"></iframe>',

                        'id' => '5',
                    ],


                ],
                'position' => TabsX::POS_ABOVE,
                'bordered' => true,
                'encodeLabels' => false
            ]);
            ?>

        </div>
    </div>




</div>

