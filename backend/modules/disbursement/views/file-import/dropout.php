<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/29/18
 * Time: 3:16 PM
 */


use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\InstitutionFundRequest */

$this->title = 'Upload Inter-Semester Dropouts';
$this->params['breadcrumbs'][] = ['label' => 'Inter-Semester Dropouts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="institution-fund-request-create">
    <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
                'analysis' => $analysis,
            ]) ?>
        </div>
    </div>
</div>
