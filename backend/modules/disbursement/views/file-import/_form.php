<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/29/18
 * Time: 3:16 PM
 */

	use yii\widgets\ActiveForm;
	use yii\helpers\Html;
	use kartik\widgets\FileInput;
	use kartik\select2\Select2;
	use yii\helpers\ArrayHelper;
	use backend\modules\disbursement\Module;


	//echo Module::InstitutionDecoder('UDOM',false);
echo  Module::FetchBootstrap('js');
//echo Module::InstalmentInfo(1,'number','instalment_definition_id');
?>

<?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]);?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?php
                echo $form->field($model, 'fileImport')->widget(FileInput::classname(), [
                    //'options' => ['accept' => 'image/*'],
                    'options' => ['accept' => 'xls/*'],
                    'pluginOptions' => [
                        'showPreview' => false,
                        'showCaption' => true,
                        'showRemove' => true,
                        'showUpload' => false
                    ]
                ]);
                ?>

            </div>

        </div>

        <div class="col-md-6">
             <div class="form-group">
                <?php echo
                '<label class="control-label">Learning Institution</label>'.
                Select2::widget(
                    [
                        'name' => 'institution',
                        'value' => '',
                        'data' => ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql("SELECT learning_institution_id, CONCAT(institution_name,' (',institution_code,')') AS 'name' FROM learning_institution  WHERE is_active = '1' AND institution_type = 'UNIVERSITY' ORDER BY institution_name ASC")->asArray()->all(),'learning_institution_id','name'),
                        'options' => ['label' => 'Learning Institution', 'placeholder' => 'Select Institution ...', 'id' => 'institution']
                    ]
                );
                // $locationDropdown = $this->form->field($this->model, 'location_id')->label(false)->widget(DepDrop::classname(), ['type' => DepDrop::TYPE_SELECT2, 'options' => ['id' => 'location-selection'], 'select2Options' => ['pluginOptions' => ['allowClear' => TRUE]], 'pluginOptions' => ['depends' => ['country-selection'], 'placeholder' => 'Select Location', 'url' => Url::to(['/location/admin/load'])]]);
                ?>

            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading bg-green"><h3>SUCCESSFULLY UPLOADED</h3></div>
            <div class="panel-body table-responsive">
                <table class="table table-condensed table-bordered table-striped table-hover table-responsive">
                    <thead>
                    <tr class="bg-green-gradient">
                        <th>Row #</th>
                        <th>Index #</th>
                        <th>Academic Year</th>
                        <th>Semester #</th>
                        <th>Remarks</th>
                    </tr>
                    </thead>
                    <tbody><?php echo $analysis['success']; ?></tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-danger">
            <div class="panel-heading bg-red"><h3>FAILED TO UPLOAD</h3></div>
            <div class="panel-body table-responsive">
                <table class="table table-condensed table-bordered table-striped table-hover table-responsive">
                    <thead>
                    <tr class="bg-red-gradient">
                        <th>Row #</th>
                        <th>Index #</th>
                        <th>Remarks</th>
                    </tr>
                    </thead>
                    <tbody><?php echo $analysis['error']; ?></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="progress"></div>
<div class="form-group">
    <?= Html::submitButton('Import',['class'=>'btn btn-primary','id'=>'importBtn']);?>
    <?= Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);?>
</div>

<script>
    $(document).ready(function () {
        $("#importBtn").on("click",function () {
            $(this). hide();
            $("#progress").html('<div style="font-size: large;"><span class="fa fa-3x fa-spinner fa-spin"></span> Please Wait... Uploading & Analyzing the File... </div>');
        });
    });
</script>
<?php ActiveForm::end();?>