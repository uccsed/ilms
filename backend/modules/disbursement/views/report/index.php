<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\report\models\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disbursement Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-index">
    <div class="panel panel-info">
    <div class="panel-heading">  

    <?= Html::encode($this->title) ?>
    </div>
        <div class="panel-body">

   <!-- <p>
        <?php /*= Html::a('Configure Report', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->
<!--    --><?php /*= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                     'attribute' => 'category',
                        'format' => 'raw',
                        'value' => function ($model) {
                            if($model->category=='1'){
                             return 'Application';   
                            }else if($model->category=='2'){
                             return 'Allocation';   
                            }else if($model->category=='3'){
                             return 'Disbursement';    
                            }else if($model->category=='4'){
                             return 'Repayment';   
                            }else if($model->category=='5'){
                             return 'Apeal';   
                            }else if($model->category=='6'){
                             return 'Complain';   
                            }
                             
                            
                        },
                    ],
            'package',
            ['class' => 'yii\grid\ActionColumn','template'=>'{view}{update}{delete}'],
        ],
    ]); */?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    'package',
                    //['class' => 'yii\grid\ActionColumn','template'=>'{view}'],


                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7'],
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'view'),
                                ]);
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'view') {
                                $url ='index.php?r=disbursement/report/view-operation&id='.$model->id;
                                return $url;
                            }
                        }
                    ],
                ],
            ]); ?>
</div>
  </div>
</div>
