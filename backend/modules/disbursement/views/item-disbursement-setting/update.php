<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/3/18
 * Time: 3:20 PM
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */

$this->title = 'Update Item Disbursement: for '.$model->LoanItem.' to '.$model->StudyCategory.' '. $model->StudyLevel;
$this->params['breadcrumbs'][] = ['label' => 'Institution Assignments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="institution-assignment-update">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?= $this->render('_formEdit', [
                'model' => $model,
            ]) ?>






        </div>
    </div>
</div>

