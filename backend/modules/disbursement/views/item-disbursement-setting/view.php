<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/3/18
 * Time: 3:12 PM
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */

$this->title = $model->LoanItem.' Configured to be disbursed to '.$model->DisbursedTo.' for '.$model->StudyCategory.' '.$model->StudyLevel;
$this->params['breadcrumbs'][] = ['label' => 'Institution Assignments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-assignment-view">
    <div class="institution-assignment-index">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">

                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'LoanItem',
                    'StudyCategory',
                    'StudyLevel',
                    'DisbursedTo',

                    'sync_id',

                ],
            ]) ?>
            </div>
</div>