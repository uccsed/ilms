<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/3/18
 * Time: 11:02 AM
 */

?>

<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>

<?php
echo Form::widget([
    'model' => $model,
    'form' => $form,
    'columns' =>1,
    'attributes' => [



        'study_level' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Study Level',
            'options' => [
                'data' => ArrayHelper::map(\backend\modules\allocation\models\ApplicantCategory::findBySql('SELECT * FROM applicant_category ORDER BY applicant_category ASC')->asArray()->all(), 'applicant_category_id', 'applicant_category'),

                //'data' =>ArrayHelper::map(\frontend\modules\application\models\AttachmentDefinition::findBySql('SELECT attachment_definition_id,attachment_desc FROM `attachment_definition`')->asArray()->all(), 'attachment_definition_id', 'attachment_desc'),

                'options' => [
                    'prompt' => 'Select',

                ],
            ],
        ],


        'study_category' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Study Category',
            'options' => [
                'data' => Module::StudyCategory(),

                //'data' =>ArrayHelper::map(\frontend\modules\application\models\AttachmentDefinition::findBySql('SELECT attachment_definition_id,attachment_desc FROM `attachment_definition`')->asArray()->all(), 'attachment_definition_id', 'attachment_desc'),

                'options' => [
                    'prompt' => 'Select',

                ],
            ],
        ],

        'loan_item_id' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Loan Items',
            'options' => [
                'data' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::findBySql('SELECT * FROM loan_item WHERE is_active="1" ORDER BY item_name ASC')->asArray()->all(), 'loan_item_id', 'item_name'),

                //'data' =>ArrayHelper::map(\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),

                'options' => [
                    'prompt' => 'You can Select Multiple items',
                    'multiple'=>TRUE,

                ],
            ],
        ],

        'disbursed_to' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Item Disbursed To',
            'options' => [
                'data' => Module::DisbursedEntities(),

                //'data' =>ArrayHelper::map(\frontend\modules\application\models\AttachmentDefinition::findBySql('SELECT attachment_definition_id,attachment_desc FROM `attachment_definition`')->asArray()->all(), 'attachment_definition_id', 'attachment_desc'),

                'options' => [
                    'prompt' => 'Select',

                ],
            ],
        ],


        /*'institution_id' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Institution',
            'options' => [
                'data' => ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql('SELECT * FROM learning_institution WHERE institution_type="UNIVERSITY" AND learning_institution_id NOT IN (SELECT institution_id FROM institution_assignment) ORDER BY institution_name ASC')->asArray()->all(), 'learning_institution_id', 'institution_code'),

                //'data' =>ArrayHelper::map(\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),

                'options' => [
                    'prompt' => 'Select',
                    'multiple'=>TRUE,

                ],
            ],
        ],*/

        //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
    ]
]);
?>

<div class="text-right">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php
    echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
    echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

    ActiveForm::end();
    ?>
</div>
