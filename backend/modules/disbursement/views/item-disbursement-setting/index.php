<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/1/18
 * Time: 4:00 PM
 */
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\application\models\VerificationCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Item Disbursement Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-assignment-index">
	<div class="panel panel-info">
    <div class="panel-heading">
    <?= Html::encode($this->title) ?>
    </div>
        <div class="panel-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Configure Item Disbursement', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

                   [
                    'attribute' => 'loan_item_id',
                        'label'=>"Loan Item",
                        'format' => 'raw',
                        'value' => function ($model) {
                             return $model->LoanItem;
                        },
                       'filterType' => GridView::FILTER_SELECT2,
                       //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                       'filter' =>ArrayHelper::map(\backend\modules\allocation\models\LoanItem::findBySql('SELECT * FROM loan_item ORDER BY item_name ASC')->asArray()->all(), 'loan_item_id', 'item_name'),
                       'filterWidgetOptions' => [
                           'pluginOptions' => ['allowClear' => true],
                       ],
                       'filterInputOptions' => ['placeholder' => 'Search  '],
                       'format' => 'raw'
                    ],

            [
                'attribute' => 'study_category',
                'label'=>"Study Category",
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->StudyCategory;
                },
                'filterType' => GridView::FILTER_SELECT2,
                //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                //'filter' =>ArrayHelper::map(\backend\modules\allocation\models\LoanItem::findBySql('SELECT * FROM loan_item ORDER BY item_name ASC')->asArray()->all(), 'loan_item_id', 'item_name'),
                'filter' =>Module::StudyCategory(),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search  '],
                'format' => 'raw'
            ],

            [
                'attribute' => 'study_level',
                'label'=>"Study Level",
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->StudyLevel;
                },
                'filterType' => GridView::FILTER_SELECT2,
                //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                'filter' =>ArrayHelper::map(\backend\modules\allocation\models\ApplicantCategory::findBySql('SELECT * FROM applicant_category ORDER BY applicant_category ASC')->asArray()->all(), 'applicant_category_id', 'applicant_category'),

                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search  '],
                'format' => 'raw'
            ],


            [
                'attribute' => 'disbursed_to',
                'label'=>"Disbursed To",
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->DisbursedTo;
                },
                'filterType' => GridView::FILTER_SELECT2,
                //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                //'filter' =>ArrayHelper::map(\backend\modules\allocation\models\LoanItem::findBySql('SELECT * FROM loan_item ORDER BY item_name ASC')->asArray()->all(), 'loan_item_id', 'item_name'),
                'filter' =>Module::DisbursedEntities(),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search  '],
                'format' => 'raw'
            ],





            //'comment',
            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{update}{delete}',
                ],
        ],
    ]); ?>
</div>
  </div>
</div>
