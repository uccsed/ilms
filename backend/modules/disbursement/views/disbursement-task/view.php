<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementTask */

$this->title = $model->task_name;
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-task-view">
<div class="panel panel-info">
    <div class="panel-heading"><?= Html::encode($this->title) ?></div>
    <div class="panel-body">
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->disbursement_task_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->disbursement_task_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'disbursement_task_id',
                'task_name',
                //'status',
                [
                    'name'=>'status',
                    'value'=>$model->status=='1'?'ACTIVE':'CEASED',
                    'label'=>'Item Status'
                ]
            ],
        ]) ?>
    </div>
</div>




</div>
