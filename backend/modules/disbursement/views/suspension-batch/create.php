<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\SuspensionBatch */

$this->title = 'Create '.ucwords(strtolower($model->type)).' Batch';
$this->params['breadcrumbs'][] = ['label' => 'Suspension Batches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suspension-batch-create">
<div class="panel">
    <div class="panel-heading">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
            'type' => $type,
            'modelImport'=>$modelImport,

        ]) ?>
    </div>
</div>

</div>
