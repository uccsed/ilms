<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\SuspensionStaging */

$this->title = 'Update Suspension Staging: #' . $model->sn;
$this->params['breadcrumbs'][] = ['label' => 'Suspension Stagings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="suspension-staging-update">


<div class="panel panel-flat">
    <div class="panel-heading"><h4><?= Html::encode($this->title) ?></h4></div>
    <div class="panel-body">
        <?= $this->render('_staging_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>


</div>
