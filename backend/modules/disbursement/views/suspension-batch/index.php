<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
use kartik\widgets\DatePicker;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\SuspensionBatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Suspension Batches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suspension-batch-index">

    <div class="panel">
        <div class="panel-heading"><h3 class="text-uppercase"><?= Html::encode($this->title) ?></h3>
            <p>
                <?= Html::a('Create Suspension Batch', ['create'], ['class' => 'btn btn-success']) ?>
                <a href="../attachments/disbursement/SUSPENSION_SAMPLE_FILE.xls" class="btn btn-primary pull-right" target="_blank">Download BULK Suspension Template</a>
            </p>
        </div>
        <div class="panel-body">
            <?php
            $gridColumns = [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'hAlign' => GridView::ALIGN_CENTER,
                ],
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'allowBatchToggle' => true,
                    'detail' => function ($model) {
                        //return $this->render('shared_list',['batchID'=>$model->id,'status'=>$model->status]);
                        return $this->render('shared_list',['batchID'=>$model->id,'status'=>null]);
                    },
                    'detailOptions' => [
                        'class' => 'kv-state-enable',
                    ],
                ],
                [
                    'attribute' => 'id',
                    'label'=>"Header ID",
                    'format' => 'raw',
                    //'hAlign'=>'right',
                    'width'=>'120px',
                    'value' => function ($model) {
                        return $model->id;
                    },


                ],
                [
                    'attribute' => 'title',
                    'label'=>"TITLE",
                    'format' => 'raw',
                    //'hAlign'=>'right',
                    //'width'=>'120px',
                    'value' => function ($model) {
                        return $model->title;
                    },

                    'pageSummary'=>'GRAND TOTAL',
                    'pageSummaryOptions'=>['class'=>'text-right text-white'],
                ],


                [
                    'attribute' => 'description',
                    'label'=>"NUMBER OF STUDENTS",
                    'format' => ['decimal',2],
                    'hAlign'=>'right',
                    //'width'=>'120px',
                    'value' => function ($model) {
                        return Module::SuspensionBatchStudents($model->id);
                    },
                    'pageSummary'=>true,
                ],



                [
                    'attribute' => 'status',
                    'label'=>"STATUS",
                    'format' => 'raw',
                    'width'=>'120px',
                    'value' => function ($model) {
                        return Module::SuspensionStatus($model->status);
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => Module::SuspensionStatus(null),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],

                ],

// ['class' => 'yii\grid\ActionColumn'],

                ['class' => 'kartik\grid\ActionColumn',
                    'template'=>'{view}',
                ],
            ];
            ?>

            <?php
            /*echo*/ ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,

                'fontAwesome' => true,
//            'asDropdown' => false
                'batchSize' => 50,
                'target' => '_blank',
                'selectedColumns' => [0, 1, 2, 3, 4, 5, 6, 7], // Col seq 2 to 6
                'columnSelectorOptions' => [
                    'label' => 'Export Columns',
                ],
                // 'hiddenColumns' => [15], // SerialColumn, Color, & ActionColumn
                //'disabledColumns' => [0, 1, 2, 3, 4, 5, 6, 9, 12], // ID & Name
                'noExportColumns' => [15],
                'dropdownOptions' => [
                    'label' => 'Export Data',
                    'class' => 'btn btn-default'
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_EXCEL => false,
                    ExportMenu::FORMAT_EXCEL_X => false,
                ],
                //'folder' => '@webroot/tmp', // this is default save folder on server
            ]) . "<hr>\n";
            ?>

            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                'showPageSummary'=>true,
                'pageSummaryRowOptions'=>['class'=>'text-bold bg-blue-gradient'],
                'pjax'=>true,
                'striped'=>true,
                'hover'=>true,
            ]);

            ?>
        </div>
    </div>


</div>
