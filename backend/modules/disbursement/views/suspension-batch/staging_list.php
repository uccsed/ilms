<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/3/18
 * Time: 10:18 PM
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
echo Module::FetchBootstrap('js');
//echo  $batchID;
?>
<p>
    <?php if ($status == '0' || $status == '-1'){ ?>
    <button id="btnMatch<?php echo $status; ?>" class="btn btn btn-primary"><span id="cross_match_icon<?php echo $status; ?>" class="fa fa-search"></span> Run A Cross-Match</button>
    <?php } ?>

    <?php if ($status == '1'){ ?>

    <button id="btnConfirm" class="btn btn btn-success pull-right"><span id="confirmation_icon" class="fa fa-check"></span> Confirm Match Matched Records</button>
    <?php } ?>
</p>


<script>
    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }
    function CrossMatch() {
        var matchCounter = 0;
        $("#cross_match_icon<?php echo $status; ?>").attr("class","fa fa-spinner fa-spin");
        $("#btnMatch<?php echo $status; ?>").attr("disabled","disabled");
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/suspension-batch/cross-match'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                headerID:'<?php echo $batchID; ?>'
            },
            success:function (data) {
                var results = data.output['results'];
                matchCounter = results.length;
                $("#cross_match_icon<?php echo $status; ?>").attr("class","fa fa-search");
                $("#btnMatch<?php echo $status; ?>").removeAttr("disabled");

                alert('A Cross-match has been made and found '+commaSeparateNumber(matchCounter)+' records');
                //window.reload();
                 window.location.replace("#");
            }
        });
    }

    function Confirmation() {
        var ConfirmationCounter = 0;
        $("#confirmation_icon").attr("class","fa fa-spinner fa-spin");
        $("#btnConfirm").attr("disabled","disabled");
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/suspension-batch/confirmation'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                headerID:'<?php echo $batchID; ?>'
            },
            success:function (data) {
                var results = data.output['results'];
                ConfirmationCounter = results.length;
                $("#confirmation_icon").attr("class","fa fa-check");
                $("#btnConfirm").removeAttr("disabled");

                alert(commaSeparateNumber(ConfirmationCounter)+' records has been successfully confirmed!');
                //window.reload();
                window.location.replace("#");
            }
        });
    }

    $(document).ready(function () {

        $("#btnMatch<?php echo $status; ?>").on('click',function () {
            CrossMatch();
        });

        $("#btnConfirm").on('click',function () {
            Confirmation();
        });
    });
</script>

<?php
$RESULTS = \backend\modules\disbursement\models\SuspensionStaging::findBySql("SELECT programme FROM suspension_staging WHERE header_id = '$batchID' ORDER BY programme ASC")->all();
//print_r($RESULTS);
$columns = [
    ['class'=>'kartik\grid\SerialColumn'],


    [
        'attribute'=>'index_number',
        //'width'=>'250px',
        'label'=>"INDEX #",

    ],

    [
        'attribute'=>'names',
        //'width'=>'250px',
        'label'=>"NAME",
        //'nowrap'=>true,

    ],
    [
        'attribute' => 'programme',
        //'width'=>'250px',
        'label'=>"PROGRAMME",
        'hAlign'=>'center',
        'format' => 'raw',
        'value' => function ($model) {
            return strtoupper($model->programme);
        },
        'filterType' => GridView::FILTER_SELECT2,
          'filter' => ArrayHelper::map($RESULTS,'programme','programme'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

    ],


    [
        'attribute'=>'year_of_study',
        //'width'=>'250px',
        'hAlign'=>'center',
        'format' => 'raw',
        'value' => function ($model) {
            return strtoupper($model->year_of_study);
        },
        'label'=>"YOS",
        'filterType' => GridView::FILTER_SELECT2,
              'filter' => array_combine(range(1,10),range(1,10)),
               'filterWidgetOptions' => [
                   'pluginOptions' => ['allowClear' => true],
               ],
               'filterInputOptions' => ['placeholder' => 'Search  '],
    ],



    [
        'attribute'=>'suspension_reason',
        'width'=>'150px',
        //'hAlign'=>'right',
        'value' => function ($model) {
            return Module::SuspensionReason($model->suspension_reason,'name');
        },
        'label'=>'REMARKS',
        'format'=>'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(\backend\modules\disbursement\models\SuspensionReason::findAll(['is_active'=>'1']),'id','name'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],
    ],



    //'comment',
    /*['class' => 'yii\grid\ActionColumn',
        'template'=>'{view}{delete}',
    ],*/

    [
        'attribute' => 'status',
        //'width'=>'250px',
        'label'=>"STATUS",
        'format' => 'raw',
        'value' => function ($model) {
            return Module::SuspensionStatus($model->status);
        },
        'filter'=>false,
        /*'filterType' => GridView::FILTER_SELECT2,
       'filter' => Module::SuspensionStatus(null),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],*/
        'format' => 'raw'
    ],

    ['class' => 'kartik\grid\ActionColumn',
        'template'=>'{update}{delete}',
        'buttons' => [
                    'update' => function ($url,$model) {
                      return Html::a(
                           '<span class="fa fa-pencil" title="Edit"></span>',
                          Yii::$app->urlManager->createAbsoluteUrl(array('disbursement/bulk-suspension/update-staging','id'=>$model->id)));
                   },
                  /*   'view' => function ($url,$model,$key) {
                            return Html::a('<span class="green">View Detail</span>', $url);
                   },*/
                 'delete' => function ($url,$model) {
                return Html::a(
                    '<span class="fa fa-trash" title="Delete"></span>',
                    Yii::$app->urlManager->createAbsoluteUrl(array('disbursement/bulk-suspension/delete-staging','id'=>$model->id)));
            },

              ],
    ],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'columns' => $columns,
    'panel'=>['class'=>'primary']
]); ?>

