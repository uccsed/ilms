<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\SuspensionStaging */
/* @var $form yii\widgets\ActiveForm */
/*
* @property integer $id
 * @property string $sn
 * @property string $index_number
 * @property string $names
 * @property string $programme
 * @property string $year_of_study
 * @property string $sync_id
 * @property integer $status
 * @property string $status_date
 * @property integer $header_id
 * @property integer $suspension_reason
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $matching_remarks
 */
?>

<div class="suspension-staging-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="form-group">
    <?= $form->field($model, 'sn')->textInput(['maxlength' => true,'class'=>'form-control col-lg-3']) ?>
</div>
    <div class="form-group">
        <?= $form->field($model, 'index_number')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'names')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'programme')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'year_of_study')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
    </div>










    <div style="display: none;">
        <?= $form->field($model, 'status')->textInput() ?>

        <?= $form->field($model, 'status_date')->textInput() ?>

        <?= $form->field($model, 'header_id')->textInput() ?>

        <?= $form->field($model, 'created_by')->textInput() ?>

        <?= $form->field($model, 'created_at')->textInput() ?>

        <?= $form->field($model, 'updated_by')->textInput() ?>

        <?= $form->field($model, 'updated_at')->textInput() ?>
    </div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-danger pull-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
