<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\SuspensionBatch */

$this->title = 'Update '.ucwords(strtolower($model->type)).' Batch: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => ''.ucwords(strtolower($model->type)).' Batches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';


?>
<div class="suspension-batch-create">
    <div class="panel">
        <div class="panel-heading">
            <h3><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
                'type' => $type,
                'modelImport'=>$modelImport,

            ]) ?>
        </div>
    </div>

</div>
