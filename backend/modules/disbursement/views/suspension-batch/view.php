<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\tabs\TabsX;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\SuspensionBatch */

$this->title = ucwords(strtolower($model->type)).' Header ID '.$model->id.': '.$model->title;
$this->params['breadcrumbs'][] = ['label' => ucwords(strtolower($model->type)).' Batches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$detailView = DetailView::widget([
    'model' => $model,
    'attributes' => [
        // 'id',
        [
            'name'=>'id',
            'value'=>$model->id,
            'format'=>'html',
            'label'=>'Header ID',
        ],
        'title',
        'description:ntext',
        //'files_uploaded:ntext',
        //'status',
        [
               'name'=>'status',
               'value'=>Module::SuspensionStatus($model->status),
               'format'=>'html',
               'label'=>'Status',
        ],
        //'status_date',
        [
            'name'=>'status_date',
            'value'=>date('D d-M-Y H:i:s',strtotime($model->status_date)),
            'format'=>'html',
            'label'=>ucwords(strtolower(Module::SuspensionStatus($model->status).' since ')),
        ],
        //'created_by',
        [
            'name'=>'created_by',
            'value'=>Module::UserInfo($model->created_by,'fullName'),
            'format'=>'html',
            'label'=>'Created By',
        ],
        //'created_at',
        [
            'name'=>'created_at',
            'value'=>date('D d-M-Y H:i:s',strtotime($model->created_at)),
            'format'=>'html',
            'label'=>'Creation Date',
        ],
       // 'updated_by',
        [
            'name'=>'updated_by',
            'value'=>Module::UserInfo($model->updated_by,'fullName'),
            'format'=>'html',
            'label'=>'Last updated by',
        ],
        //'updated_at',
        [
            'name'=>'updated_at',
            'value'=>date('D d-M-Y H:i:s',strtotime($model->updated_at)),
            'format'=>'html',
            'label'=>'Last Update',
        ],
        //'type',
    ],
])
?>
<div class="suspension-batch-view">
<div class="panel">
    <div class="panel-heading">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="panel-body">
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>

            <?php
            $url = '/disbursement/bulk-'.strtolower($model->type).'/staging-list';
            echo TabsX::widget([
                'items' => [
                    [
                        'label' => ucwords(strtolower($model->type)).' Summary',
                        'content' =>$detailView,
                        'id' => '1',
                    ],
                    [
                        'label' => 'Pending '.ucwords(strtolower($model->type)).'s',
                        'content' => '<iframe src="' . yii\helpers\Url::to([$url, 'batchID' =>$model->id, 'status' =>'0']) . '" width="100%" height="600px" style="border: 0"></iframe>',
                        'id' => '2',
                    ],

                    [
                        'label' => 'Matched',
                        'content' => '<iframe src="' . yii\helpers\Url::to([$url, 'batchID' =>$model->id, 'status' =>'1']) . '" width="100%" height="600px" style="border: 0"></iframe>',
                        'id' => '3',
                    ],

                    [
                        'label' => 'MisMatched',
                        'content' => '<iframe src="' . yii\helpers\Url::to([$url, 'batchID' =>$model->id, 'status' =>'-1']) . '" width="100%" height="600px" style="border: 0"></iframe>',
                        'id' => '4',
                    ],

                    [
                        'label' => 'Updated',
                        'content' => '<iframe src="' . yii\helpers\Url::to([$url, 'batchID' =>$model->id, 'status' =>'2']) . '" width="100%" height="600px" style="border: 0"></iframe>',

                        'id' => '5',
                    ],

                ],
                'position' => TabsX::POS_ABOVE,
                'bordered' => true,
                'encodeLabels' => false
            ]);
            ?>
        </p>
    </div>
</div>





</div>
