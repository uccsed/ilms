<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/6/18
 * Time: 9:19 AM
 */
use backend\modules\disbursement\models\SuspensionStagingSearch;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;

$searchModel = new SuspensionStagingSearch();
$dataProvider = $searchModel->SpecialSearch(Yii::$app->request->queryParams,$batchID,$status);
$dataProvider->pagination=false;
?>

<?php

$RESULTS = \backend\modules\disbursement\models\SuspensionStaging::findBySql("SELECT programme FROM suspension_staging WHERE header_id = '$batchID' ORDER BY programme ASC")->all();
//print_r($RESULTS);
$columns = [
    ['class'=>'kartik\grid\SerialColumn'],


    [
        'attribute'=>'index_number',
        //'width'=>'250px',
        'label'=>"INDEX #",

    ],

    [
        'attribute'=>'names',
        //'width'=>'250px',
        'label'=>"NAME",
        //'nowrap'=>true,

    ],
    [
        'attribute' => 'programme',
        //'width'=>'250px',
        'label'=>"PROGRAMME",
        'hAlign'=>'center',
        'format' => 'raw',
        'value' => function ($model) {
            return strtoupper($model->programme);
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map($RESULTS,'programme','programme'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

    ],


    [
        'attribute'=>'year_of_study',
        //'width'=>'250px',
        'hAlign'=>'center',
        'format' => 'raw',
        'value' => function ($model) {
            return strtoupper($model->year_of_study);
        },
        'label'=>"YOS",
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => array_combine(range(1,10),range(1,10)),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],
    ],



    [
        'attribute'=>'suspension_reason',
        'width'=>'150px',
        //'hAlign'=>'right',
        'value' => function ($model) {
            return Module::SuspensionReason($model->suspension_reason,'name');
        },
        'label'=>'REMARKS',
        'format'=>'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(\backend\modules\disbursement\models\SuspensionReason::findAll(['is_active'=>'1']),'id','name'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],
    ],



    //'comment',
    /*['class' => 'yii\grid\ActionColumn',
        'template'=>'{view}{delete}',
    ],*/

    [
        'attribute' => 'status',
        //'width'=>'250px',
        'label'=>"STATUS",
        'format' => 'raw',
        'value' => function ($model) {
            return Module::SuspensionStatus($model->status);
        },
        'filter'=>false,
        /*'filterType' => GridView::FILTER_SELECT2,
       'filter' => Module::SuspensionStatus(null),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],*/
        'format' => 'raw'
    ],

    ['class' => 'kartik\grid\ActionColumn',
        'template'=>'{update}{delete}',
        'buttons' => [
            'update' => function ($url,$model) {
                return Html::a(
                    '<span class="fa fa-pencil" title="Edit"></span>',
                    Yii::$app->urlManager->createAbsoluteUrl(array('disbursement/bulk-suspension/update-staging','id'=>$model->id)));
            },
            /*   'view' => function ($url,$model,$key) {
                      return Html::a('<span class="green">View Detail</span>', $url);
             },*/
            'delete' => function ($url,$model) {
                return Html::a(
                    '<span class="fa fa-trash" title="Delete"></span>',
                    Yii::$app->urlManager->createAbsoluteUrl(array('disbursement/bulk-suspension/delete-staging','id'=>$model->id)));
            },

        ],
    ],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'columns' => $columns,
    'panel'=>['class'=>'primary']
]); ?>