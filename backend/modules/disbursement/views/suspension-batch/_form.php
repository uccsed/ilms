<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\widgets\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\SuspensionBatch */
/* @var $form yii\widgets\ActiveForm */
echo  Module::FetchBootstrap('js');
?>

<div class="suspension-batch-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <div class="rows">
        <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'title')->textInput(['class' => 'form-control','maxlength' => true]) ?>
            </div>

            <div class="form-group">
                <?= $form->field($model, 'description')->textarea(['class' => 'form-control','rows' => 3]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <div class="form-group">
                        <?php
                        echo $form->field($modelImport, 'fileImport')->widget(FileInput::classname(), [
                            //'options' => ['accept' => 'image/*'],
                            'options' => ['accept' => 'xls/*'],
                            'pluginOptions' => [
                                'showPreview' => true,
                                'showCaption' => true,
                                'showRemove' => true,
                                'showUpload' => false
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
    </div>



<div style="display: none;">
    <?= $form->field($model, 'files_uploaded')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'status_date')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'type')->textInput([ 'value' => $type]) ?>
</div>









    <div class="form-group">
            <div id="progress"></div>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-danger pull-right']) ?>
    </div>

        <script>
            $(document).ready(function () {

                $("#importBtn").on("click",function () {
                    $(this). hide();
                    $("#progress").html('<div style="font-size: large;"><span class="fa fa-3x fa-spinner fa-spin"></span> Please Wait... Uploading & Analyzing the File... </div>');
                });
            });
        </script>
    <?php ActiveForm::end(); ?>

</div>
