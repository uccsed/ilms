<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 6/13/18
 * Time: 2:34 PM
 */

use common\models\AcademicYear;
use backend\modules\allocation\models\Programme;

use backend\modules\allocation\models\LearningInstitution;
use yii\helpers\ArrayHelper;

use yii\helpers\Html;
use yii\helpers\Url;

use backend\modules\allocation\models\LoanItem;
use backend\modules\disbursement\Module;

echo Module::FetchBootstrap('fancyInputs');
echo Module::FetchBootstrap('dashboard');
?>




<br>

<form action="#" id="default_disbursement_plan_form" name="default_disbursement_plan_form" class="form-horizontal" method="POST">





<div class="row">

    <div class="col-md-2">
        <div class="form-group">
            <div class="col-lg-12">
                <?php
                $academicYears=AcademicYear::findAll(['is_current'=>'1']);
                $yearsListData=ArrayHelper::map($academicYears,'academic_year_id','academic_year');
                echo  Html::dropDownList('academic_year','',$yearsListData,array('id'=>'academic_year','class'=>'form-control','prompt'=>'Select Academic Year'));
                ?>
                <span id="academic_year_error"></span>
            </div>
        </div>
    </div>

        <div class="col-md-2">
        <div class="form-group">
            <div class="col-lg-12">
                <?php
                $Programme=Programme::find()->all();
                $programmeListData=ArrayHelper::map($Programme,'programme_id','programme_name');
                echo  Html::dropDownList('programme_id','',$programmeListData,array('id'=>'programme_id','class'=>'form-control','prompt'=>'Select Programme'));
                ?>
                <span id="programme_error"></span>
            </div>
        </div>
    </div>



    <div class="col-md-2">
        <div class="form-group">
            <div class="col-lg-12">
                <?php
                $loanItem=LoanItem::findAll(['is_active'=>'1']);
                $loanListData=ArrayHelper::map($loanItem,'loan_item_id','item_name');
                echo  Html::dropDownList('loan_item','',$loanListData,array('id'=>'loan_item','class'=>'form-control','prompt'=>'Select Loan item','onChange'=>'SetTitles()'));
                ?>
                <span id="loan_item_error"></span>
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <button type="button" class="btn btn-primary" id="cloneButton">Clone to remaining Programmes</button>
        <!--This is just an empty column (a space holder) to keep things in shape -->

    </div>

    <div class="col-md-2">
        <div class="progress pink">
            <span class="progress-left">
                <span class="progress-bar"></span>
            </span>
            <span class="progress-right">
                <span class="progress-bar"></span>
            </span>
            <div class="progress-value" id="percent_completion">0%</div>
        </div>
    </div>


    <div class="col-md-2">

        <div class="row">
            <div class="col-md-5">
                <h3 class=""><span id="total_assigned" class="count">0</h3>
            </div>
            <div class="col-md-2" style="font-size: 58px;">/</div>
            <div class="col-md-5">
                <h3 class=""><span id="total_required" class="count">0</h3>
            </div>
        </div>

    </div>


</div>

<div class="row" id="study_years">
<!-- Here is where the inputs are being appended -->
</div>



<script>


    function DazzleAmounts(fieldName,amount,unit) {
        jQuery({ someValue: 0 }).animate({ someValue: Math.floor(amount) }, {
            duration: 3000,
            easing: 'swing', // can be anything
            step: function () { // called on every step
                // Update the element's text with rounded-up value:
                jQuery(fieldName).text(commaSeparateNumber(Math.round(this.someValue))+unit);
            }
        });
    }





    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }





function PercentCompletion() {
    $.ajax({
        url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/completion'); ?>",
        type:"POST",
        // cache:false,

        data:{
            academicYear:jQuery("#academic_year").val(),
            _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
        },

        success: function (data) {
var output = jQuery.parseJSON(data);
           // jQuery("#percent_completion").html(output.percent+'%');
            DazzleAmounts('#percent_completion',output.percent,'%');
            DazzleAmounts('#total_required',output.total,'');
            DazzleAmounts('#total_assigned',output.assigned,'');

        }
    });
}


 function CloneIt(responseDiv,basicOperations){
     var programme =  jQuery("#programme_id").val();
     var loanItem =  jQuery("#loan_item").val();
     var academicYear =  jQuery("#academic_year").val();

     $.ajax({
         url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/clone') ?>",
         type:"POST",
         cache:false,
         data:{
             type:"programme",
             programme:programme,
             loanItem:loanItem,
             currentYear:academicYear,
             nextYear:academicYear,

         },
         success:function (data) {
             jQuery('#'+responseDiv).html(data.output);
             jQuery('#'+basicOperations).show()

         }
     })
    }

function AjaxPostForm(formID,postURL,responseDiv,basicOperations) {

    var percentData = [];
    var percentField = [];

    var programme =  jQuery("#programme_id").val();
    var loanItem =  jQuery("#loan_item").val();
    var academicYear =  jQuery("#academic_year").val();





var head = programme + '-' + loanItem + '-'+ academicYear +'_';

    jQuery.each(jQuery("input[type='number']"),function (index) {

        var fieldValue = jQuery(this).val()*1;
        var fieldName = jQuery(this).attr('name');


        var tail = fieldName;
        var sync = head+tail;
           percentField.push(fieldName);
           percentData.push(fieldValue);

    });


    $.ajax({
        url:postURL,
        type:"POST",
        cache:false,
        data:{
            //postData:jQuery('#'+formID).serializeArray(),
            percentField:percentField,
            percentData:percentData,

            programme:programme,
            loanItem:loanItem,
            academicYear:academicYear,



        },
        success:function (data) {
            jQuery('#'+responseDiv).html(data.output);
            jQuery('#'+basicOperations).show()

        }
    });
}


function CheckExistence() {



    var programme =  jQuery("#programme_id").val();
    var loanItem =  jQuery("#loan_item").val();
    var academicYear =  jQuery("#academic_year").val();
    var head =  programme + '-' + loanItem + '-'+ academicYear +'_';

    jQuery.each(jQuery("input[type='number']"),function (index) {
        jQuery(this).val(0);
        ColorCode(this);

    });

    $.ajax({
        url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/item'); ?>",
        type:"POST",
        cache:false,
        data:{


            programme:programme,
            loanItem:loanItem,
            academicYear:academicYear,
            type:"default"


        },
        success:function (data) {
            var output = jQuery.parseJSON(data);

            jQuery.each(output,function (index,value) {
                var sync = value.sync_id;
                var first = sync.split('_',1);
                var fieldName= sync.replace(first+'_','');

                fieldName=fieldName.replace("percent",'');
                fieldName=fieldName.replace("[",'');
                fieldName=fieldName.replace("][",'_');
                fieldName=fieldName.replace("[",'_');
                fieldName=fieldName.replace("]",'');
                fieldName=fieldName.replace("]",'');

                FieldID=fieldName;

                jQuery('#'+FieldID).val(value.disbursement_percent);
                ColorCode('#'+FieldID);
            });
        }
    })
}
    function ColorCode(item) {

        //console.log(item);
            var value = jQuery(item).val()*1;

            if(value>0){
                jQuery(item).prop('style','font-size: large; color: green;');
            }else{
                jQuery(item).prop('style','font-size: large; color: red;');
            }


        jQuery.each(jQuery("input[type='number']"),function (index) {

            var fieldValue = jQuery(this).val()*1;

           // console.log(5);
            if(fieldValue>0){
                jQuery(this).prop('style','font-size: large; color: green;');
            }else{
                jQuery(this).prop('style','font-size: large; color: red;');
            }
        });
    }

    function SetTitles() {
        jQuery.each(jQuery(".itemName"),function (index) {

            var fieldValue = jQuery('#loan_item option:selected').text();
            jQuery(this).html(fieldValue);
        });
    }




    document.addEventListener('DOMContentLoaded', function(event) {

        jQuery('#set_default').on('change click',function () {
            var isChecked = jQuery("#set_default").is(':checked');
            if(isChecked) {
                jQuery("#is_default").val(1);
                jQuery("#information").html("This plan will be set a default plan. A default plan will be used to all colleges and programmes without a customized plan.");
            } else {
                jQuery("#is_default").val(0);
                jQuery("#information").html("");

            }
        });









        jQuery('#programme_id').on('change',function ()
        {
            $.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/years'); ?>",
                type:"POST",
                // cache:false,programme

                data:{
                    programme_id:jQuery(this).val(),
                    institution_id:jQuery('#learning_institution').val(),
                    _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                },

                success: function (data) {
                   // console.log(data);
                    jQuery("#study_years").html(" "); // remove old options
                    jQuery("#study_years").append(data.output); // remove old options

                    CheckExistence();

                }
            });

        });


        jQuery('#academic_year').on('change',function () {
            CheckExistence();
            PercentCompletion();
        });

            jQuery('#loan_item').on('change',function () {
                CheckExistence();
             });

        jQuery('#submitButton').on('click',function () {


                //Validate select/primary inputs before submitting the form
            var valid = 0;
            valid+= CustomValidator('programme_id','programme_error','Programme');
            valid+= CustomValidator('','academic_year_error','Academic Year');
            valid+= CustomValidator('loan_item','loan_item_error','Loan Item');

            if (valid>=3){ //If Valid to all fields
                jQuery('#submitButton').hide();
                jQuery('#top_operators').hide();
                jQuery('#response_div').show();
                jQuery('#default_disbursement_plan_form').hide();


                var formID = "default_disbursement_plan_form";
                var postURL = "<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/default'); ?>";
                var responseDiv = "response_div";
                var basicOperations = "top_operators";
                AjaxPostForm(formID,postURL,responseDiv,basicOperations)

            }


        });


        jQuery('#cloneButton').on('click',function () {


            //Validate select/primary inputs before submitting the form
            var valid = 0;
            valid+= CustomValidator('programme_id','programme_error','Programme');
            valid+= CustomValidator('','academic_year_error','Academic Year');
            valid+= CustomValidator('loan_item','loan_item_error','Loan Item');

            if (valid>=3){ //If Valid to all fields
                jQuery('#cloneButton').hide();
                jQuery('#top_operators').hide();
                jQuery('#response_div').show();
                jQuery('#default_disbursement_plan_form').hide();


                var responseDiv = "response_div";
                var basicOperations = "top_operators";
                CloneIt(responseDiv,basicOperations);

            }


        });





    });

function CustomValidator(item,errorDiv,fieldName){
    if(jQuery('#'+item).val()==""){
        jQuery('#'+errorDiv).show();
        jQuery('#'+errorDiv).prop("style","color: red;");
        jQuery('#'+errorDiv).html(fieldName+" can NOT be empty");
        return 0;
    }else{
        jQuery('#'+errorDiv).hide();
        jQuery('#'+errorDiv).prop("style","color: black;");
        jQuery('#'+errorDiv).html("");
        return 1;
    }
}
</script>
    <button type="button" id="submitButton"  name="submitButton" class="btn btn-primary btn-warning btn-large">Register</button>
</form>

<div id="response_div" class="panel text-center col-md-12 " style="display: none;">
    <!--<img src="image/loader/loader.gif" />
    <img src="image/loader/loader1.gif" />-->
    <img src="image/loader/loader2.gif"  />
</div>