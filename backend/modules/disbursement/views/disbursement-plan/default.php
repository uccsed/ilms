<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 6/17/18
 * Time: 9:58 PM
 */



use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;



$this->title = 'Create a new Default Disbursement Plan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-batch-index">
    <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">

            <?= $this->render('_default') ?>
        </div>

    </div>
</div>