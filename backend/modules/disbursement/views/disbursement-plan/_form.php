<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 6/13/18
 * Time: 2:34 PM
 */

use common\models\AcademicYear;
use backend\modules\allocation\models\Programme;

use backend\modules\allocation\models\LearningInstitution;
use yii\helpers\ArrayHelper;

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use backend\modules\allocation\models\LoanItem;
use backend\modules\disbursement\Module;

echo Module::FetchBootstrap('fancyInputs');
//echo Module::FetchBootstrap('dashboard');
?>




<br>

<form action="#" id="disbursement_plan_form" name="disbursement_plan_form" class="form-horizontal" method="POST">





<div class="row">

    <div class="col-md-3">
        <div class="form-group">
               <div class="col-lg-12">
                <?php
                $Institution=LearningInstitution::findall(['institution_type'=>'UNIVERSITY']);
                $institutionListData=ArrayHelper::map($Institution,'learning_institution_id','institution_name');
                //echo  Html::dropDownList('learning_institution','',$institutionListData,array('id'=>'learning_institution','class'=>'form-control','prompt'=>'Select Institution'));

                echo Select2::widget([
                    'id' => 'learning_institution',
                    'name' => 'learning_institution',
                    'value' => '',
                    'data' => $institutionListData,
                    'options' => ['placeholder' => 'Select Institution']
                ]);
                ?>

                   <span id="institution_error"></span>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <div class="col-lg-12">
                <?php
                $Programme=Programme::find()->all();
                $programmeListData=ArrayHelper::map($Programme,'programme_id','programme_name');
                //echo  Html::dropDownList('programme_id','',$programmeListData,array('id'=>'programme_id','class'=>'form-control','prompt'=>'Select Programme'));
                echo Select2::widget([
                    'id' => 'programme_id',
                    'name' => 'programme_id',
                    'value' => '',
                    'data' => $programmeListData,
                    'options' => ['placeholder' => 'Select Programme']
                ]);

                ?>
                <span id="programme_error"></span>
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <div class="col-lg-12">
                <?php
                $academicYears=AcademicYear::findAll(['is_current'=>'1']);
                $yearsListData=ArrayHelper::map($academicYears,'academic_year_id','academic_year');
                //echo  Html::dropDownList('academic_year','',$yearsListData,array('id'=>'academic_year','class'=>'form-control','prompt'=>'Select Academic Year'));
                echo Select2::widget([
                    'id' => 'academic_year',
                    'name' => 'academic_year',
                    'value' => '',
                    'data' => $yearsListData,
                    'options' => ['placeholder' => 'Select Academic Year']
                ]);

                ?>
                <span id="academic_year_error"></span>
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <div class="col-lg-12">
                <?php
                $loanItem=LoanItem::findAll(['is_active'=>'1']);
                $loanListData=ArrayHelper::map($loanItem,'loan_item_id','item_name');
                //echo  Html::dropDownList('loan_item','',$loanListData,array('id'=>'loan_item','class'=>'form-control','prompt'=>'Select Loan item','onChange'=>'SetTitles()'));
                echo Select2::widget([
                    'id' => 'loan_item',
                    'name' => 'loan_item',
                    'value' => '',
                    'data' => $loanListData,
                    'options' => ['placeholder' => 'Select Loan Items']
                ]);
                ?>
                <span id="loan_item_error"></span>
            </div>
        </div>
    </div>


    <!--<div class="col-md-2">
        <div class="form-group">
            <div class="row">
                <div class="col-md-8"><label class="control-label">Make this plan Default</label></div>
                <div class="col-md-4">
                    <div class="material-switch">
                        <input id="set_default" name="set_default" type="checkbox"/>
                        <label for="set_default" class="label-success"></label>
                    </div>
                </div>
            </div>
            <input type="hidden" name="is_default" id="is_default" value="0" />
            <div id="information"></div>
        </div>
    </div>-->



    <div class="col-md-6">
        <!--This is just an empty column (a space holder) to keep things in shape -->
    </div>
</div>

<div class="row" >
    <div class="col-md-7" id="study_years">
        <!-- Here is where the inputs are being appended -->
    </div>
    <div class="col-md-5"></div>

</div>



<script>


function AjaxPostForm(formID,postURL,responseDiv,basicOperations) {

    var percentData = [];
    var percentField = [];
    var institution =  jQuery("#learning_institution").val();
    var programme =  jQuery("#programme_id").val();
    var loanItem =  jQuery("#loan_item").val();
    var academicYear =  jQuery("#academic_year").val();
    var setDefault =  0;




var head = institution + '-' + programme + '-' + loanItem + '-'+ academicYear +'_';

    jQuery.each(jQuery("input[type='number']"),function (index) {

        var fieldValue = jQuery(this).val()*1;
        var fieldName = jQuery(this).attr('name');


        var tail = fieldName;
        var sync = head+tail;
        //console.log(sync);
            percentField.push(fieldName);
           percentData.push(fieldValue);

    });


    $.ajax({
        url:postURL,
        type:"POST",
        cache:false,
        data:{
            //postData:jQuery('#'+formID).serializeArray(),
            percentField:percentField,
            percentData:percentData,
            institution:institution,
            programme:programme,
            loanItem:loanItem,
            academicYear:academicYear,
            setDefault:setDefault,


        },
        success:function (data) {
            jQuery('#'+responseDiv).html(data.output);
            jQuery('#'+basicOperations).show();
        }
    })
}


function CheckExistence() {


    var institution =  jQuery("#learning_institution").val();
    var programme =  jQuery("#programme_id").val();
    var loanItem =  jQuery("#loan_item").val();
    var academicYear =  jQuery("#academic_year").val();
    var head = institution + '-' + programme + '-' + loanItem + '-'+ academicYear +'_';

    jQuery.each(jQuery("input[type='number']"),function (index) {
        jQuery(this).val(0);
        ColorCode(this);

    });

    $.ajax({
        url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/item'); ?>",
        type:"POST",
        cache:false,
        data:{

            institution:institution,
            programme:programme,
            loanItem:loanItem,
            academicYear:academicYear,


        },
        success:function (data) {
            var output = jQuery.parseJSON(data);
            //console.log(output);
            jQuery.each(output,function (index,value) {
                var sync = value.sync_id;
                var first = sync.split('_',1);
                var fieldName= sync.replace(first+'_','');

                fieldName=fieldName.replace("percent",'');
                fieldName=fieldName.replace("[",'');
                fieldName=fieldName.replace("][",'_');
                fieldName=fieldName.replace("[",'_');
                fieldName=fieldName.replace("]",'');
                fieldName=fieldName.replace("]",'');

                FieldID=fieldName;

                jQuery('#'+FieldID).val(value.disbursement_percent);
                ColorCode('#'+FieldID);
            });
        }
    })
}
    function ColorCode(item) {

        //console.log(item);
            var value = jQuery(item).val()*1;

            if(value>0){
                jQuery(item).prop('style','font-size: large; color: green;');
            }else{
                jQuery(item).prop('style','font-size: large; color: red;');
            }


        jQuery.each(jQuery("input[type='number']"),function (index) {

            var fieldValue = jQuery(this).val()*1;

            //console.log(5);
            if(fieldValue>0){
                jQuery(this).prop('style','font-size: large; color: green;');
            }else{
                jQuery(this).prop('style','font-size: large; color: red;');
            }
        });
    }

    function SetTitles() {
        jQuery.each(jQuery(".itemName"),function (index) {

            var fieldValue = jQuery('#loan_item option:selected').text();
            jQuery(this).html(fieldValue);
        });
    }




    document.addEventListener('DOMContentLoaded', function(event) {
           //ColorCode();
            //Information();
        jQuery('#set_default').on('change click',function () {
            var isChecked = jQuery("#set_default").is(':checked');
            if(isChecked) {
                jQuery("#is_default").val(1);
                jQuery("#information").html("This plan will be set a default plan. A default plan will be used to all colleges and programmes without a customized plan.");
            } else {
                jQuery("#is_default").val(0);
                jQuery("#information").html("");

            }
        });

        jQuery('#loan_item').on('change',function () {
            SetTitles();
        });






       jQuery('#learning_institution').on('change',function () {
            $.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/programme'); ?>",
                type:"POST",
               // cache:false,

                data:{
                    institution_id:jQuery(this).val(),
                    _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                },

                success: function (data) {

                    jQuery("#programme_id").empty(); // remove old options
                    jQuery("#programme_id").append(data.output); // remove old options

                    CheckExistence();

                }
            });
        });



        jQuery('#programme_id').on('change',function () {
            $.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/years'); ?>",
                type:"POST",
                // cache:false,programme

                data:{
                    programme_id:jQuery(this).val(),
                    institution_id:jQuery('#learning_institution').val(),
                    _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                },

                success: function (data) {

                   // console.log(data);

                    jQuery("#study_years").html(" "); // remove old options
                    jQuery("#study_years").append(data.output); // remove old options

                    CheckExistence()

                }
            });
        });


        jQuery('#academic_year').on('change',function () {
            CheckExistence();
        });

            jQuery('#loan_item').on('change',function () {
                CheckExistence();
             });

        jQuery('#submitButton').on('click',function () {


                //Validate select/primary inputs before submitting the form
                var valid = 0;
            //Validate Institution Select
            valid+= CustomValidator('learning_institution','institution_error','Learning Institution');
            valid+= CustomValidator('programme_id','programme_error','Programme');
            valid+= CustomValidator('','academic_year_error','Academic Year');
            valid+= CustomValidator('loan_item','loan_item_error','Loan Item');

            if (valid>=4){ //If Valid to all fields
                jQuery('#submitButton').hide();
                jQuery('#top_operators').hide();
                jQuery('#response_div').show();
                jQuery('#disbursement_plan_form').hide();

                //var posData = jQuery('#disbursement_plan_form').serializeArray();
                var formID = "disbursement_plan_form";
                var postURL = "<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/create'); ?>";
                var responseDiv = "response_div";
                var basicOperations = "top_operators";
                AjaxPostForm(formID,postURL,responseDiv,basicOperations)
                //console.log(posData);
            }


        });
    });

function CustomValidator(item,errorDiv,fieldName){
    if(jQuery('#'+item).val()==""){
        jQuery('#'+errorDiv).show();
        jQuery('#'+errorDiv).prop("style","color: red;");
        jQuery('#'+errorDiv).html(fieldName+" can NOT be empty");
        return 0;
    }else{
        jQuery('#'+errorDiv).hide();
        jQuery('#'+errorDiv).prop("style","color: black;");
        jQuery('#'+errorDiv).html("");
        return 1;
    }
}
</script>
    <div class="pull-right">
        <button type="button" id="submitButton"  name="submitButton" class="btn btn-success btn-large">Cancel</button>
        <button type="reset" id="resetButton"  name="resetButton" class="btn btn-default btn-large">Reset</button>
        <a href="<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/create'); ?>" id="cancelButton"  name="cancelButton" class="btn btn-warning btn-large">Cancel</a>
    </div>
    </form>

<div id="response_div" class="panel text-center col-md-12 " style="display: none;">
    <!--<img src="image/loader/loader.gif" />
    <img src="image/loader/loader1.gif" />-->
    <img src="image/loader/loader2.gif"  />
</div>