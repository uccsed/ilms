<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 6/13/18
 * Time: 2:33 PM
 */
?>


<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 6/13/18
 * Time: 2:34 PM
 */

use common\models\AcademicYear;
use backend\modules\allocation\models\Programme;

use backend\modules\allocation\models\LearningInstitution;
use yii\helpers\ArrayHelper;

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use backend\modules\allocation\models\LoanItem;
use backend\modules\disbursement\Module;

echo Module::FetchBootstrap('fancyInputs');
echo Module::FetchBootstrap('dashboard');
?>




<br>

<form action="#" id="default_clone_form" name="default_clone_form" class="form-horizontal" method="POST">

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <div class="col-lg-5">
            <?php
            //echo Html::dropDownList('plan','1',array('default'=>'Default Plan','institutional'=>'Institutional Plan'),array('id'=>'plan','class'=>'form-control','prompt'=>'Select Plan Type'));
            echo Select2::widget([
                'id' => 'plan',
                'name' => 'plan',
                'value' => '',
                'data' => array('default'=>'Default Plan','institutional'=>'Institutional Plan'),
                'options' => ['placeholder' => 'Select Plan Type']
            ]);
            ?>

            </div>
            </div>
    </div>
</div>

    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                <div class="col-lg-12">
                    <?php
                    $academicYears=AcademicYear::findAll(['is_current'=>'0']);
                    $yearsListData=ArrayHelper::map($academicYears,'academic_year_id','academic_year');
                    //echo  Html::dropDownList('source_year','',$yearsListData,array('id'=>'source_year','class'=>'form-control','prompt'=>'Select Academic Year'));
                    echo Select2::widget([
                        'id' => 'source_year',
                        'name' => 'source_year',
                        'value' => '',
                        'data' => $yearsListData,
                        'options' => ['placeholder' => 'Select Academic Year']
                    ]);

                    ?>
                    <span id="source_year_error"></span>
                </div>
            </div>
        </div>


        <div class="col-md-4"></div>

        <div class="col-md-4">
            <div class="form-group">
                <div class="col-lg-12">
                    <?php
                    $academicYears=AcademicYear::findAll(['is_current'=>'1']);
                    $yearsListData=ArrayHelper::map($academicYears,'academic_year_id','academic_year');
                    //echo  Html::dropDownList('destination_year','',$yearsListData,array('id'=>'destination_year','class'=>'form-control','prompt'=>'Select Academic Year'));
                    echo Select2::widget([
                        'id' => 'destination_year',
                        'name' => 'destination_year',
                        'value' => '',
                        'data' => $yearsListData,
                        'options' => ['placeholder' => 'Select Academic Year']
                    ]);
                    ?>
                    <span id="destination_year_error"></span>
                </div>
            </div>
        </div>







    </div>

    <div class="row" id="study_years">
        <!-- Here is where the inputs are being appended -->

        <div class="col-md-4">
            <div class="row">
                <h3 class="text-center text-uppercase">Source</h3>
                <div class="col-md-6">
                    <div class="progress blue">
                    <span class="progress-left">
                        <span class="progress-bar"></span>
                    </span>
                        <span class="progress-right">
                        <span class="progress-bar"></span>
                    </span>
                        <div class="progress-value" id="percent_completion_source">0%</div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-5">
                            <h3 class=""><span id="total_assigned_source" class="count">0</h3>
                        </div>
                        <div class="col-md-2" style="font-size: 58px;">/</div>
                        <div class="col-md-5">
                            <h3 class=""><span id="total_required_source" class="count">0</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4"></div>

        <div class="col-md-4">
            <div class="row">
                <h3 class="text-center text-uppercase">Destination</h3>
                <div class="col-md-6">
                    <div class="progress yellow">
                    <span class="progress-left">
                        <span class="progress-bar"></span>
                    </span>
                        <span class="progress-right">
                        <span class="progress-bar"></span>
                    </span>
                        <div class="progress-value" id="percent_completion_destination">0%</div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-5">
                            <h3 class=""><span id="total_assigned_destination" class="count">0</h3>
                        </div>
                        <div class="col-md-2" style="font-size: 58px;">/</div>
                        <div class="col-md-5">
                            <h3 class=""><span id="total_required_destination" class="count">0</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>




    </div>



    <script>


        function DazzleAmounts(fieldName,amount,unit) {
            jQuery({ someValue: 0 }).animate({ someValue: Math.floor(amount) }, {
                duration: 3000,
                easing: 'swing', // can be anything
                step: function () { // called on every step
                    // Update the element's text with rounded-up value:
                    jQuery(fieldName).text(commaSeparateNumber(Math.round(this.someValue))+unit);
                }
            });
        }





        function commaSeparateNumber(val) {
            while (/(\d+)(\d{3})/.test(val.toString())) {
                val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            }
            return val;
        }





        function PercentCompletion(item,destination) {
            $.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/completion'); ?>",
                type:"POST",
                // cache:false,

                data:{
                    academicYear:jQuery("#"+item).val(),
                    _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                },

                success: function (data) {
                    var output = jQuery.parseJSON(data);
                    //jQuery("#percent_completion_"+destination).html(output.percent+'%');
                    DazzleAmounts("#percent_completion_"+destination,output.percent,'%');
                    DazzleAmounts('#total_required_'+destination,output.total,'');
                    DazzleAmounts('#total_assigned_'+destination,output.assigned,'');
                    // output.percent-40,
                    jQuery("#cricleinput").val(67);

                }
            });
        }


        function CloneIt(responseDiv,basicOperations){

            var sourceYear =  jQuery("#source_year").val();
            var destinationYear =  jQuery("#destination_year").val();
            var type =  jQuery("#plan").val();

            $.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/clone') ?>",
                type:"POST",
                cache:false,
                data:{
                    type:type,
                    currentYear:sourceYear,
                    nextYear:destinationYear,

                },
                success:function (data) {
                    jQuery('#'+responseDiv).html(data.output);
                    jQuery('#'+basicOperations).show()

                }
            })
        }




        function ColorCode(item) {

            //console.log(item);
            var value = jQuery(item).val()*1;

            if(value>0){
                jQuery(item).prop('style','font-size: large; color: green;');
            }else{
                jQuery(item).prop('style','font-size: large; color: red;');
            }


            jQuery.each(jQuery("input[type='number']"),function (index) {

                var fieldValue = jQuery(this).val()*1;

                // console.log(5);
                if(fieldValue>0){
                    jQuery(this).prop('style','font-size: large; color: green;');
                }else{
                    jQuery(this).prop('style','font-size: large; color: red;');
                }
            });
        }

        function SetTitles() {
            jQuery.each(jQuery(".itemName"),function (index) {

                var fieldValue = jQuery('#loan_item option:selected').text();
                jQuery(this).html(fieldValue);
            });
        }




        document.addEventListener('DOMContentLoaded', function(event) {

            jQuery('#set_default').on('change click',function () {
                var isChecked = jQuery("#set_default").is(':checked');
                if(isChecked) {
                    jQuery("#is_default").val(1);
                    jQuery("#information").html("This plan will be set a default plan. A default plan will be used to all colleges and programmes without a customized plan.");
                } else {
                    jQuery("#is_default").val(0);
                    jQuery("#information").html("");

                }
            });










            jQuery('#source_year').on('change',function () {

                var label = jQuery('#source_year option:selected').text();
                jQuery("#source_name").html(label);
                var  plan = jQuery("#plan").val();
                //alert(plan);
                if (plan=="default"){
                    PercentCompletion('source_year','source');
                }

            });

            jQuery('#destination_year').on('change',function () {
                var label = jQuery('#destination_year option:selected').text();
                jQuery("#destination_name").html(" to "+ label);
                var  plan = jQuery("#plan").val();
                if (plan=="default"){
                    PercentCompletion('destination_year','destination');
                }
            });





            jQuery('#cloneButton').on('click',function () {


                //Validate select/primary inputs before submitting the form
                var valid = 0;

                valid+= CustomValidator('plan','plan_error','Plan Type');
                valid+= CustomValidator('source_year','source_year_error','Source Year');
                valid+= CustomValidator('destination_year','destination_year_error','Destination Year');

                if (valid>=3){ //If Valid to all fields
                    jQuery('#cloneButton').hide();
                    jQuery('#top_operators').hide();
                    jQuery('#response_div').show();
                    jQuery('#default_clone_form').hide();


                    var responseDiv = "response_div";
                    var basicOperations = "top_operators";
                    CloneIt(responseDiv,basicOperations);

                }


            });





        });

        function CustomValidator(item,errorDiv,fieldName){
            if(jQuery('#'+item).val()==""){
                jQuery('#'+errorDiv).show();
                jQuery('#'+errorDiv).prop("style","color: red;");
                jQuery('#'+errorDiv).html(fieldName+" can NOT be empty");
                return 0;
            }else{
                jQuery('#'+errorDiv).hide();
                jQuery('#'+errorDiv).prop("style","color: black;");
                jQuery('#'+errorDiv).html("");

                if(jQuery("#source_year").val()==jQuery("#destination_year").val()){

                    jQuery('#source_year_error').show();
                    jQuery('#destination_year_error').show();

                    jQuery('#source_year_error').prop("style","color: red;");
                    jQuery('#destination_year_error').prop("style","color: red;");

                    jQuery('#source_year_error').html("Source can NOT be equal to Destination");
                    jQuery('#destination_year_error').html("Destination can NOT be equal to Source");

                    return 0;
                }else{

                    if(jQuery("#source_year").val()>jQuery("#destination_year").val()){
                        jQuery('#source_year_error').show();
                        jQuery('#destination_year_error').show();

                        jQuery('#source_year_error').prop("style","color: red;");
                        jQuery('#destination_year_error').prop("style","color: red;");

                        jQuery('#source_year_error').html("You can NOT clone backward");
                        jQuery('#destination_year_error').html("You can NOT clone backward");
                    }else {
                        jQuery('#source_year_error').hide();
                        jQuery('#destination_year_error').hide();

                        jQuery('#source_year_error').prop("style", "color: black;");
                        jQuery('#destination_year_error').prop("style", "color: black;");

                        jQuery('#source_year_error').html("");
                        jQuery('#destination_year_error').html("");

                        return 1;
                    }
                }

            }
        }
    </script>
    <div class="pull-right">
        <!--<button type="button" id="submitButton"  name="submitButton" class="btn btn-primary btn-warning btn-large">Register</button>-->
        <button type="button" class="btn btn-success" id="cloneButton">Clone <span id="source_name"></span>  <span id="destination_name"></span></button>
        <button type="reset" class="btn btn-default" id="resetButton">Reset</button>
        <a href="<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/clone'); ?>" class="btn btn-warning" id="cancelButton">Cancel</a>
    </div>

</form>

<div id="response_div" class="panel text-center col-md-12 " style="display: none;">
    <!--<img src="image/loader/loader.gif" />
    <img src="image/loader/loader1.gif" />-->
    <img src="image/loader/loader2.gif"  />
</div>
