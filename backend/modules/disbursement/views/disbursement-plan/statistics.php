<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 6/13/18
 * Time: 2:31 PM
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'Disbursement Plans Statistical Analysis';
$this->params['breadcrumbs'][] = $this->title;
use backend\modules\disbursement\Module;
echo Module::FetchBootstrap('dashboard');
?>
<div class="disbursement-batch-index">
    <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right" id="top_operators">
                        <div class="row">
                            <div class="col-md-3"><a href="<?php echo url::to(['disbursement-plan/default']); ?>" class="btn btn-primary pull-left"><i class="icon-plus"></i> Default Plan</a></div>
                            <div class="col-md-3"><a href="<?php echo url::to(['disbursement-plan/create']); ?>" class="btn btn-primary pull-left"><i class="icon-plus"></i> Institutional Plan</a></div>
                            <div class="col-md-3"><a href="<?php echo url::to(['disbursement-plan/clone']); ?>" class="btn btn-success pull-left"><i class="icon-plus"></i> Clone from Existing</a></div>
                            <div class="col-md-3"><a href="<?php echo url::to(['disbursement-plan/statistics']); ?>" class="btn btn-warning pull-right"><i class="icon-cogs spin"></i> Preview Statistics</a></div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
<div class="row">

    <!--Start Panel 1-->
    <div class="col-xs-6 col-sm-3 ">
        <div class="panel">
            <div class="panel-heading"><h4 class="text-center">DIPLOMA</h4></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="progress blue">
                            <span class="progress-left">
                                <span class="progress-bar"></span>
                            </span>
                            <span class="progress-right">
                                <span class="progress-bar"></span>
                            </span>
                            <div class="progress-value"><?php echo $one=rand(0,100); ?>%</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="list-group">
                            <a href="#" class="list-group-item visitor">
                                <h3 class="pull-right"><i class="fa fa-users"></i></h3>
                                <h4 class="list-group-item-heading"><span id="lonee1" class="count" title="<?php $lonee=Module::ShortNumber(rand(0,229437),0,'array'); echo $lonee[0]; ?>"></span><?php echo $lonee[1];  ?></h4>
                                <p class="list-group-item-text">Lonee</p>
                            </a>
                            <a href="#" class="list-group-item facebook-like">
                                <h3 class="pull-right"><i class="fa fa-money"></i></h3>
                                <h4 class="list-group-item-heading"><span id="amount1"  class="count" title="<?php $amount=Module::ShortNumber(rand(0,50000000000000),2,'array'); echo $amount[0]; ?>"></span><?php echo $amount[1];  ?></h4>
                                <p class="list-group-item-text">Amount</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Panel 1-->


    <!--Start Panel 2-->
    <div class="col-xs-6 col-sm-3 ">
        <div class="panel">
            <div class="panel-heading"><h4 class="text-center">BACHELOR</h4></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="progress yellow">
                            <span class="progress-left">
                                <span class="progress-bar"></span>
                            </span>
                            <span class="progress-right">
                                <span class="progress-bar"></span>
                            </span>
                            <div class="progress-value"><?php echo $two=rand(0,100-$one); ?>%</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="list-group">
                            <a href="#" class="list-group-item visitor">
                                <h3 class="pull-right"><i class="fa fa-users"></i></h3>
                                <h4 class="list-group-item-heading"><span id="lonee2" class="count" title="<?php $lonee=Module::ShortNumber(rand(0,229437),0,'array'); echo $lonee[0]; ?>"></span><?php echo $lonee[1];  ?></h4>
                                <p class="list-group-item-text">Lonee</p>
                            </a>
                            <a href="#" class="list-group-item facebook-like">
                                <h3 class="pull-right"><i class="fa fa-money"></i></h3>
                                <h4 class="list-group-item-heading"><span id="amount2"  class="count" title="<?php $amount=Module::ShortNumber(rand(0,50000000000000),2,'array'); echo $amount[0]; ?>"></span><?php echo $amount[1];  ?></h4>
                                <p class="list-group-item-text">Amount</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Panel 2-->


    <!--Start Panel 3-->
    <div class="col-xs-6 col-sm-3 ">
        <div class="panel">
            <div class="panel-heading"><h4 class="text-center">MASTERS</h4></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="progress green">
                            <span class="progress-left">
                                <span class="progress-bar"></span>
                            </span>
                            <span class="progress-right">
                                <span class="progress-bar"></span>
                            </span>
                            <div class="progress-value"><?php echo $three=rand(0,100-($one+$two)); ?>%</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="list-group">
                            <a href="#" class="list-group-item visitor">
                                <h3 class="pull-right"><i class="fa fa-users"></i></h3>
                                <h4 class="list-group-item-heading"><span id="lonee3" class="count" title="<?php $lonee=Module::ShortNumber(rand(0,229437),0,'array'); echo $lonee[0]; ?>"></span><?php echo $lonee[1];  ?></h4>
                                <p class="list-group-item-text">Lonee</p>
                            </a>
                            <a href="#" class="list-group-item facebook-like">
                                <h3 class="pull-right"><i class="fa fa-money"></i></h3>
                                <h4 class="list-group-item-heading"><span id="amount3"  class="count" title="<?php $amount=Module::ShortNumber(rand(0,50000000000000),2,'array'); echo $amount[0]; ?>"></span><?php echo $amount[1];  ?></h4>
                                <p class="list-group-item-text">Amount</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Panel 3-->


    <!--Start Panel 4-->
    <div class="col-xs-6 col-sm-3 ">
        <div class="panel">
            <div class="panel-heading"><h4 class="text-center">PhD</h4></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="progress pink">
                            <span class="progress-left">
                                <span class="progress-bar"></span>
                            </span>
                            <span class="progress-right">
                                <span class="progress-bar"></span>
                            </span>
                            <div class="progress-value"><?php echo $four=100-($one+$two+$three); ?>%</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="list-group">
                            <a href="#" class="list-group-item visitor">
                                <h3 class="pull-right"><i class="fa fa-users"></i></h3>
                                <h4 class="list-group-item-heading"><span id="lonee4" class="count" title="<?php $lonee=Module::ShortNumber(rand(0,229437),0,'array'); echo $lonee[0]; ?>"></span><?php echo $lonee[1];  ?></h4>
                                <p class="list-group-item-text">Lonee</p>
                            </a>
                            <a href="#" class="list-group-item facebook-like">
                                <h3 class="pull-right"><i class="fa fa-money"></i></h3>
                                <h4 class="list-group-item-heading"><span id="amount4"  class="count" title="<?php $amount=Module::ShortNumber(rand(0,50000000000000),2,'array'); echo $amount[0]; ?>"></span><?php echo $amount[1];  ?></h4>
                                <p class="list-group-item-text">Amount</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Panel 4-->

</div>




<!-- USERS MINI PANELS
<div class="row">
    <div class="col-lg-2 col-sm-6">
        <div class="circle-tile ">
            <a href="#"><div class="circle-tile-heading dark-blue"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
            <div class="circle-tile-content dark-blue">
                <div class="circle-tile-description text-faded"> Users</div>
                <div class="circle-tile-number text-faded ">265</div>
                <a class="circle-tile-footer" href="#">More Info<i class="fa fa-chevron-circle-right"></i></a>
            </div>
        </div>
    </div>

    <div class="col-lg-2 col-sm-6">
        <div class="circle-tile ">
            <a href="#"><div class="circle-tile-heading red"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
            <div class="circle-tile-content red">
                <div class="circle-tile-description text-faded"> Users Online </div>
                <div class="circle-tile-number text-faded ">10</div>
                <a class="circle-tile-footer" href="#">More Info<i class="fa fa-chevron-circle-right"></i></a>
            </div>
        </div>
    </div>
</div>-->



<script>

function DazzleAmounts(fieldName,amount) {
    jQuery({ someValue: 0 }).animate({ someValue: Math.floor(amount) }, {
        duration: 3000,
        easing: 'swing', // can be anything
        step: function () { // called on every step
            // Update the element's text with rounded-up value:
            jQuery(fieldName).text(commaSeparateNumber(Math.round(this.someValue)));
        }
    });
}

    function AnimateAmount() {
            for (var i=0; i<=4; i++){
               var loan = jQuery('#lonee'+i).attr('title');
               var amount = jQuery('#amount'+i).attr('title');

                DazzleAmounts('#lonee'+i,loan);
                DazzleAmounts('#amount'+i,amount);
            }

    }




    jQuery(document).ready(function () {
        AnimateAmount();
    });
    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }
</script>


