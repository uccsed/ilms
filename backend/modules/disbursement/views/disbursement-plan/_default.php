<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 6/13/18
 * Time: 2:34 PM
 */

use common\models\AcademicYear;
use backend\modules\allocation\models\Programme;

use backend\modules\allocation\models\LearningInstitution;
use yii\helpers\ArrayHelper;

use yii\helpers\Html;
use yii\helpers\Url;

use kartik\select2\Select2;
use backend\modules\allocation\models\LoanItem;
use backend\modules\disbursement\Module;
use kartik\depdrop\DepDrop;
echo Module::FetchBootstrap('fancyInputs');
echo Module::FetchBootstrap('dashboard');

$semesterSQL="SELECT semester.semester_id AS 'id', semester.semester_number AS 'number', semester.description AS 'description' FROM semester WHERE is_active='1' ORDER  BY semester_number ASC; ";
$semesterModel = Yii::$app->db->createCommand($semesterSQL)->queryAll();

$installmentSQL="SELECT instalment_definition.instalment_definition_id AS 'id', instalment_definition.instalment AS 'number', instalment_definition.instalment_desc AS 'description', instalment_definition.semester_id AS 'semester_id' FROM instalment_definition WHERE is_active='1' ORDER  BY instalment_definition.instalment ASC; ";
$installmentModel = Yii::$app->db->createCommand($installmentSQL)->queryAll();
?>




<br>

<form action="#" id="default_disbursement_plan_form" name="default_disbursement_plan_form" class="form-horizontal" method="POST">





<div class="row">
    <div class="col-md-7">


        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <div class="col-lg-12">
                        <?php
                        $academicYears=AcademicYear::findAll(['is_current'=>'1']);
                        $yearsListData=ArrayHelper::map($academicYears,'academic_year_id','academic_year');
                        // echo  Html::dropDownList('academic_year','',$yearsListData,array('id'=>'academic_year','class'=>'select2','prompt'=>'Select Academic Year'));
                        ?>
                        <?php
                        // Multiple select without model
                        echo Select2::widget([
                            'id' => 'academic_year',
                            'name' => 'academic_year',
                            'value' => '',
                            'data' => $yearsListData,
                            'options' => ['placeholder' => 'Select Academic Year']
                        ]);
                        ?>
                        <span id="academic_year_error"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="form-group">
                    <div class="col-lg-12">
                        <?php
                        $loanItem=LoanItem::findAll(['is_active'=>'1']);
                        $loanListData=ArrayHelper::map($loanItem,'loan_item_id','item_name');
                        // echo  Html::dropDownList('loan_item','',$loanListData,array('id'=>'loan_item','class'=>'form-control','prompt'=>'Select Loan item','onChange'=>'SetTitles()'));
                        ?>
                        <?php /*echo DepDrop::widget([
                                'name' => 'city',
                                'options' => ['id'=>'loan_item','multiple'=>true, 'placeholder' => 'Select Loan Items'],
                                'pluginOptions' => [
                                   'depends'  => ['academic_year'],
                                   'placeholder' => 'select ...',
                                   'url' => Url::to(['/disbursement/disbursement-plan/remaining-items'])
                                ]
                            ]);*/  ?>
                        <?php
                        echo Select2::widget([
                            'id' => 'loan_item',
                            'name' => 'loan_item',
                            'value' => '',
                            'data' => $loanListData,
                            'options' => ['multiple'=>true, 'placeholder' => 'Select Loan Items']
                        ]);


                        ?>
                        <span id="loan_item_error"></span>
                    </div>
                </div>
            </div>
        </div>






<hr />

        <div class="form-group">
            <table class="table table-bordered">
                <thead>
                <tr class="bg-blue-gradient">
                    <th class="bg-blue-gradient"></th>
                    <?php foreach ($installmentModel as $inst=>$instDataArray){   ?>
                        <th class="text-center"><?php echo Module::THNumber($instDataArray['number']); ?> QUARTER</th>
                    <?php }  ?>
                    <th class="text-center bg-blue-gradient" width="100px">TOTAL (%)</th>
                </tr>
                </thead>
                <tbody>
                    <?php //foreach ($semesterModel as $smstr=>$smstrDataArray){   ?>
                    <tr>
                        <th class="text-nowrap  bg-blue-gradient"><?php //echo strtoupper($smstrDataArray['description']); ?></th>
                        <?php foreach ($installmentModel as $inst=>$instDataArray){ $id=$instDataArray['semester_id'].'-'.$instDataArray['id']   ?>
                            <td>

                                    <div class="input-group">
                                        <input type="number" onchange="ColorCode(this)" title="total_<?php echo $instDataArray['semester_id']; ?>" value="0" class="text-right form-control percent total_<?php echo $instDataArray['semester_id']; ?> semester<?php echo $instDataArray['semester_id']; ?>" min="0" max="100" id="<?php echo $id; ?>" name="<?php echo $id; ?>[]" style="font-size: large; color: red; text-align: right;">
                                        <div class="input-group-addon bg-blue text-white"><span class="text-bold text-white" style="font-size: 18px;">%</span></div>
                                    </div>

                            </td>
                        <?php }  ?>
                        <!--<th class="bg-orange text-right text-bold" id="total_<?php /*echo $instDataArray['semester_id']; */?>" style="font-size: large;">0</th>-->
                        <th class="bg-blue-gradient text-right text-bold" id="grandTotal" style="font-size: large;">0</th>
                    </tr>
                    <?php //}  ?>
                </tbody>

                <tfoot>

                    <tr>
                        <th class="text-nowrap  bg-blue-gradient text-right"></th>
                        <?php foreach ($installmentModel as $inst=>$instDataArray){ $id=$instDataArray['semester_id'].'_'.$instDataArray['id']   ?>
                            <td class="bg-blue-gradient"></td>
                        <?php }  ?>
                        <th class="bg-blue-gradient text-right text-bold" style="font-size: large;"></th>
                    </tr>

                </tfoot>
            </table>
        </div>
        <div class="form-group">
            <h5 class="text-bold">Remaining Items</h5>
            <ol type="1" id="remaining_items">

            </ol>
        </div>

    </div>

    <div class="col-md-5">

    </div>









    <div class="col-md-2">
        <div class="progress pink">
            <span class="progress-left">
                <span class="progress-bar"></span>
            </span>
            <span class="progress-right">
                <span class="progress-bar"></span>
            </span>
            <div class="progress-value" id="percent_completion">0%</div>
        </div>
    </div>


    <div class="col-md-2">

        <div class="row">
            <div class="col-md-5">
                <h3 class=""><span id="total_assigned" class="count">0</h3>
            </div>
            <div class="col-md-2" style="font-size: 58px;">/</div>
            <div class="col-md-5">
                <h3 class=""><span id="total_required" class="count">0</h3>
            </div>
        </div>

    </div>


</div>

<div class="row" id="study_years">
<!-- Here is where the inputs are being appended -->
</div>



<script>


    function UpdateTotalPercentage(item){

        var title = jQuery(item).attr('title');
        var total = 0;
        var negativeClear = 1;

        jQuery.each(jQuery("."+title),function () {
            var percent = (jQuery(this).val() * 1);
            total += percent;
            jQuery("#" + title).html(commaSeparateNumber(total));
        });

        var GrandPercent = 0;
            jQuery.each(jQuery(".percent"),function () {
                GrandPercent+=(jQuery(this).val() * 1);
                var currentPercentage = jQuery(this).val() * 1;
                negativeClear*=currentPercentage;
            jQuery("#grandTotal").html(commaSeparateNumber(GrandPercent));

                if(GrandPercent!==100){
                    jQuery("#grandTotal").prop('class','bg-red text-right text-bold;');
                    jQuery("#submitButton").hide();

                }else{
                    jQuery("#grandTotal").prop('class','bg-green text-right text-bold;');
                    if (negativeClear >= 0){
                        jQuery("#submitButton").show();
                    }else{
                        alert('Please makes sure you have no negative percentage');
                    }

                }

        });
    }


    function DazzleAmounts(fieldName,amount,unit) {
        jQuery({ someValue: 0 }).animate({ someValue: Math.floor(amount) }, {
            duration: 3000,
            easing: 'swing', // can be anything
            step: function () { // called on every step
                // Update the element's text with rounded-up value:
                jQuery(fieldName).text(commaSeparateNumber(Math.round(this.someValue))+unit);
            }
        });
    }





    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }





function PercentCompletion() {
    $.ajax({
        url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/completion'); ?>",
        type:"POST",
        // cache:false,

        data:{
            academicYear:jQuery("#academic_year").val(),
            _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
        },

        success: function (data) {
var output = jQuery.parseJSON(data);
           // jQuery("#percent_completion").html(output.percent+'%');
            DazzleAmounts('#percent_completion',output.percent,'%');
            DazzleAmounts('#total_required',output.total,'');
            DazzleAmounts('#total_assigned',output.assigned,'');

        }
    });
}

    function RemainingItems() {
        $("#remaining_items").html('');
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/remaining-items'); ?>",
            type:"POST",
            // cache:false,

            data:{
                academicYear:jQuery("#academic_year").val(),
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
            },

            success: function (data) {
                var output = jQuery.parseJSON(data);
              // console.log(output);
                $("#remaining_items").html('');

                $.each(output,function (index, value) {
                    jQuery("#remaining_items").append('<li>'+value.name+'</li>');
                    //console.log(value);
               });


            }
        });
    }




function AjaxPostForm(formID,postURL,responseDiv,basicOperations) {

    var percentData = [];
    var percentField = [];


    var academicYear =  jQuery("#academic_year").val();
    var loanItem =  jQuery("#loan_item").val();






    //console.log(loanItem);
    jQuery.each(jQuery("input[type='number']"),function (index) {

        var fieldValue = jQuery(this).val()*1;
        var fieldID = jQuery(this).attr('id');

           percentField.push(fieldID);
           percentData.push(fieldValue);


    });



    $.ajax({
        url:postURL,
        type:"POST",
        cache:false,
        data:{

            percentField:percentField,
            percentData:percentData,
            loanItem:loanItem,
            academicYear:academicYear,

        },
        success:function (data) {
            var link1 = '<?php echo Yii::$app->urlManager->createUrl("/disbursement/disbursement-plan/create") ?>';
            var link2 = '<?php echo Yii::$app->urlManager->createUrl("/disbursement/disbursement-plan/index") ?>';
            jQuery('#'+responseDiv).html("");
            jQuery('#'+responseDiv).append(data.output);
            jQuery('#'+responseDiv).append(
                '<div class="">' +
                '<a href="'+link1+'"  class="btn btn-success btn-large">Back To Create</a>&nbsp;&nbsp;&nbsp;' +
                '<a href="'+link2+'"  class="btn btn-primary btn-large">Show List</a>' +
                '</div>');

            jQuery('#'+basicOperations).show()

        }
    });
}



    function ColorCode(item) {

        //console.log(item);
            var value = jQuery(item).val()*1;

            if(value>0){
                jQuery(item).prop('style','font-size: large; color: green;');
            }else{
                jQuery(item).prop('style','font-size: large; color: red;');
            }


        jQuery.each(jQuery("input[type='number']"),function (index) {

            var fieldValue = jQuery(this).val()*1;

           // console.log(5);
            if(fieldValue>0){
                jQuery(this).prop('style','font-size: large; color: green;');
            }else{
                jQuery(this).prop('style','font-size: large; color: red;');
            }
        });
    }





    document.addEventListener('DOMContentLoaded', function(event) {
       // UpdateTotalPercentage();
        jQuery("#submitButton").hide();
        jQuery(".percent").on("change keyup",function () {
                UpdateTotalPercentage(this);
            });















        jQuery('#academic_year').on('change',function () {
           // CheckExistence();
            PercentCompletion();
            RemainingItems();
        });

            jQuery('#loan_item').on('change',function () {
                //CheckExistence();
             });

        jQuery('#submitButton').on('click',function () {


                //Validate select/primary inputs before submitting the form
            var valid = 0;
            //valid+= CustomValidator('programme_id','programme_error','Programme');
            valid+= CustomValidator('academic_year','academic_year_error','Academic Year');
          //  valid+= CustomValidator('loan_item','loan_item_error','Loan Item');

            if (valid>=1){ //If Valid to all fields
                jQuery('#submitButton').hide();
                jQuery('#top_operators').hide();
                jQuery('#response_div').show();
                jQuery('#default_disbursement_plan_form').hide();


                var formID = "default_disbursement_plan_form";
                var postURL = "<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/create'); ?>";
                var responseDiv = "response_div";
                var basicOperations = "top_operators";
                AjaxPostForm(formID,postURL,responseDiv,basicOperations)

            }


        });








    });

function CustomValidator(item,errorDiv,fieldName){
  /*if (item =='loan_item'){
      var loanItem =  jQuery("#loan_item").val();

      console.log(loanItem);
      if (loanItem!=='' && loanItem.length!==0){
          var value = loanItem.length;
      }else{
          var value = '';
      }

  }else{*/
      var value = jQuery('#'+item).val();
  //}

    if(value==""){
        jQuery('#'+errorDiv).show();
        jQuery('#'+errorDiv).prop("style","color: red;");
        jQuery('#'+errorDiv).html(fieldName+" can NOT be empty");
        return 0;
    }else{
        jQuery('#'+errorDiv).hide();
        jQuery('#'+errorDiv).prop("style","color: black;");
        jQuery('#'+errorDiv).html("");
        return 1;
    }


}
</script>

    <div class="pull-right">
        <button type="button" id="submitButton"  name="submitButton" class="btn btn-success btn-large">Create</button>
        <button type="reset" id="resetButton"  name="resetButton" class="btn btn-default btn-large">Reset</button>
        <a href="<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/create') ?>" id="cancelButton"  name="cancelButton" class="btn btn-warning btn-large">Cancel</a>
    </div>

</form>

<div id="response_div" class="panel text-center col-md-12 " style="display: none;">
    <!--<img src="image/loader/loader.gif" />
    <img src="image/loader/loader1.gif" />-->
    <img src="image/loader/loader2.gif"  />

</div>