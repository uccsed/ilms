<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 6/13/18
 * Time: 12:32 PM
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DatePicker;

$this->title = 'List of Default Disbursement Plans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-batch-index">
    <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">

            <div class="row">
                <div class="pull-left" id="top_operators">
                    <div class="row pull-right">
                        <!-- <div class="col-md-4"><a href="<?php /*echo url::to(['institutional-plan/default']); */?>" class="btn btn-primary pull-left"><i class="icon-plus"></i> Default Plan</a></div>
                    --><div class="col-md-4"><a href="<?php echo url::to(['disbursement-plan/create']); ?>" class="btn btn-success"> Create Plan</a></div>
                        <!--<div class="col-md-4"><a href="<?php /*echo url::to(['institutional-plan/clone']); */?>" class="btn btn-warning pull-left"><i class="icon-plus"></i> Clone from Existing</a></div>
               --> </div>
                </div>
            </div>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'academic_year',
                        'label'=>"Academic Year",
                        'format' => 'raw',
                        //'value' =>'Institution',
                        'value' => function ($model) {
                            return $model->AcademicYear;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                        'filter' =>ArrayHelper::map(\common\models\AcademicYear::findBySql('SELECT * FROM academic_year  ORDER BY is_current DESC')->asArray()->all(), 'academic_year_id', 'academic_year'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],




                    [
                        'attribute' => 'semester_number',
                        'label'=>"Semester",
                        'format' => 'raw',
                        //'value' =>'Institution',
                        'value' => function ($model) {
                            return $model->Semester;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                        'filter' =>ArrayHelper::map(\common\models\Semester::findBySql('SELECT * FROM semester WHERE  is_active="1" ORDER BY semester_number ASC')->asArray()->all(), 'semester_id', 'semester_number'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],



                    [
                        'attribute' => 'instalment_id',
                        'label'=>"Instalment",
                        'format' => 'raw',
                        //'value' =>'Institution',
                        'value' => function ($model) {
                            return $model->Instalment;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                        'filter' =>ArrayHelper::map(\backend\modules\disbursement\models\Instalment::findBySql('SELECT * FROM instalment_definition WHERE is_active="1" ORDER BY instalment ASC')->asArray()->all(), 'instalment_definition_id', 'instalment'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],

                    [
                        'attribute' => 'loan_item_id',
                        'label'=>"Loan Item",
                        'format' => 'raw',
                        //'value' =>'Institution',
                        'value' => function ($model) {
                            return $model->LoanItem;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                        'filter' =>ArrayHelper::map(\backend\modules\allocation\models\LoanItem::findBySql('SELECT loan_item_id AS "id", CONCAT(loan_item.item_name," (",loan_item.item_code,")") AS "Name" FROM loan_item ORDER BY item_name ASC')->asArray()->all(), 'id', 'Name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],





                    [
                        'attribute'=>'disbursement_percent',
                        'width'=>'150px',
                        'hAlign'=>'right',
                        'label'=>"Percent (%)",
                        'format'=>['decimal', 0],
                        'pageSummary'=>true
                    ],



                    //'comment',
                    ['class' => 'yii\grid\ActionColumn',
                        'template'=>'{view}{update}{delete}',
                    ],
                ],
            ]); ?>
        </div>


    </div>
</div>