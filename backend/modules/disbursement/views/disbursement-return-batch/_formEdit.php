<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\widgets\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementReturnBatch */
/* @var $form yii\widgets\ActiveForm */
echo  Module::FetchBootstrap('js');
$user=Yii::$app->user->id;
$iSQL="SELECT learning_institution_id AS 'id', institution_name AS 'name',institution_code AS 'code', CONCAT(IFNULL(institution_name,''),' - (',IFNULL(institution_code,''),')') AS 'remix' FROM learning_institution WHERE institution_type = 'UNIVERSITY' AND learning_institution_id IN (SELECT institution_assignment.institution_id FROM institution_assignment WHERE institution_assignment.officer_id='$user')";
$InstitutionModel = Yii::$app->db->createCommand($iSQL)->queryAll();
?>

<div class="disbursement-return-batch-form">
    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
<div class="row">
    <div class="col-md-6">
        <div class="panel">
            <div class="panel-heading"></div>
            <div class="panel-body">
                <div class="form-group" style="display: none;">
                    <?php
                    echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' =>1,
                        'attributes' => [


                            'institution_id' => ['type' => Form::INPUT_WIDGET,
                                'widgetClass' => \kartik\select2\Select2::className(),
                                //  'label' => 'Payment Request',
                                'label' => 'Learning Institution',

                                'options' => [
                                    'data' => ArrayHelper::map($InstitutionModel,'id','remix'),
                                    'options' => [
                                        'prompt'=>'Select Learning Institution',


                                    ],
                                ],
                            ],



                            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                        ]
                    ]);
                    ?>
                </div>

                <div class="form-group" style="display: none;">
                    <?php
                    echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' =>1,
                        'attributes' => [


                            'academic_year' => ['type' => Form::INPUT_WIDGET,
                                'widgetClass' => \kartik\select2\Select2::className(),
                                //  'label' => 'Payment Request',
                                'label' => 'Academic Year',

                                'options' => [
                                    'data' => ArrayHelper::map(\common\models\AcademicYear::findBySql("SELECT * FROM academic_year ORDER BY is_current DESC, academic_year DESC")->asArray()->all(),'academic_year_id','academic_year'),
                                    'options' => [
                                        'prompt' => 'Select Academic Year',
                                    ],
                                ],
                            ],

                        ]
                    ]);
                    ?>
                </div>
                <div class="form-group" style="display: none;">
                    <?php
                    echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' =>1,
                        'attributes' => [


                            'financial_year' => ['type' => Form::INPUT_WIDGET,
                                'widgetClass' => \kartik\select2\Select2::className(),
                                //  'label' => 'Payment Request',
                                'label' => 'Financial Year',

                                'options' => [
                                    'data' => ArrayHelper::map(\backend\modules\disbursement\models\FinancialYear::findBySql("SELECT * FROM financial_year ORDER BY is_active DESC, financial_year DESC")->asArray()->all(),'financial_year_id','financial_year'),
                                    'options' => [
                                        'prompt'=>'Select Financial Year',


                                    ],
                                ],
                            ],



                            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                        ]
                    ]);
                    ?>
                </div>


                <div class="form-group">
                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                </div>

            </div>

            <div class="panel-footer">
                <div class="form-group">
                    <div id="progress"></div>
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'importBtn']) ?>
                    <?= Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning pull-right']);?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <?php
                    echo $form->field($modelImport, 'fileImport')->widget(FileInput::classname(), [
                        //'options' => ['accept' => 'image/*'],
                        'options' => ['accept' => 'xls/*'],
                        'pluginOptions' => [
                            'showPreview' => false,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => false
                        ]
                    ]);
                    ?>
                </div>
            </div>
        </div>


    </div>

</div>

<div style="display: none;">
    <?= $form->field($model, 'batch_number')->textInput(['maxlength' => true])->label(false) ?>
</div>



    <script>
        $(document).ready(function () {
            $("#importBtn").on("click",function () {
                $(this). hide();
                $("#progress").html('<div style="font-size: large;"><span class="fa fa-3x fa-spinner fa-spin"></span> Please Wait... Uploading & Analyzing the File... </div>');
            });
        });
    </script>
    <?php ActiveForm::end(); ?>

</div>
