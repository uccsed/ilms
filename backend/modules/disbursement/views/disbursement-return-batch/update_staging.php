<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/3/18
 * Time: 11:56 PM
 */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementReturnBatch */

$this->title = "Update $model->index_number | ".$model->student_name."'s $model->loan_item Returns";
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Return Batches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="disbursement-return-batch-update">

    <div class="panel-info">
        <div class="panel-heading"><h4><?= Html::encode($this->title) ?></h4></div>

        <?= $this->render('_staging_form', [
            'model' => $model,
        ]) ?>
    </div>


</div>
