<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementReturnBatch */

$this->title = 'Update Disbursement Return Batch: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Return Batches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="disbursement-return-batch-update">

    <div class="panel-info">
        <div class="panel-heading"><h4><?= Html::encode($this->title) ?></h4></div>

        <?= $this->render('_formEdit', [
            'model' => $model,
            'modelImport' => $modelImport,
        ]) ?>
    </div>


</div>
