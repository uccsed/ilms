<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/3/18
 * Time: 11:57 PM
 */
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\widgets\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementReturnBatch */
/* @var $form yii\widgets\ActiveForm */
echo  Module::FetchBootstrap('js');
////id,index_number,student_name,sex,year_of_study,pay_sheet_serial_number,loan_item,instalment_number,amount,remarks,academic_year,status,match_percent,reasons,created_by,created_on,header_id,sync_id

?>

<?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-flat">
        <div class="panel-heading"></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group"> <?= $form->field($model, 'index_number')->textInput(['maxlength' => true]) ?></div>
                </div>
                <div class="col-md-5">
                    <div class="form-group"><?= $form->field($model, 'student_name')->textInput(['maxlength' => true]) ?></div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <?php
                        echo Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'columns' =>1,
                            'attributes' => [


                                'sex' => ['type' => Form::INPUT_WIDGET,
                                    'widgetClass' => \kartik\select2\Select2::className(),
                                    //  'label' => 'Payment Request',
                                    'label' => 'Sex',

                                    'options' => [
                                        'data' => ['M'=>'M','F'=>'F'],
                                        'options' => [
                                            'prompt' => 'Select Sex',
                                        ],
                                    ],
                                ],

                            ]
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group"><?= $form->field($model, 'year_of_study')->textInput(['type'=>'number','min'=>1,'max'=>10]) ?></div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php
                        echo Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'columns' =>1,
                            'attributes' => [


                                'academic_year' => ['type' => Form::INPUT_WIDGET,
                                    'widgetClass' => \kartik\select2\Select2::className(),
                                    //  'label' => 'Payment Request',
                                    'label' => 'Academic Year',

                                    'options' => [
                                        'data' => ArrayHelper::map(\common\models\AcademicYear::findBySql("SELECT * FROM academic_year ORDER BY is_current DESC, academic_year DESC")->asArray()->all(),'academic_year_id','academic_year'),
                                        'options' => [
                                            'prompt' => 'Select Academic Year',
                                        ],
                                    ],
                                ],

                            ]
                        ]);?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group"><?= $form->field($model, 'pay_sheet_serial_number')->textInput(['type'=>'number','min'=>1,'maxlength' => true]) ?></div>
                </div>


                <div class="col-md-2">
                    <div class="form-group">
                        <?php
                        echo Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'columns' =>1,
                            'attributes' => [


                                'loan_item' => ['type' => Form::INPUT_WIDGET,
                                    'widgetClass' => \kartik\select2\Select2::className(),
                                    //  'label' => 'Payment Request',
                                    'label' => 'Loan Item',

                                    'options' => [
                                        'data' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::findBySql("SELECT * FROM loan_item ORDER BY is_active DESC, item_code ASC")->asArray()->all(),'item_code','item_code'),
                                        'options' => [
                                            'prompt' => 'Select Loan Item',
                                        ],
                                    ],
                                ],

                            ]
                        ]);?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group"><?= $form->field($model, 'amount')->textInput(['class'=>'text-right  text-bold']) ?></div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <?php
                        echo Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'columns' =>1,
                            'attributes' => [


                                'instalment_number' => ['type' => Form::INPUT_WIDGET,
                                    'widgetClass' => \kartik\select2\Select2::className(),
                                    //  'label' => 'Payment Request',
                                    'label' => 'Instalment',

                                    'options' => [
                                        'data' => ArrayHelper::map(\backend\modules\disbursement\models\InstalmentDefinition::findBySql("SELECT * FROM instalment_definition ORDER BY instalment ASC, is_active DESC")->asArray()->all(),'instalment','instalment'),
                                        'options' => [
                                            'prompt' => 'Select Instalment',
                                        ],
                                    ],
                                ],

                            ]
                        ]);?>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group"><?= $form->field($model, 'remarks')->textInput() ?></div>
                </div>
            </div>

        </div>


        <div class="panel-footer">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

    </div>


<?php ActiveForm::end(); ?>