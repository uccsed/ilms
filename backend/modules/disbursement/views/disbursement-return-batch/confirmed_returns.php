<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/3/18
 * Time: 10:18 PM
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;

echo Module::FetchBootstrap('js');
?>
<p>
    <button id="btnUpdate" class="btn btn btn-success"><span id="update_icon" class="fa fa-check"></span> Update Customer Statement</button>
</p>


<script>
    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }


    function ReturnUpdate() {
        var Counter = 0;
        $("#confirmation_icon").attr("class","fa fa-spinner fa-spin");
        $("#btnConfirm").attr("disabled","disabled");
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-return-batch/update-returns'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                headerID:'<?php echo $batchID; ?>'
            },
            success:function (data) {
                var results = data.output['results'];
                Counter = results.length;
                $("#confirmation_icon").attr("class","fa fa-check");
                $("#btnConfirm").removeAttr("disabled");

                alert(commaSeparateNumber(Counter)+' records has been successfully Updated to customer Statement!');
                //window.reload();
                window.location.replace("#");
            }
        });
    }

    $(document).ready(function () {


        $("#btnUpdate").on('click',function () {
            ReturnUpdate();
        });
    });
</script>
<?php
//id,index_number,student_name,sex,year_of_study,pay_sheet_serial_number,loan_item,instalment_number,amount,remarks,academic_year,status,match_percent,reasons,created_by,created_on,header_id,sync_id
$columns = [
    ['class'=>'kartik\grid\SerialColumn'],

    [
        'attribute'=>'index_number',
        //'width'=>'250px',
        'label'=>"INDEX #",

    ],

    [
        'attribute'=>'application_id',
        //'width'=>'250px',
        'format' => 'raw',
        'value' => function ($model) {
            return strtoupper($model->application->applicant->user->firstname.' '.$model->application->applicant->user->middlename.' '.$model->application->applicant->user->surname);
        },
        'label'=>"NAME",
        //'nowrap'=>true,

    ],
    [
        'attribute' => 'sex',
        //'width'=>'250px',
        'label'=>"SEX",
        'hAlign'=>'center',
        'format' => 'raw',
        'value' => function ($model) {
            return strtoupper($model->sex);
        },
        'filterType' => GridView::FILTER_SELECT2,
          'filter' => ['M'=>'MALE','F'=>'FEMALE'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

    ],


    [
        'attribute'=>'year_of_study',

        //'width'=>'250px',
        'hAlign'=>'center',
        'label'=>"YOS",

    ],
    [
        'attribute'=>'pay_sheet_serial_number',
        //'width'=>'250px',
        'hAlign'=>'right',
        'label'=>"SN",

    ],
    [
        'attribute' => 'loan_item',
        //'width'=>'250px',
        'label'=>"LOAN ITEM",
        'format' => 'raw',
        'value' => function ($model) {
            return strtoupper($model->loanItem->item_code);
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->asArray()->all(),'loan_item_id','item_code'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

    ],
    [
        'attribute' => 'instalment_number',
        //'width'=>'250px',
        'label'=>"INSTALMENT",
        'format' => 'raw',
        'hAlign'=>'center',
        'value' => function ($model) {
            return strtoupper($model->instalment->instalment);
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(\backend\modules\disbursement\models\InstalmentDefinition::find()->asArray()->all(),'instalment_definition_id','instalment'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

        'pageSummary'=>'TOTAL',
        'pageSummaryOptions'=>['class'=>'text-right'],
    ],


    [
        'attribute'=>'amount',
        'width'=>'150px',
        'hAlign'=>'right',
        'label'=>'AMOUNT',
        'format'=>['decimal', 2],
        'pageSummary'=>true
    ],

    [
        'attribute'=>'remarks',
        'width'=>'150px',
        'label'=>'REMARKS',
        //'hAlign'=>'right',
        'format'=>'raw',

    ],



    //'comment',
    /*['class' => 'yii\grid\ActionColumn',
        'template'=>'{view}{delete}',
    ],*/

    [
        'attribute' => 'status',
        //'width'=>'250px',
        'label'=>"STATUS",
        'format' => 'raw',
        'value' => function ($model) {
            return $model->status=='1'?"UPDATED":"NEW";
        },
        'filterType' => GridView::FILTER_SELECT2,
       'filter' => ['0'=>'NEW','1'=>'UPDATED',],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],
        'format' => 'raw'
    ],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'columns' => $columns,
    'panel'=>['class'=>'primary']
]); ?>

