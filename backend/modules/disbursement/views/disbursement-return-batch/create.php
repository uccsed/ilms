<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementReturnBatch */

$this->title = 'Create Returns Header';
$this->params['breadcrumbs'][] = ['label' => 'Returns Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-return-batch-create">
    <div class="panel-info">
        <div class="panel-heading"><h4><?= Html::encode($this->title) ?></h4></div>

        <?= $this->render('_form', [
            'model' => $model,
            'modelImport' => $modelImport,
        ]) ?>
    </div>


</div>
