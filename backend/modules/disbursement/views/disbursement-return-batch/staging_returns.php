<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/3/18
 * Time: 10:18 PM
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
echo Module::FetchBootstrap('js');
?>
<p>
    <button id="btnMatch" class="btn btn btn-primary"><span id="cross_match_icon" class="fa fa-search"></span> Run A Cross-Match</button>
    <?php if ($status == 'MATCH'){ ?>

    <button id="btnConfirm" class="btn btn btn-success pull-right"><span id="confirmation_icon" class="fa fa-check"></span> Confirm Match Matched Records</button>
    <?php } ?>
</p>


<script>
    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }
    function CrossMatch() {
        var matchCounter = 0;
        $("#cross_match_icon").attr("class","fa fa-spinner fa-spin");
        $("#btnMatch").attr("disabled","disabled");
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-return-batch/cross-match'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                headerID:'<?php echo $batchID; ?>'
            },
            success:function (data) {
                var results = data.output['results'];
                matchCounter = results.length;
                $("#cross_match_icon").attr("class","fa fa-search");
                $("#btnMatch").removeAttr("disabled");

                alert('A Cross-match has been made and found '+commaSeparateNumber(matchCounter)+' records');
                //window.reload();
                 window.location.replace("#");
            }
        });
    }

    function Confirmation() {
        var ConfirmationCounter = 0;
        $("#confirmation_icon").attr("class","fa fa-spinner fa-spin");
        $("#btnConfirm").attr("disabled","disabled");
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-return-batch/confirmation'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                headerID:'<?php echo $batchID; ?>'
            },
            success:function (data) {
                var results = data.output['results'];
                ConfirmationCounter = results.length;
                $("#confirmation_icon").attr("class","fa fa-check");
                $("#btnConfirm").removeAttr("disabled");

                alert(commaSeparateNumber(ConfirmationCounter)+' records has been successfully confirmed!');
                //window.reload();
                window.location.replace("#");
            }
        });
    }

    $(document).ready(function () {

        $("#btnMatch").on('click',function () {
            CrossMatch();
        });

        $("#btnConfirm").on('click',function () {
            Confirmation();
        });
    });
</script>

<?php
//id,index_number,student_name,sex,year_of_study,pay_sheet_serial_number,loan_item,instalment_number,amount,remarks,academic_year,status,match_percent,reasons,created_by,created_on,header_id,sync_id
$columns = [
    ['class'=>'kartik\grid\SerialColumn'],

    [
        'attribute'=>'index_number',
        //'width'=>'250px',
        'label'=>"INDEX #",

    ],

    [
        'attribute'=>'student_name',
        //'width'=>'250px',
        'label'=>"NAME",
        //'nowrap'=>true,

    ],
    [
        'attribute' => 'sex',
        //'width'=>'250px',
        'label'=>"SEX",
        'hAlign'=>'center',
        'format' => 'raw',
        'value' => function ($model) {
            return strtoupper($model->sex);
        },
        'filterType' => GridView::FILTER_SELECT2,
          'filter' => ['M'=>'MALE','F'=>'FEMALE'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

    ],


    [
        'attribute'=>'year_of_study',
        //'width'=>'250px',
        'hAlign'=>'center',
        'label'=>"YOS",

    ],
    [
        'attribute'=>'pay_sheet_serial_number',
        //'width'=>'250px',
        'hAlign'=>'right',
        'label'=>"SN",

    ],
    [
        'attribute' => 'loan_item',
        //'width'=>'250px',
        'label'=>"LOAN ITEM",
        'format' => 'raw',
        'value' => function ($model) {
            return strtoupper($model->loan_item);
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->asArray()->all(),'item_code','item_code'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

    ],
    [
        'attribute' => 'instalment_number',
        //'width'=>'250px',
        'label'=>"INSTALMENT",
        'format' => 'raw',
        'hAlign'=>'center',
        'value' => function ($model) {
            return strtoupper($model->instalment_number);
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(\backend\modules\disbursement\models\InstalmentDefinition::find()->asArray()->all(),'instalment','instalment_desc'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

        'pageSummary'=>'TOTAL',
        'pageSummaryOptions'=>['class'=>'text-right'],
    ],


    [
        'attribute'=>'amount',
        'width'=>'150px',
        'hAlign'=>'right',
        'label'=>'AMOUNT',
        'format'=>['decimal', 2],
        'pageSummary'=>true
    ],

    [
        'attribute'=>'remarks',
        'width'=>'150px',
        //'hAlign'=>'right',
        'label'=>'REMARKS',
        'format'=>'raw',

    ],



    //'comment',
    /*['class' => 'yii\grid\ActionColumn',
        'template'=>'{view}{delete}',
    ],*/

    [
        'attribute' => 'status',
        //'width'=>'250px',
        'label'=>"STATUS",
        'format' => 'raw',
        'value' => function ($model) {
            return $model->status;
        },
        'filterType' => GridView::FILTER_SELECT2,
       'filter' => ['NEW'=>'NEW','MATCH'=>'MATCH','MISMATCH'=>'MISMATCH','CONFIRMED'=>'CONFIRMED',],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],
        'format' => 'raw'
    ],
    [
        'attribute'=>'match_percent',
        'width'=>'150px',
        'hAlign'=>'right',
        'label'=>'MATCH %',
        'format'=>['decimal',0],
        //'pageSummary'=>true
    ],
    ['class' => 'kartik\grid\ActionColumn',
        'template'=>'{update}{delete}',
        'buttons' => [
                    'update' => function ($url,$model) {
                      return Html::a(
                           '<span class="fa fa-pencil" title="Edit"></span>',
                          Yii::$app->urlManager->createAbsoluteUrl(array('disbursement/disbursement-return-batch/update-staging','id'=>$model->id)));
                   },
                  /*   'view' => function ($url,$model,$key) {
                            return Html::a('<span class="green">View Detail</span>', $url);
                   },*/
                 'delete' => function ($url,$model) {
                return Html::a(
                    '<span class="fa fa-trash" title="Delete"></span>',
                    Yii::$app->urlManager->createAbsoluteUrl(array('disbursement/disbursement-return-batch/delete-staging','id'=>$model->id)));
            },

              ],
    ],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'columns' => $columns,
    'panel'=>['class'=>'primary']
]); ?>

