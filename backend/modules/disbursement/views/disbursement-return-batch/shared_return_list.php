<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/6/18
 * Time: 9:19 AM
 */
use backend\modules\disbursement\models\DisbursementReturnSearch;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;

$batchID=$return_batch_id;
$searchModel = new DisbursementReturnSearch();
$dataProvider = $searchModel->searchBatch(Yii::$app->request->queryParams,$batchID);
$dataProvider->pagination=false;
?>

<?php
//id,index_number,student_name,sex,year_of_study,pay_sheet_serial_number,loan_item,instalment_number,amount,remarks,academic_year,status,match_percent,reasons,created_by,created_on,header_id,sync_id
$columns = [
    ['class'=>'kartik\grid\SerialColumn'],

    [
        'attribute'=>'index_number',
        //'width'=>'250px',
        'label'=>"INDEX #",
        'filter'=>false,

    ],

    [
        'attribute'=>'application_id',
        //'width'=>'250px',
        'format' => 'raw',
        'value' => function ($model) {
            return strtoupper($model->application->applicant->user->firstname.' '.$model->application->applicant->user->middlename.' '.$model->application->applicant->user->surname);
        },
        'label'=>"NAME",
        //'nowrap'=>true,
        'filter'=>false,

    ],
    [
        'attribute' => 'sex',
        //'width'=>'250px',
        'label'=>"SEX",
        'hAlign'=>'center',
        'format' => 'raw',
        'value' => function ($model) {
            return strtoupper($model->sex);
        },
        'filter'=>false,
    ],


    [
        'attribute'=>'year_of_study',

        //'width'=>'250px',
        'hAlign'=>'center',
        'label'=>"YOS",
        'filter'=>false,

    ],
    [
        'attribute'=>'pay_sheet_serial_number',
        //'width'=>'250px',
        'hAlign'=>'right',
        'label'=>"SN",
        'filter'=>false,

    ],
    [
        'attribute' => 'loan_item',
        //'width'=>'250px',
        'label'=>"LOAN ITEM",
        'format' => 'raw',
        'value' => function ($model) {
            return strtoupper($model->loanItem->item_code);
        },
        'filter'=>false,

    ],
    [
        'attribute' => 'instalment_number',
        //'width'=>'250px',
        'label'=>"INSTALMENT",
        'format' => 'raw',
        'hAlign'=>'center',
        'value' => function ($model) {
            return strtoupper($model->instalment->instalment);
        },
        'filter'=>false,

        'pageSummary'=>'TOTAL',
        'pageSummaryOptions'=>['class'=>'text-right'],
    ],


    [
        'attribute'=>'amount',
        'width'=>'150px',
        'hAlign'=>'right',
        'label'=>'AMOUNT',
        'format'=>['decimal', 2],
        'pageSummary'=>true,
         'filter'=>false,
    ],

    [
        'attribute'=>'remarks',
        'width'=>'150px',
        'label'=>'REMARKS',
        //'hAlign'=>'right',
        'format'=>'raw',
        'filter'=>false,
    ],



    //'comment',
    /*['class' => 'yii\grid\ActionColumn',
        'template'=>'{view}{delete}',
    ],*/

    [
        'attribute' => 'status',
        //'width'=>'250px',
        'label'=>"STATUS",
        'format' => 'raw',
        'value' => function ($model) {
            return $model->status=='1'?"UPDATED":"NEW";
        },
        'filter'=>false,
    ],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'columns' => $columns,
    //'panel'=>['class'=>'primary']
]); ?>
