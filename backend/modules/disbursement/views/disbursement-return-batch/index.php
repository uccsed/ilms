<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
use kartik\widgets\DatePicker;
use kartik\export\ExportMenu;
$user=Yii::$app->user->id;
$iSQL="SELECT learning_institution_id AS 'id', institution_name AS 'name',institution_code AS 'code', CONCAT(IFNULL(institution_name,''),' - (',IFNULL(institution_code,''),')') AS 'remix' FROM learning_institution WHERE institution_type = 'UNIVERSITY' AND learning_institution_id IN (SELECT institution_assignment.institution_id FROM institution_assignment WHERE institution_assignment.officer_id='$user')";
$InstitutionModel = Yii::$app->db->createCommand($iSQL)->queryAll();
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\DisbursementReturnBatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disbursement Returns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-return-batch-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <div class="panel">
        <div class="panel-heading"><h3 class="text-uppercase"><?= Html::encode($this->title) ?></h3>
            <p>
                <?= Html::a('Create Disbursement Return Batch', ['create'], ['class' => 'btn btn-success']) ?>
                <a href="../attachments/disbursement/RETURNS_SAMPLE_FILE.xls" class="btn btn-primary pull-right" target="_blank">Download Returns Template</a>
            </p>
        </div>
        <div class="panel-body">
            <?php
            $gridColumns = [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'hAlign' => GridView::ALIGN_CENTER,
                ],
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'allowBatchToggle' => true,
                    'detail' => function ($model) {
                        return $this->render('master_list',['return_batch_id'=>$model->id,'status'=>$model->status]);
                    },
                    'detailOptions' => [
                        'class' => 'kv-state-enable',
                    ],
                ],
                [
                    'attribute' => 'id',
                    'label'=>"Header ID",
                    'format' => 'raw',
                    //'hAlign'=>'right',
                    'width'=>'120px',
                    'value' => function ($model) {
                        return $model->id;
                    },


                ],
                [//ArrayHelper::map($InstitutionModel,'id','remix'),
                    'attribute' => 'institution_id',
                    'label'=>"INSTITUTION",
                    'format' => 'raw',
                    //'hAlign'=>'right',
                    //'width'=>'120px',
                    'value' => function ($model) {
                        return $model->learningInstitution->institution_code;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map($InstitutionModel,'id','remix'),

                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],

                ],


                [
                    'attribute' => 'academic_year',
                    'label'=>"ACADEMIC YEAR",
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->academicYear->academic_year;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(\common\models\AcademicYear::find()->orderBy("academic_year_id ASC")->asArray()->all(), 'academic_year_id', 'academic_year'),

                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],

                ],


                [
                    'attribute' => 'financial_year',
                    'label'=>"FINANCIAL YEAR",
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->financialYear->financial_year;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(\backend\modules\disbursement\models\FinancialYear::find()->orderBy("financial_year_id ASC")->asArray()->all(), 'financial_year_id', 'financial_year'),

                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],
                    'pageSummary'=>'GRAND TOTAL',
                    'pageSummaryOptions'=>['class'=>'text-right text-white'],
                ],
                [
                    'attribute' => 'PendingAmount',
                    'label'=>"PENDING (Amnt)",
                    'format' => ['decimal',2],
                    'hAlign'=>'right',
                    'width'=>'120px',
                    'value' => function ($model) {
                        return $model->PendingAmount;
                    },
                    'pageSummary'=>true,
                ],
                [
                    'attribute' => 'ConfirmedAmount',
                    'label'=>"CONFIRMED  (Amnt)",
                    'format' => ['decimal',2],
                    'hAlign'=>'right',
                    'width'=>'120px',
                    'value' => function ($model) {
                        return $model->ConfirmedAmount;
                    },
                    'pageSummary'=>true,
                ],
                [
                    'attribute' => 'UpdatedAmount',
                    'label'=>"UPDATED (Amnt)",
                    'format' => ['decimal',2],
                    'hAlign'=>'right',
                    'width'=>'120px',
                    'value' => function ($model) {
                        return $model->UpdatedAmount;
                    },
                    'pageSummary'=>true,
                ],



                [
                    'attribute' => 'status',
                    'label'=>"STATUS",
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->status;//=='1'?"UPDATED":"NEW";
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ['NEW'=>'NEW','UPDATED'=>'UPDATED'],
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],

                ],

// ['class' => 'yii\grid\ActionColumn'],

                ['class' => 'kartik\grid\ActionColumn',
                    'template'=>'{view}',
                ],
            ];
            ?>

            <?php
            /*echo*/ ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,

                'fontAwesome' => true,
//            'asDropdown' => false
                'batchSize' => 50,
                'target' => '_blank',
                'selectedColumns' => [0, 1, 2, 3, 4, 5, 6, 7], // Col seq 2 to 6
                'columnSelectorOptions' => [
                    'label' => 'Export Columns',
                ],
                // 'hiddenColumns' => [15], // SerialColumn, Color, & ActionColumn
                //'disabledColumns' => [0, 1, 2, 3, 4, 5, 6, 9, 12], // ID & Name
                'noExportColumns' => [15],
                'dropdownOptions' => [
                    'label' => 'Export Data',
                    'class' => 'btn btn-default'
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_EXCEL => false,
                    ExportMenu::FORMAT_EXCEL_X => false,
                ],
                //'folder' => '@webroot/tmp', // this is default save folder on server
            ]) . "<hr>\n";
            ?>

            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                'showPageSummary'=>true,
                'pageSummaryRowOptions'=>['class'=>'text-bold bg-blue-gradient'],
                'pjax'=>true,
                'striped'=>true,
                'hover'=>true,
            ]);

            ?>
        </div>
    </div>

</div>
