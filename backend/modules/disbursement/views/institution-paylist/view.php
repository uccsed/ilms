<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\InstitutionPaylist */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Institution Paylists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-paylist-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'request_id',
            'disbursement_batch_id',
            'application_id',
            'programme_id',
            'loan_item_id',
            'allocated_amount',
            'status',
            'created_at',
            'created_by',
        ],
    ]) ?>

</div>
