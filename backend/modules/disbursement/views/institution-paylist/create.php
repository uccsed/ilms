<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\InstitutionPaylist */

$this->title = 'Create Institution Paylist';
$this->params['breadcrumbs'][] = ['label' => 'Institution Paylists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-paylist-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
