<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\DisbursementDepositBatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disbursement Deposit Batches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-deposit-batch-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Disbursement Deposit Batch', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'institution_id',
            'academic_year',
            'description:ntext',
            'deposit_date',
            // 'deposit_cheque_number',
            // 'cheque_amount',
            // 'status',
            // 'created_on',
            // 'created_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
