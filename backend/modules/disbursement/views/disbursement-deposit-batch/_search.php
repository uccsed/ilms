<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementDepositBatchSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disbursement-deposit-batch-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'institution_id') ?>

    <?= $form->field($model, 'academic_year') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'deposit_date') ?>

    <?php // echo $form->field($model, 'deposit_cheque_number') ?>

    <?php // echo $form->field($model, 'cheque_amount') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
