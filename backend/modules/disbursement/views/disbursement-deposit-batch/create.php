<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementDepositBatch */

$this->title = 'Create Disbursement Deposit Batch';
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Deposit Batches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-deposit-batch-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelImport' => $modelImport,
    ]) ?>

</div>
