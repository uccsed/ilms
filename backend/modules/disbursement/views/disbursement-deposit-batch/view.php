<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementDepositBatch */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Deposit Batches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-deposit-batch-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'institution_id',
            'academic_year',
            'description:ntext',
            'deposit_date',
            'deposit_cheque_number',
            'cheque_amount',
            'status',
            'created_on',
            'created_by',
        ],
    ]) ?>

</div>
