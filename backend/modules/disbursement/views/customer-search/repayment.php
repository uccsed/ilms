<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/22/18
 * Time: 2:11 PM
 */


$mpdf=new mPDF('c','A4-L','','',5,5,30,25,10,10);
use backend\modules\disbursement\Module;
use Yii;

$html = '';
?>



<?php
$html.= '
<html>
<head>
<style>
body {font-family: sans-serif;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0mm solid #000000;
	border-right: 0mm solid #000000;
}
table thead td { background-color: #EEEEEE;
	text-align: center;
	border: 0mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: right;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
</style>
</head>

<body>
';
?>






<?php
$html.='<p style=" font-family: \'Courier New\';">';
$html.='
<h3 style="text-align: center; font-family: \'Courier New\';">HIGHER EDUCATION STUDENTS LOAN BOARD</h3>
      <table width="100%" style=" font-family: \'Courier New\';">
        <tr><th colspan="3" style="text-align: center; font-family: \'Courier New\';">STUDENT REPAYMENT STATEMENT FOR '.Module::APPLICANT_INFO($index,"fullName").'</th></tr>
        <tr><th colspan="3" style="text-align: center; font-family: \'Courier New\';">Schedule printed on : '. date("l F jS Y H:i:s").'</th></tr>
    </table>
';
$html.='</p>';

?>

<?php $html.=Module::CustomerStatementQuery($index,$YoS=null,$startDate=null,$endDate=null,$instalment=null)['table'] ?>


<?php
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle("heslb");
$mpdf->SetAuthor("heslb");
$mpdf->SetWatermarkText("HESLB");
$mpdf->showWatermarkText = true;
/*  $mpdf->SetWatermarkImage('../images/background.jpg');
  $mpdf->showWatermarkImage = true;*/
$mpdf->watermark_font = 'DejaVuSansCondensed';
$mpdf->watermarkTextAlpha = 0.1;
$mpdf->SetDisplayMode('fullpage');
$mpdf->setFooter('|{PAGENO} of {nbpg}|');

?>


<?php
$html.= '
</body>
</html>
';
?>
<?php
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

exit;
?>

