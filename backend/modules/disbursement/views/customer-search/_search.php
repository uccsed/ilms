<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/20/18
 * Time: 1:16 PM
 */
use kartik\widgets\Select2;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use kartik\date\DatePicker;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementSuspension */
/* @var $form yii\widgets\ActiveForm */

echo Module::FetchBootstrap('js');
?>

<?php $form = ActiveForm::begin([
    'options'=>['enctype'=>'multipart/form-data'], // important
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>

<div>
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-addon"><span class="fa fa-search"></span></div>
            <?= Html::textInput('search','',['id'=>'search','class'=>'form-control bordered border-danger','placeholder'=>'Search form IV/VI index number','style'=>'font-size:24px; height:50px;']); ?>
            <input type="hidden" id="index_number" name="index_number" class="form-control" />
            <div class="input-group-addon"><span id="search_feedback"><span class="fa fa-spinner fa-2x"></span></span></div>
            <div class="input-group-addon"><button type="button" id="searchButton" class="btn bg-red"><span class="fa fa-search"></span> Search</button></div>
        </div>
    </div>
</div>


<?php  ActiveForm::end();   ?>




























<script>
    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }

    function SearchLoanee(){
        var spinner = '<span class="fa fa-spinner fa-2x fa-spin">';
        $('#index_number').val('');
        $("#search_feedback").html(spinner);
        $("#history_table").html('<tr><td colspan="4" style="font-size: large;">'+spinner+' Please Wait ... Loading</td></tr>');
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/customer-search/search-lonee'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                search:$('#search').val(),
                option:$('#search').val(),
            },
            success:function (data) {
                //console.log(data);
                $('#index_number').val('');
                $("#history_table").html('');
                var dataArray = data;

                if (dataArray.length!=0){
                    $("#search_feedback").html('<span class="fa fa-check fa-2x text-green"></span>');
                    $("#search_feedback").html('<span class="fa fa-check fa-2x text-green"></span>');
                    $.each(dataArray,function (index,value) {
                        $('#applicantName').html(value.full_name);

                        $('#PHOTO').attr('src','../'+value.photo);
                        $('#IndexNumber').html(value.index_number);
                        $('#student_status').html(value.student_status);
                        $('#regNo').html(value.registration_number);
                        $('#indexNo').html(value.index_number);
                        $('#index_number').val(value.index_number);
                        $('#sex').html(value.Sex);
                        $('#birthDate').html(value.DOB);
                        $('#programme').html(value.programme_name+' ( <b>'+value.programme_code+'</b>)'+' [ <b>'+value.years_of_study+'</b> years]');
                        $('#institution').html(value.Institution);
                        $('#studyYear').html(value.current_study_year);
                        $('#phone_number').html(value.phone_number);
                        $('#email_address').html(value.email_address);

                    });

                }else{
                    $("#search_feedback").html('<span class="fa fa-ban fa-2x text-red"></span>');
                    $('#applicantName').html('');
                    $('#regNo').html('');
                    $('#indexNo').html('');
                    $('#index_number').val('');
                    $('#sex').html('');
                    $('#birthDate').html('');
                    $('#programme').html('');
                    $('#institution').html('');
                    $('#student_status').html('');

                }

            }
        });
    }

    $(document).ready(function () {


        $("#searchButton").on('click',function () {
            SearchLoanee();
        });


    });

</script>
