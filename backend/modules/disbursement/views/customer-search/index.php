<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/20/18
 * Time: 12:55 PM
 */
use backend\modules\disbursement\Module;
echo Module::FetchBootstrap('js');
?>

<div class="row">
    <div class="col-md-9">
        <div class="panel">
            <div class="panel-heading">
                <?php echo $this->render('_search'); ?>
            </div>
            <div class="panel-body">
                <div class="nav-tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item active">
                            <a class="nav-link active" id="application-tab" data-toggle="tab" href="#application" role="tab" aria-controls="application" aria-selected="true">APPLICATIONS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="allocation-tab" data-toggle="tab" href="#allocation" role="tab" aria-controls="allocation" aria-selected="false">ALLOCATIONS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="statement-tab" data-toggle="tab" href="#statement" role="tab" aria-controls="statement" aria-selected="false">STATEMENT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="return-tab" data-toggle="tab" href="#return" role="tab" aria-controls="return" aria-selected="false">RETURNS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="repayment-tab" data-toggle="tab" href="#repayment" role="tab" aria-controls="repayment" aria-selected="false">REPAYMENTS</a>
                        </li>

                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="application" role="tabpanel" aria-labelledby="application-tab">
                            <div id="application_operations" class="row">
                                <div class="col-md-6 pull-left">
                                    <button type="button" class="btn btn bg-blue-gradient" id="application_btn"><i class="fa fa-gear" id="application_gear"></i> <span>Load Application</span></button>
                                </div>
                                <div class="pull_right col-md-6" id="application_print_div"></div>
                            </div>
                            <div id="application_content" style="font-size: 18px;"></div>
                        </div>

                        <div class="tab-pane fade" id="allocation" role="tabpanel" aria-labelledby="allocation-tab">
                            <div id="allocation_operations" class="row">
                                <div class="col-md-6 pull-left">
                                    <button type="button" class="btn btn bg-blue-gradient" id="allocation_btn"><i class="fa fa-gear" id="allocation_gear"></i> <span>Load Allocation</span></button>
                                </div>
                                <div class="col-md-6 pull_right" id="allocation_print_div"></div>
                            </div>
                            <div id="allocation_content" style="font-size: 18px;"></div>
                        </div>

                        <div class="tab-pane fade" id="statement" role="tabpanel" aria-labelledby="statement-tab">
                            <div id="statement_operations" class="row">
                                <div class="col-md-6 pull-left">
                                    <button type="button" class="btn btn bg-blue-gradient" id="statement_btn"><i class="fa fa-gear" id="statement_gear"></i> <span>Load Statement</span></button>
                                </div>
                                <div class="col-md-6 pull_right" id="statement_print_div"></div>
                            </div>
                            <div id="statement_content" style="font-size: 18px;"></div>
                        </div>



                        <div class="tab-pane fade" id="return" role="tabpanel" aria-labelledby="return-tab">
                            <div id="return_operations" class="row">
                                <div class="col-md-6 pull-left">
                                    <button type="button" class="btn btn bg-blue-gradient" id="return_btn"><i class="fa fa-gear" id="return_gear"></i> <span>Load Returns</span></button>
                                </div>
                                <div class="col-md-6 pull_right" id="return_print_div"></div>
                            </div>
                            <div id="return_content" style="font-size: 18px;"></div>
                        </div>

                        <div class="tab-pane fade" id="repayment" role="tabpanel" aria-labelledby="repayment-tab">
                            <div id="repayment_operations" class="row">
                                <div class="col-md-6 pull-left">
                                    <button type="button" class="btn btn bg-blue-gradient" id="repayment_btn"><i class="fa fa-gear" id="repayment_gear"></i> <span>Load Repayment</span></button>
                                </div>
                                <div class="col-md-6 pull_right" id="repayment_print_div"></div>
                            </div>
                            <div id="repayment_content"></div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="panel-footer"></div>

        </div>
    </div>

    <div class="col-md-3">
        <div class="panel">
            <div class="panel-heading">
                <h2 id="IndexNumber" style="text-align: center; font-weight: bolder;  font-family: 'Courier New';"></h2>
                <p style="text-align: center;">

                    <img src="../applicant_attachment/profile/mwajuma.jpeg" width="300px" id="PHOTO" />
                </p>
            </div>
            <table class="table">
                <tbody>
                <tr><th>REG.NO</th><td id="regNo"style="text-align: left; font-size: 16px; font-weight: bolder;  font-family: 'Courier New';" ></td></tr>
                <tr><th>NAME</th><td id="applicantName" style="text-align: left; font-size: 16px; font-weight: bolder;  font-family: 'Courier New';"></td></tr>
                <tr><th>SEX</th><td id="sex" style="text-align: left; font-size: 16px; font-weight: bolder;  font-family: 'Courier New';"></td></tr>
                <tr><th>DOB</th><td id="birthDate" style="text-align: left; font-size: 16px;   font-family: 'Courier New';"></td></tr>
                <tr><th>YOS</th><td id="studyYear" style="text-align: left; font-size: 16px; font-family: 'Courier New';"></td></tr>
                <tr><th>STATUS</th><td class="text-uppercase text-bold" style="text-align: left; font-size: 16px; font-weight: bolder;  font-family: 'Courier New';" id="student_status"></td></tr>
                <tr><th>PROGRAMME</th><td id="programme" style="text-align: left; font-size: 16px;   font-family: 'Courier New';"></td></tr>
                <tr><th>INSTITUTION</th><td id="institution" style="text-align: left; font-size: 16px;   font-family: 'Courier New';"></td></tr>
                <tr><th>MOBILE</th><td id="phone_number" style="text-align: left; font-size: 16px;  font-family: 'Courier New';"></td></tr>
                </tbody>

            </table>

            <div class="panel-footer"></div>

        </div>
    </div>

</div>





<script>
    function Animate(itemName,operation=true)
    {
        var spinner = '<div style="font-size: 24px;"><span class="fa fa-2x fa-spinner fa-spin"></span> Please Wait... Loading Data...</div>';
        if(operation){
            $('#'+itemName+'_gear').attr('class','fa fa-gear fa-spin');
            $('#'+itemName+'_content').html(spinner);
        } else {
            $('#'+itemName+'_gear').attr('class','fa fa-gear');
            $('#'+itemName+'_content').html("");
        }
    }

    function ContentsLoader(dataArray,itemName){
        var index=$('#index_number').val();
        var link = "<?php echo Yii::$app->urlManager->createUrl('/disbursement/customer-search/print-document'); ?>";
        var printer = '<a href="'+link+'&option='+itemName+'&index='+index+'" class="btn bg-blue-gradient btn-info pull-right printBtn" id="'+itemName+'_printBtn" value="'+itemName+'" target="_blank"><span class="fa fa-print"></span> PRINT '+itemName.toUpperCase()+'</a>';
        $('#'+itemName+'_content').html("");
        $('#'+itemName+'_print_div').html(printer);
        $('#'+itemName+'_content').html(dataArray[0]);
    }




    function SearchEngine(itemName)
    {
        Animate(itemName); //Starts Animations
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/customer-search/search-lonee'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:itemName,
                search:$('#index_number').val()
            },
            success:function (data)
            {
                var dataArray = data;
                Animate(itemName,false); //End Animations
                if(dataArray.length!==0){
                    ContentsLoader(dataArray,itemName);
                } else {
                    $("#"+itemName+"_content").html("No data found");
                }

            }
        })

    }

    function PrintDocument(itemName)
    {
        Animate(itemName); //Starts Animations
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/customer-search/print-document'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:itemName,
                index_number:$('#index_number').val()
            },
            success:function (data)
            {
                var dataArray = data;
                Animate(itemName,false); //End Animations
                if(dataArray.length!==0){
                    ContentsLoader(dataArray,itemName);
                } else {
                    $("#"+itemName+"_content").html("No data found");
                }

            }
        })

    }

    $(document).ready(function () {


        $("body").on('click','.printBtn',function () {
            //alert('000784222'+$(this).val());
           // SearchEngine('application');
            var itemName = $(this).val();
            PrintDocument(itemName);
        });

        $("#application_btn").on('click',function () {
            SearchEngine('application');
        });

        $("#allocation_btn").on('click',function () {
            SearchEngine('allocation');
        });

        $("#statement_btn").on('click',function () {
            SearchEngine('statement');
        });

        $("#return_btn").on('click',function () {
            SearchEngine('return');
        });

        $("#repayment_btn").on('click',function () {
            SearchEngine('repayment');
        });
    });
</script>
