<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 7/9/18
 * Time: 4:00 PM
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
$this->title = 'New Disbursement Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-batch-index">
    <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">
            <div class="pull-right" id="top_operators">
                <!--<div class="row">
                    <div class="col-md-3"><a href="<?php /*echo url::to(['disbursement-calendar/index']); */?>" class="btn btn-primary pull-left"><i class="icon-plus"></i> Back to Calendar</a></div>
                </div>-->
            </div>

            <?php echo $this->render('_form') ?>
        </div>


    </div>
</div>