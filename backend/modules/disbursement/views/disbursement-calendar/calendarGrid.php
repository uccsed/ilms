<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        //'verification_comment_group_id',
        //'event_name',
        //'event_description',
        //'start_date',
        //'end_date',
        'institution_id',
      /*  [
            'attribute' => 'institution_id',
            'label'=>"Institution",
            'format' => 'raw',
            //'value' =>'Institution',
            'value' => function ($model) {
                return $model->Institution;
            },
            'filterType' => GridView::FILTER_SELECT2,
            //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
            'filter' =>ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql('SELECT * FROM learning_institution WHERE institution_type="UNIVERSITY" ORDER BY institution_name ASC')->asArray()->all(), 'learning_institution_id', 'institution_code'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Search  '],
            'format' => 'raw'
        ],*/


        [
            'attribute' => 'programme_id',
            'label'=>"Programme",
            'format' => 'raw',
            'value' => 'Programme',
            'filterType' => GridView::FILTER_SELECT2,
            //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
            'filter' =>ArrayHelper::map(\common\models\User::findBySql('SELECT user.user_id,CONCAT(user.firstname," ",user.surname) AS "Name" FROM `user` WHERE user.login_type=5')->asArray()->all(), 'user_id', 'Name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Search  '],
            'format' => 'raw'
        ],


        //'comment',
        ['class' => 'yii\grid\ActionColumn',
            'template'=>'{view}{update}{delete}',
        ],
    ],
]); ?>