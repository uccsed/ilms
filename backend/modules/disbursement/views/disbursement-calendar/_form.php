<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 7/9/18
 * Time: 4:00 PM
 */


use backend\modules\allocation\models\Programme;

use backend\modules\allocation\models\LearningInstitution;
use yii\helpers\ArrayHelper;

use yii\helpers\Html;
use yii\helpers\Url;

use kartik\select2\Select2;
use backend\modules\allocation\models\LoanItem;
use backend\modules\disbursement\Module;

//echo Module::FetchBootstrap('fancyInputs');

//$dataArray = Module::PriorityList(4);
?>

<!--<div><?php /*echo $dataArray['name']; */?> <span style="background-color: <?php /*echo $dataArray['color']; */?>;"><?php /*echo $dataArray['star']; */?></span></div>
-->
<script type="text/javascript" src="../ExtraPlugins/bootstrap/datepicker/jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>

<link href="../ExtraPlugins/bootstrap/datepicker/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="../ExtraPlugins/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<!--
<script type="text/javascript" src="../bootstrap/datepicker/jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="../bootstrap/datepicker/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="../bootstrap/datepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="../bootstrap/datepicker/js/locales/bootstrap-datetimepicker.sw.js" charset="UTF-8"></script>-->

<div class="row" id="top_operators">

    <div class="col-md-2">
        <div class="form-group">
            <div class="col-lg-12">
                <?php
                $Institution=LearningInstitution::findall(['institution_type'=>'UNIVERSITY']);
                $institutionListData=ArrayHelper::map($Institution,'learning_institution_id','institution_name');
                //echo  Html::dropDownList('learning_institution','',$institutionListData,array('id'=>'learning_institution','class'=>'form-control','prompt'=>'Select Institution'));

                echo Select2::widget([
                    'id' => 'learning_institution',
                    'name' => 'learning_institution',
                    'value' => '',
                    'data' => $institutionListData,
                    'options' => ['placeholder' => 'Select Institution']
                ]);
                ?>
                <span id="institution_error"></span>
            </div>
        </div>
    </div>



    <div class="col-md-2">
        <div class="form-group">
            <div class="col-lg-12">
                <?php
                $Programme=Programme::find()->all();
                $programmeListData=ArrayHelper::map($Programme,'programme_id','programme_name');
               // echo  Html::dropDownList('programme_id','',$programmeListData,array('id'=>'programme_id','class'=>'form-control','prompt'=>'Select Programme'));
                echo Select2::widget([
                    'id' => 'programme_id',
                    'name' => 'programme_id',
                    'value' => '',
                    'data' => $programmeListData,
                    'options' => ['placeholder' => 'Select Programme']
                ]);
                ?>
                <span id="programme_error"></span>
            </div>
        </div>
    </div>



    <div class="col-md-3">
        <div class="form-group">
            <!-- <label class="control-label col-lg-3">Event Title</label>-->
            <div class="col-lg-12">
                <input type="text" class="form-control" id="default_event_name" name="default_event_name" placeholder="Name of the Event" />
            </div>
        </div>
    </div>

    <div class="col-md-3">

        <div class="form-group">
            <!--<label class="control-label col-lg-3">Event Description</label>-->
            <div class="col-lg-12">
                <input type="text" class="form-control" id="default_event_description" name="default_event_description" placeholder="Describe the Event" />
            </div>
        </div>
    </div>
    <!-- <?php /*for ($i=0; $i<=0; $i++){ */?>
        <div class="col-md-3">
            <div class="form-group">

                <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd MM yyyy" data-link-field="date<?php /*echo $i; */?>">
                    <input class="form-control" size="16" type="text" value="" readonly="readonly" id="demo<?php /*echo $i; */?>">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                </div>
                <input type="text" id="date<?php /*echo $i; */?>" value="" /><br/>
            </div>
        </div>
    --><?php /*} */?>


</div>
<div class="row">
    <form id="disbursement_calendar_form" class="form-horizontal" method="POST" action="#">
        <div class="col-md-12" id="disbursement_calendar_contents">
            <!-- Here is where the inputs are being appended -->
        </div>
    </form>

</div>



<script type="text/javascript">

    function InitiateDatePicker() {
        $('.form_date').datetimepicker({
            language:  'en',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
    }



    $(document).ready(function () {

    });


    /*  $('.form_datetime').datetimepicker({
          //language:  'fr',
          weekStart: 1,
          todayBtn:  1,
          autoclose: 1,
          todayHighlight: 1,
          startView: 2,
          forceParse: 0,
          showMeridian: 1
      });
      $('.form_date').datetimepicker({
          language:  'fr',
          weekStart: 1,
          todayBtn:  1,
          autoclose: 1,
          todayHighlight: 1,
          startView: 2,
          minView: 2,
          forceParse: 0
      });
      $('.form_time').datetimepicker({
          language:  'fr',
          weekStart: 1,
          todayBtn:  1,
          autoclose: 1,
          todayHighlight: 1,
          startView: 1,
          minView: 0,
          maxView: 1,
          forceParse: 0
      });*/








    /* $('.form_date').datetimepicker({
         language:  'fr',
         weekStart: 1,
         todayBtn:  1,
         autoclose: 1,
         todayHighlight: 1,
         startView: 2,
         minView: 2,
         forceParse: 0
     });
     jQuery(document).ready(function () {


     });*/

    /*    $('.date').datetimepicker({
            language:  'fr',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });*/


    /*$('.form_datetime').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    });
    $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    $('.form_time').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });*/
</script>

<script type="text/javascript">
    function GenerateList()
    {
        var institution =  jQuery("#learning_institution").val();
        var programme =  jQuery("#programme_id").val();

        var responseDiv = "response_div";
        var basicOperations = "top_operators";

        jQuery.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-calendar/events'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                institution_id:institution,
                programme_id:programme,
            },
            success:function (data) {

                jQuery('#disbursement_calendar_contents').html(data.output);
                //jQuery('#'+basicOperations).show();
            }

        });
    }




    $(document).ready(function () {


        jQuery("#default_event_name").on("change keyup",function () {
            jQuery(".EventName").val(jQuery(this).val());
        });

        jQuery("#default_event_description").on("change keyup",function () {
            jQuery(".EventDescription").val(jQuery(this).val());
        });



        $("#learning_institution").on("change",function () {

            GenerateList();

            $.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-plan/programme'); ?>",
                type:"POST",
                // cache:false,

                data:{
                    institution_id:jQuery(this).val(),
                    _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                },

                success: function (data) {

                    jQuery("#programme_id").empty(); // remove old options
                    jQuery("#programme_id").append(data.output); // remove old options

                    //CheckExistence();

                }
            });
        });

        $("#programme_id").on("change",function () {
            GenerateList();

        });

        jQuery('#submitButton').on('click',function () {


            //Validate select/primary inputs before submitting the form
            var valid = 0;
            //Validate Institution Select
            valid+= CustomValidator('learning_institution','institution_error','Learning Institution');
            //valid+= CustomValidator('programme_id','programme_error','Programme');


            if (valid>=1){ //If Valid to all fields
                jQuery('#submitButton').hide();
                jQuery('#top_operators').hide();
                jQuery('#response_div').show();
                jQuery('#disbursement_calendar_contents').hide();

                //var posData = jQuery('#disbursement_plan_form').serializeArray();
                var formID = "disbursement_calendar_form";
                var postURL = "<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-calendar/create'); ?>";
                var responseDiv = "response_div";
                var basicOperations = "top_operators";
                AjaxPostForm(formID,postURL,responseDiv,basicOperations)
                //console.log(posData);
            }


        });


    });


    function CustomValidator(item,errorDiv,fieldName){
        if(jQuery('#'+item).val()==""){
            jQuery('#'+errorDiv).show();
            jQuery('#'+errorDiv).prop("style","color: red;");
            jQuery('#'+errorDiv).html(fieldName+" can NOT be empty");
            return 0;
        }else{
            jQuery('#'+errorDiv).hide();
            jQuery('#'+errorDiv).prop("style","color: black;");
            jQuery('#'+errorDiv).html("");
            return 1;
        }
    }


    function AjaxPostForm(formID,postURL,responseDiv,basicOperations) {

        var years = [];
        var StartDate = [];
        var EndDate = [];

        var EventName = [];
        var EventDescription = [];

        var institution =  jQuery("#learning_institution").val();
        var programme =  jQuery("#programme_id").val();





        var head = institution + '-' + programme;

        /*jQuery.each(jQuery("input[class='StartDate']"),function (index,value) {
            years.push($(this).title);
            StartDate.push($(this).val());
        });*/

        jQuery("input[name='StartDate[]']").each(function() {
            years.push(this.title);
            StartDate.push($(this).val());

        });

        jQuery("input[name='EndDate[]']").each(function() {
            EndDate.push($(this).val());
        });

        jQuery("input[name='EventName[]']").each(function() {
            EventName.push($(this).val());
        });

        jQuery("input[name='EventDescription[]']").each(function() {
            EventDescription.push($(this).val());
        });

        $.ajax({
            url:postURL,
            type:"POST",
            cache:false,
            data:{
                //postData:jQuery('#'+formID).serializeArray(),
                institution:institution,
                programme:programme,
                years:years,
                StartDate:StartDate,
                EndDate:EndDate,
                EventName:EventName,
                EventDescription:EventDescription,

            },
            success:function (data) {
                jQuery('#'+responseDiv).html(data.output);
                jQuery('#'+basicOperations).show();
            }
        })
    }



</script>

<div class="row">
    <div class="pull-right">
        <button type="button" id="submitButton"  name="submitButton" class="btn btn-success btn-large">Register</button>
        <button type="reset" id="resetButton"  name="resetButton" class="btn btn-default btn-large">Reset</button>
        <a href="<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-calendar/index'); ?>" id="cancelButton"  name="cancelButton" class="btn btn-warning btn-large">Cancel</a>

    </div>
</div>


</form>

<div id="response_div" class="panel text-center col-md-12 " style="display: none;">
    <!--<img src="image/loader/loader.gif" />
    <img src="image/loader/loader1.gif" />-->
    <img src="image/loader/loader2.gif"  />
</div>
