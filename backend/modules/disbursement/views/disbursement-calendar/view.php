<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/11/18
 * Time: 9:47 PM
 */


use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */

$this->title = $model->event_name.' '.$model->Institution; #.' Assignment to '.$model->Officer;
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Calendar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-assignment-view">
    <div class="institution-assignment-index">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">

                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-success']); ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>

                    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-warning']); ?>

                </p>
                <?php
               // echo date('Y-m-d',strtotime($model->start_date));
                //date('l d/m/Y',strtotime(strtotime($model->start_date).'-'.$model->pre_remainder_days.' days'))
                //echo date('d/m/Y',strtotime(($model->start_date).'-'.$model->pre_remainder_days.' days'));
                ?>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'event_name',
                        'event_description',
                        //'AcademicYear',
                        'Institution',
                        'Programme',
                        //'Semester',
                       [
                            'name'=>'start_date',
                            'value'=>date('l d/m/Y',strtotime($model->start_date)),
                            'format' => 'html',
                            'label'=>'Start Date',
                        ],

                        [
                            'name'=>'end_date',
                            'value'=>date('l d/m/Y',strtotime($model->end_date)),
                            'format' => 'html',
                            'label'=>'End Date',
                        ],
                        [
                            'name'=>'event_priority',
                            'value'=>$model->Priority,
                            'format' => 'html',
                            'label'=>'Priority',
                        ],
                        'pre_remainder_days',
                        //'targeted_personnels',
                        [
                            'name'=>'pre_remainder_days',
                            'value'=>date('l d/m/Y',strtotime(($model->start_date).'-'.$model->pre_remainder_days.' days')),
                            'format' => 'html',
                            'label'=>'Remainder Date',
                        ],
                        [
                            'name'=>'targeted_personnels',
                            'value'=> $model->AssignedTo,
                            'format' => 'html',
                            'label'=>'Assigned To',
                        ],
                        // 'Instalment',
                       /*  'LoanItem',
                        [
                            'name'=>'disbursement_percent',
                            'value'=>number_format($model->disbursement_percent).'%',
                            'type'=>'raw',
                            'label'=>'Percent',
                        ],*/
                        // 'disbursement_percent',
                        //'Institution',
                        /* 'sync_id',
                         'assigned_by',
                         'assigned_on',*/
                    ],
                ]) ?>
            </div>
        </div>
