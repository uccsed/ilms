<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/12/18
 * Time: 8:29 AM
 */
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use backend\modules\disbursement\Module;
?>


<?php $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>





<div class="row">
    <div class="col-md-7">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' =>1,
            'attributes' => [

                'institution_id' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    'label' => 'Institution',
                    'options' => [
                        'data' => ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql('SELECT * FROM learning_institution WHERE institution_type="UNIVERSITY" AND learning_institution_id IN (SELECT institution_id FROM institution_assignment) ORDER BY institution_name ASC')->asArray()->all(), 'learning_institution_id', 'institution_code'),

                        //'data' =>ArrayHelper::map(\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),

                        'options' => [
                            'prompt' => 'Select',
                            // 'multiple'=>TRUE,

                        ],
                    ],
                ],


                //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
            ]
        ]);
        ?>

        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' =>1,
            'attributes' => [

                'programme_id' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    'label' => 'Programme',
                    'options' => [
                        'data' => ArrayHelper::map(\backend\modules\allocation\models\Programme::findBySql('SELECT * FROM programme WHERE learning_institution_id IN (SELECT institution_id FROM institution_assignment) ORDER BY programme_name ASC')->asArray()->all(), 'programme_id', 'programme_name'),

                        //'data' =>ArrayHelper::map(\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),

                        'options' => [
                            'prompt' => 'Select',
                            // 'multiple'=>TRUE,

                        ],
                    ],
                ],


                //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
            ]
        ]);
        ?>

<div class="form-group"><?= $form->field($model,'event_name')->textInput(['class'=>'form-control']) ?></div>
<div class="form-group"><?= $form->field($model,'event_description')->textArea(['class'=>'form-control']) ?></div>


<div class="form-group"><?= $form->field($model,'updated_by')->hiddenInput(['class'=>'form-control'])->label(false); ?></div>
<div class="form-group"><?= $form->field($model,'updated_on')->hiddenInput(['class'=>'form-control'])->label(false); ?></div>
<div class="row">
    <div class="col-md-6">
        <?php
        echo DatePicker::widget([
            //'name' => 'start_date',
            'model' => $model,
            'attribute'=>'start_date',
            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
            //'value' => 'start_date',
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ],
            'options' => [
                // you can hide the input by setting the following
                 //'style' => 'display:none'
                'readonly'=>'readonly','placeholder' => 'Enter start date ...'
            ]
        ]);
        ?>
    </div>
    <div class="col-md-6">
        <?php
        echo DatePicker::widget([
            //'name' => 'end_date',
            'model' => $model,
            'attribute'=>'end_date',
            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
            //'value' => $model->end_date,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ],
            'options' => [
                // you can hide the input by setting the following
                //'style' => 'display:none'
                'readonly'=>'readonly','placeholder' => 'Enter end date ...'
            ]
        ]);
        ?>
    </div>
</div>

  <div class="row">
      <div class="form-group">
          <div class="row">
              <div class="col-md-6">


                  <?php
                  echo $form->field($model, 'event_priority')
                      ->dropDownList(
                          ArrayHelper::map(Module::PriorityList(), 'id', 'name'),           // Flat array ('id'=>'label')
                          ['prompt'=>'']    // options
                      );
                  ?>


              </div>
              <div class="col-md-6"><?= $form->field($model,'pre_remainder_days')->textInput(['class'=>'form-control']) ?></div>
          </div>
      </div>
  </div>


<div class="">
    <div class="form-group">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' =>1,
            'attributes' => [
                'targeted_personnels' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    'label' => 'Assigned To',
                    'options' => [
                        'data' => ArrayHelper::map(\common\models\User::find()->where(['login_type' => 5])->all(), 'user_id', 'firstname'),

                        //'data' =>ArrayHelper::map(\frontend\modules\application\models\AttachmentDefinition::findBySql('SELECT attachment_definition_id,attachment_desc FROM `attachment_definition`')->asArray()->all(), 'attachment_definition_id', 'attachment_desc'),

                        'options' => [
                            'prompt' => 'Select',
                            'multiple'=>TRUE,

                        ],
                    ],
                ],


                //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
            ]
        ]);
        ?>
    </div>
</div>






    </div>
    <div class="col-md-5"></div>
</div>


<div class="text-right">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php
    echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
    echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

    ActiveForm::end();
    ?>
</div>

