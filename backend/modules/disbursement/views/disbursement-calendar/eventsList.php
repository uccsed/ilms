<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 7/16/18
 * Time: 8:42 PM
 */
use backend\modules\disbursement\Module;
$PriorityArray = Module::PriorityList();
$priorityInput = "";
?>


<div id="feedback_div"></div>
<table class="table table-bordered table-responsive">
    <tbody id="eventList">

    </tbody>
</table>

<div id="model_list"></div>

<script>

    function LoadEditingModel(value){

        var model='<div class="modal fade" id="eventModal'+value.id+'" tabindex="-1" role="dialog" aria-labelledby="eventModalLabel">\n' +
            '    <div class="modal-dialog" role="document">\n' +
            '        <div class="modal-content">\n' +
            '            <div class="modal-header">\n' +
            '                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n' +
            '                <h4 class="modal-title text-uppercase text-bold">'+value.label+'</h4>\n' +
            '            </div>\n' +
            '            <div class="modal-body">\n' +
            '                <form>\n' +
            '                    <div class="form-group">\n' +
            '                        <label for="recipient-name" class="control-label">Event Name:</label>\n' +
            '                        <input type="text" class="form-control" id="event_name'+value.id+'" value="'+value.name+'">\n' +
            '                    </div>\n' +
            '                    <div class="form-group">\n' +
            '                        <label for="message-text" class="control-label">Description:</label>\n' +
            '                        <input type="text" class="form-control" id="event_description'+value.id+'" value="'+value.description+'">\n' +
            '                    </div>\n' +
            '\n' +
                                '<div class="row">'+
                                     '<div class="col-md-4">'+
                    '                    <div class="form-group">\n' +
                    '                        <label for="message-text" class="control-label">Start Date:</label>\n' +
                                            '<div class="input-group">'+
                                            '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>'+
                    '                        <input type="text" class="form-control" id="start_date'+value.id+'" value="'+value.start_date+'">\n' +
                                            '</div>'+
                    '                    </div>\n' +
                    '                   </div>\n' +
                '\n' +
                                    '<div class="col-md-4">'+
                        '                    <div class="form-group">\n' +
                        '                        <label for="message-text" class="control-label">End Date:</label>\n' +
                                                '<div class="input-group">'+
                                                    '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>'+
                            '                        <input type="text" class="form-control" id="end_date'+value.id+'" value="'+value.end_date+'">\n' +
                            '                    </div>\n' +
                        '                    </div>\n' +

                                    '</div>\n' +
                                        PriorityInput(value)+
                                '</div>\n' +
            '                </form>\n' +
            '            </div>\n' +
            '            <div class="modal-footer">\n' +
            '                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n' +
            '                <button type="button" class="btn btn-primary btnUpdate" onclick="UpdateEvent('+value.id+')" data-dismiss="modal">Update</button>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>';



        return model;



    }


    function PriorityInput(value){
        var output = (<?php echo json_encode($PriorityArray); ?>);


            var option = "";
        $.each(output,function (index,itemDetails) {

            if(value.id===index){
                option+='<option selected="selected" value="'+index+'">'+itemDetails.name+'</option>';
            }else {
                option+='<option value="'+index+'">'+itemDetails.name+'</option>';
            }

        });

        var input = '' +
            '<div class="col-md-4">' +
                '<div class="form-group">' +
                    '<label class="control-label">Event Priority</label>' +
                    '<div class="input-group">' +
                        '<span class="input-group-addon"><i class="fa fa-star"></i></span>' +
                        '<select class="btn btn-danger" id="priorityInput'+value.id+'">';
                            input+=option;
                        input+='</select>' +
                    '</div>' +
                '</div>' +
            '';

        return input;
    }

    function UpdateEvent(eventPk){
        $.ajax({
           url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-calendar/edit'); ?>",
           type:"POST",
           cache:false,
           data:{
               _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
               id:eventPk,
               name:$("#event_name"+eventPk).val(),
               description:$("#event_description"+eventPk).val(),
               start:$("#start_date"+eventPk).val(),
               end:$("#end_date"+eventPk).val(),
               priority:$("#priorityInput"+eventPk).val(),

           },
            success:function (data) {
                //LoadEvents(10);
                $("#feedback_div").attr("class","alert alert-info");
                $("#feedback_div").html(data.output);
                LoadEvents(10);
            }


        });

        //LoadEvents(10);
        //window.location.reload(true);
    }
    function LoadEvents(limit){
        $("#eventList").html("");
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-calendar/recent'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                limit:limit
            },
            success:function (data) {
              var output = data.output;
              $.each(output,function (index,value) {
                  $("#eventList").append(
                      '<tr class="border-danger border-double">'+
                      '<td class="col-md-3" style="background-color: '+value.color+'; color:#ffffff;">' +
                      '<div class="col-md-12 text-center" style="font-weight: lighter; font-size: xx-large;">'+value.date+'</div>'+
                      '<div class="col-md-12 text-center text-uppercase text-smaller" style="font-weight: bolder;">'+value.month+'</div>'+
                      '<div class="col-md-12 text-center text-uppercase" style="font-weight: lighter; font-size: xx-small;">'+value.priority+' Priority</div>'+
                      '<div class="col-md-12 text-right">'+value.star+'</div>'+
                      '</td>'+
                      '<td class="col-md-9" valign="top">' +
                      '<div class="col-md-12 text-bold" style="color: '+value.color+';">'+value.label+'<span class="pull-right"><a href="#" data-toggle="modal" data-target="#eventModal'+value.id+'"><span class="fa fa-pencil"></span></a></span></div>'+
                      '<blockquote class="col-md-12" style="font-size: x-small;font-style: italic;">'+value.description+'</blockquote>'+
                      '<div class="row" style="vertical-align: bottom;">' +
                      '<div class="col-md-12 text-left text-uppercase text-bold" style="font-size: x-small;">'+value.institution+' : '+value.programme+'</div>'+

                      '</div>'+
                      '</td>'+
                      '</td>'

                  );

                  $("#model_list").append(LoadEditingModel(value));

              });
            }
        });


    }
    $(document).ready(function () {
        LoadEvents(10);

        $("body").on(".btnUpdate",'click',function () {
           var eventPk = $(this).val();
            UpdateEvent(eventPk)
        });
    });
</script>



