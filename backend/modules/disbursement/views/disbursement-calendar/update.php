<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 7/9/18
 * Time: 4:01 PM
 */
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */

$this->title = 'Update '.$model->event_name.' Calendar Event for  '.$model->Institution;
$this->params['breadcrumbs'][] = ['label' => 'Calendar Event', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="institution-assignment-update">
    <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">
            <?= $this->render('_formEdit', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>