<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 7/9/18
 * Time: 3:54 PM
 */
use yii\helpers\Html;
use yii\helpers\Url;

use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;

use backend\modules\disbursement\Module;

$this->title = 'Disbursement Calendar of Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- /.row -->
<!-- jQuery 2.2.3 -->
<script src="../vendor/almasaeed2010/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- fullCalendar 2.2.5 -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="../ExtraPlugins/fullcalendar/fullcalendar.min.js"></script>
<script src='http://fullcalendar.io/js/fullcalendar-2.1.1/lib/moment.min.js'></script>
<script src='http://fullcalendar.io/js/fullcalendar-2.1.1/lib/jquery.min.js'></script>
<script src="http://fullcalendar.io/js/fullcalendar-2.1.1/lib/jquery-ui.custom.min.js"></script>
<script src='http://fullcalendar.io/js/fullcalendar-2.1.1/fullcalendar.min.js'></script>-->



<div class="disbursement-batch-index">
    <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">

            <div class="row">
                <div class="pull-left" id="top_operators">
                    <div class="col-md-3"><a href="<?php echo url::to(['disbursement-calendar/create']); ?>" class="btn btn-success pull-left">Add new Event(s)</a></div>

                </div>
            </div>
            <div class="row">

            </div>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'verification_comment_group_id',
                    'event_name',
                    //'event_description',
                    //'start_date',
                    [
                        //'attribute' => 'start_date',
                        'attribute' => 'start_date',

                        'value' => 'StartDate',
                        'format' => 'raw',
                        'label' => "Start Date",
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            //'name' => 'start_date',
                            'name' => 'DisbursementCalendar[start_date]',
                            //'value' => date("Y-m-d"),
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                //'autoclose' => true,
                            ]
                        ])

                    ],
                    // 'end_date',
                    [
                        'attribute' => 'end_date',
                        'value' => 'EndDate',
                        'format' => 'raw',
                        'label' => "End Date",
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'name' => 'DisbursementCalendar[end_date]',
                            //'value' => date("Y-m-d H:i:s"),
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                               // 'autoclose' => true,
                            ]
                        ])

                    ],
                    //'institution_id',
                    [
                        'attribute' => 'institution_id',
                        'label'=>"Institution",
                        'format' => 'raw',
                        //'value' =>'Institution',
                        'value' => function ($model) {
                            return $model->Institution;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                        'filter' =>ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql('SELECT * FROM learning_institution WHERE institution_type="UNIVERSITY" ORDER BY institution_name ASC')->asArray()->all(), 'learning_institution_id', 'institution_code'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],


                    [
                        'attribute' => 'programme_id',
                        'label'=>"Programme",
                        'format' => 'raw',
                        'value' => 'Programme',
                        'filterType' => GridView::FILTER_SELECT2,
                        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                        'filter' =>ArrayHelper::map(\backend\modules\allocation\models\Programme::findBySql('SELECT * FROM programme')->asArray()->all(), 'programme_id', 'programme_name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],

                    [
                        'attribute' => 'event_priority',
                        'label'=>"Priority",
                        'format' => 'raw',
                        'value' => 'Priority',
                        'filterType' => GridView::FILTER_SELECT2,
                        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                        'filter' =>ArrayHelper::map(Module::PriorityList(), 'id', 'name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],


                    //'comment',
                    ['class' => 'yii\grid\ActionColumn',
                        'template'=>'{view}{update}{delete}',
                    ],
                ],
            ]); ?>
        </div>


    </div>
</div>


