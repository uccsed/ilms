<?php
 
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\PayoutlistMovement */
/* @var $form yii\widgets\ActiveForm */
?>
 <div id="formBoundary">


<?php
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
echo Module::FetchBootstrap('js');
 
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]);
?>
 <?php /*= $form->field($model, 'to_officer')->label(false)->hiddenInput(['value'=>\yii::$app->user->id]) */?>
         
   <?php

echo Form::widget([ // fields with labels
    'model'=>$model,
    'form'=>$form,
    'columns'=>1,
    'attributes'=>[
   
  'comment' => ['type' => Form::INPUT_TEXTAREA,
                 'label' => 'Comment',
                
      ]
    ]
]);


   if ($action=='1'){
       $class = 'success';
   }else{
       $class = 'danger';
   }
?>
 
  <div class="text-right" id="buttons">
    <?= $form->field($model, 'date_out')->label(false)->hiddenInput(["value"=>date("Y-m-d")]) ?>
     <?= $form->field($model, 'disbursements_batch_id')->label(false)->hiddenInput(["value"=>$disbursementId]) ?>
    <?php /*= $form->field($model, 'from_officer')->label(false)->hiddenInput(['value'=>\yii::$app->user->identity->user_id]) */?>
  
 <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? "btn btn-$class" : "btn btn-$class",'id'=>'btnSubmit']) ?>

<?php
echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
?>
  <?php //= Html::a('Cancel', ['index','id'=>$model->isNewRecord ?$disbursement_schedule_id:$model->disbursement_schedule_id], ['class' => 'btn btn-primary']) ?>
              
      <?php
ActiveForm::end();
?>
 
    </div>
 </div>
<div id="response_div"></div>
<script>
    $(document).ready(function () {
        $("#btnSubmit").on('click',function () {
            $(this).hide();
            $("#formBoundary").hide();
            $("#buttons").hide();
            $("#response_div").show();

            var spinner ='<span class="fa fa-spinner fa-spin fa-5x"></span>';
            var content = '<div style="font-size: 18px;">'+spinner+' Please Wait... The system is currently processing your Decision</div>';


            $("#response_div").html(content);

        })

    });

</script>