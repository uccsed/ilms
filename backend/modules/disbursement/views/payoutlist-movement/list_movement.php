<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\PayoutlistMovementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payout List Movement';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payoutlist-movement-index">
 <div class="panel panel-info">
        <div class="panel-heading">
<?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'movement_id',
            //'disbursements_batch_id',
              [
                     'attribute' => 'from_officer',
                        'label'=>"From  Officer",
                        'vAlign' => 'middle',
                       // 'width' => '200px',
                        'value' => function ($model) {
                           return $model->FromOfficer;
                        },
                    ],
              [
                     'attribute' => 'to_officer',
                        'label'=>"To Officer",
                        'vAlign' => 'middle',
                       // 'width' => '200px',
                        'value' => function ($model) {
                            return $model->ToOfficer;
                        },
                    ],
            //'to_officer',
            //'',
            [
                'attribute' => 'Task',
                // 'label'=>"From  Officer",
                'vAlign' => 'middle',
                //  'width' => '200px',
                'value' => function ($model) {
                    return $model->Task;
                },
            ],
            'comment',
            [
                    'attribute'=>'created_at',
                    'value'=>function ($model) {
                        return date('l d/m/Y',strtotime($model->created_at));
                    },
                    'format'=>'html',
                    'label'=>'Initiation Date',
            ],

            [
                'attribute'=>'date_out',
                'value'=>function ($model) {
                    return !empty($model->date_out)?date('l d/m/Y',strtotime($model->date_out)):'Not Set';
                },
                'format'=>'html',
                'label'=>'Date Out',
            ],

            [
                'attribute'=>'date_out',
                'value'=>function ($model) {
                    return number_format($model->Duration,0);
                },
                'format'=>'html',
                'label'=>'Duration',
            ],
             [
                     'attribute' => 'movement_status',
                       // 'label'=>"From  Officer",
                        'vAlign' => 'middle',
                      //  'width' => '200px',
                        'value' => function ($model) {
                            return $model->movement_status==0?"Pending":"Attended";
                        },
                    ],
           //  'date_in',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
 </div>
</div>