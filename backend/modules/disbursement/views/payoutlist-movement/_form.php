<?php
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\PayoutlistMovement */
/* @var $form yii\widgets\ActiveForm */


 $tzsql= Yii::$app->db->createCommand("SELECT SUM(disbursed_amount) as amount FROM `disbursement` di WHERE di.disbursement_batch_id='{$disbursementId}'")->queryAll();           
   // $modelamount=  \backend\modules\disbursement\models\Disbursement::find()->select('SUM(disbursed_amount)')->where(['disbursement_batch_id'=>$disbursementId])->all(); 
  //  print_r($tzsql);
     $amountlimit=0;
     //echo $disbursementId;
     //echo $sqlall="SELECT group_concat(du.`disbursement_structure_id`) as groupdata FROM `disbursement_task_assignment` dt,disbursement_task_definition dd,disbursement_schedule ds , disbursement_user_structure du WHERE dd.disbursement_task_id=dt.`disbursement_task_id` AND ds.disbursement_schedule_id=dt.`disbursement_schedule_id` AND du.`disbursement_structure_id`=dt.`disbursement_structure_id` AND operator_name='Between' AND from_amount<'{$amountlimit}' AND to_amount<='{$amountlimit}'";
      
    foreach($tzsql as $tzrow);
       $amountlimit=$tzrow["amount"];
        $sqlall="SELECT Max(du.`disbursement_structure_id`) as maxlevel FROM `disbursement_task_assignment` dt,disbursement_task_definition dd,disbursement_schedule ds , disbursement_user_structure du WHERE dd.disbursement_task_id=dt.`disbursement_task_id` AND ds.disbursement_schedule_id=dt.`disbursement_schedule_id` AND du.`disbursement_structure_id`=dt.`disbursement_structure_id` AND operator_name='Between' AND from_amount<'{$amountlimit}' AND to_amount<='{$amountlimit}'";
       $modelp= Yii::$app->db->createCommand($sqlall)->queryAll();         
       foreach ($modelp as $rows);
        $structureId=$rows["maxlevel"];
    //SELECT * FROM `disbursement_task_assignment` dt,disbursement_task_definition dd,disbursement_schedule ds , disbursement_user_structure du WHERE dd.disbursement_task_id=dt.`disbursement_task_id` AND ds.disbursement_schedule_id=dt.`disbursement_schedule_id` AND du.`disbursement_structure_id`=dt.disbursement_structure_id
     $sqlmax="SELECT group_concat(user_id) as listuser FROM disbursement_user_structure WHERE disbursement_structure_id='{$structureId}'";
      $modelmax= Yii::$app->db->createCommand($sqlmax)->queryAll();  
    foreach($modelmax as $rowmax);
      $alluserId=$rowmax["listuser"];


      ?>
 
<?php
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
 if (empty($alluserId)){
     $alluserId = Yii::$app->user->id;
     $userModel = Yii::$app->db->createCommand("SELECT user.user_id AS 'id' FROM user WHERE login_type='5' AND user.user_id <>'$alluserId';")->queryAll();
     $userList = array();
     if (sizeof($userModel)!=0){
         foreach ($userModel as $index=>$dataArray){
             $userList[]= $dataArray['id'];
         }
         $alluserId ="'".implode("','",$userList)."'";
     }


 }
 //echo $alluserId;
//$userList = Yii::$app->user->id;

 $mvtSQL = "
         SELECT 
         max(disbursement_payoutlist_movement.movement_id), 
         disbursement_payoutlist_movement.disbursements_batch_id, 
         disbursement_payoutlist_movement.from_officer, 
         disbursement_payoutlist_movement.to_officer, 
         disbursement_payoutlist_movement.movement_status, 
         disbursement_payoutlist_movement.disbursement_task_id
        FROM disbursement_payoutlist_movement
        WHERE disbursement_payoutlist_movement.disbursements_batch_id = '$disbursementId';
 ";
 $mvtModel = Yii::$app->db->createCommand($mvtSQL)->queryAll();




 $SQL = "
 SELECT 
disbursement_user_structure.user_id, 
UPPER(CONCAT(user.firstname,' ',user.surname,' (',disbursement_structure.structure_name,')')) AS 'full_name',
disbursement_structure.structure_name AS 'structure_name',
disbursement_structure.order_level AS 'order_level'
 
FROM disbursement_user_structure
LEFT JOIN user ON disbursement_user_structure.user_id = user.user_id
LEFT JOIN disbursement_structure ON disbursement_user_structure.disbursement_structure_id = disbursement_structure.disbursement_structure_id

WHERE disbursement_user_structure.status<>'0' AND disbursement_user_structure.user_id IN ($alluserId);
ORDER BY disbursement_structure.order_level ASC
 ";

 $userModel = Yii::$app->db->createCommand($SQL)->queryAll();









$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]);
 
echo Form::widget([ // fields with labels
    'model'=>$model,
    'form'=>$form,
    'columns'=>1,
    'attributes'=>[
        //''=>['label'=>'Item Name', 'options'=>['placeholder'=>'Item Name...']],
    'to_officer' => ['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'Officer Name',
                'options' => [
                    'data' => ArrayHelper::map($userModel, 'user_id', 'full_name'),
                    'options' => [
                        'prompt' => 'Select Disbursement Structure',
                        
                    
                    ],
                ],
            ],
  'comment' => ['type' => Form::INPUT_TEXTAREA,
                 'label' => 'Comment',
                
      ]
    ]
]);
 
?>
 
  <div class="text-right">
    <?= $form->field($model, 'date_out')->label(false)->hiddenInput(["value"=>date("Y-m-d")]) ?>
     <?= $form->field($model, 'disbursements_batch_id')->label(false)->hiddenInput(["value"=>$disbursementId]) ?>
    <?= $form->field($model, 'from_officer')->label(false)->hiddenInput(['value'=>\yii::$app->user->identity->user_id]) ?>
  
 <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  
<?php
echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
?>
  <?php //= Html::a('Cancel', ['index','id'=>$model->isNewRecord ?$disbursement_schedule_id:$model->disbursement_schedule_id], ['class' => 'btn btn-primary']) ?>
              
      <?php
ActiveForm::end();
?>
 
    </div>
