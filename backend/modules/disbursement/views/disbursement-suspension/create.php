<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementSuspension */

$this->title = 'Create Disbursement Suspension';
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Suspensions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-suspension-create">
    <div class="panel panel-danger">
        <div class="panel-heading bg-red">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>






        </div>
    </div>
</div>
