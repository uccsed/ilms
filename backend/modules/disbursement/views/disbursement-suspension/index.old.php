<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
use kartik\widgets\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\DisbursementSuspensionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (Yii::$app->controller->id =='disbursement-uplifting'){
    $this->title = 'Suspensions Uplifting';
    $btnLabel = 'Uplift Suspension';
}elseif(Yii::$app->controller->id =='disbursement-suspension'){
    $this->title = 'Disbursement Suspensions';
    $btnLabel = 'Suspend Disbursement';
}

$this->params['breadcrumbs'][] = $this->title;
/*$index = 'S0449.0191.2011';
echo Module::AutoSuspendAll($index);*/
?>
<div class="disbursement-suspension-index">

    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <p>
                <?= Html::a($btnLabel, ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,

                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    ['class'=>'kartik\grid\SerialColumn'],
                    [
                        'attribute' => 'application_id',
                        'label'=>"Lonee",
                        'format' => 'raw',
                        //'hAlign'=>'right',
                        //'width'=>'120px',
                        'value' => function ($model) {
                            return $model->Applicant;
                        },
                        'filter'=>false,
                        'group'=>true,  // enable grouping,
                        'groupedRow'=>true,                    // move grouped column to a single grouped row
                        'groupOddCssClass'=>'kv-grouped-row bg-red',  // configure odd group cell css class
                        'groupEvenCssClass'=>'kv-grouped-row bg-red', // configure even group cell css class

                    ],
                    [
                        'attribute' => 'status_reason',
                        'label'=>"Reasons",
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->Reason;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(\backend\modules\disbursement\models\SuspensionReason::find()->where("is_active=1")->asArray()->all(), 'id', 'name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],

                    [
                        'attribute' => 'status',
                        'label'=>"Operation",
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Module::SuspensionType($model->status);
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                        'filter' =>Module::SuspensionType(),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],


                    [
                        'attribute' => 'loan_item_id',
                        'label'=>"Loan Item",
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->LoanItem;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),

                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],

                    [
                        'attribute' => 'suspension_percent',
                        'label'=>"Suspension (%)",
                        'format' => 'raw',
                        'hAlign'=>'right',
                        'width'=>'120px',
                        'value' => function ($model) {
                            return $model->suspension_percent;
                        },
                    ],
                    [
                        'attribute' => 'remaining_percent',
                        'label'=>"Remaining (%)",
                        'format' => 'raw',
                        'hAlign'=>'right',
                        'width'=>'120px',
                        'value' => function ($model) {
                            return $model->remaining_percent;
                        },
                    ],

                    /*[
                        'attribute' => 'status_date',
                        'label'=>"Effective Date",
                        'format' => 'raw',
                        'hAlign'=>'right',
                        'width'=>'120px',
                        'value' => function ($model) {
                            return date('d/m/Y',strtotime($model->status_date));
                        },
                    ],*/

                       [
                        //'attribute' => 'start_date',
                        'attribute' => 'status_date',

                        'value' => function ($model) {
                            return date('d/m/Y',strtotime($model->status_date));
                        },
                        'format' => 'raw',
                        'label' => "Effective Date",
                           'width'=>'200px',
                           'hAlign'=>'center',
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            //'name' => 'start_date',
                            'name' => 'DisbursementSuspension[status_date]',
                            //'value' => date("Y-m-d"),
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                //'autoclose' => true,
                            ]
                        ])

                    ],


                    //'comment',
                    ['class' => 'yii\grid\ActionColumn',
                        'options'=>['style'=>'width:100px;'],
                        'template'=>'{view}{delete}',
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <?php /*GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'application_id',
            'loan_item_id',
            'suspension_percent',
            'remaining_percent',
            // 'status',
            // 'status_reason',
            // 'remarks:ntext',
            // 'status_date',
            // 'status_by',
            // 'supporting_document:ntext',
            // 'suspension_history:ntext',
            // 'sync_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);*/ ?>

</div>
