<?php
use kartik\widgets\Select2;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use kartik\widgets\FileInput;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementSuspension */
/* @var $form yii\widgets\ActiveForm */
//echo Module::FetchBootstrap('fancyInputs');
//echo Module::FetchBootstrap('dashboard');
//echo Module::FetchBootstrap('header');
echo Module::FetchBootstrap('js');

$SQL = "SELECT * FROM suspension_reason WHERE suspension_reason.is_active='1' AND suspension_reason.type='1' ORDER BY suspension_reason.name ASC";
$dataArray = ArrayHelper::map(\backend\modules\disbursement\models\SuspensionReason::findBySql($SQL)->asArray()->all(),'id','description');

$itemData = ArrayHelper::map(\backend\modules\allocation\models\LoanItem::findBySql('SELECT * FROM loan_item WHERE loan_item.is_active="1" AND loan_item.loan_item_id IN(SELECT allocation.loan_item_id FROM allocation) ORDER BY item_name ASC')->asArray()->all(), 'loan_item_id', 'item_name');
//$model->loan_item_id = array_keys($itemData);
?>

<div class="disbursement-suspension-form">

    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data'], // important
        'type' => ActiveForm::TYPE_VERTICAL,
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><span class="fa fa-search"></span></div>
                    <?= Html::textInput('search','',['id'=>'search','class'=>'form-control bordered border-danger','placeholder'=>'Search form IV/VI index number','style'=>'font-size:24px; height:50px;']); ?>
                    <div class="input-group-addon"><span id="search_feedback"><span class="fa fa-spinner fa-2x"></span></span></div>
                    <div class="input-group-addon"><button type="button" id="searchButton" class="btn bg-red"><span class="fa fa-search"></span> Search</button></div>
                </div>
            </div>

            <?= $form->field($model, 'application_id')->hiddenInput(['class'=>'form-control'])->label(false) ?>

            <div class="form-group">
                <?php
                //echo '<label class="control-label">Suspension Mode</label>';
                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' =>1,
                    'attributes' => [

                        'mode' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            'label' => 'Suspension Mode',
                            'options' => [
                                'data' => ['1'=>'ALL ITEMS','2'=>'SELECTIVE'],
                                //'data' =>ArrayHelper::map(disbursementsuspension-mode\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),

                                'options' => [
                                    'prompt' => 'Select Mode',
                                    //'multiple'=>TRUE,

                                ],
                            ],
                        ],

                    ]
                ]);
                ?>
            </div>

            <div class="form-group" id="loan_item">
                <?php
                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' =>1,
                    'attributes' => [

                        'loan_item_id' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            'label' => 'Loan Items',
                            'options' => [
                                'data' => $itemData,

                                //'data' =>ArrayHelper::map(\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),

                                'options' => [
                                    'prompt' => 'You can Select Multiple items',
                                    'multiple'=>TRUE,

                                ],
                            ],
                        ],

                    ]
                ]);
                ?>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">

                        <?= $form->field($model, 'suspension_percent')->textInput(['value'=>'100', 'class'=>'form-control','type' => 'number','min'=>0,'max'=>100]) ?>
                            <input type="hidden" id="index_number">
                    </div>

                    <div class="col-md-6">

                        <?= $form->field($model, 'remaining_percent')->textInput(['value'=>'0','class'=>'form-control','type' => 'number','min'=>0,'max'=>100]) ?>

                    </div>
                </div>


            </div>




            <?= $form->field($model, 'status')->hiddenInput(['value' => 1,'class'=>'form-control'])->label(false) ?>

            <div class="form-group">
                <?php
                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' =>1,
                    'attributes' => [

                        'status_reason' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            'label' => 'Suspension Reasons',
                            'options' => [
                                'data' => ArrayHelper::map(\backend\modules\disbursement\models\SuspensionReason::findBySql($SQL)->asArray()->all(), 'id', 'name'),

                                //'data' =>ArrayHelper::map(\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),

                                'options' => [
                                    'prompt' => 'Select Reason',
                                    // 'multiple'=>TRUE,

                                ],
                            ],
                        ],

                    ]
                ]);
                ?>
                <blockquote id="reasons_description" class="text-muted text-italics">

                </blockquote>
            </div>

            <div class="form-group">
                <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>
            </div>

            <div class="form-group">
                <?php
                echo $form->field($model, 'file')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                ]);
                ?>

            </div>

            <div style="display: none;">
                <?= $form->field($model, 'status_date')->textInput() ?>

                <?= $form->field($model, 'status_by')->textInput() ?>

                <?= $form->field($model, 'supporting_document')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'suspension_history')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'sync_id')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="text-right">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

                <?php
                echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
                echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

                ?>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel">
                <div class="panel-heading"><h4 class="text-bold text-uppercase">Lonee's Profile</h4></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="../applicant_attachment/profile/mwajuma.jpeg" width="100%" id="PHOTO" />
                        </div>
                        <div class="col-lg-9">
                            <table class="table table-condensed table-bordered">
                                <tbody>
                                <tr>
                                    <th class="text-bold text-uppercase">index#</th><td id="indexNo"></td>
                                    <th class="text-bold text-uppercase">Registration#</th><td id="regNo"></td>
                                </tr>
                                <tr>
                                    <th class="text-bold text-uppercase">Full Name</th><td id="applicantName" colspan="3"></td>
                                </tr>
                                <tr>
                                    <th class="text-bold text-uppercase">Sex</th><td id="sex"></td>
                                    <th class="text-bold text-uppercase">DOB</th><td id="birthDate"></td>
                                </tr>
                                <tr>
                                    <th class="text-bold text-uppercase">Phone#</th><td id="phone_number"></td>
                                    <th class="text-bold text-uppercase">E-mail</th><td id="email_address"></td>
                                </tr>

                                <tr>
                                    <th class="text-bold text-uppercase">Programme</th><td id="programme"></td>
                                    <th class="text-bold text-uppercase">Year of Study</th><td id="studyYear"></td>
                                </tr>
                                <tr>
                                    <th class="text-bold text-uppercase">Institution</th><td colspan="3" id="institution"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

            <div class="panel">
                <div class="panel-heading"><h4 class="text-bold text-uppercase">Allocation</h4></div>
                <div class="panel-body">
                    <div id="statement_operations" class="row">
                        <div class="col-md-6 pull-left">
                        </div>
                        <div class="col-md-6 pull_right" id="allocation_print_div"></div>
                    </div>
                    <div id="allocation_content" style="font-size: 18px;"></div>

                </div>
            </div>
            <div class="panel">
                <div class="panel-heading"><h4 class="text-bold text-uppercase">Customer Statement</h4></div>
                <div class="panel-body">
                    <div id="statement_operations" class="row">
                        <div class="col-md-6 pull-left">
                            <!--<button type="button" class="btn btn bg-blue-gradient" id="statement_btn"><i class="fa fa-gear" id="statement_gear"></i> <span>Load Statement</span></button>-->
                        </div>
                        <div class="col-md-6 pull_right" id="statement_print_div"></div>
                    </div>
                    <div id="statement_content" style="font-size: 18px;"></div>
                 <!--  <table class="table table-condensed table-striped table-bordered">
                        <thead class="bg-red">
                        <tr class="text-uppercase">
                            <th>Loan Item</th>
                            <th>Academic Year</th>
                            <th class="text-right">Allocated Amount</th>
                            <th class="text-right">Disbursed Percentage</th>
                            <th class="text-right">Disbursed Amount</th>
                            <th class="text-right">Remaining Amount</th>
                        </tr>
                        </thead>
                        <tbody id="history_table"></tbody>
                        <tfoot class="bg-red">
                        <tr class="text-uppercase">
                            <th>TOTAL</th>

                            <th></th>
                            <th id="total_allocated" class="text-right text-bold">0.00</th>
                            <th class="text-right text-bold"></th>
                            <th id="total_disbursed" class="text-right text-bold">0.00</th>
                            <th id="total_remainig" class="text-right text-bold">0.00</th>
                        </tr>
                        </tfoot>
                    </table>-->
                </div>
            </div>
        </div>

    </div>

    <script>
        function commaSeparateNumber(val) {
            while (/(\d+)(\d{3})/.test(val.toString())) {
                val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            }
            return val;
        }
        function Animate(itemName,operation=true)
        {
            var spinner = '<div style="font-size: 24px;"><span class="fa fa-2x fa-spinner fa-spin"></span> Please Wait... Loading Data...</div>';
            if(operation){
                $('#'+itemName+'_gear').attr('class','fa fa-gear fa-spin');
                $('#'+itemName+'_content').html(spinner);
            } else {
                $('#'+itemName+'_gear').attr('class','fa fa-gear');
                $('#'+itemName+'_content').html("");
            }
        }

        function ContentsLoader(dataArray,itemName){
            var index=$('#index_number').val();
            var link = "<?php echo Yii::$app->urlManager->createUrl('/disbursement/customer-search/print-document'); ?>";
            var printer = '<a href="'+link+'&option='+itemName+'&index='+index+'" class="btn bg-blue-gradient btn-info pull-right printBtn" id="'+itemName+'_printBtn" value="'+itemName+'" target="_blank"><span class="fa fa-print"></span> PRINT '+itemName.toUpperCase()+'</a>';
            $('#'+itemName+'_content').html("");
            $('#'+itemName+'_print_div').html(printer);
            $('#'+itemName+'_content').html(dataArray[0]);
        }

        function SearchEngine(itemName)
        {
            Animate(itemName); //Starts Animations
            $.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/customer-search/search-lonee'); ?>",
                type:"POST",
                cache:false,
                data:{
                    csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                    option:itemName,
                    search:$('#index_number').val()
                },
                success:function (data)
                {
                    var dataArray = data;
                    Animate(itemName,false); //End Animations
                    if(dataArray.length!==0){
                        ContentsLoader(dataArray,itemName);
                    } else {
                        $("#"+itemName+"_content").html("No data found");
                    }

                }
            })

        }

        function SearchLoanee(){
            var spinner = '<span class="fa fa-spinner fa-2x fa-spin">';
            $("#search_feedback").html(spinner);
            $("#history_table").html('<tr><td colspan="4" style="font-size: large;">'+spinner+' Please Wait ... Loading</td></tr>');
            $.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-suspension/search-lonee'); ?>",
                type:"POST",
                cache:false,
                data:{
                    csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                    search:$('#search').val()
                },
                success:function (data) {
                    //console.log(data);
                    $("#history_table").html('');
                    var dataArray = data.output;
                    var totalAllocation = 0;
                    var totalDisbursement = 0;
                    if (dataArray.length!=0){


                        $("#search_feedback").html('<span class="fa fa-check fa-2x text-green"></span>');
                        $("#search_feedback").html('<span class="fa fa-check fa-2x text-green"></span>');
                        $.each(dataArray,function (index,value) {
                            $('#applicantName').html(value.full_name);
                            $('#regNo').html(value.registration_number);
                            $('#indexNo').html(value.index_number);
                            $('#index_number').val(value.index_number);
                            SearchEngine('statement');
                            SearchEngine('allocation');
                            $('#PHOTO').attr('src','../'+value.photo);
                            $('#sex').html(value.Sex);
                            $('#birthDate').html(value.DOB);
                            $('#programme').html(value.programme_name+' ( <b>'+value.programme_code+'</b>)'+' [ <b>'+value.years_of_study+'</b> years]');
                            $('#institution').html(value.Institution);
                            $('#studyYear').html(value.current_study_year);
                            $('#phone_number').html(value.phone_number);
                            $('#email_address').html(value.email_address);
                            $("#disbursementsuspension-application_id").val(value.application_id);

                            $("#history_table").append(
                                '<tr>' +
                                '<td>'+value.item_name+'</td>' +
                                '<td>'+value.academic_year+'</td>' +
                                '<td class="text-right text-bold">'+commaSeparateNumber((value.allocated_amount*1).toFixed(2))+'</td>' +
                                '<td class="text-right text-bold">'+commaSeparateNumber((value.disbursed_percentage*1).toFixed(2))+'</td>' +
                                '<td class="text-right text-bold">'+commaSeparateNumber((value.disbursed_amount*1).toFixed(2))+'</td>' +
                                '<td class="text-right text-bold">'+commaSeparateNumber(((value.allocated_amount*1)-(value.disbursed_amount*1)).toFixed(2))+'</td>' +


                                '</tr>'
                            );
                            ;
                            totalAllocation+=(value.allocated_amount*1);
                            totalDisbursement+=(value.disbursed_amount*1);
                            $("#total_allocated").html(commaSeparateNumber((totalAllocation*1).toFixed(2)));
                            $("#total_disbursed").html(commaSeparateNumber((totalDisbursement*1).toFixed(2)));
                            $("#total_remainig").html(commaSeparateNumber((totalAllocation-totalDisbursement).toFixed(2)));


                        });

                    }else{
                        $("#search_feedback").html('<span class="fa fa-ban fa-2x text-red"></span>');
                        $('#applicantName').html('');
                        $('#regNo').html('');
                        $('#indexNo').html('');
                        $('#sex').html('');
                        $('#birthDate').html('');
                        $('#programme').html('');
                        $('#institution').html('');
                        $("#disbursementsuspension-application_id").val('');
                        $("#history_table").html('<tr><td colspan="4" style="font-size: large;"><span class="text-red fa fa-ban fa-3x"></span> Record Not Found</td></tr>');

                    }

                }
            });
        }

        function ValidatePercentages(source,destination){
            var currentValue =  $("#"+source).val()*1;
            var destinationValue = (100-currentValue);
            $("#"+destination).val(destinationValue);
        }
        $(document).ready(function () {




            $("#loan_item").hide();

            $("#searchButton").on('click',function () {
                SearchLoanee();
            });

            $("#disbursementsuspension-status_reason").on('change',function () {

                var
                    dataMap= <?php echo json_encode($dataArray); ?>;
                var index = $(this).val();
                //console.log(index);
                $("#reasons_description").html(dataMap[index]);
            });


            $("#disbursementsuspension-suspension_percent").on('change',function () {
                ValidatePercentages('disbursementsuspension-suspension_percent','disbursementsuspension-remaining_percent');
            });

            $("#disbursementsuspension-remaining_percent").on('change',function () {
                ValidatePercentages('disbursementsuspension-remaining_percent','disbursementsuspension-suspension_percent');
            });

            $("#disbursementsuspension-mode").on('change',function () {
                var selection = $(this).val();
                console.log(selection);
                if (selection=='1'){
                    $("#loan_item").hide();
                    var items = <?php echo json_encode(array_keys($itemData)); ?>;
                    $('#disbursementsuspension-loan_item_id').val(items).trigger('change');
                }else {
                    $("#loan_item").show();
                    $("#disbursementsuspension-loan_item_id").val([]);
                    $("#disbursementsuspension-loan_item_id").val('');

                }


            });



        });
    </script>




    <?php  ActiveForm::end();   ?>
</div>
