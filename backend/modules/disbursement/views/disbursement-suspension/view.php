<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\disbursement\Module;
use kartik\tabs\TabsX;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementSuspension */
if (Yii::$app->controller->id=='disbursement-uplifting'){
    $magnitude =$model->remaining_percent==100?"FULL":"PARTIAL";
    $label='Suspensions Uplifting';
}else{
    $magnitude =$model->suspension_percent==100?"FULL":"PARTIAL";
    $label='Disbursement Suspensions';
}

$this->title = $magnitude.' '.Module::SuspensionType($model->status).' OF '.$model->LoanItem;
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-suspension-view">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">

    <p>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

            <?php $view= DetailView::widget([
                'model' => $model,
                'attributes' => [
                 //   'id',
                    //'application_id',
                    [
                           'name'=>'application_id',
                           'value'=>$model->ApplicantInfo,
                           'label'=>'Lonee',
                           'format'=>'raw',
                    ],
                    //'loan_item_id',
                    [
                        'name'=>'loan_item_id',
                        'value'=>$model->LoanItem,
                        'label'=>'Loan Item',
                        'format'=>'raw',
                    ],
                    //'suspension_percent',
                    [
                        'name'=>'suspension_percent',
                        'value'=>$model->suspension_percent,
                        'label'=>'Suspended %',
                        'format'=>['decimal',2],
                    ],
                   // 'remaining_percent',
                    [
                        'name'=>'remaining_percent',
                        'value'=>$model->remaining_percent,
                        'label'=>'Remaining %',
                        'format'=>['decimal',2],
                    ],
                    //'status',
                    [
                        'name'=>'status',
                        'value'=>$model->Status,
                        'label'=>'Operation',
                        'format'=>'raw',
                    ],
                    //'status_reason',
                    [
                        'name'=>'status_reason',
                        'value'=>$model->Reason,
                        'label'=>'Reason',
                        'format'=>'raw',
                    ],
                    'remarks:ntext',
                    //'status_date',
                    [
                        'name'=>'status_date',
                        'value'=>date('d/m/Y',strtotime($model->status_date)),
                        'label'=>'Effective Date',
                        'format'=>'raw',
                    ],
                   // 'status_by',
                    [
                        'name'=>'status_by',
                        'value'=>Module::UserInfo($model->status_by,'fullName'),
                        'label'=>'Personnel Responsible',
                        'format'=>'raw',
                    ],
                   // 'supporting_document:ntext',
                    //'suspension_history:ntext',
                   // 'sync_id',
                ],
            ]);


            echo TabsX::widget([
                'items' => [
                    [
                        'label' => 'Suspension Details',
                        'content' =>$view,
                        'id' => '1',
                    ],
                    [
                        'label' => 'Supporting Documents',
                        'content' => Module::SuspensionEvidence($model->id),
                        'id' => '2',
                    ],
                    [
                        'label' => 'Suspension History',
                        'content' => Module::SuspensionHistory($model->id),
                        'id' => '3',
                    ],
                ],
                'position' => TabsX::POS_ABOVE,
                'bordered' => true,
                'encodeLabels' => false
            ]);

            ?>




        </div>
    </div>



</div>
