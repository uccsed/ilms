<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/26/18
 * Time: 10:57 AM
 */
use backend\modules\disbursement\Module;



$SQL="
SELECT 
disbursement_suspension.status_date AS 'date', 
CONCAT(loan_item.item_name,' ( ',IFNULL(loan_item.item_code,''),' )') AS 'item_name', 
disbursement_suspension.suspension_percent AS 'percent',
disbursement_suspension.status_by AS 'operation_by',
suspension_reason.name AS 'reason'
FROM disbursement_suspension 
LEFT JOIN loan_item ON disbursement_suspension.loan_item_id  = loan_item.loan_item_id
LEFT JOIN suspension_reason ON disbursement_suspension.status_reason  = suspension_reason.id
WHERE disbursement_suspension.application_id = '$application'  AND disbursement_suspension.status='$type'
ORDER BY disbursement_suspension.status_date ASC, disbursement_suspension.loan_item_id ASC ";
$model= Yii::$app->db->createCommand($SQL)->queryAll();
?>

<table class="table table-bordered table-condensed table-striped">
    <thead>
    <tr>
        <th>S/N</th>
        <th>Date</th>
        <th>Item</th>
        <th>Percentage</th>
        <th>Reason</th>
        <th>Suspended By</th>
    </tr>
    </thead>
    <tbody>
        <?php $count = 0; foreach ($model as $index=>$dataArray){$count++; ?>
            <tr>
                <td><?php echo $count; ?></td>
                <td><?php echo date('D d-M-y',strtotime($dataArray['date'])); ?></td>
                <td><?php echo $dataArray['item_name']; ?></td>
                <th><?php echo $dataArray['percent']; ?></th>
                <td><?php echo $dataArray['reason']; ?></td>
                <td><?php echo Module::UserInfo($dataArray['operation_by'],'fullName'); ?></td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>

    </tfoot>
</table>



