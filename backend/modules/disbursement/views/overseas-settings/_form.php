<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\OverseasSettings */
/* @var $form yii\widgets\ActiveForm */
$SQL = "
                        SELECT 
                        item_disbursement_setting.loan_item_id AS 'id', 
                        CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'name'
                        
                        FROM item_disbursement_setting
                        LEFT JOIN loan_item ON item_disbursement_setting.loan_item_id = loan_item.loan_item_id
                        WHERE item_disbursement_setting.study_category = '2';
                ";



$ITEMS = Yii::$app->db->createCommand($SQL)->queryAll();
?>
<!--
<div class="overseas-settings-form">

    <?php /*$form = ActiveForm::begin(); */?>

    <?/*= $form->field($model, 'academic_year')->textInput() */?>

    <?/*= $form->field($model, 'country_id')->textInput() */?>

    <?/*= $form->field($model, 'year_of_study')->textInput() */?>

    <?/*= $form->field($model, 'loan_item')->textInput() */?>

    <?/*= $form->field($model, 'instalment')->textInput() */?>

    <?/*= $form->field($model, 'disbursement_percent')->textInput() */?>

    <?/*= $form->field($model, 'sync_id')->textInput(['maxlength' => true]) */?>

    <div class="form-group">
        <?/*= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) */?>
    </div>

    <?php /*ActiveForm::end(); */?>

</div>-->


<?php $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>


<div class="row">
    <div class="col-md-4">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' =>1,
            'attributes' => [


                'academic_year' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    //  'label' => 'Payment Request',
                    'label' => 'Academic Year',

                    'options' => [
                        'data' => ArrayHelper::map(\common\models\AcademicYear::findBySql("SELECT * FROM academic_year ORDER BY is_current DESC, academic_year DESC")->asArray()->all(),'academic_year_id','academic_year'),
                        'options' => [
                            'prompt' => 'Select Academic Year',
                            //'onchange'=>'updateStatus()'

                        ],
                    ],
                ],



                //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
            ]
        ]);
        ?>
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' =>1,
            'attributes' => [


                'country_id' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    //  'label' => 'Payment Request',
                    'label' => 'Country',

                    'options' => [
                        'data' =>  ArrayHelper::map(\frontend\modules\application\models\Country::findBySql("SELECT * FROM country WHERE country_code <> 'TZA' OR  country_code IS NULL ORDER BY country_name ASC")->asArray()->all(),'country_id','country_name'),
                        'options' => [
                            'prompt' => 'Select Country',
                            'multiple'=>TRUE,

                        ],
                    ],
                ],



                //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
            ]
        ]);
        ?>
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' =>1,
            'attributes' => [


                'year_of_study' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    //  'label' => 'Payment Request',
                    'label' => 'YOS',

                    'options' => [
                        'data' =>  array_combine(range(1,10),range(1,10)),
                        'options' => [
                            'prompt' => 'Select Year of Study',
                            'multiple'=>TRUE,

                        ],
                    ],
                ],



                //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
            ]
        ]);
        ?>



        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' =>1,
            'attributes' => [


                'loan_item' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    //  'label' => 'Payment Request',
                    'label' => 'Loan Item',

                    'options' => [
                        'data' =>  ArrayHelper::map($ITEMS,'id','name'),
                        'options' => [
                            'prompt' => 'Select Item',
                            'multiple'=>TRUE,

                        ],
                    ],
                ],



                //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
            ]
        ]);
        ?>


        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' =>1,
            'attributes' => [


                'instalment' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    //  'label' => 'Payment Request',
                    'label' => 'Installment',

                    'options' => [
                        'data' =>  ArrayHelper::map(\backend\modules\disbursement\models\InstalmentDefinition::findBySql("SELECT instalment_definition_id AS 'id',instalment_desc AS 'name' FROM instalment_definition WHERE is_active = '1' ORDER BY instalment ASC")->asArray()->all(),'id','name'),
                        'options' => [
                            'prompt' => 'Select Instalment',
                           // 'multiple'=>TRUE,

                        ],
                    ],
                ],



                //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
            ]
        ]);
        ?>


        <?= $form->field($model, 'disbursement_percent')->textInput(['type'=>'number','min'=>'0','max'=>'100','class'=>'form-control']) ?>

    </div>

    <div class="col-md-8">

    </div>
</div>


<div class="text-right">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php
    echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
    echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

    ActiveForm::end();
    ?>
</div>