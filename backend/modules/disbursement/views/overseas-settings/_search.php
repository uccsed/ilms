<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\OverseasSettingsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="overseas-settings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'academic_year') ?>

    <?= $form->field($model, 'country_id') ?>

    <?= $form->field($model, 'year_of_study') ?>

    <?= $form->field($model, 'loan_item') ?>

    <?php // echo $form->field($model, 'instalment') ?>

    <?php // echo $form->field($model, 'disbursement_percent') ?>

    <?php // echo $form->field($model, 'sync_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
