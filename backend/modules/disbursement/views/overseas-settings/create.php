<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\OverseasSettings */

$this->title = 'Create Overseas Settings';
$this->params['breadcrumbs'][] = ['label' => 'Overseas Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="overseas-settings-create">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>






        </div>
    </div>
</div>
