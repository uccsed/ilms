<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\OverseasSettings */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Overseas Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="overseas-settings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'academic_year',
            'country_id',
            'year_of_study',
            'loan_item',
            'instalment',
            'disbursement_percent',
            'sync_id',
        ],
    ]) ?>

</div>
