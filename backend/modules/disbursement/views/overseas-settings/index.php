<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
use kartik\widgets\DatePicker;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\OverseasSettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Overseas Settings';
$this->params['breadcrumbs'][] = $this->title;
$ACADEMIC_YEAR = ArrayHelper::map(\common\models\AcademicYear::findBySql("SELECT * FROM academic_year ORDER BY is_current DESC, academic_year DESC")->asArray()->all(),'academic_year_id','academic_year');
$COUNTRY = ArrayHelper::map(\frontend\modules\application\models\Country::findBySql("SELECT * FROM country WHERE country_code <> 'TZA' OR  country_code IS NULL ORDER BY country_name ASC")->asArray()->all(),'country_id','country_name');
$YOS = array_combine(range(1,10),range(1,10));
$SQL = "
                        SELECT 
                        item_disbursement_setting.loan_item_id AS 'id', 
                        CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'name'
                        
                        FROM item_disbursement_setting
                        LEFT JOIN loan_item ON item_disbursement_setting.loan_item_id = loan_item.loan_item_id
                        WHERE item_disbursement_setting.study_category = '2';
                ";



$ITEMS = ArrayHelper::map(Yii::$app->db->createCommand($SQL)->queryAll(),'id','name');
$INSTALMENTS = ArrayHelper::map(\backend\modules\disbursement\models\InstalmentDefinition::findBySql("SELECT instalment_definition_id AS 'id',instalment_desc AS 'name' FROM instalment_definition WHERE is_active = '1' ORDER BY instalment ASC")->asArray()->all(),'id','name');
?>

<div class="overseas-settings-index">
    <div class="panel">
        <div class="panel-heading"><h3 class="text-uppercase"><?= Html::encode($this->title) ?></h3>
            <p>
                <?= Html::a('Create Overseas Settings', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
        <div class="panel-body">
            <?php
            $gridColumns = [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'hAlign' => GridView::ALIGN_CENTER,
                ],
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'allowBatchToggle' => true,
                    'detail' => function ($model,$ACADEMIC_YEAR,$YOS,$ITEMS,$INSTALMENTS) {
                        //return $this->render('shared_list',['batchID'=>$model->id,'status'=>$model->status]);
                        return $this->render('country_list',['country'=>$model->country_id,
                            'ACADEMIC_YEAR'=>$ACADEMIC_YEAR,
                            'YOS'=>$YOS,
                            'ITEMS'=>$ITEMS,
                            'INSTALMENTS'=>$INSTALMENTS
                        ]);
                    },
                    'detailOptions' => [
                        'class' => 'kv-state-enable',
                    ],
                ],

                [
                    'attribute' => 'country_id',
                    'label'=>"Country",
                    'format' => 'raw',
                    //'width'=>'120px',
                    'value' => function ($model) {
                        return $model->country->country_name;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $COUNTRY,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],

                ],

                [
                    'attribute' => 'academic_year',
                    'label'=>"Academic Year",
                    'format' => 'raw',
                    'width'=>'120px',
                    'value' => function ($model) {
                        return $model->academicYear->academic_year;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $ACADEMIC_YEAR,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],

                ],


                [
                    'attribute' => 'year_of_study',
                    'label'=>"YOS",
                    'format' => 'raw',
                    'width'=>'120px',
                    'value' => function ($model) {
                        return $model->year_of_study;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $YOS,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],

                ],
                [
                    'attribute' => 'loan_item',
                    'label'=>"Loan Item",
                    'format' => 'raw',
                   // 'width'=>'120px',
                    'value' => function ($model) {
                        return $model->loanItem->item_name;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $ITEMS,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],

                ],

                [
                    'attribute' => 'instalment',
                    'label'=>"Instalment",
                    'format' => 'raw',
                    'width'=>'120px',
                    'value' => function ($model) {
                        return $model->instalmentDefinition->instalment;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $INSTALMENTS,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],

                ],



                [
                    'attribute' => 'disbursement_percent',
                    'label'=>"Disbursement Percent",
                    'format' => ['decimal',2],
                    'hAlign'=>'right',
                    'width'=>'200px',
                    'value' => function ($model) {
                        return $model->disbursement_percent;
                    },
                    //'pageSummary'=>true,
                ],





// ['class' => 'yii\grid\ActionColumn'],

                /*['class' => 'kartik\grid\ActionColumn',
                    'template'=>'{view}',
                ],*/
            ];
            ?>

            <?php
            /*echo*/ ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,

                'fontAwesome' => true,
//            'asDropdown' => false
                'batchSize' => 50,
                'target' => '_blank',
                'selectedColumns' => [0, 1, 2, 3, 4, 5, 6, 7], // Col seq 2 to 6
                'columnSelectorOptions' => [
                    'label' => 'Export Columns',
                ],
                // 'hiddenColumns' => [15], // SerialColumn, Color, & ActionColumn
                //'disabledColumns' => [0, 1, 2, 3, 4, 5, 6, 9, 12], // ID & Name
                'noExportColumns' => [15],
                'dropdownOptions' => [
                    'label' => 'Export Data',
                    'class' => 'btn btn-default'
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_EXCEL => false,
                    ExportMenu::FORMAT_EXCEL_X => false,
                ],
                //'folder' => '@webroot/tmp', // this is default save folder on server
            ]) . "<hr>\n";
            ?>

            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                //'showPageSummary'=>true,
                'pageSummaryRowOptions'=>['class'=>'text-bold bg-blue-gradient'],
                'pjax'=>true,
                'striped'=>true,
                'hover'=>true,
            ]);

            ?>
        </div>
    </div>
</div>
