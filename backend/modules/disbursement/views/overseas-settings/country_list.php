<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 12/20/18
 * Time: 4:45 PM
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
use kartik\widgets\DatePicker;
use kartik\export\ExportMenu;
use backend\modules\disbursement\models\OverseasSettingsSearch;
$searchModel = new OverseasSettingsSearch();
$dataProvider = $searchModel->SpecialSearch(Yii::$app->request->queryParams,$country);
$dataProvider->pagination=false;
?>

<?php

$columns =  [
    [
        'class' => 'kartik\grid\SerialColumn',
        'hAlign' => GridView::ALIGN_CENTER,
    ],


    /*[
        'attribute' => 'country_id',
        'label'=>"Country",
        'format' => 'raw',
        //'width'=>'120px',
        'value' => function ($model) {
            return $model->country->country_name;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $COUNTRY,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

    ],*/


    [
        'attribute' => 'academic_year',
        'label'=>"Academic Year",
        'format' => 'raw',
        'width'=>'120px',
        'value' => function ($model) {
            return $model->academicYear->academic_year;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $ACADEMIC_YEAR,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

    ],



    [
        'attribute' => 'loan_item',
        'label'=>"Loan Item",
        'format' => 'raw',
        // 'width'=>'120px',
        'value' => function ($model) {
            return $model->loanItem->item_name;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $ITEMS,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

    ],
    [
        'attribute' => 'year_of_study',
        'label'=>"YOS",
        'format' => 'raw',
        'width'=>'120px',
        'value' => function ($model) {
            return $model->year_of_study;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $YOS,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

    ],

    [
        'attribute' => 'instalment',
        'label'=>"Instalment",
        'format' => 'raw',
        'width'=>'120px',
        'value' => function ($model) {
            return $model->instalmentDefinition->instalment;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $INSTALMENTS,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],

    ],



    [
        'attribute' => 'disbursement_percent',
        'label'=>"Disbursement Percent",
        'format' => ['decimal',2],
        'hAlign'=>'right',
        'width'=>'200px',
        'value' => function ($model) {
            return $model->disbursement_percent;
        },
        //'pageSummary'=>true,
    ],




// ['class' => 'yii\grid\ActionColumn'],

    ['class' => 'kartik\grid\ActionColumn',
        'template'=>'{delete}',
    ],
];

 ?>

<?php
/*echo*/ ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,

    'fontAwesome' => true,
//            'asDropdown' => false
    'batchSize' => 50,
    'target' => '_blank',
    'selectedColumns' => [0, 1, 2, 3, 4, 5, 6, 7], // Col seq 2 to 6
    'columnSelectorOptions' => [
        'label' => 'Export Columns',
    ],
    // 'hiddenColumns' => [15], // SerialColumn, Color, & ActionColumn
    //'disabledColumns' => [0, 1, 2, 3, 4, 5, 6, 9, 12], // ID & Name
    'noExportColumns' => [15],
    'dropdownOptions' => [
        'label' => 'Export Data',
        'class' => 'btn btn-default'
    ],
    'exportConfig' => [
        ExportMenu::FORMAT_HTML => false,
        ExportMenu::FORMAT_EXCEL => false,
        ExportMenu::FORMAT_EXCEL_X => false,
    ],
    //'folder' => '@webroot/tmp', // this is default save folder on server
]) . "<hr>\n";
?>

<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $columns,
    //'showPageSummary'=>true,
    'pageSummaryRowOptions'=>['class'=>'text-bold bg-blue-gradient'],
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
]);

?>