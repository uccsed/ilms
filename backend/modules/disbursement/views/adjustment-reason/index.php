<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\AdjustmentReasonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Adjustment Reasons';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="suspension-reason-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Create Adjustment Reason', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,

                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    'description:ntext',

                    [
                        'attribute' => 'is_active',
                        'label'=>"State",
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->is_active=='1'?'ACTIVE':"INACTIVE";
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                        'filter' =>['1'=>'Active','0'=>'Inactive'],
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],


                    //'comment',
                    ['class' => 'yii\grid\ActionColumn',
                        'template'=>'{view}{delete}',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>