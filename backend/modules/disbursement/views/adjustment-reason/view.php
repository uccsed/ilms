<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\AdjustmentReason */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Adjustment Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adjustment-reason-view">

    <div class="panel panel-info">
        <div class="panel-heading">
            <h4><?= Html::encode($this->title) ?></h4>

        </div>
        <div class="panel-body">
            <p class="">
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
                    'name',
                    'description:ntext',

                    //'is_active',
                    [
                        'name'=>'is_active',
                        'value'=>$model->is_active=='1'?'ACTIVE':'INACTIVE',
                        'format'=>'html',
                        'label'=>'State',
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>


