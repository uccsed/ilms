<?php
use backend\modules\disbursement\models\InstitutionPaylist;
use backend\modules\disbursement\models\InstitutionPaylistSearch;

$searchModel = new InstitutionPaylistSearch();
$dataProvider = $searchModel->payListSearch(Yii::$app->request->queryParams,$request);

$user = Yii::$app->user->id;
$uSQL="
SELECT 
user.user_id as 'user_id',
staff.staff_id as 'staff_id',
staff.learning_institution_id as 'institution_id'
FROM user 
LEFT JOIN staff ON user.user_id = staff.user_id
WHERE user.user_id='$user' AND user.login_type='4'";
$uModel = Yii::$app->db->createCommand($uSQL)->queryAll();


if (sizeof($uModel)!=0){
    foreach ($uModel as $uIndex=>$uDataArray){
        $myInstitution = $uDataArray['institution_id'];
        $iSQL = "SELECT * FROM learning_institution WHERE learning_institution_id='$myInstitution'";
    }
}

$ulSQL="
SELECT * FROM user WHERE user.user_id='$user' AND user.login_type='5'";
$ulModel = Yii::$app->db->createCommand($ulSQL)->queryAll();
if (sizeof($ulModel)!=0){
    $liSQL="SELECT loan_item.loan_item_id AS 'loan_item', CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'itemName' FROM loan_item WHERE is_active = '1' AND is_requested = '1'";
    $iSQL = "SELECT * FROM learning_institution WHERE learning_institution_id IN(SELECT institution_assignment.institution_id FROM institution_assignment WHERE institution_assignment.officer_id='$user')";

}

$rSQL="
SELECT 
institution_fund_request.id AS 'id',
CONCAT(learning_institution.institution_name,' | ',loan_item.item_name,' - ',academic_year.academic_year) AS 'name'
  FROM institution_fund_request 
  JOIN learning_institution ON institution_fund_request.institution_id = learning_institution.learning_institution_id
  JOIN loan_item ON institution_fund_request.loan_item = loan_item.loan_item_id
  JOIN academic_year ON institution_fund_request.academic_year = academic_year.academic_year_id
  #WHERE approval='3'";

$liModel=Yii::$app->db->createCommand($liSQL)->queryAll();
$iModel=Yii::$app->db->createCommand($iSQL)->queryAll();
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\InstitutionFundRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary'=>true,
                'pjax'=>true,
                'striped'=>true,
                'hover'=>true,

                'columns' => [
                    ['class'=>'kartik\grid\SerialColumn'],
                    [
                        'attribute'=>'Student',
                        'filter'=>false,
                    ],
                    [
                        'attribute'=>'Programme',
                        'filter'=>false,
                    ],

                    [
                        'attribute' => 'loan_item_id',
                        'label'=>"Loan Item",
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->LoanItem;
                        },
                        'filter'=>false,
                        'format' => 'raw',
                        'pageSummary'=>'TOTAL',
                        'pageSummaryOptions'=>['class'=>'text-right'],
                    ],
                    [
                        'attribute'=>'allocated_amount',
                        'width'=>'150px',
                        'hAlign'=>'right',
                        'format'=>['decimal', 2],
                        'pageSummary'=>true,
                        'filter'=>false,
                    ],



                    //'comment',
                    /*['class' => 'yii\grid\ActionColumn',
                        'template'=>'{view}{delete}',
                    ],*/

                    [
                        'attribute' => 'status',
                        //'width'=>'250px',
                        'label'=>"Status",
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->Status;
                        },
                        'filter'=>false,
                        'format' => 'raw'
                    ],
                ],
            ]); ?>



