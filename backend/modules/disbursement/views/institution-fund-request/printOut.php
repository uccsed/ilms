<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/17/18
 * Time: 2:29 PM
 */
$amount_total=0;
$header_id=1;
$learning_institution_ids_s=0;
$amount_bottom_array=array();
$sn = 0;
$mpdf = new mPDF('c','A4-L','','',5,5,30,25,10,10);
use backend\modules\disbursement\Module;
use Yii;
$requestID = $model->id;


$requestCourse = Module::RequestCourse($requestID);
$html = '';
?>


<?php foreach ($requestCourse as $index=>$dataArray){ $courseID = $dataArray['group_id']; ?>
<?php
$html.= '
<html>
<head>
<style>
body {font-family: sans-serif;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0mm solid #000000;
	border-right: 0mm solid #000000;
}
table thead td { background-color: #EEEEEE;
	text-align: center;
	border: 0mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: right;
	font-weight: bolder;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
</style>
</head>

<body>
';
?>






<?php
$html.='<p style=" font-family: \'Courier New\';">';
$html.='
<h3 style="text-align: center; font-family: \'Courier New\';">HIGHER EDUCATION STUDENTS LOAN BOARD</h3>
       <table class="table table-condensed" style=" font-family: \'Courier New\'; font-size: 4mm;">
        <tr><th colspan="3" style="text-align: center;">INSTITUTION PAYMENT REQUEST FOR '. strtoupper($model->LoanItem).'</th></tr>
        <tr><th colspan="2">Prepared printed on : '. date("l F jS Y H:i:s").'</th> <th>Reference Number : '. $model->id.'</th></tr>
        <tr><th>INSTITUTION '. strtoupper($model->learningInstitution->institution_name).'</th><th> ACADEMIC YEAR '.strtoupper($model->academicYear->academic_year).' </th> <th>'.strtoupper($model->Semester).'</th></tr>
        <tr><th colspan="3">'.$dataArray["group_code"].'</th></tr>
    </table>
';
$html.='</p>';
    $html.=Module::DynamicPayoutRequestList($requestID,$courseID,$sn);
    $sn+=$dataArray['count'];
$html.='<pagebreak>';
    //$mpdf->AddPage();
    //$mpdf->WriteHTML($html);
?>


<?php } ?>

<?php
/*
//SUM_TABLE
$TABLE =Module::PayListSummary('SUM_TABLE',$batchID);
$tbl = '<table width="100%" style=" font-family: \'Courier New\';">';
$tbl.='

<tr><th colspan="3" style="text-align: center; font-weight: bolder; font-family: \'Courier New\';" align="center">PAYOUT LIST GRAND SUMMARY</th></tr>
<tr>
<th class="totals" style="text-align: left; font-weight: bolder; font-family: \'Courier New\';" align="left">ITEM</th>
<th class="totals" style="text-align: right; font-weight: bolder; font-family: \'Courier New\';" align="right">LOANEES</th>
<th class="totals" style="text-align: right; font-weight: bolder; font-family: \'Courier New\';" align="right">AMOUNT</th>
</tr>

';
//$tbl.= '<tbody>';
$grandL=$grandA = 0;
foreach ($TABLE as $indx=>$queryArray) {
    $grandL+=$queryArray["count"];
    $grandA+= $queryArray["amount"];
    $tbl.= '<tr>';
    $tbl.= '<th class="totals" style="text-align: left; font-weight: bolder; font-family: \'Courier New\';" align="left">'.$queryArray["item"].'</th>';
    $tbl.= '<th class="totals" style="text-align: right; font-weight: bolder; font-family: \'Courier New\';" align="right">'.number_format($queryArray["count"],0).'</th>';
    $tbl.= '<th class="totals" style="text-align: right; font-weight: bolder; font-family: \'Courier New\';" align="right">'.number_format($queryArray["amount"],2).'</th>';
    $tbl.= '</tr>';
}
//$tbl.= '</tbody>';
$tbl.='

<tr>
<th class="totals" style="text-align: right; font-family: \'Courier New\';" align="right">GRAND TOTAL</th>
<th class="totals" style="text-align: right;  font-family: \'Courier New\';" align="right">'.number_format($grandL,0).'</th>
<th class="totals" style="text-align: right;  font-family: \'Courier New\';" align="right">'.number_format($grandA,2).'</th>
</tr>

';

$tbl.= '</table>';
$html.='<p style=" font-family: \'Courier New\';">';
$html.='
<h3 style="text-align: center; font-family: \'Courier New\';">HIGHER EDUCATION STUDENTS LOAN BOARD</h3>
      <table width="100%" style=" font-family: \'Courier New\';">
        <tr><th colspan="3" style="text-align: center; font-family: \'Courier New\';">LOAN PAYOUT SCHEDULE FOR '.strtoupper($model->Items).'</th></tr>
        <tr><th colspan="2" style="text-align: center; font-family: \'Courier New\';">Schedule printed on : '. date("l F jS Y H:i:s").'</th> <th>Payout Reference Number : '.$model->batch_number.'</th></tr>
        <tr><th style=" font-family: \'Courier New\';">INSTITUTION: '.strtoupper($model->learningInstitution->institution_code).'</th><th> ACADEMIC YEAR: '. strtoupper($model->academicYear->academic_year).' </th> <th>'. strtoupper($model->Instalment).'</th></tr>
        <tr><th colspan="3" style="text-align: left; font-family: \'Courier New\';">GRAND SUMMARY</th></tr>
    </table>
';
$html.='</p>';
$html.=$tbl;
*/
?>

<?php
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle("heslb");
$mpdf->SetAuthor("heslb");
$mpdf->SetWatermarkText("HESLB");
$mpdf->showWatermarkText = true;
  /*  $mpdf->SetWatermarkImage('../images/background.jpg');
    $mpdf->showWatermarkImage = true;*/
$mpdf->watermark_font = 'DejaVuSansCondensed';
$mpdf->watermarkTextAlpha = 0.1;
$mpdf->SetDisplayMode('fullpage');
$mpdf->setFooter('|{PAGENO} of {nbpg}|');

?>


<?php
$html.= '
</body>
</html>
';
?>
<?php
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

exit;
?>
