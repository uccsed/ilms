<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/6/18
 * Time: 8:01 PM
 */
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use kartik\widgets\FileInput;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */
/* @var $form yii\widgets\ActiveForm */

/*echo Module::FetchBootstrap('fancyInputs');
echo Module::FetchBootstrap('dashboard');*/
echo Module::FetchBootstrap('js');
?>

<?php $form = ActiveForm::begin([
    'options'=>['enctype'=>'multipart/form-data'], // important
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>
<div class="fund-request-approval-form">
    <div class="row">
        <div class="col-md-6">
            <?php
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' =>1,
                'attributes' => [


                    'approval' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        'label' => 'Approval/Denial',
                        'options' => [
                            'data' => ['3'=>'APPROVED','4'=>'DENIED'],
                            'options' => [
                                'prompt' => 'Select',

                            ],
                        ],
                    ],



                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                ]
            ]);
            ?>

            <?= $form->field($model, 'invoice_number')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'control_number')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'approval_comments')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?php
                echo $form->field($model, 'file')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'pdf/*'],
                   // 'options' => ['accept' => '*'],
                ]);
                ?>

            </div>



            <div style="display: none;">
                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                <?= $form->field($model, 'loan_item')->textInput() ?>
                <?= $form->field($model, 'academic_year')->textInput() ?>
                <?= $form->field($model, 'institution_id')->textInput() ?>


                <?= $form->field($model, 'submitted')->textInput() ?>

                <?= $form->field($model, 'submitted_on')->textInput() ?>

                <?= $form->field($model, 'submitted_by')->textInput() ?>



                <?= $form->field($model, 'approval_date')->textInput(['value'=>date('Y-m-d H:i:s')]) ?>

                <?= $form->field($model, 'approved_by')->textInput(['value'=>Yii::$app->user->id]) ?>

                <?= $form->field($model, 'request_status')->textInput() ?>

                <?= $form->field($model, 'status_date')->textInput() ?>

                <?= $form->field($model, 'status_by')->textInput() ?>
            </div>


            <div class="text-right">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

                <?php
                echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
                echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

                ActiveForm::end();
                ?>
            </div>
        </div>
    </div>


</div>