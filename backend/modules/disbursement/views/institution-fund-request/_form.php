<?php


$user = Yii::$app->user->id;
$uSQL="
SELECT 
user.user_id as 'user_id',
staff.staff_id as 'staff_id',
staff.learning_institution_id as 'institution_id'
FROM user 
LEFT JOIN staff ON user.user_id = staff.user_id
WHERE user.user_id='$user' AND user.login_type='4'";
$uModel = Yii::$app->db->createCommand($uSQL)->queryAll();


if (sizeof($uModel)!=0){
    foreach ($uModel as $uIndex=>$uDataArray){
        $myInstitution = $uDataArray['institution_id'];
        $iSQL = "SELECT * FROM learning_institution WHERE learning_institution_id='$myInstitution'";
    }
}

$ulSQL="
SELECT * FROM user WHERE user.user_id='$user' AND user.login_type='5'";
$ulModel = Yii::$app->db->createCommand($ulSQL)->queryAll();
if (sizeof($ulModel)!=0){
    $liSQL="SELECT loan_item.loan_item_id AS 'loan_item', CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'itemName' FROM loan_item WHERE is_active = '1' AND is_requested = '1'";
    $iSQL = "SELECT * FROM learning_institution WHERE learning_institution_id IN(SELECT institution_assignment.institution_id FROM institution_assignment WHERE institution_assignment.officer_id='$user')";

}

$liModel=Yii::$app->db->createCommand($liSQL)->queryAll();
$iModel=Yii::$app->db->createCommand($iSQL)->queryAll();




/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\InstitutionFundRequest */
/* @var $form yii\widgets\ActiveForm */

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */
/* @var $form yii\widgets\ActiveForm */

echo Module::FetchBootstrap('fancyInputs');
echo Module::FetchBootstrap('dashboard');
?>

<?php $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>
<div class="institution-fund-request-form">

    <div class="row">
        <div class="col-md-6">
            <?php
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' =>1,
                'attributes' => [


                    'loan_item' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        'label' => 'Loan Item',
                        'options' => [
                            'data' => ArrayHelper::map($liModel, 'loan_item', 'itemName'),
                            'options' => [
                                'prompt' => 'Select',

                            ],
                        ],
                    ],

                    'academic_year' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        'label' => 'Academic Year',
                        'options' => [
                            // 'data' => ArrayHelper::map(\common\models\AcademicYear::findAll(), 'academic_year_id', 'academic_year'),
                            'data' => ArrayHelper::map(\common\models\AcademicYear::findBySql('SELECT * FROM academic_year ORDER BY is_current DESC')->asArray()->all(), 'academic_year_id', 'academic_year'),
                            'options' => [
                                'prompt' => 'Select',

                            ],
                        ],
                    ],

                    'semester_number' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        'label' => 'Semester',
                        'options' => [
                            // 'data' => ArrayHelper::map(\common\models\AcademicYear::findAll(), 'academic_year_id', 'academic_year'),
                            'data' => ArrayHelper::map(\common\models\Semester::findBySql('SELECT * FROM semester WHERE is_active = "1" ORDER BY semester_number ASC ')->asArray()->all(), 'semester_id', 'semester_number'),
                            'options' => [
                                'prompt' => 'Select',

                            ],
                        ],
                    ],

                    'institution_id' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        'label' => 'Learning Institution',
                        'options' => [

                            'data' => ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql($iSQL)->asArray()->all(), 'learning_institution_id', 'institution_name'),
                            'options' => [
                                'prompt' => 'Select',

                            ],
                        ],
                    ],

                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                ]
            ]);
            ?>










            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>

        <div class="col-md-6">
            <div class="panel">
                <div class="panel-heading"><h4 class="text-bold text-uppercase"><span id="academicYearAS"></span> Allocated Students</h4></div>
                <div class="panel-info">
                    <table class="table table-bordered table-condensed table-stripped">
                        <thead class="bg-primary">
                        <tr class="text-uppercase">
                            <th class="text-center">Year of Study</th>
                            <th class="text-center">Total Students</th>
                            <th class="text-center">Total Allocation</th>
                        </tr>
                        </thead>
                        <tbody id="allocation"></tbody>
                    </table>
                </div>

            </div>

           <!-- <div class="panel">
                <div class="panel-heading"><h4 class="text-bold text-uppercase"><span id="academicYearRI"></span> Reported to <span id="institute_name">Institute</span></h4></div>
                <div class="panel-info">
                    <table class="table table-bordered table-condensed table-stripped">
                        <thead class="bg-orange">
                        <tr class="text-uppercase">
                            <th class="text-center">Year of Study</th>
                            <th class="text-center">Total Students</th>
                            <th class="text-center">Total Allocation</th>
                        </tr>
                        </thead>
                        <tbody id="reported"></tbody>
                    </table>
                </div>

            </div>-->



           <!-- <div class="panel">
                <div class="panel-heading"><h4 class="text-bold text-uppercase"><span id="academicYearBR"></span> Batch Ready </h4></div>
                <div class="panel-info">
                    <table class="table table-bordered table-condensed table-stripped">
                        <thead class="bg-purple">
                        <tr class="text-uppercase">
                            <th class="text-center">Year of Study</th>
                            <th class="text-center">Total Students</th>
                            <th class="text-center">Total Allocation</th>
                        </tr>
                        </thead>
                        <tbody id="batch_ready"></tbody>
                    </table>
                </div>

            </div>-->


            <div class="panel">
                <div class="panel-heading"><h4 class="text-bold text-uppercase">To be included in this Request</h4></div>
                <div class="panel-info">
                    <table class="table table-bordered table-condensed table-stripped">
                        <thead class="bg-green">
                        <tr class="text-uppercase">
                            <th class="text-center">Year of Study</th>
                            <th class="text-center">Total Students</th>
                            <th class="text-center">Total Allocation</th>
                        </tr>
                        </thead>
                        <tbody id="qualified_request"></tbody>
                    </table>
                </div>

            </div>


        </div>
    </div>




    <div style="display: none;">
        <?= $form->field($model, 'invoice_number')->hiddenInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'submitted')->textInput() ?>

        <?= $form->field($model, 'submitted_on')->textInput() ?>

        <?= $form->field($model, 'submitted_by')->textInput() ?>

        <?= $form->field($model, 'approval')->textInput() ?>

        <?= $form->field($model, 'approval_comments')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'approval_date')->textInput() ?>

        <?= $form->field($model, 'approved_by')->textInput() ?>

        <?= $form->field($model, 'request_status')->textInput() ?>

        <?= $form->field($model, 'status_date')->textInput() ?>

        <?= $form->field($model, 'status_by')->textInput() ?>
    </div>





    <script>
        function commaSeparateNumber(val) {
            while (/(\d+)(\d{3})/.test(val.toString())) {
                val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            }
            return val;
        }

        //institutionfundrequest-loan_item
        //institutionfundrequest-institution_id
        //institutionfundrequest-academic_year


        function PaylistAnalysis() {
            var LoanItem = $("#institutionfundrequest-loan_item").val();
            var Institution = $("#institutionfundrequest-institution_id").val();
            var AcademicYear = $("#institutionfundrequest-academic_year").val();
            var Semester = $("#institutionfundrequest-semester_number").val();
            var DataArray={'1':'st','2':'nd','3':'rd','4':'th','5':'th','6':'th','7':'th','8':'th','9':'th','10':'th'};

            if (LoanItem !=="" && Institution !=="" && AcademicYear !==""){
                var spinner = "<tr><td colspan='3'><div style='font-size: large;'><span class='fa fa-spinner fa-spin fa-2x'></span> Please Wait... Loading Data...</div></td></tr>";
                $("#allocation").html(spinner);
                //$("#reported").html(spinner);
               // $("#batch_ready").html(spinner);
                $("#qualified_request").html(spinner);

                $.ajax({
                    url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/institution-fund-request/generate'); ?>",
                    type:"POST",
                    cache:false,
                    data:{
                        _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                        institution:Institution,
                        loan_item:LoanItem,
                        academic_year:AcademicYear,
                        semester_id:Semester
                    },
                    success:function (data) {
                        //console.log(data);
                        var allocation = data.output['allocation'];
                        var allocationTOTAL = 0;
                        var allocationStudentsTOTAL = 0;

                        /*
                        var reported = data.output['reported'];
                        var reportedTOTAL = 0;
                        var reportedStudentsTOTAL = 0;

                        var batchReady = data.output['batchReady'];
                        var batchReadyTOTAL = 0;
                        var batchReadyStudentsTOTAL = 0;
                        */


                        var paylist = data.output['paylist'];
                        var paylistTOTAL = 0;
                        var paylistStudentsTOTAL = 0;

                        $("#allocation").html("");
                        $("#reported").html("");
                        $("#batch_ready").html("");
                        $("#qualified_request").html("");

                        $.each(allocation,function (alindx,alValue) {
                            allocationTOTAL+=(alValue.allocated_amount*1);
                            allocationStudentsTOTAL+=(alValue.students*1);

                            $("#allocation").append(
                                "<tr>" +
                                    "<td class='text-center text-bold'>"+alValue.current_study_year+"<sup>"+DataArray[alValue.current_study_year]+"</sup> Year</td>" +
                                    "<td class='text-right text-bold'>"+commaSeparateNumber(alValue.students)+"</td>" +
                                    "<td class='text-right text-bold'>"+commaSeparateNumber(alValue.allocated_amount)+"</td>" +
                                "</tr>"
                            );
                        });

                        $("#allocation").append(
                            "<tr class='bg-primary'>" +
                            "<td class='text-center text-bold'>TOTAL</td>" +
                            "<td class='text-right text-bold'>"+commaSeparateNumber(allocationStudentsTOTAL)+"</td>" +
                            "<td class='text-right text-bold'>"+commaSeparateNumber(allocationTOTAL)+"</td>" +
                            "</tr>"
                        );


                        /*

                        $.each(reported,function (rpindx,rpValue) {
                            reportedTOTAL+=(rpValue.allocated_amount*1);
                            reportedStudentsTOTAL+=(rpValue.students*1);

                            $("#reported").append(
                                "<tr>" +
                                "<td class='text-center text-bold'>"+rpValue.current_study_year+"<sup>"+DataArray[rpValue.current_study_year]+"</sup> Year</td>" +
                                "<td class='text-right text-bold'>"+commaSeparateNumber(rpValue.students)+"</td>" +
                                "<td class='text-right text-bold'>"+commaSeparateNumber(rpValue.allocated_amount)+"</td>" +
                                "</tr>"
                            );
                        });

                        $("#reported").append(
                            "<tr class='bg-orange'>" +
                            "<td class='text-center text-bold'>TOTAL</td>" +
                            "<td class='text-right text-bold'>"+commaSeparateNumber(reportedStudentsTOTAL)+"</td>" +
                            "<td class='text-right text-bold'>"+commaSeparateNumber(reportedTOTAL)+"</td>" +
                            "</tr>"
                        );
                        */

                        /*
                        $.each(batchReady,function (brindx,brValue) {
                            batchReadyTOTAL+=(brValue.allocated_amount*1);
                            batchReadyStudentsTOTAL+=(brValue.students*1);

                            $("#batch_ready").append(
                                "<tr>" +
                                "<td class='text-center text-bold'>"+brValue.current_study_year+"<sup>"+DataArray[brValue.current_study_year]+"</sup> Year</td>" +
                                "<td class='text-right text-bold'>"+commaSeparateNumber(brValue.students)+"</td>" +
                                "<td class='text-right text-bold'>"+commaSeparateNumber(brValue.allocated_amount)+"</td>" +
                                "</tr>"
                            );
                        });

                        $("#batch_ready").append(
                            "<tr class='bg-purple'>" +
                            "<td class='text-center text-bold'>TOTAL</td>" +
                            "<td class='text-right text-bold'>"+commaSeparateNumber(batchReadyStudentsTOTAL)+"</td>" +
                            "<td class='text-right text-bold'>"+commaSeparateNumber(batchReadyTOTAL)+"</td>" +
                            "</tr>"
                        );
                        */

                        $.each(paylist,function (plstindx,plstValue) {
                            paylistTOTAL+=(plstValue.allocated_amount*1);
                            paylistStudentsTOTAL+=(plstValue.students*1);

                            $("#qualified_request").append(
                                "<tr>" +
                                "<td class='text-center text-bold'>"+plstValue.current_study_year+"<sup>"+DataArray[plstValue.current_study_year]+"</sup> Year</td>" +
                                "<td class='text-right text-bold'>"+commaSeparateNumber(plstValue.students)+"</td>" +
                                "<td class='text-right text-bold'>"+commaSeparateNumber(plstValue.allocated_amount)+"</td>" +
                                "</tr>"
                            );
                        });

                        $("#qualified_request").append(
                            "<tr class='bg-green'>" +
                            "<td class='text-center text-bold'>TOTAL</td>" +
                            "<td class='text-right text-bold'>"+commaSeparateNumber(paylistStudentsTOTAL)+"</td>" +
                            "<td class='text-right text-bold'>"+commaSeparateNumber(paylistTOTAL)+"</td>" +
                            "</tr>"
                        );






                    }
                });

            }
        }


        $(document).ready(function () {
            $("#institutionfundrequest-institution_id").on("change",function () {

                var instituteName = $("#institutionfundrequest-institution_id option:selected").text();
                $("#institute_name").html(instituteName);
                PaylistAnalysis();
            });

            $("#institutionfundrequest-loan_item").on("change",function () {
                PaylistAnalysis();
            });

            $("#institutionfundrequest-academic_year").on("change",function () {

                var academicYear = $("#institutionfundrequest-academic_year option:selected").text();
                $("#academicYearAS").html(academicYear);
                //$("#academicYearBR").html(academicYear);
                $("#academicYearRI").html(academicYear);

                PaylistAnalysis();
            });


        });
    </script>






    <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?php
        echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
        echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

        ActiveForm::end();
        ?>
    </div>

</div>
