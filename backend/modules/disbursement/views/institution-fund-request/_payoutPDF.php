<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/17/18
 * Time: 11:19 AM
 */
use backend\modules\disbursement\Module;
use yii\helpers\Html;
use Yii;
$requestID = $model->id;

$sn = 0;
$batchCourse = Module::RequestCourse($requestID);
?>
<style>
    body {font-family: sans-serif;
        font-size: 10pt;
    }
    p {	margin: 0pt; }
    table.items {
        border: 0mm solid #000000;
    }
    td { vertical-align: top; }
    .items td {
        border-left: 0mm solid #000000;
        border-right: 0mm solid #000000;
    }
    table thead td { background-color: #EEEEEE;
        text-align: center;
        border: 0mm solid #000000;
        font-variant: small-caps;
    }
    .items td.blanktotal {
        background-color: #EEEEEE;
        border: 0.1mm solid #000000;
        background-color: #FFFFFF;
        border: 0mm none #000000;
        border-top: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
    }
    .items td.totals {
        text-align: right;
        font-weight: bolder;
        border: 0.1mm solid #000000;
    }
    .items td.cost {
        text-align: "." center;
    }
</style>

<?php foreach ($batchCourse as $index=>$dataArray){ $courseID = $dataArray['group_id']; ?>

    <p style=" font-family: 'Courier New';">
        <table class="table table-condensed" style=" font-family: 'Courier New'; font-size: 4mm;">
        <tr><th colspan="3" style="text-align: center;">INSTITUTION PAYMENT REQUEST FOR <?php echo strtoupper($model->LoanItem); ?></th></tr>
        <tr><th colspan="2">Prepared printed on : <?php echo date('l F jS Y H:i:s'); ?></th> <th>Reference Number : <?php echo $model->id; ?></th></tr>
        <tr><th>INSTITUTION <?php echo strtoupper($model->learningInstitution->institution_name); ?></th><th> ACADEMIC YEAR <?php echo strtoupper($model->academicYear->academic_year); ?> </th> <th><?php echo strtoupper($model->Semester); ?></th></tr>
        <tr><th colspan="3"><?php echo $dataArray['group_code']; ?></th></tr>
    </table>
    </p>
<?php echo $TableModel = Module::DynamicPayoutRequestList($requestID,$courseID,$sn,false); ?>
    <?php echo '<hr>'; $sn+=$dataArray['count']; ?>
<?php } ?>
<?php



?>

