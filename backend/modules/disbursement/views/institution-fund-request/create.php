<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\InstitutionFundRequest */

$this->title = 'Create Institution Fund Request';
$this->params['breadcrumbs'][] = ['label' => 'Institution Fund Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-fund-request-create">
    <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
