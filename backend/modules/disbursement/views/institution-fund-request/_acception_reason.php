<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/12/18
 * Time: 5:54 PM
 */

/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/6/18
 * Time: 5:05 PM
 */
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\disbursement\Module;

?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [

        [
            'name'=>'acceptance',
            'value'=>Module::PaymentStatus($model->acceptance),
            'format'=>'raw',
            'label'=>'Acceptance',
        ],

        [
            'name'=>'acceptance_date',
            'value'=>date('d/m/Y',strtotime($model->acceptance_date)),
            'format'=>'raw',
            'label'=>'DATE',
        ],

        [
            'name'=>'acceptance_by',
            'value'=>Module::UserInfo($model->acceptance_by,"fullName"),
            'format'=>'raw',
            'label'=>Module::PaymentStatus($model->acceptance).' BY',
        ],

        [
            'name'=>'acceptance_remarks',
            'value'=>$model->acceptance_remarks,
            'format'=>'raw',
            'label'=>'Acceptance Remarks',
        ],



        /*'submitted',
        'submitted_on',
        'submitted_by',
        'approval',
        'approval_comments:ntext',
        'approval_date',
        'approved_by',
        acceptance
        acceptance_date
        acceptance_by
        acceptance_remarks

        'request_status',
        'status_date',
        'status_by',*/
    ],
]) ?>


