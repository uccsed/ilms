<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 6/13/18
 * Time: 12:32 PM
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\models\DisbursementPlan;
use backend\modules\disbursement\Module;

$searchModel = new DisbursementPlan();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams,$institution);
$dataProvider->pagination=false;
?>


<?php

$columns = [
    ['class'=>'kartik\grid\SerialColumn'],


    [
        'attribute' => 'academic_year',
        'label'=>"Academic Year",
        'format' => 'raw',
        //'value' =>'Institution',
        'value' => function ($model) {
            return $model->AcademicYear;
        },
        'filterType' => GridView::FILTER_SELECT2,
        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
        'filter' =>ArrayHelper::map(\common\models\AcademicYear::findBySql('SELECT * FROM academic_year  ORDER BY is_current DESC')->asArray()->all(), 'academic_year_id', 'academic_year'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],
        'format' => 'raw'
    ],
/*    [
        'attribute' => 'learning_institution',
        'label'=>"Institution",
        'format' => 'raw',
        //'value' =>'Institution',
        'value' => function ($model) {
            return $model->Institution;
        },
        'filterType' => GridView::FILTER_SELECT2,
        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
        'filter' =>ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql(Module::InstitutionsQuery())->asArray()->all(), 'id', 'name'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],
        'format' => 'raw'
    ],*/


    [
        'attribute' => 'programme_id',
        'label'=>"Programme",
        'format' => 'raw',
        'value' => 'Programme',
        'filterType' => GridView::FILTER_SELECT2,
        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
        'filter' =>ArrayHelper::map(\backend\modules\allocation\models\Programme::findBySql(Module::ProgrammesQuery())->asArray()->all(), 'id', 'name'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],
        'format' => 'raw'
    ],

    [
        'attribute' => 'year_of_study',
        'label'=>"Year of Study",
        'format' => 'raw',
        'value' => 'year_of_study',
        'filterType' => GridView::FILTER_SELECT2,
        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
        'filter' =>range(1,10),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],
        'format' => 'raw'
    ],

    //year_of_study

    [
        'attribute' => 'semester_number',
        'label'=>"Semester",
        'format' => 'raw',
        //'value' =>'Institution',
        'value' => function ($model) {
            return $model->Semester;
        },
        'filterType' => GridView::FILTER_SELECT2,
        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
        'filter' =>ArrayHelper::map(\common\models\Semester::findBySql('SELECT * FROM semester WHERE  is_active="1" ORDER BY semester_number ASC')->asArray()->all(), 'semester_id', 'semester_number'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],
        'format' => 'raw'
    ],



    [
        'attribute' => 'instalment_id',
        'label'=>"Instalment",
        'format' => 'raw',
        //'value' =>'Institution',
        'value' => function ($model) {
            return $model->Instalment;
        },
        'filterType' => GridView::FILTER_SELECT2,
        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
        'filter' =>ArrayHelper::map(\backend\modules\disbursement\models\Instalment::findBySql('SELECT * FROM instalment_definition WHERE is_active="1" ORDER BY instalment ASC')->asArray()->all(), 'instalment_definition_id', 'instalment'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],
        'format' => 'raw'
    ],

    [
        'attribute' => 'loan_item_id',
        'label'=>"Loan Item",
        'format' => 'raw',
        //'value' =>'Institution',
        'value' => function ($model) {
            return $model->LoanItem;
        },
        'filterType' => GridView::FILTER_SELECT2,
        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
        'filter' =>ArrayHelper::map(\backend\modules\allocation\models\LoanItem::findBySql('SELECT loan_item_id AS "id", CONCAT(loan_item.item_name," (",loan_item.item_code,")") AS "Name" FROM loan_item ORDER BY item_name ASC')->asArray()->all(), 'id', 'Name'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Search  '],
        'format' => 'raw'
    ],





    [
        'attribute'=>'disbursement_percent',
        'width'=>'150px',
        'hAlign'=>'right',
        'label'=>"Percent (%)",
        'format'=>['decimal', 0],
        'pageSummary'=>true
    ],

    ['class' => 'kartik\grid\ActionColumn',
        'template'=>'{view}{delete}',
        'buttons' => [
           /* 'update' => function ($url,$model) {
                return Html::a(
                    '<span class="fa fa-pencil" title="Edit"></span>',
                    Yii::$app->urlManager->createAbsoluteUrl(array('disbursement/institutional-plan/update','id'=>$model->disbursement_plan_id)));
            },*/
            /*   'view' => function ($url,$model,$key) {
            return Html::a('<span class="green">View Detail</span>', $url);
            },*/
            'delete' => function ($url,$model) {
                return Html::a(
                    '<span class="fa fa-trash" title="Delete"></span>',
                    Yii::$app->urlManager->createAbsoluteUrl(array('disbursement/institutional-plan/delete','id'=>$model->disbursement_plan_id)));
            },

        ],
    ],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'columns' => $columns,
    'panel'=>['class'=>'primary']
]); ?>

  