<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/3/18
 * Time: 3:12 PM
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */

$this->title = $model->Institution; #.' Assignment to '.$model->Officer;
$this->params['breadcrumbs'][] = ['label' => 'Institutional Disbursement Plan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-assignment-view">
    <div class="institution-assignment-index">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">

                <p>

                    <?= Html::a('Delete', ['delete', 'id' => $model->disbursement_plan_id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>

                    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-warning']); ?>

                </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                   // 'Officer',
                    'AcademicYear',
                    'Institution',
                    'Programme',
                    //'Semester',
                    [
                        'name'=>'Semester',
                        'value'=>$model->Semester,
                        'format' => 'html',
                        'label'=>'Semester',
                    ],

                    [
                        'name'=>'Instalment',
                        'value'=>$model->Instalment,
                        'format' => 'html',
                        'label'=>'Instalment',
                    ],
                   // 'Instalment',
                    'LoanItem',
                    [
                            'name'=>'disbursement_percent',
                            'value'=>number_format($model->disbursement_percent).'%',
                            'type'=>'raw',
                            'label'=>'Percent',
                    ],
                   // 'disbursement_percent',
                    //'Institution',
                   /* 'sync_id',
                    'assigned_by',
                    'assigned_on',*/
                ],
            ]) ?>
            </div>
</div>