<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 6/13/18
 * Time: 2:31 PM
 */


use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;



$this->title = 'Create a new Institutional Disbursement Plan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-batch-index">
    <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">

            <div class="pull-left" id="top_operators">
                <!--<div class="row">
                    <div class="col-md-4"><a href="<?php /*echo url::to(['disbursement-plan/default']); */?>" class="btn btn-primary pull-left"><i class="icon-plus"></i> Default Plan</a></div>
                    <div class="col-md-4"><a href="<?php /*echo url::to(['disbursement-plan/create']); */?>" class="btn btn-primary pull-left"><i class="icon-plus"></i> Institutional Plan</a></div>
                    <div class="col-md-4"><a href="<?php /*echo url::to(['disbursement-plan/clone']); */?>" class="btn btn-warning pull-left"><i class="icon-plus"></i> Clone from Existing</a></div>
                </div>-->
            </div>

            <?= $this->render('_form') ?>


        </div>

    </div>
</div>