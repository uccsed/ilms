<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 6/13/18
 * Time: 12:32 PM
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
use kartik\widgets\DatePicker;
use kartik\export\ExportMenu;
use yii\helpers\Url;

$this->title = 'List of Institutional Disbursement Plans';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="disbursement-batch-index">
 <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">

<div class="row">
    <div class="pull-left" id="top_operators">
        <div class="row pull-right">
            <!-- <div class="col-md-4"><a href="<?php /*echo url::to(['institutional-plan/default']); */?>" class="btn btn-primary pull-left"><i class="icon-plus"></i> Default Plan</a></div>
                    --><div class="col-md-4"><a href="<?php echo url::to(['institutional-plan/create']); ?>" class="btn btn-success"> Create Plan</a></div>
            <!--<div class="col-md-4"><a href="<?php /*echo url::to(['institutional-plan/clone']); */?>" class="btn btn-warning pull-left"><i class="icon-plus"></i> Clone from Existing</a></div>
               --> </div>
    </div>
</div>


            <?php
            $gridColumns = [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'hAlign' => GridView::ALIGN_CENTER,
                ],
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'allowBatchToggle' => true,
                    'detail' => function ($model) {
                        return $this->render('_shared_grid',['institution'=>$model->learning_institution]);
                    },
                    'detailOptions' => [
                        'class' => 'kv-state-enable',
                    ],
                ],
                [
                    'attribute' => 'academic_year',
                    'label'=>"Academic Year",
                    'format' => 'raw',
                    //'value' =>'Institution',
                    'value' => function ($model) {
                        return $model->AcademicYear;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                    'filter' =>ArrayHelper::map(\common\models\AcademicYear::findBySql('SELECT * FROM academic_year  ORDER BY is_current DESC')->asArray()->all(), 'academic_year_id', 'academic_year'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'learning_institution',
                    'label'=>"Institution",
                    'format' => 'raw',
                    //'value' =>'Institution',
                    'value' => function ($model) {
                        return $model->Institution;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                    'filter' =>ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql(Module::InstitutionsQuery())->asArray()->all(), 'id', 'name'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],
                    'format' => 'raw'
                ],


                [
                    'attribute' => 'programme_id',
                    'label'=>"Programme",
                    'format' => 'raw',
                    //'value' => 'Programme',
                    'value' => function(){
                        return '-';
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                    'filter' =>ArrayHelper::map(\backend\modules\allocation\models\Programme::findBySql(Module::ProgrammesQuery())->asArray()->all(), 'id', 'name'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'year_of_study',
                    'label'=>"Year of Study",
                    'format' => 'raw',
                    'value' => function(){
                       return '-';
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                    'filter' =>array_combine(array_values(range(1,10)),array_values(range(1,10))),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],
                    'format' => 'raw'
                ],

                [
                    'attribute' => 'semester_number',
                    'label'=>"Semester",
                    'format' => 'raw',
                    //'value' =>'Institution',
                    /*'value' => function ($model) {
                        return $model->Semester;
                    },*/
                    'value' => function(){
                        return '-';
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                    'filter' =>ArrayHelper::map(\common\models\Semester::findBySql('SELECT * FROM semester WHERE  is_active="1" ORDER BY semester_number ASC')->asArray()->all(), 'semester_id', 'semester_number'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],
                    'format' => 'raw'
                ],



                [
                    'attribute' => 'instalment_id',
                    'label'=>"Instalment",
                    'format' => 'raw',
                    //'value' =>'Institution',
                   /* 'value' => function ($model) {
                        return $model->Instalment;
                    },*/
                    'value' => function(){
                        return '-';
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                    'filter' =>ArrayHelper::map(\backend\modules\disbursement\models\Instalment::findBySql('SELECT * FROM instalment_definition WHERE is_active="1" ORDER BY instalment ASC')->asArray()->all(), 'instalment_definition_id', 'instalment'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],
                    'format' => 'raw'
                ],

                [
                    'attribute' => 'loan_item_id',
                    'label'=>"Loan Item",
                    'format' => 'raw',
                    //'value' =>'Institution',
                   /* 'value' => function ($model) {
                        return $model->LoanItem;
                    },*/
                    'value' => function(){
                        return '-';
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                    'filter' =>ArrayHelper::map(\backend\modules\allocation\models\LoanItem::findBySql('SELECT loan_item_id AS "id", CONCAT(loan_item.item_name," (",loan_item.item_code,")") AS "Name" FROM loan_item ORDER BY item_name ASC')->asArray()->all(), 'id', 'Name'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Search  '],
                    'format' => 'raw'
                ],





               /* [
                    'attribute'=>'disbursement_percent',
                    'width'=>'150px',
                    'hAlign'=>'right',
                    'label'=>"Percent (%)",
                    'format'=>['decimal', 0],
                    'pageSummary'=>true
                ],*/

// ['class' => 'yii\grid\ActionColumn'],

               /* ['class' => 'kartik\grid\ActionColumn',
                    'template'=>'{view}',
                ],*/
            ];
            ?>

            <?php
            /*echo*/ ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,

                'fontAwesome' => true,
//            'asDropdown' => false
                'batchSize' => 50,
                'target' => '_blank',
                'selectedColumns' => [0, 1, 2, 3, 4, 5, 6, 7], // Col seq 2 to 6
                'columnSelectorOptions' => [
                    'label' => 'Export Columns',
                ],
                // 'hiddenColumns' => [15], // SerialColumn, Color, & ActionColumn
                //'disabledColumns' => [0, 1, 2, 3, 4, 5, 6, 9, 12], // ID & Name
                'noExportColumns' => [15],
                'dropdownOptions' => [
                    'label' => 'Export Data',
                    'class' => 'btn btn-default'
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_EXCEL => false,
                    ExportMenu::FORMAT_EXCEL_X => false,
                ],
                //'folder' => '@webroot/tmp', // this is default save folder on server
            ]) . "<hr>\n";
            ?>

            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                'showPageSummary'=>true,
                'pageSummaryRowOptions'=>['class'=>'text-bold bg-blue-gradient'],
                'pjax'=>true,
                'striped'=>true,
                'hover'=>true,
            ]);

            ?>















        </div>


 </div>
</div>