<?php
use kartik\widgets\Select2;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use kartik\widgets\FileInput;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementUplifting */
/* @var $form yii\widgets\ActiveForm */
//echo Module::FetchBootstrap('fancyInputs');
//echo Module::FetchBootstrap('dashboard');
//echo Module::FetchBootstrap('header');
echo Module::FetchBootstrap('js');

$SQL = "SELECT * FROM suspension_reason WHERE suspension_reason.is_active='1' AND suspension_reason.type='0' ORDER BY suspension_reason.name ASC";
$dataArray = ArrayHelper::map(\backend\modules\disbursement\models\SuspensionReason::findBySql($SQL)->asArray()->all(),'id','description')
?>

<div class="disbursement-suspension-form">

    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data'], // important
        'type' => ActiveForm::TYPE_VERTICAL,
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><span class="fa fa-search"></span></div>
                    <?= Html::textInput('search','',['id'=>'search','class'=>'form-control','placeholder'=>'Search form IV/VI index number','style'=>'font-size:24px; height:50px;']); ?>
                    <div class="input-group-addon"><span id="search_feedback"><span class="fa fa-spinner fa-2x"></span></span></div>
                    <div class="input-group-addon"><button type="button" id="searchButton" class="btn bg-green"><span class="fa fa-search"></span> Search</button></div>
                </div>
            </div>

            <?= $form->field($model, 'application_id')->hiddenInput(['class'=>'form-control'])->label(false) ?>

            <div class="form-group">
                <?php
                //echo '<label class="control-label">Uplifting Mode</label>';

                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' =>1,
                    'attributes' => [

                        'mode' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            'label' => 'Uplifting Mode',
                            'options' => [
                                'data' => ['1'=>'ALL ITEMS','2'=>'SELECTIVE'],
                                //'data' =>ArrayHelper::map(disbursementsuspension-mode\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),

                                'options' => [
                                    'prompt' => 'Select Mode',
                                    //'multiple'=>TRUE,

                                ],
                            ],
                        ],

                    ]
                ]);
                ?>
            </div>

            <div class="form-group" id="loan_item">
                <?php
                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' =>1,
                    'attributes' => [

                        'loan_item_id' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            'label' => 'Loan Items',
                            'options' => [
                                'data' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::findBySql('SELECT * FROM loan_item WHERE loan_item.is_active="1" AND loan_item.loan_item_id IN(SELECT allocation.loan_item_id FROM allocation) ORDER BY item_name ASC')->asArray()->all(), 'loan_item_id', 'item_name'),

                                //'data' =>ArrayHelper::map(\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),

                                'options' => [
                                    'prompt' => 'You can Select Multiple items',
                                    'multiple'=>TRUE,

                                ],
                            ],
                        ],

                    ]
                ]);
                ?>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">

                        <?= $form->field($model, 'suspension_percent')->textInput(['readOnly'=>'readOnly','class'=>'form-control','type' => 'number','min'=>0,'max'=>100]) ?>

                    </div>

                    <div class="col-md-6">

                        <?= $form->field($model, 'remaining_percent')->textInput(['class'=>'form-control','type' => 'number','min'=>0,'max'=>100]) ?>

                    </div>
                </div>


            </div>




            <?= $form->field($model, 'status')->hiddenInput(['value' => 0,'class'=>'form-control'])->label(false) ?>

            <div class="form-group">
                <?php
                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' =>1,
                    'attributes' => [

                        'status_reason' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            'label' => 'Uplifting Reasons',
                            'options' => [
                                'data' => ArrayHelper::map(\backend\modules\disbursement\models\SuspensionReason::findBySql($SQL)->asArray()->all(), 'id', 'name'),

                                //'data' =>ArrayHelper::map(\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),

                                'options' => [
                                    'prompt' => 'Select Reason',
                                    // 'multiple'=>TRUE,

                                ],
                            ],
                        ],

                    ]
                ]);
                ?>
                <blockquote id="reasons_description" class="text-muted text-italics">

                </blockquote>
            </div>

            <div class="form-group">
                <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>
            </div>

            <div class="form-group">
                <?php
                echo $form->field($model, 'file')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                ]);
                ?>

            </div>

            <div style="display: none;">
                <?= $form->field($model, 'status_date')->textInput() ?>

                <?= $form->field($model, 'status_by')->textInput() ?>

                <?= $form->field($model, 'supporting_document')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'suspension_history')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'sync_id')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="text-right">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

                <?php
                echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
                echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

                ?>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel">
                <div class="panel-heading"><h4 class="text-bold text-uppercase">Lonee's Profile</h4></div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-3">
                            <img src="../applicant_attachment/profile/mwajuma.jpeg" width="100%" id="PHOTO" />
                        </div>
                        <div class="col-lg-9">
                            <table class="table table-condensed table-bordered">
                                <tbody>
                                <tr>
                                    <th class="text-bold text-uppercase">index#</th><td id="indexNo"></td>
                                    <th class="text-bold text-uppercase">Registration#</th><td id="regNo"></td>
                                </tr>
                                <tr>
                                    <th class="text-bold text-uppercase">Full Name</th><td id="applicantName" colspan="3"></td>
                                </tr>
                                <tr>
                                    <th class="text-bold text-uppercase">Sex</th><td id="sex"></td>
                                    <th class="text-bold text-uppercase">DOB</th><td id="birthDate"></td>
                                </tr>
                                <tr>
                                    <th class="text-bold text-uppercase">Phone#</th><td id="phone_number"></td>
                                    <th class="text-bold text-uppercase">E-mail</th><td id="email_address"></td>
                                </tr>

                                <tr>
                                    <th class="text-bold text-uppercase">Programme</th><td id="programme"></td>
                                    <th class="text-bold text-uppercase">Year of Study</th><td id="studyYear"></td>
                                </tr>
                                <tr>
                                    <th class="text-bold text-uppercase">Institution</th><td colspan="3" id="institution"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div>


            <div class="panel">
                <div class="panel-heading"><h4 class="text-bold text-uppercase">Disbursement Suspension History</h4></div>
                <div class="panel-body">
                    <table class="table table-condensed table-bordered">
                        <thead class="bg-primary">
                        <tr class="text-uppercase">
                            <th class="text-center" colspan="3">Allocation</th>
                            <th class="text-center bg-red" colspan="2">Suspended</th>
                            <th class="text-center bg-green" colspan="2">remained</th>
                            <th class="text-center bg-red" colspan="2">Suspension</th>

                        </tr>
                        <tr>
                            <th>Loan Item</th>
                            <th>Academic Year</th>
                            <th class="text-right">Amount</th>
                            <th class="text-right bg-red">Percent</th>
                            <th class="text-right bg-red">Amount</th>
                            <th class="text-right bg-green">Percent</th>
                            <th class="text-right bg-green">Amount</th>
                            <th class="bg-red">Reason</th>
                            <th class="bg-red">Date</th>
                        </tr>
                        </thead>
                        <tbody id="history_table"></tbody>
                        <tfoot class="bg-primary">
                        <tr class="text-uppercase">
                            <th>TOTAL</th>
                            <th></th>
                            <th id="total_allocated" class="text-right text-bold">0.00</th>
                            <th class="bg-red"></th>
                            <th id="total_suspended" class="text-right text-bold bg-red">0.00</th>
                            <th class="bg-green"></th>
                            <th id="total_remained" class="text-right text-bold bg-green">0.00</th>
                            <th colspan="2" class="bg-red"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <script>
        function commaSeparateNumber(val) {
            while (/(\d+)(\d{3})/.test(val.toString())) {
                val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            }
            return val;
        }

        function SearchLoanee(){
            var spinner = '<span class="fa fa-spinner fa-2x fa-spin"></span>';
            $("#search_feedback").html(spinner);
            $("#history_table").html('<tr><td colspan="9" style="font-size: large;">'+spinner+' Please Wait ... Loading</td></tr>');
            $.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-uplifting/search-lonee'); ?>",
                type:"POST",
                cache:false,
                data:{
                    csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                    search:$('#search').val()
                },
                success:function (data) {
                    console.log(data);
                    $("#history_table").html('');
                    var dataArray = data.output;
                    var totalAllocation = 0;
                    var totalUplifting = 0;
                    var totalRemaining = 0;
                    if (dataArray.length!=0){
                        $("#search_feedback").html('<span class="fa fa-check fa-2x text-green"></span>');
                        $("#search_feedback").html('<span class="fa fa-check fa-2x text-green"></span>');
                        $.each(dataArray,function (index,value) {
                            $('#applicantName').html(value.full_name);
                            $('#regNo').html(value.registration_number);
                            $('#indexNo').html(value.index_number);
                            $('#PHOTO').attr('src','../'+value.photo);
                            $('#sex').html(value.Sex);
                            $('#birthDate').html(value.DOB);
                            $('#programme').html(value.programme_name+' ( <b>'+value.programme_code+'</b>)'+' [ <b>'+value.years_of_study+'</b> years]');
                            $('#institution').html(value.Institution);
                            $('#studyYear').html(value.current_study_year);
                            $('#phone_number').html(value.phone_number);
                            $('#email_address').html(value.email_address);
                            $("#disbursementsuspension-application_id").val(value.application_id);

                            $("#history_table").append(
                                '<tr>' +
                                '<td>'+value.item_name+'</td>' +
                                '<td>'+value.academic_year+'</td>' +
                                '<td class="text-right text-bold">'+commaSeparateNumber((value.allocated_amount*1).toFixed(2))+'</td>' +
                                '<td class="text-right text-bold">'+commaSeparateNumber((value.suspension_percent*1).toFixed(2))+'</td>' +
                                '<td class="text-right text-bold">'+commaSeparateNumber((value.suspended_amount*1).toFixed(2))+'</td>' +
                                '<td class="text-right text-bold">'+commaSeparateNumber((value.remaining_percent*1).toFixed(2))+'</td>' +
                                '<td class="text-right text-bold">'+commaSeparateNumber((value.remaining_amount*1).toFixed(2))+'</td>' +
                                '<td class="text-right text-bold">'+(value.suspension_reason)+'</td>' +
                                '<td class="text-right text-bold">'+(value.suspension_date)+'</td>' +



                                '</tr>'
                            );

                            totalAllocation+=(value.allocated_amount*1);
                            totalUplifting+=(value.suspended_amount*1);
                            totalRemaining+=(value.remaining_amount*1);
                            $("#total_allocated").html(commaSeparateNumber((totalAllocation*1).toFixed(2)));
                            $("#total_suspended").html(commaSeparateNumber((totalUplifting*1).toFixed(2)));
                            $("#total_remained").html(commaSeparateNumber((totalRemaining*1).toFixed(2)));



                        });

                    }else{
                        $("#search_feedback").html('<span class="fa fa-ban fa-2x text-red"></span>');
                        $('#applicantName').html('');
                        $('#regNo').html('');
                        $('#indexNo').html('');
                        $('#sex').html('');
                        $('#birthDate').html('');
                        $('#programme').html('');
                        $('#institution').html('');
                        $("#disbursementsuspension-application_id").val('');
                        $("#history_table").html('<tr><td colspan="9" style="font-size: large;"><span class="text-red fa fa-ban fa-3x"></span> Record Not Found</td></tr>');

                    }

                }
            });
        }

        function ValidatePercentages(source,destination){
            var currentValue =  $("#"+source).val()*1;
            var destinationValue = (100-currentValue);
            $("#"+destination).val(destinationValue);
        }
        $(document).ready(function () {
            $("#loan_item").hide();

            $("#searchButton").on('click',function () {
                SearchLoanee();
            });

            $("#disbursementsuspension-status_reason").on('change',function () {

                var
                    dataMap= <?php echo json_encode($dataArray); ?>;
                var index = $(this).val();
                //console.log(index);
                $("#reasons_description").html(dataMap[index]);
            });


            $("#disbursementsuspension-suspension_percent").on('change',function () {
                ValidatePercentages('disbursementsuspension-suspension_percent','disbursementsuspension-remaining_percent');
            });

            $("#disbursementsuspension-remaining_percent").on('change',function () {
                ValidatePercentages('disbursementsuspension-remaining_percent','disbursementsuspension-suspension_percent');
            });

            $("#disbursementsuspension-mode").on('change',function () {
                var selection = $(this).val();
                console.log(selection);
                if (selection=='1'){
                    $("#loan_item").hide();
                }else {
                    $("#loan_item").show();
                    $("#disbursementsuspension-loan_item_id").val([]);
                }


            });



        });
    </script>




    <?php  ActiveForm::end();   ?>
</div>
