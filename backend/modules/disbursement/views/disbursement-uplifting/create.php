<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementSuspension */

$this->title = 'Disbursement Suspension Uplifting';
$this->params['breadcrumbs'][] = ['label' => 'Suspension Uplifting', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-suspension-create">
    <div class="panel panel-success">
        <div class="panel-heading bg-green">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>






        </div>
    </div>
</div>
