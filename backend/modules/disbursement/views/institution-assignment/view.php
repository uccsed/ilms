<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/3/18
 * Time: 3:12 PM
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */

$this->title = $model->Institution.' Assignment to '.$model->Officer;
$this->params['breadcrumbs'][] = ['label' => 'Institution Assignments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-assignment-view">
    <div class="institution-assignment-index">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">

                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'Officer',
                    'Institution',
                    //'sync_id',
                    //'assigned_by',
                    [
                        'name'=>'assigned_by',
                        'value'=>Module::UserInfo($model->assigned_by,'fullName'),
                        'format'=>'html',
                        'label'=>'Assigned By',
                    ],
                    'assigned_on',
                ],
            ]) ?>
            </div>
</div>