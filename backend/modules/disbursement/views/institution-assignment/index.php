<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/1/18
 * Time: 4:00 PM
 */
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\application\models\VerificationCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Officer Institution Assignment';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-assignment-index">
	<div class="panel panel-info">
    <div class="panel-heading">
    <?= Html::encode($this->title) ?>
    </div>
        <div class="panel-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Assign Institution to Officer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
           //'verification_comment_group_id',
           //'institution_id',
                   [
                    'attribute' => 'institution_id',
                        'label'=>"Institution",
                        'format' => 'raw',
                        'value' => function ($model) {
                             return $model->Institution;
                        },
                       'filterType' => GridView::FILTER_SELECT2,
                       //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                       'filter' =>ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql('SELECT * FROM learning_institution WHERE institution_type="UNIVERSITY" ORDER BY institution_name ASC')->asArray()->all(), 'learning_institution_id', 'institution_code'),
                       'filterWidgetOptions' => [
                           'pluginOptions' => ['allowClear' => true],
                       ],
                       'filterInputOptions' => ['placeholder' => 'Search  '],
                       'format' => 'raw'
                    ],


                    [
                     'attribute' => 'officer_id',
                        'label'=>"Officer",
                        'format' => 'raw',
                        'value' => function ($model) {
                             return $model->Officer;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
                        'filter' =>ArrayHelper::map(\common\models\User::findBySql('SELECT user.user_id,CONCAT(user.firstname," ",user.surname) AS "Name" FROM `user` WHERE user.login_type=5')->asArray()->all(), 'user_id', 'Name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],


            //'comment',
            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{delete}',
                ],
        ],
    ]); ?>
</div>
  </div>
</div>
