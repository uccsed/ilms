<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/1/18
 * Time: 3:59 PM
 */
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementUserTask */

$this->title = 'Create Institution Assignment';
$this->params['breadcrumbs'][] = ['label' => 'Assign Institution', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-user-task-create">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>






        </div>
    </div>
</div>


<?php
//echo Yii::$app->controller->renderPartial("_form");
?>
