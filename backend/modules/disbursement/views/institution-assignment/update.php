<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/3/18
 * Time: 3:20 PM
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */

$this->title = 'Update Institution Assignment: '.$model->Institution.' to ' . $model->Officer;
$this->params['breadcrumbs'][] = ['label' => 'Institution Assignments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="institution-assignment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>