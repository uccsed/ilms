<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\allocation\models\AllocationBatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'List of Allocation Batch Released for Disbursement');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allocation-batch-index">
       <div class="panel panel-info">
                        <div class="panel-heading">
                      <?= Html::encode($this->title) ?>
                        </div>
                        <div class="panel-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //= Html::a(Yii::t('app', 'Create Allocation Batch'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
                            <?php
                            /*$dataProvider->pagination->pageSize=0;
                            $dataProvider->pagination  = false;*/
                            ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'hover' => true,
        'condensed' => true,
        'floatHeader' => true,
        'showPageSummary'=>true,
        'pjax'=>true,
        'striped'=>true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            //'allocation_batch_id',
            'batch_number',
            //'batch_desc',
            [
                'attribute'=>'batch_desc',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '200px',
                'label' => 'Batch Description',
                'value' => function ($model) {
                    return $model->batch_desc;
                },
                ],
              [
                        'attribute' => 'academic_year_id',
                        'vAlign' => 'middle',
                        'width' => '200px',
                        'value' => function ($model) {
                            return $model->academicYear->academic_year;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(\common\models\AcademicYear::find()->where("is_current=1")->asArray()->all(), 'academic_year_id', 'academic_year'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search '],
                        'format' => 'raw'
                    ],
           // 'available_budget',
            // 'is_approved',
             //'approval_comment:ntext',
            // 'created_at',
            // 'created_by',
            // 'is_canceled',
            // 'cancel_comment:ntext',

            [
                    'attribute'=>'approval_status',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '200px',
                'value' => function ($model) {
                    return $model->larc_approve_status=='0'?"LARC APPROVAL":"ED APPROVAL";
                },
                    'filter'=>false,
                'format' => 'raw',
                'pageSummary'=>'GRAND TOTAL',
                'pageSummaryOptions'=>['class'=>'text-right text-green'],
                ],

            [
                'attribute'=>'approved_at',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '200px',
                'value' => function ($model) {
                    return $model->approved_at!=''?date('D d/m/Y',strtotime($model->approved_at)):"-";
                },
                'filter'=>false,
                'format' => 'raw',
                'pageSummary'=>'GRAND TOTAL',
                'pageSummaryOptions'=>['class'=>'text-right text-green'],
            ],
            /*'larc_approve_status',
            'partial_approve_status',*/
            //'',
            [
                'attribute'=>'beneficiaries',
                'hAlign' => 'right',
                'width' => '200px',
                'value' => function ($model) {
                    return Module::BatchBeneficiaries($model->allocation_batch_id);
                },
                'filter'=>false,
                'format' => ['decimal',0],
                'pageSummary'=>true,
            ],
            [
                'attribute'=>'Batch Amount',
                'hAlign' => 'right',
                'width' => '200px',
                'value' => function ($model) {
                    return Module::BatchAmount($model->allocation_batch_id);
                },
                'filter'=>false,
                'format' => ['decimal',0],
                'pageSummary'=>true,
            ],

             ['class' => 'kartik\grid\ActionColumn',
             'template' => '{viewbatch}',
                'buttons' => [
                    'update' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil" title="Edit"></span>',
                            $url);
                    },
                      'viewbatch' => function ($url,$model,$key) {
                                                return Html::a('<span class="glyphicon glyphicon-eye-open" title="Edit"></span>', $url);
                    },

                ],
                ],
        ],
       /*'hover' => true,
       'condensed' => true,
       'floatHeader' => true,*/
    ]); ?>
</div>
       </div>
</div>