<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model frontend\modules\allocation\models\AllocationBatch */

//$this->title = $model->allocation_batch_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Allocation Batch'), 'url' => ['/disbursement/default/allocation-batch']];
/*$id = 1;
echo $strDigits = date('YmdHis').Yii::$app->user->id.$id;
echo '<br>';*/
$this->params['breadcrumbs'][] = $this->title;
//echo Module::HeaderIDAlgorithm($strDigits);
?>
<div class="allocation-batch-view">
  <div class="panel panel-info">
        <div class="panel-heading">
    ALLOCATION BATCH SUMMARY
        </div>
        <div class="panel-body">

    <?= Html::a('Download Allocation Batch', ['default/allocation-batch-download','allocation_batch_id'=>$model->allocation_batch_id], ['class' => 'btn btn-warning text-right','target'=>'_blank']) ?>
    <?= Html::a('Download Allocation Batch Details', ['default/allocation-batch-details-download','allocation_batch_id'=>$model->allocation_batch_id], ['class' => 'btn btn-primary text-right','target'=>'_blank']) ?>

    </p>
    <?= DetailView::widget([
        'model' => $model,
         'condensed' => true,
        'hover' => true,
        'attributes' => [
            //'allocation_batch_id',
            'batch_number',
            //'batch_desc',
            [
                'attribute'=>'batch_desc',
                'label'=>'Batch Description',
                'format'=>'raw',

                // 'valueColOptions'=>['style'=>'width:30%']
            ],
           // 'academic_year_id',
           // 'academicYear.academic_year', 
                       [
                        'attribute'=>'academic_year_id', 
                        'label'=>'Academic Year',
                        'format'=>'raw',
                        'value'=>call_user_func(function ($data){

                                           return $data->academicYear->academic_year;
                        },$model),
                       // 'valueColOptions'=>['style'=>'width:30%']
                       ],

            [
                'attribute'=>'allocation_batch_id',
                'label'=>'Batch Amount',
                'format'=>['decimal',2],
                'value'=>call_user_func(function ($data){

                    return Module::BatchAmount($data->allocation_batch_id);
                },$model),
                // 'valueColOptions'=>['style'=>'width:30%']
            ],

            [
                'attribute'=>'allocation_batch_id',
                'label'=>'Beneficiaries',
                'format'=>['decimal',0],
                'value'=>call_user_func(function ($data){

                    return Module::BatchBeneficiaries($data->allocation_batch_id);
                },$model),
                // 'valueColOptions'=>['style'=>'width:30%']
            ],
           [
                'attribute'=>'created_at',
               'value'=>call_user_func(function ($data){

                   return date('d/m/Y H:i:s',strtotime($data->created_at));
               },$model),
                'format'=>'html',
                'label'=>'Approval Date',
           ],
            [
                'attribute'=>'is_approved',
                'value'=>call_user_func(function ($data){

                    return $data->larc_approve_status=='1'?"LARC APPROVAL":"ED APPROVAL";
                },$model),
                'format'=>'html',
                'label'=>'Approval Type',
            ]
           // 'available_budget',
           // 'is_approved',
           // 'approval_comment:ntext',

            //'created_at',
            //'created_by',
           // 'is_canceled',
            //'cancel_comment:ntext',
        ],
    ]) ?>
  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <td colspan="2"><b>ALLOCATION BATCH SUMMARY</b></td>
                      </tr>
                      <tr>
                          <td><b>LOAN ITEM</b></td>
                        <td align='right'><b>AMOUNT</b></td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                       // print_r($dataProvider);
                              
                        $total=0;
                 $sql=  backend\modules\allocation\models\Allocation::find()->select("*,sum(allocated_amount) as amount,item_name")->joinWith("loanItem")->GroupBy("item_name")->where("allocation_batch_id=$model->allocation_batch_id")->asArray()->all();
                  if(count($sql)>0){
                 foreach ($sql as $rows){
                 echo "<tr>
                        <td>".$rows["item_name"]."</td>
                        <td align='right'><b>".number_format($rows["amount"])."</b></td>
                     
                      </tr>";
                 $total+=$rows["amount"];
                           }
               echo "<tr>
                        <td> </td>
                        <td align='right'><b>".number_format($total)."</b></td>
                     
                      </tr>";
                  }
                  else{
               echo "<tr><td colspan='2'><font color='red'>Sorry No results found</font></td></tr>";       
                  }
                       ?>
                  
                    </tbody>
                    
                  </table>
</div>
      <div class="panel-footer">
          <?php
          //Yii::$app->urlManager->createUrl('//disbursement/default/allocation-batch/view&id='.$model->disb);
          $link='#';
          //find the allocation batch status
          $teststatus=  \frontend\modules\allocation\models\AllocationBatch::findone($model->allocation_batch_id);
          //end
          // if($teststatus->disburse_status==0){
          if($teststatus->disburse_status==0){
          ?>
          <p>
              <?= Html::a('Click to ACCEPT this Allocation Batch ', ['approve-status', 'id' =>$model->allocation_batch_id,"status"=>1], [
                  'class' => 'btn btn-success',
                  'data' => [
                      'confirm' => 'Are you sure you want to Verify this Allocation Batch ?',
                      'method' => 'post',
                  ],
              ]) ?>
              <?= Html::a('Click to REJECT this Allocation Batch[Allocation Batch Mismatch] ', ['approve-status', 'id' =>$model->allocation_batch_id,"status"=>2], [
                  'class' => 'btn btn-danger pull-right',
                  'data' => [
                      'confirm' => 'Are you sure you want to Verify this Allocation Batch ?',
                      'method' => 'post',
                  ],
              ]);
              }
              else{
                  ?>

                  <?php
              }
              ?>
      </div>
    </div>

</div>