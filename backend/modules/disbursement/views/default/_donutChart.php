<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/9/18
 * Time: 11:03 AM
 */
?>


<div id="itemsDonut" style="height: 300px;"></div>


<!--<script src="../ExtraPlugins/flot/jquery.flot.min.js"></script>-->
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<!--<script src="../ExtraPlugins/flot/jquery.flot.resize.min.js"></script>-->
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<!--<script src="../ExtraPlugins/flot/jquery.flot.pie.min.js"></script>-->
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<!--<script src="../ExtraPlugins/flot/jquery.flot.categories.min.js"></script>-->
<!-- Page script -->
<script>
    function DonutChart(divID,donutData) {
        $.plot("#"+divID, donutData, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                    innerRadius: 0.5,
                    label: {
                        show: true,
                        radius: 2 / 3,
                        formatter: labelFormatter,
                        threshold: 0.1
                    }

                }
            },
            legend: {
                show: false
            }
        });
    }

    function ItemsDonut(divID) {
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/default/dashboard-statistics'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:"donutData",
                academic_year:'<?php echo $academicYear; ?>',
            },
            success:function (data) {
                var output = data.output['results'];
                console.log(output);
                var donutData = [
                    {label: "Series2", data: 30, color: "#3c8dbc"},
                    {label: "Series3", data: 20, color: "#0073b7"},
                    {label: "Series4", data: 50, color: "#00c0ef"}
                ];

                DonutChart(divID,donutData);

            }
        });

    }

$(document).ready(function () {
    ItemsDonut('itemsDonut');
});





    /*
     * Custom Label formatter
     * ----------------------
     */
    function labelFormatter(label, series) {
        return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
            + label
            + "<br>"
            + Math.round(series.percent) + "%</div>";
    }
</script>