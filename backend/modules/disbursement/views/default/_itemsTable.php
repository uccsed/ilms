<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/9/18
 * Time: 8:31 AM
 */

?>
<div id="itemsTable"></div>


<script>
    function ItemsTable(divID) {
        var mini_spinner = '<div><span class="fa fa-spinner fa-spin"></span> Please Wait ...</div>';
        var medium_spinner = '<div style="font-size: 18px;"><span class="fa fa-spinner fa-2x fa-spin"></span> Please Wait ...</div>';
        var large_spinner = '<div style="font-size: 32px;"><span class="fa fa-spinner fa-5x fa-spin"></span> Please Wait ...</div>';

        $('#'+divID).html(medium_spinner);

        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/default/dashboard-statistics'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:"itemsTable",
                academic_year:'<?php echo $academicYear; ?>',
            },
            success:function (data) {
                $('#'+divID).html('');
                var output = data.output['results'];
                //console.log(output);
                $('#'+divID).html(output);


            }
        });

    }

    $(document).ready(function () {

        ItemsTable("itemsTable");
    });
</script>