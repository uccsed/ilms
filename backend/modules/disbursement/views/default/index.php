<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/8/18
 * Time: 2:39 PM
 */
use Yii;
use backend\modules\disbursement\Module;
echo Module::FetchBootstrap('js');
$academicYear = Module::CurrentAcademicYear();
$academicYearName = Module::CurrentAcademicYear('academic_year');
$date = date('Y-m-d');
$this->title = 'Disbursement Main Dashboard';
$this->params['breadcrumbs'][] = ['label' => 'Disbursement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

 <!--First Row-->
<div class="row">
    <div class="col-md-12">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-primary">
                    <div class="inner">
                        <h4 class="text-bold" style="font-size: 24px;" id="allocation_amount">0</h4>

                        <p class="text-uppercase text-bold"><?php echo $academicYearName; ?> Allocation</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-thumbs-up" id="allocation_percent"></i>

                    </div>
                    <div class="col-md-9">
                        <span class="text-bold text-xl-left" id="allocation_count"></span>
                    </div>
                    <!--data-toggle="tooltip"  data-html="true" title="<p><b>73</b><em> RELEASED BATCHES </em></p>"-->
                    <a href="<?php echo Yii::$app->urlManager->createUrl('/disbursement/default/allocation-batch');  ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->



            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h4 class="text-bold" style="font-size: 24px;" id="disbursement_amount">0</h4>

                        <p class="text-uppercase text-bold"><?php echo $academicYearName; ?> Disbursement</p>
                    </div>
                    <div class="icon">
                        <i class="" id="disbursement_percent"></i>

                    </div>
                    <div class="col-md-9">
                        <span class="text-bold text-xl-left" id="disbursement_count"></span>
                    </div>

                    <a href="#" class="small-box-footer"><!--More info <i class="fa fa-arrow-circle-right"></i>-->&nbsp;</a>
                </div>
            </div>
            <!-- ./col -->


            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h4 class="text-bold" style="font-size: 24px;" id="request_amount">0</h4>

                        <p class="text-uppercase text-bold"><?php echo $academicYearName; ?> Institution Requests</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-bank" id="request_percent"></i>

                    </div>
                    <div class="col-md-9">
                        <span class="text-bold text-xl-left" id="request_count"></span>
                    </div>

                    <a href="#" class="small-box-footer"><!--More info <i class="fa fa-arrow-circle-right"></i>-->&nbsp;</a>
                </div>
            </div>
            <!-- ./col -->






            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h4 class="text-bold" style="font-size: 24px;" id="returns_amount">0</h4>

                        <p class="text-uppercase text-bold"><?php echo $academicYearName; ?> Returns</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-ban" id="returns_percent"></i>

                    </div>
                    <div class="col-md-9">
                        <span class="text-bold text-xl-left" id="returns_count"></span>
                    </div>

                    <a href="#" class="small-box-footer"><!--More info--> <!--<i class="fa fa-arrow-circle-right"></i>--> &nbsp;</a>
                </div>
            </div>
            <!-- ./col -->

            <input type="hidden" id="allocation_countField" class="form-control"/>
            <input type="hidden" id="allocation_amountField" class="form-control"/>
        </div>
        <!-- /.row -->
    </div>

</div>
<!-- /First Row-->


<!--Second Row-->
<div class="row">
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-heading"><h4 class="text-bold text-uppercase text">Loan Items Disbursement Distribution</h4></div>
            <div class="panel-body">
                <?php echo $this->render('_areaChart',array('academicYear'=>$academicYear)); ?>
                <?php //echo $this->render('_barChart',array('academicYear'=>$academicYear)); ?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <!-- Map box -->
        <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">
                <!-- tools box -->
                <div class="pull-right box-tools">
                   <!-- <button type="button" class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Date range">
                        <i class="fa fa-calendar"></i></button>-->
                    <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                        <i class="fa fa-minus"></i></button>
                </div>
                <!-- /. tools -->

                <i class="fa fa-money"></i>

                <h3 class="box-title text-bold text-uppercase">Allocation v/s Disbursement Per Item</h3>
            </div>
            <div class="box-body">
                <?php echo $this->render('_itemsTable',array('academicYear'=>$academicYear)); ?>
                <?php //echo $this->render('_donutChart',array('academicYear'=>$academicYear)); ?>
            </div>
            <!-- /.box-body-->

        </div>
        <!-- /.box -->
    </div>

</div>
<!-- /Second Row-->

<!-- Third Row-->
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title text-bold text-uppercase">Allocation v/s Disbursement Per Item Per Category</h3>

                <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                    <!--<div class="btn-group" data-toggle="btn-toggle">
                        <button type="button" class="btn btn-default btn-sm active"><i class="fa fa-square text-green"></i>
                        </button>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-square text-red"></i></button>
                    </div>-->
                </div>
            </div>
            <div class="box-body">
                <?php echo $this->render('_matrixTable',array('academicYear'=>$academicYear)); ?>
            </div>
            <!-- /.chat -->
            <div class="box-footer">

            </div>
        </div>
    </div>
</div>
<!-- /Third Row-->


<!-- Fourth Row-->
<div class="row">
    <div class="col-md-8"></div>
    <div class="col-md-4"></div>

</div>
<!-- /Fourth Row-->




















<script>
    $('body').addClass('sidebar-collapse sidebar-mini');
    //$('[data-toggle="tooltip"]').tooltip();
    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }

    function SmallBoxData(option,academicYear) {

        var mini_spinner = '<div><span class="fa fa-spinner fa-spin"></span> Please Wait ...</div>';
        var medium_spinner = '<div style="font-size: 18px;"><span class="fa fa-spinner fa-2x fa-spin"></span> Please Wait ...</div>';
        var large_spinner = '<div style="font-size: 32px;"><span class="fa fa-spinner fa-5x fa-spin"></span> Please Wait ...</div>';

        $('#'+option+'_count').html(mini_spinner);
        $('#'+option+'_amount').html(mini_spinner);
        //$('#'+option+'_percent').html(mini_spinner);





        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/default/dashboard-statistics'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:option,
                academic_year:academicYear,
            },
            success:function (data) {
                $('#'+option+'_count').html('');
                $('#'+option+'_amount').html('');
                $('#'+option+'_percent').html('');

                var output = data.output['results'][0];
                var count = output.count*1;
                var amount = output.amount*1;

               // console.log(output);
                if(option=='allocation'){
                    $('#'+option+'_count').html('Beneficiaries: '+commaSeparateNumber(count));
                    $('#'+option+'_amount').html('<sup>Tshs.</sup> '+commaSeparateNumber(amount.toFixed(2)));

                    $('#'+option+'_countField').val(count);
                    $('#'+option+'_amountField').val(amount);
                }else {
                    var label = 'Beneficiaries: ';
                    if (option=='request'){
                        label = 'Requests: ';
                    }

                    $('#'+option+'_count').html(label+commaSeparateNumber(count));


                    $('#'+option+'_amount').html('<sup>Tshs.</sup> '+commaSeparateNumber(amount.toFixed(2)));
                }

                var allocationCount = $("#allocation_countField").val()*1;
                var allocationAmount = $("#allocation_amountField").val()*1;

                if (option=='disbursement'){
                    var percent = 0;
                    if (allocationAmount>0){
                        percent=(amount/allocationAmount);
                    }
                    $('#'+option+'_percent').html((percent).toFixed(0)+'%');
                    $('#'+option+'_count').html('Beneficiaries: '+commaSeparateNumber(count));
                }

            }

        });
    }
    $(document).ready(function () {
        SmallBoxData('allocation','<?php echo $academicYear; ?>');
        SmallBoxData('disbursement','<?php echo $academicYear; ?>');
        SmallBoxData('request','<?php echo $academicYear; ?>');
        SmallBoxData('returns','<?php echo $academicYear; ?>');
    });
</script>
