<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/8/18
 * Time: 10:18 PM
 */
use backend\modules\disbursement\Module;
//echo Module::FetchBootstrap('js');
?>
<div class="box-body">
    <div class="chart">
        <canvas id="areaChart" style="height:420px;"></canvas>
    </div>
</div>
















<!-- jQuery 2.2.3 -->
<script src="../ExtraPlugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- ChartJS 1.0.1 -->
<script src="../ExtraPlugins/chartjs/Chart.min.js"></script>

<!-- page script -->
<script>

    function ChartArea(divID,areaChartData) {

        // Get context with jQuery - using jQuery's .get() method.
        var areaChartCanvas = $("#"+divID).get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var areaChart = new Chart(areaChartCanvas);

        var areaChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 3,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            //String - A legend template
            legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true
        };

        //Create the line chart
        areaChart.Line(areaChartData, areaChartOptions);
    }

    function GenerateChart(divID) {
        var mini_spinner = '<div><span class="fa fa-spinner fa-spin"></span> Please Wait ...</div>';
        var medium_spinner = '<div style="font-size: 18px;"><span class="fa fa-spinner fa-2x fa-spin"></span> Please Wait ...</div>';
        var large_spinner = '<div style="font-size: 32px;"><span class="fa fa-spinner fa-5x fa-spin"></span> Please Wait ...</div>';

        $('#'+divID).html(medium_spinner);

        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/default/dashboard-statistics'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:"areaChartData",
                academic_year:'<?php echo $academicYear; ?>',
            },
            success:function (data) {
                $('#'+divID).html('');
                var output = data.output['results'];
                var labels = [];

                console.log(output);
                $.each(output.labels,function (index,level) {
                    labels.push(level);
                });

                var areaChartData = {
                    labels: labels,
                    datasets: output.datasets
                };

                ChartArea(divID,areaChartData);
            }
        });
        
    }

    $(document).ready(function () {

        GenerateChart("areaChart");
    });

</script>
