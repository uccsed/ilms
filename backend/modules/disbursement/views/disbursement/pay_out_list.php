<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/10/18
 * Time: 11:27 AM
 */
use yii\helpers\Html;
use kartik\grid\GridView;
use backend\modules\disbursement\Module;


echo Module::FetchBootstrap('js');
$this->title = 'Payout List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-payout-list">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <p>
                <?php //= Html::a('Add Student', ['create','id'=>$id], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'hover' => true,
                'condensed' => true,
                'floatHeader' => true,
                'showPageSummary'=>true,
                'pageSummaryRowOptions'=>['class'=>'kv-page-summary bg-blue-active'],
                'pjax'=>true,
                'striped'=>true,
                'resizableColumns'=>true,
                'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
                'panel'=>['type'=>'default', 'heading'=>'Payout List'],
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],


                    [
                        'attribute' => 'INDEXNO',
                        'label'=>"INDEXNO",
                        'vAlign' => 'middle',
                        'width' => '200px',
                        'filter'=>true
                    ],
                    [
                        'attribute' => 'NAMES',
                        'label'=>"NAMES",
                        'vAlign' => 'middle',
                        //'width' => '350px',
                        'noWrap' =>true,
                        'filter'=>true
                    ],
                    [
                        'attribute' => 'SEX',
                        'label'=>"SEX",
                        'vAlign' => 'middle',
                        //'width' => '200px',
                        'filter'=>true
                    ],

                   [
                        'attribute' => 'REGNO',
                        'label'=>"REGNO",
                        'vAlign' => 'middle',
                        'width' => '200px',
                    ],

                   [
                        'attribute' => 'YOS',
                        'label'=>"YOS",
                        'vAlign' => 'middle',
                        //'width' => '200px',
                       'filter'=>true
                    ],

                   [
                        'attribute' => 'BANK',
                        'label'=>"BANK",
                        'vAlign' => 'middle',
                        'width' => '200px',
                    ],



                    [
                        'attribute' => 'ACCOUNTNO',
                        'label'=>"ACCOUNTNO",
                        'hAlign'=>'left',
                        'format'=>'raw',
                        //'label'=>"Status",
                        //'width' => '200px',
                        'pageSummary'=>'GRAND TOTAL',
                        'pageSummaryOptions'=>['class'=>'text-right'],
                    ],


                    [
                        'attribute' => 'BS',
                        'label'=>"BS",
                        'hAlign'=>'right',
                        'format'=>['decimal', 2],
                        'width' => '150px',
                        'pageSummary'=>true,
                    ],


                    [
                        'attribute' => 'MA',
                        'label'=>"MA",
                        'hAlign'=>'right',
                        'format'=>['decimal', 2],
                        'width' => '200px',
                        'pageSummary'=>true,
                    ],
                 /*   [
                        'attribute' => 'FPT',
                        'label'=>"FPT",
                        'hAlign'=>'right',
                        'format'=>['decimal', 2],
                        'width' => '150px',
                        'pageSummary'=>true,
                    ],
                    [
                        'attribute' => 'TU',
                        'label'=>"TU",
                        'hAlign'=>'right',
                        'format'=>['decimal', 2],
                        'width' => '150px',
                        'pageSummary'=>true,
                    ],
                    [
                        'attribute' => 'SFR',
                        'label'=>"SFR",
                        'hAlign'=>'right',
                        'format'=>['decimal', 2],
                        'width' => '150px',
                        'pageSummary'=>true,
                    ],
                    [
                        'attribute' => 'RESEARCH',
                        'label'=>"RESEARCH",
                        'hAlign'=>'right',
                        'format'=>['decimal', 2],
                        'width' => '150px',
                        'pageSummary'=>true,
                    ],
                    [
                        'attribute' => 'STIPEND',
                        'label'=>"STIPEND",
                        'hAlign'=>'right',
                        'format'=>['decimal', 2],
                        'width' => '150px',
                        'pageSummary'=>true,
                    ],
                    [
                        'attribute' => 'F_ALLOWANCE',
                        'label'=>"F_ALLOWANCE",
                        'hAlign'=>'right',
                        'format'=>['decimal', 2],
                        'width' => '150px',
                        'pageSummary'=>true,
                    ],
                    [
                        'attribute' => 'M_FEE',
                        'label'=>"M_FEE",
                        'hAlign'=>'right',
                        'format'=>['decimal', 2],
                        'width' => '150px',
                        'pageSummary'=>true,
                    ],*/

                    [
                        'attribute' => 'total',
                        'label'=>"GRAND TOTAL",
                        'hAlign'=>'right',
                        'format'=>['decimal', 2],
                        'width' => '200px',
                        'pageSummary'=>true,
                    ],









                    //~ ['class' => 'yii\grid\ActionColumn'],
                ],





            ]); ?>

        </div>
    </div>

</div>

<script>
    $('body').addClass('sidebar-collapse sidebar-mini');
</script>