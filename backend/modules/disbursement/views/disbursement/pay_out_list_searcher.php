<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 12/17/18
 * Time: 7:36 AM
 */
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/20/18
 * Time: 1:16 PM
 */
use kartik\widgets\Select2;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use kartik\date\DatePicker;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementSuspension */
/* @var $form yii\widgets\ActiveForm */

echo Module::FetchBootstrap('js');
?>

<?php $form = ActiveForm::begin([
    'options'=>['enctype'=>'multipart/form-data'], // important
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>

<div class="panel">
    <div class="panel-heading">

           <div class="row">
               <div class="col-md-3">
                   <div class="form-group">
                        <select id="case" class="btn btn-primary">
                            <option value="HEADER" selected="selected">HEADER ID</option>
                            <option value="INSTITUTION">INSTITUTION</option>
                        </select>
                   </div>
               </div>
               <div class="col-md-7">
                   <div class="form-group">
                       <div class="input-group">
                           <div class="input-group-addon"><span class="fa fa-search"></span></div>
                           <?= Html::textInput('search','',['id'=>'search','class'=>'form-control bordered border-danger text-bold','placeholder'=>'Search by PayList Header ID','style'=>'font-size:24px; height:50px;']); ?>
                           <input type="hidden" id="institution" name="search" class="form-control" />
                           <input type="hidden" id="header" name="search" class="form-control" />
                           <div class="input-group-addon"><span id="search_feedback" class="fa fa-spinner fa-2x"></span></div>
                           <div class="input-group-addon"><button type="button" id="searchButton" class="btn bg-red"><span class="fa fa-search"></span> Search</button></div>
                       </div>
                   </div>
               </div>
               <div class="col-md-1"><button type="button" id="clearButton" class="btn bg-green"><span class="fa fa-eraser"></span> Clear</button></div>
           </div>

    </div>
    <div class="panel-body" id="search_results">
        <div id="search_operations" class="row">
            <div class="col-md-6 pull-left">
                <button type="button" class="btn btn bg-blue-gradient" id="search_btn" style="display: none;"><i class="fa fa-gear" id="search_gear"></i> <span>Load Search Results</span></button>
            </div>
            <div class="pull_right col-md-6" id="search_print_div"></div>
        </div>
        <div id="search_content" style="font-size: 18px;"></div>

    </div>
    <div class="panel-footer"></div>
</div>


<script>

    function ContentCleaner(){
        $("#search_content").html("");
        $("#search_print_div").html("");
        $("#search").val("");
        $("#search_btn").hide();
    }

    function Animate(itemName,operation=true)
    {
        var spinner = '<div style="font-size: 24px;"><span class="fa fa-2x fa-spinner fa-spin"></span> Please Wait... Loading Data...</div>';
        if(operation){
           // $('#'+itemName+'_feedback').attr('class','fa fa-spinner fa-2x fa-spin');
            $('#'+itemName+'_gear').attr('class','fa fa-gear fa-spin');
            $('#'+itemName+'_content').html(spinner);
        } else {
            $('#'+itemName+'_gear').attr('class','fa fa-gear');
            $('#'+itemName+'_content').html("");
            //$('#'+itemName+'_feedback').attr('class','fa fa-spinner fa-2x');
        }
    }

    function ContentsLoader(dataArray,itemName){

        $('#'+itemName+'_content').html("");

        if (dataArray.length===1){
            $('#'+itemName+'_content').append(
                "<table  class='table table-bordered table-striped' style=\"border-spacing: 0 0px; font-size: 4mm; font-family: 'Courier New';\" width=\"100%\" border=\"0\" cellspacing=\"0\">" +
                    "<thead>" +
                        "<tr>" +
                            "<td>INSTITUTION</td>" + "<th id='institution'>"+dataArray[0].institution_name+"</th>" +
                            "<td>ITEM</td>" + "<th id='loan_item'>"+ dataArray[0].item_code +"</th>" +
                            "<td>CATEGORY</td>" + "<th id='category'>" + dataArray[0].category_name+"</th>" +
                            "<td>HEADER ID</td>" + "<th id='header_id'>" + dataArray[0].header_id + "</th>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>ACADEMIC YEAR</td>" + "<th id='academic_year'>"+ dataArray[0].academic_year +"</th>" +
                            "<td>FINANCIAL YEAR</td>" + "<th id='financial_year'>" + dataArray[0].financial_year + "</th>" +
                            "<td>SEMESTER</td>" + "<th id='semester'>"+ dataArray[0].semester_number +"</th>" +
                            "<td>INSTALMENT</td>" + "<th id='instalment'>" + dataArray[0].instalment_number + "</th>" +
                        "</tr>" +
                "</thead>"+
                "</table>"
            );

          var  header = dataArray[0].batch_id;
            $('#header').val(header);
            var link = "<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-batch/payout-list-download'); ?>";
            var printer = '<a href="'+link+'&batch_id='+header+'&mode=pdf" class="btn bg-blue-gradient btn-info pull-right printBtn" id="'+itemName+'_printBtn" value="'+itemName+'" target="_blank"><span class="fa fa-print"></span> PRINT '+itemName.toUpperCase()+'</a>';

            $('#'+itemName+'_print_div').html(printer);
        }else {

        }
    }


    function PSL()
    {

        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement/load-pay-list'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                batch_id:$('#header').val(),

            },
            success:function (data)
            {
                var dataArray = data.output;

                   // $("#search_content").html("");
                    $("#search_content").append(
                        "<p>"+dataArray.summary+"</p>"+
                        "<p>"+dataArray.paylist+"</p>"
                    );

        //100005

            }
        })

    }

    function SearchEngine(itemName)
    {
        Animate(itemName); //Starts Animations
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement/pay-list-search'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                case:$('#case').val(),
                search:$('#search').val()
            },
            success:function (data)
            {
                var dataArray = data.output.results;
                Animate(itemName,false); //End Animations
                if(dataArray.length!==0){
                    $("#search_btn").show();
                    $('#'+itemName+'_feedback').removeAttr('class');
                    $('#'+itemName+'_feedback').attr('class','fa fa-check fa-2x text-success');
                    ContentsLoader(dataArray,itemName);
                } else {
                    $("#"+itemName+"_content").html("No data found");
                    $('#'+itemName+'_feedback').removeAttr('class');
                    $('#'+itemName+'_feedback').attr('class','fa fa-ban fa-2x text-red');
                }

            }
        })

    }

    function PrintDocument(itemName)
    {
        Animate(itemName); //Starts Animations
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement/print-payout'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:itemName,
                batch_id:$('#header').val()
            },
            success:function (data)
            {
                var dataArray = data;
                Animate(itemName,false); //End Animations
                if(dataArray.length!==0){
                    ContentsLoader(dataArray,itemName);
                } else {
                    $("#"+itemName+"_content").html("No data found");
                }

            }
        })

    }

    $(document).ready(function () {

        ContentCleaner();

        $("body").on('click','.printBtn',function () {
             var itemName = $(this).val();
            PrintDocument(itemName);
        });

        $("#searchButton").on('click',function () {
            SearchEngine('search');
        });

        $("#clearButton").on('click',function () {
           ContentCleaner();
        });


        $("#search_btn").on('click',function () {
            PSL();
        });
/*
        $("#statement_btn").on('click',function () {
            SearchEngine('statement');
        });

        $("#return_btn").on('click',function () {
            SearchEngine('return');
        });

        $("#repayment_btn").on('click',function () {
            SearchEngine('repayment');
        });*/
    });
    $(document).ready(function () {
        $("#case").on('change',function () {
            var search = $('#search');
            var selection = $(this).val();
            if (selection==='HEADER'){
                search.attr('placeholder','Search by PayList Header ID');
            }else {
                search.attr('placeholder','Search by Institution Code');
            }
            ContentCleaner();


        });

    });
</script>
<?php  ActiveForm::end();   ?>