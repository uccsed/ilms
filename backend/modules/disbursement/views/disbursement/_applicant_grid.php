<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/14/18
 * Time: 5:47 PM
 */
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\models\DisbursementSearch;
?>
<?php
$searchModel = new DisbursementSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams,$batch_id,$application_id);
$dataProvider->pagination->pageSize=0;


$gridColumns =[
    [
        'class' => 'kartik\grid\SerialColumn',
        'hAlign' => GridView::ALIGN_CENTER,
    ],
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'allowBatchToggle' => true,
        'detail' => function ($model) {
            return $this->render('_applicant_grid',['batch_id'=>$model->disbursement_batch_id,'application_id'=>$model->application_id]);
        },
        'detailOptions' => [
            'class' => 'kv-state-enable',
        ],
    ],


    [
        'attribute' => 'loan_item_id',
        'vAlign' => 'middle',
        'width' => '200px',
        'value' => function ($model) {
            return $model->loanItem->item_name;
        },

        'filter' => false,
        'format' => 'raw',
        'pageSummary'=>'GRAND TOTAL',
        'pageSummaryOptions'=>['class'=>'text-right'],
    ],
    // 'disbursed_amount',
    [
        'attribute' => 'disbursed_amount',
        'hAlign'=>'right',
        'format'=>['decimal', 2],
        //'label'=>"Status",
        'width' => '200px',
        'pageSummary'=>true,
    ],

];
?>


<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'showPageSummary'=>true,
    'pageSummaryRowOptions'=>['class'=>'text-bold bg-blue'],
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'condensed' => true,
    'floatHeader' => true,
]);

?>
