<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/6/18
 * Time: 7:58 PM
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\InstitutionFundRequest */

$this->title = 'Fund Request Approval';

/*if ($model->approval!=''&&!empty($model->approval))
{
    $file="_formApprovalEdit";
}else{
    $file="_formApproval";
}*/
$file="_form";
?>
<div class="institution-fund-request-create">
    <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">
            <?= $this->render($file, [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>