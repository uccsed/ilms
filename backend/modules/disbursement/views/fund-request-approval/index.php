<?php
$user = Yii::$app->user->id;
$uSQL="
SELECT 
user.user_id as 'user_id',
staff.staff_id as 'staff_id',
staff.learning_institution_id as 'institution_id'
FROM user 
LEFT JOIN staff ON user.user_id = staff.user_id
WHERE user.user_id='$user' AND user.login_type='4'";
$uModel = Yii::$app->db->createCommand($uSQL)->queryAll();


if (sizeof($uModel)!=0){
    foreach ($uModel as $uIndex=>$uDataArray){
        $myInstitution = $uDataArray['institution_id'];
        $iSQL = "SELECT * FROM learning_institution WHERE learning_institution_id='$myInstitution'";
    }
}

$ulSQL="
SELECT * FROM user WHERE user.user_id='$user' AND user.login_type='5'";
$ulModel = Yii::$app->db->createCommand($ulSQL)->queryAll();
if (sizeof($ulModel)!=0){
    $liSQL="SELECT loan_item.loan_item_id AS 'loan_item', CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'itemName' FROM loan_item WHERE is_active = '1' AND is_requested = '1'";
    $iSQL = "SELECT * FROM learning_institution WHERE learning_institution_id IN(SELECT institution_assignment.institution_id FROM institution_assignment WHERE institution_assignment.officer_id='$user')";

}

$liModel=Yii::$app->db->createCommand($liSQL)->queryAll();
$iModel=Yii::$app->db->createCommand($iSQL)->queryAll();


$rSQL="
SELECT 
institution_fund_request.id AS 'id',
CONCAT(learning_institution.institution_name,' | ',loan_item.item_name,' - ',academic_year.academic_year,'-',semester.semester_number) AS 'name'
  FROM institution_fund_request 
  JOIN learning_institution ON institution_fund_request.institution_id = learning_institution.learning_institution_id
  JOIN loan_item ON institution_fund_request.loan_item = loan_item.loan_item_id
  JOIN academic_year ON institution_fund_request.academic_year = academic_year.academic_year_id
  JOIN semester ON institution_fund_request.semester_number = semester.semester_id
  #WHERE approval='3'";
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\InstitutionFundRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Institution Fund Requests awaiting for Approval';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-fund-request-index">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

          <!--  <p>
                <?/*= Html::a('Create Institution Fund Request', ['create'], ['class' => 'btn btn-success']) */?>
            </p>-->


            <?php
    $gridColumns = [
        [
            'class' => 'kartik\grid\SerialColumn',
            'hAlign' => GridView::ALIGN_CENTER,
        ],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
            },
            'allowBatchToggle' => true,
            'detail' => function ($model) {
            //return $this->render('_pay_list_details',['request'=>$model->id]);
            return $this->render('../institution-fund-request/_payoutPDF',['model'=>$model]);
            },
            'detailOptions' => [
                'class' => 'kv-state-enable',
            ],
        ],
        [
            'attribute' => 'id',
            //'width'=>'250px',
            'label'=>"Request",
            'format' => 'raw',
            'value' => function ($model) {
                return $model->Request;
            },
            'filterType' => GridView::FILTER_SELECT2,
            //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
            'filter' =>ArrayHelper::map(\backend\modules\disbursement\models\InstitutionFundRequest::findBySql($rSQL)->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Search  '],
            'format' => 'raw',
            //'group'=>true,
        ],
        [
            'attribute' => 'institution_id',
            'label'=>"Institution",
            'format' => 'raw',

            'value' => function ($model) {
                return $model->Institution;
            },
            'filterType' => GridView::FILTER_SELECT2,
            //'filter' => ArrayHelper::map(\backend\modules\allocation\models\LoanItem::find()->where("is_active=1")->asArray()->all(), 'loan_item_id', 'item_name'),
            'filter' =>ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql($iSQL)->asArray()->all(), 'learning_institution_id', 'institution_code'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Search  '],
            'format' => 'raw',
            // 'group'=>true,  // enable grouping,
        ],

        [
            'attribute' => 'loan_item',
            'label'=>"Loan Item",
            'format' => 'raw',
            'value' => function ($model) {
                return $model->LoanItem;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map($liModel, 'loan_item', 'itemName'), 'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
            'filterInputOptions' => ['placeholder' => 'Search  '],
            'format' => 'raw'
        ],

        [
            'attribute' => 'invoice_number',
            'label'=>"Invoice Number",
            'format' => 'raw',
            'value' => function ($model) {
                return $model->invoice_number;
            },

            //'filterInputOptions' => ['placeholder' => 'Search Invoice # '],
            'format' => 'raw'
        ],

        [
            'attribute' => 'academic_year',
            'label'=>"Academic Year",
            'format' => 'raw',
            'value' => function ($model) {
                return $model->AcademicYear;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(\common\models\AcademicYear::findBySql('SELECT * FROM academic_year ORDER BY is_current DESC')->asArray()->all(), 'academic_year_id', 'academic_year'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Search  '],
            'format' => 'raw'
        ],

        [
            'attribute' => 'request_status',
            'label'=>"Status",

            'value' => function ($model) {
                return $model->Status;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \backend\modules\disbursement\Module::PaymentStatus(),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Search  '],
            'format' => 'raw',
            'pageSummary'=>'TOTAL',
            'pageSummaryOptions'=>['class'=>'text-right'],
        ],
        [
            'attribute' => 'request_amount',
            'label'=>"Request Amount",
            'hAlign' => 'right',
            'format' => ['decimal',2],
            'value' => function ($model) {
                return $model->Amount;
            },

            'filter'=>false,
            'pageSummary'=>true,

        ],
       // ['class' => 'yii\grid\ActionColumn'],

        ['class' => 'kartik\grid\ActionColumn',
                     'template'=>'{view}',
                 ],
    ];
    ?>

            <?php
            /*echo*/ ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,

                    'fontAwesome' => true,
//            'asDropdown' => false
                    'batchSize' => 50,
                    'target' => '_blank',
                    'selectedColumns' => [0, 1, 2, 3, 4, 5, 6, 7], // Col seq 2 to 6
                    'columnSelectorOptions' => [
                        'label' => 'Export Columns',
                    ],
                   // 'hiddenColumns' => [15], // SerialColumn, Color, & ActionColumn
                    //'disabledColumns' => [0, 1, 2, 3, 4, 5, 6, 9, 12], // ID & Name
                    'noExportColumns' => [15],
                    'dropdownOptions' => [
                        'label' => 'Export Data',
                        'class' => 'btn btn-default'
                    ],
                    'exportConfig' => [
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_EXCEL => false,
                        ExportMenu::FORMAT_EXCEL_X => false,
                    ],
                    //'folder' => '@webroot/tmp', // this is default save folder on server
                ]) . "<hr>\n";
            ?>

            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                'showPageSummary'=>true,
                'pageSummaryRowOptions'=>['class'=>'text-bold bg-blue'],
                'pjax'=>true,
                'striped'=>true,
                'hover'=>true,
            ]);

            ?>



        </div>
    </div>

</div>
