<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/6/18
 * Time: 5:42 PM
 */
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\grid\FormulaColumn;
?>
<!--'id',
'request_id',
'disbursement_batch_id',
'application_id',
'programme_id',
// 'loan_item_id',
// 'allocated_amount',
// 'status',
// 'created_at',
// 'created_by',-->
<div class="panel">


<?php


echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'panel'=>['type'=>'primary', 'heading'=>$itemTitle],
    'columns'=>[
        ['class'=>'kartik\grid\SerialColumn'],

        [
            'attribute'=>'Student',
           // 'width'=>'250px',

        ],
        [
            'attribute'=>'Programme',
            //'pageSummary'=>true,
        ],
        [
            'attribute'=>'LoanItem',
            //'width'=>'150px',
           // 'hAlign'=>'right',
            //'format'=>['decimal', 2],
            'pageSummary'=>'Total '.$item,
            'pageSummaryOptions'=>['class'=>'text-right text-warning'],
            //'pageSummaryFunc'=>GridView::F_AVG
        ],
        [
            'attribute'=>'allocated_amount',
            'width'=>'150px',
            'hAlign'=>'right',
            'format'=>['decimal', 0],
            'pageSummary'=>true
        ],
        /*[
            'class'=>'kartik\grid\FormulaColumn',
            'header'=>'Amount In Stock',
            'value'=>function ($model, $key, $index, $widget) {
                $p = compact('model', 'key', 'index');
                return $widget->col(4, $p) * $widget->col(5, $p);
            },
            'mergeHeader'=>true,
            'width'=>'150px',
            'hAlign'=>'right',
            'format'=>['decimal', 2],
            'pageSummary'=>true
        ],*/
    ],
]);


?>
</div>