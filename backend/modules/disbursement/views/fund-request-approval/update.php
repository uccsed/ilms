<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\InstitutionFundRequest */

$this->title = 'Update '.$model->Institution.' Fund Request: ' . $model->LoanItem.' for '.$model->AcademicYear;
$this->params['breadcrumbs'][] = ['label' => 'Institution Fund Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="institution-fund-request-update">
    <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">
            <?= $this->render('_formEdit', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
