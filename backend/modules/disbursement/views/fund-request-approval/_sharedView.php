<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/6/18
 * Time: 5:05 PM
 */
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\disbursement\Module;
$plSQL="SELECT * FROM institution_paylist WHERE institution_paylist.request_id='$model->id'";
$plModel = Yii::$app->db->createCommand($plSQL)->queryAll();


?>


<p>



    <?php
    if (sizeof($plModel)!=0 && $model->submitted==0){


        $class = [
            'class' => 'btn btn-warning btn-xl',
            'data' => [
                'confirm' => 'Are you sure you want to Submit this Request?',
                'method' => 'post',
            ],
        ];

    }else{
        $class = [
            'class' => 'btn btn-warning btn-xl', 'style' => 'display: none;',
            'data' => [
                'confirm' => 'Are you sure you want to Submit this Request?',
                'method' => 'post',
            ],
        ];
    }
    ?>

    <?php
    if ($model->submitted==0){

        $uClass=['class' => 'btn btn-primary'];
        $dClass = [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ];



        $classList = [
            'class' => 'btn btn-warning btn-xl',
            'data' => [
                'confirm' => 'Are you sure you want to Submit this Request?',
                'method' => 'post',
            ],
        ];
    }else{
        $classList = [
            'class' => 'btn btn-warning btn-xl', 'style' => 'display: none;',
            'data' => [
                'confirm' => 'Are you sure you want to Submit this Request?',
                'method' => 'post',
            ],
        ];

        $uClass=['class' => 'btn btn-primary','style' => 'display: none;'];
        $dClass=['class' => 'btn btn-danger','style' => 'display: none;'];
    }

    ?>
    <?= Html::a('Update', ['update', 'id' => $model->id], $uClass) ?>
    <?= Html::a('Delete', ['delete', 'id' => $model->id], $dClass) ?>




    <?= Html::a('Submit', ['submit', 'id' => $model->id], $class);?>

</p>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        //'id',
        'Institution',
        'LoanItem',
       // 'Semester',
        [
          'name'=>'semester_number',
          'value'=>$model->Semester,
          'format'=>'html',
          'label'=>'Semester',
        ],
        'invoice_number',
        'control_number',

        //'academic_year',
        'AcademicYear',
        'description:ntext',
        'Status',
        'status_date',
        [
            'name'=>'approval',
            'value'=>Module::PayListStatus($model->approval),
            'format'=>'raw',
            'label'=>'Approval'
        ],
        [
            'name'=>'approval_date',
            'value'=>date('D d/m/Y',strtotime($model->approval_date)).' '.number_format(Module::span($model->approval_date,date('Y-m-d'),'a')).' days Ago',
            'format'=>'raw',
            'label'=>'Approved on'
        ],
        [
            'name'=>'approved_by',
            'value'=>Module::UserInfo($model->approved_by,'fullName'),
            'format'=>'raw',
            'label'=>'Approved by'
        ],
        'approval_comments',


        [
          'name'=>'status_by',
          'value'=>Module::UserInfo($model->status_by,'fullName'),
          'format'=>'raw',
          'label'=>'Last Processed by'
        ],

        [
            'name'=>'Amount',
            'value'=>$model->Amount,
            'format'=>['decimal',2],
            'label'=>'Request Amount'
        ],

        [
            'name'=>'Beneficiaries',
            'value'=>$model->Beneficiaries,
            'format'=>['decimal',0],
            'label'=>'Beneficiaries'
        ],

        /*'submitted',
        'submitted_on',
        'submitted_by',
        'approval',
        'approval_comments:ntext',
        'approval_date',
        'approved_by',
        'request_status',
        'status_date',
        'status_by',*/
    ],
]) ?>

