<?php


$user = Yii::$app->user->id;
$uSQL="
SELECT 
user.user_id as 'user_id',
staff.staff_id as 'staff_id',
staff.learning_institution_id as 'institution_id'
FROM user 
LEFT JOIN staff ON user.user_id = staff.user_id
WHERE user.user_id='$user' AND user.login_type='4'";
$uModel = Yii::$app->db->createCommand($uSQL)->queryAll();


if (sizeof($uModel)!=0){
    foreach ($uModel as $uIndex=>$uDataArray){
        $myInstitution = $uDataArray['institution_id'];
        $iSQL = "SELECT * FROM learning_institution WHERE learning_institution_id='$myInstitution'";
    }
}

$ulSQL="
SELECT * FROM user WHERE user.user_id='$user' AND user.login_type='5'";
$ulModel = Yii::$app->db->createCommand($ulSQL)->queryAll();
if (sizeof($ulModel)!=0){
    $liSQL="SELECT loan_item.loan_item_id AS 'loan_item', CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'itemName' FROM loan_item WHERE is_active = '1' AND is_requested = '1'";
    $iSQL = "SELECT * FROM learning_institution WHERE learning_institution_id IN(SELECT institution_assignment.institution_id FROM institution_assignment WHERE institution_assignment.officer_id='$user')";

}

$liModel=Yii::$app->db->createCommand($liSQL)->queryAll();
$iModel=Yii::$app->db->createCommand($iSQL)->queryAll();


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\InstitutionFundRequest */
/* @var $form yii\widgets\ActiveForm */

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */
/* @var $form yii\widgets\ActiveForm */

//echo Module::FetchBootstrap('fancyInputs');
echo Module::FetchBootstrap('js');
?>

<?php $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>
<div class="institution-fund-request-form">

    <?= $form->field($model, 'invoice_number')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>


    <div style="display: none;">
        <?= $form->field($model, 'loan_item')->textInput() ?>
        <?= $form->field($model, 'academic_year')->textInput() ?>
        <?= $form->field($model, 'institution_id')->textInput() ?>


        <?= $form->field($model, 'submitted')->textInput() ?>

        <?= $form->field($model, 'submitted_on')->textInput() ?>

        <?= $form->field($model, 'submitted_by')->textInput() ?>

        <?= $form->field($model, 'approval')->textInput() ?>

        <?= $form->field($model, 'approval_comments')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'approval_date')->textInput() ?>

        <?= $form->field($model, 'approved_by')->textInput() ?>

        <?= $form->field($model, 'request_status')->textInput() ?>

        <?= $form->field($model, 'status_date')->textInput() ?>

        <?= $form->field($model, 'status_by')->textInput() ?>
    </div>


    <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?php
        echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
        echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

        ActiveForm::end();
        ?>
    </div>

</div>
