<?php

use yii\helpers\Html;
use kartik\tabs\TabsX;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\InstitutionFundRequest */

$this->title = $model->LoanItem." Request for ".$model->Institution.' for academic year '.$model->AcademicYear;
$this->params['breadcrumbs'][] = ['label' => 'Institution Fund Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$docs = 0;
if (!empty($model->official_document)&&$model->official_document!=''){
    $docs = sizeof((ARRAY)json_decode($model->official_document));
}
echo Module::FetchBootstrap('js');
?>
<div class="institution-fund-request-view">
    <div class="panel panel-info">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="pannel-body">


            <?php
            $request= $this->render('_sharedView', [
                'model' => $model,

            ]);

            $requestDetails= $this->render('../institution-fund-request/_shared_list', [
                'model' => $model,

            ]);

            if ($model->submitted==1 && $model->request_status=='3'){
                $approval= $this->render('approval', [
                    'model' => $model,

                ]);
            }else{
                $approval = "<h4>This Request has NOT yet been Cleared for Approval</h4>";
            }





            echo TabsX::widget([
                'items' => [
                    [
                        'label' => 'Request Details',
                        'content' =>$request,
                        'id' => '1',
                    ],
                    [
                        'label' => 'Pay List',
                        //'content' => '<iframe src="' . yii\helpers\Url::to(['/disbursement/institution-fund-request/pay-list', 'id' =>$model->id]) . '" width="100%" height="600px" style="border: 0"></iframe>',
                        'content' => $requestDetails,
                        'id' => '2',
                    ],

                    [
                        'label' => 'Supporting Documents ('.number_format($docs).')',
                        'content' => Module::FundRequestEvidence($model->id),
                        'id' => '3',
                    ],

                    [
                        'label' => 'Request Approval',
                        'content' => $approval,
                        'id' => '4',
                    ],
                ],
                'position' => TabsX::POS_ABOVE,
                'bordered' => true,
                'encodeLabels' => false
            ]);
            ?>



        </div>
    </div>
</div>
