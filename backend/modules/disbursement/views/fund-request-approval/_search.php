<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\InstitutionFundRequestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="institution-fund-request-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'institution_id') ?>

    <?= $form->field($model, 'loan_item') ?>

    <?= $form->field($model, 'invoice_number') ?>

    <?= $form->field($model, 'academic_year') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'submitted') ?>

    <?php // echo $form->field($model, 'submitted_on') ?>

    <?php // echo $form->field($model, 'submitted_by') ?>

    <?php // echo $form->field($model, 'approval') ?>

    <?php // echo $form->field($model, 'approval_comments') ?>

    <?php // echo $form->field($model, 'approval_date') ?>

    <?php // echo $form->field($model, 'approved_by') ?>

    <?php // echo $form->field($model, 'request_status') ?>

    <?php // echo $form->field($model, 'status_date') ?>

    <?php // echo $form->field($model, 'status_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
