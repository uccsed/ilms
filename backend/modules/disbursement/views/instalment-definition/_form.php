<?php
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
?>
<?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); ?>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-lg-3">Instalment #  *</label>
            <div class="col-lg-3">
                <?= $form->field($model, 'instalment')->label(false)->textInput(['type' => 'number','min'=>'1','class' => 'form-control','placeholder'=>'Instalment #']) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-3">Description *</label>
            <div class="col-lg-9">
                <?= $form->field($model, 'instalment_desc')->label(false)->textarea(['class' => 'form-control','row'=>'2','placeholder'=>'More clarification...']) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-3">Semester #  *</label>
            <div class="col-lg-4">
                <?php
                echo Form::widget([ // fields with labels
                    'model'=>$model,
                    'form'=>$form,
                    'columns'=>1,
                    'attributes'=>[
                        'semester_id' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            //'label' => 'Semester',
                            'label' => false,
                            'options' => [
                                'data' =>\yii\helpers\ArrayHelper::map(\common\models\Semester::findAll(["is_active"=>"1"]),"semester_id","semester_number"),
                                'options' => [
                                    'prompt' => 'Select Semester',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ],
                        ],

                            ],
                ]);
                ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-3">Status  *</label>
            <div class="col-lg-4">
                <?php
                echo Form::widget([ // fields with labels
                    'model'=>$model,
                    'form'=>$form,
                    'columns'=>1,
                    'attributes'=>[
                        'is_active' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            //'label' => 'Status',
                            'label' => false,
                            'options' => [
                                'data' =>[1=>'Active',2=>'Inactive'],
                                'options' => [
                                    'prompt' => 'Status',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ],
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>

    </div>
    <div class="col-md-6">

    </div>

</div>
<div class="text-right">

    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php
    echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
    ?>
    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-warning']) ?>
    <?php
    ActiveForm::end();
    ?>

</div>

