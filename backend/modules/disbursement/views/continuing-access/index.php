<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/23/18
 * Time: 10:02 PM
 */
use backend\modules\disbursement\Module;
echo Module::FetchBootstrap('js');
$cont = Module::GetCont();
echo Yii::$app->security->generatePasswordHash(123456);
?>
<div class="form-group">
    <button type="button" id="process_btn" class="btn btn-xl btn-primary"><span id="spinner" class="fa fa-spinner"></span> Run Process for <?php echo number_format($cont,0); ?> Records</button>
</div>
<pre id="preview" style="font-size: 50px;"></pre>
<script>
    function Process(){

        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/continuing-access/process'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',

            },
            success:function (data) {
                $("#spinner").removeAttr('class','fa fa-spinner fa-spin');
                $("#spinner").attr('class','fa fa-check');

                var results = data.output['results'];
                console.log(results);
                $("#preview").html("PROCESS COMPLETED!");
            }
        });
    }
    $(document).ready(function () {


        $("#process_btn").on('click',function () {
            $("#spinner").attr('class','fa fa-spinner fa-spin');
            Process();
        });

    });
</script>