<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\SuspensionReason */

$this->title = 'Create Suspension Reason';
$this->params['breadcrumbs'][] = ['label' => 'Suspension Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="suspension-reason-create">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>






        </div>
    </div>
</div>
