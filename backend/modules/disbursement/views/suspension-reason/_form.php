<?php
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\VerificationAssignment */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>
<div class="row">
    <div class="col-md-6">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' =>1,
            'attributes' => [


                'type' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    'label' => 'Type Of Reason',
                    'options' => [
                        'data' =>Module::SuspensionType() ,

                        //'data' =>ArrayHelper::map(\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),

                        'options' => [
                            'prompt' => 'Select Type',
                            //'multiple'=>TRUE,

                        ],
                    ],
                ],

                //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
            ]
        ]);
        ?>
        <?= $form->field($model, 'name')->textInput(['class'=>'form-control','maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['class'=>'form-control','rows' => 6]) ?>
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' =>1,
            'attributes' => [


                'is_active' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    'label' => 'Is Active?',
                    'options' => [
                        'data' =>['1'=>'Active','0'=>'Inactive'] ,

                        //'data' =>ArrayHelper::map(\backend\modules\application\models\VerificationCommentGroup::findBySql('SELECT verification_comment_group_id,comment_group FROM `verification_comment_group`')->asArray()->all(), 'verification_comment_group_id', 'comment_group'),

                        'options' => [
                            'prompt' => 'Select State',
                            //'multiple'=>TRUE,

                        ],
                    ],
                ],

                //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
            ]
        ]);
        ?>
    </div>


</div>





<div class="text-right">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php
    echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
    echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

    ActiveForm::end();
    ?>
</div>


