<?php
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]);
?>


<div class="col-md-6">
    <div class="form-group">
        <?php echo Form::widget([ // fields with labels
            'model'=>$model,
            'form'=>$form,
            'columns'=>1,
            'attributes'=>[
                //'semester_number'=>['label'=>'semester_number', 'options'=>['class'=>'span3','placeholder'=>'Semester Number']],
                //'description'=>['type' => Form::INPUT_TEXTAREA,'label'=>'Description', 'options'=>['placeholder'=>'']],
                'is_requested' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => \kartik\select2\Select2::className(),
                    'label' => 'Is requested by Institution?',
                    'options' => [
                        'data' =>[1=>'YES',0=>'NO'],
                        'options' => [
                            'prompt' => 'Request Status',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ],
                ],

            ]
        ]);

        ?>
    </div>
</div>

  <div class="text-right">
        
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  
<?php
echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-warning']) ?>
      <?php
ActiveForm::end();
?>
 
    </div>
