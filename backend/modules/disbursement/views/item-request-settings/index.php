<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SemesterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Loan Item Request Settings';
$this->params['breadcrumbs'][] = $this->title;

/*
 *  * @property integer $loan_item_id
 * @property string $item_name
 * @property string $item_code
 * @property double $day_rate_amount
 * @property integer $is_active
 *
 *
 */
?>
<div class="loan-item-index">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'semester_id',
                    'item_code',
                    'item_name',

                    [
                        'attribute' => 'is_active',
                        'vAlign' => 'middle',
                        //'width' => '200px',
                        'value' => function ($model) {
                            return $model->is_active==1?'Active':'Inactive';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' =>[1=>'Active',2=>'Inactive'],
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search'],
                        'format' => 'raw'
                    ],

                    [
                        'attribute' => 'is_requested',
                        'vAlign' => 'middle',
                        //'width' => '200px',
                        'value' => function ($model) {
                            return $model->is_requested==1?'YES':'NO';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' =>[1=>'YES',0=>'NO'],
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search'],
                        'format' => 'raw'
                    ],
                    //'is_active',

                    //['class' => 'yii\grid\ActionColumn'],
                    ['class' => 'yii\grid\ActionColumn',
                        'template'=>'{update}',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
