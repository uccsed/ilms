<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/9/18
 * Time: 3:17 PM
 */
use yii\helpers\Html;
use kartik\tabs\TabsX;
use backend\modules\disbursement\Module;
$this->title = strtoupper($mode->loanItem->item_name." DISBURSEMENT SUMMARY FOR ".$model->learningInstitution->institution_name." FOR ".$model->academicYear->academic_year);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'List Review Disbursement'), 'url' => ['/disbursement/disbursement-batch/reviewall-disbursement']];
$this->params['breadcrumbs'][] = $this->title;
?>


    <div class="panel panel-info">
        <div class="panel-heading">
            <h5><?= Html::encode($this->title) ?></h5>
        </div>
        <div class="panel-body">
            <?php
            $batchdetails= $this->render('view-reviewall-disbursement', [
                'model' => $model,

            ]);
            $payList= $this->render('_payoutPDF', [
                'model' => $model,

            ]);
            //$payList.= Module::MemoSummary($mode->disbursement_batch_id);
            echo TabsX::widget([
                'items' => [
                    [
                        'label' => 'Summary',
                        'content' =>$batchdetails,
                        'id' => '1',
                    ],
                    [
                        'label' => 'Details',
                        'content' =>$payList,
                        'id' => '2',
                    ],
                    /*[
                        'label' => 'Details',
                        'content' => '<iframe src="' . yii\helpers\Url::to(['/disbursement/disbursement/shared-list', 'id' =>$model->disbursement_batch_id]) . '" width="100%" height="600px" style="border: 0"></iframe>',
                        'id' => '2',
                    ],*/
                    [
                        'label' => 'Payout List Movement',
                        'content' => '<iframe src="' . yii\helpers\Url::to(['/disbursement/payoutlist-movement/list-movement', 'id' =>$model->disbursement_batch_id]) . '" width="100%" height="600px" style="border: 0"></iframe>',
                        'id' => '3',
                    ],
                ],
                'position' => TabsX::POS_ABOVE,
                'bordered' => true,
                'encodeLabels' => false
            ]);
            ?>
        </div>

    </div>



