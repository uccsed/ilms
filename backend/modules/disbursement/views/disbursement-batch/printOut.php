<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/17/18
 * Time: 2:29 PM
 */
$amount_total=0;
$header_id=1;
$learning_institution_ids_s=0;
$amount_bottom_array=array();
$sn = 0;
$mpdf = new mPDF('c','A4-L','','',5,5,30,25,10,10);
use backend\modules\disbursement\Module;
use Yii;
$batchID = $model->disbursement_batch_id;


$batchCourse = Module::DBatchCourse($batchID);
$html = '';
?>


<?php foreach ($batchCourse as $index=>$dataArray){ $courseID = $dataArray['group_id']; ?>
<?php
$html.= '
<html>
<head>
<style>
body {font-family: sans-serif;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0mm solid #000000;
	border-right: 0mm solid #000000;
}
table thead td { background-color: #EEEEEE;
	text-align: center;
	border: 0mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: right;
	font-weight: bolder;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
</style>
</head>

<body>
';
?>






<?php
$html.='<p style=" font-family: \'Courier New\';">';
$html.='
<h3 style="text-align: center; font-family: \'Courier New\';">HIGHER EDUCATION STUDENTS LOAN BOARD</h3>
      <table width="100%" style=" font-family: \'Courier New\';">
        <tr><th colspan="3" style="text-align: center; font-family: \'Courier New\';">LOAN PAYOUT SCHEDULE FOR '.strtoupper($model->Items).'</th></tr>
        <tr><th colspan="2" style="text-align: center; font-family: \'Courier New\';">Schedule printed on : '. date("l F jS Y H:i:s").'</th> <th>Payout Reference Number : '.$model->batch_number.'</th></tr>
        <tr><th style=" font-family: \'Courier New\';">INSTITUTION: '.strtoupper($model->learningInstitution->institution_code).'</th><th> ACADEMIC YEAR: '. strtoupper($model->academicYear->academic_year).' </th> <th>'. strtoupper($model->Instalment).'</th></tr>
        <tr><th colspan="3" style="text-align: left; font-family: \'Courier New\';">'.$dataArray["group_code"].'</th></tr>
    </table>
';
$html.='</p>';
    $html.=Module::DynamicPayoutList($batchID,$courseID,$sn);
    $sn+=$dataArray['count'];
$html.='<pagebreak>';
    //$mpdf->AddPage();
    //$mpdf->WriteHTML($html);
?>


<?php } ?>

<?php
$html.='<p style=" font-family: \'Courier New\';">';
$html.='
<h3 style="text-align: center; font-family: \'Courier New\';">HIGHER EDUCATION STUDENTS LOAN BOARD</h3>
      <table width="100%" style=" font-family: \'Courier New\';">
      
        <tr><th colspan="2" style="text-align: center; font-family: \'Courier New\';">Schedule printed on : '. date("l F jS Y H:i:s").'</th> <th>Payout Reference Number : '.$model->batch_number.'</th></tr>
        <tr><th style=" font-family: \'Courier New\';">INSTITUTION: '.strtoupper($model->learningInstitution->institution_code).'</th><th> ACADEMIC YEAR: '. strtoupper($model->academicYear->academic_year).' </th> <th>'. strtoupper($model->Instalment).'</th></tr>
        <tr><th colspan="3" style="text-align: left; font-family: \'Courier New\';">GRAND SUMMARY PER YOS</th></tr>
    </table>
';
$html.=Module::PLS($batchID);
$html.='</p>';
$html.=$tbl;


?>

<?php
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle("heslb");
$mpdf->SetAuthor("heslb");
$mpdf->SetWatermarkText("HESLB");
$mpdf->showWatermarkText = true;
  /*  $mpdf->SetWatermarkImage('../images/background.jpg');
    $mpdf->showWatermarkImage = true;*/
$mpdf->watermark_font = 'DejaVuSansCondensed';
$mpdf->watermarkTextAlpha = 0.1;
$mpdf->SetDisplayMode('fullpage');
$mpdf->setFooter('|{PAGENO} of {nbpg}|');

?>


<?php
$html.= '
</body>
</html>
';
?>
<?php
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

exit;
?>
