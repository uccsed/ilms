<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/14/18
 * Time: 7:12 PM
 */
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\models\DisbursementSearch;
use backend\modules\disbursement\Module;
?>

<?php
//echo $batch_id;
$searchModel = new DisbursementSearch();
$dataProvider = $searchModel->SummarySearch($batch_id);
$dataProvider->pagination->pageSize=0;
//batchDisbursement
$gridColumns =[
    [
        'class' => 'kartik\grid\SerialColumn',
        'hAlign' => GridView::ALIGN_CENTER,
    ],


    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'allowBatchToggle' => true,
        'detail' => function ($model, $key, $index, $column) { return $this->render('_applicant_grid',['batch_id'=>$model['disbursement_batch_id'],'application_id'=>$model['application_id']]);},
        'detailOptions' => [
            'class' => 'kv-state-enable',
        ],
    ],

    [
        'attribute' => 'index_number',
        'label'=>"f4 Index #",
        'vAlign' => 'middle',
        'width' => '200px',

    ],

    [
        'attribute' => 'full_name',
        'label'=>"Names",
        'vAlign' => 'middle',
        'width' => '200px',

    ],

    [

        'attribute' => 'sex',
        'label'=>"Sex",
        'vAlign' => 'middle',
        'width' => '200px',

    ],

    [
        'attribute' => 'yos',
        'label'=>"YoS",
        'vAlign' => 'middle',
        'width' => '200px',
    ],

    [
        'attribute' => 'institution',
        'label'=>"Institution",
        'vAlign' => 'middle',
        'width' => '200px',
    ],
    [
        'attribute' => 'programme_code',
        'vAlign' => 'middle',
        'width' => '200px',
    ],

    [
        'attribute' => 'disbursed_amount',
        'hAlign'=>'right',
        /*'value'=>function($model){
            return $model->disbursed_amount;
        },*/
        'format'=>['decimal', 2],
        //'label'=>"Status",
        'width' => '200px',
        'pageSummary'=>true,
    ],
    //'status',
    // 'application.applicant.f4indexno',
    // 'created_at',
    // 'created_by',

    /*['class' => 'yii\grid\ActionColumn',
        'template'=>'{update}{delete}'],*/
];
?>


<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'showPageSummary'=>true,
    'pageSummaryRowOptions'=>['class'=>'text-bold bg-blue'],
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'condensed' => true,
    'floatHeader' => true,
]);

?>
