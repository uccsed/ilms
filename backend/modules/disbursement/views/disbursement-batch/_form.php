<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementBatch */

/* @var $form yii\widgets\ActiveForm */
use backend\modules\disbursement\Module;
use backend\modules\disbursement\models\InstitutionFundRequest;


echo Module::FetchBootstrap('fancyInputs');
echo Module::FetchBootstrap('dashboard');
$user=Yii::$app->user->id;
$iSQL="SELECT * FROM learning_institution WHERE institution_type = 'UNIVERSITY' AND learning_institution_id IN (SELECT institution_assignment.institution_id FROM institution_assignment WHERE institution_assignment.officer_id='$user')";
$InstitutionModel = Yii::$app->db->createCommand($iSQL)->queryAll();
$requestSQL = "
SELECT 
institution_fund_request.id AS 'id',
CONCAT(learning_institution.institution_code,' | ',loan_item.item_name,' - ',academic_year.academic_year) AS 'name'
FROM institution_fund_request 
LEFT JOIN  loan_item ON institution_fund_request.loan_item = loan_item.loan_item_id
LEFT JOIN  learning_institution ON institution_fund_request.institution_id = learning_institution.learning_institution_id
LEFT JOIN  academic_year ON institution_fund_request.academic_year = academic_year.academic_year_id

WHERE institution_fund_request.request_status  IN (6,7) AND 
institution_fund_request.institution_id IN (SELECT institution_assignment.institution_id FROM institution_assignment WHERE institution_assignment.officer_id='$user')";
$frModel = Yii::$app->db->createCommand($requestSQL)->queryAll();

$empSQL = "
SELECT
 employer.employer_id  AS 'id',
CONCAT(employer.employer_code,' | ',employer.employer_name,' (',employer.short_name,')')  AS 'name'
FROM employer WHERE has_contract = '1'
ORDER BY employer.employer_name ASC;
";


$empModel = Yii::$app->db->createCommand($empSQL)->queryAll();


$model->disbursed_as = 1; //Loan
?>

 <script type="text/javascript">

 /********************
  * Id can be hide or show
  *
	*onload functions
	********************/
	window.onload = start;
	function start () {
		//alert("mickidadi");
		body();
		 
	}
     function body() { 
   var cat= document.getElementById('employerId').style.display = 'none';
   var cat= document.getElementById('additionVersionId').style.display = 'none';
   var cat1= document.getElementById('countryId').style.display = 'none';
  // var cat3= document.getElementById('disburseTypeId').style.display = 'none';
   var cat4= document.getElementById('InstitutionId').style.display = 'none';
  // var cat5= document.getElementById('disbursementbatch-disburse_type').style.display = 'none';
    var cat1= document.getElementById('loaderId').style.display = 'none';
   //disbursementbatch-disburse
     updateStatus();
        }



       /* function updateStatus() {

        }*/


   function updateStatus(){
       
          var category= document.getElementById('disbursementbatch-applicant_category_id').value;
             if(category==1){ //Local
             var cat1= document.getElementById('countryId').style.display = 'none';         
                  }
                else if(category==2){ // Overseas
          var cat1= document.getElementById('countryId').style.display = '';          
                }
          var level= document.getElementById('disbursementbatch-level').value;
            /* if(level==2){
             var cat1= document.getElementById('InstitutionId').style.display = 'none'; 
              var cat= document.getElementById('employerId').style.display = '';
                  }
                else if(level==1){
          var cat1= document.getElementById('InstitutionId').style.display = ''; 
           var cat= document.getElementById('employerId').style.display = 'none';
                }*/


            if (level==1){
                var cat1= document.getElementById('InstitutionId').style.display = 'block';
                var cat= document.getElementById('employerId').style.display = 'none';
            }else {
                var cat1= document.getElementById('InstitutionId').style.display = 'block';
                var cat= document.getElementById('employerId').style.display = 'block';
            }
          
             var typevalue= document.getElementById('disbursementbatch-instalment_type').value;
             if(typevalue==1){
             var cat1= document.getElementById('additionVersionId').style.display = 'none';         
                  }
                else if(typevalue>1){
          var cat1= document.getElementById('additionVersionId').style.display = '';          
                }
       //var paylist=1;
          var paylist= document.getElementById('disbursementbatch-disburse_type').value;
             if(paylist==1){
             var cat1= document.getElementById('paylistId').style.display = 'none';         
                  }
                else if(paylist>1){
          var cat1= document.getElementById('paylistId').style.display = '';          
                }       
    }
  
</script>
<style>
div.disabled {
display: none;
}
</style>
<?php
//echo Yii::$app->getUrlManager()->createUrl('/disbursement/disbursement-batch/create');
/*$this->registerJs( "
        $('body').on('beforeSubmit', 'form#myform', function () {
             var form = $(this);
            
//                        var property = document.getElementById('disbursementbatch-file').files[0];
//                        var image_name = property.name;
//                                 
//                        var image_extension = image_name.split('.').pop().toLowerCase();
//
////                        if(jQuery.inArray(image_extension,['gif','jpg','jpeg','']) == -1){
////                          alert('Invalid image file');
////                        }
                        var formData = new FormData(this);
                        //formData.append('file',property);
                       
             // return false if form still have some validation errors
             if (form.find('.has-error').length) {
                  return false;
             }
            document.getElementById('loaderId').style.display = '';
            document.getElementById('formId').style.display = 'none';
          // alert(form.attr('action'));
             // submit form
               $.ajax({
                            type   : 'post',
                            //dataType: 'json',
                            url: form.attr('action'),
                            data:formData,
                                processData: false,
                                contentType: false,
                            success: function (data) {
                               alert(data);
                          document.getElementById('formId').style.display = '';
                          document.getElementById('loaderId').style.display = 'none';
                           //  $('#displaysummary').html(data);
                            }
                          

                             }) ;
             return false;
        }); ");*/
?>





<div class="row">
    <div class="col-md-4">
        <div class="disbursement-batch-form" id="formId">

            <?php $form = ActiveForm::begin(['id' => 'myform','options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="profile-user-info profile-user-info-striped">
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Disbursement Source:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                          <?php

                            echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'disburse_type' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                        'label' => '',

                                        'options' => [
                                            'data' =>  [1=>"Lonee Items",2=>"Institution Request"],
                                            'options' => [
                                                'prompt' => 'Select Disburse Options',
                                                'onchange'=>'updateStatus()'

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>

                <div class="profile-info-row" id="paylistId" style="display: none;">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Payment Request:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">


                            <?php
                            echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'institution_payment_request_id' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                         'label' => '',

                                        'options' => [
                                            'data' => ArrayHelper::map($frModel, 'id', 'name'),
                                            'options' => [
                                                'prompt' => 'Select Request',

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>





                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Loanee Category:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                           <?php

                            echo Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'columns' =>1,
                            'attributes' => [


                            'applicant_category_id' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            //  'label' => 'Payment Request',
                            'label' => '',

                            'options' => [
                            'data' =>  [1=>"Local",2=>"OverSeas"],
                            'options' => [
                            'prompt' => 'Select Loanee Category',
                            'onchange'=>'updateStatus()'

                            ],
                            ],
                            ],



                            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                            ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row" id="countryId">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Country:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">



                            <?php
                            echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'country_id' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                        'label' => '',

                                        'options' => [
                                            'data' =>  ArrayHelper::map(\frontend\modules\application\models\Country::findBySql("SELECT * FROM country WHERE country_code <> 'TZA' OR  country_code IS NULL ORDER BY country_name ASC")->asArray()->all(),'country_id','country_name'),
                                            'options' => [
                                                'prompt' => 'Select Country',
                                                'onchange'=>'updateStatus()'

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Study Level:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">

                            <?php
                            echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'level' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                        'label' => '',

                                        'options' => [
                                            'data' => ArrayHelper::map(backend\modules\application\models\ApplicantCategory::find()->all(),'applicant_category_id','applicant_category'),
                                            'options' => [
                                                'prompt' => 'Select Study Level',
                                                'onchange'=>'updateStatus()'

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row" id="employerId">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Employer:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">


                            <?php
                            echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'employer_id' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                        'label' => '',

                                        'options' => [
                                            'data' => ArrayHelper::map($empModel, 'id', 'name'),
                                            'options' => [
                                                'prompt' => 'Select Employer',
                                                'onchange'=>'updateStatus()'

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>







                <div class="profile-info-row" id="InstitutionId">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Institution:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <div id="learning_institution_id_display"></div>

                            <?php
                            /*echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'learning_institution_id' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                        'label' => '',

                                        'options' => [
                                            'data' => ArrayHelper::map($InstitutionModel,'learning_institution_id','institution_name'),
                                            'options' => [
                                                'prompt' => 'Select Institution',
                                                'onchange'=>'updateStatus()'

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);*/


                            echo $form->field($model, 'learning_institution_id')->widget(DepDrop::classname(), [
                                'type'=>DepDrop::TYPE_SELECT2,
                            'options'=>['id'=>'disbursementbatch-learning_institution_id'],
                            //'onchange'=>'updateStatus()',
                            'pluginOptions'=>[
                            'depends'=>['disbursementbatch-applicant_category_id','disbursementbatch-country_id'],
                            'placeholder'=>'Select Learning Institution',
                            'url'=>Url::to(['/disbursement/disbursement-batch/assigned-institutions'])
                            ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Academic Year:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <div id="academic_year_display"></div>
                            <?php
                            echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'academic_year_id' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                        'label' => '',

                                        'options' => [
                                            'data' => ArrayHelper::map(\common\models\AcademicYear::findBySql("SELECT * FROM academic_year ORDER BY is_current DESC, academic_year DESC")->asArray()->all(),'academic_year_id','academic_year'),
                                            'options' => [
                                                'prompt' => 'Select Academic Year',
                                                'onchange'=>'updateStatus()'

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Financial Year:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                          <?php
                            echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'financial_year_id' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                        'label' => '',

                                        'options' => [
                                            'data' => ArrayHelper::map(\backend\modules\disbursement\models\FinancialYear::findBySql("SELECT * FROM financial_year ORDER BY is_active DESC, financial_year DESC")->asArray()->all(),'financial_year_id','financial_year'),
                                            'options' => [
                                                'prompt'=>'Select Financial Year',
                                                'onchange'=>'updateStatus()'

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>

                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Semester :</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?php
                            echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'semester_number' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                        'label' => '',

                                        'options' => [
                                            'data' => Module::Semester(),
                                            'options' => [
                                                'prompt'=>'Select Semester',
                                                'onchange'=>'updateStatus()'

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>

                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Instalment :</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">

                            <?php
                          /*  echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'instalment_definition_id' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                        'label' => '',

                                        'options' => [
                                            'data' => ArrayHelper::map(\backend\modules\disbursement\models\InstalmentDefinition::find()->where(["is_active"=>1])->orderBy(['instalment'=>'ASC'])->all(),'instalment_definition_id','instalment'),
                                            'options' => [
                                                'prompt'=>'Select Instalment',
                                                'onchange'=>'updateStatus()'

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);*/
                            ?>

                            <?php
                            echo $form->field($model, 'instalment_definition_id')->widget(DepDrop::classname(), [
                                'type'=>DepDrop::TYPE_SELECT2,
                               'options'=>['id'=>'disbursementbatch-instalment_definition_id'],
                                //'onchange'=>'updateStatus()',
                                'pluginOptions'=>[
                                    'depends'=>['disbursementbatch-semester_number'],
                                    'placeholder'=>'Select Instalment',
                                    'url'=>Url::to(['/disbursement/disbursement-batch/filtered-instalments'])
                                ]
                            ])->label(false);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Loan Item:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <div id="loan_item_display"></div>
                            <!--disburse_type,applicant_category_id,level-->
                            <?php
                            echo $form->field($model, 'loan_item_id')->widget(DepDrop::classname(), [
                                'type'=>DepDrop::TYPE_SELECT2,
                                'options'=>['id'=>'disbursementbatch-loan_item_id'],
                                //'onchange'=>'updateStatus()',
                                'pluginOptions'=>[
                                    'depends'=>['disbursementbatch-disburse_type','disbursementbatch-applicant_category_id','disbursementbatch-level'],
                                    'placeholder'=>'Select Loan Item',
                                    'url'=>Url::to(['/disbursement/disbursement-batch/filtered-items'])
                                ]
                            ]);
                        /*    echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'loan_item_id' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                        'label' => '',

                                        'options' => [
                                            'data' => ArrayHelper::map(backend\modules\allocation\models\LoanItem::findAll(["is_active"=>1]),'loan_item_id','item_name'),
                                            'options' => [
                                                'prompt'=>'Select Loan Item',
                                                'onchange'=>'updateStatus()'

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);*/
                            ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row" style="display: none;">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Loan Category:</label>
                    </div>

                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Type :</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">

                            <?php
                            echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'instalment_type' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                        'label' => '',

                                        'options' => [
                                            'data' => [1=>"Normal",2=>"Additional"],
                                            'options' => [
                                                'prompt'=>'Select Instalment Type',
                                                'onchange'=>'updateStatus()'

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row" id="additionVersionId">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Additional Version:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">


                            <?php
                            echo Form::widget([
                                'model' => $model,
                                'form' => $form,
                                'columns' =>1,
                                'attributes' => [


                                    'version' => ['type' => Form::INPUT_WIDGET,
                                        'widgetClass' => \kartik\select2\Select2::className(),
                                        //  'label' => 'Payment Request',
                                        'label' => '',

                                        'options' => [
                                            'data' =>  [1=>"Version 1",2=>"Version 2",3=>"Version 3",4=>"Version 4",5=>"Version 5",6=>"Version 6"],
                                            'options' => [
                                                'prompt' => 'Select Additional Version',
                                                'onchange'=>'updateStatus()'

                                            ],
                                        ],
                                    ],



                                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>

                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Process Duration:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'process_days')->label(false)->textInput(['class' => 'form-control','value'=>'14']) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Description:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'batch_desc')->label(false)->textInput(['maxlength' => true]) ?>

                            <?php
                            $strDigits = date('YmdHis').Yii::$app->user->id;
                           $batch_number = intval(Module::HeaderIDAlgorithm($strDigits));
                            if (strtoupper(Yii::$app->controller->action->id) == strtoupper('loan')){
                                echo $form->field($model, 'batch_number')->label(false)->hiddenInput(["value"=>$batch_number]);
                            }


                            echo $form->field($model, 'disbursed_as')->label(false)->hiddenInput(["value"=>1]) ;
                            echo $form->field($model, 'disburse_model')->label(false)->hiddenInput(["value"=>0]) ;

                            ?>
                            <?= $form->field($model, 'created_at')->label(false)->hiddenInput(["value"=>date("Y-m-d")]) ?>
                            <?= $form->field($model, 'allocation_batch_id')->label(false)->hiddenInput() ?>
                            <?= $form->field($model, 'created_by')->label(false)->hiddenInput(["value"=>\yii::$app->user->identity->user_id]) ?>
                        </div>
                    </div>
                </div>
            </div>




            <div class="space10"></div>
            <div class="col-sm-12">
                <div class="form-group button-wrapper" id="confirmButton">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['id'=>'createBtn','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <div id="loadingDiv">
                    <!--<img src="image/loader/loader.gif" />
   <img src="image/loader/loader1.gif" />-->
                    <img src="image/loader/loader2.gif"  />
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
    <div class="col-md-8">

        <!--<div class="col-md-5">-->
            <div class="panel panel-flat">
                <div class="panel-head"><h4 class="text-bold heading-text">Disbursement Plan</h4></div>
                <div class="panel-body pre-scrollable" style="height: 250px;">
                    <table class="table table-condensed table-bordered table-responsive">
                        <thead>
                        <tr class="bg-primary text-uppercase">
                            <th>Programme</th>
                            <th>Year of Study</th>
                            <th>Semester #</th>
                            <th class="text-right">Disbursement %</th>
                        </tr>
                        </thead>
                        <tbody id="plan_table"></tbody>
                    </table>
                </div>

            </div>

        <div class="panel panel-flat">
            <div class="panel-head"><h4 class="text-bold heading-text">Allocation</h4></div>
            <div class="panel-body pre-scrollable" style="height: 250px;">
                <table class="table table-condensed table-bordered">
                    <thead>
                    <tr class="bg-primary text-uppercase">
                        <th>Item</th>
                        <th>Programme</th>
                        <th class="text-right">Lonees</th>
                        <th class="text-right">Allocated Amount</th>
                    </tr>
                    </thead>
                    <tbody id="allocation_table"></tbody>
                </table>
            </div>

        </div>
        <div class="panel panel-flat">
            <div class="panel-head"><h4 class="text-bold heading-text">Disbursement <span class="text-green pull-right" id="requested_amount"></span></h4></div>
            <div class="panel-body pre-scrollable" style="height: 250px;">
                <table class="table table-condensed table-bordered">
                    <thead>
                    <tr class="bg-primary text-uppercase">
                        <th>S/N</th>
                        <th class="">Lonee</th>
                        <th>Programme</th>
                        <th>YOS</th>
                        <th>Item</th>
                        <th class="text-right">Allocated Amount</th>
                        <th class="text-right">Disbursement %</th>
                        <th class="text-right">Disbursement Amount</th>
                    </tr>
                    </thead>
                    <tbody id="disbursement_table"></tbody>
                </table>
            </div>

        </div>
        <!--</div>-->
       <!-- <div class="col-md-7">
            <div class="panel panel-flat">
                <div class="panel-head"><h4 class="text-bold heading-text">Financial Implication</h4></div>
                <div class="panel-body" id="financial_implication">

                </div>

            </div>
        </div>-->

    </div>
</div>




<div id="loaderId" style="display:none">
 
  <?php echo Html::img('@web/image/loader/loader2.gif') ?>  
</div>

<div class="space10"></div>


<script>
    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }

function LocalSearch(){
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-batch/local-search'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                type:$("#disbursementbatch-applicant_category_id").val()
            },
            success:function (data) {
                var results = data.output['results'][0];
                console.log(results.id);
                $("#disbursementbatch-country_id").val(results.id);
            }
        });
}



    function RequestMagic() {
        var requestID = jQuery("#disbursementbatch-institution_payment_request_id").val();
        jQuery.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-batch/request-data'); ?>",
            type:"POST",
            cache:false,
            data:{
                csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                requestID:requestID
            },
            success:function (data) {

                var REQUEST = data.output['request'][0];
                var PAYLIST = data.output['payList'];


                var institution =  REQUEST.institution_id;
                var item =  REQUEST.loan_item;
                var academic =  REQUEST.academic_year;
               var requestTOTAL = 0;

                jQuery.each(PAYLIST,function (index,value) {
                    requestTOTAL+=(value.allocated_amount*1);
                });

                jQuery("#requested_amount").html('TOTAL REQUESTED AMOUNT: '+commaSeparateNumber(requestTOTAL));

                jQuery("#disbursementbatch-learning_institution_id").val(institution);
                jQuery("#disbursementbatch-loan_item_id").val(item);
                jQuery("#disbursementbatch-academic_year_id").val(academic);


                jQuery("#disbursementbatch-learning_institution_id").attr("readonly","readonly");
                jQuery("#disbursementbatch-loan_item_id").attr("readonly","readonly");
                jQuery("#disbursementbatch-academic_year_id").attr("readonly","readonly");


                jQuery("#learning_institution_id_display").html(jQuery("#disbursementbatch-learning_institution_id option:selected").text());
                jQuery("#disbursementbatch-learning_institution_id").hide();

                jQuery("#academic_year_display").html(jQuery("#disbursementbatch-academic_year_id option:selected").text());
                jQuery("#disbursementbatch-academic_year_id").hide();

                jQuery("#loan_item_display").html(jQuery("#disbursementbatch-loan_item_id option:selected").text());
                jQuery("#disbursementbatch-loan_item_id").hide();

                CheckPlan();


            }
        });
    }


    function Plan() {
        var spinner = "<tr><td colspan='3'><div style='font-size: large;'><span class='fa fa-spinner fa-spin fa-2x'></span> Please Wait... Loading Data...</div></td></tr>";
        $("#plan_table").html(spinner);

        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-batch/plan'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                institution:jQuery("#disbursementbatch-learning_institution_id").val(),
                loan_item:jQuery("#disbursementbatch-loan_item_id").val(),
                academic_year:jQuery("#disbursementbatch-academic_year_id").val(),
                installment:jQuery("#disbursementbatch-instalment_definition_id").val(),
                semester:jQuery("#disbursementbatch-semester_number").val(),
                programme_category:jQuery("#disbursementbatch-level").val(),
                country:jQuery("#disbursementbatch-country_id").val(),
                location:jQuery("#disbursementbatch-applicant_category_id").val(),


            },
            success:function (data) {
                jQuery('#plan_table').html("");
                Allocation(data.output['results']);

                jQuery.each(data.output['results'],function(index,value) {

                        jQuery.each(value,function(indx,dataObject) {

                        jQuery('#plan_table').append(
                            '<tr>' +
                                '<td>' +dataObject.programme_name+'</td>'+
                                '<td>' +dataObject.year_of_study+'</td>'+
                                '<td>' +dataObject.semester_number+'</td>'+
                                '<td class="text-right">' +dataObject.disbursement_percent+'</td>'+
                            '</tr>'

                           /* '<tr>' +
                            '<td>' +value.programme_name+'</td>'+
                            '<td>' +value.year_of_study+'</td>'+
                            '<td>' +value.semester_number+'</td>'+
                            '<td class="text-right">' +value.disbursement_percent+'</td>'+
                            '</tr>'*/
                        );
                    });
                });

            }
        });
    }

    function Allocation(plan) {
        var spinner = "<tr><td colspan='3'><div style='font-size: large;'><span class='fa fa-spinner fa-spin fa-2x'></span> Please Wait... Loading Data...</div></td></tr>";
        $("#allocation_table").html(spinner);

        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-batch/allocation'); ?>",

            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                institution:jQuery("#disbursementbatch-learning_institution_id").val(),
                loan_item:jQuery("#disbursementbatch-loan_item_id").val(),
                academic_year:jQuery("#disbursementbatch-academic_year_id").val(),
                installment:jQuery("#disbursementbatch-instalment_definition_id").val(),
                semester:jQuery("#disbursementbatch-semester_number").val(),
                programme_category:jQuery("#disbursementbatch-level").val(),
                country:jQuery("#disbursementbatch-country_id").val(),
                location:jQuery("#disbursementbatch-applicant_category_id").val(),

            },
            success:function (data) {


                var totalLoanees = 0;
                var totalAllocation = 0;
                jQuery('#allocation_table').html("");
                //console.log(data.output['results']);
                jQuery.each(data.output['results'],function (index,value) {

                    totalAllocation+=(value.allocated_amount*1);
                    totalLoanees+=(value.loanee*1);

                    jQuery('#allocation_table').append(
                        '<tr>' +
                        '<td>' +value.item_name+'</td>'+
                        '<td>' +value.programme_code+'</td>'+
                        '<td class="text-right text-bold">' +commaSeparateNumber(value.loanee)+'</td>'+
                        '<td class="text-right text-bold">' +commaSeparateNumber((value.allocated_amount*1).toFixed(2))+'</td>'+
                        '</tr>'
                    );
                });

                jQuery('#allocation_table').append(
                    '<tr class="text-bold bg-primary">' +
                    '<td>TOTAL</td>'+
                    '<td></td>'+
                    '<td class="text-right text-bold"></td>'+
                    '<td class="text-right text-bold">' +commaSeparateNumber((totalAllocation*1).toFixed(2))+'</td>'+
                    '</tr>'
                );
            }
        });
    }

    function Paylist() {
        var spinner = "<tr><td colspan='3'><div style='font-size: large;'><span class='fa fa-spinner fa-spin fa-2x'></span> Please Wait... Loading Data...</div></td></tr>";
        $("#disbursement_table").html(spinner);
        $("#confirmButton").hide();
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-batch/outcome'); ?>",

            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                institution:jQuery("#disbursementbatch-learning_institution_id").val(),
                loan_item:jQuery("#disbursementbatch-loan_item_id").val(),
                academic_year:jQuery("#disbursementbatch-academic_year_id").val(),
                installment:jQuery("#disbursementbatch-instalment_definition_id").val(),
                semester:jQuery("#disbursementbatch-semester_number").val(),
                programme_category:jQuery("#disbursementbatch-level").val(),
                version:jQuery("#disbursementbatch-version").val(),
                country:jQuery("#disbursementbatch-country_id").val(),
                location:jQuery("#disbursementbatch-applicant_category_id").val(),


            },
            success:function (data) {

                var counter = 0;
                var totalAllocation = 0;
                var totalD = 0;

                jQuery('#disbursement_table').html("");





                jQuery.each(data.output['results'],function (index,value) {
                    counter = index+1;

                    totalAllocation+=(value.allocated_amount*1);
                    totalD+=(value.disbursed_amount*1);

                    jQuery("#disbursementbatch-allocation_batch_id").val(value.allocation_batch_id);

                    jQuery('#disbursement_table').append(
                        '<tr style="font-size: 12px;">' +
                        '<td>' +counter+'</td>'+
                        '<td class="">' +value.full_name+'</td>'+
                        '<td>' +value.programme_code+'</td>'+
                        '<td>' +value.year_of_study+'</td>'+
                        '<td>' +value.item_name+'</td>'+
                        '<td class="text-right text-bold">' +commaSeparateNumber((value.allocated_amount*1).toFixed(2))+'</td>'+
                        '<td class="text-right text-bold">' +commaSeparateNumber(value.disbursement_percent)+'</td>'+
                        '<td class="text-right text-bold">' +commaSeparateNumber((value.disbursed_amount*1).toFixed(2))+'</td>'+
                        '</tr>'
                    );
                });

                if(counter===0){
                    $("#confirmButton").hide();
                } else {
                    $("#confirmButton").show();
                }
                jQuery('#disbursement_table').append(
                    '<tr class="text-bold bg-primary">' +
                    '<td></td>'+
                    '<td>TOTAL</td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td class="text-right text-bold"></td>'+
                    '<td class="text-right text-bold">' +commaSeparateNumber(totalAllocation.toFixed(2))+'</td>'+
                    '<td class="text-right text-bold"></td>'+
                    '<td class="text-right text-bold">' +commaSeparateNumber(totalD.toFixed(2))+'</td>'+
                    '</tr>'
                );

            }
        })
    }




function CheckPlan() {
    var institution =  jQuery("#disbursementbatch-learning_institution_id").val();
    var item =  jQuery("#disbursementbatch-loan_item_id").val();
    var academic =  jQuery("#disbursementbatch-academic_year_id").val();
    var installment =  jQuery("#disbursementbatch-instalment_definition_id").val();
    var semester =  jQuery("#disbursementbatch-semester_number").val();
    var category =  jQuery("#disbursementbatch-level").val();
    var country =  jQuery("#disbursementbatch-country_id").val();
    var location =  jQuery("#disbursementbatch-applicant_category_id").val();

    if(location==2){
        if(country!=="" && item!=="" &&academic !=="" && installment !==""){
            Plan();
            Paylist();
        }
    } else {
        if(institution!=="" && item!=="" &&academic !=="" && installment !=="" && semester !=="" && category !==""){
            Plan();
            Paylist();
        }
    }

}

    jQuery(document).ready(function () {
        $("#confirmButton").hide();
        $("#loadingDiv").hide();
       /* jQuery("#disbursementbatch-disbursed_as").attr("disabled","disabled");
        jQuery("#disbursementbatch-disbursed_as").val(1);*/
        $("#disbursementbatch-applicant_category_id").on('change',function () {

            if($(this).val()=='1'){
                LocalSearch();
            }
        });

        jQuery("#createBtn").on("click",function () {
            $("#confirmButton").hide();
            $("#loadinDiv").show();
        });


        jQuery("#disbursementbatch-institution_payment_request_id").on("change",function () {
            RequestMagic();
        });

        jQuery("#disbursementbatch-learning_institution_id").on('change',function () {
          CheckPlan();

        });

        jQuery("#disbursementbatch-loan_item_id").on('change',function () {
            CheckPlan();
        });

        jQuery("#disbursementbatch-academic_year_id").on('change',function () {
            CheckPlan();

        });

        jQuery("#disbursementbatch-instalment_definition_id").on('change',function () {
            CheckPlan();

        });

        jQuery("#disbursementbatch-semester_number").on('change',function () {
            CheckPlan();

        });

        jQuery("#disbursementbatch-level").on('change',function () {
            CheckPlan();

        });

        jQuery("#disbursementbatch-version").on('change',function () {
            CheckPlan();

        });
        /**
        * disbursementbatch-level
         * disbursementbatch-disburse_type
         * disbursementbatch-academic_year_id
         * disbursementbatch-financial_year_id
         * disbursementbatch-instalment_definition_id
         * disbursementbatch-loan_item_id
         * disbursementbatch-disbursed_as
         * disbursementbatch-instalment_type
         *
        * */
    });

</script>