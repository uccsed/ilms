<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/14/18
 * Time: 5:47 PM
 */
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\disbursement\models\DisbursementSearch;
?>
<?php
$SQL = "
SELECT 
CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'item',
disbursement.disbursed_amount AS 'amount' 
FROM disbursement 
LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
WHERE disbursement.application_id='$application_id' AND disbursement.disbursement_batch_id = '$batch_id';";
$model = Yii::$app->db->createCommand($SQL)->queryAll();
$counter = 0;

//echo $batch_id;
?>
<!--<h3><?php /*echo $batch_id; */?> OK <?php /*echo $application_id; */?></h3>-->
<table class="table table-condensed table-stripped table-bordered">
    <thead>
    <tr class="bg-orange-active">
        <th>S/N</th>
        <th>ITEM</th>
        <th class="text-right">AMOUNT</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($model as $index=>$dataArray){ $counter++; ?>
    <tr>
        <th><?php echo $counter; ?></th>
        <th><?php echo $dataArray['item']; ?></th>
        <th class="text-bold text-right"><?php echo number_format($dataArray['amount'],2); ?></th>
    </tr>
    <?php } ?>

    </tbody>
</table>
