<?php

use yii\helpers\Html;
use backend\modules\disbursement\Module;
use kartik\tabs\TabsX;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementBatch */

$this->title = 'Update Disbursement Batch with Header ID: '.$model->batch_number;
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Batch', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->batch_number, 'url' => ['view', 'id' => $model->disbursement_batch_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="disbursement-batch-update">
 <div class="panel panel-info">
        <div class="panel-heading">
       <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
            <?php
            $batchdetails=$this->render('_formEdit', [
                'model' => $model,
            ]);

            $structure = '';
            $chainModel = Module::PayListChain($model->Amount);
            if (sizeof($chainModel)!=0){
            $sequence = 0;
            $tr = $amountRange = '';
            foreach ($chainModel as $chainIndex=>$chainDataArray){
            $sequence++;
            if ($sequence==1){
            switch ($chainDataArray['operator']){
            case "Between" :
            $amountRange = 'Approval Structure for Amount between <span class="text-danger">'.number_format($chainDataArray['min_amount'],2).'</span> and <span class="text-danger">'.number_format($chainDataArray['max_amount'],2).'</span>';
            break;

            case "Greater than":
            $amountRange = 'Approval Structure for Amount of <span class="text-danger">'.number_format($chainDataArray['min_amount'],2).'</span> & <span class="text-danger">Above</span>';
            break;
            }

            }

            $tr.='<tr>';
                $tr.='<td>'.Module::THNumber($sequence).'</td>';
                $tr.='<td>'.$chainDataArray['structure_name'].'</td>';
                $tr.='<td>'.$chainDataArray['user_name'].'</td>';
                $tr.='<td>'.$chainDataArray['task_name'].'</td>';
                $tr.='</tr>';
            }

            $structure.='
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th colspan="4" class="text-center text-uppercase" style="font-size: large;">'.$amountRange.'</th>
                </tr>
                <tr>
                    <th>Order</th>
                    <th>Structure</th>
                    <th>Personnel</th>
                    <th>Task</th>
                </tr>
                </thead>
                <tbody>'.$tr.'</tbody>
            </table>
            ';
            }





            echo TabsX::widget([
            'items' => [
            [
            'label' => 'Update Batch Details',
            'content' =>$batchdetails,
            'id' => '1',
            ],
         /*   [
            'label' => 'Disbursement Details',
            'content' => '<iframe src="' . yii\helpers\Url::to(['/disbursement/disbursement/disbursed', 'id' =>$model->disbursement_batch_id]) . '" width="100%" height="600px" style="border: 0"></iframe>',
            'id' => '2',
            ],*/
            [
            'label' => 'Approval Structure',
            'content' =>$structure,
            'id' => '3',
            ],
            /*[
            'label' => 'Payout List Movement',
            'content' => '<iframe src="' . yii\helpers\Url::to(['/disbursement/payoutlist-movement/list-movement', 'id' =>$model->disbursement_batch_id]) . '" width="100%" height="600px" style="border: 0"></iframe>',
            'id' => '4',
            ],*/

            ],
            'position' => TabsX::POS_ABOVE,
            'bordered' => true,
            'encodeLabels' => false
            ]);
            ?>
        </div>
    </div>
</div>
