<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementBatch */

/* @var $form yii\widgets\ActiveForm */

echo Module::FetchBootstrap('js');
///echo Module::HeaderIDAlgorithm(0);
?>

<?php $form = ActiveForm::begin([
    //'options'=>['enctype'=>'multipart/form-data'], // important
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>
<div class="fund-request-approval-form" id="formBoundary" >
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <?= $form->field($model, 'process_days')->textInput(['class' => 'form-control']); ?>
        </div>


        <div class="form-group">
            <?= $form->field($model, 'batch_desc')->textarea(['rows' => 3,'class' => 'form-control']); ?>
        </div>

        <div class="form-group">
            <div id="response_div"></div>

        </div>

        <div style="display: none;">
            <?= $form->field($model, 'allocation_batch_id')->textInput() ?>
            <?= $form->field($model, 'learning_institution_id')->textInput() ?>
            <?= $form->field($model, 'academic_year_id')->textInput() ?>
            <?= $form->field($model, 'financial_year_id')->textInput() ?>
            <?= $form->field($model, 'instalment_definition_id')->textInput() ?>
            <?= $form->field($model, 'semester_number')->textInput() ?>
            <?= $form->field($model, 'loan_item_id')->textInput() ?>
            <?= $form->field($model, 'batch_number')->textInput() ?>
            <?= $form->field($model, 'instalment_type')->textInput() ?>
            <?= $form->field($model, 'version')->textInput() ?>
            <?= $form->field($model, 'is_approved')->textInput() ?>
            <?= $form->field($model, 'approval_comment')->textInput() ?>
            <?= $form->field($model, 'institution_payment_request_id')->textInput() ?>
            <?= $form->field($model, 'payment_voucher_number')->textInput() ?>
            <?= $form->field($model, 'cheque_number')->textInput() ?>
            <?= $form->field($model, 'disbursed_as')->textInput() ?>
            <?= $form->field($model, 'country_id')->textInput() ?>
            <?= $form->field($model, 'applicant_category_id')->textInput() ?>
            <?= $form->field($model, 'level')->textInput() ?>
            <?= $form->field($model, 'disburse_type')->textInput() ?>
            <?= $form->field($model, 'created_at')->textInput() ?>
            <?= $form->field($model, 'created_by')->textInput() ?>
        </div>


        <div class="form-group text-right" id="buttons">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'btnSubmit']) ?>

            <?php
            echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
            echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

            ActiveForm::end();
            ?>
        </div>
      <!-- Start Shared View -->
        <div class="form-group">

        </div>

      <!-- End Shared View -->
    </div>
    <div class="col-md-6"><?php echo $this->render('_shared_view',array('model'=>$model));  ?></div>
</div>

</div>
<div id="response_div"></div>
<script>
    $(document).ready(function () {
        $("#btnSubmit").on('click',function () {
            $(this).hide();
            $("#formBoundary").hide();
            $("#buttons").hide();
            $("#response_div").show();

            var spinner ='<span class="fa fa-spinner fa-spin fa-5x"></span>';
            var content = '<div style="font-size: 18px;">'+spinner+' Please Wait... The system is currently processing Pay-out List</div>';


            $("#response_div").html(content);

        })

    });

</script>
