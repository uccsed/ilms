<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementBatch */

/* @var $form yii\widgets\ActiveForm */
use backend\modules\disbursement\Module;
echo Module::FetchBootstrap('fancyInputs');
echo Module::FetchBootstrap('dashboard');
$user=Yii::$app->user->id;
$iSQL="SELECT * FROM learning_institution WHERE institution_type = 'UNIVERSITY' AND learning_institution_id IN (SELECT institution_assignment.institution_id FROM institution_assignment WHERE institution_assignment.officer_id='$user')";
$InstitutionModel = Yii::$app->db->createCommand($iSQL)->queryAll();
?>
 
 <script type="text/javascript">

 /********************  
  * Id can be hide or show
  * 
	*onload functions
	********************/
	window.onload = start;
	function start () {
		//alert("mickidadi");
		body();
		 
	}
     function body() { 
   var cat= document.getElementById('employerId').style.display = 'none';
   var cat= document.getElementById('additionVersionId').style.display = 'none';
   var cat1= document.getElementById('countryId').style.display = 'none';
   var cat3= document.getElementById('disburseTypeId').style.display = 'none';
   var cat4= document.getElementById('InstitutionId').style.display = 'none';
  // var cat5= document.getElementById('disbursementbatch-disburse_type').style.display = 'none';
    var cat1= document.getElementById('loaderId').style.display = 'none';
   //disbursementbatch-disburse
     updateStatus();
        }



       /* function updateStatus() {

        }*/


   function updateStatus(){
       
          var category= document.getElementById('disbursementbatch-applicant_category_id').value;
             if(category==1){
             var cat1= document.getElementById('countryId').style.display = 'none';         
                  }
                else if(category==2){
          var cat1= document.getElementById('countryId').style.display = '';          
                }
          var level= document.getElementById('disbursementbatch-level').value;
             if(level==2){
             var cat1= document.getElementById('InstitutionId').style.display = 'none'; 
              var cat= document.getElementById('employerId').style.display = '';
                  }
                else if(level==1){
          var cat1= document.getElementById('InstitutionId').style.display = ''; 
           var cat= document.getElementById('employerId').style.display = 'none';
                }
          
             var typevalue= document.getElementById('disbursementbatch-instalment_type').value;
             if(typevalue==1){
             var cat1= document.getElementById('additionVersionId').style.display = 'none';         
                  }
                else if(typevalue>1){
          var cat1= document.getElementById('additionVersionId').style.display = '';          
                }
       //var paylist=1;
          var paylist= document.getElementById('disbursementbatch-disburse_type').value;
             if(paylist==1){
             var cat1= document.getElementById('paylistId').style.display = 'none';         
                  }
                else if(paylist>1){
          var cat1= document.getElementById('paylistId').style.display = '';          
                }       
    }
  
</script>
<style>
div.disabled {
display: none;
}
</style>
<?php
//echo Yii::$app->getUrlManager()->createUrl('/disbursement/disbursement-batch/create');
$this->registerJs( "
        $('body').on('beforeSubmit', 'form#myform', function () {
             var form = $(this);
            
//                        var property = document.getElementById('disbursementbatch-file').files[0];
//                        var image_name = property.name;
//                                 
//                        var image_extension = image_name.split('.').pop().toLowerCase();
//
////                        if(jQuery.inArray(image_extension,['gif','jpg','jpeg','']) == -1){
////                          alert('Invalid image file');
////                        }
                        var formData = new FormData(this);
                        //formData.append('file',property);
                       
             // return false if form still have some validation errors
             if (form.find('.has-error').length) {
                  return false;
             }
            document.getElementById('loaderId').style.display = '';
            document.getElementById('formId').style.display = 'none';
          // alert(form.attr('action'));
             // submit form
               $.ajax({
                            type   : 'post',
                            //dataType: 'json',
                            url: form.attr('action'),
                            data:formData,
                                processData: false,
                                contentType: false,
                            success: function (data) {
                               alert(data);
                          document.getElementById('formId').style.display = '';
                          document.getElementById('loaderId').style.display = 'none';
                           //  $('#displaysummary').html(data);
                            }
                          

                             }) ;
             return false;
        }); ");
?>





<div class="row">
    <div class="col-md-3">
        <div class="disbursement-batch-form" id="formId">

            <?php $form = ActiveForm::begin(['id' => 'myform','options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="profile-user-info profile-user-info-striped">
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Loanee Category:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'applicant_category_id')->label(false)->dropDownList(
                                [1=>"Local",2=>"OverSee"],
                                [
                                    'prompt'=>'Select Loanee Category',
                                    'onchange'=>'updateStatus()'
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row" id="countryId">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Country:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'country_id')->label(false)->dropDownList(
                                [1=>"USA",2=>"Kenya",3=>"UK"],
                                [
                                    'prompt'=>'Select Country',
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Study Level:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'level')->label(false)->dropDownList(
                                ArrayHelper::map(backend\modules\application\models\ApplicantCategory::find()->all(),'applicant_category_id','applicant_category'),
                                [
                                    'prompt'=>'Select Study Level',
                                    'onchange'=>'updateStatus()'
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row" id="employerId">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Employer:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'employer_id')->label(false)->dropDownList(
                                [1=>"UDSM",2=>"DUSE",3=>"UDOM"],
                                [
                                    'prompt'=>'Select Employer',
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Disburse Options:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'disburse_type')->label(false)->dropDownList(
                                [1=>"All",2=>"Selective"],
                                [
                                    'prompt'=>'Select Disburse Options',
                                    'onchange'=>'updateStatus()'
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row" id="disburseTypeId">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Disburse Type:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?/*= $form->field($model, 'disburse_type')->label(false)->dropDownList(
                                [1=>"Part",2=>"Lumpsum"],
                                [
                                    'prompt'=>'Select Disburse Type',
                                ]
                            )*/ ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row" id="InstitutionId">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Institution:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'learning_institution_id')->label(false)->dropDownList(
                                ArrayHelper::map($InstitutionModel,'learning_institution_id','institution_name'),
                                [
                                    'prompt'=>'Select Institution',
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Academic Year:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'academic_year_id')->label(false)->dropDownList(
                                ArrayHelper::map(\common\models\AcademicYear::find()->all(),'academic_year_id','academic_year'),
                                [
                                    'prompt'=>'Select Academic Year',
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Financial Year:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'financial_year_id')->label(false)->dropDownList(
                                ArrayHelper::map(\backend\modules\disbursement\models\FinancialYear::find()->all(),'financial_year_id','financial_year'),
                                [
                                    'prompt'=>'Select Financial Year',
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>

                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Semester :</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'semester_number')->label(false)->dropDownList(
                                Module::Semester(),
                                [
                                    'prompt'=>'Select Semester',
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>

                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Instalment :</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'instalment_definition_id')->label(false)->dropDownList(
                                ArrayHelper::map(\backend\modules\disbursement\models\InstalmentDefinition::find()->where(["is_active"=>1])->orderBy(['instalment'=>'ASC'])->all(),'instalment_definition_id','instalment'),
                                [
                                    'prompt'=>'Select Instalment',
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Loan Item:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'loan_item_id')->label(false)->dropDownList(
                                ArrayHelper::map(backend\modules\allocation\models\LoanItem::findAll(["is_active"=>1]),'loan_item_id','item_name'),
                                [
                                    'prompt'=>'Select Loan Item',
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Loan Category:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'disbursed_as')->label(false)->dropDownList(
                                [1=>"Loan",2=>"Grant"],
                                [
                                    'prompt'=>'Select Loan Category',
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Type :</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'instalment_type')->label(false)->dropDownList(
                                [1=>"Normal",2=>"Additional"],
                                [
                                    'prompt'=>'Select Instalment Type',
                                    'onchange'=>'updateStatus()'
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row" id="additionVersionId">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Additional Version:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'version')->label(false)->dropDownList(
                                [1=>"1",2=>"2",3=>"3",4=>"4",5=>"5",6=>"6"],
                                [
                                    'prompt'=>'Select Additional Version',
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row" id="paylistId">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Select Pay List:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'file')->label(false)->fileInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">
                        <label class="control-label" for="email">Description:</label>
                    </div>
                    <div class="profile-info-value">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'batch_desc')->label(false)->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'created_at')->label(false)->hiddenInput(["value"=>date("Y-m-d")]) ?>
                            <?= $form->field($model, 'allocation_batch_id')->label(false)->hiddenInput() ?>
                            <?= $form->field($model, 'created_by')->label(false)->hiddenInput(["value"=>\yii::$app->user->identity->user_id]) ?>
                        </div>
                    </div>
                </div>
            </div>




            <div class="space10"></div>
            <div class="col-sm-12">
                <div class="form-group button-wrapper">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
    <div class="col-md-9">

        <!--<div class="col-md-5">-->
            <div class="panel panel-flat">
                <div class="panel-head"><h4 class="text-bold heading-text">Disbursement Plan</h4></div>
                <div class="panel-body">
                    <table class="table table-condensed table-bordered">
                        <thead>
                        <tr class="bg-primary text-uppercase">
                            <th>Programme</th>
                            <th>Year of Study</th>
                            <th>Semester #</th>
                            <th class="text-right">Disbursement %</th>
                        </tr>
                        </thead>
                        <tbody id="plan_table"></tbody>
                    </table>
                </div>

            </div>

        <div class="panel panel-flat">
            <div class="panel-head"><h4 class="text-bold heading-text">Allocation</h4></div>
            <div class="panel-body">
                <table class="table table-condensed table-bordered">
                    <thead>
                    <tr class="bg-primary text-uppercase">
                        <th>Item</th>
                        <th>Programme</th>
                        <th class="text-right">Lonees</th>
                        <th class="text-right">Allocated Amount</th>
                    </tr>
                    </thead>
                    <tbody id="allocation_table"></tbody>
                </table>
            </div>

        </div>
        <div class="panel panel-flat">
            <div class="panel-head"><h4 class="text-bold heading-text">Disbursement</h4></div>
            <div class="panel-body">
                <table class="table table-condensed table-bordered">
                    <thead>
                    <tr class="bg-primary text-uppercase">
                        <th>S/N</th>
                        <th class="">Lonee</th>
                        <th>Programme</th>
                        <th>YOS</th>
                        <th>Item</th>
                        <th class="text-right">Allocated Amount</th>
                        <th class="text-right">Disbursement %</th>
                        <th class="text-right">Disbursement Amount</th>
                    </tr>
                    </thead>
                    <tbody id="disbursement_table"></tbody>
                </table>
            </div>

        </div>
        <!--</div>-->
       <!-- <div class="col-md-7">
            <div class="panel panel-flat">
                <div class="panel-head"><h4 class="text-bold heading-text">Financial Implication</h4></div>
                <div class="panel-body" id="financial_implication">

                </div>

            </div>
        </div>-->

    </div>
</div>




<div id="loaderId" style="display:none">
 
  <?php echo Html::img('@web/image/loader/loader2.gif') ?>  
</div>

<div class="space10"></div>


<script>
    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }

    function Plan() {
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-batch/plan'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                institution:jQuery("#disbursementbatch-learning_institution_id").val(),
                loan_item:jQuery("#disbursementbatch-loan_item_id").val(),
                academic_year:jQuery("#disbursementbatch-academic_year_id").val(),
                installment:jQuery("#disbursementbatch-instalment_definition_id").val(),
                semester:jQuery("#disbursementbatch-semester_number").val(),
                programme_category:jQuery("#disbursementbatch-level").val(),


            },
            success:function (data) {
                jQuery('#plan_table').html("");
                Allocation(data.output['results']);

                jQuery.each(data.output['results'],function(index,value) {
                    jQuery('#plan_table').append(
                        '<tr>' +
                            '<td>' +value.programme_code+'</td>'+
                            '<td>' +value.year_of_study+'</td>'+
                            '<td>' +value.semester_number+'</td>'+
                            '<td class="text-right">' +value.disbursement_percent+'</td>'+
                        '</tr>'
                    );
                });

            }
        });
    }

    function Allocation(plan) {
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-batch/allocation'); ?>",

            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                institution:jQuery("#disbursementbatch-learning_institution_id").val(),
                loan_item:jQuery("#disbursementbatch-loan_item_id").val(),
                academic_year:jQuery("#disbursementbatch-academic_year_id").val(),
                installment:jQuery("#disbursementbatch-instalment_definition_id").val(),
                semester:jQuery("#disbursementbatch-semester_number").val(),
                programme_category:jQuery("#disbursementbatch-level").val(),

            },
            success:function (data) {


                var totalLoanees = 0;
                var totalAllocation = 0;
                jQuery('#allocation_table').html("");
                //console.log(data.output['results']);
                jQuery.each(data.output['results'],function (index,value) {

                    totalAllocation+=(value.allocated_amount*1);
                    totalLoanees+=(value.loanee*1);

                    jQuery('#allocation_table').append(
                        '<tr>' +
                        '<td>' +value.item_name+'</td>'+
                        '<td>' +value.programme_code+'</td>'+
                        '<td class="text-right text-bold">' +commaSeparateNumber(value.loanee)+'</td>'+
                        '<td class="text-right text-bold">' +commaSeparateNumber(value.allocated_amount)+'</td>'+
                        '</tr>'
                    );
                });

                jQuery('#allocation_table').append(
                    '<tr class="text-bold bg-primary">' +
                    '<td>TOTAL</td>'+
                    '<td></td>'+
                    '<td class="text-right text-bold"></td>'+
                    '<td class="text-right text-bold">' +commaSeparateNumber(totalAllocation)+'</td>'+
                    '</tr>'
                );
            }
        });
    }

    function Paylist() {
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/disbursement/disbursement-batch/outcome'); ?>",

            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                institution:jQuery("#disbursementbatch-learning_institution_id").val(),
                loan_item:jQuery("#disbursementbatch-loan_item_id").val(),
                academic_year:jQuery("#disbursementbatch-academic_year_id").val(),
                installment:jQuery("#disbursementbatch-instalment_definition_id").val(),
                semester:jQuery("#disbursementbatch-semester_number").val(),
                programme_category:jQuery("#disbursementbatch-level").val(),


            },
            success:function (data) {
                console.log(data);
                var counter = 0;
                var totalLoanees = 0;
                var totalAllocation = 0;
                var totalDisbursement = 0;
                jQuery('#disbursement_table').html("");
                jQuery.each(data.output['results'],function (index,value) {
                    counter = index+1;
                    totalAllocation+=(value.allocated_amount*1);
                    totalDisbursement+=(value.disbursed_amount*1);
                    //allocation_batch_id
                    jQuery("#disbursementbatch-allocation_batch_id").val(value.allocation_batch_id);

                    jQuery('#disbursement_table').append(
                        '<tr style="font-size: 12px;">' +
                        '<td>' +counter+'</td>'+
                        '<td class="">' +value.full_name+'</td>'+
                        '<td>' +value.programme_code+'</td>'+
                        '<td>' +value.year_of_study+'</td>'+
                        '<td>' +value.item_name+'</td>'+
                        '<td class="text-right text-bold">' +commaSeparateNumber(value.allocated_amount)+'</td>'+
                        '<td class="text-right text-bold">' +commaSeparateNumber(value.disbursement_percent)+'</td>'+
                        '<td class="text-right text-bold">' +commaSeparateNumber(value.disbursed_amount)+'</td>'+
                        '</tr>'
                    );
                });

                jQuery('#disbursement_table').append(
                    '<tr class="text-bold bg-primary">' +
                    '<td></td>'+
                    '<td>TOTAL</td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td class="text-right text-bold"></td>'+
                    '<td class="text-right text-bold">' +commaSeparateNumber(totalAllocation)+'</td>'+
                    '<td class="text-right text-bold"></td>'+
                    '<td class="text-right text-bold">' +commaSeparateNumber(totalDisbursement)+'</td>'+
                    '</tr>'
                );

            }
        })
    }




function CheckPlan() {
    var institution =  jQuery("#disbursementbatch-learning_institution_id").val();
    var item =  jQuery("#disbursementbatch-loan_item_id").val();
    var academic =  jQuery("#disbursementbatch-academic_year_id").val();
    var installment =  jQuery("#disbursementbatch-instalment_definition_id").val();
    var semester =  jQuery("#disbursementbatch-semester_number").val();
    var category =  jQuery("#disbursementbatch-level").val();

    if(institution!=="" && item!=="" &&academic !=="" && installment !=="" && semester !=="" && category !==""){
        Plan();
        Paylist();
    }
}

    jQuery(document).ready(function () {
        jQuery("#disbursementbatch-learning_institution_id").on('change',function () {
          CheckPlan();

        });

        jQuery("#disbursementbatch-loan_item_id").on('change',function () {
            CheckPlan();
        });

        jQuery("#disbursementbatch-academic_year_id").on('change',function () {
            CheckPlan();

        });

        jQuery("#disbursementbatch-instalment_definition_id").on('change',function () {
            CheckPlan();

        });

        jQuery("#disbursementbatch-semester_number").on('change',function () {
            CheckPlan();

        });

        jQuery("#disbursementbatch-level").on('change',function () {
            CheckPlan();

        });
        /**
        * disbursementbatch-level
         * disbursementbatch-disburse_type
         * disbursementbatch-academic_year_id
         * disbursementbatch-financial_year_id
         * disbursementbatch-instalment_definition_id
         * disbursementbatch-loan_item_id
         * disbursementbatch-disbursed_as
         * disbursementbatch-instalment_type
         *
        * */
    });

</script>