<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $model frontend\modules\allocation\models\AllocationBatch */


//find loan item of the disbursement batch
//echo "SELECT group_concat(`item_name`) as `item_name` FROM `disbursement` d join  `loan_item` l on d.`loan_item_id`=l.`loan_item_id` WHERE disbursement_batch_id='{$model->disbursement_batch_id}' group by l.loan_item_id";
$sqlitem=Yii::$app->db->createCommand("SELECT  `item_name` FROM `disbursement` d join  `loan_item` l on d.`loan_item_id`=l.`loan_item_id` WHERE disbursement_batch_id='{$model->disbursement_batch_id}' group by l.loan_item_id,disbursement_batch_id")->queryAll();
$items=""; 
if(count($sqlitem)>0){
foreach ($sqlitem as $rowitem){
$items.=$rowitem["item_name"]." ;";
}
  }
//end
  $modelinst=  \backend\modules\allocation\models\base\LearningInstitution::findOne($model->learning_institution_id);
//$link = Yii::$app->urlManager->createUrl('/disbursement/disbursement-batch/view&id='.$model->disbursement_batch_id/view&id='.$model->disbursement_batch_id);
$link= url::to(['/disbursement/disbursement-batch/view','id'=>$model->disbursement_batch_id]);
?>
<div class="allocation-batch-view">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?= Html::encode("Batch Header Summary") ?>
        </div>
        <div class="panel-body">

<span class="pull-right">
    <?= Html::a('Download Payout List (PDF)', ['disbursement-batch/payout-list-download','batch_id'=>$model->disbursement_batch_id,'mode'=>'pdf'], ['class' => 'btn btn-warning text-right','target'=>'_blank']) ?>
    <?= Html::a('Download Mis-Disbursed List (PDF)', ['disbursement-batch/mis-disbursement-download','batch_id'=>$model->disbursement_batch_id], ['class' => 'btn btn-primary text-right','target'=>'_blank']) ?>
    <?= Html::a('Download Internal Memo (PDF)', ['disbursement-batch/memo-download','batch_id'=>$model->disbursement_batch_id], ['class' => 'btn bg-orange-active text-right','target'=>'_blank']) ?>

</span>
            <table id="example2" class="table table-bordered table-hover">
                <thead>

                    <tr>
                        <th colspan="3">To</th>
                        <th>Director of Finance and Administration</th>
                    </tr>
                    <tr>
                        <th colspan="3">Date</th>
                        <td><?= date("Y-m-d") ?></td>
                    </tr>
                    <tr>
                        <th colspan="3">Institution</th>
                        <td><?= $modelinst->institution_code ?></td>
                    </tr>

                    <tr>
                        <th colspan="3">Academic Year</th>
                        <td><?= $model->academicYear->academic_year; ?></td>
                    </tr>

                    <tr>
                        <th colspan="3">Semester</th>
                        <td><?= $model->Semester; ?></td>
                    </tr>

                    <tr>
                        <th colspan="3">Instalment</th>
                        <td><?= $model->Instalment; ?></td>
                    </tr>

                    <tr>
                        <th colspan="3">Financial Year</th>
                        <td><?= $model->FinancialYear; ?></td>
                    </tr>
                    <tr>
                        <th colspan="3">Header ID</th>
                        <td><?php echo $model->batch_number; //echo $modelinst->institution_code.'-'.$model->LoanItemCode.'-'.$model->academicYear->academic_year.'-'.$model->SemesterNumber.'-'.$model->InstalmentNumber.'-'.$model->disbursement_batch_id ?></td>
                    </tr>
                    <tr>
                        <th colspan="3">Loan Item</th>
                        <td><?= $items ?></td>
                    </tr>
                    <tr>
                        <th colspan="3">Status</th>
                        <td><?= $model->Status; ?></td>
                    </tr>
                    <tr>
                        <th colspan="3">Priority</th>
                        <td><?= $model->Priority; ?></td>
                    </tr>
                    <!--<tr>
                        <th>#</th>
                        <th>YOS</th>
                        <th>NUMBER OF STUDENTS</th>
                        <th align='right'>AMOUNT</th>
                    </tr>-->
                </thead>
               <!-- <tbody>
                    <?php
/*                    // print_r($dataProvider);
                    $tzsql = Yii::$app->db->createCommand("SELECT *,SUM(disbursed_amount) as amount,count(*) as counts FROM `disbursement` di,application ap,disbursement_batch dis WHERE di.`application_id`=ap.`application_id` AND di.`disbursement_batch_id`=dis.disbursement_batch_id AND dis.disbursement_batch_id='{$model->disbursement_batch_id}' group by `current_study_year`,`instalment_definition_id`")->queryAll();

                    $total = 0;
                    $i = 1;
                    if (count($tzsql) > 0) {
                        foreach ($tzsql as $rows) {
                            echo "<tr>
                        <td>" . $i . "</td>
                        <td>" . $rows["current_study_year"] . "</td>
                         <td>" . $rows["counts"] . "</td>
                        <td align='right'><b>" . number_format($rows["amount"]) . "</b></td>
                     
                      </tr>";
                            $total+=$rows["amount"];
                            $i++;
                        }
                        echo "<tr>
                        <td colspan='3'> </td>
                        <td align='right'><b>" . number_format($total) . "</b></td>
                      </tr>";
                    } else {
                        echo "<tr><td colspan='2'><font color='red'>Sorry No results found</font></td></tr>";
                    }
                    */?>

                </tbody>-->

            </table>
            <p>
                <?php $currentChain = Module::CurrentMovement($model->disbursement_batch_id); ?>
                <!--<pre>
                    <?php /*//print_r($currentChain); */?>
                </pre>-->

            </p>



            <p class="pull-right" style="">
                <?php
                $batchID = $model->disbursement_batch_id;
                $user = Yii::$app->user->id;
                $amount = $model->Amount;
                $dataArray = Module::BatchApproval($batchID)[0];
                $OWNER = Module::MovementOwner($batchID);

                $CURRENT = Module::CurrentMovement($batchID);
                $Query = Module::MovementQuery($batchID,'current');
                $currentMovementID= $dataArray['movement_id'];
                $PREVIOUS = Module::PreviousMovements($batchID,$currentMovementID);

                ?>

        <!--       <pre>
                <?php /*echo $user;*/?>
            </pre>
               <pre>
                   QUERY
                <?php /*print_r($Query);*/?>
            </pre>
            <pre>
                CURRENT
                <?php /*print_r($CURRENT);*/?>
            </pre>

            <pre>
                PREVIOUS
                <?php /*print_r($PREVIOUS);*/?>
            </pre>
            <pre>
                OWNER
                <?php /*print_r($OWNER);*/?>
            </pre>

            <pre>
                OTHER
                <?php /*print_r($dataArray); */?>
            </pre>-->


                <?php if (isset($OWNER[$user]) && sizeof($dataArray)!=0 && $model->is_submitted =='1'){ ?>
                    <?= Html::a($dataArray["task_name"], ['review-decision', 'id' => $model->disbursement_batch_id, 'level' => $dataArray["order_level"],'action'=>'1'], [
                                'class' => 'btn btn-success',
                                'data' => [
                                    'confirm' => 'Are you sure you want to '.$dataArray["task_name"].' this Disbursement?',
                                    'method' => 'post',
                                ],
                            ])
                            ?>

                    <?= Html::a('REJECT', ['review-decision', 'id' => $model->disbursement_batch_id, 'level' => $dataArray["order_level"],'action'=>'0'], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to REJECT this Disbursement?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                <?php } ?>


            </p>
        </div>
        <div class="panel-foot"><?php   echo Module::MemoSummary($model->disbursement_batch_id); ?></div>
    </div>
</div>