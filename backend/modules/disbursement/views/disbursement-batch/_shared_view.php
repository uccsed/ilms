<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/6/18
 * Time: 10:08 AM
 */
use kartik\detail\DetailView;
use backend\modules\disbursement\Module;
use yii\helpers\Html;
?>

<p>
    <span class="pull-right">
    <?= Html::a('Download Payout List (PDF)', ['disbursement-batch/payout-list-download','batch_id'=>$model->disbursement_batch_id,'mode'=>'pdf'], ['class' => 'btn btn-warning text-right','target'=>'_blank']) ?>
    <?= Html::a('Download Mis-Disbursed List (PDF)', ['disbursement-batch/mis-disbursement-download','batch_id'=>$model->disbursement_batch_id], ['class' => 'btn btn-primary text-right','target'=>'_blank']) ?>
    <?= Html::a('Download Internal Memo (PDF)', ['disbursement-batch/memo-download','batch_id'=>$model->disbursement_batch_id], ['class' => 'btn bg-orange-active text-right','target'=>'_blank']) ?>

</span>
</p>
<br />
<p>
    <?php echo DetailView::widget([
        'model' => $model,
        'condensed' => true,
        'hover' => true,
        'attributes' => [
            [
                //'name'=>'disbursement_batch_id',
                'name'=>'batch_number',
                'value'=>'<b>'.$model->batch_number.'</b>',
                //'value'=>'<b>'.$model->disbursement_batch_id.'</b>',
                'format'=>'html',
                'label'=>'Header ID',
            ],

            [
                'name'=>'learning_institution_id',
                'value'=>$model->learningInstitution->institution_name,
                'format'=>'html',
                'label'=>'Institution',
            ],
            [
                'name'=>'academic_year',
                'value'=>$model->academicYear->academic_year,
                'format'=>'html',
                'label'=>'Academic Year',
            ],
            'Items',
            // 'disbursement_batch_id',
            // 'learningInstitution.institution_name',
            //'learning_institution_id',
            //'academicYear.academic_year',
            //'allocationBatch.batch_number',
            //'batch_number',
            //'batch_desc',
            [
                'name'=>'batch_desc',
                'value'=>$model->batch_desc,
                'format'=>'html',
                'label'=>'Description',
            ],
            //'InstalmentType',
            [
                'name'=>'InstalmentType',
                'value'=>$model->InstalmentType,
                'format'=>'html',
                'label'=>'Type',
            ],

            [
                'name'=>'Instalment',
                'value'=>$model->Instalment,
                'format'=>'html',
                'label'=>'Instalment Number',
            ],
            [
                'name'=>'Semester',
                'value'=>$model->Semester,
                'format'=>'html',
                'label'=>'Semester Number',
            ],
            [
                'name'=>'Amount',
                'value'=>'<b><sup class="text-bold">Tshs</sup> '.number_format($model->Amount,2).'</b>',
                'format'=>'html',
                'label'=>'Amount Paid',
            ],
            [
                'name'=>'Beneficiaries',
                'value'=>'<b>'.number_format($model->Beneficiaries,0).'</b>',
                'format'=>'html',
                'label'=>'Total Beneficiaries',
            ],
            // 'is_approved',
            'approval_comment:ntext',
            //'institution_payment_request_id',
            'Request',
            'payment_voucher_number',
            'cheque_number',
            'Status',
            [
                'name'=>'process_days',
                'value'=>$model->Priority,
                'format'=>'html',
                'label'=>'Priority',
            ],

            [
                'name'=>'created_at',
                'value'=>$model->created_at,
                'format'=>'html',
                'label'=>'Date Booked',
            ],

            [
                'name'=>'created_by',
                'value'=>Module::UserInfo($model->created_by,'fullName'),
                'format'=>'html',
                'label'=>'Prepared By',
            ],
            // 'created_at',
            // 'created_by',
        ],
    ]);

    ?>
</p>
<p>
    <?php   echo Module::MemoSummary($model->disbursement_batch_id); ?>
</p>



