<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementBatch */

$this->title ="View Disbursement Details";
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-batch-view">
 <div class="panel panel-info">
        <div class="panel-heading">
       <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
    <p>

         <?php
         if($model->is_submitted==0 && sizeof(Module::MovementCheck($model->disbursement_batch_id))==0){ ?>

             <?php //= Html::a('Update', ['update', 'id' => $model->disbursement_batch_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->disbursement_batch_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],

        ]) ?>
             <?= Html::a('Update', ['update', 'id' => $model->disbursement_batch_id], [
                 'class' => 'btn btn-warning',
                 /*'data' => [
                     'confirm' => 'Are you sure you want to Update this Disbursement?',
                     'method' => 'post',
                 ],*/
             ]) ?>
       <?= Html::a('Submit', ['officersubmit', 'id' => $model->disbursement_batch_id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Are you sure you want to Submit this Disbursement?',
                'method' => 'post',
            ],
        ]) ?>
        <?php }  ?>
<!--<span class="pull-right">
    <?/*= Html::a('Download Payout List (PDF)', ['disbursement-batch/payout-list-download','batch_id'=>$model->disbursement_batch_id,'mode'=>'pdf'], ['class' => 'btn btn-warning text-right','target'=>'_blank']) */?>
    <?/*= Html::a('Download Mis-Disbursed List (PDF)', ['disbursement-batch/mis-disbursement-download','batch_id'=>$model->disbursement_batch_id], ['class' => 'btn btn-primary text-right','target'=>'_blank']) */?>
    <?/*= Html::a('Download Internal Memo (PDF)', ['disbursement-batch/memo-download','batch_id'=>$model->disbursement_batch_id], ['class' => 'btn bg-orange-active text-right','target'=>'_blank']) */?>

</span>-->


    </p>

            <?php echo   Yii::$app->controller->renderPartial('_shared_view',array('model'=>$model));  ?>

</div>
 </div>
</div>