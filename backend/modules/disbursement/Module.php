<?php

namespace backend\modules\disbursement;
use backend\modules\allocation\models\ApplicantCategory;
use backend\modules\allocation\models\Application;
use backend\modules\allocation\models\LoanItem;
use backend\modules\disbursement\models\Disbursement;
use backend\modules\disbursement\models\DisbursementAdjustment;
use backend\modules\disbursement\models\DisbursementBatch;
use backend\modules\disbursement\models\DisbursementDepositBatch;
use backend\modules\disbursement\models\DisbursementDepositStaging;
use backend\modules\disbursement\models\DisbursementReturn;
use backend\modules\disbursement\models\DisbursementReturnBatch;
use backend\modules\disbursement\models\DisbursementReturnStaging;
use backend\modules\disbursement\models\DisbursementSuspension;
use backend\modules\disbursement\models\InstitutionFundRequest;
use backend\modules\disbursement\models\PayoutlistMovement;
use backend\modules\disbursement\models\SuspensionBatch;
use backend\modules\disbursement\models\SuspensionReason;
use backend\modules\disbursement\models\SuspensionStaging;
use yii\helpers\ArrayHelper;
use Yii;
use yii\web\UploadedFile;
use common\models\User;
use common\models\Logs;
use yii\helpers\Json;

/**
 * disbursement module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\disbursement\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    /** The header ID Algorithm
     * The function uses Luhn's Algorithm
     */
    public static function HeaderIDAlgorithm($number)
    {
        /*settype($number,'string');
        $sumTable = array(
            array(0,1,2,3,4,5,6,7,8,9),
            array(0,2,4,6,8,1,2,3,5,7,9)
        );
        $sum = 0;
        $flip = 0;
        for ($i=strlen($number) -1; $i>=0; $i--){
            $sum+=$sumTable[$flip++ & 0*1][$number[$i]];
        }
        return $sum % 10;*/

        $sum = 0;               // Luhn checksum w/o last digit
        $even = true;           // Start with an even digit
        $n = $number;

        // Lookup table for the digitsums of 2*$i
        $evendig = array(0, 2, 4, 6, 8, 1, 3, 5, 7, 9);

        while ($n > 0) {
            $d = $n % 10;
            $sum += ($even) ? $evendig[$d] : $d;

            $even = !$even;
            $n = ($n - $d) / 10;
        }

        $sum = 9*$sum % 10;

        $output =  10 * $number + $sum;

        $HEADER=100000; //Initial HEader ID

        $SQL = "SELECT MAX(disbursement_batch_id) as 'id' FROM disbursement_batch";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $HEADER+=$model[0]['id'];
        }
        $HEADER++; //IncrementHeader By One 1

        return $HEADER;

    }



    public static function FetchBootstrap($option=null)
    {
        $output = "";



        $js= '<script src="../js/jquery-1.11.1.min.js"></script>';
        $bootstrap= '
        <link href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="../js/bootstrap.min.js"></script>
      
        ';

        $header = $js.$bootstrap;


        switch ($option){

            case "header":
                $output=$header;
                break;

            case "calendar":
                // $output.=$header;
                $output.='';

                break;

            case "fancyInputs":
                $fancyInput = '
                <style>
    .material-switch > input[type="checkbox"] {
        display: none;
    }

    .material-switch > label {
        cursor: pointer;
        height: 0px;
        position: relative;
        width: 40px;
    }

    .material-switch > label::before {
        background: rgb(0, 0, 0);
        box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
        border-radius: 8px;
        content: \'\';
        height: 16px;
        margin-top: -8px;
        position:absolute;
        opacity: 0.3;
        transition: all 0.4s ease-in-out;
        width: 40px;
    }
    .material-switch > label::after {
        background: rgb(255, 255, 255);
        border-radius: 16px;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
        content: \'\';
        height: 24px;
        left: -4px;
        margin-top: -8px;
        position: absolute;
        top: -4px;
        transition: all 0.3s ease-in-out;
        width: 24px;
    }
    .material-switch > input[type="checkbox"]:checked + label::before {
        background: inherit;
        opacity: 0.5;
    }
    .material-switch > input[type="checkbox"]:checked + label::after {
        background: inherit;
        left: 20px;
    }
    

.dropdown.dropdown-lg .dropdown-menu {
    margin-top: -1px;
    padding: 6px 20px;
}
.input-group-btn .btn-group {
    display: flex !important;
}
.btn-group .btn {
    border-radius: 0;
    margin-left: -1px;
}
.btn-group .btn:last-child {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
}
.btn-group .form-horizontal .btn[type="submit"] {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.form-horizontal .form-group {
    margin-left: 0;
    margin-right: 0;
}
.form-group .form-control:last-child {
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
}

@media screen and (min-width: 768px) {
    #adv-search {
        width: 500px;
        margin: 0 auto;
    }
    .dropdown.dropdown-lg {
        position: static !important;
    }
    .dropdown.dropdown-lg .dropdown-menu {
        min-width: 500px;
    }
}
</style>
                ';

                $output=$header.$fancyInput;
                break;

            case "dashboard":
                $dashboard='
                <style type="text/css">
                .progress{
    width: 150px;
    height: 150px;
    line-height: 150px;
    background: none;
    margin: 0 auto;
    box-shadow: none;
    position: relative;
}
.progress:after{
    content: "";
    width: 100%;
    height: 100%;
    border-radius: 50%;
    border: 12px solid #fff;
    position: absolute;
    top: 0;
    left: 0;
}
.progress > span{
    width: 50%;
    height: 100%;
    overflow: hidden;
    position: absolute;
    top: 0;
    z-index: 1;
}
.progress .progress-left{
    left: 0;
}
.progress .progress-bar{
    width: 100%;
    height: 100%;
    background: none;
    border-width: 12px;
    border-style: solid;
    position: absolute;
    top: 0;
}
.progress .progress-left .progress-bar{
    left: 100%;
    border-top-right-radius: 80px;
    border-bottom-right-radius: 80px;
    border-left: 0;
    -webkit-transform-origin: center left;
    transform-origin: center left;
}
.progress .progress-right{
    right: 0;
}
.progress .progress-right .progress-bar{
    left: -100%;
    border-top-left-radius: 80px;
    border-bottom-left-radius: 80px;
    border-right: 0;
    -webkit-transform-origin: center right;
    transform-origin: center right;
    animation: loading-1 1.8s linear forwards;
}
.progress .progress-value{
    width: 90%;
    height: 90%;
    border-radius: 50%;
    background: #44484b;
    font-size: 24px;
    color: #fff;
    line-height: 135px;
    text-align: center;
    position: absolute;
    top: 5%;
    left: 5%;
}
.progress.blue .progress-bar{
    border-color: #049dff;
}
.progress.blue .progress-left .progress-bar{
    animation: loading-2 1.5s linear forwards 1.8s;
}
.progress.yellow .progress-bar{
    border-color: #fdba04;
}
.progress.yellow .progress-left .progress-bar{
    animation: loading-3 1s linear forwards 1.8s;
}
.progress.pink .progress-bar{
    border-color: #ed687c;
}
.progress.pink .progress-left .progress-bar{
    animation: loading-4 0.4s linear forwards 1.8s;
}
.progress.green .progress-bar{
    border-color: #1abc9c;
}
.progress.green .progress-left .progress-bar{
    animation: loading-5 1.2s linear forwards 1.8s;
}
@keyframes loading-1{
    0%{
        -webkit-transform: rotate(50deg);
        transform: rotate(50deg);
    }
    100%{
        -webkit-transform: rotate(180deg);
        transform: rotate(180deg);
    }
}
@keyframes loading-2{
    0%{
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100%{
        -webkit-transform: rotate(144deg);
        transform: rotate(144deg);
    }
}
@keyframes loading-3{
    0%{
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100%{
        -webkit-transform: rotate(90deg);
        transform: rotate(90deg);
    }
}
@keyframes loading-4{
    0%{
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100%{
        -webkit-transform: rotate(36deg);
        transform: rotate(36deg);
    }
}
@keyframes loading-5{
    0%{
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100%{
        -webkit-transform: rotate(126deg);
        transform: rotate(126deg);
    }
}
@media only screen and (max-width: 990px){
    .progress{ margin-bottom: 20px; }
}
/*@import url("/css/font-awesome.min.css");*/
/*body { margin-top:20px; }*/
/*.fa { font-size: 50px;text-align: right;position: absolute;top: 7px;right: 27px;outline: none; }*/
a { transition: all .3s ease;-webkit-transition: all .3s ease;-moz-transition: all .3s ease;-o-transition: all .3s ease; }
/* Visitor */
a.visitor i,.visitor h4.list-group-item-heading { color:#E48A07; }
a.visitor:hover { background-color:#E48A07; }
a.visitor:hover * { color:#FFF; }
/* Facebook */
a.facebook-like i,.facebook-like h4.list-group-item-heading { color:#3b5998; }
a.facebook-like:hover { background-color:#3b5998; }
a.facebook-like:hover * { color:#FFF; }
/* Google */
a.google-plus i,.google-plus h4.list-group-item-heading { color:#dd4b39; }
a.google-plus:hover { background-color:#dd4b39; }
a.google-plus:hover * { color:#FFF; }
/* Twitter */
a.twitter i,.twitter h4.list-group-item-heading { color:#00acee; }
a.twitter:hover { background-color:#00acee; }
a.twitter:hover * { color:#FFF; }
/* Linkedin */
a.linkedin i,.linkedin h4.list-group-item-heading { color:#0e76a8; }
a.linkedin:hover { background-color:#0e76a8; }
a.linkedin:hover * { color:#FFF; }
/* Tumblr */
a.tumblr i,.tumblr h4.list-group-item-heading { color:#34526f; }
a.tumblr:hover { background-color:#34526f; }
a.tumblr:hover * { color:#FFF; }
/* Youtube */
a.youtube i,.youtube h4.list-group-item-heading { color:#c4302b; }
a.youtube:hover { background-color:#c4302b; }
a.youtube:hover * { color:#FFF; }
/* Vimeo */
a.vimeo i,.vimeo h4.list-group-item-heading { color:#44bbff; }
a.vimeo:hover { background-color:#44bbff; }
a.vimeo:hover * { color:#FFF; }

.circle-tile {
    margin-bottom: 15px;
    text-align: center;
}
.circle-tile-heading {
    border: 3px solid rgba(255, 255, 255, 0.3);
    border-radius: 100%;
    color: #FFFFFF;
    height: 80px;
    margin: 0 auto -40px;
    position: relative;
    transition: all 0.3s ease-in-out 0s;
    width: 80px;
}
.circle-tile-heading .fa {
    line-height: 80px;
}
.circle-tile-content {
    padding-top: 50px;
}
.circle-tile-number {
    font-size: 26px;
    font-weight: 700;
    line-height: 1;
    padding: 5px 0 15px;
}
.circle-tile-description {
    text-transform: uppercase;
}
.circle-tile-footer {
    background-color: rgba(0, 0, 0, 0.1);
    color: rgba(255, 255, 255, 0.5);
    display: block;
    padding: 5px;
    transition: all 0.3s ease-in-out 0s;
}
.circle-tile-footer:hover {
    background-color: rgba(0, 0, 0, 0.2);
    color: rgba(255, 255, 255, 0.5);
    text-decoration: none;
}
.circle-tile-heading.dark-blue:hover {
    background-color: #2E4154;
}
.circle-tile-heading.green:hover {
    background-color: #138F77;
}
.circle-tile-heading.orange:hover {
    background-color: #DA8C10;
}
.circle-tile-heading.blue:hover {
    background-color: #2473A6;
}
.circle-tile-heading.red:hover {
    background-color: #CF4435;
}
.circle-tile-heading.purple:hover {
    background-color: #7F3D9B;
}
.tile-img {
    text-shadow: 2px 2px 3px rgba(0, 0, 0, 0.9);
}




                </style>
                ';

                $output=$header.$dashboard;
                break;

            case "panels":
                $panels='
              <style type="text/css">  
.dark-blue {
    background-color: #34495E;
}
.green {
    background-color: #16A085;
}
.blue {
    background-color: #2980B9;
}
.orange {
    background-color: #F39C12;
}
.red {
    background-color: #E74C3C;
}
.purple {
    background-color: #8E44AD;
}
.dark-gray {
    background-color: #7F8C8D;
}
.gray {
    background-color: #95A5A6;
}
.light-gray {
    background-color: #BDC3C7;
}
.yellow {
    background-color: #F1C40F;
}
.text-dark-blue {
    color: #34495E;
}
.text-green {
    color: #16A085;
}
.text-blue {
    color: #2980B9;
}
.text-orange {
    color: #F39C12;
}
.text-red {
    color: #E74C3C;
}
.text-purple {
    color: #8E44AD;
}
.text-faded {
    color: rgba(255, 255, 255, 0.7);
}
</style>
                ';
                $output=$header.$panels;
                break;

            case 'js':
                $output=$js;
                break;

            case 'bootstrap':
                $output=$bootstrap;
                break;

            default:
                $output=$header;
                break;
        }

        return $output;

    }




    /**
     * Short Number function Shortens the number into thousands and suffix them with an appropriate indicator. i.e 30,000,000 = 30M
     * @return string
     * @var double $number
     * */

    public static  function ShortNumber($number,$dp,$ot=null)
    {
        $output = '';
        $array=array();
        if(($number/POW(10,6))>=1)
        {
            $output = number_format(($number/POW(10,6)),$dp).'M';

            if(($number/POW(10,9))>=1)
            {
                $output = number_format(($number/POW(10,9)),$dp).'B';
                $array=[($number/POW(10,9)),'B'];
            }

            if(($number/POW(10,12))>=1)
            {
                $output = number_format(($number/POW(10,12)),$dp).'T';
                $array=[($number/POW(10,12)),'T'];
            }
            if (sizeof($array)==0){$array=[($number/POW(10,6)),'M'];}

        }else{

            $output = number_format(($number),$dp);
            $array=[$number,''];
        }

        if ($ot!=null){ $output=$array;}
        return $output;
    }


    /**
     * Span between two dates
     * @return integer
     * @var string $date1
     * @var string $date2
     * @var string $format
     * */

    public static function span($date1,$date2,$format=null)
    {
        if($format==null) {
            $format="a"; //will return the number of days
        }
        $output=0;
        if (!empty($date1)&&!empty($date2)){
            $d1 = date_create($date1);
            $d2 = date_create($date2);
            $difference = $d1->diff($d2);

            $output=$difference->format("%".$format);
        }

        return $output;
    }

    public static function Semester(){
        $semesterSQL="SELECT semester.semester_id AS 'id', semester.semester_number AS 'number', semester.description AS 'description' FROM semester WHERE is_active='1' ORDER  BY semester_number ASC; ";
        $semesterModel = Yii::$app->db->createCommand($semesterSQL)->queryAll();
        return ArrayHelper::map($semesterModel,'id','description');
    }

    public static function PriorityList($priority=null,$option=null)
    {
        $star='<span class="fa fa-star" style="color: #FFC300;"></span>';
        $output = $data = array();
        $data[1]=array(
            'id'=>1,
            'name'=>'Low',
            'star'=>'', //No Star
            'color'=>'#154360' //blue
        );

        $data[2]=array(
            'id'=>2,
            'name'=>'Medium',
            'star'=>$star,//One Star
            'color'=>'#5b2c6f' //Purple
        );


        $data[3]=array(
            'id'=>3,
            'name'=>'High',
            'star'=>$star.$star.$star, // Tree Stars
            'color'=>'#6e2c00' //orange
        );

        $data[4]=array(
            'id'=>4,
            'name'=>'Urgent',
            'star'=>$star.$star.$star.$star.$star, // Five Stars
            'color'=>'#c0392b' //Red
        );

        $output=$data;
        if ($priority!=null){

            if ($priority!=null && $option!=null){
                $output = $data[$priority][$option];
            }else{
                $output = $data[$priority];
            }
        }



        return $output;
    }

    public static function THNumber($number){
        $output = $number;
        switch ($number){
            case 1:
                $output=$number.'<sup>st</sup>';
                break;

            case 2:
                $output=$number.'<sup>nd</sup>';
                break;

            case 3:
                $output=$number.'<sup>rd</sup>';
                break;

            case 21:
                $output=$number.'<sup>st</sup>';
                break;

            case 22:
                $output=$number.'<sup>nd</sup>';
                break;

            case 23:
                $output=$number.'<sup>rd</sup>';
                break;

            default:
                $output=$number.'<sup>th</sup>';
                break;
        }
        return $output;
    }

    public static function PlanImplementer($academicYear,$institution,$Semester,$installment,$loanItem,$study_level,$version,$country=null)
    {
        $output = array();
        $WHERE = '';
        if ($country!==null){
            $WHERE = " AND institution.country_id = '$country'";
        }else{
            $WHERE = " AND programme.learning_institution_id = '$institution'";
        }

        $SQL = "
         SELECT 
                  
                  programme_group.group_code AS 'programme_code', 
               CONCAT(programme_group.group_name,' (',IFNULL(programme_group.group_code,'N/A'),')') AS 'programme_name', 
                                loan_item.item_name AS 'item_name',
                                loan_item.loan_item_id AS 'item_id',
                
                allocation.application_id AS 'application_id', 
                CONCAT(applicant.f4indexno,' | ', user.firstname,' ',user.surname) AS 'full_name',
                allocation.loan_item_id, 
                 allocation.allocation_batch_id AS 'allocation_batch_id', 
                 allocation_batch.batch_number AS 'batch_number', 
                allocation.allocated_amount AS 'allocated_amount', 
                allocation.is_canceled, 
                allocation.cancel_comment,
                application.transfer_status,
                application.applicant_category_id AS 'study_level',
                application.loanee_category AS 'study_place',
                application.programme_id AS 'programme_id',
                application.current_study_year AS 'current_study_year',
                application.bank_id,
                application.bank_account_name,
                application.bank_account_number,
                institution.country_id AS 'country'
                
                FROM allocation 
                RIGHT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id AND allocation_batch.is_approved = '1' AND (allocation_batch.partial_approve_status = '1' OR allocation_batch.larc_approve_status = '1')
               
                LEFT JOIN application ON allocation.application_id = application.application_id
                LEFT JOIN programme ON  application.programme_id =  programme.programme_id
                LEFT JOIN programme_group ON programme.programme_group_id = programme_group.programme_group_id
                LEFT JOIN learning_institution institution ON programme.learning_institution_id = institution.learning_institution_id
                LEFT JOIN loan_item ON  allocation.loan_item_id =  loan_item.loan_item_id
                LEFT JOIN applicant ON  application.applicant_id =  applicant.applicant_id
                LEFT JOIN user ON  applicant.user_id =  user.user_id
                WHERE CONCAT(allocation_batch.allocation_batch_id,'-',allocation.application_id,'-',allocation.loan_item_id,'-$Semester-$installment-$version') NOT IN(SELECT disbursement.sync_id FROM disbursement WHERE disbursement.loan_item_id=allocation.loan_item_id AND disbursement.application_id=allocation.application_id AND disbursement.allocation_batch_id = allocation_batch.allocation_batch_id) AND allocation.is_canceled = '0' AND (allocation_batch.partial_approve_status = '1' OR allocation_batch.larc_approve_status = '1')  AND allocation_batch.academic_year_id='$academicYear' AND  application.student_status <> 'STOPED' AND
                allocation.loan_item_id IN( SELECT '$loanItem' UNION SELECT disbursement_setting2.associated_loan_item_id FROM disbursement_setting2 WHERE disbursement_setting2.academic_year_id='$academicYear' AND disbursement_setting2.instalment_definition_id = '$installment' AND disbursement_setting2.loan_item_id='$loanItem')
                $WHERE AND programme.study_level = '$study_level'
                ORDER BY programme.programme_code ASC ,application.current_study_year ASC ;
         ";
        $rawQuery = \Yii::$app->db->createCommand($SQL)->queryAll();
        if ($country!==null){
            $countryID = $country;
        }else{
            $countryID = null;
        }

        if (sizeof($rawQuery)!=0){
            foreach ($rawQuery as $index=>$dataArray)
            {
                $programme = $dataArray['programme_id'];
                $allocatedAmount = $dataArray['allocated_amount'];
                $loanItem = $dataArray['item_id'];
                $YearOfStudy = $dataArray['current_study_year'];

                //$YearOfStudy = 1;

                $disbursementAmount=0;
                $application = $dataArray['application_id'];


                //Check Suspension
                $remainingFactor = self::CheckSuspension($application,$loanItem);

                if ($remainingFactor>0){ //Check if there's any amount remaining after suspension : (for Non-suspensions, Partial Suspensions and Upliftings )
                    //Adjust Allocated Amount
                    $allocatedAmount*=$remainingFactor;

                    $percentage = self::GetPlanPercent($programme,$institution,$academicYear,$YearOfStudy,$Semester,$installment,$loanItem,$country);
                    $disbursementAmount = $allocatedAmount*($percentage/100);
                    $output[]=array(
                        'full_name'=>$dataArray['full_name'],
                        'application'=>$dataArray['application_id'],
                        'programme_id'=>$programme,
                        'programme_code'=>$dataArray['programme_code'],
                        'institution_id'=>$institution,
                        'item_id'=>$loanItem,
                        'item_name'=>$dataArray['item_name'],
                        'year_of_study'=>$dataArray['current_study_year'],
                        'allocated_amount'=>$dataArray['allocated_amount'],
                        'disbursed_amount'=>$disbursementAmount,
                        'disbursement_percent'=>$percentage,
                        'semester'=>$Semester,
                        'installment'=>$installment,
                        'academic_year'=>$academicYear,
                        'study_level'=>$dataArray['study_level'],
                        'study_place'=>$dataArray['study_place'],
                        'allocation_batch_id'=>$dataArray['allocation_batch_id'],
                        'batch_number'=>$dataArray['batch_number'],

                    );
                }else{
                    /** Skip 100% Item Suspensions **/
                }


            }
        }





        return $output;
    }

    public static function GetPlanPercent($programme,$institution,$academicYear,$YearOfStudy,$Semester,$installment,$loanItem,$country=null)
    {
        $output = 0;


        if ($country!==null){// Overseas
            //Check for Overseas Settings
            $SQL="
            SELECT overseas_settings.disbursement_percent AS 'percent' 
            FROM overseas_settings
            WHERE 
            country_id = '$country' AND 
            academic_year = '$academicYear' AND 
            instalment = '$installment' AND 
            loan_item = '$loanItem' AND 
            year_of_study = '$YearOfStudy'
        LIMIT 1";
            $sResults=\Yii::$app->db->createCommand($SQL)->queryAll();
            if (sizeof($sResults)!=0) {
                $output = $sResults[0]['percent'];
            }
        }else{ //Local
            //Check for Special Institutional Plan
            $SQLs="
            SELECT disbursement_plan.disbursement_percent AS 'percent' 
            FROM disbursement_plan 
            WHERE 
            learning_institution = '$institution' AND 
            programme_id = '$programme' AND 
            academic_year = '$academicYear' AND 
            semester_number = '$Semester'  AND  
            instalment_id = '$installment' AND 
            loan_item_id = '$loanItem' AND 
            year_of_study = '$YearOfStudy'
        LIMIT 1";
            $sResults=\Yii::$app->db->createCommand($SQLs)->queryAll();
            if (sizeof($sResults)!=0){
                $output=$sResults[0]['percent'];
            }else{
                //Check for a default plan
                $SQLd = "
            SELECT disbursement_default_plan.disbursement_percent AS 'percent'  
            FROM disbursement_default_plan 
            WHERE 
           
            academic_year = '$academicYear' AND 
            semester_number = '$Semester'  AND  
            instalment_id = '$installment' AND 
            loan_item_id = '$loanItem' 
            
            LIMIT 1";
                $dResults=\Yii::$app->db->createCommand($SQLd)->queryAll();
                if (sizeof($dResults)!=0){
                    $output=$dResults[0]['percent'];
                }else{
                    $output = 0;
                }

            }
        }




        return $output;
    }

    public static function InstitutionChecklist(){
        $output="";
        $SQL = "
                SELECT 
                learning_institution.learning_institution_id AS 'id',
                learning_institution.institution_code AS 'code',
                learning_institution.institution_name AS 'name',
                learning_institution.country_id AS 'country'
                FROM learning_institution 
                WHERE learning_institution.institution_type = 'UNIVERSITY'
                ORDER BY learning_institution.institution_name ASC;
        ";
        $model=\Yii::$app->db->createCommand($SQL)->queryAll();

        if (sizeof($model)!=0){
            $output='<ol type="1">';
            foreach ($model as $index=>$dataArray){
                $output.='<li>'.$dataArray["name"].'</li>';
            }
            $output.='</ol>';

        }


        return $output;
    }

    public static function StudyCategory($index=null){
        $dataArray = array('1'=>'Local','2'=>'Overseas');
        if ($index!=null&&isset($dataArray[$index])){
            $output = $dataArray[$index];
        }else{
            $output = $dataArray;
        }
        return $output;
    }

    public static function DisbursedEntities($index=null){
        $dataArray = array('1'=>'Student','2'=>'Institution');
        if ($index!=null&&isset($dataArray[$index])){
            $output = $dataArray[$index];
        }else{
            $output = $dataArray;
        }
        return $output;
    }

    public static function PaymentStatus($index=null){
        //0=>NEW, 1=>SUBMITTED, 3=>APPROVED, 4=>DENIED, 5=>PARTIAL DISBURSED, 6=>FULLY DISBURSED
        //$dataArray = array(0=>'NEW','1'=>'SUBMITTED','3'=>'APPROVED','4'=>'DENIED','5'=>'PARTIAL DISBURSED','6'=>'FULLY DISBURSED');
        $dataArray = self::PayListStatus();
        if ($index!==null && isset($dataArray[$index])){
            $output = $dataArray[$index];
        }else{
            $output = $dataArray;
        }
        return $output;
    }

    public static function RequestStatus($index=null){
        //0=>NEW, 1=>SUBMITTED, 3=>APPROVED, 4=>DENIED, 5=>PARTIAL DISBURSED, 6=>FULLY DISBURSED
        // $dataArray = array(0=>'NEW','1'=>'DRAFT','2'=>'SUBMITTED','3'=>'APPROVED','4'=>'DENIED','5'=>'RECEIVED','6'=>'REJECTED','7'=>'PARTIAL DISBURSED','8'=>'FULLY DISBURSED');
        $dataArray = self::PayListStatus();

        if ($index!==null && isset($dataArray[$index])){
            $output = $dataArray[$index];
        }else{
            $output = $dataArray;
        }
        return $output;
    }

    public static function PayListAnalysis($institution,$academicYear,$loanItem,$semester,$option){
        $output = array();
        switch ($option){
            case "allocation":
                $SQL = "
                        SELECT 
                        COUNT(DISTINCT applicant.f4indexno) AS 'students', 
                        #CONCAT(user.firstname,' ',user.surname) AS 'full_name',
                        application.current_study_year,
                        application.programme_id,
                        programme.learning_institution_id,
                        allocation_batch.academic_year_id, 
                        CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'item_name',
                        allocation.loan_item_id,
                        SUM(allocation.allocated_amount) AS 'allocated_amount'
                        
                        FROM allocation
                        INNER JOIN application ON  allocation.application_id = application.application_id
                        INNER JOIN programme ON  application.programme_id = programme.programme_id
                        INNER JOIN applicant ON application.applicant_id = applicant.applicant_id
                        INNER JOIN user ON applicant.user_id = user.user_id
                        INNER JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                        INNER JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                        WHERE allocation.loan_item_id = '$loanItem' AND allocation_batch.academic_year_id = '$academicYear' AND programme.learning_institution_id = '$institution'
                      
                        GROUP BY application.current_study_year, allocation.loan_item_id
                        ORDER BY application.current_study_year ASC, applicant.f4indexno ASC, allocation.loan_item_id ASC
                        
                ";
                $output=\Yii::$app->db->createCommand($SQL)->queryAll();
                break;


            case "reported":
                $SQLx ="                     
                        SELECT 
                        COUNT(DISTINCT (allocation.application_id)) AS 'students',
                        application.current_study_year,
                        application.programme_id,
                        programme.learning_institution_id,
                        allocation_batch.academic_year_id,  
                        CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'item_name',
                        allocation.loan_item_id,
                        SUM(allocation.allocated_amount) AS 'allocated_amount'
                        FROM allocation
                                         
                        INNER JOIN application ON allocation.application_id = application.application_id
                        INNER JOIN applicant ON application.applicant_id = applicant.applicant_id
                        INNER JOIN fingerprint_verification ON fingerprint_verification.index_number = applicant.f4indexno AND fingerprint_verification.institution_id='$institution' AND fingerprint_verification.academic_year = '$academicYear' AND fingerprint_verification.semester_number = '$semester'
                     
                        
                        LEFT JOIN user ON applicant.user_id = user.user_id
                        LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                        LEFT JOIN programme ON application.programme_id = programme.programme_id
                        LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                        LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                        WHERE allocation_batch.is_approved='1' AND  allocation_batch.academic_year_id = '$academicYear' AND programme.learning_institution_id = '$institution' AND allocation.loan_item_id = '$loanItem'
                        GROUP BY application.current_study_year, allocation.loan_item_id
                        
                        ORDER BY application.current_study_year ASC, applicant.f4indexno ASC, allocation.loan_item_id ASC
                ";


                $SQL ="                     
                        SELECT 
                        COUNT(DISTINCT (allocation.application_id)) AS 'students',
                        application.current_study_year,
                        application.programme_id,
                        programme.learning_institution_id,
                        allocation_batch.academic_year_id,  
                        CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'item_name',
                        allocation.loan_item_id,
                        SUM(allocation.allocated_amount) AS 'allocated_amount'
                        FROM allocation
                                         
                        INNER JOIN application ON allocation.application_id = application.application_id
                        INNER JOIN applicant ON application.applicant_id = applicant.applicant_id
                       
                        
                        LEFT JOIN user ON applicant.user_id = user.user_id
                        LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                        LEFT JOIN programme ON application.programme_id = programme.programme_id
                        LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                        LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                        WHERE allocation_batch.is_approved='1' AND  allocation_batch.academic_year_id = '$academicYear' AND programme.learning_institution_id = '$institution' AND allocation.loan_item_id = '$loanItem'
                        GROUP BY application.current_study_year, allocation.loan_item_id
                        
                        ORDER BY application.current_study_year ASC, applicant.f4indexno ASC, allocation.loan_item_id ASC
                ";
                $output=\Yii::$app->db->createCommand($SQL)->queryAll();
                break;


            case "batchReady":
                $SQL = "                      
                        SELECT 
                        COUNT(DISTINCT (allocation.application_id)) AS 'students',
                        application.current_study_year,
                        application.programme_id,
                        programme.learning_institution_id,
                        allocation_batch.academic_year_id, 
                        CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'item_name',
                        allocation.loan_item_id,
                        SUM(allocation.allocated_amount) AS 'allocated_amount'
                        FROM allocation
                        LEFT JOIN application ON allocation.application_id = application.application_id
                        LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                        LEFT JOIN user ON applicant.user_id = user.user_id
                        LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                        LEFT JOIN programme ON application.programme_id = programme.programme_id
                        LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                        LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                        WHERE allocation_batch.academic_year_id = '$academicYear' AND programme.learning_institution_id = '$institution' AND allocation.loan_item_id = '$loanItem'  AND allocation_batch.is_approved = '1' 
                        ORDER BY application.current_study_year ASC, applicant.f4indexno ASC, allocation.loan_item_id ASC
                    
                ";
                $output=\Yii::$app->db->createCommand($SQL)->queryAll();
                break;


            case "paylist":
                /*
                 * $SQL = "
                        SELECT
                        COUNT(DISTINCT (allocation.application_id)) AS 'students',
                        application.current_study_year,
                        application.programme_id,
                        programme.learning_institution_id,
                        allocation_batch.academic_year_id,
                        CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'item_name',
                        allocation.loan_item_id,
                        SUM(allocation.allocated_amount) AS 'allocated_amount'
                        FROM allocation

                        INNER JOIN application ON allocation.application_id = application.application_id
                        INNER JOIN applicant ON application.applicant_id = applicant.applicant_id
                        INNER JOIN fingerprint_verification ON fingerprint_verification.index_number = applicant.f4indexno AND fingerprint_verification.institution_id='$institution' AND fingerprint_verification.academic_year = '$academicYear' AND fingerprint_verification.semester_number = '$semester'



                        LEFT JOIN user ON applicant.user_id = user.user_id
                        LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                        LEFT JOIN programme ON application.programme_id = programme.programme_id
                        LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                        LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                        WHERE allocation_batch.academic_year_id = '$academicYear' AND programme.learning_institution_id = '$institution' AND allocation.loan_item_id = '$loanItem'  AND allocation_batch.is_approved = '1'
                        GROUP BY application.current_study_year, allocation.loan_item_id
                        ORDER BY application.current_study_year ASC, applicant.f4indexno ASC, allocation.loan_item_id ASC


                ";
                */
                 $SQL="
                    SELECT 
                        DISTINCT allocation.allocation_id,
                        COUNT(DISTINCT (allocation.application_id)) AS 'students',
                        application.current_study_year,
                        application.programme_id,
                        programme.learning_institution_id,
                        allocation_batch.academic_year_id,  
                        CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'item_name',
                        allocation.loan_item_id,
                        SUM(allocation.allocated_amount) AS 'allocated_amount'
                        FROM allocation
                                         
                        INNER JOIN application ON allocation.application_id = application.application_id 
						INNER JOIN applicant ON application.applicant_id = applicant.applicant_id #AND applicant.f4indexno IN(SELECT DISTINCT fingerprint_verification.index_number FROM fingerprint_verification WHERE  fingerprint_verification.index_number = applicant.f4indexno AND fingerprint_verification.institution_id='$institution' AND fingerprint_verification.academic_year = '$academicYear' AND fingerprint_verification.semester_number = '$semester')
                       
                    
                     
                        
                        LEFT JOIN user ON applicant.user_id = user.user_id
                        LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                        LEFT JOIN programme ON application.programme_id = programme.programme_id
                        LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                        LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                        WHERE allocation_batch.is_approved='1' AND  allocation_batch.academic_year_id = '$academicYear' AND programme.learning_institution_id = '$institution' AND allocation.loan_item_id = '$loanItem'
                        AND application.application_id NOT IN(SELECT institution_paylist.application_id FROM institution_paylist WHERE institution_paylist.sync_id = (CONCAT(allocation.application_id,'-',allocation.loan_item_id,'-',allocation_batch.academic_year_id,'-',$semester)))
                        GROUP BY application.current_study_year, allocation.loan_item_id
                        
                        ORDER BY application.current_study_year ASC, applicant.f4indexno ASC, allocation.loan_item_id ASC
                ";

                 /*

                $SQL12="
                    SELECT 
                        DISTINCT allocation.allocation_id,
                        COUNT(DISTINCT (allocation.application_id)) AS 'students',
                        application.current_study_year,
                        application.programme_id,
                        programme.learning_institution_id,
                        allocation_batch.academic_year_id,  
                        CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'item_name',
                        allocation.loan_item_id,
                        SUM(allocation.allocated_amount) AS 'allocated_amount'
                        FROM allocation
                                         
                        INNER JOIN application ON allocation.application_id = application.application_id 
						INNER JOIN applicant ON application.applicant_id = applicant.applicant_id
                     
                        
                        LEFT JOIN user ON applicant.user_id = user.user_id
                        LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                        LEFT JOIN programme ON application.programme_id = programme.programme_id
                        LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                        LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                        WHERE allocation_batch.is_approved='1' AND  allocation_batch.academic_year_id = '$academicYear' AND programme.learning_institution_id = '$institution' AND allocation.loan_item_id = '$loanItem'
                        AND application.application_id NOT IN(SELECT institution_paylist.application_id FROM institution_paylist WHERE institution_paylist.sync_id = (CONCAT(allocation.application_id,'-',allocation.loan_item_id,'-',allocation_batch.academic_year_id,'-',$semester)))
                        GROUP BY application.current_study_year, allocation.loan_item_id
                        
                        ORDER BY application.current_study_year ASC, applicant.f4indexno ASC, allocation.loan_item_id ASC
                ";*/
                $output=\Yii::$app->db->createCommand($SQL)->queryAll();
                break;


        }

        return $output;
    }

    public static function GeneratePayList($institution,$academicYear,$loanItem,$semester,$requestID)
    {
        $user = \Yii::$app->user->id;

        $output = 0;
        /*
                $SQL = "
                          INSERT IGNORE INTO institution_paylist (request_id, academic_year, semester_number, application_id, programme_id, loan_item_id, allocated_amount, status, created_at, created_by,sync_id)
                            SELECT
                            '$requestID' AS 'request_id',
                            allocation_batch.academic_year_id,
                            '$semester' AS 'semester_number',
                            allocation.application_id,
                            application.programme_id,
                            allocation.loan_item_id,
                            SUM(allocation.allocated_amount) AS 'allocated_amount',
                            '0' AS 'status',
                            CONCAT(CURDATE(),' ',CURTIME()) AS 'create_at',
                            '$user' AS 'create_by',
                            CONCAT(allocation.application_id,'-',allocation.loan_item_id,'-',allocation_batch.academic_year_id,'-',$semester) AS 'sync_id'
                            FROM allocation
                            LEFT JOIN application ON allocation.application_id = application.application_id
                            LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                            LEFT JOIN user ON applicant.user_id = user.user_id
                            LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                            LEFT JOIN programme ON application.programme_id = programme.programme_id
                            LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                            LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                            WHERE allocation_batch.academic_year_id = '$academicYear' AND programme.learning_institution_id = '$institution' AND allocation.loan_item_id = '$loanItem'  AND allocation_batch.is_approved = '1' AND EXISTS (SELECT * FROM fingerprint_verification WHERE fingerprint_verification.academic_year = '$academicYear')

                            GROUP BY allocation.application_id
                            ORDER BY user.username ASC
                ";*/

        $SQL = "
                INSERT IGNORE INTO institution_paylist (request_id, academic_year, semester_number, application_id, programme_id, loan_item_id, allocated_amount, allocation_id, allocation_batch_id, status, created_at, created_by,sync_id)
                   SELECT 
                    '$requestID' AS 'request_id',
                    allocation_batch.academic_year_id, 
                    '$semester' AS 'semester_number',
                    allocation.application_id,
                    application.programme_id,
                    allocation.loan_item_id,
                    SUM(allocation.allocated_amount) AS 'allocated_amount',
                    allocation.allocation_id AS 'allocation_id',
                    allocation.allocation_batch_id AS 'allocation_batch_id',
                    '0' AS 'status',
                    CONCAT(CURDATE(),' ',CURTIME()) AS 'create_at',
                    '$user' AS 'create_by',
                    CONCAT(allocation.application_id,'-',allocation.loan_item_id,'-',allocation_batch.academic_year_id,'-',$semester) AS 'sync_id'
                    FROM allocation
                                                       
                    INNER JOIN application ON allocation.application_id = application.application_id 
					INNER JOIN applicant ON application.applicant_id = applicant.applicant_id AND applicant.f4indexno IN(SELECT DISTINCT fingerprint_verification.index_number FROM fingerprint_verification WHERE  fingerprint_verification.index_number = applicant.f4indexno AND fingerprint_verification.institution_id='$institution' AND fingerprint_verification.academic_year = '$academicYear' AND fingerprint_verification.semester_number = '$semester')
                
                    LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                    LEFT JOIN programme ON application.programme_id = programme.programme_id
                    LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                    LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                    WHERE application.student_status = 'ONSTUDY' AND allocation_batch.academic_year_id = '$academicYear' AND programme.learning_institution_id = '$institution' AND allocation.loan_item_id = '$loanItem'  AND allocation_batch.is_approved = '1'
                    
                    GROUP BY allocation.application_id
                    ORDER BY applicant.f4indexno ASC
                ";

        $output = \Yii::$app->db->createCommand($SQL)->execute();


        return $output;
    }

    public static function AssignmentArray($user){
        $output = array();
        $iSQL="SELECT learning_institution_id FROM learning_institution WHERE institution_type = 'UNIVERSITY' AND learning_institution_id IN (SELECT institution_assignment.institution_id FROM institution_assignment WHERE institution_assignment.officer_id='$user')";
        $InstitutionModel = Yii::$app->db->createCommand($iSQL)->queryAll();
        if (sizeof($InstitutionModel)!=0){
            $data = array();

            foreach ($InstitutionModel AS $index=>$dataArray){
                $data[] = $dataArray['learning_institution_id'];
            }
            $output=$data;
        }

        return $output;
    }

    public static function AssignmentList($user){
        $output = '';
        $array = self::AssignmentArray($user);
        if (sizeof($array)!=0){

            $output="'".implode("','",$array)."'";
        }

        return $output;
    }

    public static function UserInfo($id,$field){
        $output = '';
        $model= User::findOne($id);
        if (sizeof($model)!=0){
            if ($field=='fullName'){
                $output = $model->firstname.' '.$model->surname;
            }else{
                $output= $model->$field;
            }
        }

        return $output;
    }

    public static function InstalmentType($index){
        $output = '';

        $dataArray = array(
            '1'=>'Normal',
            '2'=>'Addition',
        );
        //1: Normal instalment disbursement, 2: Addition instalment disbursement
        if (isset($dataArray[$index])){
            $output =$dataArray[$index];
        }

        return $output;
    }

    public static function MovementGenerator($batchID,$user,$amount){
        $now = date('Y-m-d H:i:s');
        $movementOrder=self::PayListChain($amount);

        $output = false;

        if (sizeof($movementOrder)!=0){
            $SQL = "";
            $deleteSQL = "DELETE FROM disbursement_payoutlist_movement WHERE disbursements_batch_id = '$batchID';";
            Yii::$app->db->createCommand($deleteSQL)->execute();
            //disbursements_batch_id, from_officer, to_officer, movement_status, disbursement_task_id, signature, date_out, comment, created_at, created_by, updated_at, updated_by
            $counter = 0; $fromOfficer='0';
            foreach ($movementOrder as $index=>$movement){

                $counter++;
                if ($counter==1){
                    $fromOfficer = $user;
                }/*else{

                }*/
                // $toOfficer = 0;
                $toOfficer = $movement['user_id'];
                $taskID = $movement['task_id'];
                $structureID = $movement['structure_id'];
                $orderLevel = $movement['order_level'];
                $SQL.= "
                        INSERT IGNORE INTO disbursement_payoutlist_movement 
                        (disbursements_batch_id, from_officer, to_officer, movement_status, disbursement_task_id, structure_id, order_level, signature, date_out, comment, created_at, created_by, updated_at, updated_by) 
                        VALUES 
                        ('$batchID','$fromOfficer','$toOfficer','0','$taskID','$structureID','$orderLevel',NULL,NULL,NULL,'$now','$user',NULL ,NULL);";

                $fromOfficer = $movement['user_id'];

            }
            if ($SQL!=''){
                Yii::$app->db->createCommand($SQL)->execute();
                $output=true;
            }
        }

        return $output;
    }

    public static  function PayListChain($amount){
        $output = array();
        $SQL = "
             SELECT 
                disbursement_task_assignment.disbursement_task_assignment_id, 
                
                disbursement_task_assignment.disbursement_schedule_id AS 'schedule_id', 
                disbursement_schedule.from_amount AS 'min_amount',
                disbursement_schedule.to_amount AS 'max_amount',
                disbursement_schedule.operator_name AS 'operator',
                
                disbursement_user_structure.user_id AS 'user_id',
                CONCAT(user.firstname,' ',user.surname) AS 'user_name',
                
                disbursement_task_assignment.disbursement_structure_id AS 'structure_id', 
                disbursement_structure.structure_name AS 'structure_name',
                disbursement_structure.order_level AS 'order_level',
                disbursement_task_assignment.disbursement_task_id AS 'task_id',
                 disbursement_task_definition.task_name AS 'task_name',
                 disbursement_task_definition.code AS 'task_code'
                 
                FROM disbursement_task_assignment
                LEFT JOIN disbursement_task_definition ON disbursement_task_assignment.disbursement_task_id = disbursement_task_definition.disbursement_task_id
                LEFT JOIN disbursement_structure ON disbursement_task_assignment.disbursement_structure_id = disbursement_structure.disbursement_structure_id AND disbursement_structure.status='1'
                LEFT JOIN disbursement_schedule ON disbursement_task_assignment.disbursement_schedule_id = disbursement_schedule.disbursement_schedule_id
                LEFT JOIN disbursement_user_structure ON disbursement_structure.disbursement_structure_id = disbursement_user_structure.disbursement_structure_id AND  disbursement_user_structure.status='1'
                LEFT JOIN user ON disbursement_user_structure.user_id = user.user_id
                
                WHERE (disbursement_schedule.operator_name = 'Between' AND  ('$amount' BETWEEN disbursement_schedule.from_amount AND disbursement_schedule.to_amount)) OR (disbursement_schedule.operator_name = 'Greater than' AND '$amount'>disbursement_schedule.from_amount)
                GROUP BY disbursement_task_assignment.disbursement_structure_id
                ORDER BY disbursement_structure.order_level DESC, disbursement_schedule.from_amount ASC
        ";
        $output = Yii::$app->db->createCommand($SQL)->queryAll();
        return $output;
    }

    public static function MovementCheck($batchID)
    {
        $output = array();
        $SQL = "SELECT * FROM disbursement_payoutlist_movement WHERE disbursement_payoutlist_movement.movement_status='1' AND disbursement_payoutlist_movement.disbursements_batch_id = '$batchID'";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model;
        }
        return $output;
    }

    public static function MovementQuery($batchID,$option=null){
        $output = array();
        $BatchModel = DisbursementBatch::findOne($batchID);
        $LIMIT = "";
        if (sizeof($BatchModel)!=0){
            $task = $level = 0 ;
            $CMM = self::CurrentMovement($batchID);
            if (sizeof($CMM)!=0){
                $task = $CMM[0]['disbursement_task_id'];
                $movementID = $CMM[0]['movement_id'];
                $order_level = $CMM[0]['order_level'];
            }
            $amount = $BatchModel->Amount;

            $field = "";
            switch ($option){
                case "current":
                    $WHERE=" disbursement_task_assignment.disbursement_task_id='$task' AND disbursement_payoutlist_movement.movement_id = '$movementID' AND disbursement_payoutlist_movement.movement_status = '0' AND disbursement_structure.order_level='$order_level' AND ";
                    $JOIN = " INNER JOIN disbursement_payoutlist_movement ON disbursement_task_assignment.disbursement_task_id = disbursement_payoutlist_movement.disbursement_task_id AND disbursement_payoutlist_movement.movement_id = '$movementID' AND disbursement_payoutlist_movement.movement_status = '0'";
                    //$LIMIT=" LIMIT 1";
                    $field.= "disbursement_payoutlist_movement.movement_id AS 'movement_id',";
                    $field.= "disbursement_payoutlist_movement.movement_status AS 'movement_status',";
                    break;
                default:
                    $WHERE = $JOIN ='';
                    break;
            }
            $SQL = "  SELECT $field
                disbursement_task_assignment.disbursement_task_assignment_id, 
                disbursement_task_assignment.disbursement_schedule_id AS 'schedule_id', 
                disbursement_schedule.from_amount AS 'min_amount',
                disbursement_schedule.to_amount AS 'max_amount',
                disbursement_schedule.operator_name AS 'operator',
                disbursement_user_structure.user_id AS 'user_id',
                CONCAT(user.firstname,' ',user.surname) AS 'user_name',
                
                
                disbursement_task_assignment.disbursement_structure_id AS 'structure_id', 
                disbursement_structure.structure_name AS 'structure_name',
                disbursement_structure.order_level AS 'order_level',
                disbursement_task_assignment.disbursement_task_id AS 'task_id',
                disbursement_task_definition.task_name AS 'task_name',
                disbursement_task_definition.code AS 'task_code'
                 
                FROM disbursement_task_assignment
                LEFT JOIN disbursement_task_definition ON disbursement_task_assignment.disbursement_task_id = disbursement_task_definition.disbursement_task_id
                LEFT JOIN disbursement_structure ON disbursement_task_assignment.disbursement_structure_id = disbursement_structure.disbursement_structure_id AND disbursement_structure.status='1'
                LEFT JOIN disbursement_schedule ON disbursement_task_assignment.disbursement_schedule_id = disbursement_schedule.disbursement_schedule_id
                LEFT JOIN disbursement_user_structure ON disbursement_structure.disbursement_structure_id = disbursement_user_structure.disbursement_structure_id AND  disbursement_user_structure.status='1'
                LEFT JOIN user ON disbursement_user_structure.user_id = user.user_id
               $JOIN 
                WHERE $WHERE (disbursement_schedule.operator_name = 'Between' AND  ('$amount' BETWEEN disbursement_schedule.from_amount AND disbursement_schedule.to_amount)) OR (disbursement_schedule.operator_name = 'Greater than' AND '$amount'>disbursement_schedule.from_amount)
                #GROUP BY disbursement_task_assignment.disbursement_structure_id
                ORDER BY disbursement_structure.order_level DESC, disbursement_schedule.from_amount ASC $LIMIT";
            //$model = Yii::$app->db->createCommand($SQL)->queryAll();
            $output = Yii::$app->db->createCommand($SQL)->queryAll();

        }


        return $output;
    }
    public static function MovementOwner($batchID){
        $output = array();


        $model = self::MovementQuery($batchID,'current');

        if(sizeof($model)!=0){
            foreach ($model as $index=>$dataArray){
                $output[$dataArray['user_id']]=$dataArray['user_id'];
            }
        }


        return $output;
    }

    public static  function CurrentMovement($batchID,$limit=null){
        $output = array();
        $WHERE = $LIMIT = "";
        /* if ($user!==null){
             $WHERE.= " AND  to_officer='$user' ";
         }*/

        if ($limit!==null){
            $LIMIT = " LIMIT $limit";
        }else{
            $LIMIT = " LIMIT 1";
        }
        $SQL = "
                SELECT disbursement_payoutlist_movement.movement_id, 
                disbursement_payoutlist_movement.disbursement_task_id, 
                disbursement_structure.order_level 
                FROM disbursement_payoutlist_movement 
                LEFT JOIN disbursement_structure ON disbursement_payoutlist_movement.structure_id = disbursement_structure.disbursement_structure_id  WHERE movement_status = '0'  AND disbursements_batch_id = '$batchID' 
                ORDER BY movement_id ASC, disbursement_structure.order_level DESC $LIMIT;";
        /*$SQL = "
                SELECT disbursement_payoutlist_movement.movement_id,
                disbursement_payoutlist_movement.disbursement_task_id,
                disbursement_structure.order_level
                FROM disbursement_payoutlist_movement
                LEFT JOIN disbursement_task_definition ON disbursement_payoutlist_movement.disbursement_task_id = disbursement_task_definition.disbursement_task_id
                LEFT JOIN disbursement_task_assignment ON disbursement_task_definition.disbursement_task_id = disbursement_task_assignment.disbursement_task_id
                LEFT JOIN disbursement_structure ON disbursement_task_assignment.disbursement_structure_id = disbursement_structure.disbursement_structure_id
                WHERE movement_status = '0'  AND disbursements_batch_id = '$batchID'
                ORDER BY movement_id ASC, disbursement_structure.order_level DESC $LIMIT;";*/
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model;
        }
        return $output;
    }

    public static  function NextMovement($batchID,$user=null){
        $output = array();

        $limit=1;$WHERE = $LIMIT = "";
        if ($user!==null){
            $WHERE.= " AND  from_officer='$user' AND to_officer='0' ";
        }


        $LIMIT = " LIMIT 1";

        $SQL = "SELECT * FROM disbursement_payoutlist_movement WHERE movement_status = '0'  AND disbursements_batch_id = '$batchID' $WHERE ORDER BY movement_id ASC $LIMIT;";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model;
        }
        return $output;
    }
    public static function RemainingMovement($batchID,$currentID){
        $SQL = "SELECT * FROM disbursement_payoutlist_movement WHERE disbursements_batch_id = '$batchID' AND movement_id > '$currentID' AND movement_status='0' ORDER BY movement_id ASC;";
        return Yii::$app->db->createCommand($SQL)->queryAll();
    }

    public static function PreviousMovements($batchID,$currentID){
        $SQL = "SELECT * FROM disbursement_payoutlist_movement WHERE disbursements_batch_id = '$batchID' AND movement_id < '$currentID' AND movement_status='1' ORDER BY movement_id ASC;";
        return Yii::$app->db->createCommand($SQL)->queryAll();
    }

    public static function MovementMatrix($batchID,$currentMovementID,$action)
    {
        $output = $dataArray = array();
        $user = Yii::$app->user->id;
        $now = date("Y-m-d H:i:s");

        $batchMovements = self::MovementQuery($batchID);

        $dataArray['CMV_ID'] = $currentMovementID;
        $REMAINING = self::RemainingMovement($batchID,$currentMovementID);
        $PREVIOUS = self::PreviousMovements($batchID,$currentMovementID);

        if(sizeof($REMAINING)!=0){
            $NextMovementID = $REMAINING[0]['movement_id'];
            $dataArray['NMV_ID'] = $NextMovementID;
        }
        if(sizeof($PREVIOUS)!==0){
            $PrevMovementID = $PREVIOUS[(sizeof($PREVIOUS)-1)]['movement_id'];
            if(sizeof($PREVIOUS)==0){
                $dataArray['PMV_ID'] = $PrevMovementID;
            }
        }


        $dataArray['BMV'] = $batchMovements;

        $prevModel = PayoutlistMovement::findOne($PrevMovementID);
        $model = PayoutlistMovement::findOne($currentMovementID);
        $modelBatch = DisbursementBatch::findOne($model->disbursements_batch_id);
        if (sizeof($model)!=0 && sizeof($modelBatch)!==0){
            if (isset($dataArray['NMV_ID'])){ //Check if there's next movement

                $nextModel = PayoutlistMovement::findOne($dataArray['NMV_ID']);

                switch ($action){
                    case 0: //REJECTION AT LAST STAGE
                        if (isset($dataArray['PMV_ID'])){

                            $modelBatch->is_approved = 0;
                            $modelBatch->is_submitted = 0;


                            ///0=>'NEW', 1=>'SUBMITTED',2=>'REVIEWED', 3=>'APPROVED', 4=>'REJECTED',6=>'ACCEPTED', 7=>'DISBURSED', 8=> 'RECEIVED', 9=>'WAITING_RETURN' , 10=>'RETURNED'
                            //Update The Entire Batch Status
                            $BatchUpdate = "UPDATE disbursement SET status_date = '$now', status = '0' WHERE disbursement_batch_id = '$batchID';";
                            Yii::$app->db->createCommand($BatchUpdate)->execute();
                        }else{
                            $modelBatch->is_approved = 0;
                            //0=>'NEW', 1=>'SUBMITTED',2=>'REVIEWED', 3=>'APPROVED', 4=>'REJECTED',6=>'ACCEPTED', 7=>'DISBURSED', 8=> 'RECEIVED', 9=>'WAITING_RETURN' , 10=>'RETURNED'
                            //Update The Entire Batch Status
                            $BatchUpdate = "UPDATE disbursement SET status_date = '$now', status = '4' WHERE disbursement_batch_id = '$batchID';";
                            Yii::$app->db->createCommand($BatchUpdate)->execute();
                            //$model->from_officer = $model->created_by;
                        }
                        if (sizeof($prevModel)!=0){
                            if ($prevModel->from_officer == $prevModel->created_by ){
                                $modelBatch->is_submitted = 0;
                            }
                            $prevModel->movement_status = 0;

                            $prevModel->save();
                        }else{
                            $modelBatch->is_submitted = 0;
                        }

                        break;


                    case 1: //APPROVAL AT MIDDLE STAGE
                        $modelBatch->is_approved = 0;
                        $modelBatch->is_submitted = 1;
                        if (sizeof($nextModel)!=0){
                            $nextModel->from_officer = $model->to_officer;
                            $nextModel->created_at = date("Y-m-d");
                            $nextModel->created_by = $user;

                            $nextModel->updated_at = date("Y-m-d");
                            $nextModel->updated_by = $user;
                            $nextModel->save();
                        }
                        break;
                }



            }else{ // If there's no Next movement

                switch ($action){
                    case 0: //REJECTION AT LAST STAGE
                        $modelBatch->is_approved = 0;
                        $modelBatch->is_submitted = 0;
                        //0=>'NEW', 1=>'SUBMITTED',2=>'REVIEWED', 3=>'APPROVED', 4=>'REJECTED',6=>'ACCEPTED', 7=>'DISBURSED', 8=> 'RECEIVED', 9=>'WAITING_RETURN' , 10=>'RETURNED'
                        //Update The Entire Batch Status
                        $BatchUpdate = "UPDATE disbursement SET status_date = '$now', status = '4' WHERE disbursement_batch_id = '$batchID';";
                        Yii::$app->db->createCommand($BatchUpdate)->execute();
                        break;


                    case 1: //APPROVAL AT LAST STAGE
                        //Approve Batch
                        $modelBatch->is_approved = 1;
                        $modelBatch->is_submitted = 1;
                        $modelBatch->approval_date =$now;
                        $modelBatch->approved_by = $user;
                        $modelBatch->approval_comment = $model->comment;

                        //0=>'NEW', 1=>'SUBMITTED',2=>'REVIEWED', 3=>'APPROVED', 4=>'REJECTED',6=>'ACCEPTED', 7=>'DISBURSED', 8=> 'RECEIVED', 9=>'WAITING_RETURN' , 10=>'RETURNED'
                        //Update The Entire Batch Status
                        $BatchUpdate = "UPDATE disbursement SET status_date = '$now', status = '7' WHERE disbursement_batch_id = '$batchID';";
                        Yii::$app->db->createCommand($BatchUpdate)->execute();
                        self::DisbursementStatementCreator($batchID);

                        break;
                }

            }

            $modelBatch->save();

        }








        $output = true;
        return $output;
    }

    public static function Structure($batchID,$user,$taskID){
        $output = '';
        $batchModel = DisbursementBatch::findOne($batchID);
        if (sizeof($batchModel)!=0){
            $amount = $batchModel->Amount;
            $chainModel = self::PayListChain($amount);
            $data=array();
            if (sizeof($chainModel)!=0){
                foreach ($chainModel AS $index=>$dataArray){
                    if ($dataArray['user_id']==$user && $dataArray['task_id']==$taskID){
                        $data[] = $dataArray['structure_name'];
                    }
                }
            }
            if (sizeof($data)!=0){
                $output = implode(',',$data);
            }

        }

        return $output;
    }

    public static function SuspensionType($index=null){
        $output = '';
        $typeArray = array('1'=>'SUSPENSION','0'=>'UPLIFTING');
        if ($index!==null){
            if (isset($typeArray[$index])){
                $output = $typeArray[$index];
            }else{
                $output = '';
            }
        }else{
            $output = $typeArray;
        }

        return $output;
    }

    public static function UploadFile($model,$fileField,$FileName,$destination){
        $output = false;
        //$model->file = UploadedFile::getInstance($model, 'file');
        $model->$fileField = UploadedFile::getInstance($model, $fileField);

        if ($model->$fileField /*&& $model->validate()*/) {
            $extension =$model->$fileField->extension;
            $model->$fileField->saveAs($destination. $FileName . '.' . $extension);
            $output=$FileName.'.'.$extension;
        }
        return $output;
    }

    public static function SuspensionEvidence($id){
        $output = '';
        $SQL = "SELECT * FROM disbursement_suspension WHERE  id = '$id';";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $documentList = '';
            foreach ($model as $index=>$dataArray) {
                $documentList = $dataArray['supporting_document'];
            }

            if ($documentList!=''){
                $documentArray=(ARRAY)json_decode($documentList);
                foreach ($documentArray AS $di=>$documentName){
                    if (!empty($documentName)&&$documentName!=false){


                        $link = '../attachments/disbursement/'.$documentName;
                        $output.='
                    <p>
                   
                    
                            <embed src="'.$link.'#page=1&zoom=100" width="100%" height="1500px">
                    </p>
                    ';
                    }
                }
            }
        }
        return $output;
    }

    public static function SuspensionHistory($id){
        $output = $tr= '';
        $SQL = "SELECT * FROM disbursement_suspension WHERE  id = '$id';";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $historyList = '';
            foreach ($model as $index=>$dataArray) {
                $historyList = $dataArray['suspension_history'];
            }

            if ($historyList!=''){
                $historyArray=(ARRAY)json_decode($historyList);
                foreach ($historyArray AS $di=>$dataObject){

                    $tr.='
                    <tr>
                        <td>'.date("d/m/Y",strtotime($dataObject->status_date)).'</td>
                        <td>'.$dataObject->status.'</td>
                        <td>'.self::SuspensionReason($dataObject->status_reason).' (<i class="text-muted">'.self::SuspensionReason($dataObject->status_reason,"description").'</i>)'.'</td>
                        <td>'.$dataObject->remarks.'</td>
                        <td class="text-right text-bold text-red">'.$dataObject->suspension_percent.'</td>
                        <td class="text-right text-bold text-green">'.$dataObject->remaining_percent.'</td>
                    </tr>
                    ';
                    $documentName=$dataObject->supporting_document;

                    /* if (!empty($documentName)&&$documentName!=false){


                         $link = '../attachments/disbursement/'.$documentName;
                         $output.='
                     <p>


                             <embed src="'.$link.'#page=1&zoom=100" width="100%" height="1500px">
                     </p>
                     ';
                     }*/
                }

                $output.='
                <table class="table table-condensed table-bordered">
                <thead class="bg-blue-gradient">
                    <tr class="text-uppercase">
                        <th>Date</th>
                        <th>Operation</th>
                        <th>Reason</th>
                        <th>Remarks</th>
                        <th>Suspended %</th>
                        <th>Remained %</th>
                    </tr>
                </thead>
                <tbody>'.$tr.'</tbody>
                </table>
                ';
            }

        }
        return $output;
    }

    public static function SuspensionReason($id,$field=null){
        $output = '';
        $model = SuspensionReason::findOne($id);
        if (sizeof($model)!=0){
            if ($field!==null){
                $output = $model->$field;
            }else{
                $output = $model->name;
            }

        }

        return $output;
    }

    public static function AutoSuspendAll($indexNumber,$userID=null,$SuspensionReason=null){
        $output = false;
        $now = date('Y-m-d H:i:s');
        if($userID==null){
            $user = '0'; //System Automated
        }else{
            $user = $userID;
        }

        if ($SuspensionReason==null){
            $reason = '1'; //'1', 'RETURN', 'Disbursement Overdue without being received by lonee', '1', '1'
            $remarks = 'Disbursement Overdue without being received by lonee';
        }else{
            $reason = $SuspensionReason;
            $remarks = self::SuspensionReason($SuspensionReason,'description');
        }


        $mode = '1'; //1=>ALL ITEMS , 2=>SELECTIVE
        $lnSQL = "";
        $doc = 'GUIDELINES_AND_CRITERIA_FOR_2018_2019.pdf';
        $history = $document = array();
        $counter = 0;
        $liSQL = "SELECT * FROM loan_item WHERE is_active = '1';";
        $liModel = Yii::$app->db->createCommand($liSQL)->queryAll();

        $aplSQL = "SELECT application.application_id FROM application
                LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                WHERE applicant.f4indexno = '$indexNumber'";
        $appModel = Yii::$app->db->createCommand($aplSQL)->queryAll();
        if (sizeof($appModel)!=0){

            $insertSQL="";
            $updateSQL="";
            $applicationID = $appModel[0]['application_id'];
            $S_percent = 100;
            $R_percent = 0;
            foreach ($liModel as $index=>$itemArray){
                $loanItem = $itemArray['loan_item_id'];
                $sync = $applicationID.'-'.$loanItem;
                $checkSQL = "SELECT * FROM disbursement_suspension WHERE sync_id = '$sync';";
                $checkModel = Yii::$app->db->createCommand($checkSQL)->queryAll();






                /****************** Start Documents & History ********************/


                if (sizeof($checkModel)!=0){

                    if (!empty($checkModel[0]['supporting_document'])&& $checkModel[0]['supporting_document']!=''){
                        $oldAttachments = (ARRAY)json_decode($checkModel[0]['supporting_document']);
                        $attIndex= sizeof($oldAttachments);
                        $oldAttachments[$attIndex]=$doc;
                        $document=$oldAttachments;

                    }else{

                        $document[]=array($doc);
                    }



                }else{
                    $document[]=array($doc);
                }



                $currentHistory = array(
                    'suspension_percent'=>$S_percent,
                    'remaining_percent'=>$R_percent,
                    'status'=>Module::SuspensionType(1),
                    'status_reason'=>$reason,
                    'remarks'=>$remarks,
                    'status_by'=>$user,
                    'status_date'=>$now,
                    'supporting_document'=>$doc,
                );

                if (sizeof($checkModel)!=0){


                    if (!empty($checkModel[0]['suspension_history'])&& $checkModel[0]['suspension_history']!=''){
                        $oldHistory = (ARRAY)json_decode($checkModel[0]['suspension_history']);
                        $histIndex= sizeof($oldHistory);
                        $oldHistory[$histIndex]=$currentHistory;
                        $history=$oldHistory;
                    }else{

                        $history[]=$currentHistory;
                    }



                }else{
                    $history[]=$currentHistory;
                }

                /****************** End Documents & History ********************/

                $DCMNT = json_encode($document);
                $HSTRY = json_encode($history);

                if (sizeof($checkModel)!=0){
                    $counter++;
                    /*   $updateSQL.='
                       UPDATE disbursement_suspension SET
                       suspension_percent = "'.$S_percent.'",
                       remaining_percent = "'.$R_percent.'",
                       status = "1",
                       status_reason = "'.$reason.'",
                       remarks = "'.$remarks.'",
                       status_by = "'.$user.'",
                       status_date = "'.$now.'",
                       mode = "'.$mode.'",
                       supporting_document = \''.$DCMNT.'\',
                       suspension_history = \''.$HSTRY.'\'
                       WHERE sync_id = "'.$sync.'";
                       ';   */

                    $updateSQL.='
                    UPDATE disbursement_suspension SET 
                    suspension_percent = "'.$S_percent.'",
                    remaining_percent = "'.$R_percent.'",
                    status = "1",
                    status_reason = "'.$reason.'",
                    remarks = "'.$remarks.'",
                    status_by = "'.$user.'",
                    status_date = "'.$now.'",
                    mode = "'.$mode.'"
                 
                    WHERE sync_id = "'.$sync.'";
                    ';
                }else{
                    $counter++;
                    /* $insertSQL.='INSERT IGNORE INTO disbursement_suspension
                                 (application_id, loan_item_id, suspension_percent, remaining_percent, status, status_reason, remarks, status_date, status_by, supporting_document, suspension_history, sync_id, mode)
                                 VALUES
                                 ("'.$applicationID.'","'.$loanItem.'","'.$S_percent.'","'.$R_percent.'","1","'.$reason.'","'.$remarks.'","'.$now.'","'.$user.'",\''.$DCMNT.'\',\''.$HSTRY.'\',"'.$sync.'","'.$mode.'");';
                */
                    $insertSQL.='INSERT IGNORE INTO disbursement_suspension 
                                (application_id, loan_item_id, suspension_percent, remaining_percent, status, status_reason, remarks, status_date, status_by, sync_id, mode) 
                                VALUES 
                                ("'.$applicationID.'","'.$loanItem.'","'.$S_percent.'","'.$R_percent.'","1","'.$reason.'","'.$remarks.'","'.$now.'","'.$user.'","'.$sync.'","'.$mode.'");';
                }

            }

            //Execute Individual items Suspension [affects Disbursement only]
            if (!empty($insertSQL)&&$insertSQL!=""){ Yii::$app->db->createCommand($insertSQL)->execute();  }
            if (!empty($updateSQL)&&$updateSQL!=""){ Yii::$app->db->createCommand($updateSQL)->execute();  }

            /** Applicant Full Suspension [affects both Allocation & Disbursement ] **/
            $SQL = "
                UPDATE application as t1
                INNER JOIN ($aplSQL) as t2
                SET t1.student_status = 'STOPED'
                WHERE t1.application_id  = t2.application_id";
            Yii::$app->db->createCommand($SQL)->execute();

            if ($counter!=0){
                $output = true;
            }
        }




        return $output;
    }

    public static function AutoUpliftAll($applicationID){
        $output = false;

        /** Applicant Full Uplift [affects both Allocation & Disbursement ] **/
        $SQL = "UPDATE application SET student_status = 'ONSTUDY' WHERE application_id  = '$applicationID' AND student_status = 'STOPED'";
        Yii::$app->db->createCommand($SQL)->execute();

        return $output;
    }

    public static function CheckSuspension($application,$loanItem)
    {
        $output = 1; //No Suspension
        $SQL = "
                SELECT 
                suspension_percent,remaining_percent,status
                FROM disbursement_suspension
                WHERE application_id = '$application' AND loan_item_id = '$loanItem'
                ORDER BY id DESC LIMIT 1";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = (($model[0]['remaining_percent'])/100);
        }else{
            $output = 1; //No Suspension
        }

        return $output;
    }

    public static  function AdjustmentType($index=null){
        $output = '';
        $dataArray = array('DR'=>'DEBIT (DR)','CR'=>'CREDIT (CR)');
        if (isset($dataArray[$index])){
            $output = $dataArray[$index];
        }else{
            if ($index==null){
                $output = $dataArray;
            }
        }

        return $output;
    }

    public static function AdjustmentNumber(){
        $output = '';
        $fySQL = "SELECT * FROM financial_year WHERE is_active='1' ORDER  BY financial_year_id DESC  LIMIT 1;";
        $fyModel = Yii::$app->db->createCommand($fySQL)->queryAll();
        if (sizeof($fyModel)!=0){
            $fy=$fyModel[0]['financial_year'];
        }else{
            $fy = date('Y',strtotime('-1 year')).'/'.date('y');
        }
        $format = 'ADJ/'.$fy;
        $counter = 0;
        $SQL = "SELECT COUNT(id) AS 'counter' FROM disbursement_adjustment WHERE YEAR(adjustment_date) = YEAR(CURDATE())";
        $model = \Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!==0){
            $counter=$model[0]['counter'];
        }
        //$counter=;
        $counter++;

        $output = $format.'/'.self::NumberFiller($counter,5,'0');
        return $output;
    }

    public static function NumberFiller($number,$size,$repeater)
    {
        $output = '';
        //$repeater = '0';

        if ($size>strlen($number)){
            $limit = $size - strlen($number);
            for ($i=1; $i<=$limit; $i++){
                $output.=$repeater;
            }
        }

        return $output.$number;
    }

    public static  function ApplicantDisbursementItems($application,$semester,$instalment){

        $SQL="
               SELECT 
                disbursement.loan_item_id AS 'id',
                #CONCAT(loan_item.item_name,' (',loan_item.item_code,') ',FORMAT((IFNULL(SUM(disbursement.disbursed_amount),0)),2)) AS 'name'
                CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'name'
                
                FROM disbursement
                LEFT JOIN application ON disbursement.application_id = application.application_id
                LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                LEFT JOIN user ON applicant.user_id = user.user_id
                LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
                LEFT JOIN programme ON application.programme_id = programme.programme_id
                LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id 
                LEFT JOIN academic_year ON disbursement_batch.academic_year_id = academic_year.academic_year_id
                
                WHERE disbursement.application_id ='$application' AND disbursement.status = '0' /*IN (0,1,2)*/ AND disbursement_batch.semester_number = '$semester' AND disbursement_batch.instalment_definition_id = '$instalment'
                GROUP BY disbursement.loan_item_id
                ORDER BY applicant.f4indexno ASC,academic_year.academic_year_id ASC,loan_item.item_name ASC";

        return Yii::$app->db->createCommand($SQL)->queryAll();
    }

    public static  function ApplicantDisbursementItem($application,$semester,$instalment,$item){

        $SQL="
               SELECT 
                disbursement.disbursement_id AS 'id',
                disbursement.disbursed_amount AS 'name'
                
                FROM disbursement
                LEFT JOIN application ON disbursement.application_id = application.application_id
                LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                LEFT JOIN user ON applicant.user_id = user.user_id
                LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
                LEFT JOIN programme ON application.programme_id = programme.programme_id
                LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id 
                LEFT JOIN academic_year ON disbursement_batch.academic_year_id = academic_year.academic_year_id
                WHERE disbursement.application_id ='$application' AND disbursement.status = '0' /* IN (1,2)*/ AND disbursement_batch.semester_number = '$semester' AND disbursement_batch.instalment_definition_id = '$instalment'AND disbursement.loan_item_id = '$item'
                GROUP BY disbursement.disbursement_id
                ORDER BY applicant.f4indexno ASC,academic_year.academic_year_id ASC,loan_item.item_name ASC";

        return Yii::$app->db->createCommand($SQL)->queryAll();
    }

    public static function Applicant($applicationID){
        $output = '';
        $SQL = "
        SELECT 
        applicant.f4indexno AS 'index_number',
        user.phone_number AS 'phone_number',
        user.email_address AS 'email_address',
        application.registration_number AS 'registration_number',
        CONCAT(user.firstname,' ',user.surname) AS 'full_name',
        CASE WHEN user.sex = 'M' THEN 'MALE' ELSE 'FEMALE' END AS 'Sex'
        FROM application
        LEFT  JOIN applicant ON application.applicant_id = applicant.applicant_id
        LEFT  JOIN user ON applicant.user_id = user.user_id
        WHERE application.application_id = '$applicationID';
        ";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output=$model[0]['index_number'].' | '.$model[0]['full_name'];
        }

        return $output;
    }

    public static function AdjustmentEffect($type,$disbursementID,$amount)
    {
        $output = false;
        if ($type=='CR'){ $amount*=-1; }
        $SQL="UPDATE disbursement SET disbursed_amount = (disbursed_amount + $amount) WHERE disbursement_id = '$disbursementID'";
        $output = Yii::$app->db->createCommand($SQL)->execute();

        return $output;
    }

    public static function PayListStatus($index=null){

        $output = array();
        $dataArray = array(0=>'NEW', 1=>'SUBMITTED',2=>'REVIEWED', 3=>'APPROVED', 4=>'DENIED',5=>'REJECTED',6=>'ACCEPTED', 7=>'DISBURSED', 8=> 'RECEIVED', 9=>'WAITING_RETURN' , 10=>'RETURNED');

        if ($index!==null){
            if (isset($dataArray[$index])){
                $output = $dataArray[$index];
            }else{
                $output = '';
            }
        }else{
            $output = $dataArray;
        }

        return $output;

    }

    public static function UpdatePaylistStatus($requestID,$status,$application=null){
        $WHERE = '';
        if ($application!==null && $application!=null && !empty($application)){
            $WHERE = " application_id = '$application' AND ";
        }
        $SQL = "UPDATE institution_paylist SET status = '$status' WHERE $WHERE request_id = '$requestID';";
        Yii::$app->db->createCommand($SQL)->execute();
        return true;
    }

    public static function UpdateRequestStatus($requestID,$status){
        $SQL = "UPDATE institution_fund_request SET request_status = '$status' WHERE id = '$requestID';";
        Yii::$app->db->createCommand($SQL)->execute();

        self::UpdatePaylistStatus($requestID,$status);
        return true;
    }

    public static function RequestAmount($requestID){
        $output = 0;
        $SQL = "SELECT SUM(allocated_amount) AS 'amount' FROM institution_paylist WHERE request_id = '$requestID';";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0]['amount'];
        }

        return $output;
    }

    public static function FundRequestEvidence($id){
        $output = '';
        $SQL = "SELECT * FROM institution_fund_request WHERE  id = '$id';";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $documentList = '';
            foreach ($model as $index=>$dataArray) {
                $documentList = $dataArray['official_document'];
            }

            if ($documentList!=''){
                $documentArray=(ARRAY)json_decode($documentList);
                foreach ($documentArray AS $di=>$documentName){
                    if (!empty($documentName)&&$documentName!=false){


                        $link = '../attachments/disbursement/'.$documentName;
                        $output.='
                    <p>
                   
                    
                            <embed src="'.$link.'#page=1&zoom=100" width="100%" height="1500px">
                    </p>
                    ';
                    }
                }
            }
        }
        return $output;
    }

    public static function CheckExistence($indexNo,$institution,$registrationNumber=null,$academicYear)
    {
        $output = array('status'=>0,'tracing'=>array());
        $recordArray=array('index_number'=>$indexNo,'institution_id'=>$institution,'registration_number'=>$registrationNumber);
        $tracing = self::TraceDetails($recordArray);

        $output['tracing'] = $tracing;

        $SQL = "
        SELECT 
            applicant.f4indexno AS 'index_number'        
            FROM allocation
            LEFT JOIN application ON allocation.application_id = application.application_id
            LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
            LEFT JOIN programme ON application.programme_id = programme.programme_id
            LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
            LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
            WHERE applicant.f4indexno = '$indexNo' /*AND application.registration_number='$registrationNumber'*/ AND  allocation_batch.academic_year_id = '$academicYear'  AND programme.learning_institution_id = '$institution'
            
        ";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){

            $output['status'] = 1;
        }else{
            $output['status'] = 0;
        }

        return $output;
    }

    public static function TraceDetails($recordArray)
    {
        $output = array();
        $originalIndexNo = $recordArray['index_number'];
        $originalInstitution = $recordArray['institution_id'];
        $originalRegNo = $recordArray['registration_number'];

        $indexStatus = false;
        $instStatus = false;
        $regStatus = false;

        $SQL1="SELECT * FROM applicant WHERE f4indexno='$originalIndexNo'";
        $SQL2="SELECT * FROM applicant WHERE f6indexno='$originalIndexNo'";
        $results1 = Yii::$app->db->createCommand($SQL1)->queryAll();
        if (sizeof($results1)!=0){
            $output['INDEX_NUMBER']='VALID FORM IV INDEX NUMBER';
            $indexStatus = true;
        }else{
            $indexStatus = false;
            $results2 = Yii::$app->db->createCommand($SQL2)->queryAll();
            if(sizeof($results2)!=0){
                $output['INDEX_NUMBER']='INVALID FORM VI INDEX NUMBER';
                $indexStatus = true;
            }else{
                $output['INDEX_NUMBER']='INVALID INDEX NUMBER';
                $indexStatus = false;
            }
        }


        if ($indexStatus){
            $SQL3="
                SELECT 
                application.registration_number AS 'registration_number',
                applicant.f4indexno AS 'f4indexno', 
                applicant.f6indexno AS 'f6indexno',
                programme.learning_institution_id AS 'institution'
                 FROM application 
                LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                LEFT JOIN programme  ON application.programme_id = programme.programme_id
                WHERE application.registration_number = '$originalRegNo' LIMIT 1";
            $results3 = Yii::$app->db->createCommand($SQL3)->queryAll();
            if (sizeof($results3)!=0)
            {
                $regStatus = true;
                if ($results3[0]['f4indexno']==$originalIndexNo || $results3[0]['f6indexno']==$originalIndexNo){

                }else{
                    $regStatus = false;
                    $output['REGISTRATION_NUMBER']='REGISTRATION NUMBER BELONGS TO '.$results3[0]['f4indexno'];
                }




            }else{
                $regStatus = false;
                $output['REGISTRATION_NUMBER']='INVALID REGISTRATION NUMBER';
            }


            $SQL4="
                SELECT 
                application.registration_number AS 'registration_number',
                applicant.f4indexno AS 'f4indexno', 
                applicant.f6indexno AS 'f6indexno',
                programme.learning_institution_id AS 'institution',
                learning_institution.institution_name AS 'institution_name'
                 FROM application 
                LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                LEFT JOIN programme  ON application.programme_id = programme.programme_id
                LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                WHERE applicant.f4indexno='$originalIndexNo' OR  applicant.f6indexno='$originalIndexNo' LIMIT 1";
            $results4 = Yii::$app->db->createCommand($SQL4)->queryAll();

            if ($results4[0]['institution']==$originalInstitution){
                $output['INSTITUTION']='APPLICANT ADMITTED AT '.$results4[0]['institution_name'];
            }else{
                $instStatus = false;

                if ($results4[0]['institution_name']!==''){
                    $output['INSTITUTION']='APPLICANT ADMITTED AT '.$results4[0]['institution_name'];
                }else{
                    $output['INSTITUTION']='APPLICANT NOT ADMITTED AT ANY INSTITUTION';
                }

            }

        }

        $score =0;
        if ($indexStatus){$score++;}
        if ($regStatus){$score++;}
        if ($instStatus){$score++;}

        if ($score<3){ $output["GENERAL"]='FAILED';}else{$output["GENERAL"]='PASSED';}


        ksort($output);

        return $output;
    }

    public static function CurrentAcademicYear($field=null)
    {
        $output = '';
        $SQL = "SELECT * FROM academic_year WHERE is_current='1' ORDER BY academic_year_id DESC LIMIT 1";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            if (isset($model[0][$field])){
                $output = $model[0][$field];
            }else{
                $output = $model[0]['academic_year_id'];
            }

        }

        return $output;

    }

    public static function CurrentFinancialYear($field=null)
    {
        $output = '';
        $SQL = "SELECT * FROM financial_year WHERE is_active = '1' ORDER BY financial_year_id DESC LIMIT 1;";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            if (isset($model[0][$field])){
                $output = $model[0][$field];
            }else{
                $output = $model[0]['financial_year_id'];
            }
        }
        return $output;
    }

    public static function CurrentInstitution($field=null)
    {
        $output = '';
        $SQL = "SELECT * FROM learning_institution WHERE is_active = '1' ORDER BY learning_institution_id DESC LIMIT 1;";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            if (isset($model[0][$field])){
                $output = $model[0][$field];
            }else{
                $output = $model[0]['learning_institution_id'];
            }
        }
        return $output;
    }

    public static function InstitutionDecoder($code,$field=null)
    {
        $output = '';
        $SQL = "SELECT * FROM learning_institution WHERE UPPER(institution_code) = '".strtoupper($code)."'";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            if (isset($model[0][$field])){
                $output = $model[0][$field];
            }else{
                $output = $model[0]['learning_institution_id'];
            }
        }

        return $output;
    }

    public static  function DisburseLoan($postArray,$model)
    {
        $output = false;

        $academicYear=$postArray['academic_year_id'];
        $institution=$postArray['learning_institution_id'];
        $semester=$postArray['semester_number'];
        $installment=$postArray['instalment_definition_id'];
        $loan_item=$postArray['loan_item_id'];
        $study_level=$model->level;
        $version = $model->version == "" ? 0 : $model->version;

        $batchId = $model->disbursement_batch_id;
        /** Start Cleaning Disbursement **/
        $cleanUpSQL = "DELETE FROM disbursement WHERE disbursement_batch_id = '$batchId';";
        Yii::$app->db->createCommand($cleanUpSQL)->execute();
        /** End Clean Disbursement **/

        $results = self::PlanImplementer($academicYear,$institution,$semester,$installment,$loan_item,$study_level,$version);

        if($model->institution_payment_request_id !='' && !empty($model->institution_payment_request_id))
        {
            /** Request Status**/
            self::UpdateRequestStatus($model->institution_payment_request_id,'7');

            //$dataArray = array(0=>'NEW', 1=>'SUBMITTED',2=>'REVIEWED', 3=>'APPROVED', 4=>'DENIED',5=>'REJECTED',6=>'ACCEPTED', 7=>'DISBURSED', 8=> 'RECEIVED', 9=>'WAITING_RETURN' , 10=>'RETURNED');
            //0=>NEW, 1=>SUBMITTED, 3=>APPROVED, 4=>DENIED, 5=>PARTIAL DISBURSED, 6=>FULLY DISBURSED

        }



        if (sizeof($results)!=0){
            $SQL="";
            $records = 0;
            $fields = "allocation_batch_id, disbursement_batch_id, application_id, programme_id, loan_item_id, year_of_study, version, disbursed_amount, disbursed_as, status, created_at, created_by, allocated_amount, remaining_balance, sync_id";
            foreach ($results as $index=>$dataArray){

                $application = $dataArray['application'];
                $programme = $dataArray['programme_id'];
                $loanItem = $dataArray['item_id'];

                $disbursementAmount = $dataArray['disbursed_amount'];
                $disbursedAs = $model->disbursed_as;
                $status = 0; //0=>'NEW', 1=>'SUBMITTED',2=>'REVIEWED', 3=>'APPROVED', 4=>'REJECTED',6=>'ACCEPTED', 7=>'DISBURSED', 8=> 'RECEIVED', 9=>'WAITING_RETURN' , 10=>'RETURNED'
                $creation = date('Y-m-d H:i:s');
                $creator = \yii::$app->user->id;
                $allocatedAmount = $dataArray['allocated_amount'];
                $allocationBatch = $dataArray['allocation_batch_id'];
                $YOS = $dataArray['year_of_study'];


                $remainingBalance = $allocatedAmount-$disbursementAmount;
                $syncID = $allocationBatch.'-'.$application.'-'.$loanItem.'-'.$model->semester_number.'-'.$model->instalment_definition_id.'-'.$version;

                $values = "'$allocationBatch', '$batchId', '$application', '$programme', '$loanItem', '$YOS', '$version', '$disbursementAmount', '$disbursedAs', '$status', '$creation', '$creator', '$allocatedAmount', '$remainingBalance', '$syncID'";
                $SQL.="INSERT IGNORE INTO disbursement ($fields) VALUES ($values);";
                //$SQL.="INSERT INTO disbursement ($fields) VALUES ($values);";
                $records++;
            }

            Yii::$app->db->createCommand($SQL)->execute();

            Yii::$app->getSession()->setFlash(
                'success', number_format($records).' Records have been Successfully Created!'
            );
            $output = true;
        }

        return $output;

    }


    public static function MisDisbursementCreator($batchID)
    {
        //Clean All Mis-disbursement for the current batch
        Yii::$app->db->createCommand("DELETE FROM mis_disbursed WHERE disbursement_batch_id = '$batchID';")->execute();

        $output = '';
        $batchModel = DisbursementBatch::findOne($batchID);
        $allocationBatch=$institution = $loan_item=$academic_year=$installment=$semester=$programme_category='';

        if (sizeof($batchModel)!=0)
        {
            $loan_item=$batchModel->loan_item_id;
            $academic_year=$batchModel->academic_year_id;
            $installment=$batchModel->instalment_definition_id;
            $semester=$batchModel->semester_number;
            $level=$batchModel->level;
            $institution=$batchModel->learning_institution_id;
            $allocationBatch=$batchModel->allocation_batch_id;
        }

        $SQL = "
            SELECT 
            DISTINCT allocation.application_id AS 'application_id'
            FROM allocation 
             RIGHT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id AND allocation_batch.is_approved = 1
            LEFT JOIN application ON allocation.application_id = application.application_id
            LEFT JOIN programme ON  application.programme_id =  programme.programme_id
            LEFT JOIN loan_item ON  allocation.loan_item_id =  loan_item.loan_item_id
            LEFT JOIN applicant ON  application.applicant_id =  applicant.applicant_id
            LEFT JOIN user ON  applicant.applicant_id =  user.user_id
            WHERE  (allocation.is_canceled = '0' AND allocation_batch.academic_year_id='$academic_year'  AND
            allocation.loan_item_id IN( SELECT '$loan_item' UNION SELECT disbursement_setting2.associated_loan_item_id FROM disbursement_setting2 WHERE disbursement_setting2.academic_year_id='$academic_year' AND disbursement_setting2.instalment_definition_id = '$installment' AND disbursement_setting2.loan_item_id='$loan_item')
            AND programme.learning_institution_id = '$institution' AND programme.study_level = '$level')
            AND  allocation.application_id NOT IN(SELECT DISTINCT disbursement.application_id FROM disbursement WHERE disbursement.disbursement_batch_id = '$batchID')
            ORDER BY programme.programme_code ASC ,application.current_study_year ASC ;        
        ";

        /*
                echo '<pre>'.$SQL.'</pre>';
                echo '<pre>'.$allocationBatch.'</pre>';
                exit();*/
        $model = Yii::$app->db->createCommand($SQL)->queryAll();


        $counter =0;
        if (sizeof($model)!=0){

            $SQLInsert = $reasons = "";
            $now = date('Y-m-d H:i:s');
            $user = Yii::$app->user->id;
            foreach ($model as $index=>$dataArray)
            {
                $counter++;
                $application_id = $dataArray['application_id'];
                $reasonsArray = array();
                $reasons = "";

                /** START CHECKING SUSPENSION ***/
                $factor = self::CheckSuspension($application_id,$loan_item);
                $APPLICATION = Yii::$app->db->createCommand("SELECT * FROM application WHERE application_id='$application_id'")->queryAll();
                if (sizeof($APPLICATION)!=0 && $APPLICATION[0]['student_status']=="STOPED"){
                    $reasonsArray[] = 'SUSPENDED';
                }
                if ($factor==0){
                    $suspension = DisbursementSuspension::findAll(['application_id'=>$application_id,'loan_item_id'=>$loan_item,'remaining_percent'=>'0']);
                    foreach ($suspension as $sus){
                        $reasonsArray[]=$sus->Reason;
                    }

                }
                /** END CHECKING SUSPENSION ***/


                /** START REGISTRATION NUMBER & ACCOUNT NUMBER ***/
                if (sizeof($APPLICATION)!=0)
                {
                    if (empty($APPLICATION[0]['bank_account_number']) || $APPLICATION[0]['bank_account_number']=='' || $APPLICATION[0]['bank_account_number']==null){ $reasonsArray[]=' HAS NO ACCOUNT NUMBER';}
                    if (empty($APPLICATION[0]['registration_number']) || $APPLICATION[0]['registration_number']=='' || $APPLICATION[0]['registration_number']==null){ $reasonsArray[]=' HAS NO REGISTRATION NUMBER';}
                }

                /** END REGISTRATION NUMBER & ACCOUNT NUMBER ***/


                $reasons = implode(' , ',$reasonsArray);
                $SQLInsert.= "
                    INSERT IGNORE INTO mis_disbursed (application_id, allocation_batch_id, disbursement_batch_id, loan_item_id, reasons, date_created, created_by) VALUES 
                    ('$application_id','$allocationBatch','$batchID','$loan_item','$reasons','$now','$user');";
            }

            if (!empty($SQLInsert)){

                Yii::$app->db->createCommand($SQLInsert)->execute();
                $output=$counter;
            }
        }



        return $output;
    }

    public static  function DisburseGrant($postArray,$model){
        $output = '';

        return $output;

    }

    public static function BatchApproval($batchID)
    {
        $output = array();

        /* $SQL = "
          SELECT
                 disbursement_task_assignment.disbursement_task_assignment_id,

                 disbursement_task_assignment.disbursement_schedule_id AS 'schedule_id',
                 disbursement_schedule.from_amount AS 'min_amount',
                 disbursement_schedule.to_amount AS 'max_amount',
                 disbursement_schedule.operator_name AS 'operator',
                 disbursement_task_assignment.disbursement_structure_id AS 'structure_id',
                 disbursement_structure.structure_name AS 'structure_name',
                 disbursement_structure.order_level AS 'order_level',
                 disbursement_task_assignment.disbursement_task_id AS 'task_id',
                  disbursement_task_definition.task_name AS 'task_name'

                 FROM disbursement_task_assignment
                 LEFT JOIN disbursement_task_definition ON disbursement_task_assignment.disbursement_task_id = disbursement_task_definition.disbursement_task_id
                 LEFT JOIN disbursement_structure ON disbursement_task_assignment.disbursement_structure_id = disbursement_structure.disbursement_structure_id AND disbursement_structure.status='1'
                 LEFT JOIN disbursement_schedule ON disbursement_task_assignment.disbursement_schedule_id = disbursement_schedule.disbursement_schedule_id
                 LEFT JOIN disbursement_user_structure ON disbursement_structure.disbursement_structure_id = disbursement_user_structure.disbursement_structure_id AND  disbursement_user_structure.status='1'

                 RIGHT JOIN disbursement_payoutlist_movement ON disbursement_payoutlist_movement.disbursement_task_id = disbursement_task_definition.disbursement_task_id AND  disbursement_payoutlist_movement.disbursements_batch_id='$batchID'

                 WHERE disbursement_payoutlist_movement.disbursements_batch_id='$batchID' AND disbursement_payoutlist_movement.movement_status='0'

                 ORDER BY disbursement_structure.order_level ASC
         ";

         $model = Yii::$app->db->createCommand($SQL)->queryAll();*/

        $model = self::MovementQuery($batchID,'current');


        /*$ckeckSQL = "SELECT movement_id, movement_status,from_officer, to_officer , (CASE WHEN to_officer='$user' THEN 'OK' ELSE 'WAIT' END) AS 'check' FROM disbursement_payoutlist_movement WHERE disbursement_payoutlist_movement.disbursements_batch_id='$batchID' AND  movement_status = '0' ORDER BY movement_id ASC LIMIT 1;";
        $prevModel = Yii::$app->db->createCommand($ckeckSQL)->queryAll();*/
        if (sizeof($model)!=0 /*&& sizeof($prevModel)!==0 &&($prevModel[0]['check']=='OK')*/){
            $output = $model;
        }

        return $output;
    }

    public static function BatchPriority($batchID,$option=null){
        //$output = 'N/A';
        $output = '-';
        $results = '';
        $priorities = self::PriorityList();
        $batch = DisbursementBatch::findOne($batchID);
        if (sizeof($batch)!=0){

            if ($batch->is_approved == 0){
                $max_days = $batch->process_days;
                $date_created = $batch->created_at;
                $elapsed_days = self::span($date_created,date('Y-m-d H:i:s'),'a');
                $percentage = 0;

                if ($max_days!=0){
                    $percentage = (100 - round(((($elapsed_days)/($max_days))*100),0));
                }

                /** Manual Priority Definition **/
                /**
                 * LOW 100 - 75 ******* 1
                 *
                 * MEDIUM 74 - 50 ***** 2
                 *
                 * HIGH 49 - 25 ******* 3
                 *
                 *
                 * URGENT >25 ********* 4
                 **/

                if ($percentage>= 75 && $percentage<=100){
                    $results = $priorities[1];
                }elseif ($percentage< 75 && $percentage>=50){
                    $results = $priorities[2];
                }elseif ($percentage<50 && $percentage<=25){
                    $results = $priorities[3];
                }elseif ($percentage>25){
                    $results = $priorities[4];
                }
            }else{
                $results = '-';
            }







        }

        if (is_array($results)&&isset($results[$option])){
            $output = $results[$option];
        }else{
            $output = $results;
        }

        return $output;
    }

    public static function TotalDisbursement($application=null,$academicYear=null,$option =null,$batchID=null)
    {
        $output = 0;

        $WHERE = "";
        if ($application!==null&&!empty($application)){
            $WHERE.= " AND disbursement.application_id = '$application'";
        }

        if ($academicYear!==null&&!empty($academicYear)){
            $WHERE.= " AND disbursement_batch.academic_year_id = '$academicYear' ";
        }

        if ($batchID!==null&&!empty($batchID)){
            $WHERE.= " AND disbursement.disbursement_batch_id = '$batchID' ";
        }



        $SQL = "
                    SELECT 
                    COUNT(DISTINCT disbursement.application_id) AS 'count',
                    SUM(disbursement.disbursed_amount) AS 'amount'
                     FROM disbursement
                     LEFT JOIN application ON disbursement.application_id = application.application_id
                     LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
                     WHERE  disbursement.status IN(1,7,8) $WHERE;
                    ";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            if (strtoupper($option)=='ALL'){
                $output = $model;
            }else{
                $output = $model[0]['amount'];
            }

        }

        return $output;
    }

    public static function TotalItemDisbursement($application,$loanItem)
    {
        $output = 0;
        $SQL = "SELECT SUM(disbursed_amount) AS 'total' FROM disbursement WHERE application_id = '$application' AND status IN(1,7,8) AND loan_item_id = '$loanItem';";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0]['total'];
        }

        return $output;
    }

    public static function TotalAllocation($academicYear=null,$application=null,$option){
        $output = 0;
        $WHERE = "";
        if ($academicYear!==null){
            $WHERE.=" AND allocation_batch.academic_year_id = '$academicYear' ";
        }

        if ($application!==null){
            $WHERE.=" AND allocation.application_id = '$application' ";
        }

        $SQL = "
        SELECT 
        COUNT(DISTINCT allocation.application_id) AS 'count',
      SUM(allocation.allocated_amount) AS 'amount'
      FROM allocation 
      RIGHT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id AND allocation_batch.is_approved = '1'
	  LEFT JOIN application ON allocation.application_id = application.application_id
      LEFT JOIN programme ON  application.programme_id =  programme.programme_id
      LEFT JOIN loan_item ON  allocation.loan_item_id =  loan_item.loan_item_id
      WHERE  allocation.is_canceled = '0' AND allocation_batch.status IN(1,10) $WHERE AND (allocation_batch.larc_approve_status = '1' OR allocation_batch.partial_approve_status='1') AND allocation_batch.disburse_status='1';
      ";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0][$option];
        }

        return $output;
    }

    public static function TotalItemAllocation($application,$loanItem)
    {
        $output = 0;
        $SQL = "
      SELECT 
      SUM(allocation.allocated_amount) AS 'total'
      FROM allocation 
      RIGHT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id AND allocation_batch.is_approved = '1'
	  LEFT JOIN application ON allocation.application_id = application.application_id
      LEFT JOIN programme ON  application.programme_id =  programme.programme_id
      LEFT JOIN loan_item ON  allocation.loan_item_id =  loan_item.loan_item_id
      WHERE allocation.application_id = '$application' AND allocation.loan_item_id = '$loanItem' AND  allocation.is_canceled = '0' AND allocation_batch.is_approved = '1';"; $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0]['total'];
        }

        return $output;
    }

    public static function DashboardStatistics($academicYear,$option=null)
    {
        $output = array();
        $SQL = "";

        switch ($option){

            case 'disbursement':
                $output = self::TotalDisbursement(null,$academicYear,'all');
                break;

            case 'request':
                $output = self::InstitutionRequests($academicYear);
                break;

            case 'itemsTable':
                $output = Module::ItemsTable($academicYear);
                break;


            case 'itemsMatrixTable':
                $output = Module::ItemsMatrixTable($academicYear);
                break;

            case 'returns':
                $output = self::DisbursementReturns($academicYear);
                break;

            case 'areaChartData':
                $output = self::areaChartData($academicYear);
                break;

            case 'allocation':
                $dataArray = array('count'=>self::TotalAllocation($academicYear,null,'count'),'amount'=>self::TotalAllocation($academicYear,null,'amount'));
                $output[] = $dataArray;
                break;
        }

        if (!empty($SQL)&&$SQL!=''){
            $output = Yii::$app->db->createCommand($SQL)->queryAll();
        }

        return $output;

    }

    public static function InstitutionRequests($academicYear){
        $SQL= "
              SELECT 
              COUNT(DISTINCT institution_paylist.request_id) AS 'count',
              IFNULL(SUM(institution_paylist.allocated_amount),0) AS 'amount'
              FROM institution_paylist
              LEFT JOIN disbursement_batch ON institution_paylist.disbursement_batch_id = disbursement_batch.disbursement_batch_id
              WHERE institution_paylist.academic_year = '$academicYear' AND institution_paylist.status IN(3,6,7,8);
        ";
        return Yii::$app->db->createCommand($SQL)->queryAll();
    }

    public static function DisbursementReturns($academicYear){
        $SQL= "
              SELECT 
                COUNT(DISTINCT disbursement.application_id) AS 'count',
                SUM(disbursement.disbursed_amount) AS 'amount'
                 FROM disbursement
                 LEFT JOIN application ON disbursement.application_id = application.application_id
                 LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
                 WHERE disbursement_batch.financial_year_id = '$academicYear' AND disbursement.status='10';";
        return Yii::$app->db->createCommand($SQL)->queryAll();
    }

    public static function areaChartData($academicYear)
    {
        $output = array();
        $dataSets = array();

        //$CATEGORIES = ArrayHelper::map(ApplicantCategory::findBySql("SELECT applicant_category.applicant_category_id AS 'id',applicant_category.applicant_category AS 'name' FROM applicant_category ORDER BY applicant_category_id ASC ")->asArray()->all(),'id','name');
        // $labels = ArrayHelper::map(LoanItem::findBySql("SELECT loan_item.loan_item_id AS 'id',loan_item.item_code AS 'name' FROM loan_item WHERE is_active='1' ORDER BY loan_item.item_name ASC ")->asArray()->all(),'id','name');


        $SQL_Category = "SELECT applicant_category.applicant_category_id AS 'id',applicant_category.applicant_category AS 'name' FROM applicant_category ORDER BY applicant_category_id ASC";
        $CATEGORIES = Yii::$app->db->createCommand($SQL_Category)->queryAll();

        $SQL_label = "SELECT loan_item.loan_item_id AS 'id',loan_item.item_code AS 'name' FROM loan_item WHERE is_active='1' ORDER BY loan_item.item_name ASC;";
        $labels = Yii::$app->db->createCommand($SQL_label)->queryAll();



        foreach ($CATEGORIES as $indexCategory=>$DataArrayCategory){
            $categoryID = $DataArrayCategory['id'];
            $categoryName = $DataArrayCategory['name'];
            $data = array();
            foreach ($labels as $indexLabel=>$DataArrayLabel)
            {
                $itemID = $DataArrayLabel['id'];
                $data[$itemID]=self::DisbursementCategoryItem($academicYear,$categoryID,$itemID);
            }

            //$color = self::RandomRGBColor(rand(0,20));
            $color = implode(',',self::RandomRGBColor()['rgb']);
            $hex = self::RandomRGBColor()['hex'];
            $dataSets[]=array(
                'label'=>$categoryName,
                'fillColor'=>"rgba($color)",
                //'fillColor'=>"rgba(210, 214, 222, 1)",
                'strokeColor'=>"rgba($color)",
                'pointColor'=>"rgba($color))",
                //'pointStrokeColor'=>"#c1c7d1",
                'pointStrokeColor'=>"#$hex",
                'pointHighlightFill'=>"#$hex",
                'pointHighlightStroke'=>"rgba($color)",
                'data'=>$data
            );
        }

        $labelsArray = array();
        foreach ($labels as $indx=>$dA){
            $labelsArray[$dA['id']]=$dA['name'];
        }


        $output=array('labels'=>$labelsArray,'datasets'=>$dataSets);
        return $output;
    }

    public static function DisbursementCategoryItem($academicYear,$category=null,$item){
        $output = 0;

        $WHERE = '';
        if ($category!==null&&!empty($category)){
            $WHERE = " AND application.applicant_category_id = '$category' ";
        }

        $SQL = "
            SELECT  IFNULL(SUM(disbursed_amount),0) AS 'total' 
            FROM disbursement 
            LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
            LEFT JOIN application ON disbursement.application_id = application.application_id
            WHERE 
            disbursement_batch.academic_year_id = '$academicYear' $WHERE AND disbursement.status IN(1,7,8) AND disbursement.loan_item_id = '$item';";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0]['total'];
        }

        return $output;
    }

    public static function AllocationCategoryItem($academicYear,$category=null,$item)
    {
        $output = 0;
        $WHERE = '';
        if ($category!==null&&!empty($category)){
            $WHERE = " AND application.applicant_category_id = '$category' ";
        }
        $SQL = "
      SELECT 
      SUM(allocation.allocated_amount) AS 'total'
      FROM allocation 
      RIGHT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id AND allocation_batch.is_approved = '1'
	  LEFT JOIN application ON allocation.application_id = application.application_id
      LEFT JOIN programme ON  application.programme_id =  programme.programme_id
      LEFT JOIN loan_item ON  allocation.loan_item_id =  loan_item.loan_item_id
      WHERE allocation_batch.academic_year_id = '$academicYear' $WHERE  AND allocation.loan_item_id = '$item' AND  allocation.is_canceled = '0' AND allocation_batch.is_approved = '1';";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0]['total'];
        }

        return $output;

    }

    public static  function RandomRGBColor() {
        $result = array('rgb' => '', 'hex' => '');
        foreach(array('r', 'b', 'g') as $col){
            $rand = mt_rand(0, 255);
            $result['rgb'][$col] = $rand;
            $dechex = dechex($rand);
            if(strlen($dechex) < 2){
                $dechex = '0' . $dechex;
            }
            $result['hex'] .= $dechex;
        }
        return $result;
    }

    public static function MatrixTable(){
        $output = $tr = $td = $table = $th = $head = $footer = $body ='';

        $table = '';

        return $output;
    }

    public static function ItemsTable($academicYear){

        $output = $tr = $td = $table = $th = $head = $footer = $body ='';
        $LImodel=LoanItem::findAll(['is_active'=>'1']);

        $table.= '<table class="table table-condensed table-bordered">';
        $head.='<thead>';
        $th.='
                <tr class="bg-orange-active">
                    <th class="text-uppercase text-bold text-center">Loan Items</th>
                    <th class="text-uppercase text-bold text-center" >ALLOCATION</th>
                    <th class="text-uppercase text-bold text-center">DISBURSEMENT</th>
                    <th class="text-uppercase text-bold text-center">%</th>
                </tr>
        ';
        $head.=$th;
        $head.='</thead>';

        $body.='<tbody>';
        $disbursementTOTAL = $allocationTOTAL = 0;

        foreach ($LImodel as $bodyItems){
            $tr.='<tr>';
            $tr.='<th class="bg-gray-active text-uppercase text-bold"> '.$bodyItems->item_name.'</th>';

            $disbursement = $allocation = 0;
            $disbursement = self::DisbursementCategoryItem($academicYear,null,$bodyItems->loan_item_id);
            $allocation = self::AllocationCategoryItem($academicYear,null,$bodyItems->loan_item_id);

            $disbursementTOTAL+=$disbursement;
            $allocationTOTAL+=$allocation;

            $DAD = self::ShortNumber($disbursement,2);
            $AAD = self::ShortNumber($allocation,2);
            $percent = 0;

            if ($allocation>0){
                $percent = ($disbursement/$allocation)*100;
            }
            $class = 'bg-orange-active';
            if ($percent>100){
                $class = 'bg-red-active';
            }

            $tr.='<th class="text-uppercase text-bold text-center bg-primary" data-toggle="tooltip" data-html="true" title="<h3> '.number_format($allocation,2).'</h3>">'. $AAD.'</th>';
            $tr.='<th class="text-uppercase text-bold text-center bg-green" data-toggle="tooltip" data-html="true" title="<h3> '.number_format($disbursement,2).'</h3>">'.$DAD.'</th>';
            $tr.='<th class="text-uppercase text-bold text-right '.$class.'" data-toggle="tooltip" data-html="true" title="'.$bodyItems->item_name.' DISBURSEMENT PERCENTAGE">'.number_format($percent,2).'%</th>';

/*
            $tr.='<th class="text-uppercase text-bold text-center bg-primary" data-toggle="tooltip" data-html="true" title="">'. $AAD.'</th>';
            $tr.='<th class="text-uppercase text-bold text-center bg-green" data-toggle="tooltip" data-html="true" title="">'.$DAD.'</th>';
            $tr.='<th class="text-uppercase text-bold text-right '.$class.'" data-toggle="tooltip" data-html="true" title="">'.number_format($percent,2).'%</th>';

*/


            $tr.='</tr>';

        }

        $body.=$tr;
        $body.='</tbody>';

        $GrandPercent = 0;
        if ($allocationTOTAL>0){
            $GrandPercent = ($disbursementTOTAL/$allocationTOTAL)*100;
        }

        $footer.='<tfoot>';
        $footer.='<tr class="bg-orange-active">';
        $footer.='<th class="text-uppercase text-bold text-center">Grand Total</th>';
        $footer.='<th class="text-uppercase text-bold text-center" data-toggle="tooltip" data-html="true" title="<h3> '.number_format($allocationTOTAL,2).'</h3>">'.self::ShortNumber($allocationTOTAL,2).'</th>';
        $footer.='<th class="text-uppercase text-bold text-center" data-toggle="tooltip" data-html="true" title="<h3> '.number_format($disbursementTOTAL,2).'</h3>">'.self::ShortNumber($disbursementTOTAL,2).'</th>';
        $footer.='<th class="text-uppercase text-bold text-right" data-toggle="tooltip" data-html="true" title="<h3> '.number_format($GrandPercent,2).'%</h3>">'.number_format($GrandPercent,2).'%</th>';
        $footer.='</tr>';
        $footer.='</tfoot>';


        $table.=$head;
        $table.=$body;
        $table.=$footer;
        $table.='</table>';

        $output = $table;

        return $output;
    }

    public static function ItemsMatrixTable($academicYear){

        $output = $tr = $td = $table = $th = $head = $footer = $body ='';
        $LImodel=LoanItem::findAll(['is_active'=>'1']);
        $ACmodel=ApplicantCategory::find()->all();

        $table.= '<table class="table table-condensed table-responsive table-bordered table-striped">';
        $head.='<thead>';

        $th.='<tr class="bg-gray-active">';
        $th.='<th rowspan="2"></th>';
        foreach ($LImodel as $headerItems){
            $th.='<th colspan="2" class="text-uppercase text-bold text-center" data-toggle="tooltip" data-html="true" title="<em>Allocation & Disbursement </em> <i> for </i> <b>'.$headerItems->item_name.'</b>">'.$headerItems->item_code.'</th>';
        }
        $th.='</tr>';

        $th.='<tr>';
        foreach ($LImodel as $headerItems){
            $th.='<th class="text-uppercase text-bold text-center bg-primary"  data-toggle="tooltip" data-html="true" title="<em>Allocation</em> <i> for </i> <b>'.$headerItems->item_name.'</b>" >ALC.</th>';
            $th.='<th class="text-uppercase text-bold text-center bg-green" data-toggle="tooltip" data-html="true" title="<em>Disbursement </em> <i> for </i> <b>'.$headerItems->item_name.'</b>" >DSB.</th>';
        }
        $th.='</tr>';

        $head.=$th;
        $head.='</thead>';

        $body.='<tbody>';

        foreach ($ACmodel as $categories)
        {
            $disbursementTOTAL = $allocationTOTAL = 0;
            $tr.='<tr>';
            $tr.='<th class="bg-gray-active text-uppercase text-bold"> '.$categories->applicant_category.'<span class="fa fa-arrow-right"></span></th>';
            foreach ($LImodel as $bodyItems) {

                $disbursement = $allocation = 0;
                $disbursement = Module::DisbursementCategoryItem($academicYear, $categories->applicant_category_id, $bodyItems->loan_item_id);
                $allocation = Module::AllocationCategoryItem($academicYear, $categories->applicant_category_id, $bodyItems->loan_item_id);

                $disbursementTOTAL += $disbursement;
                $allocationTOTAL += $allocation;

                $DAD = Module::ShortNumber($disbursement, 2);
                $AAD = Module::ShortNumber($allocation, 2);

                $tr .= '<th class="text-uppercase text-bold text-center bg-primary" data-toggle="tooltip" data-html="true" title="<h3>'.number_format($allocation,2).'</h3>">'.$AAD.'</th>';
                $tr .= '<th class="text-uppercase text-bold text-center bg-green" data-toggle="tooltip" data-html="true" title="<h3>'.number_format($disbursement,2).'</h3>">'.$DAD.'</th>';
            }

            $tr.='</tr>';


        }



        $body.=$tr;
        $body.='</tbody>';

        $GrandPercent = 0;
        if ($allocationTOTAL>0){
            $GrandPercent = ($disbursementTOTAL/$allocationTOTAL)*100;
        }

        $footer.='<tfoot>';
        $footer.='<tr class="bg-orange-active">';
        $footer.='<th class="bg-gray-active text-uppercase text-bold text-right">Total</th>';
        foreach ($LImodel as $footerItems){
            $footer.='<th class="text-uppercase text-bold text-center bg-primary"  data-toggle="tooltip" data-html="true" title="<em>Total allocation</em> <i> for </i> <b>'.$footerItems->item_name.'</b>" >--</th>';
            $footer.='<th class="text-uppercase text-bold text-center bg-green" data-toggle="tooltip" data-html="true" title="<em>Total disbursement </em> <i> for </i> <b>'. $footerItems->item_name.'</b>" >--</th>';
        }




        $footer.='</tr>';
        $footer.='</tfoot>';


        $table.=$head;
        $table.=$body;
        $table.=$footer;
        $table.='</table>';

        $output = $table;

        return $output;
    }

    public static function PayoutList($option,$academicYear=null,$institution=null,$semester=null,$instalment=null,$headerID=null,$programme=null)
    {
        $output = '';
        $WHERE = " ";
        if ($academicYear!==null &&!empty($academicYear)){ $WHERE.=" AND disbursement_batch.academic_year_id = '$academicYear' ";  }
        if ($institution!==null &&!empty($institution)){ $WHERE.=" AND disbursement_batch.learning_institution_id = '$institution' ";  }
        if ($semester!==null &&!empty($semester)){ $WHERE.=" AND disbursement_batch.semester_number = '$semester' ";  }
        if ($instalment!==null &&!empty($instalment)){ $WHERE.=" AND disbursement_batch.instalment_definition_id = '$instalment' ";  }
        if ($headerID!==null &&!empty($headerID)){ $WHERE.=" AND disbursement_batch.disbursement_batch_id = '$headerID' ";  }
        if ($programme!==null &&!empty($programme)){ $WHERE.=" AND application.programme_id = '$programme' ";  }



        $SQL="
        SELECT 
            disbursement.disbursement_batch_id AS 'HEADERID',
            applicant.f4indexno AS 'INDEXNO',
            UPPER(CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,''))) AS 'NAMES',
            UPPER(user.sex) AS 'SEX',
            application.registration_number AS 'REGNO',
            application.current_study_year AS 'YOS',
            application.bank_branch_name AS 'BANK',
            application.bank_account_number AS 'ACCOUNTNO',
            SUM( IF( disbursement.loan_item_id = 1, disbursed_amount, 0) ) AS BS,
                SUM( IF( disbursement.loan_item_id = 2, disbursed_amount, 0) ) AS MA,
                SUM( IF( disbursement.loan_item_id = 3, disbursed_amount, 0) ) AS FPT,
                SUM( IF( disbursement.loan_item_id = 4, disbursed_amount, 0) ) AS TU,
                SUM( IF( disbursement.loan_item_id = 5, disbursed_amount, 0) ) AS SFR,
                SUM( IF( disbursement.loan_item_id = 6, disbursed_amount, 0) ) AS RESEARCH,
                SUM( IF( disbursement.loan_item_id = 7, disbursed_amount, 0) ) AS STIPEND,
                SUM( IF( disbursement.loan_item_id = 8, disbursed_amount, 0) ) AS F_ALLOWANCE,
                SUM( IF( disbursement.loan_item_id = 9, disbursed_amount, 0) ) AS M_FEE,
                SUM( disbursement.disbursed_amount ) AS total,
            '_____________________' AS 'SIGNATURE'
            
            FROM disbursement
            RIGHT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement.disbursement_batch_id
            INNER JOIN application ON disbursement.application_id = application.application_id
            INNER JOIN applicant ON application.applicant_id = applicant.applicant_id
            INNER JOIN user ON applicant.user_id = user.user_id
            INNER JOIN programme ON application.programme_id = programme.programme_id
            LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
            WHERE disbursement.status IN(1,7,8) $WHERE 
            GROUP BY disbursement.application_id
            ORDER BY CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,'')) ASC,  disbursement.application_id ASC
        
        ";

        switch (strtoupper($option)){
            case 'SQL':
                $output = $SQL;
                break;

            default:
                $model = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($model)!=0){
                    $output = $model;
                }else{
                    $output = array();
                }
                break;

        }


        return $output;

    }


    public static function PayListSummary($option,$batchID)
    {
        $output = '';
        $SQL = "
              SELECT 
              application.application_id AS 'application_id',
              disbursement.disbursement_batch_id AS 'disbursement_batch_id',
             
              disbursement_batch.batch_number AS 'header_id',
              applicant.f4indexno AS 'index_number',
              UPPER(CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,''))) AS 'full_name',
              UPPER(user.sex) AS 'sex',
              application.current_study_year AS 'yos',
              learning_institution.institution_name AS 'institution',
              programme.programme_code AS 'programme_code',
              SUM(disbursement.disbursed_amount) AS 'disbursed_amount'
            FROM disbursement
            RIGHT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
            INNER JOIN application ON disbursement.application_id = application.application_id
            INNER JOIN applicant ON application.applicant_id = applicant.applicant_id
            INNER JOIN user ON applicant.user_id = user.user_id
            INNER JOIN programme ON application.programme_id = programme.programme_id
            LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
            LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
			WHERE disbursement.disbursement_batch_id = '$batchID' 
            GROUP BY disbursement.application_id
            ORDER BY CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,'')) ASC,  disbursement.application_id ASC";

        switch (strtoupper($option)){
            case "SQL":
                $output = $SQL;
                break;

            case "DATAARRAY":
                $output = Yii::$app->db->createCommand($SQL)->queryAll();
                break;

            case "SUM_AMOUNT":
                $SQL = "
              SELECT 
            SUM(disbursement.disbursed_amount) AS 'total'
            FROM disbursement
            RIGHT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
        	WHERE disbursement.disbursement_batch_id = '$batchID'";
                $model = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($model)!=0){
                    $output = $model[0]['total'];
                }else{
                    $output = 0;
                }
                break;

            case "SUM_LOANEES":
                $SQL = "
              SELECT 
            COUNT(DISTINCT disbursement.application_id) AS 'total'
            FROM disbursement
            RIGHT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
        	WHERE disbursement.disbursement_batch_id = '$batchID'";
                $model = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($model)!=0){
                    $output = $model[0]['total'];
                }else{
                    $output = 0;
                }
                break;

            case "SUM_TABLE":
                $SQL = "
              SELECT 
             CONCAT(IFNULL(loan_item.item_name,''),' (',IFNULL(loan_item.item_code,''),')') AS 'item',
            COUNT(DISTINCT disbursement.application_id) AS 'count',
            SUM(disbursement.disbursed_amount) AS 'amount'
            FROM disbursement
            RIGHT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
            LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
        	WHERE disbursement.disbursement_batch_id = '$batchID'
           GROUP BY disbursement.loan_item_id      
        	";
                $output = Yii::$app->db->createCommand($SQL)->queryAll();

                break;
        }





        return $output;

    }



    public static function PLS($batchID){
        $ITEMS = self::DBatchItems($batchID);
        $table = $thead = $tbody = $th = $tr = '';
        $SQL = "
        SELECT 
          disbursement.year_of_study AS 'YOS',
              COUNT(DISTINCT disbursement.application_id) AS 'STUDENT',";
        $th = '<tr>';
        $th.="<th>YOS</th>";
        $th.="<th style='text-align: right;'>STUDENTS</th>";

        foreach ($ITEMS as $index=>$dataArray){
            $id = $dataArray['item_id'];
            $code = $dataArray['item_code'];
            $SQL.="SUM( IF( disbursement.loan_item_id = $id, disbursed_amount, 0) ) AS '$code',";

            $th.="<th style='text-align: right;'>$code</th>";
        }
        $th.="<th style='text-align: right;'>TOTAL</th>";
        $th.= '</tr>';

        /* $SQL.="SUM( IF( disbursement.loan_item_id = 1, disbursed_amount, 0) ) AS BS,
          SUM( IF( disbursement.loan_item_id = 2, disbursed_amount, 0) ) AS MA,
          SUM( IF( disbursement.loan_item_id = 3, disbursed_amount, 0) ) AS FPT,
          SUM( IF( disbursement.loan_item_id = 4, disbursed_amount, 0) ) AS TU,
          SUM( IF( disbursement.loan_item_id = 5, disbursed_amount, 0) ) AS SFR,
          SUM( IF( disbursement.loan_item_id = 6, disbursed_amount, 0) ) AS RESEARCH,
          SUM( IF( disbursement.loan_item_id = 7, disbursed_amount, 0) ) AS STIPEND,
          SUM( IF( disbursement.loan_item_id = 8, disbursed_amount, 0) ) AS F_ALLOWANCE,
          SUM( IF( disbursement.loan_item_id = 9, disbursed_amount, 0) ) AS M_FEE,";*/

        $SQL.="SUM(disbursement.disbursed_amount) AS 'TOTAL'
            FROM disbursement
            RIGHT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
            INNER JOIN application ON disbursement.application_id = application.application_id
            INNER JOIN applicant ON application.applicant_id = applicant.applicant_id
            INNER JOIN user ON applicant.user_id = user.user_id
            INNER JOIN programme ON disbursement.programme_id = programme.programme_id
            LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
            LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
			WHERE disbursement.disbursement_batch_id = '$batchID' 
            GROUP BY disbursement.year_of_study
            ORDER BY disbursement.year_of_study ASC";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();

        $thead.='<thead>'.$th.'</thead>';

        $STUDENT = $GRAND = 0;
        $itemTotalArray = array();

        foreach ($model as $mi=>$mArray){

            $tr.="<tr>";
            $tr.="<th>".$mArray['YOS']."</th>";
            $tr.="<th style='text-align: right;'>".number_format($mArray['STUDENT'],0)."</th>";
            $STUDENT+=$mArray['STUDENT'];
            foreach ($ITEMS as $index=>$dataArray) {
                $code = $dataArray['item_code'];
                $tr .= "<th style='text-align: right; '>".number_format($mArray[$code],2)."</th>";
            }
            $GRAND+=$mArray['TOTAL'];
            $tr.="<th style='text-align: right;'>".number_format($mArray['TOTAL'],2)."</th>";
            $tr.="</tr>";

        }


        $tr.="<tr>";
        $tr.="<th>GRAND TOTAL</th>";
        $tr.="<th style='text-align: right;'>".number_format($STUDENT,0)."</th>";
        foreach ($ITEMS as $index=>$dataArray) {
            $itemID = $dataArray['item_id'];

            $tr .= "<th style='text-align: right; '>".number_format(self::BatchItemTotal($batchID,$itemID),2)."</th>";
        }
        $tr.="<th style='text-align: right;'>".number_format($GRAND,2)."</th>";
        $tr.="</tr>";



        $tbody.='<thead>'.$tr.'</thead>';


        $table.='<table width="100%" style=" font-family: \'Courier New\';">';
        $table.=$thead;
        $table.=$tbody;
        $table.= "</table>";


        return $table;
    }


    public static  function BatchItemTotal($batchID,$itemID){
        $output = 0;
        $SQL = "SELECT SUM(disbursed_amount) AS 'amount' FROM disbursement WHERE disbursement_batch_id = '$batchID' AND loan_item_id = '$itemID';";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output= $model[0]['amount'];
        }

        return $output;
    }



    public static function AutoReturnsFeedBack($index,$loanItem,$amount,$operationDate){

    }

    public static function AutoPaymentFeedBack($index,$loanItem,$amount,$operationDate){

    }

    public static function AutoSendPayList($index,$loanItem,$amount,$operationDate){

    }

    public static function AutoSendDisbursement($index,$loanItem,$amount,$operationDate){

    }


    public static function ApplicationID($indexNumber){
        $output = '';
        $aplSQL = "SELECT application.application_id FROM application
                LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                WHERE applicant.f4indexno = '$indexNumber'";
        $appModel = Yii::$app->db->createCommand($aplSQL)->queryAll();
        if (sizeof($appModel)!=0){
            $output = $appModel[0]['application_id'];
        }
        return $output;
    }


    public static function BatchAmount($batchID){
        $output = 0;
        $SQL = "SELECT SUM(allocated_amount) AS 'amount' FROM allocation WHERE allocation_batch_id = '$batchID'";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0]['amount'];
        }

        return $output;
    }


    public static function BatchBeneficiaries($batchID){
        $output = 0;
        $SQL = "SELECT COUNT(DISTINCT  application_id) AS 'count' FROM allocation WHERE allocation_batch_id = '$batchID'";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0]['count'];
        }

        return $output;
    }


    public static function BatchDisplay($batchID,$itemsArray=array())
    {
        $output = '';
        $allocationSQL = "SELECT * FROM allocation WHERE allocation.allocation_batch_id = '$batchID'";


        return $output;

    }


    public static function HeaderDisbursementSummary($batchID)
    {
        $SQL = "
          SELECT 
          loan_item.loan_item_id AS 'item_id',
          loan_item.item_name AS 'item_name',
          loan_item.item_code AS 'item_code',
        COUNT(disbursement.application_id) AS 'applicant',
        SUM(disbursement.disbursed_amount) AS 'amount' 
        FROM disbursement
        LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
        #LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
        WHERE disbursement.disbursement_batch_id = '$batchID'
        GROUP BY disbursement.loan_item_id
        
        ";
        return Yii::$app->db->createCommand($SQL)->queryAll();
    }
    public static function HeaderDisbursementSummaryTable($batchID)
    {
        $model = DisbursementBatch::findOne($batchID);
        $level = '';
        if (sizeof($model)!=0){
            $level = $model->Category;
        }
        $itemsObject = self::DBatchItems($batchID);
        $output = '';
        $grandArray = array();
        $table = $td = $th = $tr  = $thead = $tbody = $tfoot = '';

        $modelYOS = self::DBatchYos($batchID);

        $th.='<tr class="text-uppercase">';

        $th.='<th>YOS</th>';
        $th.='<th class="text-right">No. of Students</th>';

        foreach ($itemsObject as $ItemIndex=>$itemsDataArray)
        {

            $th.='<th class="text-right">';
            $th.=$itemsDataArray['item_code'];
            $th.='</th>';


        }
        $th.='<th class="text-right">TOTAL AMOUNT</th>';
        $th.='</tr>';



        $thead.=$th;




        foreach ($modelYOS as $iYos=>$dYOS) {
            $tr.= '<tr>';

            $tr.= '<td>';
            $tr.= $level.' '.self::THNumber($dYOS['yos']).' Year';
            $tr.= '</td>';
            $rawTotal = $students =0;
            $students = self::DisHeaderStudents($batchID,$dYOS['yos']);
            $tr.= '<th class="text-right text-bold">'.number_format($students).'</th>';
            foreach ($itemsObject as $ItemIndexB => $itemsDataArrayB) {
                $amount =  0;
                $dataList = self::DisSummary($batchID, $itemsDataArrayB['item_id'], $dYOS['yos']);
                if (sizeof($dataList) != 0) {
                    foreach ($dataList as $dlindx => $dlArray) {
                        $amount += $dlArray['amount'];
                        $students += $dlArray['count'];

                    }


                } else {
                    $amount = 0;
                }
                $rawTotal+=$amount;

                $grandArray[$dYOS['yos']][$itemsDataArrayB['item_id']]=$amount;
                $tr.= '<th class="text-right text-bold">'.number_format($amount,2).'</th>';


            }
            $tr.= '<th class="text-right text-bold">';
            $tr.= number_format($rawTotal,2);
            $tr.= '</th>';

            $tr.= '</tr>';
        }


        $tbody.=$tr;

        // $tfoot.= '<tr>';

        // $tfoot.= '<th>'.json_encode($grandArray).'</th>';


        /*        $sample = array();
                for ($i=1; $i<=5; $i++){

                    for ($x=1;$x<=8; $x++){
                        $sample[$i][$x]=($i*100);
                    }

                }

                print_r($sample);*/




        /* $data = array_sum(array_map(function($grandArray) {
             return $grandArray['1'];
         }, $grandArray));
         foreach ($itemsObject as $ItemIndexF => $itemsDataArrayF) {
             $tfoot .= '<th>' . json_encode(array_values($grandArray)).$data . '</th>';

         }*/

        //$tfoot.= '</tr>';

        $table.='<table class="table table-condensed table-striped table-bordered table-responsive">';
        $table.=$thead;
        $table.=$tbody;
        $table.=$tfoot;
        $table.='</table>';

        $output = $table;
        return $output;
    }


    public static function DBatchYos($batchID)
    {
        $output = '';

        $SQL = "
            SELECT 
            
            application.current_study_year AS 'yos'
            FROM disbursement
            LEFT JOIN application ON disbursement.application_id = application.application_id
            WHERE disbursement.disbursement_batch_id = '$batchID'
            GROUP BY application.current_study_year
            ORDER BY application.current_study_year ASC
        ";

        return Yii::$app->db->createCommand($SQL)->queryAll();

    }

    public static function DBatchItems($batchID)
    {
        $output = '';

        $SQL = "
            SELECT 
            loan_item.loan_item_id AS 'item_id',
            loan_item.item_name AS 'item_name',
            loan_item.item_code AS 'item_code'
            FROM disbursement
            LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
            LEFT JOIN application ON disbursement.application_id = application.application_id
            WHERE disbursement.disbursement_batch_id = '$batchID'
            GROUP BY disbursement.loan_item_id
            ORDER BY disbursement.loan_item_id ASC
        ";

        return Yii::$app->db->createCommand($SQL)->queryAll();

    }


    public static function DBatchCourse($batchID)
    {
        $output = '';

        $SQL = "
             SELECT 
            programme.programme_group_id AS 'group_id',
            COUNT( DISTINCT disbursement.application_id) AS 'count',
            programme_group.group_name AS 'group_name',
            programme_group.group_code AS 'group_code'
       
             FROM disbursement
    LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
    LEFT JOIN application ON disbursement.application_id = application.application_id
    LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
    LEFT JOIN user ON  applicant.user_id = user.user_id
  
    LEFT JOIN programme ON  disbursement.programme_id = programme.programme_id
    LEFT JOIN programme_group ON  programme.programme_group_id = programme_group.programme_group_id
            WHERE disbursement.disbursement_batch_id = '$batchID'
            GROUP BY programme.programme_group_id
            ORDER BY programme_group.group_code ASC
        ";

        return Yii::$app->db->createCommand($SQL)->queryAll();

    }
    public static function RequestCourse($requestID)
    {
        $output = '';

        $SQL = "
             SELECT 
            programme.programme_group_id AS 'group_id',
            COUNT( DISTINCT institution_paylist.application_id) AS 'count',
            programme_group.group_name AS 'group_name',
            programme_group.group_code AS 'group_code'
       
             FROM institution_paylist
    LEFT JOIN institution_fund_request ON institution_paylist.request_id = institution_fund_request.id
    LEFT JOIN application ON institution_paylist.application_id = application.application_id
    LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
    LEFT JOIN user ON  applicant.user_id = user.user_id
    LEFT JOIN programme ON  application.programme_id = programme.programme_id
    LEFT JOIN programme_group ON  programme.programme_group_id = programme_group.programme_group_id
            WHERE institution_paylist.request_id = '$requestID'
            GROUP BY programme.programme_group_id
            ORDER BY programme_group.group_code ASC
        ";

        return Yii::$app->db->createCommand($SQL)->queryAll();

    }


    public static function DisSummary($batchID,$item,$yos)
    {
        $SQL = "
          SELECT 
            COUNT(disbursement.application_id) AS 'count',
            SUM(disbursement.disbursed_amount) AS 'amount' 
            FROM disbursement
            LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
            LEFT JOIN application ON disbursement.application_id = application.application_id
            WHERE disbursement.disbursement_batch_id = '$batchID' AND disbursement.loan_item_id = '$item' AND application.current_study_year = '$yos'
       
        
        ";

        return Yii::$app->db->createCommand($SQL)->queryAll();
    }
    public static function DisHeaderStudents($batchID,$yos)
    {
        $output = 0;
        $SQL = "
          SELECT 
            COUNT(DISTINCT disbursement.application_id) AS 'count' 
            FROM disbursement
            LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
            LEFT JOIN application ON disbursement.application_id = application.application_id
            WHERE disbursement.disbursement_batch_id = '$batchID' AND application.current_study_year = '$yos'
       
        
        ";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0]['count'];
        }

        return $output;
    }


    public static function DynamicPayoutList($batchID,$courseID,$sn)
    {

        $output = '';
        $ItemsArray = Module::DBatchItems($batchID);
        $batchModel = DisbursementBatch::findOne($batchID);
        if (sizeof($batchModel)!=0)
        {
            $SQL = "";

            $SQL.="
                SELECT applicant.f4indexno AS 'INDEXNO',
              UPPER(CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,''))) AS 'NAME',
              UPPER(IFNULL(user.sex,'')) AS 'SEX',
              IFNULL(application.registration_number,'') AS 'REGNO',
              IFNULL(disbursement.year_of_study,'') AS 'YOS',
              IFNULL(application.bank_branch_name,'') AS 'BANK',
              IFNULL(application.bank_account_number,'') AS 'ACCOUNTNO', 
            IFNULL(allocation_batch.batch_number,'') AS 'BT',";

            foreach ($ItemsArray as $index=>$dataArray){
                $itemId = $dataArray['item_id'];
                $itemCode = $dataArray['item_code'];
                $itemName = $dataArray['item_name'];
                $SQL.="SUM( IF( disbursement.loan_item_id = $itemId, disbursement.disbursed_amount, 0) ) AS '$itemCode',";
            }
            $SQL.="SUM( disbursement.disbursed_amount ) AS total,";
            $SQL.="'_____________' AS 'SIGNATURE'";
            $SQL.="
                FROM disbursement
                LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
                LEFT JOIN application ON disbursement.application_id = application.application_id
                LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                LEFT JOIN user ON  applicant.user_id = user.user_id
                LEFT JOIN programme ON  disbursement.programme_id = programme.programme_id
                #LEFT JOIN programme ON  application.programme_id = programme.programme_id
                LEFT JOIN programme_group ON  programme.programme_group_id = programme_group.programme_group_id
                 LEFT JOIN allocation_batch ON  disbursement.allocation_batch_id = allocation_batch.allocation_batch_id
                WHERE disbursement.disbursement_batch_id = '$batchID' AND programme.programme_group_id = '$courseID'
                GROUP BY disbursement.application_id
                ORDER BY programme_group.group_code ASC, disbursement.year_of_study ASC, UPPER(CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,''))) ASC;";

            $tableModel = Yii::$app->db->createCommand($SQL)->queryAll();

            $table=$thead=$tbody=$tfoot=$tr=$th='';

            $th.='<tr>';
            $th.='<th class="totals" align="left">S/N</th>';
            $th.='<th class="totals" align="center">INDEXNO</th>';
            $th.='<th class="totals" align="left">NAME</th>';
            $th.='<th class="totals" align="center">SEX</th>';
            $th.='<th class="totals" align="center">REGNO</th>';
            $th.='<th class="totals" align="center">YOS</th>';
            $th.='<th class="totals" align="center">BANK</th>';
            $th.='<th class="totals" align="center">ACCOUNTNO</th>';
            $th.='<th class="totals" align="center">BT</th>';


            foreach ($ItemsArray as $indexH=>$dataArrayH){
                //$width = intval(20/sizeof($ItemsArray));
                $itemCodeH = $dataArrayH['item_code'];

                $th.='<th class="totals" align="right">'.$itemCodeH.'</th>';
            }

            // $th.='<th  class="totals">TOTAL</th>';
            $th.='<th class="totals">SIGNATURE</th>';

            $th.='</tr>';
            //$sn = 0;
            foreach ($tableModel as $indx=>$pDataArray)
            {
                $sn++;//$pDataArray["ACCOUNTNO"]
                $tr.='<tr>';
                $tr.='<td class="totals" align="center">'.$sn.'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["INDEXNO"].'</td>';
                $tr.='<td class="totals" align="left" style="">&nbsp;&nbsp;'.$pDataArray["NAME"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["SEX"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["REGNO"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["YOS"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["BANK"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["ACCOUNTNO"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["BT"].'</td>';

                foreach ($ItemsArray as $indexI=>$dataArrayI){
                    $itemCodeI = $dataArrayI['item_code'];
                    $tr.='<td class="totals" style="text-align: right;" align="right">&nbsp;&nbsp;'.number_format($pDataArray[$itemCodeI],0).'</td>';
                }
                //$tr.='<td style="text-align: right;">'.number_format($pDataArray["total"],2).'</td>';
                $tr.='<td class="totals">&nbsp;'.$pDataArray["SIGNATURE"].'</td>';


                $tr.='</tr>';
            }
            $totalCount = self::TotalCourseDis($batchID,$courseID)['count'];
            $tfoot.='<tfoot>';
            $tfoot.='<tr>';
            $tfoot.='<th colspan="9" align="right">TOTAL ('.number_format($totalCount,0).')</th>';
            foreach ($ItemsArray as $indexI=>$dataArrayI){
                $itemID = $dataArrayI['item_id'];
                $totalArray = self::TotalDisPerCourse($batchID,$itemID,$courseID);
                $totalAmount = $totalArray['amount'];

                $tfoot.='<th class="totals" style="text-align: right;" align="right">&nbsp;&nbsp;'.number_format($totalAmount,0).'</th>';
            }
            $tfoot.='<th></th>';
            $tfoot.='</tr>';
            $tfoot.='</tfoot>';


            $thead.='<thead style="display: table-header-group">'.$th.'</thead>';
            $tbody.='<tbody>'.$tr.'</tbody>';
            $table.='<table style="border-spacing: 0 4px; font-size: 3mm; font-family: \'Courier New\';" width="100%" border="0"cellspacing="10" >';
            $table.=$thead;
            $table.=$tbody;
            $table.=$tfoot;
            $table.='</table>';

            $output = $table;
        }


        return $output;

    }
    public static function DynamicPayoutRequestList($requestID,$courseID,$sn,$signature=true)
    {

        $output = '';

        $RequestModel = InstitutionFundRequest::findOne($requestID);
        if (sizeof($RequestModel)!=0)
        {
            $SQL = "";

            $SQL.="
              SELECT applicant.f4indexno AS 'INDEXNO',
              UPPER(CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,''))) AS 'NAME',
              UPPER(IFNULL(user.sex,'')) AS 'SEX',
              IFNULL(application.registration_number,'') AS 'REGNO',
              IFNULL(application.current_study_year,'') AS 'YOS',
              IFNULL(application.bank_branch_name,'') AS 'BANK',
              IFNULL(application.bank_account_number,'') AS 'ACCOUNTNO', 
            IFNULL(allocation_batch.batch_number,'') AS 'BT',

            SUM( institution_paylist.allocated_amount ) AS total,
           '_____________' AS 'SIGNATURE'
        
                FROM institution_paylist
                LEFT JOIN institution_fund_request ON institution_paylist.request_id = institution_fund_request.id
                LEFT JOIN application ON institution_paylist.application_id = application.application_id
                LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                LEFT JOIN user ON  applicant.user_id = user.user_id
                LEFT JOIN programme ON  application.programme_id = programme.programme_id
                LEFT JOIN programme_group ON  programme.programme_group_id = programme_group.programme_group_id
                 LEFT JOIN allocation_batch ON  institution_paylist.allocation_batch_id = allocation_batch.allocation_batch_id
                WHERE institution_fund_request.id = '$requestID' AND programme.programme_group_id = '$courseID'
                GROUP BY institution_paylist.application_id
                ORDER BY programme_group.group_code ASC,application.current_study_year ASC, UPPER(CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,''))) ASC;
";


            $tableModel = Yii::$app->db->createCommand($SQL)->queryAll();

            $table=$thead=$tbody=$tfoot=$tr=$th='';

            $th.='<tr>';
            $th.='<th class="totals" align="left">S/N</th>';
            $th.='<th class="totals" align="center" style="text-align: center;">INDEXNO</th>';
            $th.='<th class="totals" align="center" style="text-align: center;">NAME</th>';
            $th.='<th class="totals" align="center" style="text-align: center;">SEX</th>';
            $th.='<th class="totals" align="center" style="text-align: center;">REGNO</th>';
            $th.='<th class="totals" align="center" style="text-align: center;">YOS</th>';
            $th.='<th class="totals" align="center" style="text-align: center;">BANK</th>';
            $th.='<th class="totals" align="center" style="text-align: center;">ACCOUNTNO</th>';
            $th.='<th class="totals" align="center" style="text-align: center;">BT</th>';



            $th.='<th class="totals" style="text-align: right;" align="right">AMOUNT</th>';
            if($signature){$th.='<th class="totals">SIGNATURE</th>';}


            $th.='</tr>';
            //$sn = 0;
            foreach ($tableModel as $indx=>$pDataArray)
            {
                $sn++;
                $tr.='<tr>';
                $tr.='<td class="totals" align="left">'.$sn.'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["INDEXNO"].'</td>';
                $tr.='<td class="totals" align="left" style="">&nbsp;&nbsp;'.$pDataArray["NAME"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["SEX"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["REGNO"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["YOS"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["BANK"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["ACCOUNTNO"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["BT"].'</td>';


                $tr.='<td class="totals" style="text-align: right;" align="right">&nbsp;&nbsp;'.number_format($pDataArray["total"],0).'</td>';

                if($signature){ $tr.='<td class="totals">&nbsp;'.$pDataArray["SIGNATURE"].'</td>';}


                $tr.='</tr>';
            }
            $totalArray = self::TotalCourseRequest($requestID,$courseID);
            $totalCount = $totalArray['count'];
            $totalAmount = $totalArray['amount'];
            $tfoot.='<tfoot>';
            $tfoot.='<tr>';
            $tfoot.='<th colspan="9" align="right">TOTAL ('.number_format($totalCount,0).')</th>';
            $tfoot.='<th class="totals" style="text-align: right;" align="right">&nbsp;&nbsp;'.number_format($totalAmount,0).'</th>';

            $tfoot.='<th></th>';
            $tfoot.='</tr>';
            $tfoot.='</tfoot>';


            $thead.='<thead style="display: table-header-group; font-size: 4mm;">'.$th.'</thead>';
            $tbody.='<tbody>'.$tr.'</tbody>';
            $table.='<table style="border-spacing: 0 4px; font-size: 3.5mm; font-family: \'Courier New\'; background-color: #FFFFFF;" width="100%" border="0"cellspacing="10" >';
            $table.=$thead;
            $table.=$tbody;
            $table.=$tfoot;
            $table.='</table>';

            $output = $table;
        }


        return $output;

    }



    public static function DynamicMisDisbursementList($batchID)
    {

        $output = '';
        $batchModel = DisbursementBatch::findOne($batchID);
        if (sizeof($batchModel)!=0)
        {
            $SQL = "";

            $SQL.="
                 SELECT applicant.f4indexno AS 'INDEXNO',
                    UPPER(CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,''))) AS 'NAME',
                    UPPER(IFNULL(user.sex,'')) AS 'SEX',
                    IFNULL(application.registration_number,'') AS 'REGNO',
                    IFNULL(application.current_study_year,'') AS 'YOS',
                    IFNULL(programme_group.group_code,'') AS 'COURSE',
                    IFNULL(mis_disbursed.reasons,'') AS 'REASONS',
                    IFNULL(allocation_batch.batch_number,'') AS 'BATCHNO'
                    
                    FROM mis_disbursed
                    LEFT JOIN disbursement_batch ON mis_disbursed.disbursement_batch_id = disbursement_batch.disbursement_batch_id
                    LEFT JOIN application ON mis_disbursed.application_id = application.application_id
                    LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                    LEFT JOIN user ON  applicant.user_id = user.user_id
                    LEFT JOIN programme ON  application.programme_id = programme.programme_id
                    LEFT JOIN programme_group ON  programme.programme_group_id = programme_group.programme_group_id
                     LEFT JOIN allocation_batch ON  disbursement_batch.allocation_batch_id = allocation_batch.allocation_batch_id
                    WHERE mis_disbursed.disbursement_batch_id = '$batchID'
                    GROUP BY mis_disbursed.application_id
                    ORDER BY UPPER(CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,''))) ASC,programme_group.group_code ASC,application.current_study_year ASC;
";


            $tableModel = Yii::$app->db->createCommand($SQL)->queryAll();

            $table=$thead=$tbody=$tfoot=$tr=$th='';

            $th.='<tr>';
            $th.='<th class="totals" align="left">S/N</th>';
            $th.='<th class="totals" align="center">INDEXNO</th>';
            $th.='<th class="totals" align="left">NAME</th>';
            $th.='<th class="totals" align="center">SEX</th>';
            $th.='<th class="totals" align="center">REGNO</th>';
            $th.='<th class="totals" align="center">YOS</th>';
            $th.='<th class="totals" align="center">COURSE</th>';
            $th.='<th class="totals" align="center">REASONS</th>';
            $th.='<th class="totals" align="center">BATCHNO</th>';
            $th.='</tr>';
            $sn = 0;
            foreach ($tableModel as $indx=>$pDataArray)
            {
                $sn++;//$pDataArray["ACCOUNTNO"]
                $tr.='<tr>';
                $tr.='<td class="totals" align="center">'.$sn.'</td>';
                $tr.='<td class="totals" align="left">'.$pDataArray["INDEXNO"].'</td>';
                $tr.='<td class="totals" align="left" style="">&nbsp;&nbsp;'.$pDataArray["NAME"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["SEX"].'</td>';
                $tr.='<td class="totals" align="left">'.$pDataArray["REGNO"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["YOS"].'</td>';
                $tr.='<td class="totals" align="left">'.$pDataArray["COURSE"].'</td>';
                $tr.='<td class="totals" align="left">'.$pDataArray["REASONS"].'</td>';
                $tr.='<td class="totals" align="center">'.$pDataArray["BATCHNO"].'</td>';
                $tr.='</tr>';
            }




            $thead.='<thead style="display: table-header-group">'.$th.'</thead>';
            $tbody.='<tbody>'.$tr.'</tbody>';
            $table.='<table style="border-spacing: 0 4px; font-size: 3mm; font-family: \'Courier New\';" width="100%" border="0"cellspacing="10" >';
            $table.=$thead;
            $table.=$tbody;
            $table.=$tfoot;
            $table.='</table>';

            $output = $table;
        }


        return $output;

    }

    public static function TotalDisPerCourse($batchID,$itemID,$course){

        $output = array();

        $SQL = "
        SELECT 
        COUNT(DISTINCT disbursement.application_id)  AS 'count',
        IFNULL(SUM(disbursement.disbursed_amount), 0)  AS 'amount'
        FROM disbursement
        LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
        LEFT JOIN application ON disbursement.application_id = application.application_id
        LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
        LEFT JOIN user ON  applicant.user_id = user.user_id
        LEFT JOIN programme ON  disbursement.programme_id = programme.programme_id
        LEFT JOIN programme_group ON  programme.programme_group_id = programme_group.programme_group_id
        LEFT JOIN allocation_batch ON  disbursement_batch.allocation_batch_id = allocation_batch.allocation_batch_id
        WHERE disbursement.disbursement_batch_id = '$batchID' AND disbursement.loan_item_id = '$itemID' AND programme.programme_group_id = '$course';
        ";

        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0];

        }

        return $output;
    }

    public static function TotalCourseDis($batchID,$course){

        $output = array();

        $SQL = "
        SELECT 
        COUNT(DISTINCT disbursement.application_id)  AS 'count',
        IFNULL(SUM(disbursement.disbursed_amount), 0)  AS 'amount'
        FROM disbursement
        LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
        LEFT JOIN application ON disbursement.application_id = application.application_id
        LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
        LEFT JOIN user ON  applicant.user_id = user.user_id
        LEFT JOIN programme ON  disbursement.programme_id = programme.programme_id
        LEFT JOIN programme_group ON  programme.programme_group_id = programme_group.programme_group_id
        LEFT JOIN allocation_batch ON  disbursement_batch.allocation_batch_id = allocation_batch.allocation_batch_id
        WHERE disbursement.disbursement_batch_id = '$batchID' AND programme.programme_group_id = '$course';
        ";

        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0];

        }

        return $output;
    }

    public static function TotalCourseRequest($requestID,$course){

        $output = array();

        $SQL = "
        SELECT 
        COUNT(DISTINCT institution_paylist.application_id)  AS 'count',
        IFNULL(SUM(institution_paylist.allocated_amount), 0)  AS 'amount'
        FROM institution_paylist
        LEFT JOIN application ON institution_paylist.application_id = application.application_id
        LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
        LEFT JOIN user ON  applicant.user_id = user.user_id
        LEFT JOIN programme ON  application.programme_id = programme.programme_id
        LEFT JOIN programme_group ON  programme.programme_group_id = programme_group.programme_group_id
       
        WHERE institution_paylist.request_id = '$requestID' AND programme.programme_group_id = '$course';
        ";

        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0];

        }

        return $output;
    }

    public static function MemoSummary($batchID){
        $output = $tr = $td = $table = $th = $thead = $tbody = $tfoot = '';
        $SQL = "SELECT 

application.current_study_year AS 'yos',
  COUNT( DISTINCT disbursement.application_id) 'count',
  SUM(disbursement.disbursed_amount) 'amount'
    FROM disbursement
    LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
    LEFT JOIN application ON disbursement.application_id = application.application_id
    LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
    LEFT JOIN user ON  applicant.user_id = user.user_id
    LEFT JOIN programme ON  application.programme_id = programme.programme_id
    LEFT JOIN programme_group ON  programme.programme_group_id = programme_group.programme_group_id
    LEFT JOIN allocation_batch ON  disbursement_batch.allocation_batch_id = allocation_batch.allocation_batch_id
    
    WHERE disbursement.disbursement_batch_id = '$batchID'
    GROUP BY application.current_study_year
    ORDER BY application.current_study_year ASC;";

        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        $batchModel = DisbursementBatch::findOne($batchID);
        $level = '';
        $amount = 0;
        if (sizeof($batchModel)!=0){
            $level = $batchModel->Category;
            $amount = $batchModel->Amount;
        }


        if (sizeof($model)!=0){

            $GRAND = 0;
            foreach ($model as $index=>$dataArray){
                $amount = $dataArray['amount'];

                $GRAND+=$amount;
                $tr.='<tr>';
                $tr.='<td>'.self::THNumber($dataArray["yos"]).'</td>';
                $tr.='<td style="text-align: right;">'.number_format($dataArray['count'],0).'</td>';
                $tr.='<td  style="text-align: right;">'.number_format($amount,0).'</td>';
                $tr.='</tr>';

            }


            $thead.='<thead>';
            $thead.='<tr>';
            $thead.='<th style="text-align: center;">YOS</th>';
            $thead.='<th style="text-align: center;">No. OF STUDENTS</th>';
            $thead.='<th style="text-align: center;">AMOUNT</th>';
            $thead.='</tr>';
            $thead.='</thead>';
            $tbody.='<tbody>'.$tr.'</tbody>';
            $tfoot.='
            <tr>
                        <th>TOTAL</th>
                        <th style="text-align: right;">'.number_format($batchModel->Beneficiaries,0).'</th>
                        <th style="text-align: right;">'.number_format($batchModel->Amount,0).'</th>
                        
                    </tr>
            ';



            $payeeInfo = '
            <table style="border-spacing: 0 0px; font-size: 4mm; font-family: \'Courier New\';" width="100%" border="1" cellspacing="10">
                <thead>
                    <tr>
                        <th style="text-align: center;" align="center">PAYEE</th>
                        <th style="text-align: right;" align="right">No. OF STUDENTS</th>
                        <th style="text-align: right;" align="right">AMOUNT</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                        <td>BURSAR</td>
                        <th style="text-align: right;">'.number_format($batchModel->Beneficiaries,0).'</th>
                        <th style="text-align: right;">'.number_format($batchModel->Amount,0).'</th>
                        
                    </tr>
                </tbody>
                    
                </table>
            ';
            /*            <tr>
                                    <th style="text-align: right;" align="right">INSTITUTION</th>
                                    <th colspan="2" style="text-align: left;" align="left">&nbsp;&nbsp;'.$batchModel->learningInstitution->institution_name.'</th>
                                 </tr>
                                  <tr>
                                    <th style="text-align: right;" align="right">STUDENTS PAID</th>
                                    <th colspan="2" style="text-align: left;" align="left">&nbsp;&nbsp;'.number_format($batchModel->Beneficiaries,0).'</th>
                                 </tr>*/
            $table.='<table style="border-spacing: 0 0px; font-size: 4mm; font-family: \'Courier New\';" width="100%" border="1" cellspacing="10">';
            $table.=$thead;
            $table.=$tbody;
            $table.=$tfoot;
            $table.='</table>';
            $output = '
            <table style="border-spacing: 0 0px; font-size: 4mm; font-family: \'Courier New\';" width="100%" border="1" cellspacing="10">
                <thead>
                   
                    <tr>
                        <th style="text-align: center;" align="center">LOAN ITEMS</th>
                        <th style="text-align: center;" align="center">DISTRIBUTION PER YOS</th>
                        <th style="text-align: center;" align="center">PAYEE DETAILS</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>'.strtoupper($batchModel->Items).'</td>
                        <td>'.$table.'</td>
                        <td valign="top">'.$payeeInfo.'</td>
                    </tr>
                </tbody>
            </table>
            ';

        }

        $Chain =Module::PayListChain($amount);
        $SQLtsk = "
SELECT 
disbursement_task_definition.code, 
disbursement_payoutlist_movement.date_out, 
disbursement_payoutlist_movement.comment
FROM disbursement_payoutlist_movement
LEFT JOIN disbursement_task_definition ON disbursement_payoutlist_movement.disbursement_task_id = disbursement_task_definition.disbursement_task_id
WHERE disbursement_payoutlist_movement.disbursements_batch_id = '$batchID'";
        $modelMovement = Yii::$app->db->createCommand($SQLtsk)->queryAll();


        $tbl = $trf='';

        $tbl.='<tr><td><table style="border-spacing: 0 0px; font-size: 4mm; font-family: \'Courier New\';" width="100%" border="0" cellspacing="0">';

        $tbl .= '<tr><th colspan="2">&nbsp;</th></tr>';
        $tbl .= '<tr><th style="text-align: left; width: 200px;">1. PREPARED BY: </th><td>'.self::UserInfo($batchModel->created_by,"fullName").'</td></tr>';
        $tbl .= '<tr><th style="text-align: left;">Signature:</th><td><br>__________________________________</td></tr>';
        $tbl .= '<tr><th style="text-align: left;">Title:</th><td>LOAN DISBURSEMENT OFFICER</td></tr>';
        //$tbl .= '<tr><th style="text-align: left;">Date:</th><td>'.date("d/m/Y",strtotime($batchModel->created_at)).'</td></tr>';
        $tbl .= '<tr><th style="text-align: left;">Date:</th><td></td></tr>';
        $tbl .= '<tr><th style="text-align: left;">Comment:</th><td>'.$batchModel->batch_desc.'</td></tr>';

        $tbl.='</table></td>';

        $mvtCounter = 1; foreach ($Chain as $index=>$dataArray)
        {
            $mvtCounter++;
            if ($mvtCounter%2 !==0){  $tbl.='<tr><td>'; }else{ $tbl.='<td>'; }
            $tbl.='<table style="border-spacing: 0 0px; font-size: 4mm; font-family: \'Courier New\';" width="100%" border="0" cellspacing="0">';


            $tbl .= '<tr><th colspan="2">&nbsp;</th></tr>';
            $tbl .= '<tr><th style="text-align: left;">'.$mvtCounter.'. '. $dataArray["task_code"].'(Name)</th><td></td></tr>';
            $tbl .= '<tr><th style="text-align: left;">Signature:</th><td><br>__________________________________</td></tr>';
            $tbl .= '<tr><th style="text-align: left;">Title:</th><td>'.strtoupper($dataArray["structure_name"]).'</td></tr>';
            $tbl .= '<tr><th style="text-align: left;">Date:</th><td></td></tr>';
            //$tbl .= '<tr><th style="text-align: left;">Comment:</th><td></td></tr>';

            $tbl.='</table>';

            if ($mvtCounter%2==0){  $tbl.='</td></tr>'; }else{ $tbl.='</td>';}
        }

        $output.='<hr /><table style="border-spacing: 0 0px; font-size: 4mm; font-family: \'Courier New\';" width="100%" border="0" cellspacing="0">'.$tbl.'</table>';



        return $output;
    }

    public static function CustomerStatementCreator($operation,$amount,$operation_id,$type=null)
    {
        $output = '';
        $now = date('Y-m-d H:i:s');

        $user = Yii::$app->user->id;
        $creditAmount =$debitAmount = 0;
        $academicYear = self::CurrentAcademicYear();
        $application_id=$programme_id=$institution=$yos=$installment=$loan_item=$operationType='';

        switch (strtoupper($operation))
        {
            //'DISBURSEMENT', 'ADDITIONAL', 'DEDUCTION', 'ADJUSTMENT', 'RETURNS', 'REPAYMENT'
            case 'DISBURSEMENT':
                $debitAmount=$amount;
                $creditAmount = 0;
                $DISBURSEMENT = Disbursement::findOne($operation_id);
                $application_id = $DISBURSEMENT->application_id;
                $user = $DISBURSEMENT->disbursementBatch->created_by;
                $loan_item = $DISBURSEMENT->loan_item_id;
                $institution = $DISBURSEMENT->disbursementBatch->learning_institution_id;
                $installment = $DISBURSEMENT->disbursementBatch->instalment_definition_id;
                $yos = $DISBURSEMENT->application->current_study_year;
                $programme_id = $DISBURSEMENT->programme_id;
                $academicYear = $DISBURSEMENT->disbursementBatch->academic_year_id;

                break;

            case 'ADDITIONAL':
                $debitAmount=$amount;
                $creditAmount = 0;
                $DISBURSEMENT = Disbursement::findOne($operation_id);
                $application_id = $DISBURSEMENT->application_id;
                $user = $DISBURSEMENT->disbursementBatch->created_by;
                $loan_item = $DISBURSEMENT->loan_item_id;
                $institution = $DISBURSEMENT->disbursementBatch->learning_institution_id;
                $installment = $DISBURSEMENT->disbursementBatch->instalment_definition_id;
                $yos = $DISBURSEMENT->application->current_study_year;
                $programme_id = $DISBURSEMENT->programme_id;
                $academicYear = $DISBURSEMENT->disbursementBatch->academic_year_id;
                break;


            case 'DEDUCTION':
                $debitAmount=0;
                $creditAmount = $amount;

                $DISBURSEMENT = Disbursement::findOne($operation_id);
                $application_id = $DISBURSEMENT->application_id;
                $user = $DISBURSEMENT->disbursementBatch->created_by;
                $loan_item = $DISBURSEMENT->loan_item_id;
                $institution = $DISBURSEMENT->disbursementBatch->learning_institution_id;
                $installment = $DISBURSEMENT->disbursementBatch->instalment_definition_id;
                $yos = $DISBURSEMENT->application->current_study_year;
                $programme_id = $DISBURSEMENT->programme_id;
                $academicYear = $DISBURSEMENT->disbursementBatch->academic_year_id;
                break;

            case 'ADJUSTMENT':
                if ($type=='CR'){
                    $debitAmount=0;
                    $creditAmount = $amount;
                }else{
                    $debitAmount=$amount;
                    $creditAmount = 0;
                }
                $ADJUSTMENT = DisbursementAdjustment::findOne($operation_id);
                $user = $ADJUSTMENT->adjusted_by;
                $application_id = $ADJUSTMENT->application_id;
                $loan_item = $ADJUSTMENT->loan_item_id;
                /*$institution = $ADJUSTMENT->;
                $installment = $ADJUSTMENT->disbursement->disbursementBatch->instalment_definition_id;
                $yos = $ADJUSTMENT->disbursement->application->current_study_year;
                $programme_id = $ADJUSTMENT->disbursement->programme_id;
                $academicYear = $ADJUSTMENT->disbursement->disbursementBatch->academic_year_id;*/
                break;

            case 'RETURNS':
                $debitAmount=0;
                $creditAmount = $amount;
                $RETURN = DisbursementReturn::findOne($operation_id);
                $DISBURSEMENT = Disbursement::findOne($RETURN->disbursement_id);
                $application_id = $RETURN->application_id;

                $loan_item = $RETURN->loan_item;
                $institution = $RETURN->returnBatch->institution_id;
                $installment = $RETURN->instalment_number;
                $user = $RETURN->created_by;
                $yos = $RETURN->year_of_study;
                $programme_id = $DISBURSEMENT->programme_id;
                $academicYear = $RETURN->returnBatch->academic_year;
                $DISBURSEMENT->status = '10';//RETURN
                $DISBURSEMENT->save(false);
                break;

            case 'REPAYMENT':
                $debitAmount=0;
                $creditAmount = $amount;
                break;

        }
        $operationType = strtoupper($operation);

        $SQL="INSERT IGNORE INTO customer_statement (
application_id, programme_id, institution_id, year_of_study, academic_year_id, installment_number, operation_id, loan_item_id, debit_amount, credit_amount, operation_type, created_at, created_by) VALUES 
('$application_id','$programme_id','$institution','$yos','$academicYear','$installment','$operation_id','$loan_item','$debitAmount','$creditAmount','$operationType','$now','$user');";

        $output = Yii::$app->db->createCommand($SQL)->execute();


        return $output;

    }


    public static function BatchStatementCreator($batchID)
    {
        $output = '';
        $SQLstatement = "SELECT * FROM disbursement WHERE disbursement_batch_id = '$batchID'";
        $statementModel = Yii::$app->db->createCommand($SQLstatement)->queryAll();
        foreach ($statementModel as $index=>$statementArray){
            $id = $statementArray['disbursement_id'];
            $amount =$statementArray['disbursed_amount'];
            $output = Module::CustomerStatementCreator('DISBURSEMENT',$amount,$id,null);
        }

        return $output;
    }


    public static function CustomerStatementQuery($indexNumber,$YoS=null,$startDate=null,$endDate=null,$instalment=null)
    {
        $output = array();
        $WHERE = '';
        if ($YoS!==null){ $WHERE.= " AND customer_statement.year_of_study = '$YoS'"; }
        if ($instalment!==null){ $WHERE.= " AND customer_statement.installment_number = '$instalment'"; }

        if ($startDate!==null && $endDate!==null){
            $WHERE.= " AND (customer_statement.created_at BETWEEN '$startDate' AND '$endDate')";
        }else{
            if ($startDate!==null){ $WHERE.= " AND customer_statement.created_at = '$startDate'"; }
            if ($endDate!==null){ $WHERE.= " AND customer_statement.created_at = '$endDate'"; }
        }



        $SQL = "SELECT 
                customer_statement.operation_type AS 'TYPE',
                customer_statement.created_at AS 'DATE',
                loan_item.item_code AS 'LOAN_ITEM',
                instalment_definition.instalment AS 'INSTALMENT',
                customer_statement.debit_amount AS 'DEBITS',
                customer_statement.credit_amount AS 'CREDITS',
                learning_institution.institution_code AS 'PAID_TO',
                CASE WHEN customer_statement.operation_type='DISBURSEMENT' THEN disbursement_batch.batch_number ELSE customer_statement.operation_id END AS 'HEADER_ID',
                disbursement_batch.cheque_number AS 'CHEQUE_NO',
                UPPER(CONCAT(LEFT(IFNULL(user.firstname,''),1),'',IFNULL(user.surname,''))) AS 'OFFICER'
                FROM customer_statement
                LEFT JOIN disbursement ON customer_statement.operation_id = disbursement.disbursement_id
                LEFT JOIN instalment_definition ON customer_statement.installment_number = instalment_definition.instalment_definition_id
                LEFT JOIN loan_item ON customer_statement.loan_item_id = loan_item.loan_item_id
                LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
                LEFT JOIN learning_institution ON customer_statement.institution_id = learning_institution.learning_institution_id
                LEFT JOIN user ON customer_statement.created_by = user.user_id
                LEFT JOIN application ON customer_statement.application_id = application.application_id
                LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                LEFT JOIN disbursement_return ON customer_statement.operation_id = disbursement_return.id  AND customer_statement.operation_type = 'RETURNS'
                LEFT JOIN disbursement_return_batch ON disbursement_return.return_batch_id = disbursement_return_batch.id
                WHERE applicant.f4indexno = '$indexNumber' $WHERE
                ORDER BY customer_statement.created_at ASC;
                ";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model;
            $table = $tr = $thead = $tfoot = '';
            $D_GRAND = $C_GRAND = 0;

            foreach ($model as $index=>$dataArray){
                $tr.='<tr>';
                $tr.='<td>'.date('d-M-y',strtotime($dataArray["DATE"])).'&nbsp;&nbsp;</td>';
                $tr.='<td>'.$dataArray["LOAN_ITEM"].'</td>';
                //$tr.='<td>'.$dataArray["LOAN_ITEM"].' ('.$dataArray["TYPE"].')</td>';
                $tr.='<td style="text-align: center;" align="center">'.$dataArray["INSTALMENT"].'</td>';
                $tr.='<th style="text-align: right;">'.number_format($dataArray["DEBITS"],2).'</th>';
                $tr.='<th style="text-align: right;">'.number_format($dataArray["CREDITS"],2).'</th>';
                $tr.='<td style="text-align: center;" align="center" align="center">&nbsp;&nbsp;'.$dataArray["PAID_TO"].'</td>';
                $tr.='<td style="text-align: center;" align="center">&nbsp;&nbsp;'.$dataArray["HEADER_ID"].'</td>';
                $tr.='<td>&nbsp;&nbsp;'.$dataArray["CHEQUE_NO"].'&nbsp;&nbsp;</td>';
                $tr.='<td>'.$dataArray["OFFICER"].'</td>';
                $tr.='</tr>';
                $D_GRAND+= $dataArray["DEBITS"];
                $C_GRAND+= $dataArray["CREDITS"];
            }
            $tfoot.='<tr>';
            $tfoot.='<td></td>';
            $tfoot.='<td></td>';

            $tfoot.='<th style="text-align: right;">TOTAL</th>';
            $tfoot.='<th style="text-align: right;">'.number_format($D_GRAND,2).'</th>';
            $tfoot.='<th style="text-align: right;">'.number_format($C_GRAND,2).'</th>';
            $tfoot.='<td></td>';
            $tfoot.='<td></td>';
            $tfoot.='<td></td>';
            $tfoot.='<td></td>';
            $tfoot.='</tr>';

            $thead.='<thead style="display: table-header-group">
           <tr>
               <th style="text-align: left;" >DATE</th>
               <th style="text-align: left;" width="150px">LOAN ITEM</th>
               <th style="text-align: center;" align="center">INSTALMENT</th>
               <th style="text-align: right;" align="right" width="200px">DEBITS</th>
               <th style="text-align: right;" align="right" width="200px">CREDITS</th>
               <th style="text-align: center;" align="center">PAID TO</th>
               <th style="text-align: center;" align="center">HEADER ID</th>
               <th style="text-align: left;" >CHEQUE NO</th>
               <th style="text-align: left;" >OFFICER</th>
            </tr>
           </thead>';
            $table.='<table style="border-spacing: 0 4px; font-size: 4mm; font-family: \'Courier New\';" width="100%" border="0"cellspacing="10" >';
            $table.=$thead;
            $table.=$tr;
            $table.=$tfoot;
            $table.='</table>';


            $table.='<table style="border-spacing: 0 4px; font-size: 4mm; font-family: \'Courier New\';" width="100%" border="0"cellspacing="10" >';
            $table.='
            <tr>
            <th style="text-align: right;" align="right">NET DISBURSEMENT</th>
            <th style="text-align: right;" align="right">'.number_format(($D_GRAND-$C_GRAND),2).'</th>
            </tr>
            ';
            $table.='</table>';

            $output['table'] = $table;
            $output['model'] = $model;
        }

        return $output;
    }
    public static function CustomerAllocationQuery($indexNumber)
    {
        $output = array();




        $SQL = "
            SELECT 
            applicant.f4indexno AS 'INDEXNO',
            UPPER(CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,''))) AS 'APPLICANT',
            allocation_batch.approved_at AS 'DATE',
            loan_item.item_name AS 'LOAN_ITEM',
            allocation_batch.batch_number AS 'BATCHNO',
            allocation.allocated_amount AS 'AMOUNT',
            academic_year.academic_year AS 'AY'
            
            FROM allocation
            LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
            LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
            LEFT JOIN academic_year ON allocation_batch.academic_year_id = academic_year.academic_year_id
            LEFT JOIN application ON allocation.application_id = application.application_id
            LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
            LEFT JOIN user ON applicant.user_id = user.user_id
            
            WHERE (allocation_batch.larc_approve_status = 1 OR allocation_batch.partial_approve_status = 1) AND applicant.f4indexno = '$indexNumber' 
            ORDER BY allocation_batch.academic_year_id ASC, allocation_batch.academic_year_id ASC, loan_item.item_code ASC;

                ";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model;
            $table = $tr = $thead = $tfoot = '';
            $GRAND =  0;

            foreach ($model as $index=>$dataArray){
                $tr.='<tr>';
                $tr.='<td style="text-align: left;" align="left">'.date('d-M-y',strtotime($dataArray["DATE"])).'</td>';
                $tr.='<td style="text-align: center;" align="center">'.$dataArray["AY"].'</td>';
                $tr.='<td style="text-align: center;" align="center">'.$dataArray["BATCHNO"].'</td>';
                $tr.='<td style="text-align: left;" align="left">'.$dataArray["LOAN_ITEM"].'</td>';
                $tr.='<th style="text-align: right;">'.number_format($dataArray["AMOUNT"],2).'</th>';

                $tr.='</tr>';
                $GRAND+= $dataArray["AMOUNT"];

            }
            $tfoot.='<tr>';
            $tfoot.='<td></td>';
            $tfoot.='<td></td>';
            $tfoot.='<td></td>';

            $tfoot.='<th style="text-align: right; font-weight: bolder; font-size: 18px;">TOTAL</th>';
            $tfoot.='<th style="text-align: right; font-weight: bolder; font-size: 18px;">'.number_format($GRAND,2).'</th>';
            $tfoot.='</tr>';

            $thead.='<thead style="display: table-header-group">
           <tr>
               <th style="text-align: left;" >DATE</th>
               <th style="text-align: center;">ACADEMIC YEAR</th>
               <th style="text-align: center;">BATCH #.</th>
               <th style="text-align: left;" >LOAN ITEM</th>
               <th style="text-align: right;" align="right"> AMOUNT</th>
            
            </tr>
           </thead>';
            $table.='<table style="border-spacing: 0 4px; font-size: 4mm; font-family: \'Courier New\';" width="100%" border="0"cellspacing="10" >';
            $table.=$thead;
            $table.=$tr;
            $table.=$tfoot;
            $table.='</table>';

            $output['table'] = $table;
            $output['model'] = $model;
        }

        return $output;
    }

    public static function APPLICANT_INFO($index,$field)
    {
        $output = '';
        /* $SQL="          SELECT
                         applicant.f4indexno AS 'index_number',
                         application.passport_photo AS 'photo',
                         user.phone_number AS 'phone_number',
                         user.email_address AS 'email_address',
                         allocation.application_id AS 'application_id',
                         application.programme_id AS 'programme_id',
                         programme.learning_institution_id AS 'institution_id',
                         application.registration_number AS 'registration_number',
                         CONCAT(user.firstname,' ',user.middlename,' ',user.surname) AS 'full_name',
                         CASE WHEN user.sex = 'M' THEN 'MALE' ELSE 'FEMALE' END AS 'Sex',
                         DATE_FORMAT(applicant.date_of_birth,'%d/%m/%Y') AS 'DOB',
                         programme.programme_name AS 'programme_name',
                         programme_group.group_code AS 'programme_code',
                         programme.years_of_study AS 'years_of_study',
                         learning_institution.institution_name AS 'Institution',
                         learning_institution.institution_code AS 'institution_code',
                         application.student_status AS 'student_status',
                         loan_item.item_name,
                         allocation.loan_item_id,
                         application.current_study_year,
                         academic_year.academic_year,
                         allocation.allocation_batch_id,
                         IFNULL(SUM(allocation.allocated_amount),0) AS 'allocated_amount',
                         CEIL(((IFNULL(SUM(disbursement.disbursed_amount),0))/(IFNULL(SUM(allocation.allocated_amount),1)))*100) AS 'disbursed_percentage',
                         IFNULL(SUM(disbursement.disbursed_amount),0) AS 'disbursed_amount'
                         FROM allocation
                         LEFT JOIN application ON allocation.application_id = application.application_id
                         LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                         LEFT JOIN user ON applicant.user_id = user.user_id
                         LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                         LEFT JOIN disbursement ON allocation.application_id = disbursement.application_id AND disbursement.loan_item_id = allocation.loan_item_id
                         LEFT JOIN programme ON application.programme_id = programme.programme_id
                         LEFT JOIN programme_group ON programme.programme_group_id = programme_group.programme_group_id
                         LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                         LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                         LEFT JOIN academic_year ON allocation_batch.academic_year_id = academic_year.academic_year_id
                         LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id AND disbursement_batch.allocation_batch_id = allocation_batch.allocation_batch_id
                         WHERE applicant.f4indexno = '$index'
                         GROUP BY allocation.application_id
                         ORDER BY applicant.f4indexno ASC,academic_year.academic_year_id ASC,loan_item.item_name ASC
                         LIMIT 1
             ";*/

        $SQL= self::ApplicantQuery($index);


        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            if ($field=='fullName'){
                $output = $model[0]['full_name'];
            }else{
                if (isset($model[0][$field])){
                    $output = $model[0][$field];
                }else{
                    $output = 'N/A';
                }
            }
        }

        return $output;

    }

    public static function ApplicantQuery($index)
    {
        //programme_code,full_name,current_study_year
        $SQL="
                    SELECT 
                    applicant.f4indexno AS 'index_number',
                    user.phone_number AS 'phone_number',
                    user.email_address AS 'email_address',
                    application.application_id AS 'application_id',
                    application.programme_id AS 'programme_id',
                    programme.learning_institution_id AS 'institution_id',
                    application.registration_number AS 'registration_number',
                    CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,'')) AS 'full_name',
                    CASE WHEN user.sex = 'M' THEN 'MALE' ELSE 'FEMALE' END AS 'Sex',
                    DATE_FORMAT(applicant.date_of_birth,'%d/%m/%Y') AS 'DOB',
                    programme_group.group_name AS 'programme_name',
                    programme_group.group_code AS 'programme_code',
                    programme.years_of_study AS 'years_of_study',
                    learning_institution.institution_name AS 'Institution',
                    learning_institution.institution_code AS 'institution_code',
                    application.current_study_year,
                    application.student_status AS 'student_status',
                    applicant_attachment.attachment_path AS 'photo'


                    FROM application
                    LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                    LEFT JOIN applicant_attachment ON application.applicant_id = applicant_attachment.application_id AND applicant_attachment.attachment_definition_id = '3'
                    LEFT JOIN user ON applicant.user_id = user.user_id
                    LEFT JOIN programme ON application.programme_id = programme.programme_id
                    LEFT JOIN programme_group ON programme.programme_group_id = programme_group.programme_group_id
                    LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                    
                    WHERE applicant.f4indexno = '$index'
                    GROUP BY applicant.f4indexno
                    ORDER BY applicant.f4indexno ASC
            ";

        return $SQL;
    }

    public static function BulkReturnAnalysis($recordID,$option)
    {
        $output = '';
        switch ($option){
            case "STAGING":

                break;

            case "ACTUAL":

                break;
        }

        return $output;
    }

    public static function CrossMatchCriteria($criteriaArray)
    {
        /** Bench marks for each Matching section
         *
         **/
        /** 01 Personal Particulars **/
        $pBM = 40;

        /** 01 Transaction Disbursement Particulars **/
        $tBM = 30;

        /** 01 Other Particulars **/
        $oBM = 30;


        $output = array();
        /** 01. Personal Particulars Existence **/
        /**
         * Index Number
         * Name
         * Sex
         **/
        $output['PERSONAL_PARTICULARS']=self::PersonalParticularCrossMatch($criteriaArray,$pBM);

        /** End checking Personal Particulars Existence **/


        /** 01. Index Number Existence **/
        $SQL="SELECT ";

        /** End checking Index Number Existence **/


        return $output;

    }

    public static function UploadReturns($sheetData,$baseRow,$headerID)
    {
        $output =array('updates'=>0,'new'=>0);
        $insertSQL = $updateSQL ='';
        $insertCounter = $updateCounter = 0;
        $model= DisbursementReturnBatch::findOne($headerID);
        if (sizeof($model)!=0){
            $user = Yii::$app->user->id;
            $academicYear = $model->academic_year;
            $financialYear = $model->financial_year;

            while(!empty($sheetData[$baseRow]['B']))
            {
                $dataRow = array();
                $SHEET_INDEX = (string)$sheetData[$baseRow]['A'];
                $INDEX_NO=(string)$sheetData[$baseRow]['B'];
                $NAME=(string)$sheetData[$baseRow]['C'];
                $SEX =(string)$sheetData[$baseRow]['D'];
                $YOS = (string)$sheetData[$baseRow]['E'];
                $PAY_LIST_INDEX = (string)$sheetData[$baseRow]['F'];
                $ITEM = (string)$sheetData[$baseRow]['G'];
                $INSTALMENT = (string)$sheetData[$baseRow]['H'];
                $AMOUNT = (string)$sheetData[$baseRow]['I'];
                $REMARKS=(string)$sheetData[$baseRow]['J'];

                $dataRow['SHEET_INDEX']= $SHEET_INDEX ;
                $dataRow['INDEX_NO']= $INDEX_NO;
                $dataRow['NAME']= $NAME;
                $dataRow['SEX']= $SEX;
                $dataRow['YOS']= $YOS;
                $dataRow['PAY_LIST_INDEX']= $PAY_LIST_INDEX;
                $dataRow['ITEM']= $ITEM;
                $dataRow['INSTALMENT']= $INSTALMENT;
                $dataRow['AMOUNT']= $AMOUNT;
                $dataRow['REMARKS']= $REMARKS;


                $sync_id = $INDEX_NO.'-'.$ITEM.'-'.$INSTALMENT.'-'.$headerID.'-'.$financialYear.'-'.$financialYear;
                $match_percent  = 0;
                $reasons = "";
                $dataRow['SYNC']= $sync_id;
                $checkSQL = "SELECT id FROM disbursement_return_staging WHERE sync_id='$sync_id'";
                $checkResults = Yii::$app->db->createCommand($checkSQL)->queryAll();

                //disbursement_cheque_number,specified_header_id,specified_student_category


                if (sizeof($checkResults)!=0){
                    $updateCounter++;
                    $updateSQL.="
                                UPDATE disbursement_return_staging SET 
                                student_name = '$NAME', sex = '$SEX', year_of_study = '$YOS', pay_sheet_serial_number = '$PAY_LIST_INDEX', amount = '$AMOUNT', remarks = '$REMARKS', match_percent = '$match_percent', reasons = '$reasons'
                                WHERE sync_id='$sync_id';";
                }else{
                    $insertCounter++;
                    $insertSQL.="INSERT INTO disbursement_return_staging 
                    (index_number, student_name, sex, year_of_study, pay_sheet_serial_number, loan_item, instalment_number, amount, remarks, academic_year, match_percent, reasons, created_by, created_on, header_id,sync_id)
                     VALUES ('$INDEX_NO','$NAME','$SEX','$YOS','$PAY_LIST_INDEX','$ITEM','$INSTALMENT','$AMOUNT','$REMARKS','$academicYear','$match_percent','$reasons','$user',NOW(),'$headerID','$sync_id');
                     ";
                }
                $dataArray[] = $dataRow;
                $baseRow++;
            }
            //print_r($dataArray);

            if ($insertCounter>0){ Yii::$app->db->createCommand($insertSQL)->execute(); }
            if ($updateCounter>0){ Yii::$app->db->createCommand($updateSQL)->execute(); }


        }

        $output = array('updates'=>$updateCounter,'new'=>$insertCounter);

        return $output;

    }
    public static function UploadBulkSuspension($sheetData,$baseRow,$headerID)
    {
        $output =array('updates'=>0,'new'=>0);
        $insertSQL = $updateSQL ='';
        $insertCounter = $updateCounter = 0;
        $model= SuspensionBatch::findOne($headerID);
        if (sizeof($model)!=0){
            $user = Yii::$app->user->id;


            while(!empty($sheetData[$baseRow]['B']))
            {
                $dataRow = array();
                $SHEET_INDEX = (string)$sheetData[$baseRow]['A'];
                $INDEX_NO=(string)$sheetData[$baseRow]['B'];
                $NAME=(string)$sheetData[$baseRow]['C'];
                $PROGRAMME=(string)$sheetData[$baseRow]['D'];
                $YOS = (string)$sheetData[$baseRow]['E'];
                $REMARKS=(string)$sheetData[$baseRow]['F'];

                $dataRow['SHEET_INDEX']= $SHEET_INDEX ;
                $dataRow['INDEX_NO']= $INDEX_NO;
                $dataRow['NAME']= $NAME;
                $dataRow['PROGRAMME']= $PROGRAMME;
                $dataRow['YOS']= $YOS;
                $dataRow['REMARKS']= $REMARKS;


                $sync_id = $INDEX_NO.'-'.$headerID;
                $reasons = "";
                $dataRow['SYNC']= $sync_id;
                $checkSQL = "SELECT id FROM suspension_staging WHERE sync_id='$sync_id'";
                $checkResults = Yii::$app->db->createCommand($checkSQL)->queryAll();

                /*
                 *  * @property integer $id
  * @property string $sn
  * @property string $index_number
  * @property string $names
  * @property string $programme
  * @property string $year_of_study
  * @property string $sync_id
  * @property integer $status
  * @property string $status_date
  * @property integer $header_id
  * @property integer $suspension_reason
  * @property integer $created_by
  * @property string $created_at
  * @property integer $updated_by
  * @property string $updated_at
                 * */


                if (sizeof($checkResults)!=0){
                    $updateCounter++;
                    $updateSQL.='
                                UPDATE suspension_staging SET 
                                names = "'.$NAME.'", programme = "'.$PROGRAMME.'", year_of_study = "'.$YOS.'", sn = "'.$SHEET_INDEX.'", suspension_reason = "'.$REMARKS.'"
                                WHERE sync_id="'.$sync_id.'";';
                }else{
                    $insertCounter++;
                    $insertSQL.='INSERT IGNORE INTO suspension_staging 
                    (sn, index_number, names, programme, year_of_study, status, status_date, suspension_reason , created_by, created_at, updated_by, updated_at, header_id,sync_id)
                     VALUES ("'.$SHEET_INDEX.'","'.$INDEX_NO.'","'.$NAME.'","'.$PROGRAMME.'","'.$YOS.'","0",NOW(),"'.$REMARKS.'","'.$user.'",NOW(),"'.$user.'",NOW(),"'.$headerID.'","'.$sync_id.'");
                     ';
                }
                $dataArray[] = $dataRow;
                $baseRow++;
            }
            //print_r($dataArray);

            if ($insertCounter>0){ Yii::$app->db->createCommand($insertSQL)->execute(); }
            if ($updateCounter>0){ Yii::$app->db->createCommand($updateSQL)->execute(); }


        }

        $output = array('updates'=>$updateCounter,'new'=>$insertCounter);

        return $output;

    }
    public static function UploadBulkDeposits($sheetData,$baseRow,$headerID)
    {
        $output =array('updates'=>0,'new'=>0);
        $insertSQL = $updateSQL ='';
        $insertCounter = $updateCounter = 0;
        //$model= SuspensionBatch::findOne($headerID);
        $model= DisbursementDepositBatch::findOne($headerID);
        if (sizeof($model)!=0){
            $user = Yii::$app->user->id;
            //S/N	INDEX	NAME	YOS	REG	INSTALMENT	ITEM	AMOUNT
            $academicYear = $model->academic_year;



            while(!empty($sheetData[$baseRow]['B']))
            {
                $dataRow = array();
                $SHEET_INDEX = (string)$sheetData[$baseRow]['A'];
                $INDEX_NO=(string)$sheetData[$baseRow]['B'];
                $NAME=(string)$sheetData[$baseRow]['C'];
                $YOS = (string)$sheetData[$baseRow]['D'];
                $REGISTRATION = (string)$sheetData[$baseRow]['E'];
                $INSTALMENT = self::INSTALMENT_CONVERTER((string)$sheetData[$baseRow]['F'],'id');
                $ITEM = (string)$sheetData[$baseRow]['G'];
                $AMOUNT =(string)$sheetData[$baseRow]['H'];

                $dataRow['SHEET_INDEX']= $SHEET_INDEX ;
                $dataRow['INDEX_NO']= $INDEX_NO;
                $dataRow['NAME']= $NAME;
                $dataRow['YOS']= $YOS;
                $dataRow['REGISTRATION']= $REGISTRATION;
                $dataRow['INSTALMENT']= $INSTALMENT;
                $dataRow['ITEM']= $ITEM;
                $dataRow['AMOUNT']= $AMOUNT;


                $sync_id = $INDEX_NO.'-'.$INSTALMENT.'-'.$ITEM.'-'.$headerID;

                $dataRow['SYNC']= $sync_id;
                $checkSQL = "SELECT id FROM disbursement_deposit_staging WHERE sync_id='$sync_id'";
                $checkResults = Yii::$app->db->createCommand($checkSQL)->queryAll();




                if (sizeof($checkResults)!=0){
                    $updateCounter++;
                    $updateSQL.='
                                UPDATE disbursement_deposit_staging SET 
                                names = "'.$NAME.'", registration_number = "'.$REGISTRATION.'", year_of_study = "'.$YOS.'", pay_sheet_serial_number = "'.$SHEET_INDEX.'", amount = "'.$AMOUNT.'"
                                WHERE sync_id="'.$sync_id.'";';
                }else{
                    $insertCounter++;

                    $insertSQL.='INSERT IGNORE INTO disbursement_deposit_staging (index_number, student_name, registration_number, sex, year_of_study, pay_sheet_serial_number, loan_item, instalment_number, amount, academic_year, status, created_by, created_on,header_id, deposit_header_id, sync_id)
                                VALUES ("'.$INDEX_NO.'","'.$NAME.'","'.$REGISTRATION.'","N/A","'.$YOS.'","'.$SHEET_INDEX.'","'.$ITEM.'","'.$INSTALMENT.'","'.$AMOUNT.'","'.$academicYear.'","NEW","'.$user.'",NOW(),"'.$headerID.'","'.$headerID.'","'.$sync_id.'");';

                }
                $dataArray[] = $dataRow;
                $baseRow++;
            }
            //print_r($dataArray);

            if ($insertCounter>0){ Yii::$app->db->createCommand($insertSQL)->execute(); }
            if ($updateCounter>0){ Yii::$app->db->createCommand($updateSQL)->execute(); }


        }

        $output = array('updates'=>$updateCounter,'new'=>$insertCounter);

        return $output;

    }


    public static function PersonalParticularCrossMatch($criteriaArray,$benchmark)
    {
        $output = array();
        $index = $criteriaArray['INDEX_NO'];
        $pSQL="SELECT applicant.f4indexno AS 'index',user.firstname AS 'f_name', user.middlename AS 'm_name',user.surname AS 's_name',CONCAT(IFNULL(user.firstname,''),' ',IFNULL(user.middlename,''),' ',IFNULL(user.surname,'')) AS 'name', applicant.sex 
                FROM applicant 
                LEFT JOIN user ON applicant.user_id = user.user_id
                WHERE applicant.f4indexno='$index'";
        $pModel= Yii::$app->db->createCommand($pSQL)->queryAll();
        $pMM = $pMP = 0;
        if (sizeof($pModel)!=0){

            //INDEX CHECKING
            if ($pModel[0]['index']==$index){  $pMM+=20;  }

            //NAMES CHECKING
            if (strtoupper($pModel[0]['name'])==strtoupper($criteriaArray['NAME'])){ $pMM+=10; } // Name Match
            else{
                $short_name=$pModel[0]['f_name'].' '.$pModel[0]['s_name'];
                if (strtoupper($short_name)==strtoupper($criteriaArray['NAME'])){ $pMM+=10; } //SHORT NAME CHECKING

                $university_name_format=$pModel[0]['s_name'].' '.$pModel[0]['f_name'];
                if (strtoupper($university_name_format)==strtoupper($criteriaArray['NAME'])){ $pMM+=10; } //UNIVERSITY NAME FORMAT CHECKING

                if ($pMM<=20){
                    $nameArray = explode(' ',$criteriaArray['NAME']);

                    $f_name = $pModel[0]['f_name'];
                    if (in_array($f_name,$nameArray)){ $pMM+=2.5;}

                    $m_name = $pModel[0]['m_name'];
                    if (in_array($m_name,$nameArray)){ $pMM+=2.5;}

                    $s_name = $pModel[0]['s_name'];
                    if (in_array($s_name,$nameArray)){ $pMM+=2.5;}

                }
            }

            //SEX CHECKING
            if (strtoupper($pModel[0]['sex'])==strtoupper($criteriaArray['SEX'])){ $pMM+=10; } // Sex Match
            else{ //Sex Mismatch
                if (strtoupper($pModel[0]['sex'])=='M'&&strtoupper($criteriaArray['SEX'])=='MALE'){ $pMM+=10; } //Male Matching

                if (strtoupper($pModel[0]['sex'])=='F'&&strtoupper($criteriaArray['SEX'])=='FEMALE'){ $pMM+=10; } //Female Matching

                if (strtoupper($pModel[0]['sex'])==''){ $pMM+=5; } //No Sex retrieved from on the database

            }


            if ($benchmark!=0){
                $pMP= round((($pMM/$benchmark)*100),1);
            }


            $output = array(
                'index'=>$pModel[0]['index'],
                'name'=>$pModel[0]['name'],
                'sex'=>$pModel[0]['sex'],
                'bench_mark'=>$benchmark,
                'match_mark'=>$pMM,
                'match_percent'=>$pMP,
            );
        }else{
            $output = array(
                'index'=>'',
                'name'=>'',
                'sex'=>'',
                'bench_mark'=>$benchmark,
                'match_mark'=>$pMM,
                'match_percent'=>$pMP,
            );
        }

        return $output;

    }

    public static function ReturnBatchNumberCreator()
    {
        $output = "";
        $serial=0;
        $SQL = "SELECT MAX(CAST(batch_number AS UNSIGNED)) AS 'max_no' FROM disbursement_return_batch;";
        $model= Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $serial = $model[0]['max_no'];
        }else{
            $serial=0;
        }
        $serial++;

        $output=$serial;

        return $output;

    }


    public static  function ReturnAmount($batchID,$option)
    {
        $output = 0;

        switch (strtoupper($option)){
            case "PENDING":
                $SQL = "SELECT IFNULL(SUM(disbursement_return_staging.amount),0) AS 'amount' FROM disbursement_return_staging WHERE  header_id= '$batchID'  AND status<>'CONFIRMED'";
                $model = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($model)!=0){
                    $output=$model[0]['amount'];
                }
                break;

            case "CONFIRMED":
                $SQL = "SELECT IFNULL(SUM(disbursement_return.amount),0) AS 'amount' FROM disbursement_return WHERE return_batch_id = '$batchID' AND status='0'";
                $model = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($model)!=0){
                    $output=$model[0]['amount'];
                }
                break;

            case "UPDATED":
                $SQL = "SELECT IFNULL(SUM(disbursement_return.amount),0) AS 'amount' FROM disbursement_return WHERE return_batch_id = '$batchID' AND status='1'";
                $model = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($model)!=0){
                    $output=$model[0]['amount'];
                }
                break;
        }


        return $output;
    }


    public static function CrossMatchReturns($returnID)
    {
        $output = false;

        $indexNo = $academicYear = $itemCode = $instalment = $institution = "";
        $amount = 0;
        $ReturnModel = DisbursementReturnStaging::findOne($returnID);
        if (sizeof($ReturnModel)!=0){
            $indexNo = $ReturnModel->index_number;
            $academicYear = $ReturnModel->academic_year;
            $itemCode = $ReturnModel->loan_item;
            $instalment = $ReturnModel->instalment_number;
            $institution = $ReturnModel->returnBatch->institution_id;
            $amount = $ReturnModel->amount;
            $SQL = "
                            SELECT 
                            disbursement.disbursement_id, 
                            disbursement.disbursement_batch_id, 
                            disbursement.allocation_batch_id, 
                            disbursement.application_id, 
                            disbursement.programme_id, 
                            disbursement.loan_item_id, 
                            disbursement.version, 
                            disbursement.allocated_amount, 
                            disbursement.disbursed_amount, 
                            disbursement.remaining_balance, 
                            disbursement.disbursed_as, 
                            disbursement.status, 
                            disbursement.status_date, 
                            disbursement.created_at, 
                            disbursement.created_by, 
                            disbursement.sync_id,
                            disbursement_batch.batch_number AS 'HeaderID'
                            FROM disbursement
                            LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
                            LEFT JOIN application ON disbursement.application_id = application.application_id
                            LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
                            LEFT JOIN programme ON disbursement.programme_id = programme.programme_id
                            LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                            LEFT JOIN instalment_definition ON disbursement_batch.instalment_definition_id = instalment_definition.instalment_definition_id
                            WHERE disbursement.status <> '10' AND 
                            applicant.f4indexno = '$indexNo' AND 
                            disbursement.disbursed_amount = '$amount' AND 
                            disbursement_batch.academic_year_id = '$academicYear' AND 
                            loan_item.item_code = '$itemCode' AND 
                            instalment_definition.instalment = '$instalment' AND
                            disbursement_batch.learning_institution_id = '$institution'
                            LIMIT 1
                            ";
            $model = Yii::$app->db->createCommand($SQL)->queryAll();
            if (sizeof($model)!=0){
                $id = $model[0]['disbursement_id'];
                $HeaderID = $model[0]['HeaderID'];
                $ReturnModel->status = "MATCH";
                $ReturnModel->disbursement_id = $id;
                $ReturnModel->match_percent = 100;
                $ReturnModel->reasons = "Corresponding Disbursement has been found on header ID $HeaderID";
                if($ReturnModel->save()){
                    $output = true;
                }
            }else{
                $ReturnModel->status = "MISMATCH";
                $ReturnModel->match_percent = 0;
                $ReturnModel->reasons = "NO Corresponding Disbursement found";
                if($ReturnModel->save()){
                    $output = false;
                }
            }
        }


        return $output;

    }



    public static function CrossMatchDeposits($depositID)
    {
        $output = false;

        $indexNo = $academicYear = $itemCode = $instalment = $institution = "";
        $amount = 0;
        $DepositModel = DisbursementDepositStaging::findOne($depositID);
        if (sizeof($DepositModel)!=0){
            $indexNo = $DepositModel->index_number;
            $academicYear = $DepositModel->academic_year;
            $itemCode = self::ITEM_CONVERTER($DepositModel->loan_item,'code');
            $instalment = $DepositModel->instalment_number;
            $institution = DisbursementDepositBatch::findOne($DepositModel->header_id)->institution_id;
            $amount = $DepositModel->amount;
            $SQL = "
                            SELECT 
                            disbursement.disbursement_id, 
                            disbursement.disbursement_batch_id, 
                            disbursement.allocation_batch_id, 
                            disbursement.application_id, 
                            disbursement.programme_id, 
                            disbursement.loan_item_id, 
                            disbursement.version, 
                            disbursement.allocated_amount, 
                            disbursement.disbursed_amount, 
                            disbursement.remaining_balance, 
                            disbursement.disbursed_as, 
                            disbursement.status, 
                            disbursement.status_date, 
                            disbursement.created_at, 
                            disbursement.created_by, 
                            disbursement.sync_id,
                            disbursement_batch.batch_number AS 'HeaderID'
                            FROM disbursement
                            LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id
                            LEFT JOIN application ON disbursement.application_id = application.application_id
                            LEFT JOIN loan_item ON disbursement.loan_item_id = loan_item.loan_item_id
                            LEFT JOIN programme ON disbursement.programme_id = programme.programme_id
                            LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                            LEFT JOIN instalment_definition ON disbursement_batch.instalment_definition_id = instalment_definition.instalment_definition_id
                            WHERE disbursement.status <> '10' AND 
                            applicant.f4indexno = '$indexNo' AND 
                            disbursement.disbursed_amount = '$amount' AND 
                            disbursement_batch.academic_year_id = '$academicYear' AND 
                            loan_item.item_code = '$itemCode' AND 
                            instalment_definition.instalment = '$instalment' AND
                            disbursement_batch.learning_institution_id = '$institution'
                            LIMIT 1
                            ";
            $model = Yii::$app->db->createCommand($SQL)->queryAll();
            if (sizeof($model)!=0){
                $id = $model[0]['disbursement_id'];
                $HeaderID = $model[0]['HeaderID'];
                $DepositModel->status = "MATCH";
                $DepositModel->disbursement_id = $id;
                //$DepositModel->match_percent = 100;
                $DepositModel->matching_remarks = "Corresponding Disbursement has been found on header ID $HeaderID";
                if($DepositModel->save()){
                    $output = true;
                }
            }else{
                $DepositModel->status = "MISMATCH";
                //$DepositModel->match_percent = 0;
                $DepositModel->matching_remarks = "NO Corresponding Disbursement found";
                if($DepositModel->save()){
                    $output = false;
                }
            }
        }


        return $output;

    }



    public static function CrossMatchSuspension($stagingID)
    {
        $output = false;

        $indexNo =  $SQLq = "";
        $user = Yii::$app->user->id;
        $now = date('Y-m-d H:i:s');

        $StagingModel = SuspensionStaging::findOne($stagingID);
        if (sizeof($StagingModel)!=0){
            $indexNo = $StagingModel->index_number;
            $SQL = self::ApplicantQuery($indexNo);
            //programme_code,full_name,current_study_year

            $model = Yii::$app->db->createCommand($SQL)->queryAll();
            if (sizeof($model)!=0){
                $NAMES=strtoupper($model[0]['full_name']);
                $PROGRAMME = $model[0]['programme_code'];
                $status = 1;
                $YOS = $model[0]['current_study_year'];
                $remarks = "Records for Index number: $indexNo has been successfully Matched";
                $SQLq.= 'UPDATE suspension_staging SET 
                            status = "'.$status.'",
                            names = "'.$NAMES.'",
                            programme = "'.$PROGRAMME.'",
                            year_of_study = "'.$YOS.'",
                            matching_remarks = "'.$remarks.'",
                            updated_by = "'.$user.'",
                            updated_at = NOW()
                             WHERE id = "'.$stagingID.'";';

                $output = Yii::$app->db->createCommand($SQLq)->execute();

            }else{

                $status = -1;
                $remarks = "Records for Index number: $indexNo does NOT Exist";
                $SQLq = 'UPDATE suspension_staging SET 
                            status = "'.$status.'",
                            matching_remarks = "'.$remarks.'",
                            updated_by = "'.$user.'",
                            updated_at = NOW()
                             WHERE id = "'.$stagingID.'";';

                $output = Yii::$app->db->createCommand($SQLq)->execute();

            }
        }

        return $output;

    }
    public static function ExecuteSuspension($stagingID)
    {
        $output = false;
        $user = Yii::$app->user->id;
        $StagingModel = SuspensionStaging::findOne($stagingID);
        if (sizeof($StagingModel)!=0){
            $headerID = $StagingModel->header_id;
            $StagingModel->status = 2;
            if ($StagingModel->save()){



                $indexNumber = $StagingModel->index_number;
                $reason = $StagingModel->suspension_reason;
                self::AutoSuspendAll($indexNumber,$user,$reason);
            }

            $activeSQL = "SELECT * FROM suspension_staging WHERE status = '2' AND header_id = '$headerID';";
            $newSQL = "SELECT * FROM suspension_staging WHERE status IN(0,-1) AND header_id = '$headerID';";

            $newModel = Yii::$app->db->createCommand($newSQL)->queryAll();
            $activeModel = Yii::$app->db->createCommand($activeSQL)->queryAll();

            if (sizeof($newModel)==0&&sizeof($activeModel)!=0){
                $SQL="UPDATE suspension_batch SET status='2' WHERE id = '$headerID';";
                Yii::$app->db->createCommand($SQL)->execute();
            }

        }

        return $output;

    }
    public static function ConfirmReturns($returnID)
    {
        $output = false;
        $SQL="";
        $indexNo = $academicYear = $itemCode = $instalment = $institution = "";
        $amount = 0;
        $ReturnModel = DisbursementReturnStaging::findOne($returnID);
        if (sizeof($ReturnModel)!=0){
            $indexNo = $ReturnModel->index_number;
            $academicYear = $ReturnModel->academic_year;
            $amount = $ReturnModel->amount;
            $returnBatchID = $ReturnModel->header_id;
            $returnBatchHeaderID = $ReturnModel->returnBatch->batch_number;
            $sex = $ReturnModel->sex;
            $reasons = $ReturnModel->reasons;
            $remarks = $ReturnModel->remarks;
            $user = Yii::$app->user->id;


            //year_of_study
            $disbursementID = $ReturnModel->disbursement_id;
            $disbursementModel = Disbursement::findOne($disbursementID);
            if (sizeof($disbursementModel)!=0)
            {
                $applicationID = $disbursementModel->application_id;
                $disbursementBatch = $disbursementModel->disbursement_batch_id;
                $YOS = $disbursementModel->year_of_study;
                $PSSN = $ReturnModel->pay_sheet_serial_number;
                $LoanITEM = $disbursementModel->loan_item_id;
                $Instalment = $disbursementModel->disbursementBatch->instalment_definition_id;
                $sync_id = $ReturnModel->disbursement_id;
                $matchPercent = $ReturnModel->match_percent;
                $SQL.="
                INSERT INTO 
                disbursement_return (return_batch_id, index_number, application_id, header_id, disbursement_batch_id, sex, year_of_study, pay_sheet_serial_number, loan_item, instalment_number, amount, remarks, academic_year, status, match_percent, reasons, created_by, created_on, sync_id, disbursement_id)
                VALUES ('$returnBatchID','$indexNo','$applicationID','$returnBatchHeaderID','$disbursementBatch','$sex','$YOS','$PSSN','$LoanITEM','$Instalment','$amount','$remarks','$academicYear','0','$matchPercent','$reasons','$user',NOW(),'$sync_id','$disbursementID');
                ";
                $SQL.="UPDATE disbursement_return_staging SET status='CONFIRMED' WHERE id = '$returnID';";
                //$SQL.="UPDATE disbursement_return_batch SET status='CONFIRMED' WHERE id = '$returnBatchID';";


                Yii::$app->db->createCommand($SQL)->execute();
                $output = true;
            }
        }


        return $output;

    }

    public static function ConfirmDeposits($depositID)
    {
        $output = false;
        $SQL=$SQLfp="";
        $indexNo = $academicYear = $itemCode = $instalment = $institution = "";
        $amount = 0;
        $DepositModel = DisbursementDepositStaging::findOne($depositID);
        if (sizeof($DepositModel)!=0){
            $indexNo = $DepositModel->index_number;
            $academicYear = $DepositModel->academic_year;
            $amount = $DepositModel->amount;
            $header = $DepositModel->header_id;
            $institution = DisbursementDepositBatch::findOne($header)->institution_id;

            $instalment = $DepositModel->instalment_number;
            $semester = self::InstalmentInfo($instalment,'id','semester_id');
            $remarks = $DepositModel->matching_remarks;
            $user = Yii::$app->user->id;
            $sync = $indexNo.'-'.$institution.'-'.$academicYear.'-'.$instalment.'-E';// index, institution academicyear, instalment - E

                $SQLfp.="INSERT IGNORE INTO fingerprint_verification(index_number, academic_year, institution_id, semester_number, method, remarks, operation, created_date, created_by, sync_id) VALUES ('$indexNo','$academicYear','$institution','$semester','FILE','$remarks','ENROLLMENT',NOW(),'$user','$sync');";


                $SQL.="UPDATE disbursement_deposit_batch SET status='UPDATED' WHERE id = '$header';";
                $SQL.="UPDATE disbursement_deposit_staging SET status='CONFIRMED' WHERE id = '$depositID';";


                Yii::$app->db->createCommand($SQLfp)->execute();
                Yii::$app->db->createCommand($SQL)->execute();

                $output = $depositID;

        }


        return $output;

    }

    public static function UpdateReturns($returnID)
    {
        $output = false;
        $user = Yii::$app->user->id;
        $ReturnModel = DisbursementReturn::findOne($returnID);
        if (sizeof($ReturnModel)!=0){
            $amount = $ReturnModel->amount;
            $headerID = $ReturnModel->return_batch_id;
            $ReturnModel->status = 1;
            if ($ReturnModel->save()){


                $output = self::CustomerStatementCreator('RETURNS',$amount,$returnID);
                $indexNumber = $ReturnModel->index_number;
                self::AutoSuspendAll($indexNumber,$user);
            }

            $activeSQL = "SELECT * FROM disbursement_return WHERE status = '1' AND return_batch_id = '$headerID';";
            $newSQL = "SELECT * FROM disbursement_return WHERE status = '0' AND return_batch_id = '$headerID';";

            $newModel = Yii::$app->db->createCommand($newSQL)->queryAll();
            $activeModel = Yii::$app->db->createCommand($activeSQL)->queryAll();

            if (sizeof($newModel)==0&&sizeof($activeModel)!=0){
                $SQL="UPDATE disbursement_return_batch SET status='UPDATED' WHERE id = '$headerID';";
                Yii::$app->db->createCommand($SQL)->execute();
            }

        }

        return $output;

    }

    public static function DisbursementStatementCreator($batchID)
    {
        $output = false;
        $SQL = "SELECT * FROM disbursement WHERE disbursement_batch_id = '$batchID' AND status='7';";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if(sizeof($model)!=0)
        {
            foreach ($model AS $index=>$dataArray)
            {
                $operation="DISBURSEMENT";
                $amount=$dataArray["disbursed_amount"];
                $operation_id = $dataArray["disbursement_id"];
                $output=self::CustomerStatementCreator($operation,$amount,$operation_id);
            }
        }
        return $output;
    }


    public static function SemesterInfo($itemValue,$itemName,$outputField)
    {
        $output = $WHERE ='';
        switch ($itemName){
            case "number":
                $WHERE = "semester.semester_number = '$itemValue'";
                break;

            case "id":
                $WHERE = "semester.semester_id = '$itemValue'";
                break;
        }
        $SQL="SELECT * FROM semester WHERE $WHERE ORDER BY semester_id DESC LIMIT 1;";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0][$outputField];
        }

        return $output;
    }

    public static function InstalmentInfo($itemValue,$itemName,$outputField)
    {
        $output = $WHERE ='';
        switch ($itemName){
            case "number":
                $WHERE = "instalment_definition.instalment = '$itemValue'";
                break;

            case "id":
                $WHERE = "instalment_definition.instalment_definition_id = '$itemValue'";
                break;
        }
        $SQL="SELECT * FROM instalment_definition WHERE $WHERE ORDER BY instalment_definition.instalment_definition_id DESC LIMIT 1;";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0][$outputField];
        }

        return $output;
    }


    public static function GetCont()
    {
        $output = 0;
        $model = Yii::$app->db->createCommand("SELECT COUNT(user_id) AS 'count' FROM user WHERE is_migration_data = '1' AND login_type = '1';")->queryAll();
        if(sizeof($model)!=0){
            $output = $model[0]['count'];
        }
        return $output;
    }


    public static function PasswordHash($user_id,$passwordHash,$authKey){
        $output = false;
        $SQL="UPDATE user SET password_hash = '$passwordHash', auth_key='$authKey', status = '10', login_type = '1' WHERE user_id = '$user_id'";
        $output = Yii::$app->db->createCommand($SQL)->execute();

        $date=strtotime(date("Y-m-d"));
        Yii::$app->db->createCommand("INSERT IGNORE INTO auth_assignment(item_name,user_id,created_at) VALUES('applicant_only',$user_id,$date)")->execute();
        return $output;
    }

    public static function ContPasswordReset()
    {
        $output= $data = array();
        $SQL = "SELECT user_id,username,surname FROM user WHERE is_migration_data = '1' AND login_type = '1';";
        //$contModel = Yii::$app->db->createCommand($SQL)->queryAll();
        $contModel = Yii::$app->db->createCommand($SQL)->query();
        foreach ($contModel as $index=>$dataArray){
            $user_id = $dataArray['user_id'];
            $password = strtoupper($dataArray['surname']);
            $passwordHash = Yii::$app->security->generatePasswordHash($password);
            $authKey = Yii::$app->security->generateRandomString();

            self::PasswordHash($user_id,$passwordHash,$authKey);
            $data=array('password'=>$password,'username'=> $dataArray['username']);
            $output[$user_id]= $data;
        }
        return $output;
    }

    public static function SuspensionStatus($index=null){
        $output = '';
        $statusArray = array('0'=>'PENDING','1'=>'MATCHED','-1'=>'MIS-MATCH','2'=>'UPDATED');
        if ($index===null){
            $output = $statusArray;
        }else{
            if (isset($statusArray[$index])){
                $output = $statusArray[$index];
            }else{
                $output = '';
            }
        }

        return $output;
    }


    public static function SuspensionBatchStudents($batchID)
    {
        $output = 0;
        $SQL = "SELECT COUNT(id) AS 'count' FROM  suspension_staging WHERE header_id = '$batchID';";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output = $model[0]['count'];
        }

        return $output;
    }



    public static function PAY_LIST_PRINT_OUT($batchID){
        /**
         * Created by PhpStorm.
         * User: obedy
         * Date: 10/17/18
         * Time: 2:29 PM
         */
        $amount_total=0;
        $header_id=1;
        $learning_institution_ids_s=0;
        $amount_bottom_array=array();
        $sn = 0;

        $model = DisbursementBatch::findOne($batchID);



        $batchCourse = self::DBatchCourse($batchID);
        $html = '';



        foreach ($batchCourse as $index=>$dataArray){ $courseID = $dataArray['group_id'];

            $html.= '
<html>
<head>
<style>
body {font-family: sans-serif;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0mm solid #000000;
	border-right: 0mm solid #000000;
}
table thead td { background-color: #EEEEEE;
	text-align: center;
	border: 0mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: right;
	font-weight: bolder;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
</style>
</head>

<body>
';








            $html.='<p style=" font-family: \'Courier New\';">';
            $html.='
<h3 style="text-align: center; font-family: \'Courier New\';">HIGHER EDUCATION STUDENTS LOAN BOARD</h3>
      <table width="100%" style=" font-family: \'Courier New\';">
        <tr><th colspan="3" style="text-align: center; font-family: \'Courier New\';">LOAN PAYOUT SCHEDULE FOR '.strtoupper($model->Items).'</th></tr>
        <tr><th colspan="2" style="text-align: center; font-family: \'Courier New\';">Schedule printed on : '. date("l F jS Y H:i:s").'</th> <th>Payout Reference Number : '.$model->batch_number.'</th></tr>
        <tr><th style=" font-family: \'Courier New\';">INSTITUTION: '.strtoupper($model->learningInstitution->institution_code).'</th><th> ACADEMIC YEAR: '. strtoupper($model->academicYear->academic_year).' </th> <th>'. strtoupper($model->Instalment).'</th></tr>
        <tr><th colspan="3" style="text-align: left; font-family: \'Courier New\';">'.$dataArray["group_code"].'</th></tr>
    </table>
';
            $html.='</p>';
            $html.=Module::DynamicPayoutList($batchID,$courseID,$sn);
            $sn+=$dataArray['count'];
            $html.='<pagebreak>';
            //$mpdf->AddPage();
            //$mpdf->WriteHTML($html);



        }


        $html.='<p style=" font-family: \'Courier New\';">';
        $html.='
<h3 style="text-align: center; font-family: \'Courier New\';">HIGHER EDUCATION STUDENTS LOAN BOARD</h3>
      <table width="100%" style=" font-family: \'Courier New\';">
      
        <tr><th colspan="2" style="text-align: center; font-family: \'Courier New\';">Schedule printed on : '. date("l F jS Y H:i:s").'</th> <th>Payout Reference Number : '.$model->batch_number.'</th></tr>
        <tr><th style=" font-family: \'Courier New\';">INSTITUTION: '.strtoupper($model->learningInstitution->institution_code).'</th><th> ACADEMIC YEAR: '. strtoupper($model->academicYear->academic_year).' </th> <th>'. strtoupper($model->Instalment).'</th></tr>
        <tr><th colspan="3" style="text-align: left; font-family: \'Courier New\';">GRAND SUMMARY PER YOS</th></tr>
    </table>
';
        $html.=self::PLS($batchID);
        $html.='</p>';



        $html.= '
</body>
</html>
';


        /*$mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;

        exit;*/

        return $html;
    }


    public static function SIDE_MENU_NOTIFICATIONS($option,$user=null)
    {
        $output = '';
        $count = 0;
        switch ($option)
        {
            case "allocation_batch":
                $SQL="SELECT COUNT(allocation_batch_id) AS 'count' FROM allocation_batch WHERE status='10' AND is_approved = '1' AND (partial_approve_status = '1' OR larc_approve_status = '1'); ";
                $model = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($model)!=0){
                    $count = $model[0]['count'];
                }

                $output='<span class="label label-warning">'.number_format($count,0).'</span>';
                break;


            case "pay_out_list":
                $SQL="SELECT COUNT(disbursement_batch_id) AS 'count' FROM disbursement_batch WHERE is_approved = '0' AND is_submitted = '1'; ";
                $model = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($model)!=0){
                    $count = $model[0]['count'];
                }
                $output='<span class="label label-warning">'.number_format($count,0).'</span>';
                break;

            case "institution_request":
                $SQL="SELECT COUNT(id) AS 'count' FROM institution_fund_request WHERE is_approved = '0' AND request_status < '3'; ";
                $model = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($model)!=0){
                    $count = $model[0]['count'];
                }
                $output='<span class="label label-warning">'.number_format($count,0).'</span>';
                break;


            case "request_approval":
                $SQL="SELECT COUNT(id) AS 'count' FROM institution_fund_request WHERE is_approved = '0' AND request_status = '3'; ";
                $model = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($model)!=0){
                    $count = $model[0]['count'];
                }
                $output='<span class="label label-warning">'.number_format($count,0).'</span>';
                break;



            case "pending_disbursement":
                $SQL="SELECT COUNT(disbursement_batch_id) AS 'count' FROM disbursement_batch WHERE is_submitted = '0' AND is_approved = '0' AND created_by='$user'; ";
                $model = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($model)!=0){
                    $count = $model[0]['count'];
                }
                $output='<span class="label label-warning">'.number_format($count,0).'</span>';
                break;

            case "returns":
                //$SQL="SELECT COUNT(id) AS 'count' FROM disbursement_return_batch WHERE status = 'NEW'  AND created_by='$user'; ";
                $SQL="SELECT COUNT(id) AS 'count' FROM disbursement_return_batch WHERE status = 'NEW'; ";
                $model = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($model)!=0){
                    $count = $model[0]['count'];
                }
                $output='<span class="label label-warning">'.number_format($count,0).'</span>';
                break;


            default:

                break;
        }



        return $output;

    }



    public static function InstitutionsQuery(){
        $SQL = "
            SELECT learning_institution_id as 'id', 
            CONCAT(IFNULL(institution_name,''),' - ',IFNULL(institution_code,'')) AS 'name' 
            FROM  learning_institution 
            WHERE institution_type IN('UNIVERSITY','COLLEGE') AND is_active = '1' 
            ORDER BY institution_name ASC";
        return $SQL;
    }


    public static function ProgrammesQuery(){
        $SQL = "
            SELECT programme_id as 'id', 
            CONCAT(IFNULL(programme_name,''),' - ',IFNULL(programme_code,'')) AS 'name' 
            FROM  programme 
            ORDER BY programme_name ASC";
        return $SQL;
    }
    public static function INSTALMENT_CONVERTER($instalmentNumber,$field)
    {
        $output = '';
        $SQL = "SELECT 
                    instalment_definition_id AS 'id', 
                    instalment AS 'number', 
                    instalment_desc AS 'name', 
                    semester_id AS 'semester_id'
                FROM instalment_definition 
                WHERE instalment = '$instalmentNumber';";

        $model = Yii::$app->db->createCommand($SQL)->queryAll();

        if (sizeof($model)!=0){
            $output = $model[0][$field];
        }

        return $output;
    }


    public static function ITEM_CONVERTER($id,$field)
    {
        $output = '';
        $SQL = "SELECT 
                    loan_item_id AS 'id', 
                    item_code AS 'code', 
                    item_name AS 'name'
                FROM loan_item 
                WHERE loan_item_id = '$id';";

        $model = Yii::$app->db->createCommand($SQL)->queryAll();

        if (sizeof($model)!=0){
            $output = $model[0][$field];
        }

        return $output;
    }

}
