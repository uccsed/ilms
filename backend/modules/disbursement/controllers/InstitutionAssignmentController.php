<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/1/18
 * Time: 3:29 PM
 */

namespace backend\modules\disbursement\controllers;

 use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\disbursement\models\InstitutionAssignment;

class InstitutionAssignmentController extends Controller
{

    public $layout = "main_private";
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $searchModel = new InstitutionAssignment();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new InstitutionAssignment();

        $created_by=Yii::$app->user->id;
        $created_at=date("Y-m-d H:i:s");
        if ($model->load(Yii::$app->request->post())) {

            $officer=$model->officer_id;
            $array=$model->institution_id;
            foreach ($array as $value) {
                $empModel = new InstitutionAssignment();

                $sync = $officer.'-'.$value;
                $empModel->sync_id = $sync;
                $empModel->officer_id = $officer;
                $empModel->institution_id = $value;
                $empModel->assigned_by = $created_by;
                $empModel->assigned_on = $created_at;
                $resultsCount=InstitutionAssignment::find()->where(['officer_id'=>$officer,'institution_id'=>$value,'sync_id'=>$sync])->count();
                if($resultsCount==0){
                    $empModel->save(false);
                }
            }

            $sms="<p>Information successful added</p>";
            Yii::$app->getSession()->setFlash('success', $sms);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->loadModel($id),
        ]);
    }


    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        return $this->redirect(['index']);
    }

    public  function loadModel($id){
        $model = InstitutionAssignment::findOne($id);
        if (sizeof($model)!=0){
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}