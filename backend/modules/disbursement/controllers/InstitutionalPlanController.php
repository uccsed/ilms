<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/11/18
 * Time: 3:14 PM
 */

namespace backend\modules\disbursement\controllers;


use yii\web\Controller;
use backend\modules\disbursement\models\DisbursementPlan;
use backend\modules\allocation\models\LearningInstitution;
use backend\modules\allocation\models\Programme;
use yii\helpers\Json;


use yii\test\InitDbFixture;

use yii\web\BadRequestHttpException;
use Yii;
class InstitutionalPlanController extends Controller
{
    public $layout="main_private";

    public function actionInstitutionProgrammes() {
        $out = [];
        $user=Yii::$app->user->id;
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {

                $institution= $parents[0];

                $SQL = "
            SELECT programme_id as 'id', 
            CONCAT(IFNULL(programme_name,''),' - ',IFNULL(programme_code,'')) AS 'name' 
            FROM  programme 
            WHERE learning_institution_id = '$institution'
            ORDER BY programme_name ASC";


                $out = Yii::$app->db->createCommand($SQL)->queryAll();
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }


    public function actionIndex(){

        $searchModel = new DisbursementPlan();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchInstitutional(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate(){
        if (Yii::$app->request->isAjax) { //Making sure its an ajax request

            /**
             *  percentField:percentField,
            percentData:percentData,
            institution:institution,
            programme:programme,
            loanItem:loanItem,
            academicYear:academicYear,
             */

            $percentField = $_POST['percentField'];
            $percentData = $_POST['percentData'];
            $institution = $_POST['institution'];
            $programme = $_POST['programme'];
            $loanItem = $_POST['loanItem'];
            $academicYear = $_POST['academicYear'];
            $setDefault = $_POST['setDefault'];

            $response = self::ProcessMultipleInputs($institution,$programme,$loanItem,$academicYear,$percentField,$percentData,$setDefault);
            $output='<p style="font-size: large;">'.$response.'</p>';
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => $output,
            ];
        }else{
            /* $model = new DisbursementPlan();
             return $this->render('create',array('model'=>$model));*/
            return $this->render('create');
        }
    }



    /** Processing Multiple Inputs
     * @return string
     * @var integer $institution
     * @var integer $programme
     * @var integer $loanItem
     * @var integer $academicYear
     * @var array $percentField
     * @var array $percentData
     *
     **/
    public static function ProcessMultipleInputs($institution,$programme,$loanItem,$academicYear,$percentField,$percentData,$setDefault)
    {
        $output = 'Operation completed Successfully';

        $insertSQL='';
        $updateSQL='';

        $defaultInsertSQL = '';
        $defaultUpdateSQL = '';


        $now = date('Y-m-d H:i:s');
        $user = Yii::$app->user->id;
        $insertFields='learning_institution,academic_year,programme_id,loan_item_id,date_created,created_by,is_default,year_of_study,semester_number,instalment_id,disbursement_percent,sync_id';
        $defaulInsertFields='academic_year,programme_id,loan_item_id,date_created,created_by,year_of_study,semester_number,instalment_id,disbursement_percent,sync_id';

        $insertInitialValues="'$institution','$academicYear','$programme','$loanItem','$now','$user',$setDefault";
        $insertInitialDFPValues="'$academicYear','$programme','$loanItem','$now','$user'";

        $sync =  '';
        $syncDFP =  '';
        $head ='';
        $headDFP ='';

        $head = $institution.'-'.$programme.'-'.$loanItem.'-'.$academicYear.'_';
        $headDFP = $programme.'-'.$loanItem.'-'.$academicYear.'_';

        $insertCounter = 0;
        $updateCounter = 0;

        $defaultInsertCounter = 0;
        $defaultUpdateCounter = 0;

        foreach($percentField as $index=>$fieldName){
            $sync = '';
            $sync=$head.$fieldName;
            $syncDFP=$headDFP.$fieldName;
            $codedString = str_replace(']','',str_replace('[','',str_replace('][','-',str_replace('percent','',$fieldName))));
            $codedArray=explode('-',$codedString);

            $yearOfStudy = $codedArray[0];
            $Semester = $codedArray[1];
            $Instalment = $codedArray[2];



            isset($percentData[$index])?$percent = $percentData[$index]:$percent=0;
            //$output.=$percent;
            $checkSQL = "SELECT disbursement_plan.disbursement_plan_id AS 'id' FROM disbursement_plan WHERE disbursement_plan.sync_id='$sync' ORDER BY disbursement_plan_id DESC LIMIT 1";
            $checkModel = Yii::$app->db->createCommand($checkSQL)->queryAll();
            if (sizeof($checkModel)!=0){
                $updateCounter++;
                $updateSQL.="UPDATE disbursement_plan SET disbursement_percent='$percent' WHERE sync_id='$sync';";

            } else{
                $insertCounter++;
                $insertSQL.="INSERT INTO disbursement_plan(".$insertFields.")VALUES(".$insertInitialValues.",'".$yearOfStudy."','".$Semester."','".$Instalment."','".$percent."','".$sync."');";
            }

            if ($setDefault==1){

                $checkDFPSQL = "SELECT disbursement_default_plan.plan_id AS 'id' FROM disbursement_default_plan WHERE disbursement_default_plan.sync_id='$syncDFP' ORDER BY plan_id DESC LIMIT 1";
                $checkDFPModel = Yii::$app->db->createCommand($checkDFPSQL)->queryAll();
                if (sizeof($checkDFPModel)!=0){
                    $defaultUpdateCounter++;
                    $defaultUpdateSQL.="UPDATE disbursement_default_plan SET disbursement_percent='$percent' WHERE sync_id='$syncDFP';";

                } else{
                    $defaultInsertCounter++;
                    $defaultInsertSQL.="INSERT INTO disbursement_default_plan(".$defaulInsertFields.")VALUES(".$insertInitialDFPValues.",'".$yearOfStudy."','".$Semester."','".$Instalment."','".$percent."','".$syncDFP."');";
                }

            }


        }

        if ($insertCounter!=0){  Yii::$app->db->createCommand($insertSQL)->execute(); $output.=' <b>'.number_format($insertCounter,0).'</b> NEW percents, '; }
        if ($updateCounter!=0){  Yii::$app->db->createCommand($updateSQL)->execute(); $output.='  <b>'.number_format($updateCounter,0).'</b> AMENDED percents, ';}

        if ($setDefault==1){
            if ($defaultInsertCounter!=0){  Yii::$app->db->createCommand($defaultInsertSQL)->execute(); $output.='  AND <b>'.number_format($defaultInsertCounter,0).'</b> NEW records have been registered to <b> DEFAULT PLAN</b>. ';}
            if ($defaultUpdateCounter!=0){  Yii::$app->db->createCommand($defaultUpdateSQL)->execute(); $output.='  AND <b>'.number_format($defaultUpdateCounter,0).'</b> UPDATES have been applied to  <b>DEFAULT PLAN</b>. ';}

        }
        return $output;
    }

    /**
     * Deletes an existing InstalmentDefinition model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InstalmentDefinition model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InstalmentDefinition the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DisbursementPlan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}