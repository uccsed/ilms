<?php

namespace backend\modules\disbursement\controllers;

use backend\modules\disbursement\Module;
use Yii;
use backend\modules\disbursement\models\DisbursementSuspension;
use backend\modules\disbursement\models\DisbursementSuspensionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use backend\modules\disbursement\models\FingerprintVerification;

/**
 * DisbursementSuspensionController implements the CRUD actions for DisbursementSuspension model.
 */
class DisbursementSuspensionController extends Controller
{
    public $layout="main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DisbursementSuspension models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DisbursementSuspensionSearch();
        $dataProvider = $searchModel->SpecialSearch(Yii::$app->request->queryParams,1);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DisbursementSuspension model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionSearchLonee(){


        if (Yii::$app->request->isAjax) {

            $output = array();
            $string = $_POST['search'];
            $SQL=Module::ApplicantQuery($string);


            $output = Yii::$app->db->createCommand($SQL)->queryAll();


            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['output'=>$output];
        } else throw new BadRequestHttpException;
    }

    /**
     * Creates a new DisbursementSuspension model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DisbursementSuspension();
        $sms='';
        $created_by=Yii::$app->user->id;
        $created_at=date("Y-m-d H:i:s");
        if ($model->load(Yii::$app->request->post())) {


            $array=$model->loan_item_id;


                $fileName='SUSPENSION_'.time().'_'.$created_by;
                $fileDestination = '../attachments/disbursement/';
                $savedName = Module::UploadFile($model,'file',$fileName,$fileDestination);
            $history = $document = array();
            foreach ($array as $value) {


                $sync = $model->application_id.'-'.$value;
                $resultsCount=DisbursementSuspension::findAll(['sync_id'=>$sync]);
                if (sizeof($resultsCount)!=0){
                    foreach ($resultsCount AS $dataObject){
                        $id = $dataObject->id;
                        $suspModel = DisbursementSuspension::findOne($id);
                    }

                }else{
                    $suspModel = new DisbursementSuspension();
                }


                $suspModel->sync_id = $sync;
                $suspModel->application_id = $model->application_id;
                $suspModel->loan_item_id = $value;
                $suspModel->suspension_percent = $model->suspension_percent;
                $suspModel->remaining_percent = $model->remaining_percent;
                $suspModel->status = $model->status;
                $suspModel->status_reason = $model->status_reason;
                $suspModel->remarks = $model->remarks;
                $suspModel->mode = $model->mode;


                if (!empty($suspModel->supporting_document)&& $suspModel->supporting_document!=''){
                    $oldAttachments = (ARRAY)json_decode($suspModel->supporting_document);
                    $attIndex= sizeof($oldAttachments);
                    $oldAttachments[$attIndex]=$savedName;
                    $document=$oldAttachments;
                }else{

                     $document[]=$savedName;
                }


                $currentHistory = array(
                    'suspension_percent'=>$model->suspension_percent,
                    'remaining_percent'=>$model->remaining_percent,
                    'status'=>Module::SuspensionType($model->status),
                    'status_reason'=>$model->status_reason,
                    'remarks'=>$model->remarks,
                    'status_by'=>$created_by,
                    'status_date'=>$created_at,
                    'supporting_document'=>$savedName,
                );

                if (!empty($suspModel->suspension_history)&& $suspModel->suspension_history!=''){
                    $oldHistory = (ARRAY)json_decode($suspModel->suspension_history);
                    $histIndex= sizeof($oldHistory);
                    $oldHistory[$histIndex]=$currentHistory;
                    $history=$oldHistory;
                }else{

                    $history[]=$currentHistory;
                }


                $suspModel->suspension_history = json_encode($history);
                $suspModel->supporting_document = json_encode($document);

                $suspModel->status_by = $created_by;
                $suspModel->status_date = $created_at;
                $resultsCount=DisbursementSuspension::find()->where(['sync_id'=>$sync])->count();
                if($resultsCount==0){
                    $suspModel->save(false);
                }else{
                    $sms.= json_encode($suspModel->getErrors());
                }
            }

            $sms.="<p>Information successful added</p>";
            Yii::$app->getSession()->setFlash('success', $sms);
            if ($model->mode=='1'){
                Yii::$app->db->createCommand("UPDATE application SET student_status = 'STOPED' WHERE application_id = '$model->application_id';")->execute();
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionBulk(){
        ini_set('max_execution_time', 3000000);
        ini_set('memory_limit', '16000000000M');

        $modelImport = new \yii\base\DynamicModel([
            'fileImport'=>'File Import',
        ]);
        $modelImport->addRule(['fileImport'],'required');
        $modelImport->addRule(['fileImport'],'file',['extensions'=>'ods,xls,xlsx'],['maxSize'=>1024*1024]);
        $totalRows = 0;
        $counter = 0;
        $analysis = array('success'=>'','error'=>'');
        if(Yii::$app->request->post()){
            $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport,'fileImport');
            $output = array();
            if($modelImport->fileImport /*&& $modelImport->validate()*/){
                $inputFileType = \PHPExcel_IOFactory::identify($modelImport->fileImport->tempName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($modelImport->fileImport->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                $baseRow =

                $factor= 4;
                $trOK=$trER='';
                $output = array();
                $success = array();
                $error = array();
                $summary = '';
                $totalRows = (sizeof($sheetData) - ($baseRow-1));
                //$output[] = $baseRow;
                while(!empty($sheetData[$baseRow]['B'])){
                    $model = new FingerprintVerification();

                    /** @property integer $id
                     * @property string $index_number
                     * @property integer $academic_year
                     * @property integer $institution_id
                     * @property integer $semester_number
                     * @property string $created_date
                     * @property string $method
                     * @property integer $remarks
                     * @property string $operation*/
                    /*$model->title = (string)$sheetData[$baseRow]['B'];
                    $model->description = (string)$sheetData[$baseRow]['C'];*/

                    $now = date('Y-m-d H:i:s');
                    $user = Yii::$app->user->id;
                    $institution = $_POST['institution']; //Module::InstitutionDecoder('UDOM',false);
                    $academicYear = Module::CurrentAcademicYear();
                    $method = 'FILE'; //FILE | FINGERPRINT | OTHERS
                    //$remarks = '0'; //0 = 'N/A', 1 = 'RETURN', 2 = 'DEATH', 3 = 'DISCONTINUING', 4 = 'POSTPONEMENT', 5 = 'FORGED CERTIFICATES', 6 = 'REJECTING', 7 = 'WRONGLY ALLOCATED', 8 = 'RESUMING STUDIES', 9 = 'HANGING TRANSFER', 10 = 'RESOLVED TRANSFER ISSUES',
                    $operation = 'DROPOUTS'; //ENROLLMENT | DROPOUTS

                    $indexNo = (string)$sheetData[$baseRow]['B'];
                    $names = (string)$sheetData[$baseRow]['C'];
                    $programme = (string)$sheetData[$baseRow]['D'];
                    $YOS = (string)$sheetData[$baseRow]['E'];
                    $remarks = (string)$sheetData[$baseRow]['F'];


                    $check = Module::CheckExistence($indexNo,$institution,$registrationNumber,$academicYear);
                    $counter = ($baseRow-($factor-1));
                    if ($check){
                        $sync = $indexNo.'-'.$institution.'-'.$semester.'-'.$academicYear.'-D';

                        $model->index_number = $indexNo;
                        $model->academic_year = $academicYear;
                        $model->institution_id = $institution;
                        $model->semester_number = $semester;
                        $model->created_date = $now;
                        $model->created_by = $user;
                        $model->method = $method;
                        $model->remarks = $remarks;
                        $model->operation = $operation;
                        $model->sync_id = $sync;
                        Module::AutoSuspendAll($indexNo);

                        if ($model->save()){
                            $remarks = 'Success';
                            $success[$baseRow]=array('dataRow'=>$sheetData,'remarks'=>$remarks);
                            $trOK.="<tr class='text-bold text-green'><td>".$counter."</td><td>$indexNo</td><td>$semester</td><td>$remarks</td></tr>";
                        }else{
                            $remarks = 'FAILED: '.json_encode($model->getErrors());
                            $error[$baseRow]=array('dataRow'=>$sheetData,'remarks'=>$remarks);
                            $trER.="<tr class='text-bold text-red'><td>$counter</td><td>$indexNo</td><td>$semester</td><td>$remarks</td></tr>";
                        }
                    }else{
                        $remarks = 'FAILED: Index Number Not Recognized';
                        $error[$baseRow]=array('dataRow'=>$sheetData,'remarks'=>$remarks);
                        $trER.="<tr class='text-bold text-red'><td>$counter</td><td>$indexNo</td><td>$semester</td><td>$remarks</td></tr>";
                    }

                    $baseRow++;
                }





                if (sizeof($error)!=0){

                    $summaryE = '<p style="font-size: 18px;">'.number_format(sizeof($error)).'/'.number_format($totalRows).' Records FAILED</p>';
                    Yii::$app->getSession()->setFlash('error',$summaryE);
                    $summary.= '<p style="font-size: 18px;">'.$summaryE.'</p>';
                }

                if (sizeof($success)!=0){
                    $summaryS = '<p style="font-size: 14px;">'.number_format(sizeof($success)).'/'.number_format($totalRows).' Records has been successfully Uploaded</p>';
                    Yii::$app->getSession()->setFlash('success',$summaryS);
                    $summary.= '<p style="font-size: 14px;">'.$summaryS.'</p>';
                }


                $analysis = array('success'=>$trOK,'error'=>$trER);
            }else{

                Yii::$app->getSession()->setFlash('error','Import error due to file format issues. Please recheck and upload gain');
            }


            Yii::$app->getSession()->setFlash('info',$summary);

        }

        return $this->render('bulk',[
            'model' => $modelImport,
            'analysis'=>$analysis,
        ]);

    }

    /**
     * Updates an existing DisbursementSuspension model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DisbursementSuspension model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DisbursementSuspension model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DisbursementSuspension the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DisbursementSuspension::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
