<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/3/18
 * Time: 5:34 PM
 */

namespace backend\modules\disbursement\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use backend\modules\allocation\models\LoanItem;
use backend\modules\allocation\models\LoanItemSearch;

class ItemRequestSettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public $layout = "main_private";
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex(){

        $searchModel = new LoanItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate($id){
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash(
                'success', 'Data Successfully Updated!'
            );
            return $this->redirect(['update', 'id' => $model->loan_item_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }


    }

    public function actionUpdate($id){

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash(
                'success', 'Data Successfully Updated!'
            );
            return $this->redirect(['update', 'id' => $model->loan_item_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Instalment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Instalment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LoanItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}