<?php

namespace backend\modules\disbursement\controllers;

use backend\modules\disbursement\Module;
use Yii;
use backend\modules\disbursement\models\DisbursementReturnBatch;
use backend\modules\disbursement\models\DisbursementReturnBatchSearch;
use backend\modules\disbursement\models\DisbursementReturn;
use backend\modules\disbursement\models\DisbursementReturnStaging;
use backend\modules\disbursement\models\DisbursementReturnStagingSearch;
use backend\modules\disbursement\models\DisbursementReturnSearch;


use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * DisbursementReturnBatchController implements the CRUD actions for DisbursementReturnBatch model.
 */
class DisbursementReturnBatchController extends Controller
{
    public $layout="main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DisbursementReturnBatch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DisbursementReturnBatchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCrossMatch(){
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {
            $results = array();
            $headerID = $_POST['headerID'];
            $SQL = "SELECT id FROM disbursement_return_staging WHERE header_id = '$headerID' AND status IN('NEW','MISMATCH')";
            $model = Yii::$app->db->createCommand($SQL)->queryAll();
            if (sizeof($model)!=0){
                foreach ($model as $index=>$dataArray)
                {
                    $id = $dataArray['id'];
                    if (Module::CrossMatchReturns($id)){
                       $results[] = $id;
                    }
                }
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results'=>$results),
            ];
        } else throw new BadRequestHttpException;
    }


    public function actionConfirmation(){
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {
            $results = array();
            $headerID = $_POST['headerID'];
            $SQL = "SELECT id FROM disbursement_return_staging WHERE header_id = '$headerID' AND status ='MATCH' AND disbursement_id IS NOT NULL AND disbursement_id<>'0' ";
            $model = Yii::$app->db->createCommand($SQL)->queryAll();
            if (sizeof($model)!=0){
                foreach ($model as $index=>$dataArray)
                {
                    $id = $dataArray['id'];
                    if (Module::ConfirmReturns($id)){
                        $results[] = $id;
                    }
                }
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results'=>$results),
            ];
        } else throw new BadRequestHttpException;
    }


    public function actionUpdateReturns(){
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {
            $results = array();
            $headerID = $_POST['headerID'];
            $SQL = "SELECT id FROM disbursement_return WHERE return_batch_id = '$headerID' AND status ='0' AND disbursement_id IS NOT NULL AND disbursement_id<>'0'";
            $model = Yii::$app->db->createCommand($SQL)->queryAll();
            if (sizeof($model)!=0){
                foreach ($model as $index=>$dataArray)
                {
                    $id = $dataArray['id'];
                    if (Module::UpdateReturns($id)){
                        $results[] = $id;
                    }
                }
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results'=>$results),
            ];
        } else throw new BadRequestHttpException;
    }

    public function actionStaging($batchID,$status)
    {
        $this->layout='default_main';
        $searchModel = new DisbursementReturnStagingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$batchID,$status);

        return $this->render('staging_returns', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'batchID' => $batchID,
            'status' => $status,
        ]);
    }

    public function actionConfirmed($batchID,$status)
    {
        $this->layout='default_main';
        $searchModel = new DisbursementReturnSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$batchID,$status);

        return $this->render('confirmed_returns', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'batchID' => $batchID,
            'status' => $status,
        ]);
    }



    /**
     * Displays a single DisbursementReturnBatch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionConfirm()
    {
        $model = new DisbursementReturnBatch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('confirm', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Creates a new DisbursementReturnBatch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        $model = new DisbursementReturnBatch();
        $modelImport = new \yii\base\DynamicModel([
            'fileImport'=>'File Import',
        ]);
        $modelImport->addRule(['fileImport'],'required');
        $modelImport->addRule(['fileImport'],'file',['extensions'=>'ods,xls,xlsx'],['maxSize'=>1024*1024]);
        if ($model->load(Yii::$app->request->post())) {
            $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport,'fileImport');
            $user = Yii::$app->user->id;
            $now = date('Y-m-d H:i:s');
            $model->batch_number = Module::ReturnBatchNumberCreator();
            $model->created_by = $user;
            $model->created_on = $now;
            $model->status = 'NEW';


            if ($modelImport->fileImport){
                $inputFileType = \PHPExcel_IOFactory::identify($modelImport->fileImport->tempName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($modelImport->fileImport->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                $baseRow = $factor= 2;
                $totalRows = (sizeof($sheetData) - ($baseRow-1));
                /*print_r($totalRows);
                exit();*/
                if ($totalRows!==0){
                    if ($model->save()){
                       $output =  Module::UploadReturns($sheetData,$baseRow,$model->id);
                       // $output = array('new'=>$totalRows,'updates'=>intval($totalRows/3));
                        Yii::$app->getSession()->setFlash('success',"<b>BULK RETURN # $model->batch_number</b> has : <b>".number_format($output['new'],0)." NEW</b> records & <b>".number_format($output['updates'],0)."</b> records have been <b>UPDATED</b> under the header ID <b>$model->id</b>");
                        return $this->redirect(['view', 'id' => $model->id]);
                    }else{
                        Yii::$app->getSession()->setFlash('error',"Save Error".json_encode($model->getErrors()));

                    }
                }else{
                    Yii::$app->getSession()->setFlash('error',"You've just uploaded an empty sheet... Please make sure the sheet is not empty");
                }

            }else{
                Yii::$app->getSession()->setFlash('error','Something is wrong with the file imported... Please check again and try to re-import again');
            }


        } else {
            return $this->render('create', [
                'model' => $model,
                'modelImport'=>$modelImport,
            ]);
        }
    }

    /**
     * Updates an existing DisbursementReturnBatch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateStaging($id)
    {
        $this->layout='default_main';
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        $model = $this->findStagingModel($id);
        if ($model->load(Yii::$app->request->post())) {
            //$output =
            if ($model->save()){
                Yii::$app->getSession()->setFlash('success',"A Record with an index number <b>$model->index_number</b> has been Updated Successfully");
                return $this->redirect(['staging', 'batchID' => $model->header_id,'status'=>$model->status]);

            }else{
                Yii::$app->getSession()->setFlash('error',"Save Error".json_encode($model->getErrors()));
            }

        }
        return $this->render('update_staging', [
            'model' => $model,
        ]);
    }
    public function actionUpdate($id)
    {


        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        $model = $this->findModel($id);
        $modelImport = new \yii\base\DynamicModel([
            'fileImport'=>'File Import',
        ]);
        $modelImport->addRule(['fileImport'],'required');
        $modelImport->addRule(['fileImport'],'file',['extensions'=>'ods,xls,xlsx'],['maxSize'=>1024*1024]);
        if ($model->load(Yii::$app->request->post())) {
            $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport,'fileImport');
            /*$user = Yii::$app->user->id;
            $now = date('Y-m-d H:i:s');
            $model->batch_number = Module::ReturnBatchNumberCreator();
            $model->created_by = $user;
            $model->created_on = $now;
            $model->status = 'NEW';*/


            if ($modelImport->fileImport){
                $inputFileType = \PHPExcel_IOFactory::identify($modelImport->fileImport->tempName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($modelImport->fileImport->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                $baseRow = $factor= 2;
                $totalRows = (sizeof($sheetData) - ($baseRow-1));

                if ($totalRows!==0){
                    if ($model->save()){
                        $output =  Module::UploadReturns($sheetData,$baseRow,$model->id);
                        
                       // $output = array('new'=>$totalRows,'updates'=>intval($totalRows/3));
                        Yii::$app->getSession()->setFlash('success',"<b>BULK RETURN # $model->batch_number</b> has : <b>".number_format($output['new'],0)." NEW</b> records & <b>".number_format($output['updates'],0)."</b> records have been <b>UPDATED</b> under the header ID <b>$model->id</b>");
                        return $this->redirect(['view', 'id' => $model->id]);
                    }else{
                        Yii::$app->getSession()->setFlash('error',"Save Error".json_encode($model->getErrors()));

                    }
                }else{
                    Yii::$app->getSession()->setFlash('error',"You've just uploaded an empty sheet... Please make sure the sheet is not empty");
                }

            }else{
                Yii::$app->getSession()->setFlash('error','Something is wrong with the file imported... Please check again and try to re-import again');
            }


        } else {
            return $this->render('update', [
                'model' => $model,
                'modelImport'=>$modelImport,
            ]);
        }
    }

    /**
     * Deletes an existing DisbursementReturnBatch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $SQL="DELETE FROM disbursement_return_staging WHERE header_id='$id';";
        $SQL.="DELETE FROM disbursement_return WHERE return_batch_id='$id';";
        Yii::$app->db->createCommand($SQL)->execute();

        $model->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteStaging($id)
    {
        $model=$this->findStagingModel($id);
        $batch_id = $model->header_id;
        $model->delete();

        //return $this->redirect(['index']);
        return $this->redirect(['view', 'id' => $batch_id]);
    }

    /**
     * Finds the DisbursementReturnBatch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DisbursementReturnBatch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DisbursementReturnBatch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findStagingModel($id)
    {
        if (($model = DisbursementReturnStaging::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
