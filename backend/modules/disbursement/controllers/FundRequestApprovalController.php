<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/28/18
 * Time: 10:01 AM
 */

namespace backend\modules\disbursement\controllers;


use backend\modules\disbursement\Module;
use Yii;
use backend\modules\disbursement\models\InstitutionFundRequest;
use backend\modules\disbursement\models\InstitutionFundRequestSearch;
use backend\modules\disbursement\models\InstitutionPaylistSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class FundRequestApprovalController extends Controller
{
    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AdjustmentReason models.
     * @return mixed
     */
    public function actionIndex()
    {
        //0=>'NEW', 1=>'SUBMITTED',2=>'REVIEWED', 3=>'APPROVED', 4=>'REJECTED',6=>'ACCEPTED', 7=>'DISBURSED', 8=> 'RECEIVED', 9=>'WAITING_RETURN' , 10=>'RETURNED'
        $searchModel = new InstitutionFundRequestSearch();
        $dataProvider = $searchModel->searchSpecial(Yii::$app->request->queryParams,3);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AdjustmentReason model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model =  $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {

            //acceptance, acceptance_date, acceptance_by, acceptance_remarks, official_document
            $now = date("Y-m-d H:i:s");
            $user = Yii::$app->user->id;
            $model->request_status = $_POST['InstitutionFundRequest']['acceptance'];
            $model->status_date = $now;
            $model->status_by = $user;

            $model->request_status = $_POST['InstitutionFundRequest']['acceptance'];
            $model->acceptance_date = $now;
            $model->acceptance_by = $user;

            $fileName='FUND_REQUEST_'.time().'_'.$user;
            $fileDestination = '../attachments/disbursement/';
            $savedName = Module::UploadFile($model,'file',$fileName,$fileDestination);
            $document=array();

            if (!empty($model->official_document)&& $model->official_document!=''){
                $oldAttachments = (ARRAY)json_decode($model->official_document);
                $attIndex= sizeof($oldAttachments);
                $oldAttachments[$attIndex]=$savedName;
                $document=$oldAttachments;
            }else{

                $document[]=$savedName;
            }

            $model->official_document = json_encode($document);

            if ($model->save()) {
                if ($model->acceptance=='6'){ //Accepted
                    $sms="<p>Request has been successful APPROVED for proceeding with Disbursement Process</p>";
                    $alert = 'success';
                }else{
                    $sms="<p>Request has been REJECTED from proceeding with Disbursement Process</p>";
                    $alert = 'danger';
                }
                Module::UpdatePaylistStatus($model->id,$model->acceptance);
                Yii::$app->getSession()->setFlash($alert, $sms);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }else{
            return $this->render('view', [
                'model' =>$model,
            ]);
        }
    }

    /**
     * Creates a new AdjustmentReason model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InstitutionFundRequest();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AdjustmentReason model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AdjustmentReason model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AdjustmentReason model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdjustmentReason the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InstitutionFundRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}