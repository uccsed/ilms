<?php

namespace backend\modules\disbursement\controllers;
use backend\modules\allocation\models\Allocation;
use Yii;
//use yii\web\Controller;
use backend\modules\allocation\models\AllocationBatch;
use backend\modules\allocation\models\AllocationBatchSearch;
use backend\modules\allocation\models\AllocationSearch;
use backend\modules\application\models\ApplicationSearch;
use backend\modules\application\models\ApplicantSearch;
use common\components\Controller;
use backend\modules\disbursement\Module;
use backend\modules\disbursement\models\AllocationBatchSearcher;
/**
 * Default controller for the `disbursement` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public $layout = "main_private";
    public function actionIndex()
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        return $this->render('index');
    }

    public function actionDashboardStatistics(){
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {

            $results = array();

            $academicYear = $_POST['academic_year'];
            $option = $_POST['option'];



            $results = Module::DashboardStatistics($academicYear,$option);

            // $results = Yii::$app->db->createCommand($rSQL)->queryAll();


            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results' => $results),
            ];
        } else throw new BadRequestHttpException;
    }


    public function actionAllocationBatch()
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
       // $searchModel = new AllocationBatchSearch();
        $searchModel = new AllocationBatchSearcher();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,1);


        return $this->render('allocation-batch', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    
    }
     public function actionViewbatch($id)
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        return $this->render('allocationbatchprofile', [
            'model' => $this->findModelbatch($id),
        ]);
    }
    public function actionAllocatedStudent($id)
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $this->layout="default_main"; 
        //$searchModel = new AllocationSearch();//'pagination' => false,
        $searchModel = new AllocationBatchSearcher();//'pagination' => false,
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);
        $dataProvider = $searchModel->searcher(Yii::$app->request->queryParams,$id);
        $dataProvider->pagination->pageSize=0;
        //$dataProvider->pagination  = false;


        return $this->render('allocatedstudent', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
           'id'=>$id,
        ]);
    
    }
    protected function findModelbatch($id)
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        if (($model = AllocationBatch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
 public function actionNotification()
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $searchModel = new ApplicationSearch();
        $dataProvider = $searchModel->searchbank(Yii::$app->request->queryParams);
        //return $this->render('student_bank_account',
        return $this->render('notification',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        //return $this->render('notification');
    }
  public function actionStudentBankAccount()
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
         $this->layout="default_main"; 
        $searchModel = new ApplicationSearch();
        $dataProvider = $searchModel->searchbank(Yii::$app->request->queryParams);
  return $this->render('student_bank_account',
                             [
                             'searchModel' => $searchModel,
                             'dataProvider' => $dataProvider,
                           ]);
    }
   public function actionApproveStatus($id,$status)
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
          $model= \backend\modules\allocation\models\AllocationBatch::findOne(["allocation_batch_id"=>$id]);
            $model->disburse_status=$status;
             
         
                          if($status==1){
                            $comment=" Verified" ;
                            $comment_all="Allocation Batch Ok";
                          }
                          else{
                        $comment=" UnVerified" ;  
                        $comment_all="Allocation Batch Mismatch with Original Batch";
                          }
                  $model->disburse_comment=$comment_all;
                  $model->update();
                  //print_r($model);
                  //exit();
             Yii::$app->getSession()->setFlash(
                                'success', "Data Successfully   $comment!"
                        );
       return $this->redirect(['allocation-batch','id'=>$id]);
    }
 public function actionApplicantProfile()
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        // $this->layout="default_main"; 
        $searchModel = new ApplicantSearch();
        $dataProvider = $searchModel->searchapplicant(Yii::$app->request->queryParams);
  return $this->render('applicant_list',
                             [
                             'searchModel' => $searchModel,
                             'dataProvider' => $dataProvider,
                           ]);
    }  
    public function actionViewprofile($id)
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
       
        return $this->render('applicantprofile', [
            'model' => $this->findModelapplicant($id),
        ]);
    }
    protected function findModelapplicant($id)
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        if (($model = \backend\modules\application\models\Applicant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
  public function actionUpdateloanee($id)
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
         //$this->layout="default_main";
        
       $model =\backend\modules\application\models\Application::findone($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                   Yii::$app->getSession()->setFlash(
                                'success', 'Data Successfuly Updated!'
                        );
            return $this->redirect(['updateloanee', 'id' => $model->application_id]);
        } else {
       return $this->render('updateloan', [
            'model' =>  $model,
        ]);
        }
    }
  public function actionMyallocatation($id)
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $this->layout="default_main"; 
        $searchModel = new \backend\modules\allocation\models\AllocationSearch();
        $dataProvider = $searchModel->searchmyallocation(Yii::$app->request->queryParams,$id);
   return $this->render('myallocatation',
                             [
                             'searchModel' => $searchModel,
                             'dataProvider' => $dataProvider,
                           ]);
    }




    public function actionAllocationBatchDetailsDownload($allocation_batch_id) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $model=  AllocationBatch::findOne($allocation_batch_id);
        $modelall= Yii::$app->db->createCommand("SELECT current_study_year, programme_cost,ownership,registration_number ,`firstname`, `middlename`,apl.application_id, `surname`,institution_name,institution_code,u.sex as sex,current_study_year, `f4indexno`,apl.`programme_id`,SUM(`allocated_amount`) as amount_total,`allocation_batch_id`,programme_name,programme_code,pr.learning_institution_id ,`cluster_name`,li.`ownership`,`needness`, `myfactor`, `ability`, `fee_factor`, `student_fee` FROM `user` u join applicant ap on u.`user_id`=ap.`user_id` join application apl on apl.`applicant_id`=ap.`applicant_id` join allocation alls on alls.`application_id`=apl.`application_id` join programme pr on pr.`programme_id`=apl.`programme_id` join cluster_programme clp on clp.programme_id=pr.programme_id join cluster_definition clud on clud.`cluster_definition_id`=clp.cluster_definition_id join `learning_institution` li on li.`learning_institution_id`=pr.`learning_institution_id` WHERE allocation_batch_id='{$allocation_batch_id}' group by alls.`application_id`,`allocation_batch_id` order by pr.learning_institution_id;
")->queryAll();
        $content=$this->render('allocation-batch-detail_excel', [
            'model' =>$model,
            'modelall' =>$modelall,

        ]);
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        return $pdf->render();
    }

    public function actionAllocationBatchDownload($allocation_batch_id) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $modelall=  AllocationBatch::findOne($allocation_batch_id);
        $model= Yii::$app->db->createCommand("SELECT `firstname`, `middlename`,apl.application_id, `surname`,institution_name,institution_code,u.sex,current_study_year, `f4indexno`,apl.`programme_id`,SUM(`allocated_amount`) as amount_total,`allocation_batch_id`,programme_name,programme_code,pr.learning_institution_id FROM `user` u join applicant ap "
            . "  on u.`user_id`=ap.`user_id` join application apl "
            . "  on apl.`applicant_id`=ap.`applicant_id` "
            . "  join allocation alls on alls.`application_id`=apl.`application_id` join programme pr on pr.`programme_id`=apl.`programme_id` "
            . "   join `learning_institution` li on li.`learning_institution_id`=pr.`learning_institution_id`"
            . "  WHERE allocation_batch_id='{$allocation_batch_id}' group by alls.`application_id`,`allocation_batch_id` order by pr.learning_institution_id")->queryAll();
        $content=$this->render('allocation_batch_pdf', [
            'model' =>$model,
            'modelall' =>$modelall,

        ]);
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        return $pdf->render();
    }
}
