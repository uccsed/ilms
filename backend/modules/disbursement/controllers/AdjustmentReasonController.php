<?php

namespace backend\modules\disbursement\controllers;

use Yii;
use backend\modules\disbursement\models\AdjustmentReason;
use backend\modules\disbursement\models\AdjustmentReasonSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdjustmentReasonController implements the CRUD actions for AdjustmentReason model.
 */
class AdjustmentReasonController extends Controller
{
    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AdjustmentReason models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdjustmentReasonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AdjustmentReason model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AdjustmentReason model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AdjustmentReason();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            $new_data=\yii\helpers\Json::encode($model->attributes);
            $model_logs=\common\models\base\Logs::CreateLogall($model->id,$old_data,$new_data,$model::tableName(),"CREATE",1);
                //end for logs
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AdjustmentReason model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        // here for logs
        $old_data=\yii\helpers\Json::encode($model->oldAttributes);
        //end for logs
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            // here for logs
            $new_data=\yii\helpers\Json::encode($model->attributes);
            $model_logs=\common\models\base\Logs::CreateLogall($model->id,$old_data,$new_data,$model::tableName(),"UPDATE",1);
            //end for logs

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AdjustmentReason model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AdjustmentReason model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdjustmentReason the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdjustmentReason::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
