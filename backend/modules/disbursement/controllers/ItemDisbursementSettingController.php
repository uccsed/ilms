<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/4/18
 * Time: 7:14 AM
 */

namespace backend\modules\disbursement\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\disbursement\models\ItemDisbursementSetting;

class ItemDisbursementSettingController extends Controller
{
    public $layout = "main_private";
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $searchModel = new ItemDisbursementSetting();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new ItemDisbursementSetting();

        $created_by=Yii::$app->user->id;
        $created_at=date("Y-m-d H:i:s");
        if ($model->load(Yii::$app->request->post())) {

            $level=$model->study_level;
            $category=$model->study_category;
            $receiver = $model->disbursed_to;
            $array=$model->loan_item_id;
            foreach ($array as $value) {
                $empModel = new ItemDisbursementSetting();

               // $sync = $value.'-'.$level.'-'.$category.'-'.$receiver; //if one item can be disbursed to both institution and student
                $sync = $value.'-'.$level.'-'.$category;

                $empModel->sync_id = $sync;
                $empModel->study_category = $category;
                $empModel->study_level = $level;
                $empModel->disbursed_to = $receiver;
                $empModel->loan_item_id = $value;

                $resultsCount=ItemDisbursementSetting::find()->where(['sync_id'=>$sync])->count();
                if($resultsCount==0){
                    $empModel->save(false);
                    $sms="<p>Information successful added</p>";
                    $alert='success';
                }else{
                    $sms="<p>Information was skipped due to duplicates detection</p>";
                    $alert='danger';
                }
            }


            Yii::$app->getSession()->setFlash($alert, $sms);
            return $this->redirect(['index']);
        }
            return $this->render('create', [
                'model' => $model,
            ]);

    }


    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $level=$model->study_level;
            $category=$model->study_category;
            $receiver = $model->disbursed_to;
            $loanItem=$model->loan_item_id;
            $sync = $loanItem.'-'.$level.'-'.$category;
           // $sync = $loanItem.'-'.$level.'-'.$category.'-'.$receiver; //if one item can be disbursed to both institution and student
            $model->sync_id = $sync;
            $resultsCount=ItemDisbursementSetting::find()->where(['sync_id'=>$sync,'id'=>$model->id])->count();
            if($resultsCount!=0) {
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }

        }
            return $this->render('update', [
                'model' => $model,
            ]);



    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->loadModel($id),
        ]);
    }


    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        return $this->redirect(['index']);
    }

    public  function loadModel($id){
        $model = ItemDisbursementSetting::findOne($id);
        if (sizeof($model)!=0){
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}