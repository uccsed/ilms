<?php

namespace backend\modules\disbursement\controllers;

use Yii;
use backend\modules\disbursement\models\OverseasSettings;
use backend\modules\disbursement\models\OverseasSettingsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OverseasSettingsController implements the CRUD actions for OverseasSettings model.
 */
class OverseasSettingsController extends Controller
{
    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OverseasSettings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OverseasSettingsSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->GroupedSearch(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OverseasSettings model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OverseasSettings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OverseasSettings();

        $user=Yii::$app->user->id;
        $now=date("Y-m-d H:i:s");
        if ($model->load(Yii::$app->request->post())) {

            $academicYear=$model->academic_year;
            $percent=$model->disbursement_percent;

            $instalment=$model->instalment;

            $YOSArray=$model->year_of_study;
            $arrayItem=$model->loan_item;
            $arrayCountry=$model->country_id;
            foreach ($arrayCountry as $country) {
                foreach ($YOSArray as $YOS) {
                    foreach ($arrayItem as $item) {
                        $overseasModel = new OverseasSettings();

                        $sync = $country.'-'.$academicYear.'-'.$YOS.'-'.$item.'-'.$instalment;
                        $overseasModel->sync_id = $sync;
                        $overseasModel->academic_year = $academicYear;
                        $overseasModel->year_of_study = $YOS;
                        $overseasModel->country_id = $country;
                        $overseasModel->instalment = $instalment;
                        $overseasModel->loan_item = $item;
                        $overseasModel->disbursement_percent = $percent;
                        $overseasModel->created_at = $now;
                        $overseasModel->created_by = $user;
                        $overseasModel->updated_by = $user;
                        $overseasModel->updated_at = $now;
                        $resultsCount=OverseasSettings::find()->where(['sync_id'=>$sync])->count();
                        if($resultsCount==0){
                            $overseasModel->save(false);
                        }
                    }
                }
            }

            $sms="<p>Information successful added</p>";
            Yii::$app->getSession()->setFlash('success', $sms);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OverseasSettings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OverseasSettings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OverseasSettings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OverseasSettings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OverseasSettings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
