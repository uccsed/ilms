<?php

namespace backend\modules\disbursement\controllers;

use Yii;
use backend\modules\disbursement\models\SuspensionBatch;
use backend\modules\disbursement\models\SuspensionBatchSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\disbursement\Module;

/**
 * SuspensionBatchController implements the CRUD actions for SuspensionBatch model.
 */
class SuspensionBatchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SuspensionBatch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SuspensionBatchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SuspensionBatch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SuspensionBatch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SuspensionBatch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SuspensionBatch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }





    public function actionCrossMatch(){
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {
            $results = array();
            $headerID = $_POST['headerID'];
            $SQL = "SELECT id FROM suspension_staging WHERE header_id = '$headerID' AND status IN(0,-1)";
            $model = Yii::$app->db->createCommand($SQL)->queryAll();
            if (sizeof($model)!=0){
                foreach ($model as $index=>$dataArray)
                {
                    $id = $dataArray['id'];
                    if (Module::CrossMatchSuspension($id)){
                        $results[] = $id;
                    }
                }
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results'=>$results),
            ];
        } else throw new BadRequestHttpException;
    }



    public function actionConfirmation(){
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {
            $results = array();
            $headerID = $_POST['headerID'];
            $SQL = "SELECT id FROM suspension_staging WHERE header_id = '$headerID' AND status ='1'";
            $model = Yii::$app->db->createCommand($SQL)->queryAll();
            if (sizeof($model)!=0){
                foreach ($model as $index=>$dataArray)
                {
                    $id = $dataArray['id'];
                    if (Module::ExecuteSuspension($id)){
                        $results[] = $id;
                    }
                }
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results'=>$results),
            ];
        } else throw new BadRequestHttpException;
    }










    /**
     * Deletes an existing SuspensionBatch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SuspensionBatch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SuspensionBatch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SuspensionBatch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
