<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/20/18
 * Time: 6:23 PM
 */

namespace backend\modules\disbursement\controllers;

use backend\modules\disbursement\Module;
use Yii;
use backend\modules\disbursement\models\DisbursementSuspension;
use backend\modules\disbursement\models\DisbursementSuspensionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


class DisbursementUpliftingController extends Controller
{
    public $layout="main_private";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DisbursementSuspension models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DisbursementSuspensionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,0);

        return $this->render('../disbursement-suspension/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DisbursementSuspension model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('../disbursement-suspension/view', [
            'model' => $this->findModel($id),
        ]);
    }
    /**
     * Creates a new DisbursementSuspension model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionSearchLonee(){


        if (Yii::$app->request->isAjax) {

            $output = array();
            $string = $_POST['search'];
            $SQL="
                   SELECT 
                    applicant.f4indexno AS 'index_number',
                    user.phone_number AS 'phone_number',
                    user.email_address AS 'email_address',
                    disbursement_suspension.application_id AS 'application_id',
                    application.programme_id AS 'programme_id',
                    programme.learning_institution_id AS 'institution_id',
                    application.registration_number AS 'registration_number',
                    CONCAT(user.firstname,' ',user.surname) AS 'full_name',
                    CASE WHEN user.sex = 'M' THEN 'MALE' ELSE 'FEMALE' END AS 'Sex',
                    DATE_FORMAT(applicant.date_of_birth,'%d/%m/%Y') AS 'DOB',
                    applicant_attachment.attachment_path AS 'photo',
                    programme.programme_name AS 'programme_name',
                    programme.programme_code AS 'programme_code',
                    programme.years_of_study AS 'years_of_study',
                    learning_institution.institution_name AS 'Institution',
                    learning_institution.institution_code AS 'institution_code',
                    loan_item.item_name,
                    allocation.loan_item_id,
                    application.current_study_year,
                    academic_year.academic_year,
                    allocation.allocation_batch_id,
                    SUM(allocation.allocated_amount) AS 'allocated_amount',
                    disbursement_suspension.suspension_percent AS 'suspension_percent',
                    (SUM(allocation.allocated_amount)*(disbursement_suspension.suspension_percent/100)) AS 'suspended_amount',
                    disbursement_suspension.remaining_percent AS 'remaining_percent',
                    (SUM(allocation.allocated_amount)*(disbursement_suspension.remaining_percent/100)) AS 'remaining_amount',
                    DATE_FORMAT(disbursement_suspension.status_date,'%d/%m/%Y') AS 'suspension_date',
                    suspension_reason.name AS 'suspension_reason'
                    FROM disbursement_suspension
                    LEFT JOIN suspension_reason ON disbursement_suspension.status_reason = suspension_reason.id
                    LEFT JOIN application ON disbursement_suspension.application_id = application.application_id
                     LEFT JOIN applicant_attachment ON application.applicant_id = applicant_attachment.application_id AND applicant_attachment.attachment_definition_id = '3'
                   
                    LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                    LEFT JOIN user ON applicant.user_id = user.user_id
                    LEFT JOIN loan_item ON disbursement_suspension.loan_item_id = loan_item.loan_item_id
                    LEFT JOIN programme ON application.programme_id = programme.programme_id
                    LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                    
                    LEFT JOIN allocation ON application.application_id = allocation.application_id
                    LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                    LEFT JOIN academic_year ON allocation_batch.academic_year_id = academic_year.academic_year_id
                    
                    
                    WHERE applicant.f4indexno = '$string' AND disbursement_suspension.remaining_percent <> 100 AND disbursement_suspension.status = '1'
                    GROUP BY disbursement_suspension.id
                    ORDER BY applicant.f4indexno ASC,academic_year.academic_year_id ASC,loan_item.item_name ASC
            ";


            $output = Yii::$app->db->createCommand($SQL)->queryAll();


            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['output'=>$output];
        } else throw new BadRequestHttpException;
    }

    public function actionCreate()
    {
        $model = new DisbursementSuspension();
        $sms=''; $alert = 'info';
        $created_by=Yii::$app->user->id;
        $created_at=date("Y-m-d H:i:s");
        if ($model->load(Yii::$app->request->post())) {


            $array=$model->loan_item_id;
            Module::AutoUpliftAll($model->application_id);

            $fileName='UPLIFTING_'.time().'_'.$created_by;
            $fileDestination = '../attachments/disbursement/';
            $savedName = Module::UploadFile($model,'file',$fileName,$fileDestination);
            $history = $document = array();
            foreach ($array as $value) {


                $sync = $model->application_id.'-'.$value;
                $resultsCount=DisbursementSuspension::findAll(['sync_id'=>$sync]);
                if (sizeof($resultsCount)!=0)
                {
                    foreach ($resultsCount AS $dataObject){
                        $id = $dataObject->id;
                        $suspModel = DisbursementSuspension::findOne($id);
                    }




                    $suspModel->sync_id = $sync;
                    $suspModel->application_id = $model->application_id;
                    $suspModel->loan_item_id = $value;
                    $suspModel->suspension_percent = $model->suspension_percent;
                    $suspModel->remaining_percent = $model->remaining_percent;
                    $suspModel->status = $model->status;
                    $suspModel->status_reason = $model->status_reason;
                    $suspModel->remarks = $model->remarks;
                    $suspModel->mode = $model->mode;


                    if (!empty($suspModel->supporting_document)&& $suspModel->supporting_document!=''){
                        $oldAttachments = (ARRAY)json_decode($suspModel->supporting_document);
                        $attIndex= sizeof($oldAttachments);
                        $oldAttachments[$attIndex]=$savedName;
                        $document=$oldAttachments;
                    }else{

                        $document[]=array($savedName);
                    }


                    $currentHistory = array(
                        'suspension_percent'=>$model->suspension_percent,
                        'remaining_percent'=>$model->remaining_percent,
                        'status'=>Module::SuspensionType($model->status),
                        'status_reason'=>$model->status_reason,
                        'remarks'=>$model->remarks,
                        'status_by'=>$created_by,
                        'status_date'=>$created_at,
                        'supporting_document'=>$savedName,
                    );

                    if (!empty($suspModel->suspension_history)&& $suspModel->suspension_history!=''){
                        $oldHistory = (ARRAY)json_decode($suspModel->suspension_history);
                        $histIndex= sizeof($oldHistory);
                        $oldHistory[$histIndex]=$currentHistory;
                        $history=$oldHistory;
                    }else{

                        $history[]=$currentHistory;
                    }


                    $suspModel->suspension_history = json_encode($history);
                    $suspModel->supporting_document = json_encode($document);

                    $suspModel->status_by = $created_by;
                    $suspModel->status_date = $created_at;
                    //$resultsCount=DisbursementSuspension::find()->where(['sync_id'=>$sync])->count();
                    if($suspModel->save(false)){
                        $sms.="<p> $suspModel->LoanItem Information successful added</p>";
                        $alert = 'success';
                    }else{
                        $sms.= json_encode($suspModel->getErrors());
                        $alert = 'danger';
                    }
                }

            }


            Yii::$app->getSession()->setFlash($alert, $sms);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DisbursementSuspension model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DisbursementSuspension model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DisbursementSuspension the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DisbursementSuspension::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}