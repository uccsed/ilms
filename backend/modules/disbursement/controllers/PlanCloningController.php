<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/11/18
 * Time: 7:19 PM
 */

namespace backend\modules\disbursement\controllers;


use yii\web\Controller;
use backend\modules\disbursement\models\DisbursementDefaultPlan;
use backend\modules\disbursement\models\DisbursementPlan;

use yii\test\InitDbFixture;

use yii\web\BadRequestHttpException;
use Yii;

class PlanCloningController extends Controller
{
    public $layout="main_private";

    public function actionIndex(){

        return $this->render('index');
    }


    public function actionCreate(){
        if (Yii::$app->request->isAjax) { //Making sure its an ajax request

            /**
             *  type:type,
             * programme:programme,
             * loanItem:loanItem,
             * currentYear:currentYear,
             * nextYear:nextYear,
             */

            $type = $_POST['type'];
            $programme = $_POST['programme'];
            $loanItem = $_POST['loanItem'];
            $currentYear = $_POST['currentYear'];
            $nextYear = $_POST['nextYear'];


            $response = self::CloneMaster($type,$currentYear,$nextYear,$programme,$loanItem);
            $output='<p style="font-size: large;">'.$response.'</p>';
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => $output,
            ];
        }else {
            $model = new DisbursementPlan();
            return $this->render('create', array('model' => $model));
        }
    }


    public static function CloneMaster($type,$currentYear,$nextYear,$programme,$loanItem)
    {
        $output = "";
        $counter =$updateCounter= 0;

        $now = date('Y-m-d H:i:s');
        $user = Yii::$app->user->id;






        switch($type){
            case "programme":
                $defaultARRAY = self::DefaultArray($currentYear,$programme,$loanItem);


                //Loading All Active Semesters
                $SEMESTER = Module::Semester();

                //Loading All Active Installments
                $INSTALLMENTS = InstalmentDefinition::findAll(array('is_active'=>'1'));
                $fields = "academic_year,year_of_study,programme_id,instalment_id,loan_item_id,semester_number,disbursement_percent,sync_id,date_created,created_by";
                $insertSQL=$updateSQL="";
                $SQL = "SELECT * FROM programme WHERE programme.programme_id<>'$programme';";

                $model=Yii::$app->db->createCommand($SQL)->queryAll();

                $head = $programme.'-'.$loanItem.'-'.$currentYear.'_';
                foreach ($model as $index=>$programmeArray)
                {

                    $totalYears=$programmeArray['years_of_study'];
                    for($YOS=1;$YOS<=$totalYears;$YOS++){

                        foreach ($SEMESTER as $semesterID=>$semesterName){

                            foreach ($INSTALLMENTS as $instalment){
                                $instalmentID=$instalment->instalment_definition_id;
                                $newProgramme = $programmeArray['programme_id'];
                                $sycID =$head."percent[$YOS][$semesterID][$instalmentID]";

                                $key = $YOS.'_'.$semesterID.'_'.$instalmentID;
                                if (isset($defaultARRAY[$key])){ $percent = $defaultARRAY[$key]; }else{$percent=0;}

                                $mySYNC =$newProgramme.'-'.$loanItem.'-'.$currentYear."_percent[$YOS][$semesterID][$instalmentID]";
                                $checkSQL = "SELECT disbursement_default_plan.plan_id AS 'id' FROM disbursement_default_plan WHERE disbursement_default_plan.sync_id='$mySYNC' ORDER BY plan_id DESC LIMIT 1";
                                $checkModel = Yii::$app->db->createCommand($checkSQL)->queryAll();
                                if (sizeof($checkModel)!=0){
                                    $updateCounter++;
                                    $updateSQL.="UPDATE disbursement_default_plan SET disbursement_percent='$percent' WHERE sync_id='$mySYNC';";

                                } else{
                                    $insertSQL.="INSERT INTO disbursement_default_plan($fields) VALUES ('$currentYear','$YOS','$newProgramme','$instalmentID','$loanItem','$semesterID','$percent','$mySYNC','$now','$user');";
                                    $counter++;
                                }

                            }


                        }

                    }

                }
                if($counter>0){Yii::$app->db->createCommand($insertSQL)->execute(); $output.="A total of <b>".number_format($counter,0)."</b> Records has been successfully registered";}
                if($updateCounter>0){Yii::$app->db->createCommand($updateSQL)->execute(); $output.=" A total of <b>".number_format($updateCounter,0)."</b> Records has been updated successfully";}


                break;

            case "default":
                //$type,$currentYear,$nextYear,$programme,$loanItem
                $SQL = "DELETE FROM disbursement_default_plan WHERE academic_year='$nextYear'; ";
                $SQL.= "ALTER TABLE disbursement_default_plan AUTO_INCREMENT = 1; ";
                $SQL.= "INSERT INTO disbursement_default_plan (academic_year,year_of_study,programme_id,instalment_id,loan_item_id,semester_number,disbursement_percent,sync_id,date_created,created_by)";
                $SQL.= "SELECT $nextYear AS 'academic_year',year_of_study,programme_id,instalment_id,loan_item_id,semester_number,disbursement_percent,  CONCAT(programme_id,'-',loan_item_id,'-',$nextYear,'_','percent','[',year_of_study,']','[',semester_number,']','[',instalment_id,']') AS 'sync_id','$now' AS 'date_created','$user' AS 'created_by' FROM disbursement_default_plan WHERE academic_year='$currentYear';";
                Yii::$app->db->createCommand($SQL)->execute();
                //$output = '<pre>'.$SQL.'</pre>';//"Plan has been Successfully cloned";
                $output = "Plan has been Successfully cloned";
                break;

            case "institutional":
                //$type,$currentYear,$nextYear,$programme,$loanItem
                $SQL = "DELETE FROM disbursement_plan WHERE academic_year='$nextYear'; ";
                $SQL.= "ALTER TABLE disbursement_plan AUTO_INCREMENT = 1; ";
                $SQL.= "INSERT INTO disbursement_plan (learning_institution,academic_year,year_of_study,programme_id,instalment_id,loan_item_id,semester_number,disbursement_percent,sync_id,date_created,created_by)";
                $SQL.= "SELECT learning_institution, $nextYear AS 'academic_year',year_of_study,programme_id,instalment_id,loan_item_id,semester_number,disbursement_percent,  CONCAT(learning_institution,'-',programme_id,'-',loan_item_id,'-',$nextYear,'_','percent','[',year_of_study,']','[',semester_number,']','[',instalment_id,']') AS 'sync_id','$now' AS 'date_created','$user' AS 'created_by' FROM disbursement_plan WHERE academic_year='$currentYear';";
                Yii::$app->db->createCommand($SQL)->execute();
                //$output = '<pre>'.$SQL.'</pre>';//"Plan has been Successfully cloned";
                $output = "Plan has been Successfully cloned";
                break;

        }

        return $output;
    }

    /**
     * Finds the InstalmentDefinition model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InstalmentDefinition the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function institutionalModel($id)
    {
        if (($model = DisbursementPlan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function defaultModel($id)
    {
        if (($model = DisbursementDefaultPlan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}