<?php

namespace backend\modules\disbursement\controllers;

use backend\modules\disbursement\models\DisbursementDepositStaging;
use backend\modules\disbursement\models\DisbursementDepositStagingSearch;
use Yii;
use backend\modules\disbursement\models\DisbursementDepositBatch;
use backend\modules\disbursement\models\DisbursementDepositBatchSearch;
use backend\modules\disbursement\Module;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DisbursementDepositBatchController implements the CRUD actions for DisbursementDepositBatch model.
 */
class DisbursementDepositBatchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DisbursementDepositBatch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DisbursementDepositBatchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DisbursementDepositBatch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DisbursementDepositBatch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DisbursementDepositBatch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DisbursementDepositBatch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionCrossMatch(){
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {
            $results = array();
            $headerID = $_POST['headerID'];
            $SQL = "SELECT id FROM disbursement_deposit_staging WHERE header_id = '$headerID' AND status IN('NEW','MISMATCH')";
            $model = Yii::$app->db->createCommand($SQL)->queryAll();
            if (sizeof($model)!=0){
                foreach ($model as $index=>$dataArray)
                {
                    $id = $dataArray['id'];
                    if (Module::CrossMatchDeposits($id)){
                        $results[] = $id;
                    }
                }
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results'=>$results),
            ];
        } else throw new BadRequestHttpException;
    }


    public function actionConfirmation(){
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {
            $results = array();
            $headerID = $_POST['headerID'];
            $SQL = "SELECT id FROM disbursement_deposit_staging WHERE header_id = '$headerID' AND status ='MATCH' AND disbursement_id IS NOT NULL AND disbursement_id<>'0' ";
            $model = Yii::$app->db->createCommand($SQL)->queryAll();
            if (sizeof($model)!=0){
                foreach ($model as $index=>$dataArray)
                {
                    $id = $dataArray['id'];
                    if (Module::ConfirmDeposits($id)){
                        $results[] = $id;
                    }
                }
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results'=>$results),
            ];
        } else throw new BadRequestHttpException;
    }


    public function actionUpdateStaging($id)
    {
        $this->layout='default_main';
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        $model = $this->findStagingModel($id);
        if ($model->load(Yii::$app->request->post())) {
            //$output =
            if ($model->save()){
                Yii::$app->getSession()->setFlash('success',"A Record with an index number <b>$model->index_number</b> has been Updated Successfully");
                return $this->redirect(['staging', 'batchID' => $model->header_id,'status'=>$model->status]);

            }else{
                Yii::$app->getSession()->setFlash('error',"Save Error".json_encode($model->getErrors()));
            }

        }
        return $this->render('update_staging', [
            'model' => $model,
        ]);
    }








    public function actionStaging($batchID,$status)
    {
        $this->layout='default_main';
        $searchModel = new DisbursementDepositStagingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$batchID,$status);

        switch ($status){
            case "CONFIRMED":
                $view = 'confirmed';
                break;

            default:
                $view = 'staging_deposits';
                break;
        }

        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'batchID' => $batchID,
            'status' => $status,
        ]);
    }






    /**
     * Deletes an existing DisbursementDepositBatch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $SQL="DELETE FROM disbursement_deposit_staging WHERE header_id='$id';";
        Yii::$app->db->createCommand($SQL)->execute();

        $model->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteStaging($id)
    {
        $model=$this->findStagingModel($id);
        $batch_id = $model->header_id;
        $model->delete();

        //return $this->redirect(['index']);
        return $this->redirect(['view', 'id' => $batch_id]);
    }
    /**
     * Finds the DisbursementDepositBatch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DisbursementDepositBatch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DisbursementDepositBatch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findStagingModel($id)
    {
        if (($model = DisbursementDepositStaging::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
