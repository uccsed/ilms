<?php

namespace backend\modules\disbursement\controllers;

use Yii;
use backend\modules\disbursement\models\DisbursementBatch;
use backend\modules\disbursement\models\DisbursementBatchSearch;
//use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Controller;
use backend\modules\disbursement\Module;
use backend\modules\disbursement\models\PayoutlistMovement;
use yii\helpers\Json;
/**
 * DisbursementBatchController implements the CRUD actions for DisbursementBatch model.
 */
class DisbursementBatchController extends Controller {

    /**
     * @inheritdoc
     */
    public $layout = "main_private";

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionAssignedInstitutions() {
        $out = [];
        $user=Yii::$app->user->id;
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {

                $level= $parents[0];
                $country= $parents[1];
                $WHERE = "";
                if (!empty($country)){
                    $WHERE = " AND (country.country_id='$country') ";
                }

                switch ($level){
                    case 1:

                        $SQL="
                        SELECT 
                         learning_institution.learning_institution_id AS 'id',
                         CONCAT(learning_institution.institution_name,' (',learning_institution.institution_code,')') AS 'name'
                        FROM learning_institution 
                        LEFT JOIN country ON learning_institution.country_id = country.country_id
                        WHERE institution_type = 'UNIVERSITY'$WHERE AND learning_institution_id IN (SELECT institution_assignment.institution_id FROM institution_assignment WHERE institution_assignment.officer_id='$user') AND country.country_code = 'TZA'";

                        break;

                    case 2:
                        $SQL="
                        SELECT 
                         learning_institution.learning_institution_id AS 'id',
                         CONCAT(learning_institution.institution_name,' (',learning_institution.institution_code,')') AS 'name'
                        FROM learning_institution 
                        LEFT JOIN country ON learning_institution.country_id = country.country_id
                        WHERE institution_type = 'UNIVERSITY' $WHERE AND (country.country_code <> 'TZA' OR country.country_code IS NULL) AND (learning_institution_id IN (SELECT institution_assignment.institution_id FROM institution_assignment WHERE institution_assignment.officer_id='$user'));";

                        break;
                }

                $out = Yii::$app->db->createCommand($SQL)->queryAll();
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    public function actionFilteredInstalments() {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $out = [];
        $user=Yii::$app->user->id;

        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {

                if (!empty($parents[0])){
                    $semester = $parents[0]; // Semester




                    $SQL = "
                        SELECT 
                        instalment_definition.instalment_definition_id AS 'id', 
                        CONCAT(instalment_definition.instalment,' (',instalment_definition.instalment_desc,')') AS 'name'
                        FROM instalment_definition
                        WHERE instalment_definition.semester_id = '$semester';
                ";



                    $out = Yii::$app->db->createCommand($SQL)->queryAll();
                    // the getSubCatList function will query the database based on the
                    // cat_id and return an array like below:
                    // [
                    //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                    //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                    // ]
                    echo Json::encode(['output'=>$out, 'selected'=>'']);
                    return;
                }

            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    public function actionFilteredItems() {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $out = [];
        $user=Yii::$app->user->id;

        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {

                if (!empty($parents[0])&&!empty($parents[1])&&!empty($parents[2])){
                    $disburseTo = $parents[0]; // Student | Institution
                    $study_region= $parents[1]; // Local | Overseas
                    $study_level= $parents[2]; // Bachelor | Undergraduate | Postgraduate | Masters | Diploma | PhD etc



                    /*
                     * disburse_type => // Student | Institution
                     * applicant_category_id => // Local | Overseas
                     * level => // Bachelor | Undergraduate | Postgraduate | Masters | Diploma | PhD etc
                     *
                     */



                    $ItemCondition = "";
                    switch ($disburseTo)
                    {
                        case 1: //Not Requested by Institution
                            $ItemCondition=" AND loan_item.is_requested = '0'";
                            break;

                        case 2: //Requested by Institution
                            $ItemCondition=" AND loan_item.is_requested = '1'";
                            break;
                    }

                    $SQL = "
                        SELECT 
                        item_disbursement_setting.loan_item_id AS 'id', 
                        CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'name'
                        
                        FROM item_disbursement_setting
                        LEFT JOIN loan_item ON item_disbursement_setting.loan_item_id = loan_item.loan_item_id
                        WHERE /*item_disbursement_setting.disbursed_to = '$disburseTo' AND */
                        item_disbursement_setting.study_category = '$study_region' AND 
                        item_disbursement_setting.study_level= '$study_level' $ItemCondition;
                ";



                    $out = Yii::$app->db->createCommand($SQL)->queryAll();
                    // the getSubCatList function will query the database based on the
                    // cat_id and return an array like below:
                    // [
                    //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                    //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                    // ]
                    echo Json::encode(['output'=>$out, 'selected'=>'']);
                    return;
                }

            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    /**
     * Lists all DisbursementBatch models.
     * @return mixed
     */
    public function actionIndex() {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');



        $searchModel = new DisbursementBatchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLocalSearch(){
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {

            $results = array();
            if (!empty($_POST['type'])){
                $type = $_POST['type'];
                if ($type=='1'){
                    $SQL = "SELECT country.country_id AS 'id',country.country_name AS 'name', country.country_code AS 'code' FROM country WHERE country_code = 'TZA'  LIMIT 1";
                    $results = Yii::$app->db->createCommand($SQL)->queryAll();
                }
            }


            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results'=>$results),
            ];
        } else throw new BadRequestHttpException;
    }

    public function actionLoanList() {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');



        $searchModel = new DisbursementBatchSearch();
        $dataProvider = $searchModel->searchSpecial(Yii::$app->request->queryParams,'1','0');
       // $dataProvider = $searchModel->search(Yii::$app->request->queryParams,'0');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGrantList() {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');



        $searchModel = new DisbursementBatchSearch();
        $dataProvider = $searchModel->searchSpecial(Yii::$app->request->queryParams,'2');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DisbursementBatch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        return $this->render('profile', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DisbursementBatch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


    public function actionAllocation()
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {
            $location = $_POST['location'];



            $WHERE =" allocation.is_canceled = '0' AND (allocation_batch.partial_approve_status = '1' OR allocation_batch.larc_approve_status = '1') ";
            if (!empty($_POST['programme_category'])){$study_level = $_POST['programme_category']; $WHERE.=" AND programme.study_level = '$study_level'"; }else{ $study_level="";}
            if (!empty($_POST['academic_year'])){$academic_year = $_POST['academic_year']; $WHERE.=" AND allocation_batch.academic_year_id = '$academic_year'";  }
            if (!empty($_POST['installment'])){$installment = $_POST['installment']; }
            if (!empty($_POST['loan_item'])){
                $loan_item = $_POST['loan_item'];
                # $WHERE.=" AND allocation.loan_item_id='$loan_item'";
                $WHERE.=" AND allocation.loan_item_id IN( SELECT '$loan_item' UNION SELECT disbursement_setting2.associated_loan_item_id FROM disbursement_setting2 WHERE disbursement_setting2.academic_year_id='$academic_year' AND disbursement_setting2.instalment_definition_id = '$installment' AND disbursement_setting2.loan_item_id='$loan_item')";

            }


            switch ($location){
                case 1:
                    if (!empty($_POST['institution'])){$institution = $_POST['institution']; $WHERE.=" AND programme.learning_institution_id = '$institution'"; }else{ $institution="";}

                    $rSQL="
                        SELECT 
                                           
                           programme_group.group_code AS 'programme_code', 
                           CONCAT(programme_group.group_name,' (',IFNULL(programme_group.group_code,'N/A'),')') AS 'programme_name', 
                            
                            loan_item.item_name AS 'item_name',
                            COUNT(DISTINCT allocation.application_id) AS 'loanee', 
                            SUM(allocation.allocated_amount) AS 'allocated_amount'
                            
                            FROM allocation 
                            RIGHT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id AND allocation_batch.is_approved = '1' AND (allocation_batch.partial_approve_status = '1' OR allocation_batch.larc_approve_status = '1')
                            LEFT JOIN application ON allocation.application_id = application.application_id
                            LEFT JOIN programme ON  application.programme_id =  programme.programme_id
                            LEFT JOIN programme_group ON programme.programme_group_id = programme_group.programme_group_id
                            LEFT JOIN loan_item ON  allocation.loan_item_id =  loan_item.loan_item_id
                            WHERE   $WHERE
                            GROUP BY application.programme_id,current_study_year,allocation.loan_item_id
                            ORDER BY programme.programme_code ASC ,current_study_year ASC ;
                        ";
                    break;

                case 2:
                    if (!empty($_POST['institution'])){$institution = $_POST['institution']; $WHERE.=" AND programme.learning_institution_id = '$institution'"; }else{ $institution="";}
                    if (!empty($_POST['country'])){$country = $_POST['country']; $WHERE.=" AND learning_institution.country_id = '$country'"; }else{ $country="";}

                    $rSQL="
                        SELECT 
                                           
                           programme_group.group_code AS 'programme_code', 
                           CONCAT(programme_group.group_name,' (',IFNULL(programme_group.group_code,'N/A'),')') AS 'programme_name', 
                            
                            loan_item.item_name AS 'item_name',
                            COUNT(DISTINCT allocation.application_id) AS 'loanee', 
                            SUM(allocation.allocated_amount) AS 'allocated_amount'
                            
                            
                            FROM allocation 
                            RIGHT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id AND allocation_batch.is_approved = '1' AND (allocation_batch.partial_approve_status = '1' OR allocation_batch.larc_approve_status = '1')
                            LEFT JOIN application ON allocation.application_id = application.application_id
                            LEFT JOIN programme ON  application.programme_id =  programme.programme_id
                            LEFT JOIN programme_group ON programme.programme_group_id = programme_group.programme_group_id
                            LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                            LEFT JOIN loan_item ON  allocation.loan_item_id =  loan_item.loan_item_id
                            WHERE   $WHERE
                            GROUP BY application.programme_id,current_study_year,allocation.loan_item_id
                            ORDER BY programme.programme_code ASC ,current_study_year ASC ;
                        ";
                    break;
            }





            //if (!empty($_POST['installment'])){$installment = $_POST['installment']; $WHERE.=" AND disbursement_plan.instalment_id = '$installment'"; }
            //if (!empty($_POST['semester'])){$semester = $_POST['semester']; $WHERE.=" AND disbursement_plan.semester_number = '$semester'"; }
            // if (!empty($_POST['programme_category'])){$programme_category = $_POST['programme_category']; $WHERE.=" AND programme.programme_category_id = '$programme_category'"; }




            $results = array();

            $results = Yii::$app->db->createCommand($rSQL)->queryAll();




            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results'=>$results,'sql'=>$rSQL),
            ];
        } else throw new BadRequestHttpException;
    }


    public function actionRequestData()
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {
            $requestID = $_POST['requestID'];
            $rSQL = "SELECT * FROM institution_fund_request WHERE id = '$requestID'";
            $requestModel = Yii::$app->db->createCommand($rSQL)->queryAll();

            $pLSQL = "SELECT * FROM institution_paylist WHERE institution_paylist.request_id = '$requestID'";
            $payList = Yii::$app->db->createCommand($pLSQL)->queryAll();


            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('request' => $requestModel,'payList'=>$payList),
            ];
        } else throw new BadRequestHttpException;
    }
    public function actionOutcome()
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {

            $results = array();

            $plan = $_POST['plan'];
            $location = $_POST['location'];
            switch ($location){
                case 1:
                    $country = null;
                    break;

                case 2:
                    $country = $_POST['country'];
                    break;
            }


            //$allocation=$_POST['allocation'];
            //$WHERE = " allocation.is_canceled = '0' ";
            $WHERE =" allocation.is_canceled = '0' AND (allocation_batch.partial_approve_status = '1' OR allocation_batch.larc_approve_status = '1') ";
            if (!empty($_POST['institution'])) {
                $institution = $_POST['institution'];
                $WHERE .= " AND programme.learning_institution_id = '$institution'";
            } else {
                $institution = "";
            }
            if (!empty($_POST['academic_year'])) {
                $academicYear = $_POST['academic_year'];
            }
            if (!empty($_POST['version'])) {
                 $version = $_POST['version'];
            }else{
                $version = '0';
            }
            if (!empty($_POST['installment'])) {
                $installment = $_POST['installment'];
            }
            if (!empty($_POST['semester'])) {
                $semester = $_POST['semester'];
            }

            if (!empty($_POST['programme_category'])) {
                $study_level = $_POST['programme_category'];
            }
            if (!empty($_POST['loan_item'])) {
                $loan_item = $_POST['loan_item'];
                //$WHERE.=" AND allocation.loan_item_id='$loan_item'";
                // $WHERE.=" AND allocation.loan_item_id IN( SELECT '$loan_item' UNION SELECT disbursement_setting2.associated_loan_item_id FROM disbursement_setting2 WHERE disbursement_setting2.academic_year_id='$academic_year' AND disbursement_setting2.instalment_definition_id = '$installment' AND disbursement_setting2.loan_item_id='$loan_item')";

            }


            $results = Module::PlanImplementer($academicYear, $institution, $semester, $installment, $loan_item,$study_level,$version,$country);

            // $results = Yii::$app->db->createCommand($rSQL)->queryAll();


            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results' => $results),
            ];
        } else throw new BadRequestHttpException;
    }
    public function actionPlan(){
        ini_set('max_execution_time', -1);
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {


            $WHERE =' 1=1 ';

            $location = $_POST['location'];
            $country = $_POST['country'];

            if (!empty($_POST['institution'])){$institution = $_POST['institution'];}else{ $institution="";}


            //  if (!empty($_POST['programme_id'])){$programme_id = $_POST['programme_id']; $WHERE.=" AND programme_id = '$programme_id'"; }
            if (!empty($_POST['loan_item'])){$loan_item = $_POST['loan_item']; $WHERE.=" AND disbursement_plan.loan_item_id='$loan_item'"; }
            if (!empty($_POST['academic_year'])){$academic_year = $_POST['academic_year']; $WHERE.=" AND disbursement_plan.academic_year ='$academic_year'"; }
            if (!empty($_POST['installment'])){$installment = $_POST['installment']; $WHERE.=" AND disbursement_plan.instalment_id = '$installment'"; }
            if (!empty($_POST['semester'])){$semester = $_POST['semester']; $WHERE.=" AND disbursement_plan.semester_number = '$semester'"; }
            //if (!empty($_POST['programme_category'])){$programme_category = $_POST['programme_category']; $WHERE.=" AND programme.programme_category_id = '$programme_category'"; }
            if (!empty($_POST['programme_category'])){$programme_category = $_POST['programme_category']; $WHERE.=" AND programme.study_level = '$programme_category'"; }



            $model = array();

            switch ($location){
                case 1: //Local

                    $SQL = '';
                    //$model = array();


                     $SQLs = "
                        SELECT  
                            disbursement_plan.academic_year AS 'academic_year_id', 
                            disbursement_plan.programme_id AS 'programme_id',
                            disbursement_plan.loan_item_id AS 'loan_item_id',
                            academic_year.academic_year AS 'academic_year', 
                            disbursement_plan.year_of_study AS 'year_of_study' , 
                            programme_group.group_code AS 'programme_code', 
                            CONCAT(programme_group.group_name,' (',IFNULL(programme_group.group_code,'N/A'),')') AS 'programme_name', 
                            instalment_id AS 'instalment_id', 
                            loan_item.item_name AS 'item_name', 
                            semester.semester_number AS  'semester_number', 
                            disbursement_plan.disbursement_percent AS 'disbursement_percent'
                        FROM  disbursement_plan
                        LEFT JOIN learning_institution ON  disbursement_plan.learning_institution =  learning_institution.learning_institution_id
                        LEFT JOIN academic_year ON  disbursement_plan.academic_year =  academic_year.academic_year_id
                        LEFT JOIN programme ON  disbursement_plan.programme_id =  programme.programme_id
                        LEFT JOIN programme_group ON programme.programme_group_id = programme_group.programme_group_id
                        LEFT JOIN loan_item ON  disbursement_plan.loan_item_id =  loan_item.loan_item_id
                        LEFT JOIN instalment_definition ON  disbursement_plan.instalment_id =  instalment_definition.instalment_definition_id
                         LEFT JOIN semester ON  disbursement_plan.semester_number =  semester.semester_id
                            
                        WHERE  $WHERE
                        ORDER BY programme.programme_code ASC, year_of_study ASC, instalment_definition.instalment ASC,semester_number ASC
                        ";
                    $specific = Yii::$app->db->createCommand($SQLs)->queryAll();
                    if (sizeof($specific)!=0){
                        $model[] = $specific; $SQL = $SQLs;
                    }else{
                        $WHERE =' 1=1 ';
                        if (!empty($_POST['loan_item'])){$loan_item = $_POST['loan_item']; $WHERE.=" AND disbursement_default_plan.loan_item_id='$loan_item'"; }
                        if (!empty($_POST['academic_year'])){$academic_year = $_POST['academic_year']; $WHERE.=" AND disbursement_default_plan.academic_year ='$academic_year'"; }
                        if (!empty($_POST['installment'])){$installment = $_POST['installment']; $WHERE.=" AND disbursement_default_plan.instalment_id = '$installment'"; }
                        if (!empty($_POST['semester'])){$semester = $_POST['semester']; $WHERE.=" AND disbursement_default_plan.semester_number = '$semester'"; }
                        //if (!empty($_POST['programme_category'])){$programme_category = $_POST['programme_category']; $WHERE.=" AND programme.study_level = '$programme_category'"; }



                        /*$maxYearsSQL = "(SELECT MAX(programme.years_of_study) AS 'maxYears' FROM programme WHERE  programme.learning_institution_id = '$institution')";
                        $maxYearsModel = Yii::$app->db->createCommand($maxYearsSQL)->queryAll();
                        $maxYears = $maxYearsModel[0]['maxYears'];*/
                        $SQLd= "";
                        $pgcSQL = "(
                SELECT 
                 programme_group.group_code AS 'programme_code', 
                 CONCAT(programme_group.group_name,' (',IFNULL(programme_group.group_code,'N/A'),')') AS 'programme_name', 
                programme.years_of_study AS 'Years' 
                FROM programme 
                LEFT JOIN programme_group ON programme.programme_group_id = programme_group.programme_group_id
                WHERE  programme.learning_institution_id = '$institution' AND programme.study_level = '$programme_category')";
                        $pgcModel = Yii::$app->db->createCommand($pgcSQL)->queryAll();
                        foreach ($pgcModel AS $indx=>$programmesArray){
                            //$pgc=$programmesArray['programme_code'];
                            $pgn=$programmesArray['programme_name'];
                            $maxYears=$programmesArray['Years'];
                            for($i=1; $i<=$maxYears; $i++){
                                $SQLd= "
                            SELECT  
                            academic_year.academic_year, 
                            '$i' AS 'year_of_study', 
                            '$pgn' AS 'programme_name',
                            '$maxYears' AS 'years',
                            instalment_definition.instalment, 
                            loan_item.item_name, 
                            semester.semester_number, 
                            disbursement_default_plan.disbursement_percent
                            FROM  disbursement_default_plan
                            LEFT JOIN academic_year ON  disbursement_default_plan.academic_year =  academic_year.academic_year_id
                            LEFT JOIN semester ON  disbursement_default_plan.semester_number =  semester.semester_id
                            
                            LEFT JOIN loan_item ON  disbursement_default_plan.loan_item_id =  loan_item.loan_item_id 
                            LEFT JOIN instalment_definition ON  disbursement_default_plan.instalment_id =  instalment_definition.instalment_definition_id
                            WHERE $WHERE 
                            ORDER BY instalment_definition.instalment ASC,semester_number ASC;";
                                $default[] = Yii::$app->db->createCommand($SQLd)->queryAll();
                                $SQL.=$SQLd;

                            }
                        }
                        // $default[] = Yii::$app->db->createCommand($SQLd)->queryAll();
                        if (sizeof($default)!=0){ $model = $default; }
                    }
                    break;

                case 2: //Overseas

                    $SQL="SELECT 
                            academic_year.academic_year AS 'academic_year',
                            overseas_settings.year_of_study AS 'year_of_study',
                            'ALL PROGRAMMES' AS 'programme_name',
                            '10' AS 'years',
                           instalment_definition.instalment, 
                            loan_item.item_name, 
                            semester.semester_number, 
                            overseas_settings.disbursement_percent AS 'disbursement_percent'
                          
                          FROM overseas_settings
                            LEFT JOIN academic_year ON  overseas_settings.academic_year =  academic_year.academic_year_id
                            LEFT JOIN instalment_definition ON  overseas_settings.instalment =  instalment_definition.instalment_definition_id
                            LEFT JOIN loan_item ON  overseas_settings.loan_item =  loan_item.loan_item_id   
                            LEFT JOIN semester ON  instalment_definition.semester_id =  semester.semester_id
                            
                          
                          WHERE 
                          overseas_settings.country_id = '$country' AND 
                          overseas_settings.academic_year = '$academic_year' AND 
                          overseas_settings.loan_item = '$loan_item' AND 
                          overseas_settings.instalment = '$installment'
                          ";
                    $model[] = Yii::$app->db->createCommand($SQL)->queryAll();
                    break;
            }






            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results'=>$model, 'sql'=>$SQL),
            ];
        } else throw new BadRequestHttpException;

    }

    public function actionLoan()
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');

        $model = new DisbursementBatch();
        if ($model->load(Yii::$app->request->post())) {
            $postArray = $_POST['DisbursementBatch'];

            //print_r($postArray);
            //exit();
            $model->allocation_batch_id = $postArray['allocation_batch_id'];
            //$strDigits = date('YmdHis').Yii::$app->user->id;
            $strDigits = time().Yii::$app->user->id;
            $model->batch_number = Module::HeaderIDAlgorithm($strDigits);
            $model->disburse_model = 0;
           // $version = $postArray['version'];


            if ($model->save()){
               Module::DisburseLoan($postArray,$model);

               $batchID = $model->disbursement_batch_id;
                $misDisbursed = Module::MisDisbursementCreator($batchID);
                $message = $alert = '';
                if ($misDisbursed>0){
                    $message = number_format($misDisbursed,0).' Loanees Mis-disbursed for various reasons';
                    $alert = 'danger';
                }else{
                    $message ='Payment create successfully! No mis-disbursement found';
                    $alert = 'success';
                }
                Yii::$app->getSession()->setFlash(
                    $alert,$message
                );
                return $this->redirect(['view', 'id' => $model->disbursement_batch_id]);


            }else{
                Yii::$app->getSession()->setFlash(
                    'danger', 'Operation Failure:'.json_encode($model->getErrors()).' '
                );
            }


        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function actionGrant()
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');

        $model = new DisbursementBatch();
        if ($model->load(Yii::$app->request->post())) {
            $postArray = $_POST['DisbursementBatch'];

            //print_r($postArray);
            //exit();
            $model->allocation_batch_id = $postArray['allocation_batch_id'];
            // $version = $postArray['version'];


            if ($model->save()){
                Module::DisburseLoan($postArray,$model);
                return $this->redirect(['view', 'id' => $model->disbursement_batch_id]);


            }else{
                Yii::$app->getSession()->setFlash(
                    'danger', 'Operation Failure:'.json_encode($model->getErrors()).' '
                );
            }


        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }
    /*public function actionCreate() {
        ini_set('max_execution_time', 3000000);
        ini_set('memory_limit', '16000000000M');

        $model = new DisbursementBatch();

        if ($model->load(Yii::$app->request->post())) {
            //check the information
//              print_r($model->load(Yii::$app->request->post()));
//              $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
//              print_r($model);
//                echo "<br/> mickidadi";
//                       exit();
            $studentlist = new DisbursementBatch();
            //get loan Item dependant data
            $allloanItem = Yii::$app->db->createCommand("SELECT group_concat(`associated_loan_item_id`) as loan_item FROM `disbursement_setting2` WHERE loan_item_id='{$model->loan_item_id}' AND academic_year_id='{$model->academic_year_id}' group by `loan_item_id`")->queryAll();
            foreach ($allloanItem as $allloanItems)
                ;
            $allloan_Item = $model->loan_item_id . ',' . $allloanItems["loan_item"];
            //end 
            if ($model->disburse == 2) {
                $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
                //if($model->t!=""){
                $model->file->saveAs('upload/' . $model->file->name);
                $model->file = 'upload/' . $model->file->name;
                $data = \moonland\phpexcel\Excel::widget([
                            'mode' => 'import',
                            'fileName' => $model->file,
                            'setFirstRecordAsKeys' => true, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel.
                            'setIndexSheetByName' => true, // set this if your excel data with multiple worksheet, the index of array will be set with the sheet name. If this not set, the index will use numeric.
                                //'getOnlySheet' => 'sheet1', // you can set this property if you want to get the specified sheet from the excel data with multiple worksheet.
                ]);
                foreach ($data as $rows) {

                    $selectivedata[] = $rows['INDEXNO'];
                }

                $selectivedata1 = join("', '", $selectivedata);

                $studentdata = $studentlist->getStudentselective($model->learning_institution_id, $model->level, $model->academic_year_id, $allloan_Item, $model->instalment_definition_id, $selectivedata1);
            } else {
                $studentdata = $studentlist->getStudents($model->learning_institution_id, $model->level, $model->academic_year_id, $allloan_Item, $model->instalment_definition_id);
                //print_r($studentdata);
            }
            if (count($studentdata)>0) {

                $totalfailed = $total = 0;
                foreach ($studentdata as $studentdatas) {

                    //create disbursement
                    //$testdata = \backend\modules\disbursement\models\Disbursement::findAll(["application_id" => $studentdatas["application_id"], "loan_item_id" => $model->loan_item_id, "version" => $model->version]);
                    $testdata = $studentlist->getDisbursementstatus($model->learning_institution_id, $model->academic_year_id, $studentdatas["loan_item_id"], $studentdatas["application_id"], $model->version, $model->instalment_definition_id);
                    if (count($testdata) == 0) {
                        if ($total == 0) {
                            $model->save();
                        }
                        // check disbursement before disbursed amount for Item

                        $createdisbursement = new \backend\modules\disbursement\models\Disbursement();
                        $createdisbursement->disbursement_batch_id = $model->disbursement_batch_id;
                        $createdisbursement->application_id = $studentdatas["application_id"];
                        $createdisbursement->programme_id = $studentdatas["programme_id"];
                        $createdisbursement->loan_item_id = $studentdatas["loan_item_id"];
                        $createdisbursement->version = $model->version == "" ? 0 : $model->version;
                        $createdisbursement->disbursed_amount = $studentdatas["amount"];
                        $createdisbursement->disbursed_as = $model->disbursed_as;
                        $createdisbursement->status = 1;
                        $createdisbursement->created_at = date("Y-m-d h:i:s");
                        $createdisbursement->created_by = \yii::$app->user->identity->user_id;
                        $createdisbursement->save();
                        //end create

                        $total++;
                    } else {
                        $totalfailed++;
                    }
                }
                Yii::$app->getSession()->setFlash(
                        'success', 'Data Successfully Created! ' . $total . " Unsuccessfuly " . $totalfailed
                );
            } else {
                Yii::$app->getSession()->setFlash(
                        'info', 'Empty Disbursement!'
                );
            }
            //  exit();
            //end

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }*/

    /**
     * Updates an existing DisbursementBatch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $postArray = $_POST['DisbursementBatch'];

            switch ($model->disbursed_as){

                case '1': //LOAN
                    Module::DisburseLoan($postArray,$model);

                    $batchID = $model->disbursement_batch_id;
                    $misDisbursed = Module::MisDisbursementCreator($batchID);
                    $message = $alert = '';
                    if ($misDisbursed>0){
                        $message = number_format($misDisbursed,0).' Loanees Mis-disbursed for various reasons';
                        $alert = 'danger';
                    }else{
                        $message ='Payment create successfully! No mis-disbursement found';
                        $alert = 'success';
                    }
                    Yii::$app->getSession()->setFlash(
                        $alert,$message
                    );
                    break;

                case '2': //GRANT
                    Module::DisburseGrant($postArray,$model);
                    break;

            }


            return $this->redirect(['view', 'id' => $model->disbursement_batch_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DisbursementBatch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        \backend\modules\disbursement\models\Disbursement::deleteAll(['disbursement_batch_id' => $id]);
        $this->findModel($id)->delete();
        //delete all disbursement made on that batch /Header Id
        \backend\modules\disbursement\models\Disbursement::deleteAll(['disbursement_batch_id' => $id]);
        //end
        return $this->redirect(['index']);
    }

    /**
     * Finds the Disburs ementBatch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DisbursementBatch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        if (($model = DisbursementBatch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionOfficersubmit($id) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        // $model = new \backend\modules\disbursement\models\PayoutlistMovement();

        //if ($model->load(Yii::$app->request->post())) {
        //find data
        //find the title of user
        //$modeltitle= \backend\modules\disbursement\models\base\DisbursementUserStructure::findOne(['user_id'=>$model->to_officer]);
        // $model->to_officer=$modeltitle->disbursement_user_structure_id;
        //$model->save();
        $models = DisbursementBatch::findone($id);
        $models->is_submitted = 1;
        if($models->save()){
            $user = Yii::$app->user->id;
            $now = date('Y-m-d');
            $amount = $models->Amount;
            if (Module::MovementGenerator($models->disbursement_batch_id,$user,$amount)){
                $sms="<p>Pay List Movement Initiated successfully</p>";
                $alert = 'success';
            }else{
                $sms="<p>FAILED to Initiated Pay List Movement</p>";
                $alert = 'danger';
            }



            Yii::$app->getSession()->setFlash($alert, $sms);

        }
        //end
        return $this->redirect(['view', 'id' => $models->disbursement_batch_id]);
        /* } else {
             return $this->render('../payoutlist-movement/create', [
                         'model' => $model,
                         'disbursementId' => $id,
             ]);
         }*/
    }

    public function actionExecutiveDirector($id) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $model = new \backend\modules\disbursement\models\PayoutlistMovement();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->disbursements_batch_id]);
        } else {
            return $this->render('../payoutlist-movement/create', [
                'model' => $model,
                'disbursementId' => $id,
            ]);
        }
    }

    public function actionReviewDisbursement() {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $searchModel = new DisbursementBatchSearch();
        $dataProvider = $searchModel->searchreviewdisbursement(Yii::$app->request->queryParams,'0');

        return $this->render('review_disbursement', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionManagerLoanDisbursement($id) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $model = new \backend\modules\disbursement\models\PayoutlistMovement();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->disbursements_batch_id]);
        } else {
            return $this->render('../payoutlist-movement/create', [
                'model' => $model,
                'disbursementId' => $id,
            ]);
        }
    }

    public function actionDirectorDisbursement($id) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $model = new \backend\modules\disbursement\models\PayoutlistMovement();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->disbursements_batch_id]);
        } else {
            return $this->render('../payoutlist-movement/create', [
                'model' => $model,
                'disbursementId' => $id,
            ]);
        }
    }

    public function actionAssistanceDisbursement($id) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $model = new \backend\modules\disbursement\models\PayoutlistMovement();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->disbursements_batch_id]);
        } else {
            return $this->render('../payoutlist-movement/create', [
                'model' => $model,
                'disbursementId' => $id,
            ]);
        }
    }

    public function actionViewreviewb($id) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        return $this->render('view-review-disbursement', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionReviewallDisbursement() {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $searchModel = new DisbursementBatchSearch();
        $dataProvider = $searchModel->searchreviewdisbursement(Yii::$app->request->queryParams,0,1);

        return $this->render('reviewall_disbursement', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewreviewall($id) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        //return $this->render('view-reviewall-disbursement', [
        return $this->render('batch_review', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDisbursementVerify($id, $status) {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $model = new \backend\modules\disbursement\models\PayoutlistMovement();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //find data
            $models = DisbursementBatch::findone($model->disbursements_batch_id);
            $models->is_approved = $status;
            $models->save();
            //end 
            return $this->redirect(['viewreviewb', 'id' => $model->disbursements_batch_id]);
        } else {
            if ($status == 0) {
                return $this->render('../payoutlist-movement/create1', [
                    'model' => $model,
                    'disbursementId' => $id,
                ]);
            } else {
                return $this->render('../payoutlist-movement/create', [
                    'model' => $model,
                    'disbursementId' => $id,
                ]);
            }
        }
    }

    public function actionReviewDecision($id, $level, $action){
        ini_set('max_execution_time','-1');
        ini_set('memory_limit', '-1');
        $user=Yii::$app->user->id;
        $now = date('Y-m-d H:i:s');
        $batchID = $id;
        $currentMovement = Module::MovementQuery($batchID,'current');

        if (sizeof($currentMovement)!=0)
        {
            $currentMovementID = $currentMovement[0]['movement_id'];
            $model = PayoutlistMovement::findOne($currentMovementID);
            $preVious = Module::PreviousMovements($batchID,$currentMovementID);

            if ($model->load(Yii::$app->request->post())) {

                //update movement
                $model->to_officer=$user;
                $model->movement_status=$action;
                $model->date_out = date("Y-m-d");
                $model->updated_at = date("Y-m-d");
                $model->updated_by = $user;
                if (sizeof($preVious)==0){
                    $model->from_officer = $model->created_by;

                }else{
                    $model->from_officer = $preVious[0]['to_officer'];
                }
                if($model->save())
                {
                    Module::MovementMatrix($batchID,$currentMovementID,$action);

                }


                return $this->redirect(['reviewall-disbursement', 'id' => $model->disbursements_batch_id]);
            } else {
                return $this->render('../payoutlist-movement/createall', [
                    'model' => $model,
                    'disbursementId' => $id,
                    'level' => $level,
                    'action' => $action,
                ]);
            }

        }


    }

    //payout-list-download
    public function actionPayoutListDownload($batch_id,$mode)
    {
        switch (strtoupper($mode))
        {
            case "PDF":
                ini_set('max_execution_time','-1');

                ini_set('memory_limit', '-1');
                $model = DisbursementBatch::findOne($batch_id);
                                $content=$this->render('printOut', [
                    'model' =>$model,


                ]);
                $pdf = Yii::$app->pdf;
                $pdf->content = $content;
                return $pdf->render();
                break;

            case "EXCEL":
               /* $model=  AllocationBatch::findOne($allocation_batch_id);
                $modelall= Yii::$app->db->createCommand("SELECT current_study_year, programme_cost,ownership,registration_number ,`firstname`, `middlename`,apl.application_id, `surname`,institution_name,institution_code,u.sex as sex,current_study_year, `f4indexno`,apl.`programme_id`,SUM(`allocated_amount`) as amount_total,`allocation_batch_id`,programme_name,programme_code,pr.learning_institution_id ,`cluster_name`,li.`ownership`,`needness`, `myfactor`, `ability`, `fee_factor`, `student_fee` FROM `user` u join applicant ap on u.`user_id`=ap.`user_id` join application apl on apl.`applicant_id`=ap.`applicant_id` join allocation alls on alls.`application_id`=apl.`application_id` join programme pr on pr.`programme_id`=apl.`programme_id` join cluster_programme clp on clp.programme_id=pr.programme_id join cluster_definition clud on clud.`cluster_definition_id`=clp.cluster_definition_id join `learning_institution` li on li.`learning_institution_id`=pr.`learning_institution_id` WHERE allocation_batch_id='{$allocation_batch_id}' group by alls.`application_id`,`allocation_batch_id` order by pr.learning_institution_id;
")->queryAll();
                $content=$this->render('allocation-batch-detail_excel', [
                    'model' =>$model,
                    'modelall' =>$modelall,

                ]);
                $pdf = Yii::$app->pdf;
                $pdf->content = $content;*/
                break;
        }
    }

    public function actionMemoDownload($batch_id)
    {

                ini_set('max_execution_time','-1');

                ini_set('memory_limit', '-1');
                $model = DisbursementBatch::findOne($batch_id);
                $content=$this->render('_internal_memo', [
                    'model' =>$model,


                ]);
                $pdf = Yii::$app->pdf;
                $pdf->content = $content;
                return $pdf->render();

    }

    public function actionMisDisbursementDownload($batch_id)
    {
        ini_set('max_execution_time','-1');
        ini_set('memory_limit', '-1');

        $model = DisbursementBatch::findOne($batch_id);
        $content=$this->render('misDisbursed', [
            'model' =>$model,
        ]);
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        return $pdf->render();
    }
}
