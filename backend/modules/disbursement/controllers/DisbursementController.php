<?php

namespace backend\modules\disbursement\controllers;

use Yii;
use backend\modules\disbursement\models\Disbursement;
use backend\modules\disbursement\models\DisbursementSearch;
//use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Controller;
use yii\data\SqlDataProvider;
use backend\modules\disbursement\Module;
/**
 * DisbursementController implements the CRUD actions for Disbursement model.
 */
class DisbursementController extends Controller
{
    /**
     * @inheritdoc
     */
     public $layout = "main_private";
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Disbursement models.
     * @return mixed
     */
    public function actionIndex()
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');

        $searchModel = new DisbursementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBatchPayOutList($batchID)
    {
        ini_set('max_execution_time','-1');
        ini_set('memory_limit', '-1');

        $option="SQL";
        $sql = Module::PayoutList($option,$academicYear=null,$institution=null,$semester=null,$instalment=null,$headerID=$batchID,$programme=null);

        $count = sizeof(Yii::$app->db->createCommand($sql)->queryAll());
        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $count,
        ]);

        return $this->render('pay_out_list', [
               'dataProvider' => $dataProvider,
        ]);


    }

    public function actionPayListSearch()
    {
        ini_set('max_execution_time','-1');
        ini_set('memory_limit', '-1');
        $WHERE = "";
        $SQL = '';
        $model = array();
        if (Yii::$app->request->isAjax) {
            
            $string = $_POST['search'];
            $case = $_POST['case'];
            switch ($case)
            {
                case "INSTITUTION":
                    $WHERE.= 'learning_institution.institution_code = "'.$string.'"';
                    break;

                case "HEADER":
                    $WHERE.= 'disbursement_batch.disbursement_batch_id = "'.$string.'" OR disbursement_batch.batch_number = "'.$string.'"';
                    break;

            }

            $SQL = '
                SELECT 
                 disbursement_batch.disbursement_batch_id AS "batch_id",
                 disbursement_batch.batch_number AS "header_id",
                 loan_item.item_code AS "item_code",
                 loan_item.item_name AS "item_name",
                 learning_institution.institution_code AS "institution_code",
                 learning_institution.institution_name AS "institution_name",
                 academic_year.academic_year AS "academic_year",
                 financial_year.financial_year AS "financial_year",
                 semester.semester_number AS "semester_number",
                 instalment_definition.instalment AS "instalment_number",
                 instalment_definition.instalment_desc AS "instalment_name",
                 applicant_category.applicant_category AS "category_name",
                 applicant_category.applicant_category_code AS "category_code",
                 disbursement_batch.instalment_type AS "instalment_type",
                 disbursement_batch.version AS "version",
                 disbursement_batch.disburse_type AS "disburse_type",
                 (CASE WHEN disbursement_batch.disbursed_as = "1" THEN "LOAN" ELSE "GRANT" END) AS "disbursement_mode",
                 (CASE WHEN disbursement_batch.applicant_category_id = "1" THEN "LOCAL" ELSE "OVERSEAS" END) AS "study_location"
                FROM disbursement_batch 
                LEFT JOIN academic_year ON disbursement_batch.academic_year_id = academic_year.academic_year_id
                LEFT JOIN loan_item ON disbursement_batch.loan_item_id = loan_item.loan_item_id
                LEFT JOIN learning_institution ON disbursement_batch.learning_institution_id = learning_institution.learning_institution_id
                LEFT JOIN financial_year ON disbursement_batch.financial_year_id = financial_year.financial_year
                LEFT JOIN semester ON disbursement_batch.semester_number = semester.semester_id
                LEFT JOIN instalment_definition ON disbursement_batch.instalment_definition_id = instalment_definition.instalment_definition_id
                LEFT JOIN applicant_category ON disbursement_batch.level = applicant_category.applicant_category_id
                WHERE '.$WHERE.';';

            $model = Yii::$app->db->createCommand($SQL)->queryAll();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results'=>$model, 'sql'=>$SQL),
            ];
        } else throw new BadRequestHttpException;


    }

    public function actionLoadPayList()
    {
        ini_set('max_execution_time','-1');
        ini_set('memory_limit', '-1');

        if (Yii::$app->request->isAjax) {

            $batchID = $_POST['batch_id'];
            $summary = Module::PLS($batchID);
            $paylist= Module::PAY_LIST_PRINT_OUT($batchID);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('paylist'=>$paylist, 'summary'=>$summary),
            ];
        } else throw new BadRequestHttpException;


    }

    public function actionPayOutList()
    {
        ini_set('max_execution_time','-1');
        ini_set('memory_limit', '-1');

        return $this->render('pay_out_list_searcher');

    }

    public function actionPayOutPrint($headerID)
    {

        $searchModel = new DisbursementSearch();
        $dataProvider = $searchModel->searchPayout(Yii::$app->request->queryParams,$headerID);

        return $this->render('pay_out_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionDisbursed($id)
    {
         $this->layout="default_main"; 
        $searchModel = new DisbursementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);
        $dataProvider->pagination->pageSize=0;
        return $this->render('disbursed_list', [
            'searchModel' => $searchModel,
            'id'=>$id,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSharedList($id)
    {
        $this->layout="default_main";
        $searchModel = new DisbursementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);
        $dataProvider->pagination->pageSize=0;
        return $this->render('shared_list', [
            'searchModel' => $searchModel,
            'id'=>$id,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionMydisbursement($id)
    {
         $this->layout="default_main"; 
        $searchModel = new DisbursementSearch();
        $dataProvider = $searchModel->searchmy(Yii::$app->request->queryParams,$id);
        $dataProvider->pagination->pageSize=0;
        return $this->render('mydisbursement', [
            'searchModel' => $searchModel,
            'id'=>$id,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single Disbursement model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
         $this->layout="default_main"; 
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Disbursement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Disbursement();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->disbursement_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'id'=>$id
            ]);
        }
    }

    /**
     * Updates an existing Disbursement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
            $this->layout="default_main"; 
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                  
            return $this->redirect(['view', 'id' => $model->disbursement_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Disbursement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Disbursement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Disbursement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Disbursement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
