<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/27/18
 * Time: 1:54 PM
 */

namespace backend\modules\disbursement\controllers;
use backend\modules\disbursement\models\SuspensionBatch;
use backend\modules\disbursement\models\SuspensionBatchSearch;
use backend\modules\disbursement\models\SuspensionStaging;
use backend\modules\disbursement\models\SuspensionStagingSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\disbursement\Module;

class BulkSuspensionController extends Controller
{
    public $layout="main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DisbursementSuspension models.
     * @return mixed
     */
    public function actionIndex()
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        $searchModel = new SuspensionBatchSearch();
        $dataProvider = $searchModel->SpecialSearch(Yii::$app->request->queryParams,"SUSPENSION");

        return $this->render('../suspension-batch/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStagingList($batchID,$status)
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        $this->layout='default_main';
        $searchModel = new SuspensionStagingSearch();
        $dataProvider = $searchModel->SpecialSearch(Yii::$app->request->queryParams,$batchID,$status);

        return $this->render('../suspension-batch/staging_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'batchID' => $batchID,
            'status' => $status,
        ]);
    }

    public function actionView($id)
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        $model = $this->findModel($id);
        $action = strtolower($model->type);
        return $this->render('../'.$action.'-batch/view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new SuspensionBatch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        $model = new SuspensionBatch();
        $modelImport = new \yii\base\DynamicModel([
            'fileImport'=>'File Import',
        ]);
        $modelImport->addRule(['fileImport'],'required');
        $modelImport->addRule(['fileImport'],'file',['extensions'=>'ods,xls,xlsx'],['maxSize'=>1024*1024]);
        if ($model->load(Yii::$app->request->post())) {
            $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport,'fileImport');
            $user = Yii::$app->user->id;
            $now = date('Y-m-d H:i:s');
            $model->created_by = $user;
            $model->created_at = $now;
            $model->updated_by = $user;
            $model->updated_at = $now;
            $model->status = '0';
            $model->status_date = $now;


            if ($modelImport->fileImport){
                $inputFileType = \PHPExcel_IOFactory::identify($modelImport->fileImport->tempName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($modelImport->fileImport->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                $baseRow = $factor= 3;
                $totalRows = (sizeof($sheetData) - ($baseRow-1));
                /*print_r($totalRows);
                exit();*/
                if ($totalRows!==0){
                    if ($model->save()){
                        $output =  Module::UploadBulkSuspension($sheetData,$baseRow,$model->id);
                        // $output = array('new'=>$totalRows,'updates'=>intval($totalRows/3));
                        Yii::$app->getSession()->setFlash('success',"<b>BULK DISBURSEMENT SUSPENSION </b> has : <b>".number_format($output['new'],0)." NEW</b> records & <b>".number_format($output['updates'],0)."</b> records have been <b>UPDATED</b> under the header ID <b>$model->id</b>");
                        return $this->redirect(['view', 'id' => $model->id]);
                    }else{
                        Yii::$app->getSession()->setFlash('error',"Save Error".json_encode($model->getErrors()));

                    }
                }else{
                    Yii::$app->getSession()->setFlash('error',"You've just uploaded an empty sheet... Please make sure the sheet is not empty");
                }

            }else{
                Yii::$app->getSession()->setFlash('error','Something is wrong with the file imported... Please check again and try to re-import again');
            }


        } else {
            $model->status = '0'; // NEW
            $model->type = 'SUSPENSION'; // SUSPENSION
            return $this->render('../suspension-batch/create', [
                'model' => $model,
                'modelImport'=>$modelImport,
                'type'=>'SUSPENSION'
            ]);
        }

    }

    /**
     * Updates an existing SuspensionBatch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        $model = $this->findModel($id);

        $modelImport = new \yii\base\DynamicModel([
            'fileImport'=>'File Import',
        ]);
        $modelImport->addRule(['fileImport'],'required');
        $modelImport->addRule(['fileImport'],'file',['extensions'=>'ods,xls,xlsx'],['maxSize'=>1024*1024]);
        if ($model->load(Yii::$app->request->post())) {
            $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport,'fileImport');
            $user = Yii::$app->user->id;
            $now = date('Y-m-d H:i:s');

            $model->updated_by = $user;
            $model->updated_at = $now;
            $model->status_date = $now;


            if ($modelImport->fileImport){
                $inputFileType = \PHPExcel_IOFactory::identify($modelImport->fileImport->tempName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($modelImport->fileImport->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                $baseRow = $factor= 3;
                $totalRows = (sizeof($sheetData) - ($baseRow-1));
                /*print_r($totalRows);
                exit();*/
                if ($totalRows!==0){
                    if ($model->save()){
                        $output =  Module::UploadBulkSuspension($sheetData,$baseRow,$model->id);
                        // $output = array('new'=>$totalRows,'updates'=>intval($totalRows/3));
                        Yii::$app->getSession()->setFlash('success',"<b>BULK DISBURSEMENT SUSPENSION </b> has : <b>".number_format($output['new'],0)." NEW</b> records & <b>".number_format($output['updates'],0)."</b> records have been <b>UPDATED</b> under the header ID <b>$model->id</b>");
                        return $this->redirect(['view', 'id' => $model->id]);
                    }else{
                        Yii::$app->getSession()->setFlash('error',"Save Error".json_encode($model->getErrors()));

                    }
                }else{
                    Yii::$app->getSession()->setFlash('error',"You've just uploaded an empty sheet... Please make sure the sheet is not empty");
                }

            }else{
                Yii::$app->getSession()->setFlash('error','Something is wrong with the file imported... Please check again and try to re-import again');
            }


        } else {
            //$model->status = '0'; // NEW
            $model->type = 'SUSPENSION'; // SUSPENSION
            return $this->render('../suspension-batch/update', [
                'model' => $model,
                'modelImport'=>$modelImport,
                'type'=>'SUSPENSION'
            ]);
        }
    }
    public function actionUpdateStaging($id)
    {
        //$this->layout='default_main';
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        $model = $this->findStagingModel($id);
        if ($model->load(Yii::$app->request->post())) {
            //$output =
            if ($model->save()){
                Yii::$app->getSession()->setFlash('success',"A Record with an index number <b>$model->index_number</b> has been Updated Successfully");
                return $this->redirect(['staging', 'batchID' => $model->header_id,'status'=>$model->status]);

            }else{
                Yii::$app->getSession()->setFlash('error',"Save Error".json_encode($model->getErrors()));
            }

        }
        return $this->render('../suspension-batch/update_staging', [
            'model' => $model,
        ]);
    }
    /**
     * Deletes an existing SuspensionBatch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $SQL="DELETE FROM suspension_staging WHERE header_id='$id';";
         Yii::$app->db->createCommand($SQL)->execute();

        $model->delete();

        return $this->redirect(['index']);
    }
    public function actionDeleteStaging($id)
    {
        $model=$this->findStagingModel($id);
        $batch_id = $model->header_id;
        $status = $model->status;
        $model->delete();

        //return $this->redirect(['index']);
        //return $this->redirect(['index']);
        //return $this->redirect(['view', 'id' => $batch_id]);
        return $this->redirect(['staging-list', 'batchID' => $batch_id, 'status'=>$status]);
    }
    /**
     * Finds the SuspensionBatch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SuspensionBatch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SuspensionBatch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findStagingModel($id)
    {
        if (($model = SuspensionStaging::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}