<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/20/18
 * Time: 10:50 AM
 */

namespace backend\modules\disbursement\controllers;
use Yii;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\disbursement\Module;

use yii\web\Controller;

class CustomerSearchController extends Controller
{
    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AdjustmentReason models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    public function actionDownloadStatement()
    {
        ini_set('max_execution_time','-1');
        ini_set('memory_limit', '-1');

        //$indexNumber,$YoS=null,$startDate=null,$endDate=null,$instalment=null
        if (Yii::$app->request->isAjax) {
            if (isset($_POST['index_number'])&&!empty($_POST['index_number'])&&$_POST['index_number']!=''){ $indexNumber = $_POST['index_number']; }else{ $indexNumber=null; }
            if (isset($_POST['YoS'])&&!empty($_POST['YoS'])&&$_POST['YoS']!=''){ $YoS = $_POST['YoS']; }else{ $YoS=null; }
            if (isset($_POST['start_date'])&&!empty($_POST['start_date'])&&$_POST['start_date']!=''){ $startDate = $_POST['start_date']; }else{ $startDate=null; }
            if (isset($_POST['end_date'])&&!empty($_POST['end_date'])&&$_POST['end_date']!=''){ $endDate = $_POST['end_date']; }else{ $endDate=null; }
            if (isset($_POST['instalment'])&&!empty($_POST['instalment'])&&$_POST['instalment']!=''){ $instalment = $_POST['instalment']; }else{ $instalment=null; }

            $content=$this->render('customer_statement', [
                'model' =>Module::CustomerStatementQuery($indexNumber,$YoS,$startDate,$endDate,$instalment),
            ]);
            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            return $pdf->render();
            //$output = Yii::$app->db->createCommand($SQL)->queryAll();


           // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
           // return ['output'=>$output];
        } else throw new BadRequestHttpException;
    }

    public function actionPrintDocument($option,$index)
    {
        ini_set('max_execution_time','-1');
        ini_set('memory_limit', '-1');

            $content=$this->render($option, [
                'index' =>$index,
            ]);
            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            return $pdf->render();

    }

    public function actionSearchLonee(){


        if (Yii::$app->request->isAjax) {

            $output = array();

            $string = $_POST['search'];
            $option = $_POST['option'];

            switch ($option)
            {
                case 'application':
                    $output[] = Module::CustomerStatementQuery($string,$YoS=null,$startDate=null,$endDate=null,$instalment=null)['table'];
                    break;

                case 'allocation':
                    $output[] = Module::CustomerAllocationQuery($string,$YoS=null,$startDate=null,$endDate=null,$instalment=null)['table'];
                    break;

                case 'statement':
                    $output[] = Module::CustomerStatementQuery($string,$YoS=null,$startDate=null,$endDate=null,$instalment=null)['table'];
                    break;

                case 'repayment':
                    $output[] = Module::CustomerStatementQuery($string,$YoS=null,$startDate=null,$endDate=null,$instalment=null)['table'];
                    break;

                case 'return':
                    $output[] = Module::CustomerStatementQuery($string,$YoS=null,$startDate=null,$endDate=null,$instalment=null)['table'];
                    break;

                default:
                    $SQL= Module::ApplicantQuery($string);   /*"
                    SELECT 
                        applicant.f4indexno AS 'index_number',
                        application.passport_photo AS 'photo',
                        user.phone_number AS 'phone_number',
                        user.email_address AS 'email_address',
                        allocation.application_id AS 'application_id',
                        application.programme_id AS 'programme_id',
                        programme.learning_institution_id AS 'institution_id',
                        application.registration_number AS 'registration_number',
                         CONCAT(user.firstname,' ',user.middlename,' ',user.surname) AS 'full_name',
                        CASE WHEN user.sex = 'M' THEN 'MALE' ELSE 'FEMALE' END AS 'Sex',
                        DATE_FORMAT(applicant.date_of_birth,'%d/%m/%Y') AS 'DOB',
                        programme.programme_name AS 'programme_name',
                        programme_group.group_code AS 'programme_code',
                        programme.years_of_study AS 'years_of_study',
                        learning_institution.institution_name AS 'Institution',
                        learning_institution.institution_code AS 'institution_code',
                        application.student_status AS 'student_status',
                        loan_item.item_name,
                        allocation.loan_item_id,
                        application.current_study_year,
                        academic_year.academic_year,
                        allocation.allocation_batch_id,
                        IFNULL(SUM(allocation.allocated_amount),0) AS 'allocated_amount',
                        CEIL(((IFNULL(SUM(disbursement.disbursed_amount),0))/(IFNULL(SUM(allocation.allocated_amount),1)))*100) AS 'disbursed_percentage',
                        IFNULL(SUM(disbursement.disbursed_amount),0) AS 'disbursed_amount'
                        FROM allocation
                        LEFT JOIN application ON allocation.application_id = application.application_id
                        
                        
                        LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                        LEFT JOIN user ON applicant.user_id = user.user_id
                        LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                        LEFT JOIN disbursement ON allocation.application_id = disbursement.application_id AND disbursement.loan_item_id = allocation.loan_item_id
                        LEFT JOIN programme ON application.programme_id = programme.programme_id
                        LEFT JOIN programme_group ON programme.programme_group_id = programme_group.programme_group_id
                        LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                        LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                        LEFT JOIN academic_year ON allocation_batch.academic_year_id = academic_year.academic_year_id
                        LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id AND disbursement_batch.allocation_batch_id = allocation_batch.allocation_batch_id
                        WHERE applicant.f4indexno = '$string'
                        GROUP BY allocation.application_id
                        ORDER BY applicant.f4indexno ASC,academic_year.academic_year_id ASC,loan_item.item_name ASC
            ";*/




                    $output = Yii::$app->db->createCommand($SQL)->queryAll();
                    break;

            }



            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $output;
        } else throw new BadRequestHttpException;
    }
}