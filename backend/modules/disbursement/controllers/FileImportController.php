<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 9/29/18
 * Time: 11:08 AM
 */

namespace backend\modules\disbursement\controllers;

use backend\modules\disbursement\Module;
use kartik\file\FileInput;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\disbursement\models\FingerprintVerification;
use backend\modules\disbursement\models\DisbursementDepositBatch;

class FileImportController extends Controller
{
    /**
     * @inheritdoc
     */
    public $layout = "main_private";

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex(){
        ini_set('max_execution_time', 3000000);
        ini_set('memory_limit', '16000000000M');
        return $this->render('index');
    }
    public function actionView($id){
        ini_set('max_execution_time', 3000000);
        ini_set('memory_limit', '16000000000M');
        return $this->render('view',[
            'model'=>$this->findModel($id)
        ]);
    }

   /* public function actionEnrollment(){
        ini_set('max_execution_time', 3000000);
        ini_set('memory_limit', '16000000000M');
        $modelImport = new \yii\base\DynamicModel([
            'fileImport'=>'File Import',
        ]);
        $modelImport->addRule(['fileImport'],'required');
        $modelImport->addRule(['fileImport'],'file',['extensions'=>'ods,xls,xlsx'],['maxSize'=>1024*1024]);
            $totalRows = 0;
            $counter = 0;
        $analysis = array('success'=>'','error'=>'');
        if(Yii::$app->request->post()){
            $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport,'fileImport');
            $output = array();
            if($modelImport->fileImport ){
                $inputFileType = \PHPExcel_IOFactory::identify($modelImport->fileImport->tempName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($modelImport->fileImport->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                $baseRow =

                $factor= 4;
                $trOK=$trER='';
                $output = array();
                $success = array();
                $error = array();
                $summary = '';
                $totalRows = (sizeof($sheetData) - ($baseRow-1));
                //$output[] = $baseRow;
                while(!empty($sheetData[$baseRow]['B'])){
                    $model = new FingerprintVerification();



                    $now = date('Y-m-d H:i:s');
                    $user = Yii::$app->user->id;
                    $institution = $_POST['institution']; //Module::InstitutionDecoder('UDOM',false);
                    $academicYear = Module::CurrentAcademicYear();

                    $method = 'FILE'; //FILE | FINGERPRINT | OTHERS
                    $remarks = '0'; //0 = 'N/A', 1 = 'RETURN', 2 = 'DEATH', 3 = 'DISCONTINUING', 4 = 'POSTPONEMENT', 5 = 'FORGED CERTIFICATES', 6 = 'REJECTING', 7 = 'WRONGLY ALLOCATED', 8 = 'RESUMING STUDIES', 9 = 'HANGING TRANSFER', 10 = 'RESOLVED TRANSFER ISSUES',
                    $operation = 'ENROLLMENT'; //ENROLLMENT | DROPOUTS

                    $indexNo = (string)$sheetData[$baseRow]['B'];
                    $semester =  Module::SemesterInfo((string)$sheetData[$baseRow]['C'],'number','semester_id');;
                    $instalment =  Module::InstalmentInfo((string)$sheetData[$baseRow]['C'],'number','instalment_definition_id');
                    $registrationNo = (string)$sheetData[$baseRow]['E'];


                    $check = Module::CheckExistence($indexNo,$institution,$registrationNo,$academicYear);
                    $counter = ($baseRow-($factor-1));
                    if ($check["status"]){
                        $sync = $indexNo.'-'.$institution.'-'.$semester.'-'.$instalment.'-'.$academicYear.'-E';

                        $model->index_number = $indexNo;
                        $model->academic_year = $academicYear;
                        $model->institution_id = $institution;
                        $model->semester_number = $semester;
                        $model->instalment_number = $instalment;
                        $model->created_date = $now;
                        $model->created_by = $user;
                        $model->method = $method;
                        $model->remarks = $remarks;
                        $model->operation = $operation;
                        $model->sync_id = $sync;


                        if ($model->save()){
                            $NAME = Module::APPLICANT_INFO($model->index_number,"fullName");
                            $remarks = 'Success';
                            $success[$baseRow]=array('dataRow'=>$sheetData,'remarks'=>$remarks);
                            $trOK.="<tr class='text-bold text-green'><td>".$counter."</td><td> ".$indexNo." | ".$NAME."</td><td>".$model->academicYear->academic_year."</td><td>".$model->semester->semester_number."(".Module::THNumber($model->instalment->instalment)." INSTALMENT)</td><td>".$remarks."</td></tr>";
                        }else{
                            $remarks = 'FAILED: '.json_encode($model->getErrors());
                            $error[$baseRow]=array('dataRow'=>$sheetData,'remarks'=>$remarks);
                            $trER.="<tr class='text-bold text-red'><td>$counter</td><td>$indexNo</td><td>$semester</td><td>$remarks</td></tr>";
                        }
                    }else{
                        unset($check["tracing"]["GENERAL"]);
                        $remarks = '<pre><ul type="1"><li>'.implode('</li><li>',$check["tracing"]).'</li></ul></pre>';
                        $error[$baseRow]=array('dataRow'=>$sheetData,'remarks'=>$remarks);
                        $trER.="<tr class='text-bold text-red'><td>$counter</td><td>$indexNo</td><td>$remarks</td></tr>";
                    }

                    $baseRow++;
                }





                if (sizeof($error)!=0){

                    $summaryE = '<p style="font-size: 18px;">'.number_format(sizeof($error)).'/'.number_format($totalRows).' Records FAILED</p>';
                    Yii::$app->getSession()->setFlash('error',$summaryE);
                    $summary.= '<p style="font-size: 18px;">'.$summaryE.'</p>';
                }

                if (sizeof($success)!=0){
                    $summaryS = '<p style="font-size: 14px;">'.number_format(sizeof($success)).'/'.number_format($totalRows).' Records has been successfully Uploaded</p>';
                    Yii::$app->getSession()->setFlash('success',$summaryS);
                    $summary.= '<p style="font-size: 14px;">'.$summaryS.'</p>';
                }


                $analysis = array('success'=>$trOK,'error'=>$trER);
            }else{

                Yii::$app->getSession()->setFlash('error','Import error due to file format issues. Please recheck and upload gain');
            }


            Yii::$app->getSession()->setFlash('info',$summary);

        }

        return $this->render('enrollment',[
            'model' => $modelImport,
            'analysis'=>$analysis,
        ]);
    }*/

   public function actionEnrollment(){
       ini_set('max_execution_time', '-1');
       ini_set('memory_limit', '-1');
       $model = new DisbursementDepositBatch();
       $modelImport = new \yii\base\DynamicModel([
           'fileImport'=>'File Import',
       ]);
       $modelImport->addRule(['fileImport'],'required');
       $modelImport->addRule(['fileImport'],'file',['extensions'=>'ods,xls,xlsx'],['maxSize'=>1024*1024]);
       if ($model->load(Yii::$app->request->post())) {


           $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport,'fileImport');
           $user = Yii::$app->user->id;
           $now = date('Y-m-d H:i:s');
           $model->created_by = $user;
           $model->created_on = $now;
           $model->status = 'NEW';
           $model->status_date = $now;




           if ($modelImport->fileImport){
               $inputFileType = \PHPExcel_IOFactory::identify($modelImport->fileImport->tempName);
               $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
               $objPHPExcel = $objReader->load($modelImport->fileImport->tempName);
               $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
               $baseRow = $factor= 4;
               $totalRows = (sizeof($sheetData) - ($baseRow-1));
               /*print_r($totalRows);
               exit();*/
               if ($totalRows!==0){
                   if ($model->save()){
                       $output =  Module::UploadBulkDeposits($sheetData,$baseRow,$model->id);
                       // $output = array('new'=>$totalRows,'updates'=>intval($totalRows/3));
                       Yii::$app->getSession()->setFlash('success',"<b>BULK DISBURSEMENT DEPOSIT </b> has : <b>".number_format($output['new'],0)." NEW</b> records & <b>".number_format($output['updates'],0)."</b> records have been <b>UPDATED</b> under the header ID <b>$model->id</b>");
                       return $this->redirect(['view', 'id' => $model->id]);
                   }else{
                       Yii::$app->getSession()->setFlash('error',"Save Error".json_encode($model->getErrors()));

                   }
               }else{
                   Yii::$app->getSession()->setFlash('error',"You've just uploaded an empty sheet... Please make sure the sheet is not empty");
               }

           }else{
               Yii::$app->getSession()->setFlash('error','Something is wrong with the file imported... Please check again and try to re-import again');
           }


       } else {
           $model->status = 'NEW'; // NEW
           return $this->render('../disbursement-deposit-batch/create', [
               'model' => $model,
               'modelImport'=>$modelImport,
           ]);
       }


   }

    public function actionDropOuts(){
        ini_set('max_execution_time', 3000000);
        ini_set('memory_limit', '16000000000M');

        $modelImport = new \yii\base\DynamicModel([
            'fileImport'=>'File Import',
        ]);
        $modelImport->addRule(['fileImport'],'required');
        $modelImport->addRule(['fileImport'],'file',['extensions'=>'ods,xls,xlsx'],['maxSize'=>1024*1024]);
        $totalRows = 0;
        $counter = 0;
        $analysis = array('success'=>'','error'=>'');
        if(Yii::$app->request->post()){
            $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport,'fileImport');
            $output = array();
            if($modelImport->fileImport /*&& $modelImport->validate()*/){
                $inputFileType = \PHPExcel_IOFactory::identify($modelImport->fileImport->tempName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($modelImport->fileImport->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                $baseRow =

                $factor= 4;
                $trOK=$trER='';
                $output = array();
                $success = array();
                $error = array();
                $summary = '';
                $totalRows = (sizeof($sheetData) - ($baseRow-1));
                //$output[] = $baseRow;
                while(!empty($sheetData[$baseRow]['B'])){
                    $model = new FingerprintVerification();

                    /** @property integer $id
                     * @property string $index_number
                     * @property integer $academic_year
                     * @property integer $institution_id
                     * @property integer $semester_number
                     * @property string $created_date
                     * @property string $method
                     * @property integer $remarks
                     * @property string $operation*/
                    /*$model->title = (string)$sheetData[$baseRow]['B'];
                    $model->description = (string)$sheetData[$baseRow]['C'];*/

                    $now = date('Y-m-d H:i:s');
                    $user = Yii::$app->user->id;
                    $institution = $_POST['institution']; //Module::InstitutionDecoder('UDOM',false);
                    $academicYear = Module::CurrentAcademicYear();
                    $method = 'FILE'; //FILE | FINGERPRINT | OTHERS
                    //$remarks = '0'; //0 = 'N/A', 1 = 'RETURN', 2 = 'DEATH', 3 = 'DISCONTINUING', 4 = 'POSTPONEMENT', 5 = 'FORGED CERTIFICATES', 6 = 'REJECTING', 7 = 'WRONGLY ALLOCATED', 8 = 'RESUMING STUDIES', 9 = 'HANGING TRANSFER', 10 = 'RESOLVED TRANSFER ISSUES',
                    $operation = 'DROPOUTS'; //ENROLLMENT | DROPOUTS

                    $indexNo = (string)$sheetData[$baseRow]['B'];
                    $semester = (string)$sheetData[$baseRow]['C'];
                    $remarks = (string)$sheetData[$baseRow]['D'];


                    //$check = Module::CheckExistence($indexNo,$institution,$academicYear);
                    $check = Module::CheckExistence($indexNo,$institution,null,$academicYear);
                    $counter = ($baseRow-($factor-1));
                    if ($check){
                        $sync = $indexNo.'-'.$institution.'-'.$semester.'-'.$academicYear.'-D';

                        $model->index_number = $indexNo;
                        $model->academic_year = $academicYear;
                        $model->institution_id = $institution;
                        $model->semester_number = $semester;
                        $model->created_date = $now;
                        $model->created_by = $user;
                        $model->method = $method;
                        $model->remarks = $remarks;
                        $model->operation = $operation;
                        $model->sync_id = $sync;
                        Module::AutoSuspendAll($indexNo,null,$remarks);

                        if ($model->save()){
                            $remarks = 'Success';
                            $success[$baseRow]=array('dataRow'=>$sheetData,'remarks'=>$remarks);
                            $trOK.="<tr class='text-bold text-green'><td>".$counter."</td><td>$indexNo</td><td>$semester</td><td>$remarks</td></tr>";
                        }else{
                            $remarks = 'FAILED: '.json_encode($model->getErrors());
                            $error[$baseRow]=array('dataRow'=>$sheetData,'remarks'=>$remarks);
                            $trER.="<tr class='text-bold text-red'><td>$counter</td><td>$indexNo</td><td>$semester</td><td>$remarks</td></tr>";
                        }
                    }else{
                        $remarks = 'FAILED: Index Number Not Recognized';
                        $error[$baseRow]=array('dataRow'=>$sheetData,'remarks'=>$remarks);
                        $trER.="<tr class='text-bold text-red'><td>$counter</td><td>$indexNo</td><td>$semester</td><td>$remarks</td></tr>";
                    }

                    $baseRow++;
                }





                if (sizeof($error)!=0){

                    $summaryE = '<p style="font-size: 18px;">'.number_format(sizeof($error)).'/'.number_format($totalRows).' Records FAILED</p>';
                    Yii::$app->getSession()->setFlash('error',$summaryE);
                    $summary.= '<p style="font-size: 18px;">'.$summaryE.'</p>';
                }

                if (sizeof($success)!=0){
                    $summaryS = '<p style="font-size: 14px;">'.number_format(sizeof($success)).'/'.number_format($totalRows).' Records has been successfully Uploaded</p>';
                    Yii::$app->getSession()->setFlash('success',$summaryS);
                    $summary.= '<p style="font-size: 14px;">'.$summaryS.'</p>';
                }


                $analysis = array('success'=>$trOK,'error'=>$trER);
            }else{

                Yii::$app->getSession()->setFlash('error','Import error due to file format issues. Please recheck and upload gain');
            }


            Yii::$app->getSession()->setFlash('info',$summary);

        }

        return $this->render('dropout',[
            'model' => $modelImport,
            'analysis'=>$analysis,
        ]);

    }


    /**
     * Finds the DisbursementDepositBatch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DisbursementDepositBatch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DisbursementDepositBatch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}