<?php

namespace backend\modules\disbursement\controllers;

use backend\modules\disbursement\Module;
use Yii;
use backend\modules\disbursement\models\InstitutionFundRequest;
use backend\modules\disbursement\models\InstitutionFundRequestSearch;
use backend\modules\disbursement\models\InstitutionPaylistSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InstitutionFundRequestController implements the CRUD actions for InstitutionFundRequest model.
 */
class InstitutionFundRequestController extends Controller
{
    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InstitutionFundRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InstitutionFundRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InstitutionFundRequest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model =  $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {


            $now = date("Y-m-d H:i:s");
            $user = Yii::$app->user->id;
            $model->request_status = $_POST['InstitutionFundRequest']['approval'];
            $model->status_date = $now;
            $model->status_by = $user;
            $fileName='FUND_REQUEST_'.time().'_'.$user;
            $fileDestination = '../attachments/disbursement/';
            $savedName = Module::UploadFile($model,'file',$fileName,$fileDestination);
            $document=array();

            if (!empty($model->official_document)&& $model->official_document!=''){
                $oldAttachments = (ARRAY)json_decode($model->official_document);
                $attIndex= sizeof($oldAttachments);
                $oldAttachments[$attIndex]=$savedName;
                $document=$oldAttachments;
            }else{

                $document[]=$savedName;
            }

            $model->official_document = json_encode($document);

            if ($model->save()) {
                if ($model->approval=='3'){ //Approved
                    $sms="<p>Request has been successful APPROVED for proceeding with Disbursement Process</p>";
                    $alert = 'success';
                }else{
                    $sms="<p>Request has been DENIED from proceeding with Disbursement Process</p>";
                    $alert = 'danger';
                }
                Module::UpdatePaylistStatus($model->id,$model->approval);
                Yii::$app->getSession()->setFlash($alert, $sms);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }else{
            return $this->render('view', [
                'model' =>$model,
            ]);
        }

    }

    /**
     * Creates a new InstitutionFundRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InstitutionFundRequest();

        if ($model->load(Yii::$app->request->post())) {

            //created_at,created_by
            $now = date("Y-m-d H:i:s");
            $user = Yii::$app->user->id;

            $model->submitted = 0; //New/Not Submitted
            $model->created_at = $now;
            $model->created_by = $user;

            $model->request_status = 0; //New
            $model->status_date = $now;
            $model->status_by = $user;
            if ($model->save()) {
                $institution=$model->institution_id;
                $academicYear=$model->academic_year;
                $loanItem=$model->loan_item;
                $requestID=$model->id;
                $semster=$model->semester_number;
                Module::GeneratePayList($institution,$academicYear,$loanItem,$semster,$requestID);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);

    }


    public function actionPayList($id)
    {
        $this->layout="default_main";
        $searchModel = new InstitutionPaylistSearch();
        $dataProvider = $searchModel->payListSearch(Yii::$app->request->queryParams,$id);

        $model = $this->findModel($id);

        $itemTitle = "Pay List for ".$model->LoanItem.' for '.$model->AcademicYear;
        return $this->render('_payList', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id'=>$id,
            'itemTitle'=>$itemTitle,
            'item'=>$model->LoanItem
        ]);

    }
    public function actionGenerate()
    {
        if (Yii::$app->request->isAjax) { //Making sure its an ajax request

            $institution = $_POST['institution'];
            $loan_item = $_POST['loan_item'];
            $academic_year = $_POST['academic_year'];
            $semester = $_POST['semester_id'];

            $output=array(
                /*'allocation'=>Module::PayListAnalysis($institution,$academic_year,$loan_item,'allocation'),
                'reported'=>Module::PayListAnalysis($institution,$academic_year,$loan_item,'reported'),
                'batchReady'=>Module::PayListAnalysis($institution,$academic_year,$loan_item,'batchReady'),
                'paylist'=>Module::PayListAnalysis($institution,$academic_year,$loan_item,'paylist'),*/

               /* 'allocation'=>Module::PayListAnalysis($institution,$academic_year,$loan_item,'allocation'),
                'reported'=>Module::PayListAnalysis($institution,$academic_year,$loan_item,'reported'),*/
                'allocation'=>Module::PayListAnalysis($institution,$academic_year,$loan_item,$semester,'batchReady'),
                'paylist'=>Module::PayListAnalysis($institution,$academic_year,$loan_item,$semester,'paylist'),
            );

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => $output,
            ];
        } else throw new BadRequestHttpException;
    }

    /**
     * Updates an existing InstitutionFundRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $institution=$model->institution_id;
            $academicYear=$model->academic_year;
            $loanItem=$model->loan_item;
            $requestID=$model->id;
            $semster=$model->semester_number;
            Yii::$app->db->createCommand("DELETE FROM institution_paylist WHERE request_id='$requestID'")->execute();
            Module::GeneratePayList($institution,$academicYear,$loanItem,$semster,$requestID);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionSubmit($id)
    {
        $model = $this->findModel($id);
        $now = date("Y-m-d H:i:s");
        $user = Yii::$app->user->id;
        $model->submitted = 1; //Submitted
        $model->submitted_by = $user;
        $model->submitted_on = $now;

        $model->request_status = 2; //Submitted
        $model->status_date = $now;
        $model->status_by = $user;

        if ($model->save()) {



            Module::UpdatePaylistStatus($model->id,2);


            $sms="<p>Request successful Submitted for Approval Process</p>";
            Yii::$app->getSession()->setFlash('success', $sms);
            return $this->redirect(['view', 'id' => $model->id]);


        } else {
            $sms="<p>Request Submission FAILED: ".json_encode($model->getErrors())."</p>";
            Yii::$app->getSession()->setFlash('danger', $sms);
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    public function actionPayListApproval($id)
    {

        $model = $this->findModel($id);
        $now = date("Y-m-d H:i:s");
        $user = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {

            $model->request_status = $_POST['InstitutionFundRequest']['approval'];
            $model->status_date = $now;
            $model->status_by = $user;

            if ($model->save()) {
                if ($model->approval=='3'){ //Approved
                    $sms="<p>Request has been successful APPROVED for proceeding with Disbursement Process</p>";
                    $alert = 'success';
                }else{
                    $sms="<p>Request has been DENIED from proceeding with Disbursement Process</p>";
                    $alert = 'danger';
                }

                Module::UpdatePaylistStatus($model->id,$model->request_status);
                Yii::$app->getSession()->setFlash($alert, $sms);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('approval', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing InstitutionFundRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
       $model->delete();

        $SQL = "DELETE FROM institution_paylist WHERE request_id = '$id';";
        Yii::$app->db->createCommand($SQL)->execute();
        return $this->redirect(['index']);
    }

    public function actionListDownload($requestID,$mode)
    {
        switch (strtoupper($mode))
        {
            case "PDF":
                ini_set('max_execution_time','-1');

                ini_set('memory_limit', '-1');
                $model = InstitutionFundRequest::findOne($requestID);
                $content=$this->render('printOut', [
                    'model' =>$model,


                ]);
                $pdf = Yii::$app->pdf;
                $pdf->content = $content;
                return $pdf->render();
                break;

            case "EXCEL":
                /* $model=  AllocationBatch::findOne($allocation_batch_id);
                 $modelall= Yii::$app->db->createCommand("SELECT current_study_year, programme_cost,ownership,registration_number ,`firstname`, `middlename`,apl.application_id, `surname`,institution_name,institution_code,u.sex as sex,current_study_year, `f4indexno`,apl.`programme_id`,SUM(`allocated_amount`) as amount_total,`allocation_batch_id`,programme_name,programme_code,pr.learning_institution_id ,`cluster_name`,li.`ownership`,`needness`, `myfactor`, `ability`, `fee_factor`, `student_fee` FROM `user` u join applicant ap on u.`user_id`=ap.`user_id` join application apl on apl.`applicant_id`=ap.`applicant_id` join allocation alls on alls.`application_id`=apl.`application_id` join programme pr on pr.`programme_id`=apl.`programme_id` join cluster_programme clp on clp.programme_id=pr.programme_id join cluster_definition clud on clud.`cluster_definition_id`=clp.cluster_definition_id join `learning_institution` li on li.`learning_institution_id`=pr.`learning_institution_id` WHERE allocation_batch_id='{$allocation_batch_id}' group by alls.`application_id`,`allocation_batch_id` order by pr.learning_institution_id;
 ")->queryAll();
                 $content=$this->render('allocation-batch-detail_excel', [
                     'model' =>$model,
                     'modelall' =>$modelall,

                 ]);
                 $pdf = Yii::$app->pdf;
                 $pdf->content = $content;*/
                break;
        }
    }

    /**
     * Finds the InstitutionFundRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InstitutionFundRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InstitutionFundRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
