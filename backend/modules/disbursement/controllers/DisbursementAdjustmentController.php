<?php

namespace backend\modules\disbursement\controllers;

use backend\modules\disbursement\Module;
use Yii;
use backend\modules\disbursement\models\DisbursementAdjustment;
use backend\modules\disbursement\models\DisbursementAdjustmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * DisbursementAdjustmentController implements the CRUD actions for DisbursementAdjustment model.
 */
class DisbursementAdjustmentController extends Controller
{
    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DisbursementAdjustment models.
     * @return mixed
     */
    public function actionIndex()
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');

        $searchModel = new DisbursementAdjustmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DisbursementAdjustment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DisbursementAdjustment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');

        $model = new DisbursementAdjustment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $type=$model->type;
            $amount = 0;
            switch ($type)
            {
                case 'DR':
                    $amount = $model->debit_amount;
                break;

                case 'CR':
                 $amount = $model->credit_amount;
                break;
            }

            $disbursementID = $model->disbursement_id;

            Module::AdjustmentEffect($type,$disbursementID,$amount);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DisbursementAdjustment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    public function actionSearchLonee(){


        if (Yii::$app->request->isAjax) {

            $output = array();
            $string = $_POST['search'];
            $SQL="
                    SELECT 
                        applicant.f4indexno AS 'index_number',
                        user.phone_number AS 'phone_number',
                        user.email_address AS 'email_address',
                        allocation.application_id AS 'application_id',
                        application.programme_id AS 'programme_id',
                        programme.learning_institution_id AS 'institution_id',
                        application.registration_number AS 'registration_number',
                        CONCAT(user.firstname,' ',user.surname) AS 'full_name',
                        CASE WHEN user.sex = 'M' THEN 'MALE' ELSE 'FEMALE' END AS 'Sex',
                        DATE_FORMAT(applicant.date_of_birth,'%d/%m/%Y') AS 'DOB',
                        programme.programme_name AS 'programme_name',
                        programme.programme_code AS 'programme_code',
                        programme.years_of_study AS 'years_of_study',
                        learning_institution.institution_name AS 'Institution',
                        learning_institution.institution_code AS 'institution_code',
                        loan_item.item_name,
                        allocation.loan_item_id,
                        application.current_study_year,
                        academic_year.academic_year,
                        allocation.allocation_batch_id,
                        IFNULL((allocation.allocated_amount),0) AS 'allocated_amount',
                        CEIL(((IFNULL((disbursement.disbursed_amount),0))/(IFNULL((allocation.allocated_amount),1)))*100) AS 'disbursed_percentage',
                        IFNULL((disbursement.disbursed_amount),0) AS 'disbursed_amount'
                        FROM allocation
                        LEFT JOIN application ON allocation.application_id = application.application_id
                        
                        
                        LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                        LEFT JOIN user ON applicant.user_id = user.user_id
                        LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                        LEFT JOIN disbursement ON allocation.application_id = disbursement.application_id AND disbursement.loan_item_id = allocation.loan_item_id
                        LEFT JOIN programme ON application.programme_id = programme.programme_id
                        LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                        LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                        LEFT JOIN academic_year ON allocation_batch.academic_year_id = academic_year.academic_year_id
                        LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id AND disbursement_batch.allocation_batch_id = allocation_batch.allocation_batch_id
                        WHERE applicant.f4indexno = '$string'
                        GROUP BY disbursement.disbursement_id
                        ORDER BY applicant.f4indexno ASC,academic_year.academic_year_id ASC,loan_item.item_name ASC
            ";


            $output = Yii::$app->db->createCommand($SQL)->queryAll();


            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['output'=>$output];
        } else throw new BadRequestHttpException;
    }

    public function actionSearchLoneeDisbursement(){


        if (Yii::$app->request->isAjax) {

            $output = array();
            $application= $_POST['application'];
            $semester = $_POST['semester'];
            $instalment = $_POST['instalment'];
            $SQL="
               SELECT 
                disbursement.disbursement_id,
                loan_item.item_name,
                loan_item.item_code,
                disbursement.loan_item_id,
                
                CONCAT(loan_item.item_name,' (',loan_item.item_code,') ',FORMAT((IFNULL(SUM(disbursement.disbursed_amount),0)),2)) AS 'item_summary',
                application.current_study_year,
                academic_year.academic_year,
                allocation.allocation_batch_id,
                IFNULL(SUM(allocation.allocated_amount),0) AS 'allocated_amount',
                CEIL(((IFNULL(SUM(disbursement.disbursed_amount),0))/(IFNULL(SUM(allocation.allocated_amount),1)))*100) AS 'disbursed_percentage',
                IFNULL(SUM(disbursement.disbursed_amount),0) AS 'disbursed_amount'
                FROM allocation
                LEFT JOIN application ON allocation.application_id = application.application_id
                
                
                LEFT JOIN applicant ON application.applicant_id = applicant.applicant_id
                LEFT JOIN user ON applicant.user_id = user.user_id
                LEFT JOIN loan_item ON allocation.loan_item_id = loan_item.loan_item_id
                LEFT JOIN disbursement ON allocation.application_id = disbursement.application_id AND disbursement.loan_item_id = allocation.loan_item_id
                LEFT JOIN programme ON application.programme_id = programme.programme_id
                LEFT JOIN learning_institution ON programme.learning_institution_id = learning_institution.learning_institution_id
                LEFT JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
                LEFT JOIN academic_year ON allocation_batch.academic_year_id = academic_year.academic_year_id
                LEFT JOIN disbursement_batch ON disbursement.disbursement_batch_id = disbursement_batch.disbursement_batch_id AND disbursement_batch.allocation_batch_id = allocation_batch.allocation_batch_id
                
                WHERE disbursement.application_id ='$application' AND disbursement.status = '0' /*IN (1,2)*/ AND disbursement_batch.semester_number = '$semester' AND disbursement_batch.instalment_definition_id = '$instalment'
                GROUP BY disbursement.disbursement_id
                ORDER BY applicant.f4indexno ASC,academic_year.academic_year_id ASC,loan_item.item_name ASC
            ";


            $output = Yii::$app->db->createCommand($SQL)->queryAll();


            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['output'=>$output];
        } else throw new BadRequestHttpException;
    }


    public function actionApplicantItems() {
        $out = [];

            if (isset($_POST['depdrop_parents'])) {
                $parents = $_POST['depdrop_parents'];
                if ($parents != null) {

                    $application= $parents[0];
                    $semester = $parents[1];
                    $instalment = $parents[2];


                $out = Module::ApplicantDisbursementItems($application,$semester,$instalment);
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionApplicantDisbursements() {
        $out = [];
        /*$application= $_POST['application'];
        $semester = $_POST['semester'];
        $instalment = $_POST['instalment'];*/
        //if (isset($_POST['application'])&&isset($_POST['semester'])&&isset($_POST['instalment'])) {
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {

                $application= $parents[0];
                $semester = $parents[1];
                $instalment = $parents[2];
                $item = $parents[3];


                $out = Module::ApplicantDisbursementItem($application,$semester,$instalment,$item);
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    /**
     * Deletes an existing DisbursementAdjustment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DisbursementAdjustment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DisbursementAdjustment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');

        if (($model = DisbursementAdjustment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
