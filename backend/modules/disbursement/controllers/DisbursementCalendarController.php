<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 7/9/18
 * Time: 3:50 PM
 */

namespace backend\modules\disbursement\controllers;



use backend\modules\allocation\models\LearningInstitution;
use backend\modules\allocation\models\Programme;
use backend\modules\disbursement\Module;
use backend\modules\disbursement\models\DisbursementCalendar;


use yii\test\InitDbFixture;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use Yii;

class DisbursementCalendarController extends Controller
{
    public $layout="main_private";

    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->loadModel($id),
        ]);
    }

    public function actionIndex(){

        $searchModel = new DisbursementCalendar();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate(){

        if (Yii::$app->request->isAjax) { //Making sure its an ajax request

            /**
                institution:institution,
                programme:programme,
                years:years,
                StartDate:StartDate,
                EndDate:EndDate,
                EventName:EventName,
                EventDescription:EventDescription,
           **/
            $institution = $_POST['institution'];
            $programme = $_POST['programme'];
            $years = $_POST['years'];
            $StartDate = $_POST['StartDate'];
            $EndDate = $_POST['EndDate'];
            $EventName = $_POST['EventName'];
            $EventDescription = $_POST['EventDescription'];


            $response="No event was recorded!";
            if (sizeof($StartDate)!=0 && sizeof($EventName)!=0){
                $response = self::ProcessMultipleInputs($institution,$programme,$years,$StartDate,$EndDate,$EventName,$EventDescription);
            }

            $output='<p style="font-size: large;">'.$response.'</p>';
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => $output,
            ];
        }else{

            return $this->render('create');
        }

    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if ($model->load(Yii::$app->request->post())) {
            //DisbursementCalendar[targeted_personnels]


            //exit();
            $model->targeted_personnels = (ARRAY)json_encode($_POST['DisbursementCalendar']['targeted_personnels']);
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public static function ProcessMultipleInputs($institution,$programme,$years,$StartDate,$EndDate,$EventName,$EventDescription)
    {
        $output = 'Operation completed Successfully';
        $now = date('Y-m-d H:i:s');
        $user = Yii::$app->user->id;
        $target = json_encode(array($user));
        $insertFields='event_name,event_description,start_date,end_date,institution_id,year_of_study,programme_id,event_priority,pre_remainder_days,targeted_personnels,created_by,created_on,updated_by,updated_on';
        $insertCounter=0;
        $updateCounter=0;

        $insertSQL=$updateSQL="";
        $name = "";
        $description = "";
        $start="";
        $end = "";


        foreach ($years as $index=>$year)
        {
            if (isset($EventName[$index])&&!empty($EventName[$index])){$name  = $EventName[$index];}else{ $name="";}
            if (isset($EventDescription[$index])&&!empty($EventDescription[$index])){$description  = $EventDescription[$index];}else{ $description="";}

            if (isset($StartDate[$index])&&!empty($StartDate[$index])){$start  = date('Y-m-d',strtotime($StartDate[$index]));}else{ $start="";}
            if (isset($EndDate[$index])&&!empty($EndDate[$index])){$end  = date('Y-m-d',strtotime($EndDate[$index]));}else{ $end="";}


            if ($name!=""&&$start!=""){
                if ($end==""){ $end=$start;}
                if ($description==""){ $description=$name;}

                $insertSQL.="INSERT INTO disbursement_calendar(".$insertFields.") VALUES ('".$name."','".$description."','".$start."','".$end."','".$institution."','".$year."','".$programme."','1','0','$target','".$user."','".$now."','".$user."','".$now."');";
                $insertCounter++;
            }
        }


        if ($insertCounter!=0){  Yii::$app->db->createCommand($insertSQL)->execute(); $output.=' <b>'.number_format($insertCounter,0).'</b> NEW Events, '; }
        if ($updateCounter!=0){  Yii::$app->db->createCommand($updateSQL)->execute(); $output.='  <b>'.number_format($updateCounter,0).'</b> Updated Events, ';}

        return $output;
    }


    public function actionChange($id){
        unset(Yii::$app->session['current_id']);
        Yii::$app->session['current_id']=$id;
        return $this->redirect(['edit']);
    }

    public function actionEdit(){

        $output = "Updating failed!";
        $id=$_POST['id'];

        $model = $this->loadModel($id);

        $model->event_name=$_POST['name'];
        $model->event_description=$_POST['description'];
        $model->event_priority=$_POST['priority'];
        $model->start_date=$_POST['start'];
        $model->end_date=$_POST['end'];


        if ($model->save()){
            $output = "Event Updated Successfully";
        }



        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'output' => $output,
        ];
    }

    public function actionDelete($id)
    {
        $model=$this->loadModel($id);
        $model->delete();
        return $this->redirect(['index']);
    }





    public function actionRecent(){
        $output = array();
        $array = array('1'=>'st','2'=>'nd','3'=>'rd');
            $limit = $_POST['limit'];
        $SQL = "SELECT * FROM disburesement_calendar WHERE DATE(start_date) >= CURDATE() ORDER BY DATE(start_date) ASC, event_priority DESC LIMIT $limit";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        foreach ($model as $index=>$dataArray)
        {
            $INST = LearningInstitution::findOne($dataArray['institution_id']);

            if (sizeof($INST)!=0){
                $institution = $INST->institution_name;
            }else{
                $institution="";
            }

            $PROG = Programme::findOne($dataArray['programme_id']);

            if (sizeof($PROG)!=0){
                $programme = $PROG->programme_name;
            }else{
                $programme="ALL PROGRAMMES";
            }
            $year = '';
            if (isset($array[$dataArray['year_of_study']])){$year=$dataArray['year_of_study'].'<sup>'.$array[$dataArray['year_of_study']].'</sup>';}else{$year=$dataArray['year_of_study'].'<sup>th</sup>';}

            $output[$dataArray['id']]=array(
                'id'=>$dataArray['id'],
                'label'=>$year.' year: '.$dataArray['event_name'],
                'name'=>$dataArray['event_name'],
                'description'=>$dataArray['event_description'],
                'institution'=>$institution,
                'programme'=>$programme,
                'color'=>Module::PriorityList($dataArray['event_priority'],'color'),
                'priority'=>Module::PriorityList($dataArray['event_priority'],'name'),
                'star'=>Module::PriorityList($dataArray['event_priority'],'star'),
                'start_date'=>date('Y-m-d',strtotime($dataArray['start_date'])),
                'end_date'=>date('Y-m-d',strtotime($dataArray['end_date'])),
                'start'=>date('D M,jS, Y',strtotime($dataArray['start_date'])),
                'end'=>date('D M,jS, Y',strtotime($dataArray['end_date'])),
                'date'=>date('d',strtotime($dataArray['start_date'])),
                'month'=>date('F',strtotime($dataArray['start_date'])),
                'year'=>date('Y',strtotime($dataArray['start_date'])),
            );
        }


        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'output' => $output,
        ];
    }






    public function actionEvents(){


        if (Yii::$app->request->isAjax) { //Making sure its an ajax request

            //Initializing Variables
            $years = 0;
            $output = array();


            //Construction Programme Filtering Conditions
            $WHERE=" WHERE 1=1 ";
            if (isset($_POST['institution_id'])&&!empty($_POST['institution_id'])){ $institution = $_POST['institution_id']; $WHERE.=" AND learning_institution_id='$institution'";}
            if (isset($_POST['programme_id'])&&!empty($_POST['programme_id'])){ $programme_id = $_POST['programme_id']; $WHERE.=" AND programme_id='$programme_id'";}



            //Constructing SQL Query for selected institution and/or programme
            $SQL = "SELECT MAX(programme.years_of_study) AS 'years_of_study',programme_name FROM programme ".$WHERE." LIMIT 1";
            $model = Yii::$app->db->createCommand($SQL)->queryAll();



            // GENERATING TABULAR MATRIX WITH INPUTS FOR CALENDAR EVENTS
            /** ******************************************************* START ***********************************************************/
            $div = $script = $link ="";

            $div = "";

            $div = "";
            $link.='<link href="../ExtraPlugins/bootstrap/datepicker/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">';
            $link.='<link href="../ExtraPlugins/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">';
          $script.='<script type="text/javascript" src="../ExtraPlugins/bootstrap/datepicker/bootstrap/js/bootstrap.min.js"></script>';
          $script.='<script type="text/javascript" src="../ExtraPlugins/bootstrap/datepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>';
          $script.='<script type="text/javascript" src="../ExtraPlugins/bootstrap/datepicker/js/locales/bootstrap-datetimepicker.sw.js" charset="UTF-8"></script>';
            $div.=$link;
            $div.=$script;
            $programmeName="All programmes";

            //Determining the maximum number of years for particular programme/programmes
            foreach ($model as $index=>$programme){
                $years=$programme['years_of_study'];
                if(isset($_POST['programme_id'])&&!empty($_POST['programme_id'])){
                    $programmeName=$programme['programme_name'];
                }else{
                    $programmeName="All programmes";
                }
            }


            // LOOP 01:  Loop in respect to number of years for a particular programme
            for($i=1;$i<=$years; $i++){

                    $sup = "";
                    switch ($i){
                        case 1:
                            $sup = "st";
                            break;

                        case 2:
                            $sup = "nd";
                            break;

                        case 3:
                            $sup = "rd";
                            break;

                        default:
                            $sup = "th";
                            break;

                    }
                    $label="<div><span style='font-size: xx-large;'>".$i."</span><sup>".$sup."</sup> <small>Year</small></div>";




                $div.='
                    <div class="col-md-12">    
                        <table class="table table-bordered table-condensed">
                            <thead class="text-bold">
                                <tr class="">
                                    <th width="100px" class="btn-primary text-left text-bold" style="font-size: large;">'.$label.'</th>';
                             $div .= '<th class="text-uppercase text-left" colspan="4"><div class="col-lg-12 text-center text-muted text-nowrap itemName" style="font-size: 10px;"></div>' . $programmeName. '</th>';
                             $div.='</tr>';

                            $div.='<tr class="bg-primary">
                                    <th width="120px" class="btn-primary text-left text-bold" style=""></th>';
                             $div .= '<th width="" class="text-uppercase text-left"><div class="col-lg-12 text-center text-muted text-nowrap itemName" style="font-size: 10px;"></div>Title</th>';
                             $div .= '<th width="" class="text-uppercase text-left"><div class="col-lg-12 text-center text-muted text-nowrap itemName" style="font-size: 10px;"></div>Description</th>';
                             $div .= '<th width="150px" class="text-uppercase text-left"><div class="col-lg-12 text-center text-muted text-nowrap itemName" style="font-size: 10px;"></div>start Date</th>';
                             $div .= '<th width="150px" class="text-uppercase text-left"><div class="col-lg-12 text-center text-muted text-nowrap itemName" style="font-size: 10px;"></div>End Date</th>';

                        $div.='</tr>
                             </thead>
                             <tbody>
                        ';




                        $div.='
                        <tr class="bg-success">
                        <th colspan="2"><input class="form-control text-bold text-left EventName"  type="text"  id="EventName'.$i.'" name="EventName[]" title="'.$i.'" placeholder="Event Name" /></th>
                        <th><input class="form-control text-italics text-left EventDescription" style="font-size: large; color: brown;" type="text" id="EventDescription'.$i.'" name="EventDescription[]" title="'.$i.'"  placeholder="Event Description" /></th>
                            <th width="200px">
                             <div class="form-group">
                                <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd MM yyyy" data-link-field="StartDate'.$i.'" title="'.$i.'">
                                    <input class="form-control startDisplay"  type="text" value="" readonly="readonly"  id="startDisplay'.$i.'" title="'.$i.'">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                </div>
                                <input type="hidden" id="StartDate'.$i.'" name="StartDate[]" value="" title="'.$i.'" />
                            </div>
                        </th>
                        
                          <th width="200px" style="text-align: right;">
                             <div class="form-group">
                                <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd MM yyyy" data-link-field="EndDate'.$i.'" title="'.$i.'">
                                    <input class="form-control"  type="text" value="" readonly="readonly"  id="endDisplay'.$i.'" title="'.$i.'">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                </div>
                                <input type="hidden" id="EndDate'.$i.'" name="EndDate[]" value="" title="'.$i.'" />
                            </div>
                        </th>
                 
                 
                        ';

                    // END of LOOP 02 (B)

                    $div.='</tr>';
                }
                // END of LOOP 03

                $div.='</tbody>
                    </table>
                </div>';

            $div.="
            <script>
             InitiateDatePicker();
</script>
            ";

                $output[]=$div;


            }
            // END OF LOOP 01: Programme years loop
            /** ******************************************************* END ***********************************************************/


            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => $output,
            ];
        }











































    protected function loadModel($id)
    {
        if (($model = DisbursementCalendar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}