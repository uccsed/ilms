<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 6/13/18
 * Time: 12:28 PM
 */

namespace backend\modules\disbursement\controllers;
use backend\modules\allocation\models\LearningInstitution;
use backend\modules\disbursement\Module;
use backend\modules\disbursement\models\DisbursementDefaultPlan;



use backend\modules\allocation\models\Programme;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\helpers\ArrayHelper;
use Yii;
use backend\modules\disbursement\models\InstalmentDefinition;
use yii\helpers\Json;
class DisbursementPlanController extends Controller
{
    public $layout="main_private";

    public function actionProgramme(){
        if (Yii::$app->request->isAjax) {

            $output = array();
            $institution = $_POST['institution_id'];
            $INST = LearningInstitution::findOne(array('learning_institution_id'=>$institution));
            //echo $institution;
            $data = Programme::findAll(array('learning_institution_id'=>$institution));

            $data = ArrayHelper::map($data, 'programme_id', 'programme_name');

            $output[]= "<option value=''>Select $INST->institution_name Programmes</option>";
            foreach ($data as $value => $programme_name) {
                $output[]= "<option value='$value'>$programme_name</option>";
            }

            //echo $output;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'output' => $output,
                    ];
        } else throw new BadRequestHttpException;

    }

    public function actionRemainingItems() {
        $out = [];
        $user=Yii::$app->user->id;
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        if (isset($_POST['academicYear'])) {

                $academicYear= $_POST['academicYear'];

                $WHERE = "";
                if (!empty($academicYear)){
                    $WHERE = "  AND loan_item.loan_item_id NOT IN (SELECT disbursement_default_plan.loan_item_id FROM disbursement_default_plan WHERE disbursement_default_plan.academic_year='$academicYear') ";
                }

                        $SQL="
                        SELECT 
                         loan_item.loan_item_id AS 'id',
                         CONCAT(loan_item.item_name,' (',loan_item.item_code,')') AS 'name'
                        FROM loan_item 
                       
                        WHERE loan_item.is_active = '1' $WHERE";


                $out = Yii::$app->db->createCommand($SQL)->queryAll();

                echo Json::encode($out);
                return;
           // }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    /**
     * Generating form fields for an institutional Disbursement Plan
     * */


    public function actionItem(){
        $output = array();
        $WHERE = "WHERE 1=1 ";
        if (isset($_POST['type'])&&$_POST['type']="default"){
            $table = 'disbursement_default_plan';
        }else{
            if (isset($_POST['institution'])){ $institution = $_POST['institution']; $WHERE.=" AND disbursement_plan.learning_institution='$institution'"; }
            $table = 'disbursement_plan';
        }
             if (isset($_POST['academicYear'])){ $academicYear = $_POST['academicYear']; $WHERE.=" AND $table.academic_year='$academicYear'"; }
            if (isset($_POST['programme'])){ $programme = $_POST['programme']; $WHERE.=" AND $table.programme_id='$programme'"; }
            if (isset($_POST['loanItem'])){ $loanItem = $_POST['loanItem']; $WHERE.=" AND $table.loan_item_id='$loanItem'"; }
            //learning_institution,academic_year,programme_id,loan_item_id,
            $SQL = "SELECT * FROM $table ".$WHERE;
            $output=Yii::$app->db->createCommand($SQL)->queryAll();


        echo  json_encode($output);
    }
    public function actionYears(){


        if (Yii::$app->request->isAjax) { //Making sure its an ajax request

           //Initializing Variables
            $years = 0;
            $output = array();

            //Loading All Active Semesters
            $semester = Module::Semester();

            //Loading All Active Installments
            $INSTALLMENTS = Yii::$app->db->createCommand("SELECT * FROM instalment_definition WHERE is_active = '1' ORDER BY instalment ASC")->queryAll();


            //Construction Programme Filtering Conditions
            $WHERE=" WHERE 1=1 ";
            if (isset($_POST['institution_id'])){ $institution = $_POST['institution_id']; $WHERE.=" AND learning_institution_id='$institution'";}
            if (isset($_POST['programme_id'])){ $programme_id = $_POST['programme_id']; $WHERE.=" AND programme_id='$programme_id'";}



            //Constructing SQL Query for selected institution and/or programme
            $SQL = "SELECT MAX(programme.years_of_study) AS 'years_of_study' FROM programme ".$WHERE;
            $model = Yii::$app->db->createCommand($SQL)->queryAll();



            // GENERATING TABULAR MATRIX WITH INPUTS FOR DISBURSEMENT PLAN
            /** ******************************************************* START ***********************************************************/
            $div = "";

            //Determining the maximum number of years for particular programme/programmes
            foreach ($model as $index=>$programme){
                $years=$programme['years_of_study'];
            }


            // LOOP 01:  Loop in respect to number of years for a particular programme
            for($i=1;$i<=$years; $i++){
                if($i>=4)
                {
                    $label="<span>".$i."<sup>th</sup> <small>yr</small></span>";
                } else{
                    $sup = "";
                    switch ($i){
                        case 1:
                            $sup = "st";
                            break;

                        case 2:
                            $sup = "nd";
                            break;

                        case 3:
                            $sup = "rd";
                            break;

                    }
                    $label="<span>".$i."<sup>".$sup."</sup> <small>yr</small></span>";
                }

                $div = "";

                $div='
                    <div class="col-md-12">
                        <table class="table table-bordered table-condensed">
                            <thead class="text-bold bg-blue-gradient">
                                <tr class="">
                                    <th width="120px" rowspan="2" class="btn bg-blue-gradient text-center text-bold" style="font-size: xx-large;">'.$label.'</th>';
                            //LOOP 02 (A) : Loop in respect to the number of instalments to generate columns with instalment names to the right after the year of study label on table heading
                              foreach ($INSTALLMENTS as $indx=>$instalmentArray){ $div .= '<th class="text-uppercase text-center"><div class="col-lg-12 text-center text-white text-nowrap itemName" style="font-size: 10px;"></div>' . Module::THNumber($instalmentArray["instalment"]) . ' QUARTER</th>'; }
                            // END of LOOP 02 (A)
                        $div.='
                                 </tr>
                             </thead>
                             <tbody>
                        ';

                //LOOP 03 : Loop in respect to semesters  generate data rows with semester name on first column and reference  on fields across semester label on table body
               // foreach ($semester as $sIndex=>$semesterName){
                $semesterName = '';
                    $div.='
                    <tr>
                        <th rowspan="2"  class="text-uppercase bg-blue-gradient">'.$semesterName.'</th>
                    ';

                    //LOOP 02 (B) : Loop in respect to number of instalments to  generate data rows with instalment reference below semester label on table body
                    foreach ($INSTALLMENTS as $inx=>$instlArray){
                        $sync = '';
                        $sIndex = $instlArray['semester_id'];
                        //CheckExistence
                        $div.='
                        <th class="bg-blue-gradient">
                        <div class="input-group">
                         <input onchange="ColorCode(this)" class="form-control text-bold text-center percent" style="font-size: large; color: red;" type="number" min="0" max="100" id="'.$i.'_'.$sIndex.'_'.$instlArray["instalment_definition_id"].'" name="percent['.$i.']['.$sIndex.']['.$instlArray["instalment_definition_id"].']" value="0.00" />
                        
                        <div class="input-group-addon bg-blue" style="font-size: 18px;">%</div>
                        
                        </div>
                       
                        </th>
                 
                        ';
                    }
                    // END of LOOP 02 (B)

                    $div.='</tr>';
               // }
                // END of LOOP 03

                $div.='</tbody>
                    </table>
                </div>';

                $output[]=$div;


            }
            // END OF LOOP 01: Programme years loop
            /** ******************************************************* END ***********************************************************/


    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'output' => $output,
                    ];
        } else throw new BadRequestHttpException;

    }

    public function actionIndex(){

        $searchModel = new DisbursementDefaultPlan();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function  actionCompletion(){
        $output = array('total'=>0,'assigned'=>0,'percent'=>0);
        if (isset($_POST['academicYear'])&&!empty($_POST['academicYear'])){
            $academicYear = $_POST['academicYear'];
            //$SEMESTER = Module::Semester();
            //$semester = sizeof($SEMESTER);
            $INSTALLMENTS = InstalmentDefinition::findAll(array('is_active'=>'1'));
            $instalment = sizeof($INSTALLMENTS);

            $SQL = "SELECT COUNT(disbursement_default_plan.plan_id) AS 'assigned'  FROM disbursement_default_plan WHERE disbursement_default_plan.academic_year='$academicYear';";
            $model = Yii::$app->db->createCommand($SQL)->queryAll();
            $ASSIGNED = $model[0]['assigned'];

            $SQLitems = "SELECT * FROM loan_item WHERE is_active='1';";
            $itemsResults = Yii::$app->db->createCommand($SQLitems)->queryAll();
            $ITEMS = sizeof($itemsResults);


            $YEARS = $ITEMS*$instalment;



            $TOTAL = ($YEARS);


            $PERCENT = 0;
            if ($TOTAL!=0){
                $PERCENT = ($ASSIGNED/$TOTAL)*100;
            }

            $output = array('total'=>$TOTAL,'assigned'=>$ASSIGNED,'percent'=>round($PERCENT,1));
        }

        echo json_encode($output);
    }


   /* public function actionCreate(){
        if (Yii::$app->request->isAjax) { //Making sure its an ajax request



            $percentField = $_POST['percentField'];
            $percentData = $_POST['percentData'];
            $institution = $_POST['institution'];
            $programme = $_POST['programme'];
            $loanItem = $_POST['loanItem'];
            $academicYear = $_POST['academicYear'];
            $setDefault = $_POST['setDefault'];

            $response = self::ProcessMultipleInputs($institution,$programme,$loanItem,$academicYear,$percentField,$percentData,$setDefault);
            $output='<p style="font-size: large;">'.$response.'</p>';
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => $output,
            ];
        }else{

            return $this->render('create');
        }
    }*/


    public function actionCreate(){
        if (Yii::$app->request->isAjax) { //Making sure its an ajax request
            $response='No record Affected';
            /**
            syncData:syncData,
            percentField:percentField,
            percentData:percentData,
            loanItem:loanItem,
            academicYear:academicYear,
             */
            $SQLc= $SQLu='';
            $percentField = $_POST['percentField'];
            $percentData = $_POST['percentData'];


            $loanItem = $_POST['loanItem'];
            $academicYear = $_POST['academicYear'];

            $now = date("Y-m-d H:i:s");
            $user = Yii::$app->user->id;

            $percent = 0;
            $counter = 0;
            $C_counter = 0;
            $U_counter = 0;
            foreach ($loanItem as $indexLI=>$Item){
                $item = $Item;

                foreach ($percentField as $index=>$FieldID){

                    $percent = $percentData[$index];
                    $fieldDataArray = explode('-',$FieldID);
                    $semester = $fieldDataArray[0];
                    $installment = $fieldDataArray[1];


                    $sync = $academicYear.'-'.$item.'-'.$semester.'-'.$installment;
                    $checkSQL = "SELECT * FROM disbursement_default_plan WHERE sync_id = '$sync';";
                    $checkModel = Yii::$app->db->createCommand($checkSQL)->queryAll();
                    if (sizeof($checkModel)!=0){
                        $SQLu.="UPDATE disbursement_default_plan SET 
                                    academic_year='$academicYear', instalment_id='$installment', loan_item_id='$item', semester_number='$semester', disbursement_percent='$percent'
                                    WHERE  sync_id='$sync';
                                ";
                        $U_counter++;
                    }else{
                        $SQLc.= "  INSERT IGNORE INTO disbursement_default_plan 
                        (academic_year, instalment_id, loan_item_id, semester_number, disbursement_percent, sync_id, date_created, created_by) VALUES 
                        ('$academicYear','$installment','$item','$semester','$percent','$sync','$now','$user');";
                    $C_counter++;
                    }


                    $counter++;
                }

            }
            if ($SQLc!=''){
                if (Yii::$app->db->createCommand($SQLc)->execute()){ /**Do nothing**/ }else{ $C_counter=0;}
                $response.=number_format($C_counter,0).'/'.number_format($counter,0).' Records has been Created';
            }
            if ($SQLu!=''){
                if (Yii::$app->db->createCommand($SQLu)->execute()){ /**Do nothing**/ }else{ $U_counter=0;}
                $response.=number_format($U_counter,0).'/'.number_format($counter,0).' Records has been Updated';
            }


            $output='<p style="font-size: large;">'.$response.'</p>';
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => $output,
            ];
        }else{
            /* $model = new DisbursementPlan();
             return $this->render('create',array('model'=>$model));*/
            return $this->render('create');
        }
    }

    public function actionClone(){
        if (Yii::$app->request->isAjax) { //Making sure its an ajax request

            /**
             *  type:type,
             * programme:programme,
             * loanItem:loanItem,
             * currentYear:currentYear,
             * nextYear:nextYear,
             */

            $type = $_POST['type'];
            $programme = $_POST['programme'];
            $loanItem = $_POST['loanItem'];
            $currentYear = $_POST['currentYear'];
            $nextYear = $_POST['nextYear'];


            $response = self::CloneMaster($type,$currentYear,$nextYear,$programme,$loanItem);
            $output='<p style="font-size: large;">'.$response.'</p>';
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => $output,
            ];
        }else {
            $model = new DisbursementPlan();
            return $this->render('clone', array('model' => $model));
        }
    }


    public static function CloneMaster($type,$currentYear,$nextYear,$programme,$loanItem)
    {
        $output = "";
        $counter =$updateCounter= 0;

        $now = date('Y-m-d H:i:s');
        $user = Yii::$app->user->id;






        switch($type){
            case "programme":
                $defaultARRAY = self::DefaultArray($currentYear,$programme,$loanItem);


                //Loading All Active Semesters
                $SEMESTER = Module::Semester();

                //Loading All Active Installments
                $INSTALLMENTS = InstalmentDefinition::findAll(array('is_active'=>'1'));
                $fields = "academic_year,year_of_study,programme_id,instalment_id,loan_item_id,semester_number,disbursement_percent,sync_id,date_created,created_by";
                $insertSQL=$updateSQL="";
                $SQL = "SELECT * FROM programme WHERE programme.programme_id<>'$programme';";

                $model=Yii::$app->db->createCommand($SQL)->queryAll();

                $head = $programme.'-'.$loanItem.'-'.$currentYear.'_';
                foreach ($model as $index=>$programmeArray)
                {

                    $totalYears=$programmeArray['years_of_study'];
                    for($YOS=1;$YOS<=$totalYears;$YOS++){

                        //foreach ($SEMESTER as $semesterID=>$semesterName){

                            foreach ($INSTALLMENTS as $instalment){
                                $semesterID = $instalment->semester_id;
                                $instalmentID=$instalment->instalment_definition_id;
                                $newProgramme = $programmeArray['programme_id'];
                                $sycID =$head."percent[$YOS][$semesterID][$instalmentID]";

                                $key = $YOS.'_'.$semesterID.'_'.$instalmentID;
                                if (isset($defaultARRAY[$key])){ $percent = $defaultARRAY[$key]; }else{$percent=0;}

                                $mySYNC =$newProgramme.'-'.$loanItem.'-'.$currentYear."_percent[$YOS][$semesterID][$instalmentID]";
                                $checkSQL = "SELECT disbursement_default_plan.plan_id AS 'id' FROM disbursement_default_plan WHERE disbursement_default_plan.sync_id='$mySYNC' ORDER BY plan_id DESC LIMIT 1";
                                $checkModel = Yii::$app->db->createCommand($checkSQL)->queryAll();
                                if (sizeof($checkModel)!=0){
                                    $updateCounter++;
                                    $updateSQL.="UPDATE disbursement_default_plan SET disbursement_percent='$percent' WHERE sync_id='$mySYNC';";

                                } else{
                                    $insertSQL.="INSERT INTO disbursement_default_plan($fields) VALUES ('$currentYear','$YOS','$newProgramme','$instalmentID','$loanItem','$semesterID','$percent','$mySYNC','$now','$user');";
                                    $counter++;
                                   }

                            }


                       // }

                    }

                }
                 if($counter>0){Yii::$app->db->createCommand($insertSQL)->execute(); $output.="A total of <b>".number_format($counter,0)."</b> Records has been successfully registered";}
                 if($updateCounter>0){Yii::$app->db->createCommand($updateSQL)->execute(); $output.=" A total of <b>".number_format($updateCounter,0)."</b> Records has been updated successfully";}


                break;

            case "default":
                //$type,$currentYear,$nextYear,$programme,$loanItem
                $SQL = "DELETE FROM disbursement_default_plan WHERE academic_year='$nextYear'; ";
                $SQL.= "ALTER TABLE disbursement_default_plan AUTO_INCREMENT = 1; ";
                $SQL.= "INSERT INTO disbursement_default_plan (academic_year,year_of_study,programme_id,instalment_id,loan_item_id,semester_number,disbursement_percent,sync_id,date_created,created_by)";
                $SQL.= "SELECT $nextYear AS 'academic_year',year_of_study,programme_id,instalment_id,loan_item_id,semester_number,disbursement_percent,  CONCAT(programme_id,'-',loan_item_id,'-',$nextYear,'_','percent','[',year_of_study,']','[',semester_number,']','[',instalment_id,']') AS 'sync_id','$now' AS 'date_created','$user' AS 'created_by' FROM disbursement_default_plan WHERE academic_year='$currentYear';";
                Yii::$app->db->createCommand($SQL)->execute();
                //$output = '<pre>'.$SQL.'</pre>';//"Plan has been Successfully cloned";
                $output = "Plan has been Successfully cloned";
                break;

            case "institutional":
                //$type,$currentYear,$nextYear,$programme,$loanItem
                $SQL = "DELETE FROM disbursement_plan WHERE academic_year='$nextYear'; ";
                $SQL.= "ALTER TABLE disbursement_plan AUTO_INCREMENT = 1; ";
                $SQL.= "INSERT INTO disbursement_plan (learning_institution,academic_year,year_of_study,programme_id,instalment_id,loan_item_id,semester_number,disbursement_percent,sync_id,date_created,created_by)";
                $SQL.= "SELECT learning_institution, $nextYear AS 'academic_year',year_of_study,programme_id,instalment_id,loan_item_id,semester_number,disbursement_percent,  CONCAT(learning_institution,'-',programme_id,'-',loan_item_id,'-',$nextYear,'_','percent','[',year_of_study,']','[',semester_number,']','[',instalment_id,']') AS 'sync_id','$now' AS 'date_created','$user' AS 'created_by' FROM disbursement_plan WHERE academic_year='$currentYear';";
                Yii::$app->db->createCommand($SQL)->execute();
                //$output = '<pre>'.$SQL.'</pre>';//"Plan has been Successfully cloned";
                $output = "Plan has been Successfully cloned";
                break;

        }

        return $output;
    }





    public function actionStatistics(){
        $model = new DisbursementPlan();
        return $this->render('statistics',array('model'=>$model));
    }


    /** Processing Multiple Inputs
     * @return string
     * @var integer $institution
     * @var integer $programme
     * @var integer $loanItem
     * @var integer $academicYear
     * @var array $percentField
     * @var array $percentData
     *
     **/
    public static function ProcessMultipleInputs($institution,$programme,$loanItem,$academicYear,$percentField,$percentData,$setDefault)
    {
        $output = 'Operation completed Successfully';

            $insertSQL='';
            $updateSQL='';

            $defaultInsertSQL = '';
            $defaultUpdateSQL = '';


            $now = date('Y-m-d H:i:s');
            $user = Yii::$app->user->id;
            $insertFields='learning_institution,academic_year,programme_id,loan_item_id,date_created,created_by,is_default,year_of_study,semester_number,instalment_id,disbursement_percent,sync_id';
            $defaulInsertFields='academic_year,programme_id,loan_item_id,date_created,created_by,year_of_study,semester_number,instalment_id,disbursement_percent,sync_id';

            $insertInitialValues="'$institution','$academicYear','$programme','$loanItem','$now','$user',$setDefault";
            $insertInitialDFPValues="'$academicYear','$programme','$loanItem','$now','$user'";

            $sync =  '';
            $syncDFP =  '';
            $head ='';
            $headDFP ='';

            $head = $institution.'-'.$programme.'-'.$loanItem.'-'.$academicYear.'_';
            $headDFP = $programme.'-'.$loanItem.'-'.$academicYear.'_';

            $insertCounter = 0;
            $updateCounter = 0;

            $defaultInsertCounter = 0;
            $defaultUpdateCounter = 0;

            foreach($percentField as $index=>$fieldName){
                 $sync = '';
                 $sync=$head.$fieldName;
                 $syncDFP=$headDFP.$fieldName;
                 $codedString = str_replace(']','',str_replace('[','',str_replace('][','-',str_replace('percent','',$fieldName))));
                $codedArray=explode('-',$codedString);

                $yearOfStudy = $codedArray[0];
                $Semester = $codedArray[1];
                $Instalment = $codedArray[2];



               isset($percentData[$index])?$percent = $percentData[$index]:$percent=0;
                //$output.=$percent;
               $checkSQL = "SELECT disbursement_plan.disbursement_plan_id AS 'id' FROM disbursement_plan WHERE disbursement_plan.sync_id='$sync' ORDER BY disbursement_plan_id DESC LIMIT 1";
               $checkModel = Yii::$app->db->createCommand($checkSQL)->queryAll();
               if (sizeof($checkModel)!=0){
                   $updateCounter++;
                   $updateSQL.="UPDATE disbursement_plan SET disbursement_percent='$percent' WHERE sync_id='$sync';";

               } else{
                   $insertCounter++;
                   $insertSQL.="INSERT INTO disbursement_plan(".$insertFields.")VALUES(".$insertInitialValues.",'".$yearOfStudy."','".$Semester."','".$Instalment."','".$percent."','".$sync."');";
               }

                if ($setDefault==1){

                  $checkDFPSQL = "SELECT disbursement_default_plan.plan_id AS 'id' FROM disbursement_default_plan WHERE disbursement_default_plan.sync_id='$syncDFP' ORDER BY plan_id DESC LIMIT 1";
                    $checkDFPModel = Yii::$app->db->createCommand($checkDFPSQL)->queryAll();
                    if (sizeof($checkDFPModel)!=0){
                        $defaultUpdateCounter++;
                        $defaultUpdateSQL.="UPDATE disbursement_default_plan SET disbursement_percent='$percent' WHERE sync_id='$syncDFP';";

                    } else{
                        $defaultInsertCounter++;
                        $defaultInsertSQL.="INSERT INTO disbursement_default_plan(".$defaulInsertFields.")VALUES(".$insertInitialDFPValues.",'".$yearOfStudy."','".$Semester."','".$Instalment."','".$percent."','".$syncDFP."');";
                    }

                }


            }

            if ($insertCounter!=0){  Yii::$app->db->createCommand($insertSQL)->execute(); $output.=' <b>'.number_format($insertCounter,0).'</b> NEW percents, '; }
            if ($updateCounter!=0){  Yii::$app->db->createCommand($updateSQL)->execute(); $output.='  <b>'.number_format($updateCounter,0).'</b> AMENDED percents, ';}

        if ($setDefault==1){
            if ($defaultInsertCounter!=0){  Yii::$app->db->createCommand($defaultInsertSQL)->execute(); $output.='  AND <b>'.number_format($defaultInsertCounter,0).'</b> NEW records have been registered to <b> DEFAULT PLAN</b>. ';}
            if ($defaultUpdateCounter!=0){  Yii::$app->db->createCommand($defaultUpdateSQL)->execute(); $output.='  AND <b>'.number_format($defaultUpdateCounter,0).'</b> UPDATES have been applied to  <b>DEFAULT PLAN</b>. ';}

        }
        return $output;
    }

    public static function ProcessMultipleDefaultInputs($programme,$loanItem,$academicYear,$percentField,$percentData)
    {
        $output = 'Operation completed Successfully';

        $insertSQL='';
        $updateSQL='';


        $now = date('Y-m-d H:i:s');
        $user = Yii::$app->user->id;
        $insertFields='academic_year,programme_id,loan_item_id,date_created,created_by,year_of_study,semester_number,instalment_id,disbursement_percent,sync_id';
        $insertInitialValues="'$academicYear','$programme','$loanItem','$now','$user'";

        $sync =  '';
        $head ='';
        $head = $programme.'-'.$loanItem.'-'.$academicYear.'_';

        $insertCounter = 0;
        $updateCounter = 0;



        foreach($percentField as $index=>$fieldName){
            $sync = '';
            $sync=$head.$fieldName;

            $codedString = str_replace(']','',str_replace('[','',str_replace('][','-',str_replace('percent','',$fieldName))));
            $codedArray=explode('-',$codedString);

            $yearOfStudy = $codedArray[0];
            $Semester = $codedArray[1];
            $Instalment = $codedArray[2];



            isset($percentData[$index])?$percent = $percentData[$index]:$percent=0;
            //$output.=$percent;
            $checkSQL = "SELECT disbursement_default_plan.plan_id AS 'id' FROM disbursement_default_plan WHERE disbursement_default_plan.sync_id='$sync' ORDER BY plan_id DESC LIMIT 1";
            $checkModel = Yii::$app->db->createCommand($checkSQL)->queryAll();
            if (sizeof($checkModel)!=0){
                $updateCounter++;
                $updateSQL.="UPDATE disbursement_default_plan SET disbursement_percent='$percent' WHERE sync_id='$sync';";

            } else{
                $insertCounter++;
                $insertSQL.="INSERT INTO disbursement_default_plan(".$insertFields.")VALUES(".$insertInitialValues.",'".$yearOfStudy."','".$Semester."','".$Instalment."','".$percent."','".$sync."');";
            }


        }

        if ($insertCounter!=0){  Yii::$app->db->createCommand($insertSQL)->execute(); $output.=' <b>'.number_format($insertCounter,0).'</b> NEW percents, '; }
        if ($updateCounter!=0){  Yii::$app->db->createCommand($updateSQL)->execute(); $output.='  <b>'.number_format($updateCounter,0).'</b> AMENDED percents, ';}


        return $output;
    }
    public static function CheckPlanExistence($sync)
    {
        $output = 0;
        $checkSQL = "SELECT disbursement_plan.disbursement_percent AS 'percent' FROM disbursement_plan WHERE disbursement_plan.sync_id='$sync' ORDER BY disbursement_plan_id DESC LIMIT 1";
        $checkModel = Yii::$app->db->createCommand($checkSQL)->queryAll();
        foreach ($checkModel as $index=>$dataArray){
            $output=$dataArray['percent'];
        }

        return $output;

    }

    public static function CheckDefaultExistence($sync=null,$academicYear=null,$programme=null,$semester=null,$installment=null,$loanItem=null,$studyYear=null)
    {
        $WHERE="";
        //academic_year,programme_id,loan_item_id,year_of_study,semester_number,instalment_id,
        if ($sync!=null){ $WHERE.=" disbursement_default_plan.sync_id='$sync'"; }else{$WHERE.=" 1=1 ";}
        if ($academicYear!=null){ $WHERE.=" AND disbursement_default_plan.academic_year='$academicYear'"; }
        if ($programme!=null){ $WHERE.=" AND disbursement_default_plan.programme_id='$programme'"; }
        if ($semester!=null){ $WHERE.=" AND disbursement_default_plan.semester_number='$semester'"; }
        if ($installment!=null){ $WHERE.=" AND disbursement_default_plan.instalment_id='$installment'"; }
        if ($loanItem!=null){ $WHERE.=" AND disbursement_default_plan.loan_item_id='$loanItem'"; }
        if ($studyYear!=null){ $WHERE.=" AND disbursement_default_plan.year_of_study='$studyYear'"; }

        $output = 0;
        $checkSQL = "SELECT disbursement_default_plan.disbursement_percent AS 'percent' FROM disbursement_default_plan WHERE  $WHERE ORDER BY disbursement_default_plan.plan_id DESC LIMIT 1";
        $checkModel = Yii::$app->db->createCommand($checkSQL)->queryAll();
        foreach ($checkModel as $index=>$dataArray){
            $output=$dataArray['percent'];
        }

        return $output;

    }



    public static function DefaultArray($academicYear,$programme,$loanItem)
    {
        $WHERE=" 1=1 ";

        $output = array();
        $checkSQL = "SELECT disbursement_default_plan.disbursement_percent AS 'percent',disbursement_default_plan.year_of_study AS 'study_year',disbursement_default_plan.semester_number AS 'semester',disbursement_default_plan.instalment_id AS 'instalment'   FROM disbursement_default_plan WHERE disbursement_default_plan.loan_item_id='$loanItem' AND disbursement_default_plan.academic_year='$academicYear' AND disbursement_default_plan.programme_id='$programme' ORDER BY year_of_study ASC,semester_number ASC,instalment_id ASC";
        //$checkSQL = "SELECT disbursement_default_plan.disbursement_percent AS 'percent',disbursement_default_plan.year_of_study AS 'study_year',disbursement_default_plan.semester_number AS 'semester',disbursement_default_plan.instalment_id AS 'instalment' FROM disbursement_default_plan WHERE  $WHERE ORDER BY year_of_study ASC,semester_number ASC,instalment_id ASC  ";
        $checkModel = Yii::$app->db->createCommand($checkSQL)->queryAll();
        foreach ($checkModel as $index=>$dataArray){
            $key = $dataArray['study_year'].'_'.$dataArray['semester'].'_'.$dataArray['instalment'];
            //$key = $YOS.'_'.$semesterID.'_'.$instalmentID;
            $output[$key]=$dataArray['percent'];
        }

        return $output;

    }

    /**
     * Deletes an existing InstalmentDefinition model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InstalmentDefinition model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InstalmentDefinition the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DisbursementDefaultPlan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}