<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/23/18
 * Time: 9:54 PM
 */

namespace backend\modules\disbursement\controllers;

use Yii;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use backend\modules\disbursement\Module;

class ContinuingAccessController extends Controller
{
    public $layout = "main_private";
    public function actionIndex(){
        return $this->render('index');
    }

    public function actionProcess()
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {

            $results = Module::ContPasswordReset();

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results' => $results),
            ];
        } else throw new BadRequestHttpException;
    }

}