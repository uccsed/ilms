<?php

namespace backend\modules\disbursement\models;

use Yii;
use backend\modules\allocation\models\LoanItem;
use backend\modules\disbursement\models\Disbursement;
use backend\modules\disbursement\models\AdjustmentReason;
use backend\modules\allocation\models\AllocationBatch;
use backend\modules\allocation\models\Application;
use backend\modules\disbursement\Module;

/**
 * This is the model class for table "disbursement_adjustment".
 *
 * @property integer $id
 * @property string $adjustment_number
 * @property integer $application_id
 * @property integer $loan_item_id
 * @property integer $disbursement_id
 * @property string $type
 * @property double $original_amount
 * @property double $debit_amount
 * @property double $credit_amount
 * @property integer $reasons
 * @property string $remarks
 * @property string $adjustment_date
 * @property integer $adjusted_by
 */
class DisbursementAdjustment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disbursement_adjustment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adjustment_number', 'application_id', 'loan_item_id', 'disbursement_id', 'type', 'original_amount', 'reasons', 'remarks'], 'required'],
            [['application_id', 'loan_item_id', 'disbursement_id', 'reasons', 'adjusted_by'], 'integer'],
            [['original_amount', 'debit_amount', 'credit_amount'], 'number'],
            [['remarks'], 'string'],
            [['adjustment_date'], 'safe'],
            [['adjustment_number'], 'string', 'max' => 100],
            [['type'], 'string', 'max' => 2],
            [['adjustment_number'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'adjustment_number' => 'Adjustment Number',
            'application_id' => 'Application',
            'Applicant' => 'Applicant',
            'loan_item_id' => 'Loan Item',
            'disbursement_id' => 'Disbursement',
            'type' => 'Type',
            'original_amount' => 'Original Amount',
            'debit_amount' => 'Debit Amount',
            'credit_amount' => 'Credit Amount',
            'reasons' => 'Reasons',
            'remarks' => 'Remarks',
            'adjustment_date' => 'Adjustment Date',
            'adjusted_by' => 'Adjusted By',
        ];
    }

    public function getLoanItem(){
        $output = '';
        $model = LoanItem::findOne($this->loan_item_id);
        if (sizeof($model)!=0){
            $output = $model->item_name.' ('.$model->item_code.')';
        }

        return $output;
    }

    public function getReason(){
        $output = '';
        $model = AdjustmentReason::findOne($this->reasons);
        if (sizeof($model)!=0){
            $output = $model->name;
        }

        return $output;
    }

    public function getType(){
        $output = '';
        $model = Module::AdjustmentType($this->type);
        if (!empty($model)&&!is_array($model)){
            $output = $model;
        }

        return $output;
    }

    public function getApplicant(){
        $output = '';

        $output = Module::Applicant($this->application_id);

        return $output;
    }


    /**
 * @return \yii\db\ActiveQuery
 */
    public function getApplication()
    {
        return $this->hasOne(\backend\modules\application\models\Application::className(), ['application_id' => 'application_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisbursement()
    {
        return $this->hasOne(Disbursement::className(), ['disbursement_id' => 'disbursement_id']);
    }





}
