<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\disbursement\models\DisbursementReturnBatch;
use backend\modules\disbursement\models\DisbursementReturnStaging;

/**
 * DisbursementReturnStagingSearch represents the model behind the search form about `backend\modules\disbursement\models\DisbursementStagingBatch`.
 */
class DisbursementReturnStagingSearch extends DisbursementReturnStaging
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
       /* return [
            [['id', 'institution_id', 'academic_year', 'financial_year', 'created_by'], 'integer'],
            [['batch_number', 'description', 'status', 'created_on', 'academic_year', 'financial_year'], 'safe'],
        ];*/
        return [
            [['year_of_study', 'pay_sheet_serial_number', 'instalment_number', 'academic_year', 'created_by', 'header_id'], 'integer'],
            [['id', 'index_number', 'student_name', 'sex', 'year_of_study', 'pay_sheet_serial_number', 'loan_item', 'instalment_number', 'amount', 'remarks', 'academic_year', 'status', 'match_percent', 'reasons', 'created_by', 'created_on', 'header_id', 'sync_id'], 'safe'],
            /*[['amount', 'match_percent'], 'number'],
            [['remarks', 'reasons'], 'string'],
            [['created_by', 'sync_id'], 'required'],
            [['created_on'], 'safe'],
            [['index_number', 'loan_item', 'sync_id'], 'string', 'max' => 100],
            [['student_name'], 'string', 'max' => 200],
            [['sex'], 'string', 'max' => 6],
            [['status'], 'string', 'max' => 20],
            [['sync_id'], 'unique'],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */



    public function search($params,$batchID,$status)
    {
        $query = DisbursementReturnStaging::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['header_id'=>$batchID,'status'=>$status]);
        $query->andFilterWhere([
            'id' => $this->id,
            'index_number' => $this->index_number,
            'year_of_study' => $this->year_of_study,
            'academic_year' => $this->academic_year,
            'header_id' => $this->header_id,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'index_number', $this->index_number])
            ->andFilterWhere(['like', 'student_name', $this->student_name])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
    public function searchBatch($params,$batchID)
    {
        $query = DisbursementReturnStaging::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['header_id'=>$batchID]);
        $query->andFilterWhere([
            'id' => $this->id,
            'index_number' => $this->index_number,
            'year_of_study' => $this->year_of_study,
            'academic_year' => $this->academic_year,
            'header_id' => $this->header_id,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'index_number', $this->index_number])
            ->andFilterWhere(['like', 'student_name', $this->student_name])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
