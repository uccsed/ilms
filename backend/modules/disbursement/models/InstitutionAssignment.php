<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\allocation\models\LearningInstitution;
use frontend\modules\repayment\models\User;
/**
 * This is the model class for table "institution_assignment".
 *
 * @property integer $id
 * @property integer $officer_id
 * @property integer $institution_id
 * @property string $sync_id
 * @property integer $assigned_by
 * @property string $assigned_on
 */
class InstitutionAssignment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institution_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['officer_id', 'institution_id'], 'required'],
            [['officer_id', 'institution_id', 'assigned_by'], 'integer'],
            [['assigned_on'], 'safe'],
            [['sync_id'], 'string', 'max' => 100],
            [['sync_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'officer_id' => 'Officer ID',
            'institution_id' => 'Institution ID',
            'sync_id' => 'Sync ID',
            'assigned_by' => 'Assigned By',
            'assigned_on' => 'Assigned On',
        ];
    }

    public function search($params)
    {
        $query = InstitutionAssignment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

       /* if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }*/

        // grid filtering conditions
        $query->andFilterWhere([
            'officer_id' => $this->officer_id,
            'institution_id' => $this->institution_id,
           // 'comment' => $this->comment,
            'assigned_by' => $this->assigned_by,
            'assigned_on' => $this->assigned_on,
        ]);

        return $dataProvider;
    }

    public function getInstitution(){
        $output = '';
        $model = LearningInstitution::findOne($this->institution_id);
        if (sizeof($model)!=0){
            $output = $model->institution_name.' ('.$model->institution_code.')';
        }
        return $output;
    }

    public function getOfficer(){
        $output = '';
        $model = User::findOne($this->officer_id);
        if (sizeof($model)!=0){
            $output = $model->firstname.' '.$model->surname;
        }
        return $output;
    }
}
