<?php

namespace backend\modules\disbursement\models;

use Yii;

/**
 * This is the model class for table "adjustment_reason".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $is_active
 */
class AdjustmentReason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adjustment_reason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'is_active'], 'required'],
            [['description'], 'string'],
            [['is_active'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'is_active' => 'Is Active',
        ];
    }
}
