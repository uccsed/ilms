<?php

namespace backend\modules\disbursement\models;

use Yii;

/**
 * This is the model class for table "suspension_staging".
 *
 * @property integer $id
 * @property string $sn
 * @property string $index_number
 * @property string $names
 * @property string $programme
 * @property string $year_of_study
 * @property string $sync_id
 * @property integer $status
 * @property string $status_date
 * @property integer $header_id
 * @property integer $suspension_reason
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $matching_remarks
 */
class SuspensionStaging extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suspension_staging';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['index_number', 'status_date', 'header_id', 'suspension_reason'], 'required'],
            [['status', 'header_id', 'created_by', 'updated_by'], 'integer'],
            [['status_date', 'created_at', 'updated_at', 'sync_id', 'suspension_reason', 'matching_remarks'], 'safe'],
            [['sync_id'], 'unique'],
            [['sn', 'index_number'], 'string', 'max' => 200],
            [['names'], 'string', 'max' => 45],
            [['programme'], 'string', 'max' => 100],
            [['year_of_study'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sn' => 'Sn',
            'index_number' => 'index_number',
            'names' => 'Names',
            'programme' => 'Programme',
            'year_of_study' => 'Year Of Study',
            'status' => 'Status',
            'status_date' => 'Status Date',
            'header_id' => 'Header ID',
            'suspension_reason' => 'Reasons',
            'matching_remarks' => 'Matching Remarks',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
}
