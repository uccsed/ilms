<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\disbursement\models\DisbursementAdjustment;

/**
 * DisbursementAdjustmentSearch represents the model behind the search form about `backend\modules\disbursement\models\DisbursementAdjustment`.
 */
class DisbursementAdjustmentSearch extends DisbursementAdjustment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'application_id', 'loan_item_id', 'disbursement_id', 'reasons', 'adjusted_by'], 'integer'],
            [['adjustment_number', 'type', 'remarks', 'adjustment_date'], 'safe'],
            [['original_amount', 'debit_amount', 'credit_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DisbursementAdjustment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'application_id' => $this->application_id,
            'loan_item_id' => $this->loan_item_id,
            'disbursement_id' => $this->disbursement_id,
            'original_amount' => $this->original_amount,
            'debit_amount' => $this->debit_amount,
            'credit_amount' => $this->credit_amount,
            'reasons' => $this->reasons,
            'adjustment_date' => $this->adjustment_date,
            'adjusted_by' => $this->adjusted_by,
        ]);

        $query->andFilterWhere(['like', 'adjustment_number', $this->adjustment_number])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'remarks', $this->remarks]);

        return $dataProvider;
    }
}
