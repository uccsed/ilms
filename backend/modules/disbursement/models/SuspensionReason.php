<?php

namespace backend\modules\disbursement\models;

use Yii;
use backend\modules\disbursement\Module;
/**
 * This is the model class for table "suspension_reason".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $type
 * @property integer $is_active
 */
class SuspensionReason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suspension_reason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['type', 'is_active'], 'required'],
            [['type', 'is_active'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'type' => 'Type',
            'is_active' => 'Is Active',
        ];
    }

    public function getType(){

        return Module::SuspensionType($this->type);
    }
}
