<?php

namespace backend\modules\disbursement\models;

use backend\modules\administration\models\LearningInstitution;
use common\models\AcademicYear;
use Yii;

/**
 * This is the model class for table "disbursement_deposit_batch".
 *
 * @property integer $id
 * @property integer $institution_id
 * @property integer $academic_year
 * @property string $description
 * @property string $deposit_date
 * @property string $deposit_cheque_number
 * @property double $cheque_amount
 * @property string $status
 * @property string $status_date
 * @property string $created_on
 * @property integer $created_by
 */
class DisbursementDepositBatch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disbursement_deposit_batch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['institution_id', 'academic_year', 'deposit_date','cheque_amount', 'deposit_cheque_number'/*, 'created_on', 'created_by'*/], 'required'],
            [['institution_id', 'academic_year', 'created_by'], 'integer'],
            [['description'], 'string'],
            [['deposit_date', 'created_on', 'status', 'status_date'], 'safe'],
            [['cheque_amount'], 'number'],
            [['deposit_cheque_number'], 'string', 'max' => 100],
            //[['cheque_amount'], 'min' => 0],
            [['status'], 'string', 'max' => 20],
            [['deposit_cheque_number'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'institution_id' => 'Institution ID',
            'academic_year' => 'Academic Year',
            'description' => 'Description',
            'deposit_date' => 'Deposit Date',
            'deposit_cheque_number' => 'Deposit Cheque Number',
            'cheque_amount' => 'Cheque Amount',
            'status' => 'Status',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
        ];
    }


    public function getAcademicYear(){
        return $this->hasOne(AcademicYear::className(),['academic_year_id'=>'academic_year']);
    }



    public function getLearningInstitution(){
        return $this->hasOne(LearningInstitution::className(),['learning_institution_id'=>'institution_id']);
    }
}
