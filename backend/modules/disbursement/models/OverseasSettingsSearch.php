<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\disbursement\models\OverseasSettings;

/**
 * OverseasSettingsSearch represents the model behind the search form about `backend\modules\disbursement\models\OverseasSettings`.
 */
class OverseasSettingsSearch extends OverseasSettings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'academic_year', 'country_id', 'year_of_study', 'loan_item', 'instalment'], 'integer'],
            [['disbursement_percent'], 'number'],
            [['sync_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OverseasSettings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'academic_year' => $this->academic_year,
            'country_id' => $this->country_id,
            'year_of_study' => $this->year_of_study,
            'loan_item' => $this->loan_item,
            'instalment' => $this->instalment,
            'disbursement_percent' => $this->disbursement_percent,
        ]);

        $query->andFilterWhere(['like', 'sync_id', $this->sync_id]);

        return $dataProvider;
    }

    public function GroupedSearch($params)
    {
        $query = OverseasSettings::find()->groupBy(['country_id']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'academic_year' => $this->academic_year,
            'country_id' => $this->country_id,
            'year_of_study' => $this->year_of_study,
            'loan_item' => $this->loan_item,
            'instalment' => $this->instalment,
            'disbursement_percent' => $this->disbursement_percent,
        ]);

        $query->andFilterWhere(['like', 'sync_id', $this->sync_id]);

        return $dataProvider;
    }

    public function SpecialSearch($params,$country=null)
    {
        $query = OverseasSettings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if ($country!==null){
            $query->andWhere(['country_id'=>$country]);
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'academic_year' => $this->academic_year,
            'country_id' => $this->country_id,
            'year_of_study' => $this->year_of_study,
            'loan_item' => $this->loan_item,
            'instalment' => $this->instalment,
            'disbursement_percent' => $this->disbursement_percent,
        ]);

        $query->andFilterWhere(['like', 'sync_id', $this->sync_id]);

        return $dataProvider;
    }
}
