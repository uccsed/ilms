<?php

namespace backend\modules\disbursement\models;

use function PHPSTORM_META\elementType;
use Yii;
use yii\web\UploadedFile;
use backend\modules\application\models\Applicant;
use backend\modules\allocation\models\Application;
use backend\modules\allocation\models\LoanItem;
use common\models\User;
use backend\modules\disbursement\models\SuspensionReason;
use backend\modules\allocation\models\Programme;
use backend\modules\disbursement\Module;


/**
 * This is the model class for table "disbursement_suspension".
 *
 * @property integer $id
 * @property integer $application_id
 * @property integer $loan_item_id
 * @property double $suspension_percent
 * @property double $remaining_percent
 * @property integer $status
 * @property integer $status_reason
 * @property string $remarks
 * @property string $status_date
 * @property integer $status_by
 * @property string $supporting_document
 * @property string $suspension_history
 * @property string $sync_id
 * @property integer $mode
 */
class DisbursementSuspension extends \yii\db\ActiveRecord
{
    public  $file;
    public $firstName;
    public $middleName;
    public $surName;
    public $f4indexno;
    public $Institution;
    public $Programme;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disbursement_suspension';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        if (Yii::$app->controller->id=='disbursement-uplifting'){
            $maxRule = [['remaining_percent'], 'number', 'min' => 1, 'max' => 100];
        }else{
            //$maxRule = [['suspension_percent', 'remaining_percent'], 'number', 'min' => 0, 'max' => 100];
            $maxRule = [['suspension_percent'], 'number', 'min' => 1, 'max' => 100];
        }
        return [
            $maxRule,
            [['file'], 'file'],
            [['application_id', 'loan_item_id', 'suspension_percent', 'remaining_percent', 'status', 'status_reason', /*'file',*/ 'mode'], 'required'],
            [['application_id', 'loan_item_id', 'status', 'status_reason', 'status_by','mode'], 'integer'],
            [['suspension_percent', 'remaining_percent'], 'number'],
            [['remarks', 'supporting_document', 'suspension_history'], 'string'],
            [['status_date','sync_id','f4indexno','firstName','middleName','surName','Institution','Programme'], 'safe'],
            [['sync_id'], 'string', 'max' => 100],
            [['sync_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        if (Yii::$app->controller->id=='disbursement-uplifting'){
            $label1 = 'Uplifting Percent';
            $label = "Uplifting";

        }else{
            $label1 = 'Remaining Percent';
            $label = "Suspension";
        }
        return [
            'id' => 'ID',
            'application_id' => 'Application ID',
            'loan_item_id' => 'Loan Item ID',
            'suspension_percent' => "Suspension Percent",
            'remaining_percent' => $label1,
            'status' => 'Status',
            'status_reason' => 'Status Reason',
            'remarks' => 'Remarks',
            'status_date' => 'Status Date',
            'status_by' => 'Status By',
            'supporting_document' => 'Supporting Document',
            'file' => 'Supporting Document',
            'suspension_history' => "$label History",
            'sync_id' => 'Sync ID',
            'mode' => "$label Mode",
            'f4indexno'=>'Index #',
            'firstName'=>'First Name',
            'middleName'=>'Middle Name',
            'surName'=>'Surname',
            'Institution'=>'Institution',
            'Programme'=>'Programme'
        ];
    }

    public function getApplicantInfo(){
        $output = '';
        $model= Application::findOne($this->application_id);
        if (sizeof($model)!=0){
            $applicantModel = Applicant::findOne($model->applicant_id);
            if (sizeof($applicantModel)!=0){
                $userModel = User::findOne($applicantModel->user_id);
                if (sizeof($userModel)!=0){
                    $output = $userModel->username.' | '.$userModel->firstname.' '.$userModel->surname.' ['.$userModel->sex.']';
                }
            }
        }

        return $output;
    }

    public function getApplicantNames(){
        $output = '';
        $model= Application::findOne($this->application_id);
        if (sizeof($model)!=0){
            $applicantModel = Applicant::findOne($model->applicant_id);
            if (sizeof($applicantModel)!=0){
                $userModel = User::findOne($applicantModel->user_id);
                if (sizeof($userModel)!=0){
                    $output = $userModel->firstname.' '.$userModel->surname.' ['.$userModel->sex.']';
                }
            }
        }

        return $output;
    }




    public function getLoanItem(){
        $output = '';
        $model = LoanItem::findOne($this->loan_item_id);
        if (sizeof($model)!=0){
            $output = $model->item_name.' ('.$model->item_code.')';
        }
        return $output;
    }

    public function getStatus(){
        return Module::SuspensionType($this->status);
    }

    public function getReason(){
        $output = '';
        $model = SuspensionReason::findOne($this->status_reason);
        if (sizeof($model)!=0){
            $output = $model->name;
        }
        return $output;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplication() {
        return $this->hasOne(Application::className(), ['application_id' => 'application_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplicant() {
        return $this->hasMany(Application::className(), ['applicant_id' => 'applicant_id']);
    }


}
