<?php

namespace backend\modules\disbursement\models;

use backend\modules\allocation\models\AcademicYear;
use backend\modules\allocation\models\LearningInstitution;
use Yii;
use backend\modules\disbursement\Module;
/**
 * This is the model class for table "disbursement_return_batch".
 *
 * @property integer $id
 * @property string $batch_number
 * @property integer $institution_id
 * @property integer $academic_year
 * @property integer $financial_year

 * @property string $description
 * @property string $return_cheque_number
 * @property double $cheque_amount
 * @property string $status
 * @property string $created_on
 * @property integer $created_by
 * academic_year,financial_year
 * return_cheque_number,cheque_amount
 */
class DisbursementReturnBatch extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disbursement_return_batch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'file'],
            [['institution_id', /*'created_on', 'created_by',*/ 'academic_year', 'financial_year'], 'required'],
            [['institution_id', 'academic_year', 'financial_year', 'created_by', 'cheque_amount'], 'integer'],
            [['description', 'return_cheque_number'], 'string'],
            [['created_on', 'academic_year', 'financial_year','return_cheque_number', 'cheque_amount'], 'safe'],
            //[['batch_number'], 'string', 'max' => 200],
            [['batch_number'], 'unique'],
            [['status'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Return Header ID',
            'batch_number' => 'Return Batch Number',
            'institution_id' => 'Institution',
            'academic_year' => 'Academic Year',
            'financial_year' => 'Financial Year',
            'description' => 'Description',
            'return_cheque_number' => 'Return Cheque Number',
            'cheque_amount' => 'Return Cheque Amount',
            'status' => 'Status',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
        ];
    }

    public function getAcademicYear(){
        return $this->hasOne(AcademicYear::className(),['academic_year_id'=>'academic_year']);
    }

    public function getFinancialYear(){
        return $this->hasOne(FinancialYear::className(),['financial_year_id'=>'financial_year']);
    }

    public function getLearningInstitution(){
        return $this->hasOne(LearningInstitution::className(),['learning_institution_id'=>'institution_id']);
    }

    public function getPendingAmount()
    {
        $output = Module::ReturnAmount($this->id,'PENDING');

        return $output;
    }

    public function getConfirmedAmount()
    {
        $output = Module::ReturnAmount($this->id,'CONFIRMED');

        return $output;
    }

    public function getUpdatedAmount()
    {
        $output = Module::ReturnAmount($this->id,'UPDATED');

        return $output;
    }
}
