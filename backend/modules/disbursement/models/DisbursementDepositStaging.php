<?php

namespace backend\modules\disbursement\models;

use Yii;

/**
 * This is the model class for table "disbursement_deposit_staging".
 *
 * @property integer $id
 * @property string $index_number
 * @property string $student_name
 * @property string $registration_number
 * @property string $sex
 * @property integer $year_of_study
 * @property integer $pay_sheet_serial_number
 * @property string $loan_item
 * @property integer $instalment_number
 * @property double $amount
 * @property integer $academic_year
 * @property string $status
 * @property string $matching_remarks
 * @property integer $created_by
 * @property string $created_on
 * @property integer $header_id
 * @property string $sync_id
 * @property integer $disbursement_id
 * @property string $disbursement_cheque_number
 * @property integer $deposit_header_id
 */
class DisbursementDepositStaging extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disbursement_deposit_staging';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year_of_study', 'pay_sheet_serial_number', 'instalment_number', 'academic_year', 'created_by', 'header_id', 'disbursement_id', 'deposit_header_id'], 'integer'],
            [['amount'], 'number'],
            [['matching_remarks'], 'string'],
            [['created_by', 'sync_id', 'deposit_header_id'], 'required'],
            [['created_on', 'registration_number'], 'safe'],
            [['index_number', 'loan_item', 'sync_id', 'disbursement_cheque_number'], 'string', 'max' => 100],
            [['student_name'], 'string', 'max' => 200],
            [['sex'], 'string', 'max' => 6],
            [['status'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'index_number' => 'Index Number',
            'student_name' => 'Student Name',
            'registration_number' => 'Registration Number',
            'sex' => 'Sex',
            'year_of_study' => 'Year Of Study',
            'pay_sheet_serial_number' => 'Pay Sheet Serial Number',
            'loan_item' => 'Loan Item',
            'instalment_number' => 'Instalment Number',
            'amount' => 'Amount',
            'academic_year' => 'Academic Year',
            'status' => 'Status',
            'matching_remarks' => 'Matching Remarks',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'header_id' => 'Header ID',
            'sync_id' => 'Sync ID',
            'disbursement_id' => 'Disbursement ID',
            'disbursement_cheque_number' => 'Disbursement Cheque Number',
            'deposit_header_id' => 'Deposit Header ID',
        ];
    }

    public function getDepositBatch(){

        return $this->hasOne(DisbursementDepositBatch::className(),['id'=>'header_id']);
    }
    public function getInstalment(){

        return $this->hasOne(InstalmentDefinition::className(),['instalment_definition_id'=>'instalment_number']);
    }

}
