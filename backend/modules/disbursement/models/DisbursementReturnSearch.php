<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\disbursement\models\DisbursementReturn;

/**
 * DisbursementReturnSearch represents the model behind the search form about `backend\modules\disbursement\models\DisbursementReturn`.
 */
class DisbursementReturnSearch extends DisbursementReturn
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'return_batch_id', 'disbursement_batch_id', 'year_of_study', 'pay_sheet_serial_number', 'loan_item', 'instalment_number', 'academic_year', 'status', 'created_by'], 'integer'],
            [['index_number', 'application_id', 'header_id', 'sex', 'remarks', 'reasons', 'created_on', 'sync_id'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$batchID,$status)
    {
        $query = DisbursementReturn::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['return_batch_id'=>$batchID,'disbursement_return.status'=>$status]);
        $query->andFilterWhere([
            'id' => $this->id,
            'return_batch_id' => $this->return_batch_id,
            'disbursement_batch_id' => $this->disbursement_batch_id,
            'year_of_study' => $this->year_of_study,
            'pay_sheet_serial_number' => $this->pay_sheet_serial_number,
            'loan_item' => $this->loan_item,
            'instalment_number' => $this->instalment_number,
            'amount' => $this->amount,
            'academic_year' => $this->academic_year,
            'disbursement_return.status' => $this->status,
            'created_by' => $this->created_by,
            'created_on' => $this->created_on,
        ]);
        $query->joinWith("application");
        $query->joinwith(["application","application.applicant"]);
        $query->joinwith(["application","application.applicant.user"]);

        $query->andFilterWhere(['like', 'index_number', $this->index_number])
            //->andFilterWhere(['like', 'application_id', $this->application_id])

            ->andFilterWhere(['like', 'header_id', $this->header_id])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'reasons', $this->reasons])
            ->andFilterWhere(['like', 'sync_id', $this->sync_id])

        ->andFilterWhere(['like', 'CONCAT(user.firstname," ",user.middlename," ",user.surname)', $this->application_id]);
       /* ->andFilterWhere(['like', 'user.middlename', $this->application_id])
        ->andFilterWhere(['like', 'user.surname', $this->application_id]);*/

        return $dataProvider;
    }
    public function searchBatch($params,$batchID)
    {
        $query = DisbursementReturn::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['return_batch_id'=>$batchID]);
        $query->andFilterWhere([
            'id' => $this->id,
            'return_batch_id' => $this->return_batch_id,
            'disbursement_batch_id' => $this->disbursement_batch_id,
            'year_of_study' => $this->year_of_study,
            'pay_sheet_serial_number' => $this->pay_sheet_serial_number,
            'loan_item' => $this->loan_item,
            'instalment_number' => $this->instalment_number,
            'amount' => $this->amount,
            'academic_year' => $this->academic_year,
            'disbursement_return.status' => $this->status,
            'created_by' => $this->created_by,
            'created_on' => $this->created_on,
        ]);
        $query->joinWith("application");
        $query->joinwith(["application","application.applicant"]);
        $query->joinwith(["application","application.applicant.user"]);

        $query->andFilterWhere(['like', 'index_number', $this->index_number])
            //->andFilterWhere(['like', 'application_id', $this->application_id])

            ->andFilterWhere(['like', 'header_id', $this->header_id])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'reasons', $this->reasons])
            ->andFilterWhere(['like', 'sync_id', $this->sync_id])

            ->andFilterWhere(['like', 'CONCAT(user.firstname," ",user.middlename," ",user.surname)', $this->application_id]);
        /* ->andFilterWhere(['like', 'user.middlename', $this->application_id])
         ->andFilterWhere(['like', 'user.surname', $this->application_id]);*/

        return $dataProvider;
    }
}
