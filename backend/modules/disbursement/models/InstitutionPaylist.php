<?php

namespace backend\modules\disbursement\models;

use Yii;
use backend\modules\application\models\Applicant;
use backend\modules\allocation\models\Application;
use backend\modules\disbursement\models\InstitutionFundRequest;
use backend\modules\allocation\models\LoanItem;
use backend\modules\allocation\models\LearningInstitution;
use backend\modules\allocation\models\AcademicYear;
use backend\modules\allocation\models\Programme;
use common\models\User;
use backend\modules\disbursement\Module;


/**
 * This is the model class for table "institution_paylist".
 *
 * @property integer $id
 * @property integer $request_id
 * @property integer $academic_year
 * @property integer $disbursement_batch_id
 * @property integer $allocation_batch_id
 * @property integer $allocation_id
 * @property integer $application_id
 * @property integer $programme_id
 * @property integer $loan_item_id
 * @property double $allocated_amount
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * allocation_batch_id,allocation_id
 */
class InstitutionPaylist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institution_paylist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id', 'application_id', 'programme_id', 'loan_item_id', 'status', 'created_at', 'created_by', 'academic_year'], 'required'],
            [['request_id', 'disbursement_batch_id', 'application_id', 'programme_id', 'loan_item_id', 'status', 'created_by', 'academic_year', 'allocation_batch_id', 'allocation_id'], 'integer'],
            [['allocated_amount'], 'number'],
            [['created_at','allocation_batch_id', 'allocation_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_id' => 'Request',
            'academic_year' => 'Academic Year',
            'disbursement_batch_id' => 'Disbursement Batch',
            'allocation_batch_id'=>'Allocation Batch',
            'allocation_id'=>'Allocation',
            'application_id' => 'Student',
            'programme_id' => 'Programme',
            'loan_item_id' => 'Loan Item',
            'allocated_amount' => 'Allocated Amount',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    public function getStudent()
    {
        $output = '';
        $modelApplication = Application::findOne($this->application_id);
        if (sizeof($modelApplication)!=0){
            $modelApplicant = Applicant::findOne($modelApplication->applicant_id);
            if (sizeof($modelApplicant)!=0){
                $modelUser = User::findOne($modelApplicant->user_id);
                if (sizeof($modelUser)!=0){
                    $output = $modelUser->username.' | '.$modelUser->firstname.' '.$modelUser->surname;
                }
            }
        }

        return $output;
    }

    public function getRequest()
    {
        $output = '';
        $model= InstitutionFundRequest::findOne($this->request_id);
        if (sizeof($model)!=0){
           $output = $model->Institution.' - '.$model->LoanItem.' for '.$model->AcademicYear;
        }

        return $output;
    }


    public function getStatus(){

        return Module::PayListStatus($this->status);
    }

    public function getInstitution(){
        $output = '';
        $model = LearningInstitution::findOne($this->institution_id);
        if (sizeof($model)!=0){
            $output = $model->institution_name.' ('.$model->institution_code.')';
        }
        return $output;
    }

    public function getLoanItem(){
        $output = '';
        $model = LoanItem::findOne($this->loan_item_id);
        if (sizeof($model)!=0){
            $output = $model->item_name.' ('.$model->item_code.')';
        }
        return $output;
    }

    public function getAcademicYear(){
        $output = '';
        $model = AcademicYear::findOne($this->academic_year);
        if (sizeof($model)!=0){
            $output = $model->academic_year;
        }
        return $output;
    }

    public function getProgramme(){
        $output = '';
        $model = Programme::findOne($this->programme_id);
        if (sizeof($model)!=0){
            $output = $model->programme_name.' ('.$model->programme_code.')';
        }
        return $output;
    }

}
