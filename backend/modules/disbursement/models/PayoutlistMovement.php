<?php

namespace backend\modules\disbursement\models;

use Yii;
use \backend\modules\disbursement\models\base\PayoutlistMovement as BasePayoutlistMovement;
use backend\modules\disbursement\Module;
use backend\modules\disbursement\models\DisbursementTask;
use backend\modules\disbursement\models\DisbursementBatch;

/**
 * This is the model class for table "disbursement_payoutlist_movement".
 * structure_id, order_level
 */
class PayoutlistMovement extends BasePayoutlistMovement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['disbursements_batch_id', 'from_officer', 'to_officer'], 'required'],
            [['disbursements_batch_id', 'from_officer', 'to_officer', 'movement_status', 'disbursement_task_id', 'structure_id', 'order_level'], 'integer'],
            [['date_out', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'safe'],
            [['comment'], 'string'],
            [['signature'], 'string', 'max' => 200]
        ]);
    }

    public function getFromOfficer(){

        return Module::UserInfo($this->from_officer,'fullName');

    }

    public function getToOfficer(){

        return strtoupper($this->ToStructure).' | '.Module::UserInfo($this->to_officer,'fullName');

    }

    public function getTask(){

        $output = '';
        $model = DisbursementTask::findOne($this->disbursement_task_id);
        if (sizeof($model)!=0){
            $output = $model->task_name;
        }
        return $output;

    }

    public function getDuration(){
        $output = '';
        $prevID = $this->movement_id - 1;
        $prevSQL = "
                      SELECT * FROM disbursement_payoutlist_movement 
                      WHERE movement_status = '1' AND 
                      movement_id = '$prevID'";
        $prevModel = Yii::$app->db->createCommand($prevSQL)->queryAll();
        if (sizeof($prevModel)!=0){
            $startDate = $prevModel[0]['date_out'];

        }else{
            $startDate = $this->created_at;
            $prevSQL0 = "
                      SELECT * FROM disbursement_payoutlist_movement 
                      WHERE movement_status = '0' AND 
                      movement_id = '$prevID'";
            $prevModel0 = Yii::$app->db->createCommand($prevSQL0)->queryAll();
            if (sizeof($prevModel0)!=0){
                $startDate = $endDate = date('Y-m-d H:i:s');
            }else{
                $startDate = $this->created_at;
                $endDate = date('Y-m-d H:i:s');
            }

        }

        if ($this->date_out!='' && !empty($this->date_out) && $this->date_out!=null){
            $startDate = $this->created_at;
            $endDate = $this->date_out;
        }else{
            $startDate = $this->created_at;
            $endDate = date('Y-m-d H:i:s');
        }
        $output = Module::span($startDate,$endDate,'a');



        return $output;
    }

    public function getFromStructure(){
        return Module::Structure($this->disbursements_batch_id,$this->from_officer,$this->disbursement_task_id);
    }

    public function getToStructure(){
        return Module::Structure($this->disbursements_batch_id,$this->to_officer,$this->disbursement_task_id);
    }
	
}
