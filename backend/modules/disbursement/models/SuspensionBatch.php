<?php

namespace backend\modules\disbursement\models;

use Yii;

/**
 * This is the model class for table "suspension_batch".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $files_uploaded
 * @property integer $status
 * @property string $status_date
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $type
 */
class SuspensionBatch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suspension_batch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['description', 'files_uploaded', 'type'], 'string'],
            [['status', 'created_by', 'updated_by'], 'integer'],
            [['status_date', 'created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['title'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'files_uploaded' => 'Files Uploaded',
            'status' => 'Status',
            'status_date' => 'Status Date',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'type' => 'Type',
        ];
    }
}
