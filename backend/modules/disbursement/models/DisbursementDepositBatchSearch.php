<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\disbursement\models\DisbursementDepositBatch;

/**
 * DisbursementDepositBatchSearch represents the model behind the search form about `backend\modules\disbursement\models\DisbursementDepositBatch`.
 */
class DisbursementDepositBatchSearch extends DisbursementDepositBatch
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'institution_id', 'academic_year', 'created_by'], 'integer'],
            [['description', 'deposit_date', 'deposit_cheque_number', 'status', 'created_on'], 'safe'],
            [['cheque_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DisbursementDepositBatch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'institution_id' => $this->institution_id,
            'academic_year' => $this->academic_year,
            'deposit_date' => $this->deposit_date,
            'cheque_amount' => $this->cheque_amount,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'deposit_cheque_number', $this->deposit_cheque_number])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
