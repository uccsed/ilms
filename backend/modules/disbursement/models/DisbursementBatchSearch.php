<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\disbursement\models\DisbursementBatch;
use backend\modules\disbursement\Module;

/**
 * DisbursementBatchSearch represents the model behind the search form about `backend\modules\disbursement\models\DisbursementBatch`.
 */
class DisbursementBatchSearch extends DisbursementBatch
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['disbursement_batch_id', 'allocation_batch_id', 'learning_institution_id', 'academic_year_id', 'instalment_definition_id', 'batch_number', 'instalment_type', 'is_approved', 'is_submitted', 'institution_payment_request_id', 'created_by'], 'integer'],
            [['batch_desc', 'approval_comment', 'payment_voucher_number', 'cheque_number', 'created_at','loan_item_id','semester_number','financial_year_id', 'is_submitted', 'is_approved'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$approval=null)
    {
        //$query = DisbursementBatch::find()->where(["learning_institution_id"=>Yii::$app->session["learn_institution_id"]]);
        $query = DisbursementBatch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

      /*  if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }*/
        // grid filtering conditions
        //if ($approval!='' && $approval!==null && !empty($approval)){
        if(isset($approval) && $approval!==null){
            $query->andWhere(['is_approved'=>$approval]);
        }

        //}
        $query->andFilterWhere([
            'disbursement_batch_id' => $this->disbursement_batch_id,
            'allocation_batch_id' => $this->allocation_batch_id,
            'learning_institution_id' => $this->learning_institution_id,
            'academic_year_id' => $this->academic_year_id,
            'instalment_definition_id' => $this->instalment_definition_id,
            'batch_number' => $this->batch_number,
            'instalment_type' => $this->instalment_type,
            'is_approved' => $this->is_approved,
            'is_submitted' => $this->is_submitted,
            'institution_payment_request_id' => $this->institution_payment_request_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'disbursed_as' => $this->disbursed_as,
            'loan_item_id' => $this->loan_item_id,
            'semester_number' => $this->semester_number,
            'financial_year_id' => $this->financial_year_id,

        ]);

        $query->andFilterWhere(['like', 'batch_desc', $this->batch_desc])
            ->andFilterWhere(['like', 'approval_comment', $this->approval_comment])
            ->andFilterWhere(['like', 'payment_voucher_number', $this->payment_voucher_number])
            ->andFilterWhere(['like', 'cheque_number', $this->cheque_number]);

        return $dataProvider;
    }

    public function searchSpecial($params,$type,$approval=null)
    {
        $query = DisbursementBatch::find();
            //->where(["learning_institution_id"=>Yii::$app->session["learn_institution_id"],'disbursed_as'=>$type]);


        $user= Yii::$app->user->id;
        $array = Module::AssignmentArray($user);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $WHERE = array();
        if(isset($approval) && $approval!==null){
            //$query->andWhere(['is_approved'=>$approval]);
            $WHERE = ['is_approved'=>$approval];
        }
      /*  if ($approval!==null&&$approval!==''&&!empty($approval)){
            $WHERE = ['is_approved'=>$approval];
        }*/
        $query->andWhere(array_merge(['learning_institution_id'=>$array],$WHERE));
        $query->andFilterWhere([
            'disbursement_batch_id' => $this->disbursement_batch_id,
            'allocation_batch_id' => $this->allocation_batch_id,
            'learning_institution_id' => $this->learning_institution_id,
            'academic_year_id' => $this->academic_year_id,
            'instalment_definition_id' => $this->instalment_definition_id,
            'batch_number' => $this->batch_number,
            'instalment_type' => $this->instalment_type,
            'is_approved' => $this->is_approved,
            'is_submitted' => $this->is_submitted,
            'institution_payment_request_id' => $this->institution_payment_request_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'loan_item_id' => $this->loan_item_id,
            'semester_number' => $this->semester_number,
            'financial_year_id' => $this->financial_year_id,
            'is_approved' => $this->is_approved,
            //'disbursed_as' => $this->disbursed_as,
            'disbursed_as' => $type,

        ]);

        $query->andFilterWhere(['like', 'batch_desc', $this->batch_desc])
            ->andFilterWhere(['like', 'approval_comment', $this->approval_comment])
            ->andFilterWhere(['like', 'payment_voucher_number', $this->payment_voucher_number])
            ->andFilterWhere(['like', 'cheque_number', $this->cheque_number]);

        return $dataProvider;
    }

     public function searchreviewdisbursement($params,$approval=null,$submitted=null)
    {
        $query = DisbursementBatch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        //if ($approval!==null&&$approval!=''&&!empty($approval)){
        if ($approval!==null){
            $query->andWhere(['is_approved'=>$approval]);
        }
        if ($submitted!==null){
            $query->andWhere(['is_submitted'=>$submitted]);
        }

        $query->andFilterWhere([
            'disbursement_batch_id' => $this->disbursement_batch_id,
            'allocation_batch_id' => $this->allocation_batch_id,
            'learning_institution_id' => $this->learning_institution_id,
            'academic_year_id' => $this->academic_year_id,
            'instalment_definition_id' => $this->instalment_definition_id,
            'batch_number' => $this->batch_number,
            'instalment_type' => $this->instalment_type,
            //'is_approved' => $this->is_approved,
            'is_submitted' => $this->is_submitted,
            'institution_payment_request_id' => $this->institution_payment_request_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'batch_desc', $this->batch_desc])
            ->andFilterWhere(['like', 'approval_comment', $this->approval_comment])
            ->andFilterWhere(['like', 'payment_voucher_number', $this->payment_voucher_number])
            ->andFilterWhere(['like', 'cheque_number', $this->cheque_number]);

        return $dataProvider;
    }
}
