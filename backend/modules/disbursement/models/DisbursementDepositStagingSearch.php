<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\disbursement\models\DisbursementDepositStaging;

/**
 * DisbursementDepositStagingSearch represents the model behind the search form about `backend\modules\disbursement\models\DisbursementDepositStaging`.
 */
class DisbursementDepositStagingSearch extends DisbursementDepositStaging
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year_of_study', 'pay_sheet_serial_number', 'instalment_number', 'academic_year', 'created_by', 'header_id', 'disbursement_id', 'deposit_header_id'], 'integer'],
            [['index_number', 'student_name', 'sex', 'loan_item', 'status', 'matching_remarks', 'created_on', 'sync_id', 'disbursement_cheque_number'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$batchID,$status)
    {
        $query = DisbursementDepositStaging::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andWhere(['header_id'=>$batchID,'status'=>$status]);
        $query->andFilterWhere([
            'id' => $this->id,
            'year_of_study' => $this->year_of_study,
            'pay_sheet_serial_number' => $this->pay_sheet_serial_number,
            'instalment_number' => $this->instalment_number,
            'amount' => $this->amount,
            'academic_year' => $this->academic_year,
            'created_by' => $this->created_by,
            'created_on' => $this->created_on,
            'header_id' => $this->header_id,
            'disbursement_id' => $this->disbursement_id,
            'deposit_header_id' => $this->deposit_header_id,
        ]);

        $query->andFilterWhere(['like', 'index_number', $this->index_number])
            ->andFilterWhere(['like', 'student_name', $this->student_name])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'loan_item', $this->loan_item])
           // ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'matching_remarks', $this->matching_remarks])
            ->andFilterWhere(['like', 'sync_id', $this->sync_id])
            ->andFilterWhere(['like', 'disbursement_cheque_number', $this->disbursement_cheque_number]);

        return $dataProvider;
    }
}
