<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\allocation\models\LearningInstitution;
use backend\modules\allocation\models\Programme;
use backend\modules\allocation\models\LoanItem;
use backend\modules\disbursement\models\Instalment;
use backend\modules\allocation\models\AcademicYear;
use common\models\Semester;
use backend\modules\disbursement\Module;

use frontend\modules\repayment\models\User;
/**
 * This is the model class for table "disbursement_plan".
 *
 * @property integer $disbursement_plan_id
 * @property integer $learning_institution
 * @property integer $academic_year
 * @property integer $year_of_study
 * @property integer $programme_id
 * @property integer $instalment_id
 * @property integer $loan_item_id
 * @property integer $semester_number
 * @property double $disbursement_percent
 * @property string $sync_id
 * @property string $date_created
 * @property integer $created_by
 * @property integer $is_default
 */
class DisbursementPlan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disbursement_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['learning_institution', 'academic_year', 'year_of_study', 'programme_id', 'instalment_id', 'loan_item_id', 'semester_number','disbursement_percent', 'sync_id'], 'required'],
            [['learning_institution', 'academic_year', 'year_of_study', 'programme_id', 'instalment_id','semester_number', 'loan_item_id', 'created_by', 'is_default'], 'integer'],
            [['disbursement_percent'], 'number'],
            [['date_created'], 'safe'],
            [['sync_id'], 'string', 'max' => 30],
            [['sync_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'disbursement_plan_id' => 'Disbursement Plan ID',
            'learning_institution' => 'Learning Institution',
            'academic_year' => 'Academic Year',
            'year_of_study' => 'Year Of Study',
            'programme_id' => 'Programme ID',
            'instalment_id' => 'Instalment ID',
            'loan_item_id' => 'Loan Item ID',
            'semester_number' => 'Semester Number',
            'disbursement_percent' => 'Disbursement Percent',
            'sync_id' => 'Sync ID',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'is_default' => 'Is Default',
        ];
    }

    public function search($params,$institution=null)
    {
        $query = DisbursementPlan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        /* if (!$this->validate()) {
             // uncomment the following line if you do not want to return any records when validation fails
             // $query->where('0=1');
             return $dataProvider;
         }*/

        // grid filtering conditions
        if ($institution!==null){
            $query->andWhere(['learning_institution' => $institution]);
        }


        $query->andFilterWhere([
            'disbursement_plan_id' => $this->disbursement_plan_id,
            'learning_institution' => $this->learning_institution,
            'academic_year' => $this->academic_year,
            'year_of_study' => $this->year_of_study,
            'programme_id' => $this->programme_id,
            'instalment_id' => $this->instalment_id,
            'loan_item_id' => $this->loan_item_id,
            'semester_number' => $this->semester_number,
            'disbursement_percent' => $this->disbursement_percent,
            'sync_id' => $this->sync_id,
            'created_by' => $this->created_by,

            'date_created' => $this->date_created,
            'is_default' => $this->is_default,

        ]);

        return $dataProvider;
    }

    public function searchInstitutional($params)
    {
        $query = DisbursementPlan::find()->groupBy(['learning_institution']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        /* if (!$this->validate()) {
             // uncomment the following line if you do not want to return any records when validation fails
             // $query->where('0=1');
             return $dataProvider;
         }*/

        // grid filtering conditions



        $query->andFilterWhere([
            'disbursement_plan_id' => $this->disbursement_plan_id,
            'learning_institution' => $this->learning_institution,
            'academic_year' => $this->academic_year,
            'year_of_study' => $this->year_of_study,
            'programme_id' => $this->programme_id,
            'instalment_id' => $this->instalment_id,
            'loan_item_id' => $this->loan_item_id,
            'semester_number' => $this->semester_number,
            'disbursement_percent' => $this->disbursement_percent,
            'sync_id' => $this->sync_id,
            'created_by' => $this->created_by,

            'date_created' => $this->date_created,
            'is_default' => $this->is_default,

        ]);

        return $dataProvider;
    }

    public function getInstitution(){
        $output = '';
        $model = LearningInstitution::findOne($this->learning_institution);
        if (sizeof($model)!=0){
            $output = $model->institution_name.' ('.$model->institution_code.')';
        }

        return $output;
    }


    public function getProgramme(){
        $output = '';
        $model = Programme::findOne($this->programme_id);
        if (sizeof($model)!=0){
            $output = $model->programme_name.' ('.$model->programme_code.')';
        }

        return $output;
    }


    public function getLoanItem(){
        $output = '';
        $model = LoanItem::findOne($this->loan_item_id);
        if (sizeof($model)!=0){
            $output = $model->item_name.' ('.$model->item_code.')';
        }
        return $output;
    }

    public function getAcademicYear(){
        $output = '';
        $model = AcademicYear::findOne($this->academic_year);
        if (sizeof($model)!=0){
            $output = $model->academic_year;
        }
        return $output;
    }

    public function getInstalment(){
        $output = '';
        $model = Instalment::findOne($this->instalment_id);
        if (sizeof($model)!=0){
            $output = Module::THNumber($model->instalment).' Instalment';
        }
        return $output;
    }

    public function getSemester(){
        $output = '';
        $model = Semester::findOne($this->semester_number);
        if (sizeof($model)!=0){
            $output = Module::THNumber($model->semester_number).' Semester';
        }
        return $output;
    }


}
