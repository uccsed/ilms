<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\allocation\models\LoanItem;
use backend\modules\allocation\models\ApplicantCategory;
use backend\modules\disbursement\Module;

/**
 * This is the model class for table "item_disbursement_setting".
 *
 * @property integer $id
 * @property integer $loan_item_id
 * @property integer $study_category
 * @property integer $study_level
 * @property integer $disbursed_to
 * @property string $sync_id
 */
class ItemDisbursementSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_disbursement_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loan_item_id', 'study_category','study_level', 'disbursed_to'], 'required'],
            [['loan_item_id', 'study_category','study_level', 'disbursed_to'], 'integer'],
            [['sync_id'], 'string', 'max' => 100],
            [['sync_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'loan_item_id' => 'Loan Item',
            'study_category' => 'Study Category',
            'study_level' => 'Study Level',
            'disbursed_to' => 'Disbursed To',
            'sync_id' => 'Sync ID',
        ];
    }

    public function search($params)
    {
        $query = ItemDisbursementSetting::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        /* if (!$this->validate()) {
             // uncomment the following line if you do not want to return any records when validation fails
             // $query->where('0=1');
             return $dataProvider;
         }*/

        // grid filtering conditions
        $query->andFilterWhere([
            'loan_item_id' => $this->loan_item_id,
            'study_category' => $this->study_category,
            // 'comment' => $this->comment,
            'study_level' => $this->study_level,
            'disbursed_to' => $this->disbursed_to,
        ]);

        return $dataProvider;
    }

    public function getLoanItem(){
        $output = '';
        $model = LoanItem::findOne($this->loan_item_id);
        if (sizeof($model)!=0){
            $output = $model->item_name.' ('.$model->item_code.')';
        }
        return $output;
    }

    public function getStudyLevel(){
        $output = '';
        $model = ApplicantCategory::findOne($this->study_level);
        if (sizeof($model)!=0){
            $output = $model->applicant_category;
        }
        return $output;
    }

    public function getStudyCategory(){
        $output = '';
        $model = Module::StudyCategory($this->study_category);
        if (!is_array($model)){
            $output = $model;
        }
        return $output;
    }

    public function getDisbursedTo(){
        $output = '';
        $model = Module::DisbursedEntities($this->disbursed_to);
        if (!is_array($model)){
            $output = $model;
        }
        return $output;
    }





}
