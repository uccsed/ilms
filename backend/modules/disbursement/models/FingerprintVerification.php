<?php

namespace backend\modules\disbursement\models;

use backend\modules\allocation\models\AcademicYear;
use common\models\Semester;
use Yii;

/**
 * This is the model class for table "fingerprint_verification".
 *
 * @property integer $id
 * @property string $index_number
 * @property integer $academic_year
 * @property integer $institution_id
 * @property integer $semester_number
 * @property integer $instalment_number
 * @property string $created_date
 * @property integer $created_by
 * @property string $method
 * @property integer $remarks
 * @property string $operation
 * @property string $sync_id
 */
class FingerprintVerification extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fingerprint_verification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'file'],
            [['index_number'], 'required'],
            [['sync_id'], 'unique'],
            [['academic_year', 'institution_id', 'semester_number', 'instalment_number', 'created_by', 'remarks'], 'integer'],
            [['created_date','sync_id'], 'safe'],
            [['index_number'], 'string', 'max' => 200],
            [['method'], 'string', 'max' => 20],
            [['operation'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'index_number' => 'Index Number',
            'academic_year' => 'Academic Year',
            'institution_id' => 'Institution ID',
            'semester_number' => 'Semester Number',
            'instalment_number' => 'Instalment Number',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'method' => 'Method',
            'remarks' => 'Remarks',
            'operation' => 'Operation',
            'sync_id' => 'Enrollment Code',
        ];
    }
    public function getAcademicYear(){
        return $this->hasOne(AcademicYear::className(),['academic_year_id'=>'academic_year']);
    }
    public function getSemester(){
        return $this->hasOne(Semester::className(),['semester_id'=>'semester_number']);
    }

    public function getInstalment(){
        return $this->hasOne(InstalmentDefinition::className(),['instalment_definition_id'=>'instalment_number']);
    }
}
