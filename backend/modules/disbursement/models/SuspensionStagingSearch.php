<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\disbursement\models\SuspensionStaging;

/**
 * SuspensionStagingSearch represents the model behind the search form about `backend\modules\disbursement\models\SuspensionStaging`.
 */
class SuspensionStagingSearch extends SuspensionStaging
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'header_id', 'created_by', 'updated_by', 'suspension_reason'], 'integer'],
            [['sn', 'index_number', 'names', 'programme', 'year_of_study', 'status_date', 'created_at', 'updated_at', 'suspension_reason'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SuspensionStaging::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'status_date' => $this->status_date,
            'header_id' => $this->header_id,
            'suspension_reason' => $this->suspension_reason,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'sn', $this->sn])
            ->andFilterWhere(['like', 'index', $this->index_number])
            ->andFilterWhere(['like', 'names', $this->names])
            ->andFilterWhere(['like', 'programme', $this->programme])
            ->andFilterWhere(['like', 'year_of_study', $this->year_of_study]);

        return $dataProvider;
    }

    public function SpecialSearch($params,$batchID,$status=null)
    {
        $query = SuspensionStaging::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if ($status!==null){
            $query->andWhere(['status'=>$status]);
        }
        $query->andWhere(['header_id'=>$batchID]);

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'status_date' => $this->status_date,
            'suspension_reason' => $this->suspension_reason,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'sn', $this->sn])
            ->andFilterWhere(['like', 'index', $this->index_number])
            ->andFilterWhere(['like', 'names', $this->names])
            ->andFilterWhere(['like', 'programme', $this->programme])
            ->andFilterWhere(['like', 'year_of_study', $this->year_of_study]);

        return $dataProvider;
    }
}
