<?php

namespace backend\modules\disbursement\models;

use common\models\Country;
use Yii;

/**
 * This is the model class for table "overseas_settings".
 *
 * @property integer $id
 * @property integer $academic_year
 * @property integer $country_id
 * @property integer $year_of_study
 * @property integer $loan_item
 * @property integer $instalment
 * @property double $disbursement_percent
 * @property string $sync_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */

//created_at,created_by,updated_at,updated_by
class OverseasSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'overseas_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['academic_year', 'country_id', 'year_of_study', 'loan_item', 'instalment', 'disbursement_percent'], 'required'],
            [['academic_year', 'country_id', 'year_of_study', 'loan_item', 'instalment'], 'integer'],
            [['disbursement_percent'], 'number'],
            [['sync_id'], 'string', 'max' => 200],
            [['sync_id'], 'unique'],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'academic_year' => 'Academic Year',
            'country_id' => 'Country ID',
            'year_of_study' => 'Year Of Study',
            'loan_item' => 'Loa Item',
            'instalment' => 'Instalment',
            'disbursement_percent' => 'Disbursement Percent',
            'sync_id' => 'Sync ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry() {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    public function getAcademicYear() {
        return $this->hasOne(\common\models\AcademicYear::className(), ['academic_year_id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstalmentDefinition() {
        return $this->hasOne(InstalmentDefinition::className(), ['instalment_definition_id' => 'instalment']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanItem() {
        return $this->hasOne(\backend\modules\allocation\models\LoanItem::className(), ['loan_item_id' => 'loan_item']);
    }



}
