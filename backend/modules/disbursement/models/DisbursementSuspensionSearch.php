<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\disbursement\models\DisbursementSuspension;

/**
 * DisbursementSuspensionSearch represents the model behind the search form about `backend\modules\disbursement\models\DisbursementSuspension`.
 */
class DisbursementSuspensionSearch extends DisbursementSuspension
{

    public $firstName;
    public $middleName;
    public $surName;
    public $f4indexno;
    public $Institution;
    public $Programme;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'application_id', 'loan_item_id', 'status', 'status_reason', 'status_by','mode'], 'integer'],
            [['suspension_percent', 'remaining_percent'], 'number'],
            [['remarks', 'status_date', 'supporting_document', 'suspension_history', 'sync_id','f4indexno','firstName','middleName','surName','Institution','Programme'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$type=null)
    {

        $query = DisbursementSuspension::find();



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if ($type!==null){
            $condition = ['status'=>$type];
            $query->andFilterWhere($condition);
        }



            $query->andFilterWhere([
            'id' => $this->id,
            'application_id' => $this->application_id,
            'loan_item_id' => $this->loan_item_id,
            'suspension_percent' => $this->suspension_percent,
            'remaining_percent' => $this->remaining_percent,
            'status' => $this->status,
            'status_reason' => $this->status_reason,
            'status_date' => $this->status_date,
            'status_by' => $this->status_by,
            'mode' => $this->mode,
        ]);

        $query->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'supporting_document', $this->supporting_document])
            ->andFilterWhere(['like', 'suspension_history', $this->suspension_history])
            ->andFilterWhere(['like', 'sync_id', $this->sync_id]);

        return $dataProvider;
    }

    public function SpecialSearch($params,$type)
    {

        $query = DisbursementSuspension::find()->groupBy(['application_id']);



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if ($type!==null){
            $condition = ['disbursement_suspension.status'=>$type];
            $query->andFilterWhere($condition);
        }



        $query->andFilterWhere([
            'id' => $this->id,
            'application_id' => $this->application_id,
            'loan_item_id' => $this->loan_item_id,
            'suspension_percent' => $this->suspension_percent,
            'remaining_percent' => $this->remaining_percent,
            'disbursement_suspension.status' => $this->status,
            'status_reason' => $this->status_reason,
            'status_date' => $this->status_date,
            'status_by' => $this->status_by,
            'mode' => $this->mode,
        ]);
        // grid filtering conditions
        $query->joinWith("application");
        $query->joinwith(["application","application.applicant"]);
        $query->joinwith(["application","application.programme"]);
        $query->joinwith(["application","application.programme.programmeGroup"]);

        $query->joinwith(["application","application.programme.learningInstitution"]);
        $query->joinwith(["application","application.applicant.user"]);

        $query->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'supporting_document', $this->supporting_document])
            ->andFilterWhere(['like', 'suspension_history', $this->suspension_history])
            ->andFilterWhere(['like', 'sync_id', $this->sync_id])
        ->andFilterWhere(['like', 'user.surname', $this->surName])
        ->andFilterWhere(['like', 'user.middlename', $this->middleName])
        ->andFilterWhere(['like', 'applicant.f4indexno', $this->f4indexno])
        ->andFilterWhere(['like', 'programme_group.group_code', $this->Programme])
        ->andFilterWhere(['like', 'learning_institution.institution_code', $this->Institution]);

        return $dataProvider;
    }
}
