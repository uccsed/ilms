<?php

namespace backend\modules\disbursement\models;

use Yii;

/**
 * This is the model class for table "disbursement_return_staging".
 *
 * @property integer $id
 * @property string $index_number
 * @property string $student_name
 * @property string $sex
 * @property integer $year_of_study
 * @property integer $pay_sheet_serial_number
 * @property string $loan_item
 * @property integer $instalment_number
 * @property double $amount
 * @property string $remarks
 * @property integer $academic_year
 * @property string $status
 * @property double $match_percent
 * @property string $reasons
 * @property integer $created_by
 * @property string $created_on
 * @property integer $header_id
 * @property integer $disbursement_id
 * @property string $sync_id
 *
 * id,index_number,student_name,sex,year_of_study,pay_sheet_serial_number,loan_item,instalment_number,amount,remarks,academic_year,status,match_percent,reasons,created_by,created_on,header_id,sync_id
 *
 *
 * disbursement_cheque_number,specified_header_id,specified_student_category
 */
class DisbursementReturnStaging extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disbursement_return_staging';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year_of_study', 'pay_sheet_serial_number', 'instalment_number', 'academic_year', 'created_by', 'header_id', 'disbursement_id'], 'integer'],
            [['amount', 'match_percent'], 'number'],
            [['remarks', 'reasons', 'disbursement_cheque_number', 'specified_header_id', 'specified_student_category'], 'string'],
            [['created_by', 'sync_id'], 'required'],
            [['created_on', 'disbursement_id', 'disbursement_cheque_number', 'specified_header_id', 'specified_student_category'], 'safe'],
            [['index_number', 'loan_item', 'sync_id'], 'string', 'max' => 100],
            [['student_name'], 'string', 'max' => 200],
            [['sex'], 'string', 'max' => 6],
            [['status'], 'string', 'max' => 20],
            [['sync_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'index_number' => 'Index Number',
            'student_name' => 'Student Name',
            'sex' => 'Sex',
            'year_of_study' => 'Year Of Study',
            'pay_sheet_serial_number' => 'Pay Sheet Serial Number',
            'loan_item' => 'Loan Item',
            'instalment_number' => 'Instalment Number',
            'amount' => 'Amount',
            'remarks' => 'Remarks',
            'academic_year' => 'Academic Year',
            'status' => 'Status',
            'match_percent' => 'Match Percent',
            'reasons' => 'Reasons',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'header_id' => 'Header ID',
            'sync_id' => 'Sync ID',
            'disbursement_id' => 'Disbursement ID',
            'disbursement_cheque_number'=>'HESLB Cheque Number',
            'specified_header_id'=>'HEADER',
            'specified_student_category'=>'Category'
        ];
    }

    public function getReturnBatch(){

        return $this->hasOne(DisbursementReturnBatch::className(),['id'=>'header_id']);
    }
   /* public function getInstalment(){
        return $this->hasOne(InstalmentDefinition::className(),['instalment_definition_id'=>'instalment_number']);
    }*/

}
