<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\disbursement\models\InstitutionFundRequest;


/**
 * InstitutionFundRequestSearch represents the model behind the search form about `backend\modules\disbursement\models\InstitutionFundRequest`.
 */
class InstitutionFundRequestSearch extends InstitutionFundRequest
{
    public $request_amount;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'institution_id', 'loan_item', 'academic_year', 'submitted', 'submitted_by', 'approval', 'approved_by', 'request_status', 'status_by'], 'integer'],
            [['invoice_number', 'description', 'submitted_on', 'approval_comments', 'approval_date', 'status_date','request_amount'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search($params)
    {
        $query = InstitutionFundRequest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'institution_id' => $this->institution_id,
            'loan_item' => $this->loan_item,
            'academic_year' => $this->academic_year,
            'submitted' => $this->submitted,
            'submitted_on' => $this->submitted_on,
            'submitted_by' => $this->submitted_by,
            'approval' => $this->approval,
            'approval_date' => $this->approval_date,
            'approved_by' => $this->approved_by,
            'request_status' => $this->request_status,
            'status_date' => $this->status_date,
            'status_by' => $this->status_by,

        ]);

        $query->andFilterWhere(['like', 'invoice_number', $this->invoice_number])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'approval_comments', $this->approval_comments]);

        return $dataProvider;
    }



    public function searchSpecial($params,$status)
    {
        $query = InstitutionFundRequest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['request_status'=>$status]);
        $query->andFilterWhere([
            'id' => $this->id,
            'institution_id' => $this->institution_id,
            'loan_item' => $this->loan_item,
            'academic_year' => $this->academic_year,
            'submitted' => $this->submitted,
            'submitted_on' => $this->submitted_on,
            'submitted_by' => $this->submitted_by,
            'approval' => $this->approval,
            'approval_date' => $this->approval_date,
            'approved_by' => $this->approved_by,
            //'request_status' => $this->request_status,
            'status_date' => $this->status_date,
            'status_by' => $this->status_by,
        ]);

        $query->andFilterWhere(['like', 'invoice_number', $this->invoice_number])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'approval_comments', $this->approval_comments]);

        return $dataProvider;
    }

}
