<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 10/12/18
 * Time: 9:52 AM
 */

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use backend\modules\allocation\models\AllocationBatch;
use backend\modules\allocation\models\Allocation;
use yii\data\ActiveDataProvider;
class AllocationBatchSearcher extends AllocationBatch
{

    public $firstName;
    public $surName;
    public $f4indexno;
    public $Institution;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['allocation_batch_id', 'batch_number', 'academic_year_id', 'is_approved', 'created_by', 'is_canceled'], 'integer'],
            [['batch_desc', 'approval_comment', 'created_at', 'cancel_comment'], 'safe'],
            [['available_budget'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    public function searcher($params,$id)
    {
        $query = Allocation::find()->where(["allocation_batch_id"=>$id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' =>FALSE,
//             'sort' => ['attributes' => [
//
//                   //related columns
//                   'firstName' => [
//                        'asc' => ['user.firstname' => SORT_ASC],
//                        'default' => SORT_ASC
//                   ],
//                   'surName' => [
//                       'asc' => ['user.surname' => SORT_ASC],
//                        'default' => SORT_ASC
//                   ],
//                  'f4indexno' => [
//                       'asc' => ['applicant.f4indexno' => SORT_ASC],
//                        'default' => SORT_ASC
//                   ],
//                  'surName' => [
//                       'asc' => ['user.surname' => SORT_ASC],
//                        'default' => SORT_ASC
//                   ],
//                  'surName' => [
//                       'asc' => ['user.surname' => SORT_ASC],
//                        'default' => SORT_ASC
//                   ],
//              ],],
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->joinWith("application");
        $query->joinwith(["application","application.applicant"]);
        $query->joinwith(["application","application.programme"]);
        $query->joinwith(["application","application.programme.learningInstitution"]);
        $query->joinwith(["application","application.applicant.user"]);
        $query->andFilterWhere([
           // 'allocation_id' => $this->allocation_id,
            'allocation_batch_id' => $this->allocation_batch_id,
           /* 'application_id' => $this->application_id,
            'loan_item_id' => $this->loan_item_id,
            'allocated_amount' => $this->allocated_amount,*/
            'is_canceled' => $this->is_canceled,
        ]);
        $query->andFilterWhere(['like', 'user.firstname', $this->firstName])
            ->andFilterWhere(['like', 'user.surname', $this->surName])
            ->andFilterWhere(['like', 'applicant.f4indexno', $this->f4indexno])
            ->andFilterWhere(['like', 'learningInstitution.institution_code', $this->Institution])
            ->andFilterWhere(['like', 'cancel_comment', $this->cancel_comment]);
        return $dataProvider;
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = AllocationBatch::find()->where("disburse_status IN(0,1)");
        $query = AllocationBatch::find()->where("disburse_status IN(0,1) AND (larc_approve_status = '1' OR partial_approve_status = '1') ");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'allocation_batch_id' => $this->allocation_batch_id,
            'batch_number' => $this->batch_number,
            'academic_year_id' => $this->academic_year_id,
            'available_budget' => $this->available_budget,
            'is_approved' => $this->is_approved,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'is_canceled' => $this->is_canceled,
        ]);

        $query->andFilterWhere(['like', 'batch_desc', $this->batch_desc])
            ->andFilterWhere(['like', 'approval_comment', $this->approval_comment])

            ->andFilterWhere(['like', 'cancel_comment', $this->cancel_comment]);

        return $dataProvider;
    }
    public function searchhli($params)
    {
        $query = AllocationBatch::find()->where("disburse_status IN(0,1)");


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions

        $query->andFilterWhere([
            'allocation_batch_id' => $this->allocation_batch_id,
            'batch_number' => $this->batch_number,
            'academic_year_id' => $this->academic_year_id,
            'available_budget' => $this->available_budget,
            'is_approved' => $this->is_approved,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'is_canceled' => $this->is_canceled,
        ]);

        $query->andFilterWhere(['like', 'batch_desc', $this->batch_desc])
            ->andFilterWhere(['like', 'approval_comment', $this->approval_comment])

            ->andFilterWhere(['like', 'cancel_comment', $this->cancel_comment]);

        return $dataProvider;
    }




}