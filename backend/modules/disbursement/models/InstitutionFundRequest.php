<?php

namespace backend\modules\disbursement\models;

use Yii;
use backend\modules\allocation\models\LearningInstitution;
use common\models\User;
use backend\modules\allocation\models\AcademicYear;
use backend\modules\allocation\models\LoanItem;
use common\models\Semester;
use backend\modules\disbursement\Module;

/**
 * This is the model class for table "institution_fund_request".
 *
 * @property integer $id
 * @property integer $institution_id
 * @property integer $loan_item
 * @property string $invoice_number
 * @property integer $semester_number
 * @property string $control_number
 * @property integer $academic_year
 * @property string $description
 * @property integer $submitted
 * @property string $submitted_on
 * @property integer $submitted_by
 * @property integer $approval
 * @property string $approval_comments
 * @property string $approval_date
 * @property integer $approved_by
 * @property integer $request_status
 * @property string $status_date
 * @property integer $status_by
 * @property string $created_at
 * @property integer $created_by
 *
 * @property integer $acceptance
 * @property string $acceptance_date
 * @property integer $acceptance_by
 * @property string $acceptance_remarks
 * @property string $official_document
 *
 * @property float $request_amount

 * created_at,created_by
 * acceptance, acceptance_date, acceptance_by, acceptance_remarks, official_document
 */
class InstitutionFundRequest extends \yii\db\ActiveRecord
{
    public $request_amount;
    public  $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institution_fund_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $extra = [];
        /*$action = Yii::$app->controller->action->id;
        $extra = [];
        $SQL = "SELECT * FROM learning_institution WHERE learning_institution_id = '$this->institution_id' AND ownership = '1'";
        $instModel = Yii::$app->db->createCommand($SQL)->queryAll();
        switch ($action){


            case "view":
                if (isset($_POST)){
                    $extra = ['invoice_number', 'approval', 'approval_comments', 'file'];
                    if (sizeof($instModel)!=0){ //Set Control number to be mandatory for government owned institutions
                        $extra = array_merge($extra,['control_number']);
                    }
                }

                break;
        }*/

        if (Yii::$app->controller->id=='fund-request-approval'){
            $extra = ['acceptance', 'acceptance_date', 'acceptance_by', 'acceptance_remarks', 'file'];
        }





        return [
            [['file'], 'file'],
            //[['file'], 'file', 'extensions' => 'pdf, jpg'],
            [array_merge(['institution_id', 'loan_item', 'academic_year', 'description','semester_number'],$extra), 'required'],
            [['institution_id', 'loan_item','semester_number', 'academic_year', 'submitted', 'submitted_by', 'approval', 'approved_by', 'request_status', 'status_by'], 'integer'],
            [['description', 'approval_comments'], 'string'],
            [['submitted_on', 'approval_date', 'status_date', 'created_at', 'created_by', 'control_number','acceptance', 'acceptance_date', 'acceptance_by', 'acceptance_remarks', 'official_document'], 'safe'],
            [['invoice_number'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {

        switch (strtoupper(Yii::$app->controller->id)){
            case "FUND-REQUEST-APPROVAL":
                $docTitle = 'Official Document';
                break;

            default:
                $docTitle = 'Official Document';
                break;

        }
        return [
            'id' => 'ID',
            'institution_id' => 'Leaning Institution',
            'loan_item' => 'Loan Item',
            'semester_number' => 'Semester Number',
            'invoice_number' => 'Invoice Number',
            'control_number' => 'GEPG Control Number',
            'is_approved' => 'Is Approved',
            'academic_year' => 'Academic Year',
            'description' => 'Description',
            'submitted' => 'Submitted',
            'submitted_on' => 'Submitted On',
            'submitted_by' => 'Submitted By',
            'approval' => 'Approval',
            'approval_comments' => 'Approval Comments',
            'approval_date' => 'Approval Date',
            'approved_by' => 'Approved By',
            'request_status' => 'Request Status',
            'status_date' => 'Status Date',
            'status_by' => 'Status By',
            'request_amount' => 'Request Amount',

            //acceptance, acceptance_date, acceptance_by, acceptance_remarks, official_document
            'acceptance'=>'Request Process',
            'acceptance_date'=>'Process Date',
            'acceptance_by'=>'Processed By',
            'acceptance_remarks'=>'Remarks',
            'official_document'=>$docTitle,
            'file'=>$docTitle,

        ];
    }
    public function getRequest()
    {


            $output = $this->InstitutionCode.' - '.$this->LoanItemCode.'-'.$this->AcademicYear.'-'.$this->Semester;


        return $output;
    }
    public function getInstitution(){
        $output = '';
            $model = LearningInstitution::findOne($this->institution_id);
            if (sizeof($model)!=0){
                $output = $model->institution_name.' ('.$model->institution_code.')';
            }
        return $output;
    }

    public function getInstitutionCode(){
        $output = '';
        $model = LearningInstitution::findOne($this->institution_id);
        if (sizeof($model)!=0){
            $output = $model->institution_code;
        }
        return $output;
    }

    public function getAmount(){
        return Module::RequestAmount($this->id);
    }

    public function getStatus(){
        $output = '';
        //if ($this->is_approved)
        $output =Module::RequestStatus($this->request_status);

        return $output;
    }

    public function getLoanItem(){
        $output = '';
        $model = LoanItem::findOne($this->loan_item);
        if (sizeof($model)!=0){
            $output = $model->item_name.' ('.$model->item_code.')';
        }
        return $output;
    }
    public function getSemester(){
        $output = '';
        $model = Semester::findOne($this->semester_number);
        if (sizeof($model)!=0){
            $output = Module::THNumber($model->semester_number).' Semester';
        }
        return $output;
    }

    public function getLoanItemCode(){
        $output = '';
        $model = LoanItem::findOne($this->loan_item);
        if (sizeof($model)!=0){
            $output = $model->item_code;
        }
        return $output;
    }

    public function getAcademicYear(){
        $output = '';
        $model = AcademicYear::findOne($this->academic_year);
        if (sizeof($model)!=0){
            $output = $model->academic_year;
        }
        return $output;
    }


    public static function UserInfo($id,$field)
    {
        $output = '';
        $model = User::findOne($id);
        if (sizeof($model)!=0){

            switch ($field){
                case 'fullName':
                    $output = $model->firstname.' '.$model->surname;
                    break;

                default:
                    $output = $model->$field;
                    break;
            }

        }
        return $output;
    }


    public function getSubmittedBy(){
         return self::UserInfo($this->submitted_by,"fullName");
    }

    public function getApprovedBy(){
        return self::UserInfo($this->approved_by,"fullName");
    }

    public static function Status($index){
        $output = '';
        $array = Module::PaymentStatus($index);
        if (!is_array($array)){
            $output = $array;
        }

        return $output;
    }
    public function getBeneficiaries()
    {
        $output = 0;
        $request = $this->id;
        $SQL = "SELECT COUNT(DISTINCT institution_paylist.application_id) AS 'total' FROM institution_paylist WHERE institution_paylist.request_id = '$request';";
        $model = Yii::$app->db->createCommand($SQL)->queryAll();
        if (sizeof($model)!=0){
            $output=$model[0]['total'];
        }
        return $output;
    }

    public function getLearningInstitution(){
        return $this->hasOne(LearningInstitution::className(),['learning_institution_id'=>'institution_id']);
    }



}
