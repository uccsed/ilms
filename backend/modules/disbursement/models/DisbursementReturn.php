<?php

namespace backend\modules\disbursement\models;

use backend\modules\allocation\models\Application;
use backend\modules\allocation\models\LoanItem;
use backend\modules\application\models\Applicant;
use backend\modules\disbursement\Module;
use Yii;

/**
 * This is the model class for table "disbursement_return".
 *
 * @property integer $id
 * @property integer $return_batch_id
 * @property string $index_number
 * @property string $application_id
 * @property string $header_id
 * @property integer $disbursement_batch_id
 * @property string $sex
 * @property integer $year_of_study
 * @property integer $pay_sheet_serial_number
 * @property integer $loan_item
 * @property integer $instalment_number
 * @property double $amount
 * @property string $remarks
 * @property integer $academic_year
 * @property integer $status
 * @property double $match_percent
 * @property string $reasons
 * @property integer $created_by
 * @property integer $disbursement_id
 * @property string $created_on
 * @property string $sync_id
 */
class DisbursementReturn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disbursement_return';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['return_batch_id', 'index_number', 'application_id', 'header_id', 'disbursement_batch_id', 'sex', 'year_of_study', 'loan_item', 'instalment_number', 'amount', 'remarks', 'academic_year', 'created_by', 'sync_id'], 'required'],
            [['return_batch_id', 'disbursement_batch_id', 'year_of_study', 'pay_sheet_serial_number', 'loan_item', 'instalment_number', 'academic_year', 'status', 'created_by', 'disbursement_id'], 'integer'],
            [['amount', 'match_percent'], 'number'],
            [['remarks', 'reasons'], 'string'],
            [['created_on', 'disbursement_id'], 'safe'],
            [['index_number', 'sync_id'], 'string', 'max' => 100],
            [['application_id', 'header_id'], 'string', 'max' => 200],
            [['sex'], 'string', 'max' => 6],
            [['sync_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'return_batch_id' => 'Return Batch ID',
            'index_number' => 'Index Number',
            'application_id' => 'Application ID',
            'header_id' => 'Header ID',
            'disbursement_batch_id' => 'Disbursement Batch ID',
            'sex' => 'Sex',
            'year_of_study' => 'Year Of Study',
            'pay_sheet_serial_number' => 'Pay Sheet Serial Number',
            'loan_item' => 'Loan Item',
            'instalment_number' => 'Instalment Number',
            'amount' => 'Amount',
            'remarks' => 'Remarks',
            'academic_year' => 'Academic Year',
            'status' => 'Status',
            'match_percent' => 'Match Percent',
            'reasons' => 'Reasons',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'sync_id' => 'Sync ID',
            'disbursement_id' => 'Disbursement ID',
        ];
    }

    public function getStudent_name()
    {
        $output = '';
        //$output = Module::st
        return $output;
    }

    public function getApplication(){
        return $this->hasOne(Application::className(),['application_id'=>'application_id']);
    }

    public function getInstalment(){
        return $this->hasOne(InstalmentDefinition::className(),['instalment_definition_id'=>'instalment_number']);
    }

    public function getReturnBatch(){
        return $this->hasOne(DisbursementReturnBatch::className(),['id'=>'return_batch_id']);
    }

    public function getLoanItem(){
        return $this->hasOne(LoanItem::className(),['loan_item_id'=>'loan_item']);
    }
}
