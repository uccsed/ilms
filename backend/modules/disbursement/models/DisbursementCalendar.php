<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\allocation\models\LearningInstitution;
use backend\modules\allocation\models\Programme;
use frontend\modules\repayment\models\User;
use backend\modules\disbursement\Module;
/**
 * This is the model class for table "disbursement_calendar".
 *
 * @property integer $id
 * @property string $event_name
 * @property string $event_description
 * @property string $start_date
 * @property string $end_date
 * @property integer $institution_id
 * @property integer $programme_id
 * @property integer $event_priority
 * @property integer $pre_remainder_days
 * @property string $targeted_personnels
 * @property integer $created_by
 * @property string $created_on
 * @property integer $updated_by
 * @property string $updated_on
 */
class DisbursementCalendar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disbursement_calendar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_name', 'start_date', 'end_date', 'institution_id'], 'required'],
            [['event_description'], 'string'],
            [['start_date', 'end_date', 'created_on', 'updated_on'], 'safe'],
            [['institution_id', 'programme_id', 'event_priority', 'pre_remainder_days', 'created_by', 'updated_by'], 'integer'],
            [['event_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_name' => 'Event Name',
            'event_description' => 'Event Description',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'institution_id' => 'Institution ID',
            'programme_id' => 'Programme ID',
            'event_priority' => 'Event Priority',
            'pre_remainder_days' => 'Pre Remainder Days',
            'targeted_personnels' => 'Targeted Personnels',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
        ];
    }

    public function search($params)
    {
        $startYear = date('Y').'-01-01';
        $endYear = date('Y').'-12-31';
        $query = DisbursementCalendar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        /* if (!$this->validate()) {
             // uncomment the following line if you do not want to return any records when validation fails
             // $query->where('0=1');
             return $dataProvider;
         }*/

        // grid filtering conditions
        $query->andFilterWhere(['between', 'start_date', "$startYear", "$endYear",]);
        $query->orFilterWhere(['between', 'end_date', "$startYear", "$endYear",]);

        $query->andFilterWhere([
            'id' => $this->id,
            'event_name' => $this->event_name,
            'event_description' => $this->event_description,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'institution_id' => $this->institution_id,
            'programme_id' => $this->programme_id,
            'event_priority' => $this->event_priority,
            'pre_remainder_days' => $this->pre_remainder_days,
            'targeted_personnels' => $this->targeted_personnels,
            'created_by' => $this->created_by,

            'created_on' => $this->created_on,
            'updated_by' => $this->updated_by,
            'updated_on' => $this->updated_on,

        ]);

        return $dataProvider;
    }


    public function getStartDate(){
        $output = '-';
        if (!empty($this->start_date)&&$this->start_date!='0000-00-00'&&$this->start_date!=''){
            $output = date('l d/m/Y',strtotime($this->start_date));
        }
        return $output;
    }


    public function getEndDate(){
        $output = '-';
        if (!empty($this->end_date)&&$this->end_date!='0000-00-00'&&$this->end_date!=''){
            $output = date('l d/m/Y',strtotime($this->end_date));
        }
        return $output;
    }

    public function getPriority(){
        $output = '-';
        if (!empty($this->event_priority)&&$this->event_priority!=''&&$this->event_priority!='0'){
            $output = '('.Module::PriorityList($this->event_priority,'name').') '.Module::PriorityList($this->event_priority,'star');
        }
        return $output;
    }

    public function getInstitution(){
        $output = '';
        $model = LearningInstitution::findOne($this->institution_id);
        if (sizeof($model)!=0){
            $output = $model->institution_name.' ('.$model->institution_code.')';
        }

        return $output;
    }


    public function getProgramme(){
        $output = '';
        $model = Programme::findOne($this->programme_id);
        if (sizeof($model)!=0){
            $output = $model->programme_name.' ('.$model->programme_code.')';
        }

        return $output;
    }


    public function getAssignedTo(){
        $output = 'None';
        if (!empty($this->targeted_personnels)&&$this->targeted_personnels!=''){
            $cleanList = str_replace('"',"'",str_replace(array('[',']'),'',$this->targeted_personnels));
            if (!empty($cleanList)&&$cleanList!=''){
                $SQL = "SELECT CONCAT(user.firstname,' ',user.surname) AS 'Name' FROM user WHERE  user_id IN ($cleanList)";
                $usersModel = Yii::$app->db->createCommand($SQL)->queryAll();
                if (sizeof($usersModel)!=0){
                    $data =  array();
                    foreach ($usersModel as $index=>$dataArray){
                        $data[]=$dataArray['Name'];
                    }
                    $output = implode(',',$data);
                }
            }
        }

        return $output;
    }
}
