<?php

namespace backend\modules\disbursement\models;

use Yii;

use yii\data\ActiveDataProvider;
use backend\modules\allocation\models\LearningInstitution;
use backend\modules\allocation\models\Programme;
use backend\modules\allocation\models\LoanItem;
use backend\modules\disbursement\models\Instalment;
use backend\modules\allocation\models\AcademicYear;
use common\models\Semester;
use backend\modules\disbursement\Module;

use frontend\modules\repayment\models\User;
/**
 * This is the model class for table "disbursement_default_plan".
 *
 * @property integer $plan_id
 * @property integer $semester_number
 * @property integer $academic_year
 * @property integer $instalment_id
 * @property integer $loan_item_id

 * @property double $disbursement_percent

 * @property string $sync_id
 * @property integer $created_by
 * @property string $created_on
 */
class DisbursementDefaultPlan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disbursement_default_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['semester_number', 'loan_item_id', 'academic_year', 'instalment_id', 'sync_id'], 'required'],
            [['semester_number', 'loan_item_id', 'instalment_number', 'created_by'], 'integer'],
            [['disbursement_percent'], 'number'],
            [['created_on'], 'safe'],
            [['sync_id'], 'string', 'max' => 30],
            [['sync_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => 'Plan ID',
            'semester_number' => 'Semester Number',
            'loan_item_id' => 'Loan Item',
            'academic_year' => 'Academic Year',
            'disbursement_percent' => 'Disbursement Percent',
            'instalment_id' => 'Instalment Number',
            'sync_id' => 'Sync ID',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
        ];
    }

    public function search($params)
    {
        $query = DisbursementDefaultPlan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        /* if (!$this->validate()) {
             // uncomment the following line if you do not want to return any records when validation fails
             // $query->where('0=1');
             return $dataProvider;
         }*/



        // grid filtering conditions
        $query->andFilterWhere([
            'plan_id' => $this->plan_id,
            'semester_number' => $this->semester_number,
            'loan_item_id' => $this->loan_item_id,
            'instalment_id' => $this->instalment_id,
            'academic_year' => $this->academic_year,
            'disbursement_percent' => $this->disbursement_percent,
            'sync_id' => $this->sync_id,
            'created_by' => $this->created_by,


        ]);

        return $dataProvider;
    }




    public function getLoanItem(){
        $output = '';
        $model = LoanItem::findOne($this->loan_item_id);
        if (sizeof($model)!=0){
            $output = $model->item_name.' ('.$model->item_code.')';
        }
        return $output;
    }

    public function getAcademicYear(){
        $output = '';
        $model = AcademicYear::findOne($this->academic_year);
        if (sizeof($model)!=0){
            $output = $model->academic_year;
        }
        return $output;
    }

    public function getInstalment(){
        $output = '';
        $model = Instalment::findOne($this->instalment_id);
        if (sizeof($model)!=0){
            $output = Module::THNumber($model->instalment).' Instalment';
        }
        return $output;
    }

    public function getSemester(){
        $output = '';
        $model = Semester::findOne($this->semester_number);
        if (sizeof($model)!=0){
            $output = Module::THNumber($model->semester_number).' Semester';
        }
        return $output;
    }
}
