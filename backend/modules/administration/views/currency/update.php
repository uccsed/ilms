<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\Currency */

$this->title = 'Update Currency: ' . $model->currency_desc;
$this->params['breadcrumbs'][] = ['label' => 'Currencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->currency_id, 'url' => ['view', 'id' => $model->currency_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="currency-update">
    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
