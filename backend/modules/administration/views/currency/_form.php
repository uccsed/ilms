<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\Currency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="currency-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'currency_ref')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency_postfix')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency_desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ex_rate')->textInput() ?>

    <?= $form->field($model, 'lowest_coin')->textInput() ?>

<div style="display: none;">
    <?php
    if (strtoupper(Yii::$app->controller->action->id) === strtoupper('create')){  ?>
        <?= $form->field($model, 'created_at')->textInput(['value'=>date('Y-m-d H:i:s')]) ?>

        <?= $form->field($model, 'created_by')->textInput(['value'=>Yii::$app->user->id]) ?>
    <?php } ?>

    <?= $form->field($model, 'updated_at')->textInput(['value'=>date('Y-m-d H:i:s')]) ?>

    <?= $form->field($model, 'updated_by')->textInput(['value'=>Yii::$app->user->id]) ?>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'], ['class' =>'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
