<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\Currency */

$this->title = $model->currency_desc.'('.$model->currency_ref.')';
$this->params['breadcrumbs'][] = ['label' => 'Currencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-view">




<div class="panel">
    <div class="panel-heading">
        <h3><?= Html::encode($this->title) ?></h3>
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->currency_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->currency_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>
    <div class="panel-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'currency_id',
                'currency_ref',
                'currency_postfix',
                'currency_desc',
                'ex_rate',
                'lowest_coin',
                'created_at',
                //'created_by',
                'updated_at',
                //'updated_by',
            ],
        ]) ?>
    </div>
</div>


</div>
