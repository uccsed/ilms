<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\Region */

$this->title = $model->region_name;
$this->params['breadcrumbs'][] = ['label' => 'Regions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">
    <div class="panel">
        <div class="panel-heading">
            <h3><?= Html::encode($this->title) ?>
                <span class="pull-right">
                    <?= Html::a('Update', ['update', 'id' => $model->region_id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->region_id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
            </span>
            </h3>

        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'region_id',
                    'region_name',
                   // 'description:ntext',
                    //'is_active',
                    [
                        'name'=>'zone_id',
                        'value'=>$model->Zone,
                        'format'=>'raw',
                        'label'=>'Zone',
                    ],
                ],
            ]) ?>


            <h5 class="text-bold text-uppercase">Districts</h5>
            <?php echo Yii::$app->controller->renderPartial('nested_district_list',array('regionID'=>$model->region_id)); ?>
        </div>

    </div>


</div>
