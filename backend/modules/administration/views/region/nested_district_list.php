<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\administration\models\DistrictSearch;




$searchDistrictModel = new DistrictSearch();
$dataDistrictProvider = $searchDistrictModel->searchPerRegion(Yii::$app->request->queryParams,$regionID);
$dataDistrictProvider->pagination=false;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administration\models\DistrictSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */




            $gridDistrictColumns = [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'hAlign' => GridView::ALIGN_CENTER,
                ],
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'allowBatchToggle' => true,
                    'detail' => function ($model) {
                        return $this->render('../ward/shared_list',['districtID'=>$model->district_id]);
                    },
                    'detailOptions' => [
                        'class' => 'kv-state-enable',
                    ],
                ],

                [
                    'attribute' => 'district_name',
                    'label'=>"District",
                    'format' => 'raw',

                    'value' => function ($model) {
                        return $model->district_name;
                    },

                    // 'group'=>true,  // enable grouping,
                ],

                [
                    'attribute' => 'region_id',
                    'label'=>"Region",
                    'format' => 'raw',

                    'value' => function ($model) {
                        return $model->region->region_name;
                    },

                    // 'group'=>true,  // enable grouping,
                ],



                ['class' => 'kartik\grid\ActionColumn',
                     'template'=>'{view}',
                    'buttons' => [

                    'view' => function ($url,$model,$key) {
                $url  =Yii::$app->urlManager->createUrl(['/administration/district/view','id'=>$key]);
                          return Html::a('<span class="fa fa-eye"></span>', $url);
                  },

               ],
                ],
            ];
            ?>

            <?php
            /*echo*/ ExportMenu::widget([
                'dataProvider' => $dataDistrictProvider,
                'columns' => $gridDistrictColumns,

                'fontAwesome' => true,
//            'asDropdown' => false
                'batchSize' => 50,
                'target' => '_blank',
                'selectedColumns' => [0, 1, 2, 3, 4, 5, 6, 7], // Col seq 2 to 6
                'columnSelectorOptions' => [
                    'label' => 'Export Columns',
                ],
                // 'hiddenColumns' => [15], // SerialColumn, Color, & ActionColumn
                //'disabledColumns' => [0, 1, 2, 3, 4, 5, 6, 9, 12], // ID & Name
                'noExportColumns' => [15],
                'dropdownOptions' => [
                    'label' => 'Export Data',
                    'class' => 'btn btn-default'
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_EXCEL => false,
                    ExportMenu::FORMAT_EXCEL_X => false,
                ],
                //'folder' => '@webroot/tmp', // this is default save folder on server
            ]) . "<hr>\n";
            ?>

            <?php
            echo GridView::widget([
                'dataProvider' => $dataDistrictProvider,
                'filterModel' => $searchDistrictModel,
                'columns' => $gridDistrictColumns,
                //'showPageSummary'=>true,
                //'pageSummaryRowOptions'=>['class'=>'text-bold bg-blue'],
                'pjax'=>true,
                'striped'=>true,
                'hover'=>true,
            ]);

            ?>


