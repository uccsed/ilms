<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\Region */

$this->title = 'Create Region';
$this->params['breadcrumbs'][] = ['label' => 'Regions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-create">
    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
