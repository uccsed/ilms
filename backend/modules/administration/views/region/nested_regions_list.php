<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 12/28/18
 * Time: 3:16 PM
 */
?>
<?php
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\administration\models\RegionSearch;

use backend\modules\administration\models\ZoneSearch;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administration\models\ZoneSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$searchZoneModel = new RegionSearch();
$dataZoneProvider = $searchZoneModel->searchPerZone(Yii::$app->request->queryParams,$zoneID);
$dataZoneProvider->pagination=false;

?>

            <?php
            $gridZoneColumns = [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'hAlign' => GridView::ALIGN_CENTER,
                ],
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'allowBatchToggle' => true,
                    'detail' => function ($model) {
                        //return $this->render('../district/shared_list',['regionID'=>$model->region_id]);
                        return $this->render('nested_district_list',['regionID'=>$model->region_id]);
                    },
                    'detailOptions' => [
                        'class' => 'kv-state-enable',
                    ],
                ],

                [
                    'attribute' => 'region_name',
                    'label'=>"Zone",
                    'format' => 'raw',

                    'value' => function ($model) {
                        return $model->region_name;
                    },

                    // 'group'=>true,  // enable grouping,
                ],
                [
                    'attribute' => 'zone_id',
                    'label'=>"Zone",
                    'format' => 'raw',

                    'value' => function ($model) {
                        return $model->Zone;
                    },

                    // 'group'=>true,  // enable grouping,
                ],


                ['class' => 'kartik\grid\ActionColumn',
                     'template'=>'{view}',
                    'buttons' => [

                        'view' => function ($url,$model,$key) {
                            $url  =Yii::$app->urlManager->createUrl(['/administration/region/view','id'=>$key]);
                            return Html::a('<span class="fa fa-eye"></span>', $url);
                        },

                    ],
                ],
            ];
            ?>

            <?php
            /*echo*/ ExportMenu::widget([
                'dataProvider' => $dataZoneProvider,
                'columns' => $gridZoneColumns,

                'fontAwesome' => true,
//            'asDropdown' => false
                'batchSize' => 50,
                'target' => '_blank',
                'selectedColumns' => [0, 1, 2, 3, 4, 5, 6, 7], // Col seq 2 to 6
                'columnSelectorOptions' => [
                    'label' => 'Export Columns',
                ],
                // 'hiddenColumns' => [15], // SerialColumn, Color, & ActionColumn
                //'disabledColumns' => [0, 1, 2, 3, 4, 5, 6, 9, 12], // ID & Name
                'noExportColumns' => [15],
                'dropdownOptions' => [
                    'label' => 'Export Data',
                    'class' => 'btn btn-default'
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_EXCEL => false,
                    ExportMenu::FORMAT_EXCEL_X => false,
                ],
                //'folder' => '@webroot/tmp', // this is default save folder on server
            ]) . "<hr>\n";
            ?>

            <?php
            echo GridView::widget([
                'dataProvider' => $dataZoneProvider,
                'filterModel' => $searchZoneModel,
                'columns' => $gridZoneColumns,
                //'showPageSummary'=>true,
                //'pageSummaryRowOptions'=>['class'=>'text-bold bg-blue'],
                'pjax'=>true,
                'striped'=>true,
                'hover'=>true,
            ]);

            ?>



