<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\Occupation */

$this->title = $model->occupation_desc;
$this->params['breadcrumbs'][] = ['label' => 'Occupations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="occupation-view">

    <div class="panel">
        <div class="panel-heading">
            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->occupation_id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->occupation_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('Create Occupation', ['create'], ['class' => 'btn btn-success pull-right']) ?>
            </p>
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                   // 'occupation_id',
                    [
                            'name'=>'occupation_category_id',
                            'value'=>strtoupper($model->occupationCategory->category_desc),
                            'format'=>'html',
                            'label'=>'Category',
                    ],
                    //'occupation_category_id',
                   // 'occupation_desc',
                    [
                        'name'=>'occupation_desc',
                        'value'=>$model->occupation_desc,
                        'format'=>'html',
                        'label'=>'Occupation',
                    ],
                ],
            ]) ?>
        </div>
    </div>

</div>
