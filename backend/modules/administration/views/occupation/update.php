<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\Occupation */

$this->title = 'Update Occupation: ' . $model->occupation_desc;
$this->params['breadcrumbs'][] = ['label' => 'Occupations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->occupation_id, 'url' => ['view', 'id' => $model->occupation_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="occupation-update">

    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
