<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administration\models\OccupationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Occupations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="occupation-index">

    <p>
        <?= Html::a('Create Occupation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <div class="panel">
        <div class="panel-heading"><h4><?= Html::encode($this->title) ?></h4></div>
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],

                  //  'occupation_id',
                    //'occupation_category_id',
                    'occupation_desc',
                    //'occupation_category_id',
                    [
                        'attribute' => 'occupation_category_id',
                        'label'=>"Category",
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->occupationCategory->category_desc;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' =>ArrayHelper::map(\backend\modules\administration\models\OccupationCategory::find()->asArray()->all(), 'occupation_category_id', 'category_desc'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Search  '],
                        'format' => 'raw'
                    ],

                    ['class' => 'kartik\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
