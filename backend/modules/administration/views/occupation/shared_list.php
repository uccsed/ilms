<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 12/28/18
 * Time: 1:28 PM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/6/18
 * Time: 9:19 AM
 */
use backend\modules\administration\models\OccupationSearch;
use kartik\grid\GridView;



$searchModel = new OccupationSearch();
$dataProvider = $searchModel->searchPerCategory(Yii::$app->request->queryParams,$categoryID);
$dataProvider->pagination=false;
?>

<?php
$columns = [
    ['class'=>'kartik\grid\SerialColumn'],

    [
        'attribute'=>'occupation_desc',
        //'width'=>'250px',
        'label'=>"Occupation",
        'filter'=>false,

    ],

];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    //'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'columns' => $columns,
    //'panel'=>['class'=>'primary']
]); ?>

