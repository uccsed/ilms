<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\Ward */

$this->title = $model->ward_name;
$this->params['breadcrumbs'][] = ['label' => 'Wards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ward-view">
    <div class="panel">
        <div class="panel-heading">
            <h3><?= Html::encode($this->title) ?>
                <span class="pull-right">
                    <?= Html::a('Update', ['update', 'id' => $model->ward_id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->ward_id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
            </span>
            </h3>

        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'ward_id',
                    'ward_name',
                    // 'description:ntext',
                    //'is_active',
                    [
                        'name'=>'district_id',
                        'value'=>$model->district->district_name,
                        'format'=>'raw',
                        'label'=>'District',
                    ],
                ],
            ]) ?>

        </div>

    </div>
</div>
