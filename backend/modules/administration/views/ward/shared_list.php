<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 12/28/18
 * Time: 1:28 PM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/6/18
 * Time: 9:19 AM
 */
use backend\modules\administration\models\WardSearch;
use kartik\grid\GridView;
use yii\helpers\Html;



$searchModel = new WardSearch();
$dataProvider = $searchModel->searchPerDistrict(Yii::$app->request->queryParams,$districtID);
$dataProvider->pagination=false;
?>

<?php
$columns = [
    ['class'=>'kartik\grid\SerialColumn'],

    [
        'attribute'=>'ward_name',
        //'width'=>'250px',
        'label'=>"Ward",
        'filter'=>false,

    ],

    ['class' => 'kartik\grid\ActionColumn',
         'template'=>'{view}',
        'buttons' => [

            'view' => function ($url,$model,$key) {
                $url  =Yii::$app->urlManager->createUrl(['/administration/ward/view','id'=>$key]);
                return Html::a('<span class="fa fa-eye"></span>', $url);
            },

        ],
    ],

];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    //'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'columns' => $columns,
    //'panel'=>['class'=>'primary']
]); ?>

