<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 12/28/18
 * Time: 1:28 PM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 11/6/18
 * Time: 9:19 AM
 */
use backend\modules\administration\models\DistrictSearch;
use kartik\grid\GridView;



$searchModel = new DistrictSearch();
$dataProvider = $searchModel->searchPerRegion(Yii::$app->request->queryParams,$regionID);
$dataProvider->pagination=false;
?>

<?php
$columns = [
    ['class'=>'kartik\grid\SerialColumn'],

    [
        'attribute'=>'district_name',
        //'width'=>'250px',
        'label'=>"District",
        'filter'=>false,

    ],

];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    //'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'columns' => $columns,
    //'panel'=>['class'=>'primary']
]); ?>

