<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\District */

$this->title = 'Update District: ' . $model->district_name;
$this->params['breadcrumbs'][] = ['label' => 'Districts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->district_id, 'url' => ['view', 'id' => $model->district_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="district-update">
    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
