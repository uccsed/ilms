<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\District */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="district-form">




    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL,
    ]); ?>

    <?php
    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' =>1,
        'attributes' => [

            'district_name'=>['label'=>'District', 'options'=>['placeholder'=>'District name']],

            'region_id' => ['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'Zone',
                'options' => [
                    'data' => ArrayHelper::map(\backend\modules\administration\models\Region::find()->all(), 'region_id', 'region_name'),
                    'options' => [
                        'prompt' => 'Select',

                    ],
                ],
            ],




            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
        ]
    ]);
    ?>

    <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?php
        echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
        echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

        ActiveForm::end();
        ?>
    </div>

</div>
