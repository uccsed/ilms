<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\District */

$this->title = $model->district_name;
$this->params['breadcrumbs'][] = ['label' => 'Districts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="district-view">

    <div class="panel">
        <div class="panel-heading">
            <h3><?= Html::encode($this->title) ?>
                <span class="pull-right">
                    <?= Html::a('Update', ['update', 'id' => $model->district_id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->district_id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
            </span>
            </h3>

        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'district_id',
                    'district_name',
                    // 'description:ntext',
                    //'is_active',
                    [
                        'name'=>'region_id',
                        'value'=>$model->region->region_name,
                        'format'=>'raw',
                        'label'=>'Region',
                    ],
                ],
            ]) ?>


            <h5 class="text-bold text-uppercase">Wards</h5>
            <?php echo Yii::$app->controller->renderPartial('../ward/shared_list',array('districtID'=>$model->district_id)); ?>
        </div>

    </div>

</div>
