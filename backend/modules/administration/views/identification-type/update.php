<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\IdentificationType */

$this->title = 'Update Identification Type: ' . $model->identification_type;
$this->params['breadcrumbs'][] = ['label' => 'Identification Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->identification_type_id, 'url' => ['view', 'id' => $model->identification_type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="identification-type-update">
    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
