<?php

use yii\helpers\Html;

/* @var $this  yii\web\View */
/* @var $model mdm\admin\models\BizRule */

$this->title = Yii::t('rbac-admin', 'Create Rule');
$this->params['breadcrumbs'][] = ['label' => 'Security Settings'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Rules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-create">
<div class="panel">
    <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div>
    <div class="panel-body">
        <?=
        $this->render('_form', [
            'model' => $model,
        ]);
        ?>
    </div>
</div>
</div>
