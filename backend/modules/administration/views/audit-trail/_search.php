<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 3/15/19
 * Time: 7:25 AM
 */
use kartik\widgets\Select2;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use kartik\date\DatePicker;
use kartik\time\TimePicker;

use backend\modules\disbursement\Module;
use backend\modules\administration\models\UserCategory;
use common\models\User;
use backend\modules\administration\ADMINISTRATION;
use yii\helpers\Url;
$searchFields=array();
$searchFields['data_before']='Data Before';
$searchFields['data_after']='Data After';
$searchFields['table_name']='Table Name';
$searchFields['action']='Action';
$searchFields['record_id']='Record ID';
$searchFields['trigger_url']='Trigger URL';
$searchFields['comment']='Comments';

echo Module::FetchBootstrap('js');
?>

<?php $form = ActiveForm::begin([
    'options'=>['enctype'=>'multipart/form-data'], // important
    'type' => ActiveForm::TYPE_VERTICAL,
]);
?>


<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <?php
               echo '<label>Start Date</label>';
                echo DatePicker::widget([
                //'model' => $model,
                'id' => 'start_date',
                'name' => 'start_date',
                'value' => date('Y-m-d'),
                'options' => ['placeholder' => 'Select start date ...'],
                'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_APPEND,
                'convertFormat' => true,
                'pluginOptions' => [
                'format' => 'yyyy-MM-dd',

                'todayHighlight' => true,
                    'autoclose' => true,
                ]
                ]);

                ?>
            </div>

            <div class="col-md-6">
                <?php
                echo '<label>Time</label>';


                echo TimePicker::widget([
                    'id' => 'start_time',
                    'name' => 'start_time',
                    'pluginOptions' => [
                        'showSeconds' => true,
                        'showMeridian' => false,
                        'minuteStep' => 1,
                        'secondStep' => 5,
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <?php
                echo '<label>Start Date</label>';
                echo DatePicker::widget([
                    //'model' => $model,
                    'id' => 'end_date',
                    'name' => 'end_date',
                    'value' => date('Y-m-d'),
                    'options' => ['placeholder' => 'Select end date ...'],
                    'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_APPEND,
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'format' => 'yyyy-MM-dd',

                        'todayHighlight' => true,
                        'autoclose' => true,
                    ]
                ]);

                ?>
            </div>

            <div class="col-md-6">
                <?php
                echo '<label>Time</label>';


                echo TimePicker::widget([
                    'id' => 'end_time',
                    'name' => 'end_time',
                    'pluginOptions' => [
                        'showSeconds' => true,
                        'showMeridian' => false,
                        'minuteStep' => 1,
                        'secondStep' => 5,
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
<hr />
<div class="row">

   <div class="col-md-3">
            <div class="form-group">
                <div class="col-lg-12">
                    <?php


                    echo Select2::widget([
                        'id' => 'action',
                        'name' => 'action',
                        'value' => '',
                        'data' => ['CREATE'=>'CREATE','UPDATE'=>'UPDATE','DELETE'=>'DELETE'],
                        'options' => ['placeholder' => 'Select Manipulation Action']
                    ]);
                    ?>
                </div>
            </div>
        </div>



    <div class="col-md-3">
        <div class="form-group">
            <div class="col-lg-12">
                <?php
               /* $USER_CATEGORY=UserCategory::find()->asArray()->all();
                $categoryListData=ArrayHelper::map($USER_CATEGORY,'id','name');
                $output = ;*/
                echo Select2::widget([
                    'id' => 'table',
                    'name' => 'table',
                    'value' => '',
                    'data' => ADMINISTRATION::TABLES_LIST(),
                    'options' => ['placeholder' => 'Select Table']
                ]);
                ?>
            </div>
        </div>
    </div>



    <div class="col-md-3">
        <div class="form-group">
            <div class="col-lg-12">
                <?php
                $USER_CATEGORY=UserCategory::find()->asArray()->all();
                $categoryListData=ArrayHelper::map($USER_CATEGORY,'id','name');

                echo Select2::widget([
                    'id' => 'login_type',
                    'name' => 'login_type',
                    'value' => '',
                    'data' => $categoryListData,
                    'options' => ['placeholder' => 'Select User Category']
                ]);
                ?>
            </div>
        </div>
    </div>



    <div class="col-md-3">
        <div class="form-group">
            <div class="col-lg-12">
                <?php
                $USER_CATEGORY=UserCategory::find()->asArray()->all();
                $categoryListData=ArrayHelper::map($USER_CATEGORY,'id','name');

                echo Select2::widget([
                    'id' => 'login_type',
                    'name' => 'login_type',
                    'value' => '',
                    'data' => $categoryListData,
                    'options' => ['placeholder' => 'Select User Category']
                ]);
                ?>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <div class="col-lg-12">
                <?php
                $userSQL = "SELECT user_id AS 'id', CONCAT(IFNULL(firstname,''),' ', IFNULL(middlename,''),' ',IFNULL(surname,'')) AS 'name' FROM user LIMIT 0,1;";
                $USER=User::findBySql($userSQL)->asArray()->all();
                $userListData=ArrayHelper::map($USER,'id','name');
                /*echo Select2::widget([
                    'id' => 'user_id',
                    'name' => 'user_id',
                    'value' => '',
                    'data' => $userListData,
                    'options' => ['placeholder' => 'Select a Specific User']
                ]);*/


                echo DepDrop::widget([
                    'type'=>DepDrop::TYPE_SELECT2,
                    'id' => 'user_id',
                    'name' => 'user_id',
                    'options'=>['id'=>'user_id'],
                    //'onchange'=>'updateStatus()',
                    'pluginOptions'=>[
                        'depends'=>['login_type'],
                        'placeholder'=>'Select a Specific User',
                        'url'=>Url::to(['/administration/audit-trail/user-filter'])
                    ]
                ]);

                ?>

            </div>
        </div>
    </div>
    <hr />
    <div class="col-md-12">

        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon"><span class="fa fa-search"></span></div>
                <?= Html::textInput('search','',['id'=>'search','class'=>'form-control bordered border-danger','placeholder'=>'Search Within audit trail ','style'=>'font-size:24px; height:50px;']); ?>
                <input type="hidden" id="index_number" name="index_number" class="form-control" />
                <div class="input-group-addon"><span id="search_feedback"><span class="fa fa-spinner fa-2x"></span></span></div>
                <div class="input-group-addon">
                    <button type="button" id="searchButton" class="btn bg-red"><span class="fa fa-search"></span> Search</button></div>
            </div>
        </div>
        <!--<div class="input-group content-group">


            <div class="has-feedback has-feedback-left">

                <input type="text" class="form-control"  id="remoteSearch" name="remoteSearch" placeholder="Search Within audit trail">
                <div class="form-control-feedback">
                    <i class="icon-search4 text-muted text-size-base"></i>
                </div>
            </div>

            <div class="input-group-btn">
                <button type="submit" id="remoteSubmit" name="remoteSubmit"  class="btn btn-primary btn-icon block-page"><i class="icon icon-search4"></i> Search</button>
                <button id="refresh" name="refresh" class="btn btn-icon btn-success" type="button"  onclick="FormCleaner()"><i class="icon icon-sync"></i> Clear</button>

            </div>

        </div>-->
    </div>
</div>




    <script>
        function commaSeparateNumber(val) {
            while (/(\d+)(\d{3})/.test(val.toString())) {
                val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            }
            return val;
        }

        function Search(){
            var spinner = '<span class="fa fa-spinner fa-2x fa-spin"></span>';
            $('#audit_trail_results').val('');
            $("#search_feedback").html(spinner);
            $("#audit_trail_results").html(spinner+' Please Wait ... Loading');
            $.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl('/administration/audit-trail/search'); ?>",
                type:"POST",
                cache:false,
                data:{
                    csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                    search:$('#search').val(),

                    table:$('#table').val(),
                    login_type:$('#login_type').val(),
                    user_id:$('#user_id').val(),
                    start_date:$('#start_date').val() + ' ' + $('#start_time').val(),
                    end_date:$('#end_date').val() + ' ' + $('#end_time').val(),
                    action:$('#action').val(),

                },
                success:function (data) {

                    $("#audit_trail_results").html('');
                    var dataArray = data;
                    var results = dataArray.output.output;
                    var count = dataArray.output.count;

                    $("#audit_trail_results").append('<h4>'+commaSeparateNumber(count)+' Records Found</h4>');
                    $("#audit_trail_results").append(results);


                }
            });
        }

        $(document).ready(function () {


            $("#searchButton").on('click',function () {
                Search();
            });


        });

    </script>



<?php  ActiveForm::end();   ?>