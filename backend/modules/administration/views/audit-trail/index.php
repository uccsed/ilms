<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 3/15/19
 * Time: 7:24 AM
 */
?>
<?php

use yii\helpers\Html;
use yii\grid\GridView;


$this->title = 'AUDIT TRAIL RECORDS';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="audit-trail-index">
    <div class="panel">
        <div class="panel-heading"><h4 class="text-bold"><?= Html::encode($this->title) ?></h4></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12"><h4>SEARCH <span class="pull-right"><?= Html::a('Analytics', ['analytics'], ['class' => 'btn btn-primary']) ?></span></h4></div>
                <div class="col-md-12">
                    <?php echo Yii::$app->controller->renderPartial('_search'); ?>
                </div>
            </div>
        </div>
    </div>


    <div class="panel">
        <div class="panel-heading"><h4>AUDIT TRAIL SEARCH RESULTS</h4></div>
       <!-- <div class="panel-body" id="audit_trail_results">-->
        <div id="audit_trail_results">

        </div>
    </div>

</div>
