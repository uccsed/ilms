<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 3/15/19
 * Time: 2:11 PM
 */
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\disbursement\Module;
use backend\modules\administration\ADMINISTRATION;


$this->title = 'Detailed Audit trail for '.$model->action.' action on '.$model->table_name.' table on record ID '.$model->primary_id_value;
$this->params['breadcrumbs'][] = ['label' => 'Audit Trail', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="country-view">
    <div class="panel">
        <div class="panel-heading">
            <h3><?= Html::encode($this->title) ?></h3>
            <p>
                <?= Html::a('Back to search', ['index'], ['class' => 'btn btn-success']) ?>

            </p>

        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'country_id',
                    'logs_id',
                    'action',
                    'http_user_agent',
                    [
                        'name'=>'remote_address',
                        'value'=>$model->remote_address,
                        'format'=>'raw',
                        'label'=>'Trigger Url',
                    ],
                    [
                        'name'=>'created_by',
                        'value'=>Module::UserInfo($model->created_by,'fullName'),
                        'format'=>'raw',
                        'label'=>'Executed by',
                    ],
                    [
                        'name'=>'created_at',
                        'value'=>date('d/m/Y H:i:s',strtotime($model->created_at)),
                        'format'=>'raw',
                        'label'=>'Action Date',
                    ]
                ],
            ]); ?>

            <?php echo ADMINISTRATION::AUDIT_TRAIL_DETAILED($model->logs_id);  ?>

        </div>
    </div>

</div>


<!--
primary_id_value
column_name
old_data
new_data

remote_address
table_name
action
created_by
updated_by
created_at
updated_at
status-->




