<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 12/21/18
 * Time: 3:06 PM
 */
use backend\modules\administration\ADMINISTRATION;
use kartik\widgets\Select2;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use kartik\date\DatePicker;
use kartik\time\TimePicker;

use backend\modules\disbursement\Module;
use backend\modules\administration\models\UserCategory;
use common\models\User;

use yii\helpers\Url;
//echo Module::FetchBootstrap('js');


?>

<!--<pre>
    <?php
/*    //print_r(ADMINISTRATION::MONTH_OF_A_YEAR('short'));
    $month = 1;
    $year =  2019;
    $format = 'number';
    $option = null;
    $option = 17;
    print_r(ADMINISTRATION::DAYS_OF_A_MONTH($month,$year,$format,$option));
    */?>
</pre>-->
<!--<div class="box-body">
    <div class="chart">
        <canvas id="areaChart" style="height:420px;"></canvas>
    </div>
</div>-->



<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body chart-responsive">
        <div class="chart" id="bar-chart" style="height: 350px;"></div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->



<!-- solid sales graph -->
<div class="box box-solid">


    <div class="box-footer no-border">
        <div class="row">

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-floppy-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">NEW RECORDS</span>
                    <span class="info-box-number text-green" id="create" style="font-size: 50px;">0</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-pencil-square-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text" >CHANGED RECORDS</span>
                    <span class="info-box-number text-yellow" id="update" style="font-size: 50px;">0</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-trash-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">DELETED RECORDS</span>
                    <span class="info-box-number text-red" id="delete" style="font-size: 50px;">0</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </div>
    <!-- /.box-footer -->
</div>
<!-- /.box -->










<!-- jQuery 2.2.3 -->
<script src="../ExtraPlugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- Morris.js charts -->
<script src="../ExtraPlugins/morris/raphael-min.js"></script>
<script src="../ExtraPlugins/morris/morris.min.js"></script>
<!-- FastClick -->
<script src="../ExtraPlugins/fastclick/fastclick.js"></script>
<!-- page script -->







<script>

    $('body').addClass('sidebar-collapse sidebar-mini');

    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }

    function ChartArea(divID,areaChartData) {

        //<canvas id="areaChart" style="height:420px;"></canvas>

        $("#chart").html("");
        $("#chart").append('<canvas id="'+divID+'" style="height:420px;"></canvas>');
        $("#"+divID).html("");

        // Get context with jQuery - using jQuery's .get() method.
        var areaChartCanvas = $("#"+divID).get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var areaChart = new Chart(areaChartCanvas);

        var areaChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 3,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            //datasetFill: true,
            datasetFill: false,
            //String - A legend template
            legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true
        };

        //Create the line chart
        areaChart.Line(areaChartData, areaChartOptions);
    }

    function GenerateChart(divID) {
        var mini_spinner = '<div><span class="fa fa-spinner fa-spin"></span> Please Wait ...</div>';
        var medium_spinner = '<div style="font-size: 18px;"><span class="fa fa-spinner fa-2x fa-spin"></span> Please Wait ...</div>';
        var large_spinner = '<div style="font-size: 32px;"><span class="fa fa-spinner fa-5x fa-spin"></span> Please Wait ...</div>';

        $('#'+divID).html(medium_spinner);


        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/administration/audit-trail/analytics-data'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:"monthlyAreaChartData",
                month:$("#date").val(),

            },
            success:function (data) {
                $('#'+divID).html('');
                var output = data.output['results'];


              // console.log(output);

               var chartData = output;
                var bar = new Morris.Bar({
                    element: 'bar-chart',
                    resize: true,
                    data: chartData/*[
                        {y: '2006', a: 100, b: 90, c:50},
                        {y: '2007', a: 75, b: 65, c:100},
                        {y: '2008', a: 50, b: 40, c:20},
                        {y: '2009', a: 75, b: 65, c:83},
                        {y: '2010', a: 50, b: 40, c:64},
                        {y: '2011', a: 75, b: 65, c:100},
                        {y: '2012', a: 100, b: 90, c:84}
                    ]*/,
                    barColors: ['#129305', '#ffa30f', '#e4050e'],
                    xkey: 'y',
                    ykeys: ['a', 'b', 'c'],
                    labels: ['CREATE', 'UPDATE', 'DELETE'],
                    hideHover: 'auto'
                });

                //ChartArea(divID,areaChartData);
                ANALYTICS();
            }
        });

    }


    function ANALYTICS() {
        var mini_spinner = '<div><span class="fa fa-spinner fa-spin"></span> Please Wait ...</div>';
        var medium_spinner = '<div style="font-size: 18px;"><span class="fa fa-spinner fa-2x fa-spin"></span> Please Wait ...</div>';
        var large_spinner = '<div style="font-size: 32px;"><span class="fa fa-spinner fa-5x fa-spin"></span> Please Wait ...</div>';

        $('#create').html(large_spinner);
        $('#update').html(large_spinner);
        $('#delete').html(large_spinner);


        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/administration/audit-trail/analytics-data'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:"monthlyAnalytics",
                month:$("#date").val(),
            },
            success:function (data) {
                $('#create').html(0);
                $('#update').html(0);
                $('#delete').html(0);
                var output = data.output['results'];
                //console.log(output);

               $('#create').html(commaSeparateNumber(output.create));
               $('#update').html(commaSeparateNumber(output.update));
               $('#delete').html(commaSeparateNumber(output.delete));
            }
        });

    }

    $(document).ready(function () {


        //BAR CHART
        /*var bar = new Morris.Bar({
            element: 'bar-chart',
            resize: true,
            data: [
                {y: '2006', a: 100, b: 90},
                {y: '2007', a: 75, b: 65},
                {y: '2008', a: 50, b: 40},
                {y: '2009', a: 75, b: 65},
                {y: '2010', a: 50, b: 40},
                {y: '2011', a: 75, b: 65},
                {y: '2012', a: 100, b: 90}
            ],
            barColors: ['#00a65a', '#f56954'],
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['CPU', 'DISK'],
            hideHover: 'auto'
        });*/




        $('#date_selected').html($("#date").val());
        /*$('#date').on('change keyup',function () {
            $('#date_selected').html($("#date").val());
        });*/

        GenerateChart("areaChart");

        $("#searchBtn").on('click',function () {
            GenerateChart("areaChart");
            $('#date_selected').html($("#date").val());
        });

    });

</script>












