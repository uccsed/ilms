<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 12/21/18
 * Time: 3:06 PM
 */
use kartik\date\DatePicker;

?>




<!-- solid sales graph -->
<div class="box box-solid">
    <div class="box-header">
        <i class="fa fa-th"></i>

        <!--<h3 class="box-title text-uppercase text-bold">-->

            <div class="row">
                <div class="col-md-3">
                    <h3>
                        <div class="form-group">
                            <div class="input-group">
                               <?php

                                echo DatePicker::widget([
                                    'id' => 'date',
                                    'name' => 'date',
                                    'value' => date('M,Y'),
                                    'options' => ['placeholder' => 'Month ...'],
                                    'pluginOptions' => [
                                        //'todayHighlight' => true,
                                        'startView'=>'year',
                                        'minViewMode'=>'months',
                                        'todayBtn' => true,
                                        'format' => 'M,yyyy',
                                        'autoclose' => true,
                                    ]
                                ]);
                                ?>
                                <div class="input-group-addon bg-blue-gradient"><button class="btn btn-xs btn-primary" id="searchBtn"><span class="fa fa-search"></span></button></div>
                            </div>
                        </div>
                    </h3>


                </div>
                <div class="col-md-9 text-bold text-uppercase"><h3>Audit trail statistics for <span id="date_selected"></span></h3></div>

            </div>
        <!--</h3>-->









        <div class="box-tools pull-right">

            <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <div class="box-body border-radius-none">
        <!--<canvas class="chart" id="areaChart" style="height: 250px;"></canvas>-->
        <!--<div class="chart" id="chart">
            <div class="chart" id="bar-chart" style="height: 350px;"></div>
        </div>-->
    </div>
    <!-- /.box-body -->

</div>
<!-- /.box -->

