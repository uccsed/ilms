<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 3/26/19
 * Time: 9:34 AM
 */
use yii\helpers\Html;
use yii\grid\GridView;


$this->title = 'AUDIT TRAIL ANALYTICS';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel">
    <div class="panel-heading"><h4>AUDIT TRAIL ANALYTICS</h4></div>
    <div class="panel-body">
        <?php echo Yii::$app->controller->renderPartial('_chart_search'); ?>

    </div>
</div>
<?php echo Yii::$app->controller->renderPartial('_chart'); ?>