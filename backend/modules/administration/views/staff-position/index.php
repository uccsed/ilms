<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administration\models\StaffPositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Staff Positions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-position-index">
    <p>
        <?= Html::a('Create Staff Position', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <div class="panel">
        <div class="panel-heading"><h4><?= Html::encode($this->title) ?></h4></div>
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'staff_position_id',
                    'staff_position',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
