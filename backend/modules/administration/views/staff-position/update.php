<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\StaffPosition */

$this->title = 'Update Staff Position: ' . $model->staff_position;
$this->params['breadcrumbs'][] = ['label' => 'Staff Positions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->staff_position_id, 'url' => ['view', 'id' => $model->staff_position_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="staff-position-update">

    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
