<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\CompanySetupSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-setup-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'abbreviation') ?>

    <?= $form->field($model, 'company_name') ?>

    <?= $form->field($model, 'nature_of_work') ?>

    <?= $form->field($model, 'vote') ?>

    <?php // echo $form->field($model, 'sub_vote') ?>

    <?php // echo $form->field($model, 'TIN') ?>

    <?php // echo $form->field($model, 'default_currency') ?>

    <?php // echo $form->field($model, 'physical_address') ?>

    <?php // echo $form->field($model, 'mailing_address') ?>

    <?php // echo $form->field($model, 'email_address') ?>

    <?php // echo $form->field($model, 'mobile_number') ?>

    <?php // echo $form->field($model, 'telephone_number') ?>

    <?php // echo $form->field($model, 'fax') ?>

    <?php // echo $form->field($model, 'logo') ?>

    <?php // echo $form->field($model, 'website') ?>

    <?php // echo $form->field($model, 'company_motto') ?>

    <?php // echo $form->field($model, 'hq_country') ?>

    <?php // echo $form->field($model, 'hq_region') ?>

    <?php // echo $form->field($model, 'hq_district') ?>

    <?php // echo $form->field($model, 'report_footer') ?>

    <?php // echo $form->field($model, 'report_watermark') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
