<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\CompanySetup */


$this->title = strtoupper($model->company_name).' ('.$model->abbreviation.')';
$this->params['breadcrumbs'][] = ['label' => 'Company Setups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-setup-view">
    <?php echo Yii::$app->controller->renderPartial('_shared_view',array('model'=>$model,'title'=>$this->title)); ?>

</div>
