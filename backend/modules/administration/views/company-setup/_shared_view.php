<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 1/9/19
 * Time: 4:32 PM
 */
use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<div class="row">
    <div class="col-md-9">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('Edit '.$model->company_name, ['create'], ['class' => 'btn btn-success']) ?>
                <h3><?= Html::encode($title) ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <!-- <h5 class="header_highlight text-bold"> Basic Information</h5>-->
                    <div class="col-lg-6">
                        <div class="input-group">
                            <div class="input-group-addon bg-gray-gradient text-bold" style=" font-family: 'Courier New';"><span class="fa fa-clipboard"></span> COMPANY NAME</div>
                            <div class="form-control"><?php echo $model->company_name; ?></div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="input-group">
                            <div class="input-group-addon bg-gray-gradient text-bold" style=" font-family: 'Courier New';"><span class="fa fa-clipboard"></span> ABBREVIATION</div>
                            <div class="form-control"><?php echo $model->abbreviation; ?></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="input-group">
                            <div class="input-group-addon bg-gray-gradient text-bold" style=" font-family: 'Courier New';"><span class="fa fa-clipboard"></span> NATURE OF WORK</div>
                            <div class="form-control"><?php echo $model->natureOfWork->description; ?></div>
                        </div>
                    </div>
                </div>

                <hr />

                <div class="row">
                    <div class="col-lg-3">
                        <div class="input-group">
                            <div class="input-group-addon bg-gray-gradient text-bold" style=" font-family: 'Courier New';"><span class="fa fa-clipboard"></span> VOTE NUMBER</div>
                            <div class="form-control"><?php echo $model->vote; ?></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="input-group">
                            <div class="input-group-addon bg-gray-gradient text-bold" style=" font-family: 'Courier New';"><span class="fa fa-clipboard"></span> SUB-VOTE NUMBER</div>
                            <div class="form-control"><?php echo $model->sub_vote; ?></div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="input-group">
                            <div class="input-group-addon bg-gray-gradient text-bold" style=" font-family: 'Courier New';"><span class="fa fa-clipboard"></span> TIN </div>
                            <div class="form-control"><?php echo $model->TIN; ?></div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="input-group">
                            <div class="input-group-addon bg-gray-gradient text-bold" style=" font-family: 'Courier New';"><span class="fa fa-clipboard"></span> DEFAULT CURRENCY</div>
                            <div class="form-control"><?php echo $model->currency->currency_desc; ?></div>
                        </div>
                    </div>
                </div>



                <hr />

                <div class="row">
                    <div class="col-lg-6">
                        <div class="input-group">
                            <div class="input-group-addon bg-gray-gradient text-bold" style=" font-family: 'Courier New';"><span class="fa fa-clipboard"></span> PHYSICAL ADDRESS</div>
                            <div class="form-control"><?php echo $model->physical_address; ?></div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group">
                            <div class="input-group-addon bg-gray-gradient text-bold" style=" font-family: 'Courier New';"><span class="fa fa-clipboard"></span> MAILING ADDRESS</div>
                            <div class="form-control"><?php echo $model->mailing_address; ?></div>
                        </div>
                    </div>

                </div>





                <hr />

                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <div class="input-group-addon bg-gray-gradient text-bold" style=" font-family: 'Courier New';"><span class="fa fa-clipboard"></span> COMPANY MOTTO</div>
                            <div class="form-control"><?php echo $model->company_motto; ?></div>
                        </div>
                    </div>



                    <div class="col-lg-12"><hr />
                        <div class="input-group">
                            <div class="input-group-addon bg-gray-gradient text-bold" style=" font-family: 'Courier New';"><span class="fa fa-clipboard"></span> REPORT WATERMARK</div>
                            <div class="form-control"><?php echo $model->report_watermark; ?></div>
                        </div>
                    </div>



                    <div class="col-lg-12"><hr />
                        <div class="input-group">
                            <div class="input-group-addon bg-gray-gradient text-bold" style=" font-family: 'Courier New';"><span class="fa fa-clipboard"></span> REPORT FOOTER</div>
                            <div class="form-control"><?php echo $model->report_footer; ?></div>
                        </div>
                    </div>

                </div>






            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <!-- <div class="widget-user-header bg-black" style="background: url('../image/distr/photo1.png') center center;">-->
            <div class="widget-user-header bg-black" style="background: url('../image/JENGO_HESLB_600_337.jpg') center center;">
                <h4 class="text-uppercase bg-purple"><?php echo $model->company_name; ?></h4>
                <h5 class="widget-user-desc"><span class="pull-left"><?php echo $model->abbreviation; ?> </span> <span class="pull-right"></span></h5>
            </div>
            <div class="widget-user-image">
                <img class="img-circle" src="../image/logo/ucc@olas.heslb.go.tz" alt="Company Logo">


            </div>
            <div class="box-footer">
                <table class="table table-bordered">
                    <tr><th class="text-uppercase text-bold">Mobile</th><td><?php echo $model->mobile_number; ?></td></tr>
                    <tr><th class="text-uppercase text-bold">Land Line</th><td><?php echo $model->telephone_number; ?></td></tr>
                    <tr><th class="text-uppercase text-bold">Fax</th><td><?php echo $model->fax; ?></td></tr>
                    <tr><th class="text-uppercase text-bold">Email</th><td><?php echo $model->email_address; ?></td></tr>
                    <tr><th class="text-uppercase text-bold">Postal Address</th><td><?php echo $model->mailing_address; ?></td></tr>
                    <tr><th class="text-uppercase text-bold">Website</th><td><?php echo $model->website; ?></td></tr>
                    <tr><th class="text-uppercase text-bold">Country</th><td><?php echo $model->country->country_name; ?></td></tr>
                    <tr><th class="text-uppercase text-bold">Region</th><td><?php echo $model->region->region_name; ?></td></tr>
                    <tr><th class="text-uppercase text-bold">District</th><td><?php echo $model->district->district_name; ?></td></tr>
                </table>
            </div>
        </div>



        <div class="panel">





            <div class="panel-heading">

            </div>


        </div>
    </div>
</div>


