<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\CompanySetup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-setup-form">



    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL,
    ]); ?>





    <div class="row">
        <h4 class="header_highlight text-bold"> Basic Information</h4>
        <div class="col-md-4">
            <?php
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' =>1,
                'attributes' => [

                    'company_name'=>['label'=>'Company Name', 'options'=>['class'=>'form-group','placeholder'=>'Company Legal name']],

                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?php
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' =>1,
                'attributes' => [

                    'abbreviation'=>['label'=>'Abbreviation', 'options'=>['class'=>'form-group','placeholder'=>'Company abbreviation']],

                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?php
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' =>1,
                'attributes' => [

                    //'district_name'=>['label'=>'District', 'options'=>['placeholder'=>'District name']],

                    'nature_of_work' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        'label' => 'Nature of Work',
                        'options' => [
                            'data' => ArrayHelper::map(\backend\modules\repayment\models\NatureOfWork::find()->all(), 'nature_of_work_id', 'description'),
                            'options' => [
                                'prompt' => 'Select',

                            ],
                        ],
                    ],




                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                ]
            ]);
            ?>
        </div>

    </div>


    <div class="row">
        <div class="col-md-4">
            <?php
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' =>1,
                'attributes' => [

                    'vote'=>['label'=>'Vote Number', 'options'=>['class'=>'form-group','placeholder'=>'Vote Number']],

                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?php
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' =>1,
                'attributes' => [

                    'sub_vote'=>['label'=>'Sub Vote Number', 'options'=>['class'=>'form-group','placeholder'=>'Sub-Vote number']],

                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?php
            /*echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' =>1,
                'attributes' => [

                    'TIN'=>['label'=>'TIN', 'options'=>['class'=>'form-group','placeholder'=>'Tax Identification Number']],

                ]
            ]);*/
?>

           <?= $form->field($model, 'TIN')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '999-999-999',]) ?>

        </div>

    </div>




    <!-- Address Row -->
    <div class="row">
        <!-- Physical Address Row -->
        <div class="col-md-6">
            <h4 class="header_highlight text-bold"> Address</h4>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' =>1,
                        'attributes' => [
                            'hq_country' => ['type' => Form::INPUT_WIDGET,
                                'widgetClass' => \kartik\select2\Select2::className(),
                                'label' => 'Country',
                                'options' => [
                                    'data' => ArrayHelper::map(\backend\modules\administration\models\Country::find()->all(), 'country_id', 'country_name'),
                                    'options' => [
                                        'prompt' => 'Select',

                                    ],
                                ],
                            ],




                            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-12">
                    <?php
                    echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' =>1,
                        'attributes' => [
                            'hq_region' => ['type' => Form::INPUT_WIDGET,
                                'widgetClass' => \kartik\select2\Select2::className(),
                                'label' => 'Region',
                                'options' => [
                                    'data' => ArrayHelper::map(\backend\modules\administration\models\Region::find()->all(), 'region_id', 'region_name'),
                                    'options' => [
                                        'prompt' => 'Select',

                                    ],
                                ],
                            ],




                            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-12">
                    <?php
                    echo $form->field($model, 'hq_district')->widget(DepDrop::classname(), [
                        'type'=>DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'companysetup-hq_district'],
                        'pluginOptions'=>[
                            'depends'=>['companysetup-hq_region'],
                            'placeholder'=>'Select District',
                            'url'=>Url::to(['/administration/company-setup/filtered-districts'])
                        ]
                    ]);
                    ?>
                </div>

                <div class="col-md-12">
                    <?= $form->field($model, 'physical_address')->textarea(['class'=>'form-control','rows' => 2]) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'mailing_address')->textarea(['class'=>'form-control','rows' => 2]) ?>
                </div>

            </div>
        </div>
        <!-- / Physical Address Row -->

        <!-- Contacts Row -->
        <div class="col-md-6">
            <h4 class="header_highlight text-bold">Contacts</h4>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' =>1,
                        'attributes' => [

                            'email_address'=>['label'=>'E-mail Address', 'options'=>['class'=>'form-group','placeholder'=>'E-mail Address']],

                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-12">
                    <?php
                    echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' =>1,
                        'attributes' => [

                            'mobile_number'=>['label'=>'Mobile Number', 'options'=>['class'=>'form-group','placeholder'=>'Mobile Number']],

                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-12">
                    <?php
                    echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' =>1,
                        'attributes' => [

                            'telephone_number'=>['label'=>'Land Line', 'options'=>['class'=>'form-group','placeholder'=>'Land Line']],

                        ]
                    ]);
                    ?>
                </div>

                <div class="col-md-12">
                    <?php
                    echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' =>1,
                        'attributes' => [

                            'fax'=>['label'=>'fax', 'options'=>['class'=>'form-group','placeholder'=>'fax']],

                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-12">
                    <?php
                    echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' =>1,
                        'attributes' => [

                            'website'=>['label'=>'website', 'options'=>['class'=>'form-group','placeholder'=>'website']],

                        ]
                    ]);
                    ?>
                </div>

            </div>
        </div>
        <!--/ Contacts Row -->
    </div>
    <!--/ Address Row -->










    <div class="row">
        <h4 class="header_highlight text-bold"> Report Settings</h4>
        <div class="col-md-6">
            <?= $form->field($model, 'report_footer')->textarea(['rows' => 2]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'report_watermark')->textarea(['rows' => 2]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'company_motto')->textarea(['rows' => 2]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'logo')->textarea(['rows' => 6]) ?>
        </div>

    </div>















    <div class="row">
        <h4 class="header_highlight text-bold"> Other Settings</h4>

        <div class="col-md-4">


            <?php
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' =>1,
                'attributes' => [

                    //'district_name'=>['label'=>'District', 'options'=>['placeholder'=>'District name']],

                    'default_currency' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        'label' => 'Default Currency',
                        'options' => [
                            'data' => ArrayHelper::map(\backend\modules\administration\models\Currency::find()->all(), 'currency_id', 'currency_desc'),
                            'options' => [
                                'prompt' => 'Select Default Currency',

                            ],
                        ],
                    ],




                    //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">

        </div>
        <div class="col-md-4">

        </div>

    </div>



    <?= $form->field($model, 'updated_at')->hiddenInput(['value'=>date('Y-m-d H:i:s')])->label(false) ?>

    <?= $form->field($model, 'updated_by')->hiddenInput(['value'=>Yii::$app->user->id])->label(false) ?>


    <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?php
        echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
        echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

        ActiveForm::end();
        ?>
    </div>









</div>
