<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administration\models\CompanySetupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = strtoupper($model->company_name).' ('.$model->abbreviation.')';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-setup-index">
    <?php echo Yii::$app->controller->renderPartial('_shared_view',array('model'=>$model,'title'=>$this->title)); ?>
</div>
