<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administration\models\UserCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-category-index">

    <p>
        <?= Html::a('Create User Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <div class="panel">
        <div class="panel-heading"><h4><?= Html::encode($this->title) ?></h4></div>
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    'code',
                    'name',
                    'description:ntext',
                    'is_active',
                    // 'icon_tag',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
