<?php

/**
 * @var $content string
 */
use yii\helpers\Url;
use yii\helpers\Html;
use common\widgets\Alert;

yiister\adminlte\assets\Asset::register($this);

?>
 
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <?php $this->beginBody() ?>
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo Yii::$app->urlManager->createUrl('/#'); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">iLMS</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>iLMS</b></span>
                </a>
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <!-- The user image in the navbar-->
                                    <img src="http://placehold.it/160x160" class="user-image" alt="">
                                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                    <span class="hidden-xs"><?= Yii::$app->user->identity->firstname." ".Yii::$app->user->identity->middlename." ".Yii::$app->user->identity->surname; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- The user image in the menu -->
                                    <li class="user-header">
                                        <img src="http://placehold.it/160x160" class="img-circle" alt="">
                                        <p>
                                            <?= Yii::$app->user->identity->firstname." ".Yii::$app->user->identity->middlename." ".Yii::$app->user->identity->surname; ?>
                                        </p>
                                    </li>
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <?=
                                            Html::a(
                                                    'Sign out', ['/site/logout'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                            )
                                            ?>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                   <?=
            \yiister\adminlte\widgets\Menu::widget(
                [
                    "items" => [
                        ["label" => "Dashboard", "url" => Url::to(['/administration/default/index']), 'active' => (Yii::$app->controller->id =='default'&&Yii::$app->controller->action->id =='index'), "icon" => "dashboard",'visible' =>yii::$app->user->can('/administration/default/index')],
                       
                        [
                            "label" => "General Settings",
                            "icon" => "gear",
                            "url" => "#",
                            "items" => [
                                ["label" => "Nature of Work", "url" => Url::to(['/administration/nature-of-work/index']), 'active' => (Yii::$app->controller->id == 'nature-of-work'),'visible' =>yii::$app->user->can('/administration/nature-of-work/index')],
                                ["label" => "Currency", "url" => Url::to(['/administration/currency/index']), 'active' => (Yii::$app->controller->id == 'currency'),'visible' =>yii::$app->user->can('/administration/currency/index')],
                                ["label" => "Country", "url" => Url::to(['/administration/country/index']), 'active' => (Yii::$app->controller->id == 'country'),'visible' =>yii::$app->user->can('/administration/country/index')],
                                ["label" => "Company Setup", "url" => Url::to(['/administration/company-setup/index']), 'active' => (Yii::$app->controller->id == 'company-setup'),'visible' =>yii::$app->user->can('/administration/company-setup/index')],

                                ["label" => "Academic Year", "url" => Url::to(['/administration/academic-year/index']), 'active' => (Yii::$app->controller->id == 'academic-year'),'visible' =>yii::$app->user->can('/administration/academic-year/index')],
                                ["label" => "Financial Year", "url" => Url::to(['/administration/financial-year/index']), 'active' => (Yii::$app->controller->id == 'financial-year'),'visible' =>yii::$app->user->can('/administration/financial-year/index')],

                                ["label" => "Zone", "url" => Url::to(['/administration/zone/index']), 'active' => (Yii::$app->controller->id == 'zone'),'visible' =>yii::$app->user->can('/administration/zone/index')],
                                ["label" => "Region", "url" => Url::to(['/administration/region/index']), 'active' => (Yii::$app->controller->id == 'region'),'visible' =>yii::$app->user->can('/administration/region/index')],
                                ["label" => "District", "url" => Url::to(['/administration/district/index']), 'active' => (Yii::$app->controller->id == 'district'),'visible' =>yii::$app->user->can('/administration/district/index')],
                                ["label" => "Ward", "url" => Url::to(['/administration/ward/index']), 'active' => (Yii::$app->controller->id == 'ward'),'visible' =>yii::$app->user->can('/administration/ward/index')],
                                ["label" => "Title", "url" => Url::to(['/administration/title/index']), 'active' => (Yii::$app->controller->id == 'title'),'visible' =>yii::$app->user->can('/administration/title/index')],
                                ["label" => "Staff", "url" => Url::to(['/administration/staff/index']), 'active' => (Yii::$app->controller->id == 'staff'),'visible' =>yii::$app->user->can('/administration/staff/index')],
                                ["label" => "Staff Position", "url" => Url::to(['/administration/staff-position/index']), 'active' => (Yii::$app->controller->id == 'staff-position'),'visible' =>yii::$app->user->can('/administration/staff-position/index')],
                                ["label" => "Guarantor Position", "url" => Url::to(['/administration/guarantor-position/index']), 'active' => (Yii::$app->controller->id == 'guarantor-position'),'visible' =>yii::$app->user->can('/administration/guarantor-position/index')],
                                ["label" => "Occupation Category", "url" => Url::to(['/administration/occupation-category/index']), 'active' => (Yii::$app->controller->id == 'occupation-category'),'visible' =>yii::$app->user->can('/administration/occupation-category/index')],
                                ["label" => "Occupation", "url" => Url::to(['/administration/occupation/index']), 'active' => (Yii::$app->controller->id == 'occupation'),'visible' =>yii::$app->user->can('/administration/occupation/index')],
                                ["label" => "Identification Type", "url" => Url::to(['/administration/identification-type/index']), 'active' => (Yii::$app->controller->id == 'identification-type'),'visible' =>yii::$app->user->can('/administration/identification-type/index')],

                            ],
                        ],
                        [
                            "label" => "Security Settings",
                            "icon" => "shield",
                            "url" => "#",
                            "items" => [

                                        ["label" => "Routes", "url" => Url::to(['/administration/route']), 'active' => (Yii::$app->controller->id == 'route'),'visible' =>yii::$app->user->can('/administration/route/index')],
                                        ["label" => "Roles", "url" => Url::to(['/administration/role']), 'active' => (Yii::$app->controller->id == 'role'),'visible' =>yii::$app->user->can('/administration/role/index')],
                                        ["label" => "Roles Assignment", "url" => Url::to(['/administration/assignment']), 'active' => (Yii::$app->controller->id == 'assignment'),'visible' =>yii::$app->user->can('/administration/assignment/index')],

                                ["label" => "Business Rules", "url" => Url::to(['/administration/rule']), 'active' => (Yii::$app->controller->id == 'rule'),'visible' =>yii::$app->user->can('/administration/rule/index')],
                                ["label" => "Permissions", "url" => Url::to(['/administration/permission']), 'active' => (Yii::$app->controller->id == 'permission'),'visible' =>yii::$app->user->can('/administration/permission/index')],
                                #["label" => "Menu", "url" => Url::to(['/administration/menu']), 'active' => (Yii::$app->controller->id == 'menu'),'visible' =>yii::$app->user->can('/administration/menu/index')],


                                ["label" => "Users Categories", "url" => Url::to(['/administration/user-category/index']), 'active' => (Yii::$app->controller->id == 'user-category'),'visible' =>yii::$app->user->can('/administration/user-category/index')],
                                        ["label" => "Users Registration", "url" => Url::to(['/administration/user/index']), 'active' => (Yii::$app->controller->id == 'user'),'visible' =>yii::$app->user->can('/administration/user/index')],
                                        /*
                                        ["label" => "Routes", "url" => Url::to(['/administration/default/currency']), 'active' => (Yii::$app->controller->id == 'currency'),'visible' =>yii::$app->user->can('/administration/currency/index')],
                                        ["label" => "Roles", "url" => Url::to(['/administration/default/currency']), 'active' => (Yii::$app->controller->id == 'currency'),'visible' =>yii::$app->user->can('/administration/currency/index')],
                                        ["label" => "Roles Assignment", "url" => Url::to(['/administration/default/currency']), 'active' => (Yii::$app->controller->id == 'currency'),'visible' =>yii::$app->user->can('/administration/currency/index')],
                                        */



                            ],
                        ],
                        ["label" => "Audit Trail","icon" => "fa fa-history", "url" => Url::to(['/administration/audit-trail/index']), 'active' => (Yii::$app->controller->id == 'audit-trail'),'visible' =>yii::$app->user->can('/administration/audit-trail/index')],
                        ["label" => "Report","icon" => "fa fa-bar-chart", "url" => Url::to(['/administration/report/index']), 'active' => (Yii::$app->controller->id == 'report'),'visible' =>yii::$app->user->can('/administration/report/index')],
                        //["label" => "Social Economics Analysis","icon" => "fa fa-bar-chart", "url" => Url::to(['/administration/default/social']), 'active' => (Yii::$app->controller->id == 'report'),'visible' =>yii::$app->user->can('/administration/default/social')],
                        //["label" => "Report","icon" => "fa fa-bar-chart", "url" => Url::to(['/administration/default/social-economics-analysis-printout']), 'active' => (Yii::$app->controller->id == 'report'),'visible' =>yii::$app->user->can('/administration/default/social-economics-analysis-printout')],



                    ],
                    'encodeLabels' => false,
                ]
                    
            );
            ?>
                </section>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                   
                    <?php if (isset($this->params['breadcrumbs'])): ?>
                        <?=
                        \yii\widgets\Breadcrumbs::widget(
                                [
                                    'encodeLabels' => false,
                                    'homeLink' => [
                                        'label' => new \rmrevin\yii\fontawesome\component\Icon('home') . ' Home',
                                        'url' => ["index"],
                                    ],
                                    'links' => $this->params['breadcrumbs'],
                                ]
                        )
                        ?>
<?php endif; ?>
                </section>
                <section class="content">
                    <?= Alert::widget() ?>
                    <?= $content ?>
                    <?php
                    yii\bootstrap\Modal::begin([
                        'headerOptions' => ['id' => 'modalHeader'],
                        'id' => 'modal',
                        'size' => 'modal-medium',
                        //keeps from closing modal with esc key or by clicking out of the modal.
                        // user must click cancel or X to close
                        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
                    ]);
                    echo "<div id='modalContent'></div>";
                    yii\bootstrap\Modal::end();
                    ?>
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->
            <footer class="main-footer">
                <strong>Higher Education Students' Loans Board(HESLB) &copy; <?= date("Y") == "2005" ? "2005" : "2005 - " . date("Y") ?></strong>
                <a class="pull-right hidden-xs" href="http://ucc.co.tz">Powered by UCC</a>
            </footer>
        </div><!-- ./wrapper -->
<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
