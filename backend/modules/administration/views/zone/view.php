<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\Zone */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Zones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zone-view">

    <div class="panel">
        <div class="panel-heading">
            <h3><?= Html::encode($this->title) ?>
            <span class="pull-right">
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
            </span>
            </h3>

        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                   // 'id',
                    'name',
                    'description:ntext',
                    //'is_active',
                    [
                       'name'=>'is_active',
                       'value'=>$model->is_active==1?'YES':'NO',
                       'format'=>'raw',
                       'label'=>'Is Active?',
                    ],
                ],
            ]) ?>


            <h5 class="text-bold text-uppercase">Regions</h5>
            <?php echo Yii::$app->controller->renderPartial('../region/nested_regions_list',array('zoneID'=>$model->id)); ?>
        </div>

    </div>



</div>
