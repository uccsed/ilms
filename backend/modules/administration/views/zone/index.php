<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administration\models\ZoneSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Zones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zone-index">
    <p>
        <?= Html::a('Create Zone', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="panel">
        <div class="panel-heading"><h4><?= Html::encode($this->title) ?></h4></div>
        <div class="panel-body">
            <?php



/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administration\models\RegionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

            <?php
            $gridColumns = [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'hAlign' => GridView::ALIGN_CENTER,
                ],
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'allowBatchToggle' => true,
                    'detail' => function ($model) {
                        //return $this->render('../district/shared_list',['regionID'=>$model->region_id]);
                        return $this->render('../region/nested_regions_list',['zoneID'=>$model->id]);
                    },
                    'detailOptions' => [
                        'class' => 'kv-state-enable',
                    ],
                ],

                [
                    'attribute' => 'name',
                    'label'=>"Zone",
                    'format' => 'raw',

                    'value' => function ($model) {
                        return $model->name;
                    },

                    // 'group'=>true,  // enable grouping,
                ],
                [
                    'attribute' => 'description',
                    'label'=>"Description",
                    'format' => 'raw',

                    'value' => function ($model) {
                        return $model->description;
                    },

                    // 'group'=>true,  // enable grouping,
                ],


                ['class' => 'kartik\grid\ActionColumn',
                    // 'template'=>'{view}',
                ],
            ];
            ?>

            <?php
            /*echo*/ ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,

                'fontAwesome' => true,
//            'asDropdown' => false
                'batchSize' => 50,
                'target' => '_blank',
                'selectedColumns' => [0, 1, 2, 3, 4, 5, 6, 7], // Col seq 2 to 6
                'columnSelectorOptions' => [
                    'label' => 'Export Columns',
                ],
                // 'hiddenColumns' => [15], // SerialColumn, Color, & ActionColumn
                //'disabledColumns' => [0, 1, 2, 3, 4, 5, 6, 9, 12], // ID & Name
                'noExportColumns' => [15],
                'dropdownOptions' => [
                    'label' => 'Export Data',
                    'class' => 'btn btn-default'
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_EXCEL => false,
                    ExportMenu::FORMAT_EXCEL_X => false,
                ],
                //'folder' => '@webroot/tmp', // this is default save folder on server
            ]) . "<hr>\n";
            ?>

            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                //'showPageSummary'=>true,
                //'pageSummaryRowOptions'=>['class'=>'text-bold bg-blue'],
                'pjax'=>true,
                'striped'=>true,
                'hover'=>true,
            ]);

            ?>


        </div>
    </div>


</div>
