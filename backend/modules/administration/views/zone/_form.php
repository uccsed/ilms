<?php
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\Zone */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>
<?php
echo Form::widget([
    'model' => $model,
    'form' => $form,
    'columns' =>1,
    'attributes' => [

        'name'=>['label'=>'Zone Name', 'options'=>['placeholder'=>'Zone name']],
        'description'=>['label'=>'Zone Description', 'options'=>['placeholder'=>'Zone description']],



        'is_active' => ['type' => Form::INPUT_WIDGET,
            'widgetClass' => \kartik\select2\Select2::className(),
            'label' => 'Is Active?',
            'options' => [
                'data' => ['1'=>'YES','0'=>'NO'],
                'options' => [
                    'prompt' => 'Select',

                ],
            ],
        ],
]

]);
?>

<?php

echo '<label class="control-label">Regions</label>';
echo Select2::widget([
    'name' => 'regions',
    'data' => ArrayHelper::map(\backend\modules\administration\models\Region::find()->asArray()->all(),'region_id','region_name'),
    'options' => [
        'placeholder' => 'Select Regions ...',
        'multiple' => true
    ],
]);
?>

<div class="text-right">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php
    echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
    echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

    ActiveForm::end();
    ?>
</div>
<!--
<div class="zone-form">

    <?php /*$form = ActiveForm::begin(); */?>

    <?/*= $form->field($model, 'name')->textInput(['maxlength' => true]) */?>

    <?/*= $form->field($model, 'description')->textarea(['rows' => 6]) */?>

    <?/*= $form->field($model, 'is_active')->textInput() */?>

    <div class="form-group">
        <?/*= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) */?>
    </div>

    <?php /*ActiveForm::end(); */?>

</div>-->
