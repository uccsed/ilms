<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\Staff */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="staff-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'title_id')->textInput() ?>

    <?= $form->field($model, 'staff_position_id')->textInput() ?>

    <?= $form->field($model, 'learning_institution_id')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'Staff' => 'Staff', 'TCU' => 'TCU', 'HLI' => 'HLI', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
