<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\FinancialYear */

$this->title = 'Update Financial Year: ' . $model->financial_year;
$this->params['breadcrumbs'][] = ['label' => 'Financial Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->financial_year_id, 'url' => ['view', 'id' => $model->financial_year_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="financial-year-update">
    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
