<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\disbursement\Module;
/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\FinancialYear */

$this->title = $model->financial_year;
$this->params['breadcrumbs'][] = ['label' => 'Financial Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo Module::FetchBootstrap('js');
?>
<div class="financial-year-view">


    <div class="panel">
        <div class="panel-heading">
            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->financial_year_id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->financial_year_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <button type="button" id="closeBtn" class="btn btn-warning pull-right">Close Financial year <?php echo $model->financial_year; ?></button>
            </p>
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'financial_year_id',
                    'financial_year',
                    //'is_current',
                    [
                        'name'=>'is_current',
                        'value'=>$model->is_active==1?'YES':'NO',
                        'label'=>'Is Current',
                        'format'=>'raw',
                    ],

                    [
                        'name'=>'start_date',
                        'value'=>date('D M jS, Y',strtotime($model->start_date)),
                        'label'=>'Date Opened',
                        'format'=>'raw',
                    ],
                    [
                        'name'=>'end_date',
                        'value'=>date('D M jS, Y',strtotime($model->end_date)),
                        'label'=>'Date Closed',
                        'format'=>'raw',
                    ],

                ],
            ]) ?>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $("#closeBtn").on('click',function () {
                var message = '<?php echo $model->financial_year; ?>';
                var id = '<?php echo $model->financial_year_id; ?>';
                $.ajax({
                    url:"<?php echo Yii::$app->urlManager->createUrl('/administration/financial-year/close-year') ?>",
                    type:"POST",
                    cache:false,
                    data:{
                        _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                        id:id,
                    },
                    success:function (data) {
                        alert('Financial Year '+message+' has been successfully closed!');
                        window.location.reload();
                    }

                });


            });

        });
    </script>

</div>
