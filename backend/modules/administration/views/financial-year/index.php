<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administration\models\FinancialYearSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Financial Years';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="financial-year-index">

    <p>
        <?= Html::a('Create Financial Year', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <div class="panel">
        <div class="panel-heading"><h4><?= Html::encode($this->title) ?></h4></div>
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'financial_year_id',
                    'financial_year',
                    'is_active',
                    'start_date',
                    'end_date',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
