<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\GuarantorPosition */

$this->title = 'Update Guarantor Position: ' . $model->guarantor_position;
$this->params['breadcrumbs'][] = ['label' => 'Guarantor Positions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->guarantor_position_id, 'url' => ['view', 'id' => $model->guarantor_position_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="guarantor-position-update">
    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
