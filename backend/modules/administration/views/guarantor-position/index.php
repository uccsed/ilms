<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administration\models\GuarantorPositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Guarantor Positions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guarantor-position-index">

    <p>
        <?= Html::a('Create Guarantor Position', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <div class="panel">
        <div class="panel-heading"><h4><?= Html::encode($this->title) ?></h4></div>
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'guarantor_position_id',
                    'guarantor_position',
                    'status',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
