<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\GuarantorPosition */

$this->title = $model->guarantor_position;
$this->params['breadcrumbs'][] = ['label' => 'Guarantor Positions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guarantor-position-view">


    <div class="panel">
        <div class="panel-heading">
            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->guarantor_position_id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->guarantor_position_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'guarantor_position_id',
                    'guarantor_position',
                    //'status',
                    [
                        'name'=>'status',
                        'value'=>$model->status=='1'?'ACTIVE':'INACTIVE',
                        'format'=>'html',
                        'label'=>'Status',
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>
