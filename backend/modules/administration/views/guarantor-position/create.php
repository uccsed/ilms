<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\GuarantorPosition */

$this->title = 'Create Guarantor Position';
$this->params['breadcrumbs'][] = ['label' => 'Guarantor Positions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guarantor-position-create">
    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
