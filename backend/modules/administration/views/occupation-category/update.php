<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\OccupationCategory */

$this->title = 'Update Occupation Category: ' . $model->category_desc;
$this->params['breadcrumbs'][] = ['label' => 'Occupation Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->occupation_category_id, 'url' => ['view', 'id' => $model->occupation_category_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="occupation-category-update">

    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
