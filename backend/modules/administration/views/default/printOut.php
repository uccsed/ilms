<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 1/4/19
 * Time: 3:13 PM
 */
use backend\modules\administration\ADMINISTRATION;
$mpdf = new mPDF('c','A4-L','','',5,5,30,25,10,10);
?>
<?php
$html.= '
<html>
<head>
<style>
body {font-family: sans-serif;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0mm solid #000000;
	border-right: 0mm solid #000000;
}
table thead td { background-color: #EEEEEE;
	text-align: center;
	border: 0mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: right;
	font-weight: bolder;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
</style>
</head>

<body>
';
?>



<?php
$html.=ADMINISTRATION::SOCIAL_ECONOMIC_ANALYSIS();
?>



<?php
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle("heslb");
$mpdf->SetAuthor("heslb");
$mpdf->SetWatermarkText("HESLB");
$mpdf->showWatermarkText = true;
/*  $mpdf->SetWatermarkImage('../images/background.jpg');
  $mpdf->showWatermarkImage = true;*/
$mpdf->watermark_font = 'DejaVuSansCondensed';
$mpdf->watermarkTextAlpha = 0.1;
$mpdf->SetDisplayMode('fullpage');
$mpdf->setFooter('|{PAGENO} of {nbpg}|');

?>


<?php
$html.= '
</body>
</html>
';
?>
<?php
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

exit;
?>
