
<?php
use backend\modules\administration\ADMINISTRATION;
$this->title = 'iLMIS Administrative Dashboard';
$this->params['breadcrumbs'][] = ['label' => 'Administration', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//echo ADMINISTRATION::AllocationByCluster();
?>
<div class="administration-default-index">
    <!--Top Row-->
    <div class="row">

        <!-- DATABASE -->
        <div class="col-lg-3 col-xs-6">

            <div class="row">
                <div class="col-xs-4 panel border-primary" style="height: 134px;">
                       <div class="chart" id="database_donut"  style="height: 134px;"></div>
                </div>
                <div class="col-xs-8" style="height: 134px;">
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3 class="text-bold" style="font-size: 24px;" ><span id="database_size"></span> <sup>GB</sup> <span class="pull-right"  id="database_percent"></span></h3>

                            <p class="text-uppercase text-bold"  style="font-size: 18px;" > Database Size</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-database"></i>

                        </div>
                        <div class="col-md-9">
                            <span class="text-bold text-xl-left"></span>
                        </div>
                        <a href="#" class="small-box-footer"  style="font-size: 24px;" ><span id="database_count"></span> Tables</a>
                    </div>
                </div>
            </div>

        </div>
        <!-- /DATABASE -->

        <!-- SYSTEM FILES & FOLDERS -->
        <div class="col-lg-3 col-xs-6">

            <div class="row">
                <div class="col-xs-4 panel border-orange" style="height: 134px;">
                    <div class="chart" id="folder_donut"  style="height: 134px;"></div>
                </div>
                <div class="col-xs-8" style="height: 134px;">
                    <div class="small-box bg-orange">
                        <div class="inner">
                            <h3 class="text-bold" style="font-size: 24px;" ><span id="folder_size"></span> <sup>GB</sup> <span class="pull-right"  id="folder_percent"></span></h3>
                            <p class="text-uppercase text-bold" style="font-size: 18px;"> Files & Folders</p>
                        </div>
                        <div class="icon"><i class="fa fa-folder-open"></i></div>
                        <div class="col-md-9"><span class="text-bold text-xl-left"></span></div>
                        <a href="#" class="small-box-footer"  style="font-size: 24px;"><span id="folder_count"></span> Folders</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /SYSTEM FILES & FOLDERS -->


        <!-- SYSTEM USERS -->

        <div class="col-lg-3 col-xs-6">

            <div class="row">
                <div class="col-xs-4 panel border-green" style="height: 134px;">
                    <div class="chart" id="user_donut"  style="height: 134px;"></div>
                </div>
                <div class="col-xs-8" style="height: 134px;">
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3 class="text-bold" style="font-size: 24px;" ><span id="user_size"></span>  <span class="pull-right"  id="user_percent"></span></h3>
                            <p class="text-uppercase text-bold" style="font-size: 18px;"> System Users</p>
                        </div>
                        <div class="icon"><i class="fa fa-users"></i></div>
                        <div class="col-md-9"><span class="text-bold text-xl-left"></span></div>
                        <a href="#" class="small-box-footer"  style="font-size: 24px;">Today Login: <span id="user_count"></span> Users</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- /SYSTEM USERS -->

        <!-- SERVER -->


        <div class="col-lg-3 col-xs-6">

            <div class="row">
                <div class="col-xs-4 panel border-red" style="height: 134px;">
                    <div class="chart" id="cpu_donut"  style="height: 134px;"></div>
                </div>
                <div class="col-xs-8" style="height: 134px;">
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3 class="text-bold" style="font-size: 24px;" ><span>CPU : <span id="cpu_size"></span>%</span> </h3>
                            <p class="text-uppercase text-bold" style="font-size: 18px;"> Memory Usage<span class="pull-right"><span  id="cpu_percent"></span></span></p>
                        </div>
                        <div class="icon"><i class="fa fa-server"></i></div>
                        <div class="col-md-9"><span class="text-bold text-xl-left"></span></div>
                        <a href="#" class="small-box-footer"  style="font-size: 24px;">CORES: <span id="cpu_count"></span></a>
                    </div>
                </div>
            </div>
        </div>

        <!-- /SERVER -->

    </div>
    <!--/Top Row-->


    <!--Middle Row-->
    <div class="row">
        <div class="col-md-9">
            <div class="panel">
                <?php echo Yii::$app->controller->renderPartial('_middle_chart'); ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel">
                <div class="panel-heading bg-blue-gradient"><h4>SYSTEM USERS</h4></div>
                <?php /*echo ADMINISTRATION::USER_CATEGORY_TABLE(); */?>
                <div id="system_users"> </div>

                <div class="panel-footer bg-blue-gradient"><h4></h4></div>
            </div>


            <div class="panel">
                <div class="panel-heading bg-blue-gradient"><h4><?php echo date('D M jS,Y'); ?> LOGIN</h4></div>
                <div class="box-body chart-responsive">
                    <div class="chart" id="user_login_donut" style="height: 300px; position: relative;"></div>
                </div>

            </div>




            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-black" style="background: url('../dist/img/photo1.png') center center;">
                    <h3 class="widget-user-username" id="recent_login_full_name">Elizabeth Pierce</h3>
                    <h5 class="widget-user-desc"><span class="pull-left" id="recent_login_type">Web Designer </span> <span class="pull-right" id="recent_login_last_login"></span></h5>
                </div>
                <div class="widget-user-image">
                    <!--<img class="img-circle" src="../dist/img/user3-128x128.jpg" alt="User Avatar">-->
                    <div class="img-circle">
                        <span class="fa fa-user fa-3x text-white"></span>
                    </div>

                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header" id="recent_login_sex">M/F</h5>
                                <span class="description-text">SEX</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header" id="recent_login_mobile">255</h5>
                                <span class="description-text">MOBILE</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4">
                            <div class="description-block">
                                <h5 class="description-header"id="recent_login_email" >@</h5>
                                <span class="description-text">EMAIL</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.widget-user -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->



        </div>
    </div>
    <!-- /Middle Row-->






</div>




<script>

    $('body').addClass('sidebar-collapse sidebar-mini');

    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return val;
    }
    function MiniDonutChart(option,donutData,donutColors){
        var donut = new Morris.Donut({
            element: option+'_donut',
            resize: true,
            colors: donutColors,
            //colors: ["#3c8dbc", "#f56954", "#00a65a"],
            data: donutData,

            /* data: [
                {label: "Download Sales", value: 12},
                {label: "In-Store Sales", value: 30},
                {label: "Mail-Order Sales", value: 20}
            ],*/
            hideHover: 'auto'
        });
    }


    function DashboardStatistics(option){
        var mini_spinner = '<div><span class="fa fa-spinner fa-spin"></span> Please Wait ...</div>';
        var medium_spinner = '<div style="font-size: 18px;"><span class="fa fa-spinner fa-2x fa-spin"></span> Please Wait ...</div>';
        var large_spinner = '<div style="font-size: 24px;"><span class="fa fa-spinner fa-5x fa-spin"></span> Please Wait ...</div>';

        $('#'+option+'_size').html(mini_spinner);
        $('#'+option+'_count').html(mini_spinner);
        $('#'+option+'_percent').html(mini_spinner);

        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/administration/default/dashboard-statistics'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:option,
            },
            success:function (data) {
                $('#'+option+'_size').html('');
                $('#'+option+'_count').html('');
                $('#'+option+'_percent').html('');

                var output = data.output.results;
               // console.log(output);

                var count = output.count*1;
                var size = output.size*1;
                var percent = output.percent*1;


                if (option==='user'){
                    $('#'+option+'_size').html(commaSeparateNumber(size.toFixed(0)));
                }else {
                    $('#'+option+'_size').html(commaSeparateNumber(size.toFixed(2)));
                }



                $('#'+option+'_count').html(commaSeparateNumber(count.toFixed(0)));
                $('#'+option+'_percent').html(percent.toFixed(1)+'%');

                var donutData = output.donutData;
                var donutColor = output.donutColors;
                if (option==='user') {
                    MiniDonutChart(option, donutData, donutColor);
                    MiniDonutChart('user_types', donutData, donutColor);
                }else {
                    MiniDonutChart(option, donutData, donutColor);
                }

            }
        });
    }



    function UserLogin(option){
        var mini_spinner = '<div><span class="fa fa-spinner fa-spin"></span> Please Wait ...</div>';
        var medium_spinner = '<div style="font-size: 18px;"><span class="fa fa-spinner fa-2x fa-spin"></span> Please Wait ...</div>';
        var large_spinner = '<div style="font-size: 24px;"><span class="fa fa-spinner fa-5x fa-spin"></span> Please Wait ...</div>';

        $('#'+option+'_donut').html(mini_spinner);


        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/administration/default/dashboard-statistics'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:option,
            },
            success:function (data) {
                $('#'+option+'_donut').html('');


                var output = data.output.results;

                var donutData = output.donutData;
                var donutColor = output.donutColors;
                MiniDonutChart(option, donutData, donutColor);


            }
        });
    }

    function SystemUsers(option){
        var mini_spinner = '<div><span class="fa fa-spinner fa-spin"></span> Please Wait ...</div>';
        var medium_spinner = '<div style="font-size: 18px;"><span class="fa fa-spinner fa-2x fa-spin"></span> Please Wait ...</div>';
        var large_spinner = '<div style="font-size: 24px;"><span class="fa fa-spinner fa-5x fa-spin"></span> Please Wait ...</div>';

        $('#'+option).html(mini_spinner);


        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/administration/default/dashboard-statistics'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:option,
            },
            success:function (data) {
                $('#'+option).html('');


                var output = data.output.results;
                $('#'+option).html(output);




            }
        });
    }


    function RecentUserLogin(option){
        var mini_spinner = '<div><span class="fa fa-spinner fa-spin"></span> Please Wait ...</div>';
        var medium_spinner = '<div style="font-size: 18px;"><span class="fa fa-spinner fa-2x fa-spin"></span> Please Wait ...</div>';
        var large_spinner = '<div style="font-size: 24px;"><span class="fa fa-spinner fa-5x fa-spin"></span> Please Wait ...</div>';
        $('#'+option+'_full_name').html(mini_spinner);
        $('#'+option+'_type').html(mini_spinner);
        $('#'+option+'_last_login').html(mini_spinner);
        $('#'+option+'_sex').html(mini_spinner);
        $('#'+option+'_mobile').html(mini_spinner);
        $('#'+option+'_email').html(mini_spinner);

        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl('/administration/default/dashboard-statistics'); ?>",
            type:"POST",
            cache:false,
            data:{
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                option:option,
            },
            success:function (data) {
                $('#'+option+'_full_name').html('');
                $('#'+option+'_type').html('');
                $('#'+option+'_last_login').html('');
                $('#'+option+'_sex').html('');
                $('#'+option+'_mobile').html('');
                $('#'+option+'_email').html('');

                var output = data.output.results;
                //console.log(output);
                var full_name = '';
                var type = '';
                var last_login = '';
                var sex = '';
                var mobile = '';
                var email = '';
                $.each(output,function (index,dataObject) {
                     full_name = dataObject.firstname+' '+dataObject.middlename+' '+dataObject.surname;
                     type = dataObject.loginType;
                    last_login = dataObject.last_login;
                    sex = dataObject.sex;
                    mobile = dataObject.mobile;
                    email = dataObject.email;
                });
                $('#'+option+'_full_name').html(full_name);
                $('#'+option+'_type').html(type);
                $('#'+option+'_last_login').html(last_login);
                $('#'+option+'_sex').html(sex);
                $('#'+option+'_mobile').html(mobile);
                $('#'+option+'_email').html(email);

            }
        });
    }

    $(document).ready(function () {
        //alert('oyeeh!');
        DashboardStatistics('database');
        DashboardStatistics('folder');
        DashboardStatistics('user');
        DashboardStatistics('cpu');
        UserLogin('user_login');
        RecentUserLogin('recent_login');
        SystemUsers('system_users');




        setInterval(myMethod, 30000);

        function myMethod( )
        {
            //this will repeat every 30 seconds
            //you can reset counter here
            UserLogin('user_login');
            RecentUserLogin('recent_login');
            DashboardStatistics('cpu');
        }
    });
</script>
