<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 1/4/19
 * Time: 4:06 PM
 */
use Yii;
use yii\helpers\Html;
use backend\modules\administration\ADMINISTRATION;
?>
<?= Html::a('Download Social Economics Analysis (PDF)', ['default/social-economics-analysis-printout'], ['class' => 'btn bg-orange-active text-right','target'=>'_blank']) ?>
<div>
    <div class="panel">
        <div class="panel-heading"><h4 class="text-bold">SOCIAL ECONOMICS ANALYSIS</h4></div>
        <div class="panel-body"><?php echo ADMINISTRATION::SOCIAL_ECONOMIC_ANALYSIS(); ?></div>
    </div>
</div>


