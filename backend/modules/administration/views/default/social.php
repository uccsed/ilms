<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 1/3/19
 * Time: 4:11 PM
 */

$attachmentArray = array();
$totalSQL = "
SELECT 
COUNT(DISTINCT allocation.application_id) AS 'GrandTotal',
SUM(allocation.allocated_amount) AS 'allocated_amount'
 
FROM allocation
INNER JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
INNER JOIN application ON allocation.application_id = application.application_id
INNER JOIN applicant ON application.applicant_id = applicant.applicant_id
INNER JOIN user ON applicant.user_id = user.user_id
INNER JOIN programme ON application.programme_id = programme.programme_id
INNER JOIN programme_group ON programme.programme_group_id = programme_group.programme_group_id
LEFT JOIN applicant_attachment ON allocation.application_id = applicant_attachment.application_id
WHERE 
#(applicant_attachment.attachment_definition_id ='21' OR applicant_attachment.attachment_definition_id = '23' OR applicant_attachment.attachment_definition_id = '25')
applicant_attachment.attachment_definition_id IN (6,9,12,15,20,21,23,25) 
#((applicant_attachment.attachment_definition_id = '12' AND applicant_attachment.attachment_definition_id <> '15') OR (applicant_attachment.attachment_definition_id = '15' AND applicant_attachment.attachment_definition_id <> '12')) # AND applicant_attachment.attachment_definition_id <> '15') OR (applicant_attachment.attachment_definition_id <> '12' AND applicant_attachment.attachment_definition_id = '15')
 AND allocation_batch.academic_year_id = '1'  
";
$totalModel = Yii::$app->db->createCommand($totalSQL)->queryAll();
$GRAND_TOTAL = 0;
$GRAND_AMOUNT = 0;
$GRAND_PERCENT = 0;
$grandFemalePercent = $grandMalePercent = 0;
if (sizeof($totalModel)!=0){
    $GRAND_TOTAL = $totalModel[0]['GrandTotal'];
    $GRAND_AMOUNT = $totalModel[0]['allocated_amount'];
}
$mainSQL = "
SELECT 
programme_group.programme_group_id AS 'programme_group_id',
UPPER(programme_group.group_name) AS 'COURSE',
COUNT(DISTINCT allocation.application_id) AS 'GrandTotal',
SUM(allocation.allocated_amount) AS 'allocated_amount'
 
FROM allocation
INNER JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
INNER JOIN application ON allocation.application_id = application.application_id
INNER JOIN applicant ON application.applicant_id = applicant.applicant_id
INNER JOIN user ON applicant.user_id = user.user_id
INNER JOIN programme ON application.programme_id = programme.programme_id
INNER JOIN programme_group ON programme.programme_group_id = programme_group.programme_group_id
LEFT JOIN applicant_attachment ON allocation.application_id = applicant_attachment.application_id
WHERE attachment_definition_id IN (6,9,12,15,20,21,23,25) AND allocation_batch.academic_year_id = '1' 
GROUP BY programme_group.programme_group_id
ORDER BY programme_group.group_name ASC
#LIMIT 5;
;
";
$mainModel = Yii::$app->db->createCommand($mainSQL)->queryAll();

$criteriaArray= array(
    '0'=>'DISABILITY',
    '1'=>'FULL ORPHAN',
    '2'=>'PARTIAL ORPHAN',
    '3'=>'POOR FAMILY',
    '4'=>'SPONSORED');
$sexArray = array('M'=>'M','F'=>'F');
$criteriaDocuments = array();
$criteriaDocuments[0] = "applicant_attachment.attachment_definition_id = '6'";
$criteriaDocuments[1] = "(applicant_attachment.attachment_definition_id = '12' AND applicant_attachment.attachment_definition_id = '15')";
$criteriaDocuments[2] = "((applicant_attachment.attachment_definition_id = '12' AND applicant_attachment.attachment_definition_id <> '15') OR (applicant_attachment.attachment_definition_id <> '12' AND applicant_attachment.attachment_definition_id = '15'))";
$criteriaDocuments[3] = "(applicant_attachment.attachment_definition_id = '9')";
$criteriaDocuments[4] = "(applicant_attachment.attachment_definition_id ='21' OR applicant_attachment.attachment_definition_id = '23' OR applicant_attachment.attachment_definition_id = '25')";
$grandAllocation = $grandBeneficiaries = $grandMale = $grandFemale = 0;
?>


<table width="100%" style=" font-family: 'Courier New';" border="1">
    <thead>
    <tr>
        <th>COURSE STATUS</th>
        <?php foreach ($criteriaArray as $criteriaIndex=>$criteria){ ?>
        <th style="text-align: center;" colspan="2"><?php echo $criteria;  ?></th>
       <!-- <th style="text-align: center;" colspan="2">DISABILITY</th>
        <th style="text-align: center;" colspan="2">FULL ORPHAN</th>
        <th style="text-align: center;" colspan="2">PARTIAL ORPHAN</th>
        <th style="text-align: center;" colspan="2">POOR FAMILY</th>
        <th style="text-align: center;" colspan="2">SPONSORED</th>-->

        <?php } ?>
        <th style="text-align: center;" rowspan="2">FEMALE %</th>
        <th style="text-align: center;" rowspan="2">MALE %</th>
        <th style="text-align: center;" rowspan="2">Grand Total</th>
        <th style="text-align: center;" rowspan="2">%</th>
        <th style="text-align: center;" rowspan="2">Allocated Amount</th>
    </tr>
    <tr>
        <th style="text-align: center;"></th>
        <th style="text-align: center;">M</th>
        <th style="text-align: center;">F</th>
        <th style="text-align: center;">M</th>
        <th style="text-align: center;">F</th>
        <th style="text-align: center;">M</th>
        <th style="text-align: center;">F</th>
        <th style="text-align: center;">M</th>
        <th style="text-align: center;">F</th>
        <th style="text-align: center;">M</th>
        <th style="text-align: center;">F</th>
        <!--      <th>M</th>
              <th>F</th>-->
    </tr>
    </thead>
    <tbody>

    <?php foreach ($mainModel AS $mainIndex=>$dataArrayMain){ $programme = $dataArrayMain['programme_group_id']; ?>
        <tr>
            <th><?php echo $dataArrayMain['COURSE']; ?></th>
            <?php foreach ($criteriaArray as $criteriaIndex=>$criteria){ ?>
                <?php foreach ($sexArray AS $sexIndex=>$sex){ ?>
                    <th style="text-align: right;"><?php echo number_format(sumSEX($programme,$sex,$criteriaDocuments[$criteriaIndex],'GrandTotal'),0); ?></th>
                <?php } ?>

            <?php } ?>

            <?php
            $total = $dataArrayMain['GrandTotal'];

            $percentMale = $percentFemale = $percentTotal = 0;
            $maleCount = sumSEX($programme,'M',null,'GrandTotal');
            $femaleCount = sumSEX($programme,'F',null,'GrandTotal');
            $grandFemale+= $femaleCount;
            $grandMale+= $maleCount;


            if ($total>0){
                $percentFemale = round((($femaleCount/$total)*100),0);
                $percentMale = round((($maleCount/$total)*100),0);
            }

            if ($GRAND_TOTAL>0){
                $percentTotal = round(((($femaleCount+$maleCount)/$GRAND_TOTAL)*100),2);

            }
            $GRAND_PERCENT+=$percentTotal;


            ?>
            <th style="text-align: right;"><?php echo $percentFemale; ?>%</th>
            <th style="text-align: right;"><?php echo $percentMale; ?>%</th>
            <th style="text-align: right;"><?php echo number_format($dataArrayMain['GrandTotal'],0); ?></th>
            <th style="text-align: right;"><?php echo $percentTotal; ?>%</th>
            <th style="text-align: right;"><?php echo number_format($dataArrayMain['allocated_amount'],2); ?></th>
        </tr>
        <?php
        $grandBeneficiaries+= $dataArrayMain['GrandTotal'];
        $grandAllocation+= $dataArrayMain['allocated_amount'];
        ?>
    <?php } ?>

    </tbody>
    <tfoot>
    <tr>
        <th>Grand Total</th>
        <?php foreach ($criteriaArray as $criteriaIndex=>$criteria){ ?>
            <?php foreach ($sexArray AS $sexIndex=>$sex){ ?>


                <th style="text-align: right;"><?php echo number_format(sumSEX(null,$sex,$criteriaDocuments[$criteriaIndex],'GrandTotal'),0); ?></th>
            <?php } ?>
        <?php } ?>


        <?php
            if ($GRAND_TOTAL>0){

                $FEMALE = sumSEX(null,'F',null,'GrandTotal');
                $MALE = sumSEX(null,'M',null,'GrandTotal');

                $grandFemalePercent = round((($FEMALE/$GRAND_TOTAL)*100),0);
                    $grandMalePercent = round((($MALE/$GRAND_TOTAL)*100),0);
            }
        ?>
        <th style="text-align: right;"><?php echo number_format($grandFemalePercent,0); ?>%</th>
        <th style="text-align: right;"><?php echo number_format($grandMalePercent,0); ?>%</th>

        <th style="text-align: right;"><?php echo number_format($grandBeneficiaries,0); ?></th>
        <th style="text-align: right;"><?php echo $GRAND_PERCENT; ?>%</th>
        <th style="text-align: right;"><?php echo number_format($grandAllocation,2); ?></th>
    </tr>
    </tfoot>
</table>

<?php
function sumSEX($programme=null,$sex,$criteria=null,$option)
{

    if (/*$criteria!==null && $criteria!='' && */!empty($criteria)){
        $WHERE = $criteria;
    }else{
        //$WHERE = 'attachment_definition_id IN (6,9,12,15,20,21,23,25)';
       // $WHERE = 'applicant_attachment.attachment_definition_id IN (6,9,12,15,20,21,23,25)';
        $WHERE = "(applicant_attachment.attachment_definition_id = '6' OR applicant_attachment.attachment_definition_id = '9' OR applicant_attachment.attachment_definition_id = '12' OR applicant_attachment.attachment_definition_id = '15' OR applicant_attachment.attachment_definition_id = '20' OR applicant_attachment.attachment_definition_id = '21' OR applicant_attachment.attachment_definition_id = '23' OR applicant_attachment.attachment_definition_id = '25')";
    }

      if ($programme!==null){
          $PGM = " AND programme.programme_group_id = '$programme'";
      }else{
          $PGM = '';
      }

    $SQL = "
            SELECT 
           COUNT(DISTINCT allocation.application_id) AS 'GrandTotal',
            SUM(allocation.allocated_amount) AS 'allocated_amount'
            FROM allocation
            INNER JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
            INNER JOIN application ON allocation.application_id = application.application_id
            INNER JOIN applicant ON application.applicant_id = applicant.applicant_id
            INNER JOIN user ON applicant.user_id = user.user_id
            INNER JOIN programme ON application.programme_id = programme.programme_id
            INNER JOIN programme_group ON programme.programme_group_id = programme_group.programme_group_id
            LEFT JOIN applicant_attachment ON allocation.application_id = applicant_attachment.application_id
            WHERE $WHERE AND allocation_batch.academic_year_id = '1'  
            AND user.sex = '$sex' $PGM
     ";
    $model =  Yii::$app->db->createCommand($SQL)->queryAll();
    if (sizeof($model)!=0){
        return $model[0][$option];
    }else{
        return 0;
    }

    /*     $SQL = "
         SELECT
    UPPER(programme_group.group_name) AS 'COURSE',
    user.sex 'SEX',
    COUNT(DISTINCT allocation.application_id) AS 'GrandTotal',
    SUM(allocation.allocated_amount) AS 'allocated_amount'

    FROM allocation
    INNER JOIN allocation_batch ON allocation.allocation_batch_id = allocation_batch.allocation_batch_id
    INNER JOIN application ON allocation.application_id = application.application_id
    INNER JOIN applicant ON application.applicant_id = applicant.applicant_id
    INNER JOIN user ON applicant.user_id = user.user_id
    INNER JOIN programme ON application.programme_id = programme.programme_id
    INNER JOIN programme_group ON programme.programme_group_id = programme_group.programme_group_id
    LEFT JOIN applicant_attachment ON allocation.application_id = applicant_attachment.application_id

    WHERE $WHERE AND allocation_batch.academic_year_id = '1' AND user.sex = '$sex' $PGM;
         #LIMIT 10";

         $model =  Yii::$app->db->createCommand($SQL)->queryAll();

         if (sizeof($model)!=0){
             return $model[0][$option];
         }else{
             return 0;
         }*/

    //return 0;


}
?>
