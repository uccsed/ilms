<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 1/8/19
 * Time: 12:56 PM
 */
use backend\modules\administration\ADMINISTRATION;
?>
<?php

ob_end_clean();
$objPHPExcel = new \PHPExcel();
$sheet=0;

$objPHPExcel->setActiveSheetIndex($sheet);

$objPHPExcel->setActiveSheetIndex(0)
    ->mergeCells('B1:C1')
    ->mergeCells('D1:E1')
    ->mergeCells('F1:G1')
    ->mergeCells('H1:I1')
    ->mergeCells('J1:K1')



    ->setCellValue('A1', 'COURSE STATUS')

    ->setCellValue('B1', "DISABILITY")
    ->setCellValue('B2', "M")
    ->setCellValue('C2', "F")

    ->setCellValue('D1', "FULL ORPHAN")
    ->setCellValue('D2', "M")
    ->setCellValue('E2', "F")

    ->setCellValue('F1', "PARTIAL ORPHAN")
    ->setCellValue('F2', "M")
    ->setCellValue('G2', "F")

    ->setCellValue('H1', "POOR FAMILY")
    ->setCellValue('H2', "M")
    ->setCellValue('I2', "F")

    ->setCellValue('J1', "SPONSORED")
    ->setCellValue('J2', "M")
    ->setCellValue('K2', "F")

    ->setCellValue('L1', "FEMALE %")
    ->setCellValue('M1', "MALE %")
    ->setCellValue('N1', "GRAND TOTAL")
    ->setCellValue('O1', "%")
    ->setCellValue('P1', "ALLOCATED AMOUNT");


$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('O1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);




$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);

/*$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);*/

$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize('8');


ADMINISTRATION::SOCIAL_ECONOMIC_ANALYSIS_EXCEL($objPHPExcel);
header('Content-Type: application/vnd.ms-excel');
$filename = "SocialEconomicAllocation_".date("d-m-Y-His").".xls";
header('Content-Disposition: attachment;filename='.$filename .' ');
header('Cache-Control: max-age=0');
$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit();

?>

