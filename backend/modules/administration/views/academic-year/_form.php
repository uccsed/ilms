<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\AcademicYear */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="academic-year-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL,
    ]); ?>

    <?php
    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' =>1,
        'attributes' => [

            'academic_year'=>['label'=>'Academic Year', 'options'=>['placeholder'=>'Academic Year']],


            'is_current' => ['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'Is Current ?',
                'options' => [
                    'data' => ['1'=>'YES','0'=>'NO'],
                    'options' => [
                        'prompt' => 'Select',

                    ],
                ],
            ],

            //'closed_date'=>['label'=>'Closed Date', 'options'=>['placeholder'=>'Date Closed']],



            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],

            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
        ]
    ]);


    echo $form->field($model, 'closed_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter End date ...'],
        'value'=>date('Y-m-d'),
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>

    <?php /*= $form->field($model, 'academic_year')->textInput(['maxlength' => true]) ?><!--

    <? = $form->field($model, 'is_current')->textInput() ?>

    --><? = $form->field($model, 'closed_date')->textInput() ?>

    <? /*= $form->field($model, 'closed_by')->textInput() */?>

    <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?php
        echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
        echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

        ActiveForm::end();
        ?>
    </div>

</div>
