<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\AcademicYear */

$this->title = $model->academic_year;
$this->params['breadcrumbs'][] = ['label' => 'Academic Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo Module::FetchBootstrap('js');
?>
<div class="academic-year-view">


<div class="panel">
    <div class="panel-heading">
        <h3><?= Html::encode($this->title) ?></h3>

        <p>
            <?= Html::a('Update', ['update', 'id' => $model->academic_year_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->academic_year_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
            <button type="button" id="closeBtn" class="btn btn-warning pull-right">Close Academic year <?php echo $model->academic_year; ?></button>
        </p>
    </div>
    <div class="panel-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'academic_year_id',
                'academic_year',
                //'is_current',
                [
                     'name'=>'is_current',
                     'value'=>$model->is_current==1?'YES':'NO',
                     'label'=>'Is Current',
                     'format'=>'raw',
                ],
                //'closed_date',
                [
                    'name'=>'closed_date',
                    'value'=>date('D M jS, Y',strtotime($model->closed_date)),
                    'label'=>'Date Closed',
                    'format'=>'raw',
                ],
                //'closed_by',
                [
                    'name'=>'closed_by',
                    'value'=>Module::UserInfo($model->closed_by,'fullName'),
                    'label'=>'Closed By',
                    'format'=>'raw',
                ],
            ],
        ]) ?>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#closeBtn").on('click',function () {
            var message = '<?php echo $model->academic_year; ?>';
            var id = '<?php echo $model->academic_year_id; ?>';
            $.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl('/administration/academic-year/close-year') ?>",
                type:"POST",
                cache:false,
                data:{
                    _csrf : '<?=Yii::$app->request->getCsrfToken()?>',
                    id:id,
                },
                success:function (data) {
                    alert('Academic Year '+message+' has been successfully closed!');
                    window.location.reload();
                }

            });


        });

    });
</script>
</div>
