<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\disbursement\Module;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administration\models\AcademicYearSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Academic Years';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-year-index">

    <p>
        <?= Html::a('Create Academic Year', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="panel">
        <div class="panel-heading"><h4><?= Html::encode($this->title) ?></h4></div>
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'academic_year_id',
                    'academic_year',
                    //'is_current',
                    [
                        'attribute'=>'is_current',
                        //'width'=>'250px',
                        'value'=>function($model){
                            return $model->is_current == 1?'YES':'NO';
                        },
                        'label'=>"Is Current",
                        'filter'=>false,

                    ],
                    //'closed_date',
                    [
                        'attribute'=>'closed_date',
                        //'width'=>'250px',
                        'value'=>function($model){
                            return $model->closed_date != '0000-00-00 00:00:00' && !empty($model->closed_date)? date('D jS M,Y',strtotime($model->closed_date)):'NOT SET';
                        },
                        'label'=>"Date Closed",
                        'filter'=>false,

                    ],

                    //'closed_by',
                    [
                        'attribute'=>'closed_by',
                        //'width'=>'250px',
                        'value'=>function($model){
                            return Module::UserInfo($model->closed_by,'fullName');
                        },
                        'label'=>"Closed By",
                        'filter'=>false,

                    ],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>

</div>
