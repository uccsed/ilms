<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\AcademicYear */

$this->title = 'Update Academic Year: ' . $model->academic_year;
$this->params['breadcrumbs'][] = ['label' => 'Academic Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->academic_year_id, 'url' => ['view', 'id' => $model->academic_year_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="academic-year-update">
    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
