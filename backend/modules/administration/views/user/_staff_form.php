<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 3/21/19
 * Time: 10:33 AM
 */
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use kartik\widgets\FileInput;
use yii\widgets\MaskedInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use backend\modules\administration\ADMINISTRATION;
use backend\modules\administration\models\UserCategory;
?>
<div class="row">
    <div class="form-group col-md-6">
        <label class="col-lg-3 control-label">Title</label>
        <div class="col-lg-9">
            <?php
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' =>1,
                'attributes' => [

                    'title_id' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        //'label' => 'Zone',
                        'label' => false,
                        'options' => [
                            'data' => ArrayHelper::map(\backend\modules\administration\models\Title::find()->all(), 'title_id', 'title'),
                            'options' => [
                                'prompt' => 'Select Staff Title',

                            ],
                        ],
                    ],

                ]
            ]);
            ?>
        </div>
    </div>

    <div class="form-group col-md-12">
        <label class="col-lg-3 control-label">Position</label>
        <div class="col-lg-9">
            <?php
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' =>1,
                'attributes' => [

                    'staff_position_id' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        //'label' => 'Zone',
                        'label' => false,
                        'options' => [
                            'data' => ArrayHelper::map(\backend\modules\administration\models\StaffPosition::find()->all(), 'staff_position_id', 'staff_position'),
                            'options' => [
                                'prompt' => 'Select Staff Position',

                            ],
                        ],
                    ],

                ]
            ]);
            ?>
        </div>
    </div>


    <div class="form-group col-md-12">
        <label class="col-lg-3 control-label">Learning Institution</label>
        <div class="col-lg-9">
            <?php
            $instSQL = "SELECT learning_institution_id AS 'id', CONCAT(IFNULL(institution_name,''),' - ',IFNULL(institution_code,'')) AS 'name' FROM learning_institution WHERE institution_type IN('UNIVERSITY','COLLEGE') ORDER BY institution_name ASC;";
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' =>1,
                'attributes' => [

                    'learning_institution_id' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => \kartik\select2\Select2::className(),
                        //'label' => 'Zone',
                        'label' => false,
                        'options' => [
                            'data' => ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql($instSQL)->asArray()->all(), 'id', 'name'),
                            'options' => [
                                'prompt' => 'Select Institution',

                            ],
                        ],
                    ],

                ]
            ]);
            ?>
        </div>
    </div>
</div>
