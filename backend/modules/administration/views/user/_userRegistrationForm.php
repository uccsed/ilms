<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 3/20/19
 * Time: 11:52 AM
 */

?>


<?php


use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use kartik\widgets\FileInput;
use yii\widgets\MaskedInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use backend\modules\administration\ADMINISTRATION;
use backend\modules\administration\models\UserCategory;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\District */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <?php $form = ActiveForm::begin(
            [
                    'options'=>['enctype'=>'multipart/form-data'],
                    'type' => ActiveForm::TYPE_VERTICAL,
             ]); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-heading"><h3>Primary Information</h3></div>
                <div class="panel-body">

                    <div class="row">

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="col-lg-12 control-label">First Name</label>
                                <div class="col-lg-12">
                                    <?php
                                    echo Form::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'columns' =>1,
                                        'attributes' => [

                                            'firstname'=>['label'=>false, 'options'=>['placeholder'=>'First_name']],


                                            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                        ]
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="col-lg-12 control-label">Middle Name</label>
                                <div class="col-lg-12">
                                    <?php
                                    echo Form::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'columns' =>1,
                                        'attributes' => [

                                            'middlename'=>['label'=>false, 'options'=>['placeholder'=>'Middle Name']],


                                            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                        ]
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="col-lg-12 control-label">Surname</label>
                                <div class="col-lg-12">
                                    <?php
                                    echo Form::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'columns' =>1,
                                        'attributes' => [

                                            'surname'=>['label'=>false, 'options'=>['placeholder'=>'Surname']],


                                            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                        ]
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12"><hr /></div>
                        <div class="col-lg-8">
                            <div class="form-group col-md-12">
                                <label class="col-lg-3 control-label">User Category</label>
                                <div class="col-lg-9">
                                    <?php
                                    echo Form::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'columns' =>1,
                                        'attributes' => [

                                            'login_type' => ['type' => Form::INPUT_WIDGET,
                                                'widgetClass' => \kartik\select2\Select2::className(),
                                                //'label' => 'Zone',
                                                'label' => false,
                                                'options' => [
                                                    'data' => ArrayHelper::map(\backend\modules\administration\models\UserCategory::find()->all(), 'id', 'name'),
                                                    'options' => [
                                                        'prompt' => 'Select USer Category',

                                                    ],
                                                ],
                                            ],

                                        ]
                                    ]);
                                    ?>
                                </div>
                            </div>


                            <div class="form-group col-md-12">
                                <label class="col-lg-3 control-label">Username</label>
                                <div class="col-lg-9">
                                    <?php
                                    echo Form::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'columns' =>1,
                                        'attributes' => [

                                            'username'=>['label'=>false, 'options'=>['placeholder'=>'username']],


                                            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                        ]
                                    ]);
                                    ?>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="col-lg-3 control-label">Password</label>
                                <div class="col-lg-9">
                                    <?php

                                    echo Form::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'columns' =>1,
                                        'attributes' => [

                                            'password'=>['label'=>false, 'options'=>['type'=>'password','placeholder'=>'password']],


                                            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                        ]
                                    ]);
                                    ?>

                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="col-lg-3 control-label">E-mail</label>
                                <div class="col-lg-9">
                                    <?php

                                    echo Form::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'columns' =>1,
                                        'attributes' => [

                                            'email_address'=>['label'=>false, 'options'=>['type'=>'email','placeholder'=>'E-mail Address']],


                                            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                                        ]
                                    ]);
                                    ?>

                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="col-lg-3 control-label">Mobile #</label>
                                <div class="col-lg-9">
                                    <?php


                                    echo $form->field($model, 'phone_number')->widget(MaskedInput::class, [
                                        'mask' => '0999 999 999',
                                        'options'=>['placeholder'=>'0717 366 599','class'=>'form-control']
                                    ])->label(false);


                                    /*
                                          echo MaskedInput::widget([
                                        'name' => 'input-33',
                                        'clientOptions' => [
                                            'alias' =>  'decimal',
                                            'groupSeparator' => ',',
                                                'autoGroup' => true
                                            ],
                                        ]);


                                    echo MaskedInput::widget([
                                        'name' => 'input-36',
                                        'clientOptions' => [
                                            'alias' =>  'email'
                                        ],
                                    ]);

                                    */
                                    ?>

                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="col-lg-3 control-label">Sex</label>
                                <div class="col-lg-9">
                                    <?php
                                    echo Form::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'columns' =>1,
                                        'attributes' => [

                                            'sex' => ['type' => Form::INPUT_WIDGET,
                                                'widgetClass' => \kartik\select2\Select2::className(),
                                                'label' => false,
                                                'options' => [
                                                    'data' => ['M'=>'Male','F'=>'Female'],
                                                    'options' => [
                                                        'prompt' => 'Select Sex',

                                                    ],
                                                ],
                                            ],

                                        ]
                                    ]);
                                    ?>

                                </div>
                            </div>




                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <?php
                                echo $form->field($model, 'photo')->widget(FileInput::classname(), [
                                    //'options' => ['accept' => 'image/*'],
                                    'options' => ['accept' => 'image/*'],
                                    'pluginOptions' => [
                                        'showPreview' => true,
                                        'showCaption' => true,
                                        'showRemove' => true,
                                        'showUpload' => false
                                    ]
                                ]);


                                ?>
                            </div>
                        </div>
                    </div>












                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-9">
                            <?php

                            ?>
                        </div>
                    </div>




                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-heading"><h3>Secondary Information</h3></div>
                <div class="panel-body">
                   <div id="staff_form" class="staff">
                       <?php echo Yii::$app->controller->renderPartial('_staff_form',['model'=>$model,'form'=>$form]) ?>
                   </div>

                    <div id="employer_form" class="employer">
                        <?php echo Yii::$app->controller->renderPartial('_employer_form',['model'=>$model,'form'=>$form]) ?>
                    </div>



                </div>
            </div>

        </div>

    </div>




    <?php
    /*echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' =>1,
        'attributes' => [

            'district_name'=>['label'=>'District', 'options'=>['placeholder'=>'District name']],

            'region_id' => ['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'Zone',
                'options' => [
                    'data' => ArrayHelper::map(\backend\modules\administration\models\Region::find()->all(), 'region_id', 'region_name'),
                    'options' => [
                        'prompt' => 'Select',

                    ],
                ],
            ],




            //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
        ]
    ]);*/
    ?>

    <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?php
        echo Html::resetButton('Reset', ['class'=>'btn btn-default']);
        echo Html::a("Cancel&nbsp;&nbsp;<span class='label label-warning'></span>", ['index'], ['class' => 'btn btn-warning']);

        ActiveForm::end();
        ?>
    </div>

</div>


