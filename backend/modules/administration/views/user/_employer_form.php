<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 3/21/19
 * Time: 10:30 AM
 */
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\PasswordInput;
use kartik\widgets\FileInput;
use yii\widgets\MaskedInput;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use backend\modules\administration\ADMINISTRATION;
use backend\modules\administration\models\UserCategory;
?>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-lg-12 control-label">Employer Name</label>
            <div class="col-lg-12">
                <?php
                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' =>1,
                    'attributes' => [

                        'employer_name'=>['label'=>false, 'options'=>['placeholder'=>'Employer Name']],

                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label class="col-lg-12 control-label">Short Name</label>
            <div class="col-lg-12">
                <?php
                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' =>1,
                    'attributes' => [

                        'short_name'=>['label'=>false, 'options'=>['placeholder'=>'Short Name']],


                        //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label class="col-lg-12 control-label">Vote Code</label>
            <div class="col-lg-12">
                <?php
                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' =>1,
                    'attributes' => [

                        'vote_number'=>['label'=>false, 'options'=>['placeholder'=>'Vote Code']],


                        //'comment'=>['label'=>'Comment', 'options'=>['placeholder'=>'Comment']],
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group col-md-12">
            <label class="col-lg-3 control-label">Employer Type</label>
            <div class="col-lg-9">
                <?php
                $instSQL = "SELECT learning_institution_id AS 'id', CONCAT(IFNULL(institution_name,''),' - ',IFNULL(institution_code,'')) AS 'name' FROM learning_institution WHERE institution_type IN('UNIVERSITY','COLLEGE') ORDER BY institution_name ASC;";
                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' =>1,
                    'attributes' => [

                        'employer_type_id' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            //'label' => 'Zone',
                            'label' => false,
                            'options' => [
                                'data' => ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql($instSQL)->asArray()->all(), 'id', 'name'),
                                'options' => [
                                    'prompt' => 'Select Institution',

                                ],
                            ],
                        ],

                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group col-md-12">
            <label class="col-lg-3 control-label">TIN</label>
            <div class="col-lg-9">
                <?php
                echo $form->field($model, 'phone_number')->widget(MaskedInput::class, [
                    'mask' => '0999 999 999',
                    'options'=>['placeholder'=>'0717 366 599','class'=>'form-control']
                ])->label(false);
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group col-md-12">
            <label class="col-lg-3 control-label">NATURE OF WORK</label>
            <div class="col-lg-9">
                <?php
                $instSQL = "SELECT learning_institution_id AS 'id', CONCAT(IFNULL(institution_name,''),' - ',IFNULL(institution_code,'')) AS 'name' FROM learning_institution WHERE institution_type IN('UNIVERSITY','COLLEGE') ORDER BY institution_name ASC;";
                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' =>1,
                    'attributes' => [

                        'nature_of_work_id' => ['type' => Form::INPUT_WIDGET,
                            'widgetClass' => \kartik\select2\Select2::className(),
                            //'label' => 'Zone',
                            'label' => false,
                            'options' => [
                                'data' => ArrayHelper::map(\backend\modules\allocation\models\LearningInstitution::findBySql($instSQL)->asArray()->all(), 'id', 'name'),
                                'options' => [
                                    'prompt' => 'Select Institution',

                                ],
                            ],
                        ],

                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
</div>
<div class="row">
    <div class="col-md-6"></div>
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
</div>

<div class="row">
    <div class="col-md-6"></div>
    <div class="col-md-6"></div>
</div>
