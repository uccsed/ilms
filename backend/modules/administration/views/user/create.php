<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\Region */

$this->title = 'Register New System user';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-create">
    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">

        </div>
    </div>
    <?= $this->render('_userRegistrationForm', [
        'model' => $model,
        'modelImport'=>$modelImport
    ]) ?>
</div>
