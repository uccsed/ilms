<?php

use yii\helpers\Html;
use yii\grid\GridView;
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel mdm\admin\models\searchs\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('rbac-admin', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
<div class="panel">
    <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div>
    <div class="panel-body">
        <?= Html::a(Yii::t('rbac-admin', 'Create New User'), ['create'], ['class' => 'btn btn-success']) ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'username',
                'email_address:email',
                'created_at:date',

                [
                    'attribute' => 'login_type',
                    'value' => function($model) {
                        return $model->userCategory->name;
                    },
                    'filter' => \yii\helpers\ArrayHelper::map(\backend\modules\administration\models\UserCategory::find()->asArray()->all(),'id','name')
                ],
                [
                    'attribute' => 'status',
                    'value' => function($model) {
                        return $model->status == 0 ? 'Inactive' : 'Active';
                    },
                    'filter' => [
                        0 => 'Inactive',
                        10 => 'Active'
                    ]
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => Helper::filterActionColumn(['view', 'activate', 'delete']),
                    'buttons' => [
                        'activate' => function($url, $model) {
                            if ($model->status == 10) {
                                return '';
                            }
                            $options = [
                                'title' => Yii::t('rbac-admin', 'Activate'),
                                'aria-label' => Yii::t('rbac-admin', 'Activate'),
                                'data-confirm' => Yii::t('rbac-admin', 'Are you sure you want to activate this user?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ];
                            return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, $options);
                        }
                    ]
                ],
            ],
        ]);
        ?>
    </div>
</div>



</div>
