<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administration\models\NatureOfWork */

$this->title = 'Update Nature Of Work: ' . $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Nature Of Works', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nature_of_work_id, 'url' => ['view', 'id' => $model->nature_of_work_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nature-of-work-update">
    <div class="panel">
        <div class="panel-heading bg-blue-gradient"><h3><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
