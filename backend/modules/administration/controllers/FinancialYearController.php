<?php

namespace backend\modules\administration\controllers;

use Yii;
use backend\modules\administration\models\FinancialYear;
use backend\modules\administration\models\FinancialYearSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\administration\ADMINISTRATION;
/**
 * FinancialYearController implements the CRUD actions for FinancialYear model.
 */
class FinancialYearController extends Controller
{
    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FinancialYear models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FinancialYearSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCloseYear()
    {
        if (Yii::$app->request->isAjax) {
            $id = $_POST['id'];
            $output = ADMINISTRATION::CloseFinancialYear($id);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('output' => $output),
            ];
        } else throw new BadRequestHttpException;
    }

    /**
     * Displays a single FinancialYear model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinancialYear model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FinancialYear();

        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs


            if ($model->save()) {
                // here for logs
                $new_data=\yii\helpers\Json::encode($model->attributes);
                $model_logs=\common\models\base\Logs::CreateLogall($model->financial_year_id,$old_data,$new_data,"financial_year","CREATE",1);
                //end for logs
                if ($model->is_active == 1) {
                    $SQL = "UPDATE financial_year SET is_active = '0', end_date = NOW() WHERE is_active = '1' AND financial_year_id <> '$model->financial_year_id'";
                    Yii::$app->db->createCommand($SQL)->execute();
                }
                return $this->redirect(['view', 'id' => $model->financial_year_id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FinancialYear model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs


            if ($model->save()) {
                // here for logs
                $new_data=\yii\helpers\Json::encode($model->attributes);
                $model_logs=\common\models\base\Logs::CreateLogall($model->financial_year_id,$old_data,$new_data,"financial_year","UPDATE",1);
                //end for logs
                if ($model->is_active == 1) {
                    $SQL = "UPDATE financial_year SET is_active = '0', end_date = NOW() WHERE is_active = '1' AND financial_year_id <> '$model->financial_year_id'";
                    Yii::$app->db->createCommand($SQL)->execute();
                }
                return $this->redirect(['view', 'id' => $model->financial_year_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FinancialYear model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       $model =  $this->findModel($id);
        // here for logs
        $old_data=\yii\helpers\Json::encode($model->oldAttributes);
        //end for logs
       $model->delete();
        // here for logs
        $new_data=\yii\helpers\Json::encode(array());
        $model_logs=\common\models\base\Logs::CreateLogall($model->financial_year_id,$old_data,$new_data,"financial_year","DELETE",1);
        return $this->redirect(['index']);
    }

    /**
     * Finds the FinancialYear model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinancialYear the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinancialYear::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
