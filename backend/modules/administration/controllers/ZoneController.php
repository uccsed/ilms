<?php

namespace backend\modules\administration\controllers;

use backend\modules\administration\models\Region;
use Yii;
use backend\modules\administration\models\Zone;
use backend\modules\administration\models\ZoneSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ZoneController implements the CRUD actions for Zone model.
 */
class ZoneController extends Controller
{
    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Zone models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ZoneSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Zone model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Zone model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Zone();

        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs

            $regionsArray = $_POST['regions'];
            if ($model->save()) {
                foreach ($regionsArray as $index => $regionID) {
                    $REGION = Region::findOne($regionID);
                    if (sizeof($REGION) != 0) {
                        $old_dataR = \yii\helpers\Json::encode($REGION->oldAttributes);
                        $REGION->zone_id = $model->id;
                        $REGION->save();
                        // here for logs
                        $new_dataR = \yii\helpers\Json::encode($REGION->attributes);
                        $model_logsR = \common\models\base\Logs::CreateLogall($REGION->region_id, $old_dataR, $new_dataR, "administration", "UPDATE", 1);
                        //end for logs

                    }
                }
                // here for logs
                $new_data = \yii\helpers\Json::encode($model->attributes);
                $model_logs = \common\models\base\Logs::CreateLogall($model->id, $old_data, $new_data, "administration", "CREATE", 1);
                //end for logs
                return $this->redirect(['view', 'id' => $model->id]);
                }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Zone model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs

            $regionsArray = $_POST['regions'];
            if ($model->save()) {
                foreach ($regionsArray as $index => $regionID) {
                    $REGION = Region::findOne($regionID);
                    if (sizeof($REGION) != 0) {
                        $old_dataR = \yii\helpers\Json::encode($REGION->oldAttributes);
                        $REGION->zone_id = $model->id;
                        $REGION->save();
                        // here for logs
                        $new_dataR = \yii\helpers\Json::encode($REGION->attributes);
                        $model_logs = \common\models\base\Logs::CreateLogall($REGION->region_id, $old_dataR, $new_dataR, "administration", "UPDATE", 1);
                        //end for logs

                    }
                }
                // here for logs
                $new_data = \yii\helpers\Json::encode($model->attributes);
                $model_logs = \common\models\base\Logs::CreateLogall($model->id, $old_data, $new_data, "administration", "UPDATE", 1);
                //end for logs
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Zone model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model  = $this->findModel($id);

        // here for logs
        $old_data=\yii\helpers\Json::encode($model->oldAttributes);
        //end for logs
        $model->delete();

        // here for logs
        $new_data=\yii\helpers\Json::encode(array());
        $model_logs=\common\models\base\Logs::CreateLogall($id,$old_data,$new_data,"zone","DELETE",1);
        //end for logs

        return $this->redirect(['index']);
    }

    /**
     * Finds the Zone model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Zone the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Zone::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
