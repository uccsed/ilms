<?php

namespace backend\modules\administration\controllers;

use Yii;
use backend\modules\administration\models\CompanySetup;
use backend\modules\administration\models\CompanySetupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * CompanySetupController implements the CRUD actions for CompanySetup model.
 */
class CompanySetupController extends Controller
{

    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionFilteredDistricts() {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        $out = [];
        $user=Yii::$app->user->id;

        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {

                if (!empty($parents[0])){
                    $region = $parents[0]; // Region ID




                    $SQL = "
                        SELECT 
                        district.district_id AS 'id', 
                        district.district_name AS 'name'
                        FROM district
                        WHERE district.region_id = '$region';
                ";



                    $out = Yii::$app->db->createCommand($SQL)->queryAll();
                    // the getSubCatList function will query the database based on the
                    // cat_id and return an array like below:
                    // [
                    //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                    //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                    // ]
                    echo Json::encode(['output'=>$out, 'selected'=>'']);
                    return;
                }

            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }



    /**
     * Lists all CompanySetup models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'model' => $this->findModel(1),
        ]);
    }

    /**
     * Displays a single CompanySetup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()
    {
        return $this->render('view', [
            'model' => $this->findModel(1),
        ]);
    }

    /**
     * Creates a new CompanySetup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = $this->findModel(1);

        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs

            if($model->save()){
                // here for logs
                $new_data=\yii\helpers\Json::encode($model->attributes);
                $model_logs=\common\models\base\Logs::CreateLogall($model->id,$old_data,$new_data,"company_setup","UPDATE",1);
                //end for logs
                return $this->redirect(['view', 'id' => $model->id]);
            }



        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CompanySetup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(1);

        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs

            if($model->save()){
                // here for logs
                $new_data=\yii\helpers\Json::encode($model->attributes);
                $model_logs=\common\models\base\Logs::CreateLogall($model->id,$old_data,$new_data,"company_setup","UPDATE",1);
                //end for logs
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CompanySetup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        //$this->findModel(1)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CompanySetup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanySetup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanySetup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
