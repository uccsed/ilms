<?php

namespace backend\modules\administration\controllers;

use Yii;
use backend\modules\administration\models\form\Login;
use backend\modules\administration\models\form\PasswordResetRequest;
use backend\modules\administration\models\form\ResetPassword;
use backend\modules\administration\models\form\Signup;
use backend\modules\administration\models\form\ChangePassword;
use backend\modules\administration\models\User;
use backend\modules\administration\models\searchs\User as UserSearch;
use backend\modules\disbursement\Module;
use backend\modules\administration\ADMINISTRATION;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\base\UserException;
use yii\mail\BaseMailer;

/**
 * User controller
 */
class UserController extends Controller
{

    public $layout = "main_private";
    private $_oldMailPath;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'actions' => ['signup', 'reset-password', 'login', 'request-password-reset'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                    [
//                        'actions' => ['logout', 'change-password', 'index', 'view', 'delete', 'activate'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'logout' => ['post'],
                    'activate' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (Yii::$app->has('mailer') && ($mailer = Yii::$app->getMailer()) instanceof BaseMailer) {
                /* @var $mailer BaseMailer */
                $this->_oldMailPath = $mailer->getViewPath();
                $mailer->setViewPath('@mdm/admin/mail');
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterAction($action, $result)
    {
        if ($this->_oldMailPath !== null) {
            Yii::$app->getMailer()->setViewPath($this->_oldMailPath);
        }
        return parent::afterAction($action, $result);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }



    public function actionCreate()
    {
        $model = new User();
        if (Yii::$app->request->isPost){


            $code = ADMINISTRATION::userCategoryInfo($model->login_type,'code');
            $categoryCode = str_replace(' ','_',$code);

            $fileName = strtoupper($categoryCode).'_'.time().'_'.$model->created_by;
            $fileDestination = '../attachments/system_users/';
            $savedName = Module::UploadFile($model,'photo',$fileName,$fileDestination);

            $user = Yii::$app->user->id;
            $now = date('Y-m-d H:i:s');
            $model->password_hash = '';
            $model->passport_photo=$savedName;
            $model->status = 0; //Inactive 10: Active, 0: Inactive
            $model->status_comment = 'Newly Registered user system user';
            $model->created_by = $user;
            $model->created_at = $now;
            $model->updated_at = $now;
            $model->updated_by = $user;
            $model->login_counts = 0;
            $model->last_login_date = null;
            $model->is_default_password = 1;
/*
            * @property integer $security_question_id
            * @property string $security_answer


            * @property string $date_password_changed
            * @property string $auth_key
            * @property string $password_reset_token
            * @property integer $activation_email_sent
            * @property integer $email_verified

*/
            $title = $model->title_id;
            $position = $model->staff_position_id;
            $institution = $model->learning_institution_id;
            //$type = $model->type;

            if ($model->save())
            {
                /*public $title_id;
                public $staff_position_id;
                public $learning_institution_id;
                public $type;*/
                $staffParams = array();
                $staffParams['user_id'] = $model->user_id;
                $staffParams['title_id'] = $title;
                $staffParams['staff_position_id'] = $position;
                $staffParams['learning_institution_id'] = $institution;


                //'Staff', 'TCU', 'HLI'

                //'Staff'=>5,
                // 'TCU'=>3,
                // 'HLI'=>4

                $employerParams = array();
                $employerParams['user_id']=$model->user_id;
                /*$model->employer_name = $params['employer_name'];
                $model->short_name = $params['short_name'];
                $model->employer_code = $params['employer_code'];
                $model->employer_type_id = $params['employer_type_id'];
                $model->TIN = $params['TIN'];
                $model->phone_number = $params['phone_number'];
                $model->fax_number = $params['fax_number'];
                $model->email_address = $params['email_address'];
                $model->postal_address = $params['postal_address'];
                $model->physical_address = $params['physical_address'];
                $model->ward_id = $params['ward_id'];
                $model->nature_of_work_id = $params['nature_of_work_id'];
                $model->created_at = $now;
                $model->created_by = $user;
                $model->vote_number = $params['vote_number'];*/



                switch($model->login_type){
                    case 3:
                        $staffParams['type'] = 'TCU';
                        ADMINISTRATION::REGISTER_STAFF($staffParams);
                        break;

                    case 4:
                        $staffParams['type'] = 'HLI';
                        ADMINISTRATION::REGISTER_STAFF($staffParams);
                        break;

                    case 5:
                        $staffParams['type'] = 'Staff';
                        ADMINISTRATION::REGISTER_STAFF($staffParams);
                        break;



                    case 1: //Applicant

                        break;


                    case 2: //Employer
                        ADMINISTRATION::REGISTER_EMPLOYER($employerParams);
                        /*
                         *
 * @property integer $user_id,
 * @property string $employer_name,
 * @property string $short_name,
 * @property string $employer_code,
 * @property integer $employer_type_id,
 * @property string $TIN,
 * @property string $phone_number,
 * @property string $fax_number,
 * @property string $email_address,
 * @property string $postal_address,
 * @property string $physical_address,
 * @property integer $ward_id,
 * @property integer $nature_of_work_id,
 * @property string $email_verification_code,
 * @property integer $verification_status,
 * @property string $rejection_reason,
 * @property string $rejection_date,
 * @property string $created_at,
 * @property integer $created_by,
 * @property integer $nature_of_work_other,
 * @property integer $salary_source,
 * @property string $vote_number,
 * @property integer $financial_year_id,
 * @property integer $academic_year_id
                        */


                        break;

                }

                $this->redirect(['view','id'=>$model->user_id]);
            }
        }

        return $this->render('create',[
            'model'=>$model,

        ]);
    }


    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->getUser()->logout();

        return $this->goHome();
    }

    /**
     * Signup new user
     * @return string
     */
    public function actionSignup()
    {
        $model = new Signup();
        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($user = $model->signup()) {
                return $this->goHome();
            }
        }

        return $this->render('signup', [
                'model' => $model,
        ]);
    }

    /**
     * Request reset password
     * @return string
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequest();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                'model' => $model,
        ]);
    }

    /**
     * Reset password
     * @return string
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPassword($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->getRequest()->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                'model' => $model,
        ]);
    }

    /**
     * Reset password
     * @return string
     */
    public function actionChangePassword()
    {
        $model = new ChangePassword();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->change()) {
            return $this->goHome();
        }

        return $this->render('change-password', [
                'model' => $model,
        ]);
    }

    /**
     * Activate new user
     * @param integer $id
     * @return type
     * @throws UserException
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        /* @var $user User */
        $user = $this->findModel($id);
        if ($user->status == User::STATUS_INACTIVE) {
            $user->status = User::STATUS_ACTIVE;
            if ($user->save()) {
                return $this->goHome();
            } else {
                $errors = $user->firstErrors;
                throw new UserException(reset($errors));
            }
        }
        return $this->goHome();
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
