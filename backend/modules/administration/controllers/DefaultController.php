<?php

namespace backend\modules\administration\controllers;

use yii\web\Controller;
use Yii;
/**
 * Default controller for the `administration` module
 */

use backend\modules\administration\ADMINISTRATION;
class DefaultController extends Controller
{

    public $layout = "main_private";
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        return $this->render('index');
    }

    public function actionSocial()
    {
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        //return $this->render('social');
        //return $this->render('socialEconomics');
        return $this->render('excelPrintOut');
       // return $this->render('clusterAllocation');
    }

    public function actionSocialEconomicsAnalysisPrintout()
    {

        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');

        $content=$this->render('printOut');
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        return $pdf->render();

    }


    public function actionDashboardStatistics(){
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {

            $results = array();


            $option = $_POST['option'];

            if (isset($_POST['date'])&&!empty($_POST['date'])){ $date = $_POST['date']; }else{ $date = null;}



            switch ($option)
            {
                case 'database':
                    $dbSize = ADMINISTRATION::dbSize('GB');
                    $results['size'] = $dbSize;
                    $results['count'] = ADMINISTRATION::dbTableCount();
                    $totalStorage = ADMINISTRATION::serverStorage('total','unit','GB');
                    $freeStorage = ADMINISTRATION::serverStorage('free','unit','GB');
                    $filesStorage = $totalStorage - ($freeStorage + $dbSize);
                    $dbPercent = 0;
                    if ($totalStorage!=0){
                        $dbPercent = ($dbSize/$totalStorage)*100;
                    }
                    $results['percent'] = $dbPercent;
                    $results['donutData'] = [
                        array('label'=>'Database Consumption','value'=>round($dbSize,2)),
                        array('label'=>'Files Consumption','value'=>round($filesStorage,2)),
                        array('label'=>'Free Space','value'=>round($freeStorage,2)),

                    ];
                    $results['donutColors'] = ["#3c8dbc", "#f56954", "#00a65a"];
                    break;

                case 'folder':
                    $totalStorage = ADMINISTRATION::serverStorage('total','unit','GB');
                    $freeStorage = ADMINISTRATION::serverStorage('free','unit','GB');

                    $dbSize = ADMINISTRATION::dbSize('GB');

                    //$folderSize = $totalStorage - $dbSize;
                    $folderSize = $totalStorage - ($freeStorage + $dbSize);



                    $results['size'] = $folderSize;
                    $results['count'] = ADMINISTRATION::CountFolder('/var/www/html/*');


                    $dbPercent = 0;
                    if ($totalStorage!=0){
                        $dbPercent = ($folderSize/$totalStorage)*100;
                    }
                    $results['percent'] = $dbPercent;
                    $results['donutData'] = [
                        array('label'=>'Database Consumption','value'=>round($dbSize,2)),
                        array('label'=>'File Consumption','value'=>round($folderSize,2)),
                        array('label'=>'Free Space','value'=>round($freeStorage,2)),

                    ];
                    //$results['donutColors'] = ["#3c8dbc", "#f56954", "#00a65a"];
                    //$results['donutColors'] = ["#ff851b", "#3c8dbc", "#00a65a"]; #, "#f56954", "#00a65a"];
                    $results['donutColors'] = [ "#3c8dbc", "#ff851b","#00a65a"]; #, "#f56954", "#00a65a"];
                    break;



                case 'user':
                   $totalUsers = ADMINISTRATION::USER_COUNT();

                    $todayUsers =ADMINISTRATION::USER_LOGIN_COUNT(date('Y-m-d'));

                    $results['size'] = $totalUsers;
                    $results['count'] = $todayUsers;


                    $userPercent = 0;
                    if ($totalUsers!=0){
                        $userPercent = ($totalUsers/$totalUsers)*100;
                    }
                    $results['percent'] = $userPercent;
                    $results['donutData'] = ADMINISTRATION::USER_CATEGORY_TABLE(null,'array');
                    $results['donutColors'] = [ "#3c8dbc", "#ff851b","#00a65a"]; #, "#f56954", "#00a65a"];
                    break;



                case 'cpu':
                    $totalCores = ADMINISTRATION::get_system_cores();
                    $cpuUsage = ADMINISTRATION::get_server_cpu_usage();


                    $results['size'] = $cpuUsage;
                    $results['count'] = $totalCores;


                    $memoryUsage =ADMINISTRATION::get_server_memory_usage();

                    $results['percent'] = $memoryUsage;
                    $freeMemory = 100 - $memoryUsage;

                    $results['donutData'] = [
                        array('label'=>'Memory Usage','value'=>round($memoryUsage,2)),
                       // array('label'=>'File Consumption','value'=>round($folderSize,2)),
                        array('label'=>'Free Memory','value'=>round($freeMemory,2)),

                    ];
                   // $results['donutColors'] = ["#ff851b","#00a65a"]; #, "#f56954", "#00a65a"];
                    $results['donutColors'] = ["#f56954","#00a65a"]; #, "#f56954", "#00a65a"];

                //"#ff851b", "#f56954"
                    break;

                case 'user_login':
                    $results['donutData'] = ADMINISTRATION::USER_CATEGORY_TABLE(date('Y-m-d'),'array');
                    $results['donutColors'] = [ "#3c8dbc", "#ff851b","#00a65a"]; #, "#f56954", "#00a65a"];
                    break;

                case 'system_users':
                    $results = ADMINISTRATION::USER_CATEGORY_TABLE(null,'table');;
                    break;

                    case 'recent_login':
                    $results = ADMINISTRATION::RECENT_USER(null);
                    break;

                case 'analytics':
                    $results = ADMINISTRATION::DashboardStatistics($date,$option);
                    break;

                default:
                    $results = ADMINISTRATION::DashboardStatistics($date,$option);
                    break;
            }



            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results' => $results),
            ];
        } else throw new BadRequestHttpException;
    }
}
