<?php

namespace backend\modules\administration\controllers;

use Yii;
use backend\modules\administration\models\Title;
use backend\modules\administration\models\TitleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TitleController implements the CRUD actions for Title model.
 */
class TitleController extends Controller
{

    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Title models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TitleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Title model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Title model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Title();
        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs


            if ($model->save()) {
                // here for logs
                $new_data=\yii\helpers\Json::encode($model->attributes);
                $model_logs=\common\models\base\Logs::CreateLogall($model->title_id,$old_data,$new_data,"title","CREATE",1);
                //end for logs

                return $this->redirect(['view', 'id' => $model->title_id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Title model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs


            if ($model->save()) {
                // here for logs
                $new_data=\yii\helpers\Json::encode($model->attributes);
                $model_logs=\common\models\base\Logs::CreateLogall($model->title_id,$old_data,$new_data,"title","UPDATE",1);
                //end for logs

                return $this->redirect(['view', 'id' => $model->title_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Title model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model  = $this->findModel($id);

        // here for logs
        $old_data=\yii\helpers\Json::encode($model->oldAttributes);
        //end for logs
        $model->delete();

        // here for logs
        $new_data=\yii\helpers\Json::encode(array());
        $model_logs=\common\models\base\Logs::CreateLogall($id,$old_data,$new_data,"title","DELETE",1);
        //end for logs

        return $this->redirect(['index']);
    }

    /**
     * Finds the Title model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Title the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Title::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
