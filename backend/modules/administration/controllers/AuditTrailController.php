<?php
/**
 * Created by PhpStorm.
 * User: obedy
 * Date: 3/15/19
 * Time: 7:18 AM
 */

namespace backend\modules\administration\controllers;
use backend\modules\administration\ADMINISTRATION;
use backend\modules\disbursement\Module;
use Yii;
use common\models\Logs;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\helpers\Json;
/**
 * AuditTrailController implements the Read/Preview & Search ONLY actions for Logs model.
 */
class AuditTrailController extends Controller
{

    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AcademicYear models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }



    public function actionAnalytics()
    {

        return $this->render('analytics');

    }

    public function actionAnalyticsData()
    {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
        if (Yii::$app->request->isAjax) {

            $results = array();


            $option = $_POST['option'];

            if (isset($_POST['month'])&&!empty($_POST['month'])){ $date = $_POST['month']; }else{ $date = null;}



            switch ($option)
            {

                case 'monthlyAnalytics':
                    $results = ADMINISTRATION::DashboardStatistics($date,$option);
                    break;

                case 'monthlyAreaChartData':
                    $results = ADMINISTRATION::DashboardStatistics($date,$option);
                    break;

            }



            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('results' => $results),
            ];
        } else throw new BadRequestHttpException;
    }






    public function actionUserFilter() {
        $out = [];
        $user=Yii::$app->user->id;
        ini_set('max_execution_time','-1');

        ini_set('memory_limit', '-1');
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {

                $loginType= $parents[0];

                $SQL = "
            SELECT user_id AS 'id', 
            CONCAT(IFNULL(firstname,''),' ', IFNULL(middlename,''),' ',IFNULL(surname,''),' (',username,')') AS 'name' 
            FROM user
            WHERE login_type = '$loginType'
            ORDER BY CONCAT(IFNULL(firstname,''),' ', IFNULL(middlename,''),' ',IFNULL(surname,'')) ASC
            
            LIMIT 500;";


                $out = Yii::$app->db->createCommand($SQL)->queryAll();
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionSearch(){
        if (Yii::$app->request->isAjax) { //Making sure its an ajax request
            $count = 0;
            $search = $_POST['search'];
            $login_type = $_POST['login_type'];
            $user_id = $_POST['user_id'];

            $start_date = $_POST['start_date'];
            $end_date = $_POST['end_date'];

            $action = $_POST['action'];
            $table = $_POST['table'];

            $params = array();

            $params['start_date']=$start_date;
            $params['end_date']=$end_date;

            $params['search']=$search;
            $params['login_type']=$login_type;
            $params['user_id']=$user_id;
            $params['action']=$action;
            $params['table']=$table;

            $model = ADMINISTRATION::AUDIT_TRAIL_QUERY($params);
            if (sizeof($model)!=0){
                $table = '
                            <table class="table table-condensed table-bordered table-striped">
                            <thead class="bg-blue-gradient">
                                <tr>
                                    <th>S/N</th>
                                    <th>DATE</th>
                                    <th>ACTION</th>
                                    <th>AFFECTED TABLE</th>
                                    <th>RECORD ID</th>
                                    <th>TRIGGER URL</th>
                                    <th>USER</th>
                                    <th>ACTIONS</th>
                                </tr>
                            </thead>
                            <tbody>
                          ';
                $tr = '';
                $count = 0;
                foreach ($model as $index=>$dataArray)
                {
                    $count++;
                    $id = $dataArray['id'];
                    $date = $dataArray['created_at'];
                    $ACTION = $dataArray['action'];
                    $affectedTable = $dataArray['affected_table'];
                    $recordID = $dataArray['record_id'];
                    $triggerUrl = $dataArray['trigger_url'];
                    $userID = $dataArray['created_by'];
                    $USER = Module::UserInfo($userID,'fullName');
                     $link= Yii::$app->urlManager->createUrl(['/administration/audit-trail/view','id'=>$id]);
                     $operations = '<a href="'.$link.'"><span>View Details</span></a>';
                    $tr.="
                        <tr>
                            <td>$count</td>
                            <td>$date</td>
                            <td>$ACTION</td>
                            <td>$affectedTable</td>
                            <td>$recordID</td>
                            <td>$triggerUrl</td>
                            <td>$USER</td>
                            <td>$operations</td>
                        </tr>";
                }
                $table.=$tr;
                $table.= '</tbody></table>';

                $results=$table;

            }else{
                $results = '<span class="fa fa-ban fa-2x text-red"></span> No Results Found!';
            }


            $output=array(
               'output'=>$results, 'count'=>$count
            );

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => $output,
            ];
        } else throw new BadRequestHttpException;
    }


    /**
     * Displays a single AcademicYear model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }





    /**
     * Finds the AcademicYear model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Logs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Logs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



}