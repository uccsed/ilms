<?php

namespace backend\modules\administration\controllers;

use backend\modules\administration\ADMINISTRATION;
use Yii;
use backend\modules\administration\models\AcademicYear;
use backend\modules\administration\models\AcademicYearSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;

/**
 * AcademicYearController implements the CRUD actions for AcademicYear model.
 */
class AcademicYearController extends Controller
{
    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AcademicYear models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AcademicYearSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    public function actionCloseYear()
    {
        if (Yii::$app->request->isAjax) {
        $id = $_POST['id'];
        $output = ADMINISTRATION::CloseAcademicYear($id);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'output' => array('output' => $output),
            ];
        } else throw new BadRequestHttpException;
    }

    /**
     * Displays a single AcademicYear model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AcademicYear model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AcademicYear();

        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs


        if ($model->save()) {
            // here for logs
            $new_data=\yii\helpers\Json::encode($model->attributes);
            $model_logs=\common\models\base\Logs::CreateLogall($model->academic_year_id,$old_data,$new_data,"academic_year","CREATE",1);
            //end for logs
            if ($model->is_current == 1) {
                $user = Yii::$app->user->id;
                $SQL = "UPDATE academic_year SET is_current = '0', closed_by = '$user', closed_date = NOW() WHERE is_current = '1' AND academic_year_id <> '$model->academic_year_id'";
                Yii::$app->db->createCommand($SQL)->execute();
            }
            return $this->redirect(['view', 'id' => $model->academic_year_id]);
        }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AcademicYear model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs


            if ($model->save()) {
                // here for logs
                $new_data=\yii\helpers\Json::encode($model->attributes);
                $model_logs=\common\models\base\Logs::CreateLogall($model->academic_year_id,$old_data,$new_data,"academic_year","UPDATE",1);
                //end for logs
                if ($model->is_current == 1) {
                    $user = Yii::$app->user->id;
                    $SQL = "UPDATE academic_year SET is_current = '0', closed_by = '$user', closed_date = NOW() WHERE is_current = '1' AND academic_year_id <> '$model->academic_year_id'";
                    Yii::$app->db->createCommand($SQL)->execute();
                }
                return $this->redirect(['view', 'id' => $model->academic_year_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AcademicYear model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        // here for logs
        $old_data=\yii\helpers\Json::encode($model->oldAttributes);
        //end for logs
        $model->delete();
        $new_data=\yii\helpers\Json::encode(array());
        $model_logs=\common\models\base\Logs::CreateLogall($id,$old_data,$new_data,"academic_year","DELETE",1);
        //end for logs
        return $this->redirect(['index']);
    }

    /**
     * Finds the AcademicYear model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AcademicYear the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AcademicYear::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
