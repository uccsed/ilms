<?php

namespace backend\modules\administration\controllers;

use Yii;
use backend\modules\administration\models\NatureOfWork;
use backend\modules\administration\models\NatureOfWorkSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NatureOfWorkController implements the CRUD actions for NatureOfWork model.
 */
class NatureOfWorkController extends Controller
{
    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NatureOfWork models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NatureOfWorkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NatureOfWork model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NatureOfWork model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NatureOfWork();

        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs


            if ($model->save()) {
                // here for logs
                $new_data=\yii\helpers\Json::encode($model->attributes);
                $model_logs=\common\models\base\Logs::CreateLogall($model->nature_of_work_id,$old_data,$new_data,"nature_of_work","CREATE",1);
                //end for logs

                return $this->redirect(['view', 'id' => $model->nature_of_work_id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing NatureOfWork model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs


            if ($model->save()) {
                // here for logs
                $new_data=\yii\helpers\Json::encode($model->attributes);
                $model_logs=\common\models\base\Logs::CreateLogall($model->nature_of_work_id,$old_data,$new_data,"nature_of_work","UPDATE",1);
                //end for logs

                return $this->redirect(['view', 'id' => $model->nature_of_work_id]);
            }

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing NatureOfWork model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model  = $this->findModel($id);

        // here for logs
        $old_data=\yii\helpers\Json::encode($model->oldAttributes);
        //end for logs
        $model->delete();

        // here for logs
        $new_data=\yii\helpers\Json::encode(array());
        $model_logs=\common\models\base\Logs::CreateLogall($id,$old_data,$new_data,"nature_of_work","DELETE",1);
        //end for logs

        return $this->redirect(['index']);
    }

    /**
     * Finds the NatureOfWork model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NatureOfWork the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NatureOfWork::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
