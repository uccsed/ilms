<?php

namespace backend\modules\administration\controllers;

use Yii;
use backend\modules\administration\models\OccupationCategory;
use backend\modules\administration\models\OccupationCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OccupationCategoryController implements the CRUD actions for OccupationCategory model.
 */
class OccupationCategoryController extends Controller
{

    public $layout = "main_private";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OccupationCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OccupationCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OccupationCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OccupationCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OccupationCategory();

        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs


            if ($model->save()) {
                // here for logs
                $new_data=\yii\helpers\Json::encode($model->attributes);
                $model_logs=\common\models\base\Logs::CreateLogall($model->occupation_category_id,$old_data,$new_data,"occupation_category","CREATE",1);
                //end for logs

                return $this->redirect(['view', 'id' => $model->occupation_category_id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OccupationCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            // here for logs
            $old_data=\yii\helpers\Json::encode($model->oldAttributes);
            //end for logs


            if ($model->save()) {
                // here for logs
                $new_data=\yii\helpers\Json::encode($model->attributes);
                $model_logs=\common\models\base\Logs::CreateLogall($model->occupation_category_id,$old_data,$new_data,"occupation_category","UPDATE",1);
                //end for logs

                return $this->redirect(['view', 'id' => $model->occupation_category_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OccupationCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model  = $this->findModel($id);

        // here for logs
        $old_data=\yii\helpers\Json::encode($model->oldAttributes);
        //end for logs
        $model->delete();

        // here for logs
        $new_data=\yii\helpers\Json::encode(array());
        $model_logs=\common\models\base\Logs::CreateLogall($id,$old_data,$new_data,"occupation_category","DELETE",1);
        //end for logs


        return $this->redirect(['index']);
    }

    /**
     * Finds the OccupationCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OccupationCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OccupationCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
