<?php

namespace backend\modules\administration\models;

use Yii;

/**
 * This is the model class for table "region".
 *
 * @property integer $region_id
 * @property integer $zone_id
 * @property string $region_name
 *
 * @property District[] $districts
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_name'], 'required'],
            [['zone_id'], 'safe'],
            [['region_name'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'region_id' => 'Region ID',
            'region_name' => 'Region Name',
            'zone_id' => 'Zone Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(District::className(), ['region_id' => 'region_id']);
    }

    public function getZone(){
        $output = '';
        $model = Zone::findOne($this->zone_id);
        if (sizeof($model)!=0){
            $output = $model->name;
        }
        return $output;
    }
}
