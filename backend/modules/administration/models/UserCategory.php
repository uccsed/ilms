<?php

namespace backend\modules\administration\models;

use Yii;

/**
 * This is the model class for table "user_login_type".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property integer $is_active
 * @property string $icon_tag
 */
class UserCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_login_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['description'], 'string'],
            [['is_active'], 'integer'],
            [['code'], 'string', 'max' => 20],
            [['name'], 'string', 'max' => 100],
            [['icon_tag'], 'string', 'max' => 200],
            [['code'], 'unique'],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'description' => 'Description',
            'is_active' => 'Is Active',
            'icon_tag' => 'Icon Tag',
        ];
    }
}
