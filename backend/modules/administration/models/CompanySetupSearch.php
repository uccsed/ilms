<?php

namespace backend\modules\administration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\administration\models\CompanySetup;

/**
 * CompanySetupSearch represents the model behind the search form about `backend\modules\administration\models\CompanySetup`.
 */
class CompanySetupSearch extends CompanySetup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nature_of_work', 'default_currency', 'hq_country', 'hq_region', 'hq_district', 'updated_by'], 'integer'],
            [['abbreviation', 'company_name', 'vote', 'sub_vote', 'TIN', 'physical_address', 'mailing_address', 'email_address', 'mobile_number', 'telephone_number', 'fax', 'logo', 'website', 'company_motto', 'report_footer', 'report_watermark', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanySetup::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nature_of_work' => $this->nature_of_work,
            'default_currency' => $this->default_currency,
            'hq_country' => $this->hq_country,
            'hq_region' => $this->hq_region,
            'hq_district' => $this->hq_district,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'abbreviation', $this->abbreviation])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'vote', $this->vote])
            ->andFilterWhere(['like', 'sub_vote', $this->sub_vote])
            ->andFilterWhere(['like', 'TIN', $this->TIN])
            ->andFilterWhere(['like', 'physical_address', $this->physical_address])
            ->andFilterWhere(['like', 'mailing_address', $this->mailing_address])
            ->andFilterWhere(['like', 'email_address', $this->email_address])
            ->andFilterWhere(['like', 'mobile_number', $this->mobile_number])
            ->andFilterWhere(['like', 'telephone_number', $this->telephone_number])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'company_motto', $this->company_motto])
            ->andFilterWhere(['like', 'report_footer', $this->report_footer])
            ->andFilterWhere(['like', 'report_watermark', $this->report_watermark]);

        return $dataProvider;
    }
}
