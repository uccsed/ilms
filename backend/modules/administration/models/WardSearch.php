<?php

namespace backend\modules\administration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\administration\models\Ward;

/**
 * WardSearch represents the model behind the search form about `backend\modules\administration\models\Ward`.
 */
class WardSearch extends Ward
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ward_id', 'district_id'], 'integer'],
            [['ward_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ward::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ward_id' => $this->ward_id,
            'district_id' => $this->district_id,
        ]);

        $query->andFilterWhere(['like', 'ward_name', $this->ward_name]);

        return $dataProvider;
    }

    public function SearchPerDistrict($params,$districtID)
    {
        $query = Ward::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andWhere(['district_id' => $districtID]);

        $query->andFilterWhere([
            'ward_id' => $this->ward_id,
            //'district_id' => $this->district_id,
        ]);

        $query->andFilterWhere(['like', 'ward_name', $this->ward_name]);

        return $dataProvider;
    }
}
