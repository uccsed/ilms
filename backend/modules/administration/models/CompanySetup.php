<?php

namespace backend\modules\administration\models;

use Yii;

/**
 * This is the model class for table "company_setup".
 *
 * @property integer $id
 * @property string $abbreviation
 * @property string $company_name
 * @property integer $nature_of_work
 * @property string $vote
 * @property string $sub_vote
 * @property string $TIN
 * @property integer $default_currency
 * @property string $physical_address
 * @property string $mailing_address
 * @property string $email_address
 * @property string $mobile_number
 * @property string $telephone_number
 * @property string $fax
 * @property string $logo
 * @property string $website
 * @property string $company_motto
 * @property integer $hq_country
 * @property integer $hq_region
 * @property integer $hq_district
 * @property string $report_footer
 * @property string $report_watermark
 * @property string $updated_at
 * @property integer $updated_by
 */
class CompanySetup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_setup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abbreviation', 'company_name', 'nature_of_work', 'default_currency', 'hq_country', 'hq_region', 'hq_district', 'updated_by'], 'required'],
            [['nature_of_work', 'default_currency', 'hq_country', 'hq_region', 'hq_district', 'updated_by'], 'integer'],
            [['physical_address', 'mailing_address', 'logo', 'company_motto', 'report_footer', 'report_watermark'], 'string'],
            [['updated_at'], 'safe'],
            [['abbreviation', 'vote', 'sub_vote', 'email_address', 'mobile_number', 'telephone_number', 'fax'], 'string', 'max' => 100],
            [['company_name', 'website'], 'string', 'max' => 200],
            [['TIN'], 'string', 'max' => 20],
            [['abbreviation'], 'unique'],
            [['company_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abbreviation' => 'Abbreviation',
            'company_name' => 'Company Name',
            'nature_of_work' => 'Nature Of Work',
            'vote' => 'Vote',
            'sub_vote' => 'Sub Vote',
            'TIN' => 'Tin',
            'default_currency' => 'Default Currency',
            'physical_address' => 'Physical Address',
            'mailing_address' => 'Mailing Address',
            'email_address' => 'Email Address',
            'mobile_number' => 'Mobile Number',
            'telephone_number' => 'Telephone Number',
            'fax' => 'Fax',
            'logo' => 'Logo',
            'website' => 'Website',
            'company_motto' => 'Company Motto',
            'hq_country' => 'Hq Country',
            'hq_region' => 'Hq Region',
            'hq_district' => 'Hq District',
            'report_footer' => 'Report Footer',
            'report_watermark' => 'Report Watermark',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry(){
        return $this->hasOne(Country::className(),['country_id'=>'hq_country']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion(){
        return $this->hasOne(Region::className(),['region_id'=>'hq_region']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict(){
        return $this->hasOne(District::className(),['district_id'=>'hq_district']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNatureOfWork(){
        return $this->hasOne(NatureOfWork::className(),['nature_of_work_id'=>'nature_of_work']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency(){
        return $this->hasOne(Currency::className(),['currency_id'=>'default_currency']);
    }






}
