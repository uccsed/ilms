<?php

namespace backend\modules\administration\models;

use Yii;

/**
 * This is the model class for table "financial_year".
 *
 * @property integer $financial_year_id
 * @property string $financial_year
 * @property integer $is_active
 * @property string $start_date
 * @property string $end_date
 */
class FinancialYear extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'financial_year';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['financial_year', 'start_date', 'end_date'], 'required'],
            [['is_active'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['financial_year'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'financial_year_id' => 'Financial Year ID',
            'financial_year' => 'Financial Year',
            'is_active' => 'Is Active',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
        ];
    }
}
