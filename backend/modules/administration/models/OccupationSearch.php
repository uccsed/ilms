<?php

namespace backend\modules\administration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\administration\models\Occupation;

/**
 * OccupationSearch represents the model behind the search form about `backend\modules\administration\models\Occupation`.
 */
class OccupationSearch extends Occupation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['occupation_id', 'occupation_category_id'], 'integer'],
            [['occupation_desc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Occupation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'occupation_id' => $this->occupation_id,
            'occupation_category_id' => $this->occupation_category_id,
        ]);

        $query->andFilterWhere(['like', 'occupation_desc', $this->occupation_desc]);

        return $dataProvider;
    }


    public function searchPerCategory($params,$categoryID)
    {
        $query = Occupation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'occupation_id' => $this->occupation_id,
            'occupation_category_id' => $categoryID,
        ]);

        $query->andFilterWhere(['like', 'occupation_desc', $this->occupation_desc]);

        return $dataProvider;
    }
}
