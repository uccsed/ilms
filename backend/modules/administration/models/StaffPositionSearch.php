<?php

namespace backend\modules\administration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\administration\models\StaffPosition;

/**
 * StaffPositionSearch represents the model behind the search form about `backend\modules\administration\models\StaffPosition`.
 */
class StaffPositionSearch extends StaffPosition
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['staff_position_id'], 'integer'],
            [['staff_position'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StaffPosition::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'staff_position_id' => $this->staff_position_id,
        ]);

        $query->andFilterWhere(['like', 'staff_position', $this->staff_position]);

        return $dataProvider;
    }
}
