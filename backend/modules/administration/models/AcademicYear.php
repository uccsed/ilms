<?php

namespace backend\modules\administration\models;

use Yii;

/**
 * This is the model class for table "academic_year".
 *
 * @property integer $academic_year_id
 * @property string $academic_year
 * @property integer $is_current
 * @property string $closed_date
 * @property integer $closed_by
 *
 * @property User $closedBy
 * @property AllocationBatch[] $allocationBatches
 * @property AllocationFeeFactor[] $allocationFeeFactors
 * @property AllocationMovementSchedule[] $allocationMovementSchedules
 * @property AllocationPriority[] $allocationPriorities
 * @property Application[] $applications
 * @property Content[] $contents
 * @property CriteriaField[] $criteriaFields
 * @property CriteriaQuestion[] $criteriaQuestions
 * @property DisbursementSetting[] $disbursementSettings
 * @property DisbursementSetting2[] $disbursementSetting2s
 * @property SystemSetting12[] $systemSetting12s
 */
class AcademicYear extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'academic_year';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['academic_year'], 'required'],
            [['is_current', 'closed_by'], 'integer'],
            [['closed_date'], 'safe'],
            [['academic_year'], 'string', 'max' => 10],
            [['closed_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['closed_by' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'academic_year_id' => 'Academic Year ID',
            'academic_year' => 'Academic Year',
            'is_current' => 'Is Current',
            'closed_date' => 'Closed Date',
            'closed_by' => 'Closed By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClosedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'closed_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationBatches()
    {
        return $this->hasMany(AllocationBatch::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationFeeFactors()
    {
        return $this->hasMany(AllocationFeeFactor::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationMovementSchedules()
    {
        return $this->hasMany(AllocationMovementSchedule::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationPriorities()
    {
        return $this->hasMany(AllocationPriority::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(Content::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCriteriaFields()
    {
        return $this->hasMany(CriteriaField::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCriteriaQuestions()
    {
        return $this->hasMany(CriteriaQuestion::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisbursementSettings()
    {
        return $this->hasMany(DisbursementSetting::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisbursementSetting2s()
    {
        return $this->hasMany(DisbursementSetting2::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSystemSetting12s()
    {
        return $this->hasMany(SystemSetting12::className(), ['academic_year_id' => 'academic_year_id']);
    }
}
