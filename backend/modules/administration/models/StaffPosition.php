<?php

namespace backend\modules\administration\models;

use Yii;

/**
 * This is the model class for table "staff_position".
 *
 * @property integer $staff_position_id
 * @property string $staff_position
 *
 * @property Staff[] $staff
 */
class StaffPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'staff_position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['staff_position'], 'required'],
            [['staff_position'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'staff_position_id' => 'Staff Position ID',
            'staff_position' => 'Staff Position',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasMany(Staff::className(), ['staff_position_id' => 'staff_position_id']);
    }
}
