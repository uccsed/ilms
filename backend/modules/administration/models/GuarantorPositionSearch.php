<?php

namespace backend\modules\administration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\administration\models\GuarantorPosition;

/**
 * GuarantorPositionSearch represents the model behind the search form about `backend\modules\administration\models\GuarantorPosition`.
 */
class GuarantorPositionSearch extends GuarantorPosition
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['guarantor_position_id', 'status'], 'integer'],
            [['guarantor_position'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GuarantorPosition::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'guarantor_position_id' => $this->guarantor_position_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'guarantor_position', $this->guarantor_position]);

        return $dataProvider;
    }
}
