<?php

namespace backend\modules\administration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\administration\models\OccupationCategory;

/**
 * OccupationCategorySearch represents the model behind the search form about `backend\modules\administration\models\OccupationCategory`.
 */
class OccupationCategorySearch extends OccupationCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['occupation_category_id'], 'integer'],
            [['category_desc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OccupationCategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'occupation_category_id' => $this->occupation_category_id,
        ]);

        $query->andFilterWhere(['like', 'category_desc', $this->category_desc]);

        return $dataProvider;
    }
}
