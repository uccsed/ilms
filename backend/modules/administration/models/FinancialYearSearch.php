<?php

namespace backend\modules\administration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\administration\models\FinancialYear;

/**
 * FinancialYearSearch represents the model behind the search form about `backend\modules\administration\models\FinancialYear`.
 */
class FinancialYearSearch extends FinancialYear
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['financial_year_id', 'is_active'], 'integer'],
            [['financial_year', 'start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinancialYear::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'financial_year_id' => $this->financial_year_id,
            'is_active' => $this->is_active,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
        ]);

        $query->andFilterWhere(['like', 'financial_year', $this->financial_year]);

        return $dataProvider;
    }
}
