<?php

namespace backend\modules\administration\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property integer $currency_id
 * @property string $currency_ref
 * @property string $currency_postfix
 * @property string $currency_desc
 * @property double $ex_rate
 * @property double $lowest_coin
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property Allocation[] $allocations
 * @property BankAccount[] $bankAccounts
 * @property User $createdBy
 * @property User $updatedBy
 * @property ProgrammeCost[] $programmeCosts
 * @property SystemSetting12[] $systemSetting12s
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currency_ref', 'currency_postfix', 'ex_rate', 'created_at'], 'required'],
            [['ex_rate', 'lowest_coin'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['currency_ref'], 'string', 'max' => 3],
            [['currency_postfix', 'currency_desc'], 'string', 'max' => 45],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency_id' => 'Currency ID',
            'currency_ref' => 'Currency Ref',
            'currency_postfix' => 'Currency Postfix',
            'currency_desc' => 'Currency Desc',
            'ex_rate' => 'Ex Rate',
            'lowest_coin' => 'Lowest Coin',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocations()
    {
        return $this->hasMany(Allocation::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankAccounts()
    {
        return $this->hasMany(BankAccount::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgrammeCosts()
    {
        return $this->hasMany(ProgrammeCost::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSystemSetting12s()
    {
        return $this->hasMany(SystemSetting12::className(), ['currency_id' => 'currency_id']);
    }
}
