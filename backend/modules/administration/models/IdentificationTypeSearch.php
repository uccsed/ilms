<?php

namespace backend\modules\administration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\administration\models\IdentificationType;

/**
 * IdentificationTypeSearch represents the model behind the search form about `backend\modules\administration\models\IdentificationType`.
 */
class IdentificationTypeSearch extends IdentificationType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['identification_type_id', 'status'], 'integer'],
            [['identification_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IdentificationType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'identification_type_id' => $this->identification_type_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'identification_type', $this->identification_type]);

        return $dataProvider;
    }
}
