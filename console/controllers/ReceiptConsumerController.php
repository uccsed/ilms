<?php

namespace console\controllers;

use Yii;
use common\components\Controller;
use yii\console\Controller;
use yii\filters\VerbFilter;
use PhpAmqpLib\Connection\AMQPStreamConnection;
date_default_timezone_set('Africa/Nairobi');
/**
 * ApplicantController implements the CRUD actions for Applicant model.
 */
class ReceiptConsumerController extends Controller {


    
    public function actionIndex()
    {

        $connection = new AMQPStreamConnection('41.59.225.155', 5672, 'admin', '0lams@2018?ucc');
        
        $channel = $connection->channel();
        
        $channel->queue_declare('GePGReceiptQueue', false, true, false, false);
        
        //echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";
        
        $callback = function($msg){
        
            //echo $msg->body;
$orgncontent=$msg->body;
            
            $array = json_decode($msg->body, true);
          
            try{
                $this->actionPostReceipt($array,$orgncontent);
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            } catch (\Exception $ex){
                echo $ex->getMessage();
            }
        };
        
        //$channel->basic_qos(null, 1, null);
        
        $channel->basic_consume('GePGReceiptQueue', '', false, false, false, false, $callback);
        
        while(count($channel->callbacks)) {
            $channel->wait();
        }
        
        $channel->close();
        
        $connection->close();
    }

    public function actionPostReceipt($xml,$orgncontent) {
         
        $bill_number = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['BillId'];
        $bill_number = $bill_number[0];
        $amount = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['BillAmt'];
        $amount = $amount[0];

        $receipt_number = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['PayRefId'];
        $receipt_number = $receipt_number[0];

        $transaction_date = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['TrxDtTm'];
        $transaction_date = $transaction_date[0];

        $payment_setup = \backend\modules\application\models\Application::findOne(['bill_number' => $bill_number]);
        $payment_setup->receipt_number =  $receipt_number;
        $payment_setup->date_receipt_received = date('Y-m-d'.'\T'.'H:i:s');
        $payment_setup->save();


 
       $content = $orgncontent;      
        $transaction_id = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['TrxId'];
		$transaction_id=$transaction_id[0];
        $bill_amount = $amount;          
        $paid_amount = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['PaidAmt'];
		$paid_amount=$paid_amount[0];
		$control_number = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['PayCtrNum'];
		$control_number=$control_number[0];
		//new added
		$paymentChanelUsed = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['UsdPayChnl'];
		if(!empty($paymentChanelUsed)){
		$paymentChanelUsed=$paymentChanelUsed[0];
        }else{
		$paymentChanelUsed=NULL;
		}		
		$payerPhoneNumber = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['PyrCellNum'];
		if(!empty($payerPhoneNumber)){
		$payerPhoneNumber=$payerPhoneNumber[0];	
        }else{
		$payerPhoneNumber=NULL;
		}		
		$payerName = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['PyrName'];	
		if(!empty($payerName)){
		$payerName=$payerName[0];
		}else{
		$payerName=NULL;
		}
		
		$paymentReceiptPservProv = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['PspReceiptNumber'];
		if(!empty($paymentReceiptPservProv)){
		$paymentReceiptPservProv=$paymentReceiptPservProv[0];
		}else{
		$paymentReceiptPservProv=NULL;
		}				
		$paymServProvName = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['PspName'];
		if(!empty($paymServProvName)){
		$paymServProvName=$paymServProvName[0];
		}else{
		$paymServProvName=NULL;
		}
		$creditedAccount = (array)$xml['gepgPmtSpInfo']['PymtTrxInf']['CtrAccNum'];
		if(!empty($creditedAccount)){
		$creditedAccount=$creditedAccount[0];
		}else{
		$creditedAccount=NULL;
		}	

		//end new added

$queryGetAppID = " SELECT application_id,amount_paid,applicant_id FROM application WHERE  bill_number='".$bill_number."'";
                    $ResultApplicationID = Yii::$app->db->createCommand($queryGetAppID)->queryOne();
					$finalAppID=$ResultApplicationID['application_id'];
                                        $billAmount=$ResultApplicationID['amount_paid'];
                                       
           /*
            $query = "insert into gepg_receipt(bill_number, response_message,retrieved,trans_id,payer_ref_id,control_number,bill_amount,paid_amount,currency,trans_date,payer_phone,payer_name,receipt_number,account_number,application_id,transact_date_gepg) values "
                    . "('{$bill_number}','',0,'{$transaction_id}','','{$control_number}','{$bill_amount}','{$paid_amount}','','".date('Y-m-d H:i:s')."','','','{$receipt_number}','','{$finalAppID}','{$transaction_date}')";           
           */
		   
		   $query = "insert into gepg_receipt(bill_number, response_message,retrieved,trans_id,payer_ref_id,control_number,bill_amount,paid_amount,currency,trans_date,payer_phone,payer_name,receipt_number,account_number,application_id,transact_date_gepg,paymentChanelUsed,paymentReceiptPservProv,paymServProvName) values "
                    . "('{$bill_number}','',0,'{$transaction_id}','','{$control_number}','{$bill_amount}','{$paid_amount}','','".date('Y-m-d H:i:s')."','{$payerPhoneNumber}','{$payerName}','{$receipt_number}','{$creditedAccount}','{$finalAppID}','{$transaction_date}','{$paymentChanelUsed}','{$paymentReceiptPservProv}','{$paymServProvName}')";
		   
            Yii::$app->db->createCommand($query)->execute();

            $query3Bill = "UPDATE gepg_bill SET status='3' WHERE bill_number='".$bill_number."'";
             Yii::$app->db->createCommand($query3Bill)->execute();
             
                  if(strcmp($paid_amount,$billAmount)==0){			 
			 $applicationFeePaid = "UPDATE application SET payment_status='1' WHERE bill_number='".$bill_number."'";
                         Yii::$app->db->createCommand($applicationFeePaid)->execute();
                                 ###################### mickidadimsoka@gmail.com ####################
                                       $applicant_Id=$ResultApplicationID['applicant_id'];
                                       $modeluser=  \frontend\modules\application\models\Applicant::findOne($applicant_Id);
                Yii::$app->db->createCommand("update auth_assignment  set item_name='applicant_only' WHERE user_id='{$modeluser->user_id}'")->execute();           
                                  ################# create applicant role #########
                             
                  }

        
        

    }
    public function getDataString($inputstr,$datatag){
	$datastartpos = strpos($inputstr, $datatag);
	$dataendpos = strrpos($inputstr, $datatag);
	$data=substr($inputstr,$datastartpos - 1,$dataendpos + strlen($datatag)+2 - $datastartpos);
	return $data;
    }

}
