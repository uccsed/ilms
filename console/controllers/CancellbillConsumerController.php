<?php

namespace console\controllers;

use Yii;
use common\components\Controller;
use yii\console\Controller;
use yii\filters\VerbFilter;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use frontend\modules\application\rabbit\Producer;
use backend\modules\application\models\Application;

date_default_timezone_set('Africa/Nairobi');
/**
 * ApplicantController implements the CRUD actions for Applicant model.
 */
class CancellbillConsumerController extends Controller {
               

      public function actionIndex() {

        $query1 = " SELECT bill_number,id FROM gepg_bill WHERE  status=1 AND cancel_requsted_status=1";
        $Result1 = Yii::$app->db->createCommand($query1)->queryOne();
               
			$reconMasterID = $Result1['id'];
			$BillId1 = $Result1['bill_number'];
			$SpCode='SP111';
			$SpSysId='LHESLB001';
            			//echo $SpReconcReqId;
			$dataToQueue = ["SpCode" => $SpCode, 
                                     "SpSysId"=>$SpSysId, 
                                     "BillId1"=>$BillId1]; 
                                     
            if($reconMasterID >0){
            Producer::queue("GePGBillCancellationRequestQueue", $dataToQueue);
			
			$query3 = "UPDATE gepg_bill SET cancel_requsted_status='0' WHERE id='".$reconMasterID."'";
             Yii::$app->db->createCommand($query3)->execute();
			 }
        
    }

}

