<?php

namespace console\controllers;

use Yii;
use common\components\Controller;
use yii\console\Controller;
use yii\filters\VerbFilter;
use PhpAmqpLib\Connection\AMQPStreamConnection;
/**
 * ApplicantController implements the CRUD actions for Applicant model.
 */

date_default_timezone_set('Africa/Nairobi');

class BillSubmitionConsumerController extends Controller {


    
    public function actionIndex()
    {

        $connection = new AMQPStreamConnection('41.59.225.155', 5672, 'admin', '0lams@2018?ucc');
        
        $channel = $connection->channel();
        
        $channel->queue_declare('GePGBillSubmitionQueue', false, true, false, false);
        
        echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";
        
        $callback = function($msg){
        
            echo $msg->body;
            
            $array = json_decode($msg->body, true);
          
            $bill_number = $array['billNumber'];
            $amount = $array['amount'];
            $name = $array['name'];
            $phone_number = $array['phone_number'];
            $email = $array['email'];
            $applicantID = $array['applicantID'];
            $indexNumber = $array['indexNumber'];
          
            try{
                $this->submitBill($bill_number, $amount, $name, $phone_number, $email, $applicantID, $indexNumber);
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            }  catch (\Exception $ex){
                echo $ex->getMessage();
            }
            
        };
        
        //$channel->basic_qos(null, 1, null);
        
        $channel->basic_consume('GePGBillSubmitionQueue', '', false, false, false, false, $callback);
        
        while(count($channel->callbacks)) {
            $channel->wait();
        }
        
        $channel->close();
        
        $connection->close();
    }

    
    public function submitBill($bill_number=NULL, $amount=NULL, $name=NULL, $phone_number=NULL, $email=NULL, $applicantID=NULL, $indexNumber=NULL) 
    {
        /*
        if(empty($bill_number)){
         throw new exception("Invalid bill number provided");
        }
        
        if(empty($amount)){
         throw new exception("Invalid amount provided");
        }
        
        if(empty($name)){
         throw new exception("Invalid applicant name");
        }
        
        if(empty($phone_number)){
         throw new exception("Invalid phone number provided");
        }
        
        if(empty($email)){
         throw new exception("Invalid email provided");
        }
        
        if(empty($applicantID)){
         throw new exception("Invalid applicant ID provided");
        }
    
        if(empty($indexNumber)){
         throw new exception("Invalid f4indexno provided");
        }*/
        
        if (!$cert_store = file_get_contents("/var/www/html/olams/frontend/web/sign/heslbolams.pfx")) {
          echo "Error: Unable to read the cert file\n".\Yii::getAlias('@webroot');
          exit;
        }
        else
        {
            
            if (openssl_pkcs12_read($cert_store, $cert_info, "heslbolams"))
            {

                $Billno =$bill_number;
                $days = 90;
                $bill_amount = $amount;

                $bill_desc = "Application Fees Payment";
                $expire_date = Date('Y-m-d'.'\T'.'h:i:s', strtotime("+$days days"));

                $bill_gen_date = date('Y-m-d'.'\T'.'h:i:s');
                
                //Bill Request

                $name=trim($name);
				$phone_number=trim($phone_number);
				//$email=trim($email);
				$email="";

                $content ="<gepgBillSubReq>".
                    "<BillHdr>".
                        "<SpCode>SP111</SpCode>".
                        "<RtrRespFlg>true</RtrRespFlg>".
                    "</BillHdr>".
                    "<BillTrxInf>".
                        "<BillId>".$Billno."</BillId>".
                        "<SubSpCode>7001</SubSpCode>".
                        "<SpSysId>LHESLB001</SpSysId>".
                        "<BillAmt>".$bill_amount."</BillAmt>".
                        "<MiscAmt>0</MiscAmt>".
                        "<BillExprDt>".$expire_date."</BillExprDt>".
                        "<PyrId>".$applicantID."</PyrId>".
                        "<PyrName>".$name."</PyrName>".
                        "<BillDesc>".$bill_desc."</BillDesc>".
                        "<BillGenDt>".$bill_gen_date."</BillGenDt>".
                        "<BillGenBy>".$applicantID."</BillGenBy>".
                        "<BillApprBy>".$name."</BillApprBy>".
                                        "<PyrCellNum>".$phone_number."</PyrCellNum>".
                                        "<PyrEmail>".$email."</PyrEmail>".
                        "<Ccy>TZS</Ccy>".
                        "<BillEqvAmt>".$bill_amount."</BillEqvAmt>".
                        "<RemFlag>false</RemFlag>".
                       "<BillPayOpt>3</BillPayOpt>".
                        "<BillItems>".
                           "<BillItem>".
                                "<BillItemRef>".$Billno."</BillItemRef>".
                                "<UseItemRefOnPay>N</UseItemRefOnPay>".
                                "<BillItemAmt>".$bill_amount."</BillItemAmt>".
                               "<BillItemEqvAmt>".$bill_amount."</BillItemEqvAmt>".
                               "<BillItemMiscAmt>0</BillItemMiscAmt>".
                               "<GfsCode>140313</GfsCode>".
                            "</BillItem>".
                        "</BillItems>".
                    "</BillTrxInf>".
                "</gepgBillSubReq>";

             //create signature
            openssl_sign($content, $signature, $cert_info['pkey'], "sha1WithRSAEncryption");

          //  $sign_bill = fluidxml(false);



            $signature = base64_encode($signature);  //output crypted data base64 encoded

            //    $sign_bill->add('Gepg', true)
            //              ->add($content)
            //              ->add('gepgSignature', $signature);

            //Compose xml request
             $data = "<Gepg>".$content."<gepgSignature>".$signature."</gepgSignature></Gepg>";
            
            //  $data = $sign_bill->xml(true);

            // echo "<pre>";
            //         var_dump($data);
            //   echo "<pre>";    
            //     die;
             
            $resultCurlPost = "";
            //$serverIp = "http://154.118.230.18";
            $serverIp = "http://154.118.230.202";

            $uri = "/api/bill/sigqrequest"; //this is for qrequest

            $data_string = $data;
            //      echo "Message ready to GePG:"."\n".$data_string."\n";

            $ch = curl_init($serverIp.$uri);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                                'Content-Type:application/xml',
                                                'Gepg-Com:default.sp.in',
                                                'Gepg-Code:SP111',
                                                'Content-Length:'.strlen($data_string))
            );

            curl_setopt($ch, CURLOPT_TIMEOUT, 50);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50);

            //Capture returned content from GePG
            $resultCurlPost = curl_exec($ch);
            curl_close($ch);
            //$resultCurlPost=$data;


$queryq4 = "insert into gepg_bill4(response_message,date_created) value "
                . "('{$resultCurlPost}','".date('Y-m-d H:i:s')."')";
            Yii::$app->db->createCommand($queryq4)->execute();

            
            if(!empty($resultCurlPost)){

                ##to get content to insert in the gepg_bill table
		$datatag = "gepgBillSubReqAck";
		$sigtag = "gepgSignature";
		$vdata = self::getDataString($resultCurlPost,$datatag);
		$vsignature = self::getSignatureString($resultCurlPost,$sigtag);
		
		$dataContents="Data Received:".$vdata."Signature Received:".$vsignature;
		
		if (!$pcert_store = file_get_contents("/var/www/html/olams/frontend/web/sign/gepgpubliccertificatetoclients.pfx")) {
			//echo "Error: Unable to read the cert file\n";
			//exit;
			$getConFileFailed="Error: Unable to read the cert file";
		}else{

			//Read Certificate
			if (openssl_pkcs12_read($pcert_store,$pcert_info,"gepg@2018")) {
				//Decode Received Signature String
				$rawsignature = base64_decode($vsignature);

				//Verify Signature and state whether signature is okay or not
				$ok = openssl_verify($vdata, $rawsignature, $pcert_info['extracerts']['0']);
				if ($ok == 1) {
					//echo "\n\nSignature Status:";
				    //echo "GOOD";
					$getConFileFailed1="Signature Status:GOOD";
				} elseif ($ok == 0) {
					//echo "\n\nSignature Status:";
				    //echo "BAD";
					$getConFileFailed1="Signature Status:BAD";
				} else {
					//echo "\n\nSignature Status:";
				    //echo "UGLY, Error checking signature";
					$getConFileFailed1="Signature Status:UGLY, Error checking signature";
				}
				$getConFileFailed=$getConFileFailed1;
			}  
		}
                    //$response_message="Received Response".$resultCurlPost.$dataContents.$getConFileFailed;
                    //$bill_request='hthtyjytjyjyuj';
                    $bill_request="Received Response".$dataContents.$getConFileFailed;
                    //$vValuesa='kjnrger';
		
                    echo $bill_request."\n\n";

$queryGetAppID = " SELECT application_id FROM application WHERE  bill_number='".$Billno."'";
                    $ResultApplicationID = Yii::$app->db->createCommand($queryGetAppID)->queryOne();
					$finalAppID=$ResultApplicationID['application_id'];
/*
$queryq7e = "insert into gepg_bill7(response_message,date_created) value "
                . "('{$content}','".date('Y-m-d H:i:s')."')";
            Yii::$app->db->createCommand($queryq7e)->execute();
*/			
			
			$date_createdsc=date("Y-m-d H:i:s");
			Yii::$app->db->createCommand()
        ->insert('gepg_bill7', [
        'response_message' =>$content,
        'date_created' =>$date_createdsc,   
        ])->execute();

                    
        $query = "insert into gepg_bill(bill_number, bill_request,retry,status,response_message,date_created,application_id) value "
                . "('{$Billno}','',0,0,'{$bill_request}','".date('Y-m-d H:i:s')."','{$finalAppID}')";
            Yii::$app->db->createCommand($query)->execute();
  //echo $bill_request;
			
		##to get content to insert in the gepg_bill table

       return true;

        /*
		echo "\n\n";
		echo "Received Response\n";
		echo  $resultCurlPost;
		echo "\n";		

		//Tag for respose
		$datatag = "gepgBillSubReqAck";
		$sigtag = "gepgSignature";
               // $controltag = "PayCntrNum";

		$vdata = self::getDataString($resultCurlPost,$datatag);
		$vsignature = self::getSignatureString($resultCurlPost,$sigtag);
                //$vcontrolNo = self::getDataString($resultCurlPost,$controltag);

		echo "\n\n";
		echo "Data Received:\n";
		echo $vdata;
                echo "\n\n";
//		echo "Control #:\n";
//		echo $vcontrolNo;
		echo "\n\n";
		echo "Signature Received:\n";
		echo $vsignature;
		echo "\n";

		if (!$pcert_store = file_get_contents("/var/www/html/olams/frontend/web/sign/gepgpubliccertificate.pfx")) {
			echo "Error: Unable to read the cert file\n";
			exit;
		}else{

			//Read Certificate
			if (openssl_pkcs12_read($pcert_store,$pcert_info,"passpass")) {
				//Decode Received Signature String
				$rawsignature = base64_decode($vsignature);

				//Verify Signature and state whether signature is okay or not
				$ok = openssl_verify($vdata, $rawsignature, $pcert_info['extracerts']['0']);
				if ($ok == 1) {
					echo "\n\nSignature Status:";
				    echo "GOOD";
				} elseif ($ok == 0) {
					echo "\n\nSignature Status:";
				    echo "BAD";
				} else {
					echo "\n\nSignature Status:";
				    echo "UGLY, Error checking signature";
				}
			}  
		}
		*/
  }
        else
        {
                //echo "No result Returned"."\n";
            return false;
        }

        }
        else
        {

    echo "Error: Unable to read the cert store.\n";
    exit;
        }

}
    }

    
      public function getDataString($inputstr,$datatag){
	$datastartpos = strpos($inputstr, $datatag);
	$dataendpos = strrpos($inputstr, $datatag);
	$data=substr($inputstr,$datastartpos - 1,$dataendpos + strlen($datatag)+2 - $datastartpos);
	return $data;
}

public function getSignatureString($inputstr,$sigtag){
	$sigstartpos = strpos($inputstr, $sigtag);
	$sigendpos = strrpos($inputstr, $sigtag);
	$signature=substr($inputstr,$sigstartpos + strlen($sigtag)+1,$sigendpos - $sigstartpos -strlen($sigtag)-3);
	return $signature;
}
  
}
