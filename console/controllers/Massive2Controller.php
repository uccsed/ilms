<?php

namespace console\controllers;

use Yii;
use common\components\Controller;
use yii\console\Controller;
use yii\filters\VerbFilter;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use frontend\modules\application\rabbit\Producer;
use backend\modules\application\models\Application;
date_default_timezone_set('Africa/Nairobi');
/**
 * ApplicantController implements the CRUD actions for Applicant model.
 */
class Massive2Controller extends Controller {
               

      public function actionIndex() {

        $query1 = " SELECT applicant_id FROM applicant WHERE applicant_id >'3221'";
        $Results1 = Yii::$app->db->createCommand($query1)->queryAll();
               
       
      
        foreach ($Results1 as $Result1) {
            $applicantID = $Result1['applicant_id'];
			$to_time = strtotime(date("Y-m-d H:i:s"));			
			$query = "insert into application(applicant_id,academic_year_id) value "
                . "('{$applicantID}','1')";
            Yii::$app->db->createCommand($query)->execute();
			
			## save bill number and request control number
			                        $queryUO = " SELECT application_id FROM application order by application_id DESC limit 1";
                                    $ResultsUO = Yii::$app->db->createCommand($queryUO)->queryOne();
									$applicationID=$ResultsUO['application_id'];
									
			                        $applicatDetails=Application::getApplicantDetails($applicantID);
									$bill_number="HESLB_".$applicationID;
									$amount=30000;
									
									$name=$applicatDetails->user->firstname." ".$applicatDetails->user->middlename." ".$applicatDetails->user->surname;
									$phone_number=$applicatDetails->user->phone_number;
									$email=$applicatDetails->user->email_address;
									$applicantID=$applicatDetails->applicant_id;
									$indexNumber=$applicatDetails->f4indexno;
									Application::saveBillNumber($applicationID,$bill_number,$amount);
                                                                        
                                      $dataToQueue = ['billNumber' => $bill_number, 
                                                      "amount"=>$amount, 
                                                      "name"=>$name, 
                                                      "phone_number"=>$phone_number, 
                                                      "email"=>$email, 
                                                      "applicantID"=>$applicantID, 
                                                      "indexNumber"=>$indexNumber];
                                                                        
                                     Producer::queue("GePGBillSubmitionQueue", $dataToQueue);

				    //$this->submitBill($bill_number, $amount, $name, $phone_number, $email, $applicantID, $indexNumber);
									##end save bill number
            			

        }
    }

}

