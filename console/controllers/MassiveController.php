<?php

namespace console\controllers;

use Yii;
use common\components\Controller;
use yii\console\Controller;
use yii\filters\VerbFilter;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use frontend\modules\application\rabbit\Producer;
use backend\modules\application\models\Application;
date_default_timezone_set('Africa/Nairobi');
/**
 * ApplicantController implements the CRUD actions for Applicant model.
 */
class MassiveController extends Controller {
               

      public function actionIndex() {

        //$query1 = " SELECT * FROM application WHERE (control_number IS NULL OR control_number='0') AND bill_number IS NOT NULL ORDER BY application_id DESC";
        //$Results1 = Yii::$app->db->createCommand($query1)->queryAll();
		
		$query1 = "SELECT * FROM application WHERE application_id=1474";
        $Results1 = Yii::$app->db->createCommand($query1)->queryAll();
               
       
      
        foreach ($Results1 as $Result1) {
            $applicantID = $Result1['applicant_id'];
			$bill_number = $Result1['bill_number'];
			$amount = $Result1['amount_paid'];
			$date_bill_generated = $Result1['date_bill_generated'];
			
			$to_time = strtotime(date("Y-m-d H:i:s"));
            $from_time = strtotime($date_bill_generated);
            //echo "from_time==".$date_bill_generated."---to---".date("Y-m-d H:i:s")." ".round(abs($to_time - $from_time) / 60,2). " minute"."<br/>";
			$pendingTimeInMinutes=round(abs($to_time - $from_time) / 60,2);
			$setTimeTowaightInMinutes=5;
			
			$applicatDetails=Application::getApplicantDetails($applicantID);
			$name=$applicatDetails->user->firstname." ".$applicatDetails->user->middlename." ".$applicatDetails->user->surname;
			$phone_number=$applicatDetails->user->phone_number;
			$email=$applicatDetails->user->email_address;
			$applicantID=$applicatDetails->applicant_id;
			$indexNumber=$applicatDetails->f4indexno; 
            			
				//if($pendingTimeInMinutes > $setTimeTowaightInMinutes){
				$dataToQueue = ['billNumber' => $bill_number, 
                                                      "amount"=>$amount, 
                                                      "name"=>$name, 
                                                      "phone_number"=>$phone_number, 
                                                      "email"=>$email, 
                                                      "applicantID"=>$applicantID, 
                                                      "indexNumber"=>$indexNumber];

            Producer::queue("GePGBillSubmitionQueue", $dataToQueue);
           
         //}
        }
    }

}

